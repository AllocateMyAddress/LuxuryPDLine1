﻿namespace Receiver
{
    partial class frmReceiver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lsvMsgList = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // lsvMsgList
            // 
            this.lsvMsgList.Location = new System.Drawing.Point(230, 127);
            this.lsvMsgList.Margin = new System.Windows.Forms.Padding(4);
            this.lsvMsgList.Name = "lsvMsgList";
            this.lsvMsgList.Size = new System.Drawing.Size(340, 196);
            this.lsvMsgList.TabIndex = 1;
            this.lsvMsgList.UseCompatibleStateImageBehavior = false;
            // 
            // frmReceiver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lsvMsgList);
            this.Name = "frmReceiver";
            this.Text = "frmReceiver";
            this.Load += new System.EventHandler(this.frmReceiver_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lsvMsgList;
    }
}