﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GetData
{
   public class CsLogFun
    {
        public static object locker = new object();
        /// <summary>
        /// 生成错误信息记录
        /// </summary>
        /// <param name="err"></param>
        public static void WriteToLog(Exception err)
        {
            try
            {
                string path = @"D:\GetData\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(@"D:\GetData"))
                {
                    Directory.CreateDirectory(@"D:\GetData");
                }
                lock (locker)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine("报错时间:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss"));
                            sw.WriteLine("出错文件：" + "原因：" + err.ToString());
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                CsLogFun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }

        }
        /// <summary>
        /// 生成错误信息记录
        /// </summary>
        /// <param name="err"></param>
        public static void WriteToLog(string err)
        {
            try
            {
                string path = @"D:\GetData\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(@"D:\GetData"))
                {
                    Directory.CreateDirectory(@"D:\GetData");
                }
                lock (locker)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine("报错时间:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss"));
                            sw.WriteLine("出错文件：" + "原因：" + err.ToString());
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CsLogFun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }

        }
    }
}
