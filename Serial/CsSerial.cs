﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;

namespace Serial
{
    public class CsSerial
    {

        List<TimeSpan> dateTimes = new List<TimeSpan>();
        SerialPort SerialPort1 = new SerialPort();
        public bool  SerialOpen(string PortName, string BaudRate, string DataBits)
        {
            Int32 int2 = Convert.ToInt32(BaudRate);
            Int32 int3 = Convert.ToInt32(DataBits);
            try
            {
                if (PortName == null)
                {
                    return false;
                }
                if (SerialPort1.IsOpen == true)
                {
                    SerialPort1.Close();
                }
                SerialPort1.PortName = PortName;
                SerialPort1.BaudRate = int2;
                SerialPort1.DataBits = int3;
                SerialPort1.StopBits = StopBits.One;

                SerialPort1.Open();
                if (!SerialPort1.IsOpen)
                {
                    return false;
                }
                //SerialPort1.PinChanged += new SerialPinChangedEventHandler(SerialPort_PinChanged);
            }
            catch (Exception err)
            {
                return false;
                //MessageBox.Show(err.Message.ToString());
            }
            return true;
        }
        public void Serialclose()
        {
            if (SerialPort1.IsOpen)
            {
                SerialPort1.Close();
            }

        }
        public void Senddata(string str1, bool Isnot16)
        {
            try
            {
                if (!SerialPort1.IsOpen)
                {
                    return;
                }
                Byte[] data = Encoding.Default.GetBytes(str1);
                if (Isnot16)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        byte temp = data[i];
                        string tempHex = temp.ToString("X2") + " ";
                        SerialPort1.Write(tempHex);
                    }
                }
                else
                {
                    SerialPort1.Write(data, 0, data.Length);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message.ToString());
            }

        }
        //private void SerialPort_PinChanged(object sender, SerialPinChangedEventArgs e)
        //{
        //    if (e.EventType == SerialPinChange.Break)
        //    {
        //        TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
        //        dateTimes.Add(timeSpanstart);
        //        if (dateTimes.Count > 1)
        //        {
        //            int count = dateTimes.Count;
        //            TimeSpan ts = dateTimes[count - 1].Subtract(dateTimes[count]).Duration();
        //            MessageBox.Show(ts.ToString());
        //        }
        //    }
        //}

    }
}
