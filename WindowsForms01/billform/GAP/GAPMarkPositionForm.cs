﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class GAPMarkPositionForm : Form
    {
        #region 变量
        string WaiCode = "";
        string PoType_v = "";
        string CatornStyle_guid_v = "";
        int MarkPattern_v = 0;
        int CatornType_v = 0;
        int IsRotate_v = 0;
        int LabelingType_v = 0;
        int PartitionType_v = 0;
        string Poguid_v = "";
        int long_v = 0;
        int width_v = 0;
        int height_v = 0;
        int top_v = 0;
        int left_v = 0;
        int weight_v = 0;
        int GoodWeight_v = 0;
        int ColumnNunber_v = 0;
        int LayernumtBox_v = 0;
        int ThicktBox_v = 0;
        int JianspeedtBox_v = 0;
        int GrabspeedtBox_v = 0;
        int Orientation_v = 0;
        int LineNum_v = 0;
        public string packingqty = "0";
        public string IsUnpacking = "";
        public string MarkingIsshieid = "";
        public string PrintIsshieid = "";
        public string GAPIsPrintRun = "";
        public string IsPalletizing = "";
        public string Partitiontype = "";
        public string IsPackingRun = "";
        public string IsYaMaHaRun = "";
        public string IsSealingRun = "";
        public string IsScanRun = "";
        public string IsPalletizingRun = "";
        public string TiaoMaVal = "";
        public string HookDirection = "";
        DataGridView PakingDvg = null;
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        GAPChoiceRotateForm GAPChoicerotateform = null;
        PackingForm Packingform = null;
        #endregion
        public GAPMarkPositionForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            NewShow();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            batchSetbtn.Visible = false;
        }
        /// <summary>
        /// 设置纸箱参数信息
        /// </summary>
        /// <param name="id_"></param>
        /// <param name="long_"></param>
        /// <param name="width_"></param>
        /// <param name="Heghit_"></param>
        /// <param name="TntervalTop_"></param>
        /// <param name="TntervalLeft_"></param>
        /// <param name="CartonWeight_"></param>
        /// <param name="MarkPattern_"></param>
        /// <param name="CatornType_"></param>
        /// <param name="IsRotate_"></param>
        /// <param name="LabelingType_"></param>
        /// <param name="PartitionType_"></param>
        public GAPMarkPositionForm(string id_, string long_, string width_, string Heghit_, string TntervalTop_, string TntervalLeft_, string CartonWeight_, string GoodWeight_, string ColumnNunber_, string LineNum_, string Layernum_, string Thick_, string Jianspeed_, string Grabspeed_, string MarkPattern_, string CatornType_, string IsRotate_, string LabelingType_, string PartitionType_, string Orientation_, string OrderNoQty_, string CatornStyle_guid_,DataGridView PakingDvg_)
        {
            InitializeComponent();
            PoguidtBox.Text = id_;
            longtBox.Text = long_;
            widthtBox.Text = width_;
            heighttBox.Text = Heghit_;
            toptBox.Text = TntervalTop_;
            lefttBox.Text = TntervalLeft_;
            weighttBox.Text = CartonWeight_;
            GoodWeighttBox.Text = GoodWeight_;
            ColumntBox.Text = ColumnNunber_;
            LinetBox.Text = LineNum_;
            LayernumtBox.Text = Layernum_;
            ThicktBox.Text = Thick_;
            //JianspeedtBox.Text = "1";
                //JianspeedtBox.Text = Jianspeed_;
            GrabspeedtBox.Text = Grabspeed_;
            MarkPatterncBox.Text = MarkPattern_;
            CatornTypecBox.Text = CatornType_;
            IsRotatetBox.Text = IsRotate_;
            LabelingTypecbox.Text = LabelingType_;
            PartitionTypecBox.Text = PartitionType_;
            OrientationcBox.Text = Orientation_;
            qtytBox.Text = OrderNoQty_;
            CatornStyle_guidtBox.Text = CatornStyle_guid_;
            PakingDvg = PakingDvg_;
            NewShow();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }
        /// <summary>
        /// 查询纸箱参数显示
        /// </summary>
        public GAPMarkPositionForm( string long_, string width_, string Heghit_, string TntervalTop_, string TntervalLeft_, string CartonWeight_, string GoodWeight_, string  ColumnNunber_, string LineNum_, string Layernum_, string Thick_, string Jianspeed_, string Grabspeed_, string MarkPattern_, string CatornType_, string IsRotate_, string LabelingType_, string OrderNoQty_, string PartitionType_, string Orientation_)
        {
            InitializeComponent();
            //idtBox.Text = id;
            longtBox.Text = long_;
            widthtBox.Text = width_;
            heighttBox.Text = Heghit_;
            toptBox.Text = TntervalTop_;
            lefttBox.Text = TntervalLeft_;
            weighttBox.Text = CartonWeight_;
            GoodWeighttBox.Text = GoodWeight_;
            ColumntBox.Text = ColumnNunber_;
            LinetBox.Text = LineNum_;
            LayernumtBox.Text = Layernum_;
            ThicktBox.Text = Thick_;
            JianspeedtBox.Text = Jianspeed_;
            GrabspeedtBox.Text = Grabspeed_;
            MarkPatterncBox.Text = MarkPattern_;
            CatornTypecBox.Text = CatornType_;
            IsRotatetBox.Text = IsRotate_;
            LabelingTypecbox.Text = LabelingType_;
            PartitionTypecBox.Text = PartitionType_;
            OrientationcBox.Text = Orientation_;
            qtytBox.Text = OrderNoQty_;
            foreach (Control contrl in this.Controls)
            {
                contrl.Enabled = false;
            }
            Setbtn.Visible = false;
            batchSetbtn.Visible = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }

        private void GAPMarkPositionForm_Load(object sender, EventArgs e)
        {

        }
        public void NewShow()
        {
            longtBox.ReadOnly = true;
            widthtBox.ReadOnly = true;
            heighttBox.ReadOnly = true;
            toptBox.ReadOnly = true;
            lefttBox.ReadOnly = true;
            weighttBox.ReadOnly = true;
            ColumntBox.ReadOnly = true;
            LayernumtBox.ReadOnly = true;
            qtytBox.ReadOnly = true;
            // qtytBox.ReadOnly = true;
            //ThicktBox.ReadOnly = true;
            //JianspeedtBox.ReadOnly = true;
            //GrabspeedtBox.ReadOnly = true;
            IsRotatetBox.ReadOnly = true;
            MarkPatterncBox.Enabled=false;
            CatornTypecBox.Enabled = false;
            LabelingTypecbox.Enabled = false;
            PartitionTypecBox.Enabled = false;
            OrientationcBox.Enabled = false;
            LayernumtBox.ReadOnly = true;
            LinetBox.ReadOnly = true;
            LinetBox.Text = "1";
            GoodWeighttBox.Text = "1";
            ThicktBox.Text = "50";
            LayernumtBox.Text = "10";
            weighttBox.Text = "1";
            //JianspeedtBox.Text = "90";
        }

        private void choosebtn_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(GAPChoicerotateform, this, "WindowsForms01.GAPChoiceRotateForm");
        }

        private void Setbtn_Click(object sender, EventArgs e)
        {
            try
            {
                //检查设备是否启动
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return;
                }
                List<Control> controls_list1 = new List<Control>();
                List<Control> controls_list2 = new List<Control>();
                Control[] controls_shuzu1 = { longtBox, widthtBox, heighttBox, toptBox, lefttBox, LayernumtBox, GoodWeighttBox, ThicktBox, ColumntBox,qtytBox,LinetBox };
                Control[] controls_shuzu2 = { MarkPatterncBox, CatornTypecBox, IsRotatetBox, LabelingTypecbox, PartitionTypecBox , weighttBox,OrientationcBox };
                foreach (Control ctrl in controls_shuzu1)
                {
                    controls_list1.Add(ctrl);
                }
                foreach (Control ctrl in controls_shuzu2)
                {
                    controls_list2.Add(ctrl);
                }
                if (Chekin.Chekedin(this, controls_list1,"") && Chekin.Chekedin_notnull(this, controls_list2,""))
                {
                    GetContrlVal();
                    #region 计算垛板单位数量
                    M_stcPalletizingData.strCatornLong = long_v.ToString();
                    M_stcPalletizingData.strCatornWidth = width_v.ToString();
                    M_stcPalletizingData.strCatornHeight = height_v.ToString();
                    M_stcPalletizingData.strWsizeFlag = "start";
                    TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                    while (M_stcPalletizingData.ReadPalletizingflag != "1")
                    {
                        TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                        if (ts.Seconds > 1)
                        {
                            break;
                        }
                    }
                    M_stcPalletizingData.strResetFlag = "start";
                    #endregion
                    //string selectsql = string.Format("SELECT*FROM dbo.GAPMarkingPosition WHERE guid='{0}' ", CatornStyle_guid_v);
                    //DataSet ds = content1.Select_nothing(selectsql);
                    int count_v = 0;
                    if (!string.IsNullOrEmpty(CatornStyle_guid_v))
                    {
                         count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.GAPMarkingPosition WHERE guid='{0}' ", CatornStyle_guid_v));
                    }
                    if (string.IsNullOrEmpty(CatornStyle_guid_v)|| count_v==0)
                    {
                        string guid_v = System.Guid.NewGuid().ToString();
                        string sqlstr = string.Format("INSERT INTO GAPMarkingPosition (Long,Width,Heghit,TntervalTop,TntervalLeft,CartonWeight,MarkPattern,CatornType,IsRotate,LabelingType,PartitionType,ColumnNunber,Layernum,Thick,Jianspeed,Grabspeed,Entrytime,GoodWeight,guid,Orientation,LineNum) VALUES ({0},{1},{2},{3},{4},{5},'{6}','{7}','{8}','{9}','{10}',{11},{12},{13},{14},{15},'{16}','{17}','{18}','{19}','{20}')", long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, PartitionType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, JianspeedtBox_v, GrabspeedtBox_v, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), GoodWeight_v, guid_v,Orientation_v,LineNum_v);
                        content1.Select_nothing(sqlstr);
                        //更新装箱单CatornStyle_guid
                        //string strsql = string.Format(" UPDATE dbo.PACKING SET CatornStyle_guid = '{0}' WHERE guid = '{1}'", guid_v, Poguid_v);
                        //content1.Select_nothing(strsql);
                    }
                    else
                    {
                        string sqldelete = string.Format("UPDATE dbo.GAPMarkingPosition  SET  GoodWeight='{0}' ,Thick='{1}',Jianspeed='{2}',Grabspeed='{3}', ColumnNunber='{4}',IsRotate='{5}',Orientation='{7}', LineNum='{8}' WHERE guid='{6}' ", GoodWeight_v, ThicktBox_v, JianspeedtBox_v, GrabspeedtBox_v, ColumnNunber_v, IsRotate_v, CatornStyle_guid_v,Orientation_v,LineNum_v);
                        content1.Select_nothing(sqldelete);
                    }
                    //Packingform = (PackingForm)this.Owner;
                    //Packingform.NewPakingShow();
                    //MessageBox.Show("设置完成");
                    #region 设置为当前订单
                    DialogResult btchose = MessageBox.Show("是否将订单设置为当前", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (btchose == DialogResult.OK)
                    {
                        if (PakingDvg.Rows.Count > 0 && PakingDvg.SelectedRows.Count > 0)
                        {
                            bool isindex_v; int index_v;
                            CsFormShow.DvgGetcurrentIndex(PakingDvg, out isindex_v, out index_v);
                            if (isindex_v)
                            {
                                CsFormShow csformShow = new CsFormShow();
                                csformShow.SetPackingQty(index_v, PakingDvg, this, "箱数", M_stcPalletizingData.Palletizing_Unit.ToString());
                                DataGridViewRow DGVROW = PakingDvg.Rows[index_v];
                                Packingform = (PackingForm)this.Owner;
                                Packingform.packingqty = packingqty;
                                Packingform.IsUnpacking = IsUnpacking;
                                Packingform.MarkingIsshieid = MarkingIsshieid;
                                Packingform.PrintIsshieid = PrintIsshieid;
                                Packingform.GAPIsPrintRun = GAPIsPrintRun;
                                Packingform.IsPalletizing = IsPalletizing;
                                Packingform.Partitiontype = Partitiontype;
                                Packingform.IsPackingRun = IsPackingRun;
                                Packingform.IsYaMaHaRun = IsYaMaHaRun;
                                Packingform.IsSealingRun = IsSealingRun;
                                Packingform.IsPalletizingRun = IsPalletizingRun;
                                Packingform.IsScanRun = IsScanRun;
                                Packingform.TiaoMaVal = TiaoMaVal;
                                Packingform.HookDirection = HookDirection;
                                if (packingqty == "0")
                                {
                                    MessageBox.Show("装箱数量不能为0");
                                    return;
                                }
                                #region 判断是否超订单数
                                bool b_IsExceedPoNumber = IsExceedPoNumber(int.Parse(packingqty), PakingDvg.Rows[index_v].Cells["订单号码"].Value.ToString().Trim(), PakingDvg.Rows[index_v].Cells["Sku号码"].Value.ToString().Trim(), PakingDvg.Rows[index_v].Cells["从"].Value.ToString().Trim());
                                if (b_IsExceedPoNumber)
                                {
                                    DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                    if (diaIsExceedPoNumber == DialogResult.Cancel)
                                    {
                                        return;
                                    }
                                }
                                #endregion
                                bool b_v = Packingform.GetGAPMarkingPositionRowData(PakingDvg.Rows[index_v]);
                                if (b_v)
                                {
                                    //string strsql = string.Format(" UPDATE dbo.PACKING SET packingtotal = ISNULL(packingtotal,0)+{0} WHERE guid = '{1}'", int.Parse(qtytBox.Text.ToString().Trim()), Poguid_v);
                                    //content1.Select_nothing(strsql);
                                    //先判断上一个单是否箱子放完
                                    string changetype = IsPackingEnd();
                                    if (changetype.Contains("取消切单"))
                                    {
                                        return;
                                    }
                                    else if (changetype.Contains("强制切单"))
                                    {
                                        // CsGetData.GAPCompelChange();
                                        return;
                                    }
                                    //确认可以切单后操作
                                    Packingform.SetToMarking(DGVROW);//更新本次喷码信息
                                    Packingform.NewPakingShow();
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("提示：没有可执行的数据");
                            return;
                        }
                    }
                    #endregion
                    //注意此处一定要在这里释放窗体，不然以上设置当前订单会提示对象没有实例
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    return;
                }
     
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        public void GetContrlVal()
        {
            Poguid_v = PoguidtBox.Text.ToString().Trim();
            CatornStyle_guid_v=CatornStyle_guidtBox.Text.ToString().Trim(); 
            long_v = (int)double.Parse(longtBox.Text.ToString().Trim());
            width_v = (int)double.Parse(widthtBox.Text.ToString().Trim());
            height_v = (int)double.Parse(heighttBox.Text.ToString().Trim());
            top_v = (int)double.Parse(toptBox.Text.ToString().Trim());
            left_v = (int)double.Parse(lefttBox.Text.ToString().Trim());
            weight_v = (int)double.Parse(weighttBox.Text.ToString().Trim());
            GoodWeight_v = (int)double.Parse(GoodWeighttBox.Text.ToString().Trim());
            ColumnNunber_v = (int)double.Parse(ColumntBox.Text.ToString().Trim());
            LineNum_v = (int)double.Parse(LinetBox.Text.ToString().Trim());
            LayernumtBox_v = (int)double.Parse(LayernumtBox.Text.ToString().Trim());
            ThicktBox_v = (int)double.Parse(ThicktBox.Text.ToString().Trim());
            //JianspeedtBox_v = (int)double.Parse(JianspeedtBox.Text.ToString().Trim());
           // GrabspeedtBox_v = (int)double.Parse(GrabspeedtBox.Text.ToString().Trim());
            MarkPattern_v = CsMarking.GetCatornParameter(MarkPatterncBox.Text.ToString().Trim());
            CatornType_v = CsMarking.GetCatornParameter(CatornTypecBox.Text.ToString().Trim());
            IsRotate_v = CsMarking.GetCatornParameter(IsRotatetBox.Text.ToString().Trim());
            LabelingType_v = CsMarking.GetCatornParameter(LabelingTypecbox.Text.ToString().Trim());
            PartitionType_v = CsMarking.GetCatornParameter(PartitionTypecBox.Text.ToString().Trim());
            Orientation_v = CsMarking.GetCatornParameter(OrientationcBox.Text.ToString().Trim());

        }
        /// <summary>
        /// 判断是否超订单数
        /// </summary>
        /// <param name="qty_"></param>
        /// <param name="po_"></param>
        /// <param name="SKU_Code_"></param>
        /// <param name="starcode_"></param>
        /// <returns></returns>
        public bool IsExceedPoNumber(int qty_, string po_, string SKU_Code_, string starcode_)
        {
            try
            {
                string sqlstr = string.Format("SELECT*FROM dbo.PACKING WHERE  订单号码='{0}' AND  Sku号码='{1}' AND 从='{2}' AND  箱数<packingtotal+{3} ", po_, SKU_Code_, starcode_, qty_);
                DataSet ds = content1.Select_nothing(sqlstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return false;
        }
        /// <summary>
        /// 判断上一个单装箱完成
        /// </summary>
        /// <returns></returns>
        public string IsPackingEnd()
        {
            try
            {
                if (!string.IsNullOrEmpty(M_stcScanData.strpo))
                {
                    string sqlstr = "";
                    if (M_stcScanData.potype.Contains("GU") && !string.IsNullOrEmpty(CsPakingData.GetMarking_Colorcode("Order_No")))
                    {
                        sqlstr = string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND SKU_Code='{1}' AND Warehouse='{2}' AND CurrentPackingNum=isnull(FinishPackingNum,0) AND potype='{3}'AND Color_Code='{4}' AND Set_Code='{5}'", M_stcScanData.strpo, M_stcScanData.strPoTiaoMa, M_stcScanData.stridentification, "GU", M_stcScanData.Color_Code, M_stcScanData.Set_Code);
                    }
                    else if (M_stcScanData.potype.Contains("GAP") && (!string.IsNullOrEmpty(CsPakingData.GetMarking_Net("订单号码")) || !string.IsNullOrEmpty(CsPakingData.GetMarking_NONet("订单号码"))))
                    {
                        sqlstr = string.Format("SELECT * FROM dbo.MarkingLineUp WHERE  订单号码='{0}'  AND 从='{1}' AND CurrentPackingNum=isnull(FinishPackingNum,0)", M_stcScanData.strpo, M_stcScanData.stridentification);
                    }
                    int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT * FROM dbo.MarkingLineUp WHERE  订单号码='{0}'  AND 从='{1}'", M_stcScanData.strpo, M_stcScanData.stridentification));
                    if (!string.IsNullOrEmpty(sqlstr)&& count_v>0)
                    {
                        DataSet ds = content1.Select_nothing(sqlstr);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            #region 不允许强制切单的情况
                            //MessageBox.Show("本次装箱数量未放完不能切换单");
                            //return "取消切单";
                            #endregion
                            #region 允许强制切单的情况
                            DialogResult diachose = MessageBox.Show("本次装箱数量未放完是否强制切单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                            if (diachose == DialogResult.OK)
                            {
                                MessageBox.Show("请先结束当前订单，切单时停止扫码操作并且将设备调成手动状态");
                                return "强制切单";
                            }
                            else if (diachose == DialogResult.Cancel)
                            {
                                return "取消切单";
                            }
                            #endregion
                        }
                    }
                }
                return "正常切单";
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }


        private void GAPMarkPositionForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //检查设备是否启动
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return;
                }
                List<Control> controls_list1 = new List<Control>();
                List<Control> controls_list2 = new List<Control>();
                Control[] controls_shuzu1 = { longtBox, widthtBox, heighttBox, toptBox, lefttBox, LayernumtBox, GoodWeighttBox, ThicktBox, JianspeedtBox, GrabspeedtBox, ColumntBox, qtytBox, LinetBox };
                Control[] controls_shuzu2 = { MarkPatterncBox, CatornTypecBox, IsRotatetBox, LabelingTypecbox, PartitionTypecBox, weighttBox, OrientationcBox };
                foreach (Control ctrl in controls_shuzu1)
                {
                    controls_list1.Add(ctrl);
                }
                foreach (Control ctrl in controls_shuzu2)
                {
                    controls_list2.Add(ctrl);
                }
                if (Chekin.Chekedin(this, controls_list1,"") && Chekin.Chekedin_notnull(this, controls_list2,""))
                {
                    GetContrlVal();
                    #region 计算垛板单位数量
                    M_stcPalletizingData.strCatornLong = long_v.ToString();
                    M_stcPalletizingData.strCatornWidth = width_v.ToString();
                    M_stcPalletizingData.strCatornHeight = height_v.ToString();
                    M_stcPalletizingData.strWsizeFlag = "start";
                    TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                    while (M_stcPalletizingData.ReadPalletizingflag != "1")
                    {
                        TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                        if (ts.Seconds > 1)
                        {
                            break;
                        }
                    }
                    M_stcPalletizingData.strResetFlag = "start";
                    #endregion
                    //string selectsql = string.Format("SELECT*FROM dbo.GAPMarkingPosition WHERE guid='{0}' ", CatornStyle_guid_v);
                    //DataSet ds = content1.Select_nothing(selectsql);
                    if (string.IsNullOrEmpty(CatornStyle_guid_v))
                    {
                        string guid_v = System.Guid.NewGuid().ToString();
                        string sqlstr = string.Format("INSERT INTO GAPMarkingPosition (Long,Width,Heghit,TntervalTop,TntervalLeft,CartonWeight,MarkPattern,CatornType,IsRotate,LabelingType,PartitionType,ColumnNunber,Layernum,Thick,Jianspeed,Grabspeed,Entrytime,GoodWeight,guid,Orientation,LineNum) VALUES ({0},{1},{2},{3},{4},{5},'{6}','{7}','{8}','{9}','{10}',{11},{12},{13},{14},{15},'{16}','{17}','{18}','{19}','{20}')", long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, PartitionType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, JianspeedtBox_v, GrabspeedtBox_v, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), GoodWeight_v, guid_v, Orientation_v, LineNum_v);
                        content1.Select_nothing(sqlstr);
                        //批量更新装箱单CatornStyle_guid
                        //string sql = string.Format(" UPDATE dbo.PACKING SET CatornStyle_guid = '{0}' WHERE   订单号码 IN(SELECT  ISNULL(订单号码, '') FROM dbo.PACKING WHERE  guid = '{1}') AND NOT EXISTS(SELECT * FROM dbo.MarkingLineUp AS a WHERE a.packing_guid = dbo.PACKING.guid)", CatornStyle_guid_v, Poguid_v);
                        //content1.Select_nothing(sql);
                    }
                    else
                    {
                        string sqldelete = string.Format("UPDATE dbo.GAPMarkingPosition  SET  GoodWeight='{0}' ,Thick='{1}',Jianspeed='{2}',Grabspeed='{3}', ColumnNunber='{4}',IsRotate='{5}',Orientation='{7}', LineNum='{8}' WHERE guid='{6}' ", GoodWeight_v, ThicktBox_v, JianspeedtBox_v, GrabspeedtBox_v, ColumnNunber_v, IsRotate_v, CatornStyle_guid_v, Orientation_v, LineNum_v);
                        content1.Select_nothing(sqldelete);
                        //批量更新装箱单CatornStyle_guid
                        //string sql = string.Format(" UPDATE dbo.PACKING SET CatornStyle_guid = '{0}' WHERE   订单号码 IN(SELECT  ISNULL(订单号码, '') FROM dbo.PACKING WHERE  guid = '{1}') AND NOT EXISTS(SELECT * FROM dbo.MarkingLineUp AS a WHERE a.packing_guid = dbo.PACKING.guid)", CatornStyle_guid_v, Poguid_v);
                        //content1.Select_nothing(sql);
                    }
                    //Packingform = (PackingForm)this.Owner;
                    //Packingform.NewPakingShow();
                    //MessageBox.Show("设置完成");
                    #region 设置为当前订单
                    DialogResult btchose = MessageBox.Show("是否将订单设置为当前", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (btchose == DialogResult.OK)
                    {
                        if (PakingDvg.Rows.Count > 0 && PakingDvg.SelectedRows.Count > 0)
                        {
                            bool isindex_v; int index_v;
                            CsFormShow.DvgGetcurrentIndex(PakingDvg, out isindex_v, out index_v);
                            if (isindex_v)
                            {
                                //判断是否设置装箱数量
                                CsFormShow csformShow = new CsFormShow();
                                csformShow.SetPackingQty(index_v, PakingDvg, this, "箱数", M_stcPalletizingData.Palletizing_Unit.ToString());
                                DataGridViewRow DGVROW = PakingDvg.Rows[index_v];
                                Packingform = (PackingForm)this.Owner;
                                Packingform.packingqty = packingqty;
                                Packingform.IsUnpacking = IsUnpacking;
                                Packingform.MarkingIsshieid = MarkingIsshieid;
                                Packingform.PrintIsshieid = PrintIsshieid;
                                Packingform.GAPIsPrintRun = GAPIsPrintRun;
                                Packingform.IsPalletizing = IsPalletizing;
                                Packingform.Partitiontype = Partitiontype;
                                Packingform.IsPackingRun = IsPackingRun;
                                Packingform.IsYaMaHaRun = IsYaMaHaRun;
                                Packingform.IsSealingRun = IsSealingRun;
                                Packingform.IsPalletizingRun= IsPalletizingRun;
                                Packingform.IsScanRun = IsScanRun;
                                Packingform.TiaoMaVal = TiaoMaVal;
                                Packingform.HookDirection = HookDirection;
                                if (packingqty.Trim() == "0")
                                {
                                    MessageBox.Show("提示：请重新设置装箱数量");
                                    return;
                                }
                                #region 判断是否超订单数
                                bool b_IsExceedPoNumber = IsExceedPoNumber(int.Parse(packingqty), PakingDvg.Rows[index_v].Cells["订单号码"].Value.ToString().Trim(), PakingDvg.Rows[index_v].Cells["Sku号码"].Value.ToString().Trim(), PakingDvg.Rows[index_v].Cells["从"].Value.ToString().Trim());
                                if (b_IsExceedPoNumber)
                                {
                                    DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                    if (diaIsExceedPoNumber == DialogResult.Cancel)
                                    {
                                        return;
                                    }
                                }
                                #endregion
                                bool b_v = Packingform.GetGAPMarkingPositionRowData(PakingDvg.Rows[index_v]);
                                if (b_v)
                                {
                                    //string strsql = string.Format(" UPDATE dbo.PACKING SET packingtotal = ISNULL(packingtotal,0)+{0} WHERE guid = '{1}'", int.Parse(qtytBox.Text.ToString().Trim()), Poguid_v);
                                    //content1.Select_nothing(strsql);
                                    //先判断上一个单是否箱子放完
                                    string changetype = IsPackingEnd();
                                    if (changetype.Contains("取消切单"))
                                    {
                                        return;
                                    }
                                    else if (changetype.Contains("强制切单"))
                                    {
                                        CsGetData.GAPCompelChange();
                                    }
                                    //确认可以切单后操作
                                    Packingform.SetToMarking(DGVROW);//更新本次喷码信息
                                    Packingform.NewPakingShow();
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("提示：没有可执行的数据");
                            return;
                        }
                    }
                    #endregion
                    //注意此处一定要在这里释放窗体，不然以上设置当前订单会提示对象没有实例
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    return;
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void PoguidtBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void JianspeedtBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
