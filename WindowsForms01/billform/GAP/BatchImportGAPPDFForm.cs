﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsForms01
{
    public partial class BatchImportGAPPDFForm : Form
    {
        List<string> list_route = new List<string>();
        PdfForm Pdfform = null;
        public BatchImportGAPPDFForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            routetBox.ReadOnly = true;
        }

        private void BatchImportGAPPDFForm_Load(object sender, EventArgs e)
        {

        }

        private void LoadRoutebtn_Click(object sender, EventArgs e)
        {
            try
            {
                string defaultfilePath = "";
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                dialog.ShowNewFolderButton = false;
                dialog.Description = "请选择文件路径";
                //if (defaultfilePath != "")
                //{
                //    //设置此次默认目录为上一次选中目录  
                //    dialog.SelectedPath = defaultfilePath;
                //}
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK || result == DialogResult.Yes)
                {
                    string foldPath = dialog.SelectedPath;
                    routetBox.Text = defaultfilePath = foldPath;
                    string[] files = Directory.GetFiles(routetBox.Text + @"\", "*.pdf");
                    foreach (string file in files)
                    {
                        list_route.Add(file);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void okbtn_Click(object sender, EventArgs e)
        {
            string FileName_ = "";
            try
            {
                foreach (string FileName_v in list_route)
                {
                    FileName_ = FileName_v;
                    Batch_import(FileName_v);
                }
                MessageBox.Show("导入成功");
                Pdfform = (PdfForm)this.Owner;
                Pdfform.NewPdfShow();
                this.Dispose();
                this.Close();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("路径:" + FileName_ + Environment.NewLine + "文件格式或着内容不正确");
                throw new Exception("提示：" + err.Message);
            }
        }
        public void Batch_import(string FileName_)
        {
            try
            {
                DataTable M_dt = new DataTable();
                M_dt = PDFParser.PDFParser.OnCreated(FileName_, 0);
                //数据存数据库
                for (int i = 0; i < M_dt.Rows.Count; i++)
                {
                    string sqlselect = "SELECT*FROM pdfdata WHERE number=" + "'" + M_dt.Rows[i]["number"].ToString() + "'";
                    DataTable dt_old= CsFormShow.GoSqlSelect(sqlselect);
                    if (dt_old.Rows.Count > 0)
                    {
                        //MessageBox.Show("已存在此条码的记录");
                        continue;
                    }
                    else
                    {
                        string sqlpdf = string.Format("insert into PDFDATA (page,StyleProgram,size,number,po,style,qty,NO1,NO2,shipfrom1,shipfrom2,shipfrom3,shipfrom4,shipto1,shipto2,shipto3,number1,Entrytime) values ({0},'{1}','{2}','{3}','{4}','{5}',{6},'{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}')",
                        M_dt.Rows[i]["page"], M_dt.Rows[i]["StyleProgram"], M_dt.Rows[i]["size"], M_dt.Rows[i]["number"], M_dt.Rows[i]["po"], M_dt.Rows[i]["style"], M_dt.Rows[i]["qty"], M_dt.Rows[i]["NO1"], M_dt.Rows[i]["NO2"], M_dt.Rows[i]["shipfrom1"], M_dt.Rows[i]["shipfrom2"], M_dt.Rows[i]["shipfrom3"], M_dt.Rows[i]["shipfrom4"], M_dt.Rows[i]["shipto1"], M_dt.Rows[i]["shipto2"], M_dt.Rows[i]["shipto3"], M_dt.Rows[i]["number1"].ToString().Replace(" ", ""), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString());
                        CsFormShow.GoSqlUpdateInsert(sqlpdf);
                    }
                }
                Cslogfun.WriteOperationlog("操作员：" + stcUserData.M_username + " ，执行了操作：" + "导入一次‘" + this.Text + "’数据");
                //将当前页的pdf数据写入PLC寄存器
                //for (int i = 0; i < M_dt.Columns.Count; i++)
                //{
                //    string ColumnName = M_dt.Columns[i].ColumnName;
                //    string val = M_dt.Rows[0][i].ToString();
                //    //csGetBao1.WriteToPlc(ColumnName, val,Devicename);
                //}

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
    }
}
