﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GetData;
using System.Threading;

namespace WindowsForms01
{
    public partial class PackingForm : Form
    {
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        inexcel exceltosql = new inexcel();
        FinishPackingForm Finishpackingform = null;
        SetQtyForm  SetQtyForm = null;
        GAPMarkPositionForm GAPMarkpositionform = null;
        GAPSideStartNumForm GAPSideStartNumform = null;
        MarkPositionForm markpositionform = null;
        PositionForm Positionform = null;
        TiaoMaForm tiaoMaform = null;
        MarkStartForm MarkStartform = null;
        BatchImportGAPForm batchImportGAPform = null;
        public string RotateStyle = "";
        public string packingqty = "0";
        public string IsUnpacking = "";
        public string MarkingIsshieid = "";
        public string PrintIsshieid = "";
        public string GAPIsPrintRun = "";
        public string GAPSideIsshieid = "";
        public string IsPalletizing = "";
        public string Partitiontype = "";
        public string IsPackingRun = "";
        public string IsYaMaHaRun = "";
        public string IsSealingRun = "";
        public string IsPalletizingRun = "";
        public string IsRepeatCheck = "";
        public string IsScanRun = "";
        public string TiaoMaVal = "";
        public string HookDirection = "";
        public string GAPSideStartNum_v = "";
        //string th = "";
        public PackingForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.PackingdGV, 40, 40, true, Color.LightSteelBlue,this);
            NewPakingShow();
            if (!stcUserData.M_username.Contains("starcity"))
            {
                toolStripDropDownButton2.Visible = false;
            }
            //CsFormShow.DvgSetcurrent(PackingdGV);
        }
        /// <summary>
        /// 数据导入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 导入EXCELXToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog fd = new OpenFileDialog();
                fd.Filter = "EXCEL文档(*.xls)|*.xls";
                DataTable dt = new DataTable();
                DataTable dttotal = new DataTable();
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    string fileName = fd.FileName;
                    GetData.GetExcelData getData = new GetExcelData();
                    dt = getData.bind(fileName, "Detail", "E15", "AO");
                    GetData.GetExcelData gettotal= new GetExcelData();
                    dttotal = gettotal.bind(fileName, "Detail", "C7", "O9");
                    dt.Columns.Remove("F4");
                    dt.Columns.Remove("F6");
                    dt.Columns.Remove("F8");
                    dt.Columns.Remove("F13");
                    dt.Columns.Remove("F17");
                    dt.Columns.Remove("F30");
                    dt.Columns.Remove("F32");
                    dt.Columns.Remove("F35");

                    dt.Columns[0].ColumnName = "范围";
                    dt.Columns[1].ColumnName = "从";
                    dt.Columns[2].ColumnName = "到";
                    dt.Columns[3].ColumnName = "开始序列号";
                    dt.Columns[4].ColumnName = "截止序列号";
                    dt.Columns[5].ColumnName = "包装代码";
                    dt.Columns[6].ColumnName = "行号";
                    dt.Columns[7].ColumnName = "订单号码";
                    dt.Columns[8].ColumnName = "买方项目号";
                    dt.Columns[9].ColumnName = "Sku号码";
                    dt.Columns[10].ColumnName = "简短描述";
                    dt.Columns[11].ColumnName = "Size";
                    dt.Columns[12].ColumnName = "Color";
                    dt.Columns[13].ColumnName = "Hard Tag Indicator";
                    dt.Columns[14].ColumnName = "数量";
                    dt.Columns[15].ColumnName = "内部包装的项目数量";
                    dt.Columns[16].ColumnName = "内包装计数";
                    dt.Columns[17].ColumnName = "箱数";
                    //dt.Columns[18].ColumnName = "R";
                    dt.Columns[19].ColumnName = "外箱代码";
                    dt.Columns[20].ColumnName = "净净重";
                    dt.Columns[21].ColumnName = "净重";
                    dt.Columns[22].ColumnName = "毛重";
                    dt.Columns[23].ColumnName = "重量单位";
                    dt.Columns[24].ColumnName = "长";
                    dt.Columns[25].ColumnName = "宽";
                    dt.Columns[26].ColumnName = "高";
                    dt.Columns[27].ColumnName = "尺寸单位";
                    dt.Columns[28].ColumnName = "扫描ID";
                    //this.PackingdGV.Sort(PackingdGV.Columns["范围"], System.ComponentModel.ListSortDirection.Ascending);//排序
                }
                #region 录入订单数据到数据库
                if (dt.Rows.Count > 0)
                {
                    DataTable dt_insert = new DataTable();
                    dt_insert = dt.Clone();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string sqlstr = string.Format("SELECT*FROM dbo.PACKING WHERE  Color='{0}' AND 从='{1}' AND 订单号码='{2}' ", dt.Rows[i]["Color"].ToString().Trim(), dt.Rows[i]["从"].ToString().Trim(), dt.Rows[i]["订单号码"].ToString().Trim());
                        DataSet ds = content1.Select_nothing(sqlstr);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            dt.Rows[i]["数量"] = CsGetData.GetNumFromStr(dt.Rows[i]["数量"].ToString().Trim());
                            dt.Rows[i]["箱数"] = CsGetData.GetNumFromStr(dt.Rows[i]["箱数"].ToString().Trim());
                            dt.Rows[i]["从"] = CsGetData.GetNumFromStr(dt.Rows[i]["从"].ToString().Trim());
                            dt.Rows[i]["到"] = CsGetData.GetNumFromStr(dt.Rows[i]["到"].ToString().Trim());
                            dt_insert.ImportRow(dt.Rows[i]);
                        }
                    }
                    if (dt_insert.Rows.Count > 0)
                    {
                        dt_insert.Columns.Add("Entrytime");
                        for (int i = 0; i < dt_insert.Rows.Count; i++)
                        {
                            dt_insert.Rows[i]["Entrytime"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString();
                            dt.Rows[i]["箱数"] = CsGetData.GetNumFromStr(dt.Rows[i]["箱数"].ToString().Trim());
                        }
                        exceltosql.insertToSql(dt_insert, "dbo.PACKING");
                    }
                    //dt_insert.Columns["实际出货数量"].ColumnName = "OUTQTY";
                    MessageBox.Show("导入成功！");
                    #region 更新订单总箱数
                    string Totalqty = "";
                    for (int i = 0; i < dttotal.Rows.Count; i++)
                    {
                        string po_number = dttotal.Rows[i]["PO Number"].ToString();
                        if (dttotal.Rows[i]["PO Number"].ToString().Contains("Totals"))
                        {
                            Totalqty = CsGetData.GetNumFromStr(dttotal.Rows[i][3].ToString().Trim());
                        }
                    }
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE A SET PoQty='{1}' FROM  (SELECT 订单号码,SUM(ISNULL(箱数,0)) AS PoQty FROM dbo.PACKING WHERE 订单号码='{0}' GROUP BY  订单号码) AS POQTY,dbo.PACKING AS A WHERE A.订单号码=POQTY.订单号码", dt.Rows[0]["订单号码"].ToString(), Totalqty));
                    #endregion
                    NewPakingShow();
                    Cslogfun.WriteOperationlog("操作员：" + stcUserData.M_username + " ，执行了操作：" + "导入一次‘" + this.Text + "’数据");
                }
                else
                {
                    MessageBox.Show("没有数据！");
                }
                #endregion

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 置为当前时将当前选择的颜色范围的行数据放在当前队列最前面并清空之前的队列
        /// </summary>
        /// <param name="DgvRow"></param>
        public void SetToMarking(DataGridViewRow DgvRow)
        {
            try
            {
                #region 获取dgv列值
                string guid = DgvRow.Cells["guid"].Value.ToString().Trim();
                string fanwei = DgvRow.Cells["范围"].Value.ToString().Trim();
                string startcode = DgvRow.Cells["从"].Value.ToString().Trim();
                string endcode = DgvRow.Cells["到"].Value.ToString().Trim();
                string qty = DgvRow.Cells["数量"].Value.ToString().Trim();
                string inqty = DgvRow.Cells["内部包装的项目数量"].Value.ToString().Trim();
                string innum = DgvRow.Cells["内包装计数"].Value.ToString().Trim();
                string Cartonqty = DgvRow.Cells["箱数"].Value.ToString().Trim();
                string netweight = DgvRow.Cells["净重"].Value.ToString().Trim();
                string grossweight = DgvRow.Cells["毛重"].Value.ToString().Trim();
                string cartonlong = DgvRow.Cells["长"].Value.ToString().Trim();
                string cartonwidth = DgvRow.Cells["宽"].Value.ToString().Trim();
                string cartonheight = DgvRow.Cells["高"].Value.ToString().Trim();
                string startnum = DgvRow.Cells["开始序列号"].Value.ToString().Trim();
                string endnum = DgvRow.Cells["截止序列号"].Value.ToString().Trim();
                string baocode = DgvRow.Cells["包装代码"].Value.ToString().Trim();
                string wai_code = DgvRow.Cells["外箱代码"].Value.ToString().Trim();
                string po = DgvRow.Cells["订单号码"].Value.ToString().Trim();
                string Skucode = DgvRow.Cells["Sku号码"].Value.ToString().Trim();
                string Color = DgvRow.Cells["Color"].Value.ToString().Trim();
                string finishmarknum = CsGetData.GetGAPMarkingNumber(DgvRow.Cells["guid"].Value.ToString().Trim());
                //string Color_Code = DgvRow.Cells["Color_Code"].Value.ToString().Trim();
                //string Set_Code = DgvRow.Cells["Set_Code"].Value.ToString().Trim();
                string ScanID = DgvRow.Cells["扫描ID"].Value.ToString().Trim();
                string markend = "";
                string labelend = "";
                #endregion
                Dictionary<string, string> DicMarking = new Dictionary<string, string>();
                string sqlstr = string.Format("SELECT*FROM MarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1 and 订单号码='{0}' AND  从='{1}'", po, startcode);
                DataSet ds = content1.Select_nothing(sqlstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("此单已经存在订单队列中");
                    return;
                }
                #region//国外普通单喷码信息
                if (wai_code.ToUpper() != "C4" && wai_code.ToUpper() != "C6")
                {
                    DicMarking.Clear();//清除之前
                    DicMarking["订单号码"] = DgvRow.Cells["订单号码"].Value.ToString().Trim().Substring(0, 5)+"    "+  DgvRow.Cells["订单号码"].Value.ToString().Trim().Substring(5, 2);
                    //DicMarking["订单号码"] = DgvRow.Cells["订单号码"].Value.ToString().Trim().Substring(0, 5);
                    //DicMarking["订单号码2"] = DgvRow.Cells["订单号码"].Value.ToString().Trim().Substring(5, 2);
                    if (!string.IsNullOrEmpty(DgvRow.Cells["内包装计数"].Value.ToString().Trim())&& DgvRow.Cells["内包装计数"].Value.ToString().Trim()!="0")//是多条装一包
                    {
                        DicMarking["SKU/Item"] = DgvRow.Cells["包装代码"].Value.ToString().Trim();
                        int Unit_v = int.Parse(DgvRow.Cells["数量"].Value.ToString().Trim()) / int.Parse(DgvRow.Cells["箱数"].Value.ToString().Trim());
                        DicMarking["Unit/Prepack"] = Unit_v.ToString() + "/" + DgvRow.Cells["内包装计数"].Value.ToString().Trim();
                    }
                    else//一条装一包
                    {
                        int len = DgvRow.Cells["Sku号码"].Value.ToString().Trim().Length;
                        if (len < 14)
                        {
                            for (int i = 0; i < 14 - len ; i++)
                            {
                                DgvRow.Cells["Sku号码"].Value = DgvRow.Cells["Sku号码"].Value.ToString().Trim() + "0";
                            }
                        }
                        DicMarking["SKU/Item"] = DgvRow.Cells["Sku号码"].Value.ToString().Trim().Substring(0, 9) + "-" + DgvRow.Cells["Sku号码"].Value.ToString().Trim().Substring(9, 4);
                        DicMarking["Unit/Prepack"] = DgvRow.Cells["内部包装的项目数量"].Value.ToString().Trim() + "/0";
                    }
                    int Carton_v = int.Parse(DgvRow.Cells["从"].Value.ToString().Trim()) + int.Parse(finishmarknum);
                    DicMarking["Carton"] = Carton_v + "        " + DgvRow.Cells["PoQty"].Value.ToString().Trim();
                    DicMarking["从"] = DgvRow.Cells["从"].Value.ToString().Trim();
                    DicMarking["到"] = DgvRow.Cells["到"].Value.ToString().Trim();
                    DicMarking["箱数"] =DgvRow.Cells["箱数"].Value.ToString().Trim();
                    DicMarking["订单总箱数"] = DgvRow.Cells["PoQty"].Value.ToString().Trim();
                    DicMarking["END"] = "";
                    DicMarking["po"] = DgvRow.Cells["订单号码"].Value.ToString().Trim();
                    DicMarking["Color"] = DgvRow.Cells["Color"].Value.ToString().Trim();
                    DicMarking["开始序列号"] = DgvRow.Cells["开始序列号"].Value.ToString().Trim();
                    DicMarking["Carton1"] = DgvRow.Cells["从"].Value.ToString().Trim();
                    DicMarking["Carton2"] = DgvRow.Cells["到"].Value.ToString().Trim();
                    DicMarking["Cartoncount"] = "1";
                    DicMarking["CurrentPackingNum"] = packingqty;
                    DicMarking["FinishPalletizingNum"] = "0";
                    DicMarking["扫描ID"] = DgvRow.Cells["扫描ID"].Value.ToString().Trim();
                    DicMarking["外箱代码"] = DgvRow.Cells["外箱代码"].Value.ToString().Trim();
                    DicMarking["packing_guid"] = DgvRow.Cells["guid"].Value.ToString().Trim();
                    DicMarking["长"] = DgvRow.Cells["长"].Value.ToString().Trim();
                    DicMarking["宽"] = DgvRow.Cells["宽"].Value.ToString().Trim();
                    DicMarking["高"] = DgvRow.Cells["高"].Value.ToString().Trim();
                    DicMarking["重量"] = DgvRow.Cells["毛重"].Value.ToString().Trim();
                    DicMarking["PrintIsshieid"] = PrintIsshieid;
                    DicMarking["GAPIsPrintRun"] = GAPIsPrintRun;
                    DicMarking["GAPSideIsshieid"] = GAPSideIsshieid;
                    DicMarking["MarkingIsshieid"] = MarkingIsshieid;
                    DicMarking["IsUnpacking"] = IsUnpacking;
                    DicMarking["IsPalletizing"] = IsPalletizing;
                    DicMarking["PartitionType"] = Partitiontype;
                    DicMarking["IsPalletizing"] = CsMarking.GetCatornParameter(IsPalletizing).ToString();
                    CsPakingData.M_list_DicMarkingNONet.Add(DicMarking);
                    string insertsql = string.Format("INSERT INTO dbo.MarkingLineUp (packing_guid,范围,从,到,数量,内部包装的项目数量,内包装计数,箱数,净重,毛重,长,宽,高,开始序列号,截止序列号,包装代码,外箱代码,订单号码,Sku号码,Color,扫描ID,喷码完成,贴标完成,Entrytime,Cartoncount,CurrentPackingNum,PrintIsshieid,IsPalletizing,MarkingIsshieid,IsUnpacking,PartitionType,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,TiaoMaVal,PoQty,IsScanRun,GAPIsPrintRun,HookDirection,GAPSideIsshieid) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}')", guid, fanwei, startcode, endcode, qty, inqty, innum, Cartonqty, netweight, grossweight, cartonlong, cartonwidth, cartonheight, startnum, endnum, baocode, wai_code, po, Skucode, Color, ScanID, markend, labelend, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(),1,packingqty, PrintIsshieid,IsPalletizing,MarkingIsshieid,IsUnpacking,Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, TiaoMaVal,DgvRow.Cells["PoQty"].Value.ToString().Trim(),IsScanRun,GAPIsPrintRun, HookDirection,GAPSideIsshieid);
                    content1.Select_nothing(insertsql);
                    //M_MarkingData.M_PackingCode = "1";
                    //写入喷码参数到plc
                    CsMarking.M_MarkParameter = "NONet";
                }
                #endregion
                #region //国外网单喷码信息
                else if (wai_code.ToUpper() == "C4" || wai_code.ToUpper() == "C6")
                {
                    DicMarking.Clear();//清除之前
                    int Carton_v = int.Parse(DgvRow.Cells["从"].Value.ToString().Trim()) + int.Parse(finishmarknum);
                    DicMarking["Carton"] = Carton_v + "                " + DgvRow.Cells["PoQty"].Value.ToString().Trim();
                    DicMarking["订单号码"] = DgvRow.Cells["订单号码"].Value.ToString().Trim().Substring(0, 5) + "    " + DgvRow.Cells["订单号码"].Value.ToString().Trim().Substring(5, 2);
                    //DicMarking["订单号码1"] = DgvRow.Cells["订单号码"].Value.ToString().Trim().Substring(0, 5);
                    //DicMarking["订单号码2"] = DgvRow.Cells["订单号码"].Value.ToString().Trim().Substring(5, 2);
                    DicMarking["style/size"] = DgvRow.Cells["Sku号码"].Value.ToString().Trim().Substring(0, 9)
                    + "        " + DgvRow.Cells["Sku号码"].Value.ToString().Trim().Substring(9, 4);
                    //DicMarking["style"] = DgvRow.Cells["Sku号码"].Value.ToString().Trim().Substring(0, 6);
                    //DicMarking["size"] = DgvRow.Cells["Sku号码"].Value.ToString().Trim().Substring(9, 4);
                    if (!string.IsNullOrEmpty(DgvRow.Cells["内包装计数"].ToString().Trim()) && DgvRow.Cells["内包装计数"].ToString().Trim() != "0")//是多条装一包
                    {
                        int Unit_v = int.Parse(DgvRow.Cells["数量"].ToString().Trim()) / int.Parse(DgvRow.Cells["箱数"].ToString().Trim());
                        DicMarking["qty"] = Unit_v + "/" + DgvRow.Cells["内包装计数"].ToString().Trim();
                    }
                    else//一条装一包
                    {
                        DicMarking["qty"] = DgvRow.Cells["内部包装的项目数量"].ToString().Trim() ;
                    }
                    DicMarking["GrossWeight"] = DgvRow.Cells["毛重"].Value.ToString().Trim();
                    DicMarking["NetWeight"] = DgvRow.Cells["净重"].Value.ToString().Trim();
                    DicMarking["从"] = DgvRow.Cells["从"].Value.ToString().Trim();
                    DicMarking["到"] = DgvRow.Cells["到"].Value.ToString().Trim();
                    DicMarking["箱数"] = DgvRow.Cells["箱数"].Value.ToString().Trim();
                    DicMarking["订单总箱数"] = DgvRow.Cells["PoQty"].Value.ToString().Trim();
                    DicMarking["END"] = "";
                    DicMarking["po"] = DgvRow.Cells["订单号码"].Value.ToString().Trim();
                    DicMarking["Color"] = DgvRow.Cells["Color"].Value.ToString().Trim();
                    DicMarking["开始序列号"] = DgvRow.Cells["开始序列号"].Value.ToString().Trim();
                    DicMarking["Carton1"] = DgvRow.Cells["从"].Value.ToString().Trim();
                    DicMarking["Carton2"] = DgvRow.Cells["到"].Value.ToString().Trim();
                    DicMarking["StartNum"] = Carton_v.ToString();
                    DicMarking["Cartoncount"] = "1";
                    DicMarking["CurrentPackingNum"] = packingqty;
                    DicMarking["FinishPalletizingNum"] = "0";
                    DicMarking["外箱代码"] = DgvRow.Cells["外箱代码"].Value.ToString().Trim();
                    DicMarking["packing_guid"] = DgvRow.Cells["guid"].Value.ToString().Trim();
                    DicMarking["长"] = DgvRow.Cells["长"].Value.ToString().Trim();
                    DicMarking["宽"] = DgvRow.Cells["宽"].Value.ToString().Trim();
                    DicMarking["高"] = DgvRow.Cells["高"].Value.ToString().Trim();
                    DicMarking["重量"] = DgvRow.Cells["毛重"].Value.ToString().Trim();
                    DicMarking["GAPSideStartNum"] = DgvRow.Cells["GAPSideStartNum"].Value.ToString().Trim();
                    DicMarking["PrintIsshieid"] =  CsMarking.GetCatornParameter(PrintIsshieid).ToString();
                    DicMarking["GAPIsPrintRun"] = CsMarking.GetCatornParameter(GAPIsPrintRun).ToString();
                    DicMarking["GAPSideIsshieid"] = CsMarking.GetCatornParameter(GAPSideIsshieid).ToString();
                    DicMarking["MarkingIsshieid"] = CsMarking.GetCatornParameter(MarkingIsshieid).ToString();
                    DicMarking["PartitionType"] = Partitiontype;
                    DicMarking["IsUnpacking"] = IsUnpacking ;
                    DicMarking["IsScanRun"] =  CsMarking.GetCatornParameter(IsScanRun).ToString();
                    DicMarking["HookDirection"] = CsMarking.GetCatornParameter(HookDirection).ToString();
                    DicMarking["IsPackingRun"] = CsMarking.GetCatornParameter(IsPackingRun).ToString();
                    DicMarking["IsYaMaHaRun"] = CsMarking.GetCatornParameter(IsYaMaHaRun).ToString();
                    DicMarking["IsSealingRun"] = CsMarking.GetCatornParameter(IsSealingRun).ToString();
                    DicMarking["IsPalletizingRun"] =CsMarking.GetCatornParameter(IsPalletizingRun).ToString();
                    DicMarking["IsPalletizing"] = CsMarking.GetCatornParameter(IsPalletizing).ToString();
                    DicMarking["HookDirection"] = CsMarking.GetCatornParameter(HookDirection).ToString();
                    DicMarking["packing_guid"] = DgvRow.Cells["guid"].Value.ToString().Trim();
                    CsPakingData.M_list_DicMarkingNet.Add(DicMarking);

                    //M_MarkingData.M_PackingCode = "2";
                    string insertsql = string.Format("INSERT INTO dbo.MarkingLineUp (packing_guid,范围,从,到,数量,内部包装的项目数量,内包装计数,箱数,净重,毛重,长,宽,高,开始序列号,截止序列号,包装代码,外箱代码,订单号码,Sku号码,Color,扫描ID,喷码完成,贴标完成,Entrytime,Cartoncount,CurrentPackingNum,PrintIsshieid,MarkingIsshieid,IsUnpacking,IsPalletizing,PartitionType,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,TiaoMaVal,PoQty,IsScanRun,GAPIsPrintRun,HookDirection,GAPSideIsshieid) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}')", guid, fanwei, startcode, endcode, qty, inqty, innum, Cartonqty, netweight, grossweight, cartonlong, cartonwidth, cartonheight, startnum, endnum, baocode, wai_code, po, Skucode, Color, ScanID, markend, labelend, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(),1,packingqty, PrintIsshieid, MarkingIsshieid, IsUnpacking,IsPalletizing,Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun,TiaoMaVal, DgvRow.Cells["PoQty"].Value.ToString().Trim(),IsScanRun, GAPIsPrintRun, HookDirection,GAPSideIsshieid);
                    content1.Select_nothing(insertsql);
                    //设置喷码参数到plc
                    CsMarking.M_MarkParameter = "Net";
                }
                #endregion
                //加入历史记录表
                 int count_v= CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.HistoricRcords WHERE po_guid='{0}'", guid));
                if (count_v==0)
                {
                    string gaplineup_guid = CsFormShow.SqlGetVal(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE   订单号码='{0}' AND 从='{1}' ", DgvRow.Cells["订单号码"].Value.ToString().Trim(), DicMarking["从"]), "guid");

                    CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO HistoricRcords (po_guid,LineUp_guid,StartTime,Total) VALUES('{0}','{1}','{2}','{3}')", guid, gaplineup_guid, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") , DgvRow.Cells["箱数"].Value.ToString().Trim()));
                }
                //更新扫码队列
                CsGetData.UpdataScanLineUp();
                string strsql = string.Format(" UPDATE dbo.PACKING SET TiaoMaVal='{1}' WHERE guid = '{2}'", int.Parse(packingqty), TiaoMaVal,guid);
                content1.Select_nothing(strsql);
                MessageBox.Show("切换单完成");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }

        private void PackingForm_Load(object sender, EventArgs e)
        {
        }

        private void 置位当前ToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            DialogResult dia = MessageBox.Show("是否手动执行一次喷码动作？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dia == DialogResult.No)
            {
                return;
            }
            else
            {
                CsMarking.ManualUpdatePackingPage();
            }

        }

        private void 删除数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (PackingdGV.SelectedRows.Count == 0)
                {
                    MessageBox.Show("请选择有效订单数据！！");
                    return;
                }
                for (int i = 0; i < PackingdGV.SelectedRows.Count; i++)
                {
                    bool b_v = (bool)PackingdGV.SelectedRows[i].Cells["已选择"].Value;
                    if (b_v)
                    {
                        MessageBox.Show("订单数据已经在排单中，请先结束排单再删除！！");
                        return;
                    }
                }
                Delet_click.Delete_TSMenuItem_Click(this, PackingdGV, "dbo.PACKING");
                NewPakingShow();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        public void NewPakingShow()
        {
            try
            {
                PackingdGV.DataSource = null;
                string sqlselect = "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY Entrytime, 订单号码, 从) AS '序号',(SELECT CASE WHEN COUNT(0) <> 0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END FROM dbo.SchedulingTable WHERE  po_guid = dbo.PACKING.guid) AS '已选择',guid,订单号码,范围,从,到,数量,内部包装的项目数量,内包装计数,PoQty as '订单总箱数',箱数,finishmarknumber AS 已完成数量,净重,毛重,长,宽,高,开始序列号,截止序列号,包装代码,Sku号码,Color,Size,扫描ID,外箱代码,Entrytime,packingtotal,finishmarknumber,dbo.PACKING.TiaoMaVal,(SUBSTRING(Sku号码,0,7)+'-'+SUBSTRING(Sku号码,7,2)+'-'+SUBSTRING(Sku号码,9,1)+'-'+SUBSTRING(Sku号码,10,5)) AS sku  FROM dbo.PACKING WHERE  ISNULL(ENDPO, 0) <> 1 ) AS a  ORDER BY 已选择 DESC";
                DataTable dt  =CsFormShow.GoSqlSelect(sqlselect);
                //ds1.Tables[0].DefaultView.Sort = "从 ASC";//升序排列
                //DataTable dt1 = ds1.Tables[0].DefaultView.ToTable();//返回一个新的DataTable
                //ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(0);
                if (dt.Rows.Count>0)
                {
                    this.PackingdGV.DataSource =dt;
                    // PackingdGV.Columns["当前选择"].Visible = false;
                    // PackingdGV.Columns["当前选择"].ReadOnly = true;
                    // PackingdGV.Columns["CatornStyle_guid"].Visible = false;
                    PackingdGV.Columns["guid"].Visible = false;
                    // PackingdGV.Columns["开始序列号"].Visible = false;
                    PackingdGV.Columns["截止序列号"].Visible = false;
                    PackingdGV.Columns["Size"].Visible = false;
                    PackingdGV.Columns["sku"].Visible = false;
                    PackingdGV.Columns["finishmarknumber"].Visible = false;
                    PackingdGV.Columns["packingtotal"].Visible = false;
                    PackingdGV.Columns["扫描ID"].Visible = false;
                    for (int i = 0; i < this.PackingdGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.PackingdGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        public void NewPakingShow(string po,string sku_)
        {
            try
            {
                PackingdGV.DataSource = null;
                string sqlselect = "SELECT*FROM (SELECT ROW_NUMBER() OVER(ORDER BY Entrytime, 订单号码, 从)AS 序号,(SELECT CASE WHEN COUNT(0) <> 0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END FROM dbo.SchedulingTable WHERE   po_guid = dbo.PACKING.guid) AS '已选择',guid,订单号码,范围,从,到,数量,内部包装的项目数量,内包装计数,PoQty as '订单总箱数',箱数,finishmarknumber AS 已完成数量,净重,毛重,长,宽,高,开始序列号,截止序列号,包装代码,Sku号码,Color,Size,扫描ID,外箱代码,Entrytime,packingtotal,finishmarknumber,dbo.PACKING.TiaoMaVal,(SUBSTRING(Sku号码,0,7)+'-'+SUBSTRING(Sku号码,7,2)+'-'+SUBSTRING(Sku号码,9,1)+'-'+SUBSTRING(Sku号码,10,5)) AS sku   FROM dbo.PACKING WHERE Sku号码 LIKE  '%' + '" + sku_ + "' + '%' AND  订单号码 LIKE '%' + '" + po + "' + '%' and ISNULL(ENDPO,0)<> 1)  AS a ORDER BY 已选择 DESC";
                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                //ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                //PackingdGV.Columns["当前选择"].Visible = false;
                // PackingdGV.Columns["当前选择"].ReadOnly = true;
                if (dt.Rows.Count>0)
                {
                    this.PackingdGV.DataSource = dt;
                    //PackingdGV.Columns["CatornStyle_guid"].Visible = false;
                    PackingdGV.Columns["guid"].Visible = false;
                    //PackingdGV.Columns["开始序列号"].Visible = false;
                    PackingdGV.Columns["截止序列号"].Visible = false;
                    PackingdGV.Columns["Size"].Visible = false;
                    PackingdGV.Columns["sku"].Visible = false;
                    PackingdGV.Columns["finishmarknumber"].Visible = false;
                    PackingdGV.Columns["packingtotal"].Visible = false;
                    PackingdGV.Columns["扫描ID"].Visible = false;
                    for (int i = 0; i < this.PackingdGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.PackingdGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void SelecttSpBtn_Click(object sender, EventArgs e)
        {
            NewPakingShow(PotSpTBox.TextBox.Text.ToString().Trim(),SkutSpTBox.Text.ToString().Trim());
        }

        private void 纸箱喷码尺寸ToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void FinishtSpBtn_Click(object sender, EventArgs e)
        {
            Finishpackingform = (WindowsForms01.FinishPackingForm)CsFormShow.FormShow(Finishpackingform, null, "WindowsForms01.FinishPackingForm");
        }


        private void PackingdGV_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
        }

        private void 设置纸箱信息toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                //bool boolval = CsFormShow.DvgRowSetcurrent(PackingdGV);
                //if (!boolval)
                //    return;
                if (PackingdGV.SelectedRows.Count > 1 || String.IsNullOrEmpty(PackingdGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim()))
                {
                    MessageBox.Show("请选择一行有效数据");
                    return;
                }
                bool b_v =CsFormShow.IsExistenceInSql("dbo.SchedulingTable", "po_guid", PackingdGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim());
                if (b_v)
                {
                    MessageBox.Show("此订单已经在排单列表中");
                    return;
                }
                // string guid_v = PackingdGV.SelectedRows[0].Cells["guid"].Value.ToString();
                Dictionary<string, string> DicPaking = new Dictionary<string, string>();
                DicPaking =CsGetData.GetCatornData(PackingdGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim());
                string id_v = DicPaking["装箱单guid"];
                //string CatornStyle_guid_v= DicPaking["CatornStyle_guid"];
                string long_v =((int)(double.Parse( DicPaking["箱子长"].ToString().Trim())*10)).ToString();
                string width_v = ((int)(double.Parse(DicPaking["箱子宽"].ToString().Trim()) * 10)).ToString();
                string Heghit_v = ((int)(double.Parse(DicPaking["箱子高"].ToString().Trim()) * 10)).ToString();
                bool isindex_v; int index_v;
                CsFormShow.DvgGetcurrentIndex(PackingdGV, out isindex_v, out index_v);
                if (isindex_v)
                {
                    if (SetQtyForm == null || SetQtyForm.IsDisposed)
                    {
                        SetQtyForm = new SetQtyForm(DicPaking["装箱单guid"], DicPaking["箱子长"], DicPaking["箱子宽"], DicPaking["箱子高"], DicPaking["距离底部"], DicPaking["距离左边"], DicPaking["重量"], DicPaking["箱内件数"], DicPaking["喷码模式"], DicPaking["纸箱大小"], DicPaking["贴标类型"], DicPaking["隔板类型"], DicPaking["正反方向"], DicPaking["箱数"], PackingdGV, index_v, this, "箱数", M_stcPalletizingData.Palletizing_Unit.ToString());
                        SetQtyForm.Owner = this;
                        SetQtyForm.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        SetQtyForm.ShowDialog();
                    }
                    else
                    {
                        SetQtyForm.ShowDialog();
                        SetQtyForm.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        SetQtyForm.BringToFront();
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 查看当前纸箱参数tStpMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                if (PackingdGV.Rows.Count > 0 && PackingdGV.SelectedRows.Count > 0)
                {

                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(PackingdGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        string selectsql = string.Format("SELECT dbo.MarkingLineUp.箱数, Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '3' THEN '国内' ELSE '国外' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  dbo.GAPSchedulingTable.PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型' , CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向' FROM dbo.PACKING, dbo.GAPSchedulingTable ,dbo.MarkingLineUp  where dbo.PACKING.guid = dbo.GAPSchedulingTable.po_guid AND dbo.MarkingLineUp.packing_guid=dbo.PACKING.guid and  dbo.PACKING.guid='{0}'", PackingdGV.Rows[index_v].Cells["guid"].Value.ToString().Trim());
                        ds = content1.Select_nothing(selectsql);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            MessageBox.Show("当前订单没有设置纸箱参数");
                            return;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("提示：请选择有效订单");
                    return;
                }
                string long_v = ds.Tables[0].Rows[0]["箱子长"].ToString();
                string width_v = ds.Tables[0].Rows[0]["箱子宽"].ToString();
                string Heghit_v = ds.Tables[0].Rows[0]["箱子高"].ToString();
                string TntervalTop_v = ds.Tables[0].Rows[0]["距离底部"].ToString();
                string TntervalLeft_v = ds.Tables[0].Rows[0]["距离左边"].ToString();
                string CartonWeight_v = ds.Tables[0].Rows[0]["重量"].ToString();
                string GoodWeight_v = ds.Tables[0].Rows[0]["产品重量"].ToString();
                string ColumnNunber_v = ds.Tables[0].Rows[0]["列数"].ToString();
                string LineNum_v = ds.Tables[0].Rows[0]["行数"].ToString();
                string Layernum_v = ds.Tables[0].Rows[0]["箱内件数"].ToString();
                string OrderNoQty_v = ds.Tables[0].Rows[0]["箱数"].ToString();
                string Thick_v = ds.Tables[0].Rows[0]["产品厚度"].ToString();
                //string Jianspeed_v = ds.Tables[0].Rows[0]["加减速度"].ToString();
                //string Grabspeed_v = ds.Tables[0].Rows[0]["抓取速度"].ToString();
                string MarkPattern_v = ds.Tables[0].Rows[0]["喷码模式"].ToString();
                string CatornType_v = ds.Tables[0].Rows[0]["纸箱大小"].ToString();
                string IsRotate_v = ds.Tables[0].Rows[0]["是否旋转"].ToString();
                string LabelingType_v = ds.Tables[0].Rows[0]["贴标类型"].ToString();
                string PartitionType_v = ds.Tables[0].Rows[0]["隔板类型"].ToString();
                string Orientation_v = ds.Tables[0].Rows[0]["正反方向"].ToString();
                if (GAPMarkpositionform == null || GAPMarkpositionform.IsDisposed)
                {
                    GAPMarkpositionform = new GAPMarkPositionForm(long_v, width_v, Heghit_v, TntervalTop_v, TntervalLeft_v, CartonWeight_v, GoodWeight_v, ColumnNunber_v,LineNum_v, Layernum_v, Thick_v, "", "", MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, OrderNoQty_v,PartitionType_v,Orientation_v);
                    GAPMarkpositionform.Owner = this;
                    GAPMarkpositionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    GAPMarkpositionform.ShowDialog();
                }
                else
                {
                    Positionform.ShowDialog();
                    Positionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    Positionform.BringToFront();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        public bool  GetGAPMarkingPositionRowData(DataGridViewRow DgvRow)
        {
            bool bool_ = false;
            try
            {
                string Poguid_ = "";
                DataSet ds = new DataSet();
                #region 计算箱件数
                string Thick = "";
                Poguid_ = DgvRow.Cells["guid"].Value.ToString().Trim();
                string count_bao = DgvRow.Cells["内包装计数"].Value.ToString().Trim();
                DataTable dtLayernum = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", Poguid_));
                string waixiang_code = dtLayernum.Rows[0]["外箱代码"].ToString();
                string Layernum = "";
                if (dtLayernum.Rows.Count>0)
                {
                    Layernum = CsGetData.GetLayernum(dtLayernum, IsUnpacking, "GAP").ToString();
                }
                #endregion
                if (!string.IsNullOrEmpty(Poguid_))
                {
                    string selectsql = string.Format("SELECT ISNULL(箱数,0)-ISNULL(finishmarknumber,0) as '箱数', Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '3' THEN '国内' ELSE '国外' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向'  FROM dbo.PACKING, dbo.GAPSchedulingTable  where dbo.PACKING.guid = dbo.GAPSchedulingTable.po_guid and  dbo.PACKING.guid = '{0}'", Poguid_);
                        ds = content1.Select_nothing(selectsql);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            MessageBox.Show("当前订单没有设置纸箱参数");
                            return bool_;
                        }
                        else
                        {
                            //计算厚度
                            int Height =int.Parse( ds.Tables[0].Rows[0]["箱子高"].ToString().Trim());
                            int culumn = int.Parse(ds.Tables[0].Rows[0]["列数"].ToString().Trim());
                            Thick = (Height / (int.Parse(Layernum) / culumn)).ToString();
                            //计算隔板类型
                            int catornlong = int.Parse(ds.Tables[0].Rows[0]["箱子长"].ToString().Trim());
                            int catornwidth = int.Parse(ds.Tables[0].Rows[0]["箱子宽"].ToString().Trim());
                            string PartitionType_v = "";
                            string IsBottomPartition = "";
                            string IsTopPartition = "";
                            CsGetData.GetPartitionData(catornlong, catornwidth, Partitiontype, out PartitionType_v, out IsTopPartition, out IsBottomPartition);
                            CsPakingData.M_DicPakingData["IsTopPartition"] = IsTopPartition;
                            CsPakingData.M_DicPakingData["IsBottomPartition"] = IsBottomPartition;
                            CsPakingData.M_DicPakingData["箱子长"] = ds.Tables[0].Rows[0]["箱子长"].ToString().Trim();
                            CsPakingData.M_DicPakingData["箱子宽"] = ds.Tables[0].Rows[0]["箱子宽"].ToString().Trim();
                            CsPakingData.M_DicPakingData["箱子高"] = ds.Tables[0].Rows[0]["箱子高"].ToString().Trim();
                            CsPakingData.M_DicPakingData["距离底部"] = ds.Tables[0].Rows[0]["距离底部"].ToString().Trim();
                            CsPakingData.M_DicPakingData["距离左边"] = ds.Tables[0].Rows[0]["距离左边"].ToString().Trim();
                            CsPakingData.M_DicPakingData["重量"] = ds.Tables[0].Rows[0]["重量"].ToString().Trim();
                            CsPakingData.M_DicPakingData["产品重量"] = ds.Tables[0].Rows[0]["产品重量"].ToString().Trim();
                            CsPakingData.M_DicPakingData["列数"] = ds.Tables[0].Rows[0]["列数"].ToString().Trim();
                            CsPakingData.M_DicPakingData["行数"] = ds.Tables[0].Rows[0]["行数"].ToString().Trim();
                            CsPakingData.M_DicPakingData["箱内件数"] = Layernum;
                            CsPakingData.M_DicPakingData["箱数"] =packingqty /*ds.Tables[0].Rows[0]["箱数"].ToString().Trim()*/;
                            CsPakingData.M_DicPakingData["产品厚度"] = Thick;
                        //CsPakingData.M_DicPakingData["加减速度"] = ds.Tables[0].Rows[0]["加减速度"].ToString().Trim();
                        // CsPakingData.M_DicPakingData["抓取速度"] = ds.Tables[0].Rows[0]["抓取速度"].ToString().Trim();
                            if (waixiang_code.Contains("C2"))
                            {
                                CsPakingData.M_DicPakingData["喷码模式"] = "5";
                            }
                            else
                            {
                                CsPakingData.M_DicPakingData["喷码模式"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["喷码模式"].ToString().Trim()).ToString();
                            }
                            CsPakingData.M_DicPakingData["纸箱大小"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                            CsPakingData.M_DicPakingData["是否旋转"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["是否旋转"].ToString().Trim()).ToString();
                            CsPakingData.M_DicPakingData["贴标类型"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["贴标类型"].ToString().Trim()).ToString();
                            if (string.IsNullOrEmpty(PartitionType_v))
                            {
                                CsPakingData.M_DicPakingData["PartitionType"] = "0";
                            }
                            else
                            {
                                CsPakingData.M_DicPakingData["隔板类型"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["隔板类型"].ToString().Trim()).ToString();
                            }
                            CsPakingData.M_DicPakingData["正反方向"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["正反方向"].ToString().Trim()).ToString();
                            CsPakingData.M_DicPakingData["IsPalletizing"] = CsMarking.GetCatornParameter(IsPalletizing).ToString();
                            CsPakingData.M_DicPakingData["IsPackingRun"] =   CsMarking.GetCatornParameter(IsPackingRun).ToString();
                            CsPakingData.M_DicPakingData["IsYaMaHaRun"] =    CsMarking.GetCatornParameter(IsYaMaHaRun).ToString();
                            CsPakingData.M_DicPakingData["IsSealingRun"] =    CsMarking.GetCatornParameter(IsSealingRun).ToString();
                            CsPakingData.M_DicPakingData["MarkingIsshieid"] =    CsMarking.GetCatornParameter(MarkingIsshieid).ToString();
                            CsPakingData.M_DicPakingData["PrintIsshieid"] =    CsMarking.GetCatornParameter(PrintIsshieid).ToString();
                            CsPakingData.M_DicPakingData["GAPIsPrintRun"] = CsMarking.GetCatornParameter(GAPIsPrintRun).ToString();
                            CsPakingData.M_DicPakingData["GAPSideIsshieid"] = CsMarking.GetCatornParameter(GAPSideIsshieid).ToString();
                            CsPakingData.M_DicPakingData["IsPalletizingRun"] =    CsMarking.GetCatornParameter(IsPalletizingRun).ToString();
                            CsPakingData.M_DicPakingData["IsScanRun"] = CsMarking.GetCatornParameter(IsScanRun).ToString();
                            CsPakingData.M_DicPakingData["HookDirection"] = CsMarking.GetCatornParameter(HookDirection).ToString();
                            CsPakingData.M_DicPakingData["PoTypeSignal"] = "3";
                        bool_ =true;
                        }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return bool_;
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return;
                }
                DialogResult btchose = MessageBox.Show("是否终止订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    DataSet ds = new DataSet();
                    if (PackingdGV.Rows.Count > 0 && PackingdGV.SelectedRows.Count > 0)
                    {

                        bool isindex_v; int index_v;
                        CsFormShow.DvgGetcurrentIndex(PackingdGV, out isindex_v, out index_v);
                        if (isindex_v)
                        {
                           string strpo = PackingdGV.Rows[index_v].Cells["订单号码"].Value.ToString().Trim();
                            string startcode = PackingdGV.Rows[index_v].Cells["从"].Value.ToString().Trim();
                            string selectsql = string.Format("UPDATE dbo.PACKING SET ENDPO=1,EndTime='{1}' WHERE guid='{0}'", PackingdGV.Rows[index_v].Cells["guid"].Value.ToString().Trim(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            content1.Select_nothing(selectsql);
                            //清除本次队列
                            string sqlstrlineup = string.Format("DELETE FROM  dbo.MarkingLineUp  WHERE 订单号码='{0}' AND 从='{1}'", strpo, startcode);
                            CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                            CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM  dbo.PRINTLineuUp  WHERE po='{0}' ", strpo));
                            CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM  dbo.PalletizingtypeTable  WHERE potype LIKE 'GAP'"));
                            //重新加载喷码队列
                            CsInitialization.UpdateGAPLineUp();
                            //重新加载扫码队列信息
                            CsGetData.UpdataScanLineUp();//更新扫码队列
                            CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                            //更新码垛队列数量
                            CsInitialization.UpdateGAPPalletizingLineUp();
                        }
                    }
                    else
                    {
                        MessageBox.Show("提示：请选择有效订单");
                        return;
                    }
                    NewPakingShow();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 手动执行喷码ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dia = MessageBox.Show("是否手动执行一次喷码动作？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dia == DialogResult.No)
            {
                return;
            }
            else
            {
                CsMarking.ManualUpdatePackingPage();
            }
        }

        private void 手动执行码垛ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsPalletizing.GAPPalletizingLineUpGoOnce();
        }

        private void 修改条码toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                if (PackingdGV.Rows.Count > 0 && PackingdGV.SelectedRows.Count > 0)
                {
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(PackingdGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        if (tiaoMaform == null || tiaoMaform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                        {
                            tiaoMaform =new TiaoMaForm(PackingdGV.Rows[index_v].Cells["guid"].Value.ToString().Trim());
                            tiaoMaform.Owner = this;
                            tiaoMaform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                            tiaoMaform.ShowDialog();
                        }
                        else
                        {
                            tiaoMaform.Show();
                            tiaoMaform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                            tiaoMaform.BringToFront();
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult btchose = MessageBox.Show("是否强制结束当前订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(this.PackingdGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                       bool b_v= CsGetData.GAPCompelChange(this.PackingdGV, index_v);
                        if (!b_v)
                        {
                            return;
                        }
                        MessageBox.Show("已结束当前订单订单");
                        NewPakingShow();
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void SetMarkStarttSpBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (MarkStartform == null || MarkStartform.IsDisposed)
                {
                    MarkStartform = new MarkStartForm();
                    MarkStartform.Owner = this;
                    MarkStartform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    MarkStartform.ShowDialog();
                }
                else
                {
                    MarkStartform.ShowDialog();
                    MarkStartform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    MarkStartform.BringToFront();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            try
            {
                bool isindex_v; int index_v;
                CsFormShow.DvgGetcurrentIndex(PackingdGV, out isindex_v, out index_v);
                if (isindex_v)
                {
                    if (MarkStartform == null || MarkStartform.IsDisposed)
                    {
                        MarkStartform = new MarkStartForm(PackingdGV, index_v);
                        MarkStartform.Owner = this;
                        MarkStartform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        MarkStartform.ShowDialog();
                    }
                    else
                    {
                        MarkStartform.ShowDialog();
                        MarkStartform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        MarkStartform.BringToFront();
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult btchose = MessageBox.Show("是否强制结束当前订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(this.PackingdGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                       bool b_v=  CsGetData.GAPUnusualCompelChange(this.PackingdGV, index_v);
                        if (!b_v)
                        {
                            return;
                        }
                        MessageBox.Show("已结束当前订单订单");
                        NewPakingShow();
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void 批量导入ExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(batchImportGAPform, this, "WindowsForms01.BatchImportGAPForm");
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                bool isindex_v; int index_v;
                CsFormShow.DvgGetcurrentIndex(this.PackingdGV, out isindex_v, out index_v);
                if (isindex_v)
                {
                    if (GAPSideStartNumform == null || GAPSideStartNumform.IsDisposed)
                    {
                        GAPSideStartNumform = new GAPSideStartNumForm();
                        GAPSideStartNumform.Owner = this;
                        GAPSideStartNumform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        GAPSideStartNumform.ShowDialog();
                    }
                    else
                    {
                        GAPSideStartNumform.ShowDialog();
                        GAPSideStartNumform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        GAPSideStartNumform.BringToFront();
                    }
                    if (string.IsNullOrEmpty(GAPSideStartNum_v))
                    {
                        return;
                    }
                    M_stcPrintData.DicSerialManualPrint["TiaoMaVal"]= PackingdGV.Rows[index_v].Cells["TiaoMaVal"].Value.ToString();
                    M_stcPrintData.DicSerialManualPrint["TiaoMaVal"]= PackingdGV.Rows[index_v].Cells["TiaoMaVal"].Value.ToString();
                    M_stcPrintData.DicSerialManualPrint["sku"]= PackingdGV.Rows[index_v].Cells["sku"].Value.ToString(); 
                    M_stcPrintData.DicSerialManualPrint["Color"]= PackingdGV.Rows[index_v].Cells["Color"].Value.ToString();
                    M_stcPrintData.DicSerialManualPrint["Size"]= PackingdGV.Rows[index_v].Cells["Size"].Value.ToString();
                    M_stcPrintData.DicSerialManualPrint["GAPSideStartNum"]=GAPSideStartNum_v;
                    CSReadPlc.serialManualFlag = "start";
                    MessageBox.Show("发送打印完成");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.Message);
            }
        }
    }
}
