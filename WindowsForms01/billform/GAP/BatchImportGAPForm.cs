﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using GetData;

namespace WindowsForms01
{
    public partial class BatchImportGAPForm : Form
    {
        List<string> list_Excelroute = new List<string>();
        List<string> list_Pdfroute = new List<string>();
        List<string> list_ExcelALL = new List<string>();
        List<string> list_ExcelExcept = new List<string>();
        PackingForm Packingform = null;
        SetDateForm SetDateform = null;
        public string WHDate = "";
        public BatchImportGAPForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            routetBox.ReadOnly = true;
        }

        private void LoadRoutebtn_Click(object sender, EventArgs e)
        {
            try
            {
                string defaultfilePath = "";
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                dialog.ShowNewFolderButton = false;
                dialog.Description = "请选择文件路径";
                if (defaultfilePath != "")
                {
                    //设置此次默认目录为上一次选中目录  
                    dialog.SelectedPath = defaultfilePath;
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK || result == DialogResult.Yes)
                {
                    string foldPath = dialog.SelectedPath;
                    routetBox.Text = defaultfilePath = foldPath;
                    string[] files_excel = Directory.GetFiles(routetBox.Text + @"\", "*.xls");
                    string[] files_pdf = Directory.GetFiles(routetBox.Text + @"\", "*.pdf");
                    foreach (string file_excel in files_excel)
                    {
                        list_Excelroute.Add(file_excel);
                    }
                    foreach (string file_pdf in files_pdf)
                    {
                        list_Pdfroute.Add(file_pdf);
                    }
                    //找出无对应PDF数据的选项
                    //list_ExcelExcept = list_ExcelALL.Except(list_Excelroute).ToList();
                    //string strexcept = "";
                    //foreach (string item in list_ExcelExcept)
                    //{
                    //    string str= item.Substring(item.Length-11,7);
                    //    strexcept += str+",";
                    //}
                    //if (!string.IsNullOrEmpty(strexcept))
                    //{
                    //    MessageBox.Show("订单："+ strexcept+"没有相应的标签数据请检查");
                    //}

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void okbtn_Click(object sender, EventArgs e)
        {
            string FileName_="";
            try
            {
                //弹窗设置出货日期
                //if (SetDateform == null || SetDateform.IsDisposed)
                //{
                //    SetDateform = new SetDateForm();
                //    SetDateform.Owner = this;
                //    SetDateform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                //    SetDateform.ShowDialog();

                //}
                //else
                //{
                //    SetDateform.ShowDialog();
                //    SetDateform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                //    SetDateform.BringToFront();
                //}
                //if (string.IsNullOrEmpty(WHDate))
                //{
                //    MessageBox.Show("请重新导入数据");
                //    return;
                //}
                foreach (string FileName_v in list_Excelroute)
                {
                    FileName_ = FileName_v;
                   CsGetData.Batch_import_excel_GAP(FileName_v);
                }
                foreach (string FileName_v in list_Pdfroute)
                {
                    FileName_ = FileName_v;
                    CsGetData.Batch_import_pdf_GAP(FileName_v);
                }
                MessageBox.Show("导入成功");
                Packingform = (PackingForm)this.Owner;
                Packingform.NewPakingShow();
                this.Dispose();
                this.Close();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show("路径:"+ FileName_ + Environment.NewLine + "文件格式或着内容不正确"+ err.ToString());
                //throw new Exception("提示：" + err.Message);
            }
        }
        private void BatchImportGAPForm_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void routetBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
