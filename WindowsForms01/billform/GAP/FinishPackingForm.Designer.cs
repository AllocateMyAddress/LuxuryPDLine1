﻿namespace WindowsForms01
{
    partial class FinishPackingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FinishPackingForm));
            this.FinishdGV = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.取消结束订单ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.PotSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.SelecttSpBtn = new System.Windows.Forms.ToolStripButton();
            this.删除数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.FinishdGV)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FinishdGV
            // 
            this.FinishdGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FinishdGV.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.FinishdGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FinishdGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FinishdGV.ContextMenuStrip = this.contextMenuStrip1;
            this.FinishdGV.Location = new System.Drawing.Point(0, 53);
            this.FinishdGV.Name = "FinishdGV";
            this.FinishdGV.RowTemplate.Height = 27;
            this.FinishdGV.Size = new System.Drawing.Size(972, 415);
            this.FinishdGV.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.取消结束订单ToolStripMenuItem,
            this.删除数据ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(211, 80);
            // 
            // 取消结束订单ToolStripMenuItem
            // 
            this.取消结束订单ToolStripMenuItem.Name = "取消结束订单ToolStripMenuItem";
            this.取消结束订单ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.取消结束订单ToolStripMenuItem.Text = "取消结束订单";
            this.取消结束订单ToolStripMenuItem.Click += new System.EventHandler(this.取消结束订单ToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Silver;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.PotSpTBox,
            this.SelecttSpBtn});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(972, 50);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(69, 47);
            this.toolStripLabel1.Text = "单号(po)";
            this.toolStripLabel1.Click += new System.EventHandler(this.toolStripLabel1_Click);
            // 
            // PotSpTBox
            // 
            this.PotSpTBox.AutoSize = false;
            this.PotSpTBox.Name = "PotSpTBox";
            this.PotSpTBox.Size = new System.Drawing.Size(150, 50);
            this.PotSpTBox.Click += new System.EventHandler(this.PotSpTBox_Click);
            // 
            // SelecttSpBtn
            // 
            this.SelecttSpBtn.AutoSize = false;
            this.SelecttSpBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.SelecttSpBtn.Image = ((System.Drawing.Image)(resources.GetObject("SelecttSpBtn.Image")));
            this.SelecttSpBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SelecttSpBtn.Name = "SelecttSpBtn";
            this.SelecttSpBtn.Size = new System.Drawing.Size(100, 47);
            this.SelecttSpBtn.Text = "查询";
            this.SelecttSpBtn.Click += new System.EventHandler(this.SelecttSpBtn_Click);
            // 
            // 删除数据ToolStripMenuItem
            // 
            this.删除数据ToolStripMenuItem.Name = "删除数据ToolStripMenuItem";
            this.删除数据ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.删除数据ToolStripMenuItem.Text = "删除数据";
            this.删除数据ToolStripMenuItem.Click += new System.EventHandler(this.删除数据ToolStripMenuItem_Click);
            // 
            // FinishPackingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 467);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.FinishdGV);
            this.Name = "FinishPackingForm";
            this.Text = "已完成订单列表";
            this.Load += new System.EventHandler(this.FinishPackingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FinishdGV)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView FinishdGV;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox PotSpTBox;
        private System.Windows.Forms.ToolStripButton SelecttSpBtn;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 取消结束订单ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除数据ToolStripMenuItem;
    }
}