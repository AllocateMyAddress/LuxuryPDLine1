﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class MarkStartForm : Form
    {
        PackingForm Packingform = null;
        DataGridView packingdvg;
        int index;
        public MarkStartForm()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }
        public MarkStartForm(DataGridView packingdvg_,int index_)
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            packingdvg = packingdvg_;
            index = index_;
            string packingtotal_v = "0";
            string finishmarknumber_v = "0";
            packingtotal_v = packingdvg.Rows[index].Cells["packingtotal"].Value.ToString().Trim();
            finishmarknumber_v = packingdvg.Rows[index].Cells["finishmarknumber"].Value.ToString().Trim();
            // DataTable dt=  CsFormShow.GoSqlSelect(string.Format("SELECT  packingtotal FROM dbo.PACKING AS a WHERE  a.guid='{0}'", packingdvg.Rows[index].Cells["guid"].Value.ToString().Trim()));
            if (packingtotal_v=="0"||string.IsNullOrEmpty(packingtotal_v))
            {
                PackingNumtBox.Text = packingdvg.Rows[index].Cells["从"].Value.ToString().Trim();
            }
            else
            {
                PackingNumtBox.Text = (int.Parse(packingdvg.Rows[index].Cells["从"].Value.ToString().Trim()) + int.Parse(packingtotal_v)).ToString();
            }
            if (finishmarknumber_v == "0" || string.IsNullOrEmpty(finishmarknumber_v))
            {
                MarkingNumtBox.Text = packingdvg.Rows[index].Cells["从"].Value.ToString().Trim();
            }
            else
            {
                MarkingNumtBox.Text = (int.Parse(packingdvg.Rows[index].Cells["从"].Value.ToString().Trim()) + int.Parse(finishmarknumber_v)).ToString();
            }
        }

        private void MarkStartForm_Load(object sender, EventArgs e)
        {

        }

        private void okbtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool bl = Chekin.IsInt(this.MarkingNumtBox.Text.ToString());
                //bool b2 = Chekin.IsInt(this.PackingNumtBox.Text.ToString());
                if (!bl)
                {
                    MessageBox.Show("不能为空或者非整数");
                    return;
                }
                int MarkingNum = int.Parse(this.MarkingNumtBox.Text.ToString().Trim());
                //int PackingNum = int.Parse(this.PackingNumtBox.Text.ToString().Trim());
                string packing_guid = packingdvg.Rows[index].Cells["guid"].Value.ToString().Trim();
                //int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.PACKING AS a ,dbo.MarkingLineUp AS b WHERE ISNULL(IsEndMarking,0)<>1 AND a.guid=b.packing_guid AND a.guid='{0}' AND ISNULL(a.finishmarknumber,0)<ISNULL(b.Cartoncount,0)", packing_guid));
                //int count_v1 = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.PACKING AS a ,dbo.MarkingLineUp AS b WHERE ISNULL(IsEndMarking,0)<>1 AND a.guid=b.packing_guid AND a.guid='{0}' AND ISNULL(a.finishmarknumber,0)>ISNULL(b.Cartoncount,0)", packing_guid));
                int startcode = int.Parse(packingdvg.Rows[index].Cells["从"].Value.ToString().Trim());
                int endcode = int.Parse(packingdvg.Rows[index].Cells["到"].Value.ToString().Trim());
               // int packingtotal_v = int.Parse(packingdvg.Rows[index].Cells["packingtotal"].Value.ToString().Trim());
                if (MarkingNum < startcode || MarkingNum > endcode)
                {
                    MessageBox.Show("喷码箱号不在范围内，请重新输入！！");
                    return;
                }
                //if (PackingNum < startcode || PackingNum > endcode)
                //{
                //    MessageBox.Show("装箱箱号不在范围内，请重新输入！！");
                //    return;
                //}
                CsInitialization.SetMarkingStartNum(packing_guid, MarkingNum);
                Packingform = (PackingForm)this.Owner;
                Packingform.NewPakingShow();
                this.Close();
                this.Dispose();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void MarkStartForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
            this.Close();
        }
    }
}
