﻿namespace WindowsForms01
{
    partial class PackingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PackingForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.PotSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.SkutSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.SelecttSpBtn = new System.Windows.Forms.ToolStripButton();
            this.FinishtSpBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.手动执行喷码ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.手动执行码垛ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PackingdGV = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.设置纸箱信息toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.删除数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PackingdGV)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Silver;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.PotSpTBox,
            this.toolStripLabel2,
            this.SkutSpTBox,
            this.SelecttSpBtn,
            this.FinishtSpBtn,
            this.toolStripDropDownButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1040, 48);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(54, 45);
            this.toolStripLabel1.Text = "订单号";
            // 
            // PotSpTBox
            // 
            this.PotSpTBox.AutoSize = false;
            this.PotSpTBox.Name = "PotSpTBox";
            this.PotSpTBox.Size = new System.Drawing.Size(150, 48);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(69, 45);
            this.toolStripLabel2.Text = "SKU号码";
            // 
            // SkutSpTBox
            // 
            this.SkutSpTBox.AutoSize = false;
            this.SkutSpTBox.Name = "SkutSpTBox";
            this.SkutSpTBox.Size = new System.Drawing.Size(150, 48);
            // 
            // SelecttSpBtn
            // 
            this.SelecttSpBtn.AutoSize = false;
            this.SelecttSpBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.SelecttSpBtn.Image = ((System.Drawing.Image)(resources.GetObject("SelecttSpBtn.Image")));
            this.SelecttSpBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SelecttSpBtn.Name = "SelecttSpBtn";
            this.SelecttSpBtn.Size = new System.Drawing.Size(100, 45);
            this.SelecttSpBtn.Text = "查询";
            this.SelecttSpBtn.Click += new System.EventHandler(this.SelecttSpBtn_Click);
            // 
            // FinishtSpBtn
            // 
            this.FinishtSpBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.FinishtSpBtn.Image = ((System.Drawing.Image)(resources.GetObject("FinishtSpBtn.Image")));
            this.FinishtSpBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FinishtSpBtn.Name = "FinishtSpBtn";
            this.FinishtSpBtn.Size = new System.Drawing.Size(88, 45);
            this.FinishtSpBtn.Text = "查询已完成";
            this.FinishtSpBtn.Click += new System.EventHandler(this.FinishtSpBtn_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.手动执行喷码ToolStripMenuItem,
            this.toolStripMenuItem4,
            this.手动执行码垛ToolStripMenuItem});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(113, 45);
            this.toolStripDropDownButton2.Text = "手动执行操作";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(179, 6);
            // 
            // 手动执行喷码ToolStripMenuItem
            // 
            this.手动执行喷码ToolStripMenuItem.Name = "手动执行喷码ToolStripMenuItem";
            this.手动执行喷码ToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.手动执行喷码ToolStripMenuItem.Text = "手动执行喷码";
            this.手动执行喷码ToolStripMenuItem.Click += new System.EventHandler(this.手动执行喷码ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(179, 6);
            // 
            // 手动执行码垛ToolStripMenuItem
            // 
            this.手动执行码垛ToolStripMenuItem.Name = "手动执行码垛ToolStripMenuItem";
            this.手动执行码垛ToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.手动执行码垛ToolStripMenuItem.Text = "手动执行码垛";
            this.手动执行码垛ToolStripMenuItem.Click += new System.EventHandler(this.手动执行码垛ToolStripMenuItem_Click);
            // 
            // PackingdGV
            // 
            this.PackingdGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PackingdGV.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.PackingdGV.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PackingdGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PackingdGV.ContextMenuStrip = this.contextMenuStrip1;
            this.PackingdGV.Location = new System.Drawing.Point(0, 45);
            this.PackingdGV.Name = "PackingdGV";
            this.PackingdGV.ReadOnly = true;
            this.PackingdGV.RowHeadersWidth = 51;
            this.PackingdGV.RowTemplate.Height = 27;
            this.PackingdGV.Size = new System.Drawing.Size(1040, 446);
            this.PackingdGV.TabIndex = 1;
            this.PackingdGV.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.PackingdGV_DataBindingComplete);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.设置纸箱信息toolStripMenuItem2,
            this.toolStripSeparator2,
            this.toolStripMenuItem6,
            this.toolStripSeparator7,
            this.toolStripMenuItem2,
            this.toolStripSeparator4,
            this.toolStripMenuItem1,
            this.toolStripMenuItem5,
            this.删除数据ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(211, 176);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // 设置纸箱信息toolStripMenuItem2
            // 
            this.设置纸箱信息toolStripMenuItem2.Name = "设置纸箱信息toolStripMenuItem2";
            this.设置纸箱信息toolStripMenuItem2.Size = new System.Drawing.Size(210, 24);
            this.设置纸箱信息toolStripMenuItem2.Text = "加入排单";
            this.设置纸箱信息toolStripMenuItem2.Click += new System.EventHandler(this.设置纸箱信息toolStripMenuItem2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(207, 6);
            this.toolStripSeparator2.Visible = false;
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(210, 24);
            this.toolStripMenuItem6.Text = "设置起始箱号";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(207, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(210, 24);
            this.toolStripMenuItem2.Text = "完成订单";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(207, 6);
            // 
            // 删除数据ToolStripMenuItem
            // 
            this.删除数据ToolStripMenuItem.Name = "删除数据ToolStripMenuItem";
            this.删除数据ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.删除数据ToolStripMenuItem.Text = "删除数据";
            this.删除数据ToolStripMenuItem.Click += new System.EventHandler(this.删除数据ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(210, 24);
            this.toolStripMenuItem1.Text = "打印网单标签";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(207, 6);
            // 
            // PackingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 491);
            this.Controls.Add(this.PackingdGV);
            this.Controls.Add(this.toolStrip1);
            this.Name = "PackingForm";
            this.Text = "装箱信息";
            this.Load += new System.EventHandler(this.PackingForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PackingdGV)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.DataGridView PackingdGV;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 删除数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox PotSpTBox;
        private System.Windows.Forms.ToolStripButton SelecttSpBtn;
        private System.Windows.Forms.ToolStripButton FinishtSpBtn;
        private System.Windows.Forms.ToolStripMenuItem 设置纸箱信息toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem 手动执行喷码ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem 手动执行码垛ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox SkutSpTBox;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
    }
}