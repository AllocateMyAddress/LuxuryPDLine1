﻿namespace WindowsForms01
{
    partial class TiaoMaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TiaoMatBox = new System.Windows.Forms.TextBox();
            this.Okbtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "条码值";
            // 
            // TiaoMatBox
            // 
            this.TiaoMatBox.Location = new System.Drawing.Point(82, 47);
            this.TiaoMatBox.Name = "TiaoMatBox";
            this.TiaoMatBox.Size = new System.Drawing.Size(199, 25);
            this.TiaoMatBox.TabIndex = 1;
            // 
            // Okbtn
            // 
            this.Okbtn.Location = new System.Drawing.Point(111, 95);
            this.Okbtn.Name = "Okbtn";
            this.Okbtn.Size = new System.Drawing.Size(106, 49);
            this.Okbtn.TabIndex = 2;
            this.Okbtn.Text = "保存";
            this.Okbtn.UseVisualStyleBackColor = true;
            this.Okbtn.Click += new System.EventHandler(this.Okbtn_Click);
            // 
            // TiaoMaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 156);
            this.Controls.Add(this.Okbtn);
            this.Controls.Add(this.TiaoMatBox);
            this.Controls.Add(this.label1);
            this.Name = "TiaoMaForm";
            this.Text = "修改条码";
            this.Load += new System.EventHandler(this.TiaoMaForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TiaoMatBox;
        private System.Windows.Forms.Button Okbtn;
    }
}