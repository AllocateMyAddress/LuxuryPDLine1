﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class HistoricRcordsForm : Form
    {
        public HistoricRcordsForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.HistoricRcordsdGV, 40, 40, true, Color.LightSteelBlue,this);
            NewShow();
        }

        private void SelecttSpBtn_Click(object sender, EventArgs e)
        {
            NewShow(PotSpTBox.TextBox.Text.ToString().Trim(), SkutSpTBox.Text.ToString().Trim());
        }
        public void NewShow()
        {
            try
            {
                HistoricRcordsdGV.DataSource = null;
                string sqlselect =string.Format("SELECT a.guid, b.订单号码,b.Sku号码,b.从,b.到,Color , a.po_guid,a.LineUp_guid,a.StartTime AS '开始时间' ,a.EndTime AS '结束时间',Total AS '订单总箱数',Yield AS '已装箱数量' FROM dbo.HistoricRcords AS a LEFT JOIN (SELECT*FROM dbo.PACKING  ) AS b ON a.po_guid=b.guid ORDER BY a.EndTime DESC");
                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                if (dt.Rows.Count >0)
                {
                    this.HistoricRcordsdGV.DataSource = dt;
                    this.HistoricRcordsdGV.Columns["guid"].Visible = false;
                    this.HistoricRcordsdGV.Columns["po_guid"].Visible = false;
                    this.HistoricRcordsdGV.Columns["LineUp_guid"].Visible = false;
                    for (int i = 0; i < this.HistoricRcordsdGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.HistoricRcordsdGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        public void NewShow(string po, string sku_)
        {
            try
            {
                HistoricRcordsdGV.DataSource = null;
                string sqlselect = string.Format("SELECT a.guid, b.订单号码,b.Sku号码,b.从,b.到,Color , a.po_guid,a.LineUp_guid,a.StartTime AS '开始时间' ,a.EndTime AS '结束时间',Total AS '订单总箱数',Yield AS '已装箱数量' FROM dbo.HistoricRcords AS a LEFT JOIN (SELECT*FROM dbo.PACKING  ) AS b ON a.po_guid=b.guid WHERE 订单号码 LIKE '%'+ '{0}'+ '%' AND Sku号码 LIKE '%'+'{1}'+'%' ORDER BY a.EndTime DESC", po, sku_);
                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                if (dt.Rows.Count > 0)
                {
                    this.HistoricRcordsdGV.DataSource = dt;
                    this.HistoricRcordsdGV.Columns["guid"].Visible = false;
                    this.HistoricRcordsdGV.Columns["po_guid"].Visible = false;
                    this.HistoricRcordsdGV.Columns["LineUp_guid"].Visible = false;
                    for (int i = 0; i < this.HistoricRcordsdGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.HistoricRcordsdGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void HistoricRcordsForm_Load(object sender, EventArgs e)
        {

        }

        private void 删除数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Delet_click.Delete_TSMenuItem_Click(this, this.HistoricRcordsdGV, "dbo.HistoricRcords");
                NewShow();
                //NewdataGridView_show();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
    }
}
