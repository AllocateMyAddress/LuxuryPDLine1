﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using A.BLL;

namespace WindowsForms01
{
    public partial class PrintForm : Form
    {
        A.BLL.newsContent content1 = new newsContent();
        public PrintForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.PrintDVG, 40, 40, true, Color.LightSteelBlue,this);
            PrintDVG.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            NewFormShow();
        }
        private void PrintForm_Load(object sender, EventArgs e)
        {

        }
        public void NewFormShow()
        {
            try
            {
                PrintDVG.DataSource = null;
                string sqlstr = string.Format("SELECT  ROW_NUMBER()OVER(ORDER BY ID,Entrytime ) AS 序号,  guid, page AS '页码',po AS '订单号码' ,qty AS '数量',number1 AS '条码' FROM dbo.PRINTLineuUp  WHERE DeviceName='{0}'",CSReadPlc.DevName.Trim());
                DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                if (dt.Rows.Count>0)
                {
                    dt.Columns.Add("当前选择", typeof(bool)).SetOrdinal(0);
                    PrintDVG.DataSource = dt;
                    PrintDVG.Columns["guid"].Visible = false;
                    for (int i = 0; i < this.PrintDVG.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.PrintDVG.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }

        }
        public void NewFormShow(string po_,string page_)
        {
            try
            {
                PrintDVG.DataSource = null;
                po_ = po_.ToUpper();
                string sqlstr = string.Format("SELECT  ROW_NUMBER()OVER(ORDER BY Entrytime ,po,page) AS 序号,  guid, page AS '页码',po AS '订单号码',PID ,qty AS '数量',number1 AS '条码'FROM dbo.PRINTLineuUp WHERE page LIKE '%'+'{0}'+'%' AND po LIKE '%'+'{1}'+'%' ",  page_,po_);
                DataTable dt =CsFormShow.GoSqlSelect(sqlstr);
                if (dt.Rows.Count>0)
                {
                    dt.Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                    PrintDVG.DataSource = dt;
                    PrintDVG.Columns["guid"].Visible = false;
                    for (int i = 0; i < this.PrintDVG.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.PrintDVG.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                string po = potSpTBox.TextBox.Text.ToString().Trim();
                string page = pagetSpTBox.TextBox.Text.ToString().Trim();
                NewFormShow(po,page);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }

        }

        private void PrintForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void 置为当前ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (PrintDVG.Rows.Count > 0 && PrintDVG.SelectedRows.Count > 0)
                {
                   //bool boolval= CsFormShow.DvgRowSetcurrent(PrintDVG);
                   //if (!boolval)
                   //     return;
                    if (PrintDVG.SelectedRows.Count==1)
                    {
                        string strsql = string.Format("SELECT *FROM dbo.PRINTLineuUp WHERE guid='{0}'", PrintDVG.SelectedRows[0].Cells["guid"].Value.ToString().Trim());
                        DataSet ds = content1.Select_nothing(strsql);
                        CsPrint.ManualPrintLineuUp(ds.Tables[0].Rows[0]);
                    }
                }
                else
                {
                    MessageBox.Show("提示：没有可执行的数据");
                    return;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Delet_click.Delete_TSMenuItem_Click(this, PrintDVG, "dbo.PRINTLineuUp");
                //刷新打印队列表
                M_stcPrintData.lis_printdata.Clear();//先清除再重新加载
               DataSet ds = content1.Select_nothing("SELECT*FROM PRINTLineuUp ORDER BY Entrytime");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    CsPrint.PrintLineuUp(ds.Tables[0].Rows[i]);
                }
                //NewdataGridView_show();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
    }
}
