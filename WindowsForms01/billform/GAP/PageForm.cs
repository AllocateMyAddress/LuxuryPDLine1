﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class PageForm : Form
    {
        public PdfForm pdfform = null;
        public PageForm()
        {
            InitializeComponent();
            this.PagetBx.Focus();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }

        private void PageForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (PagetBx.Text.ToString().Trim() == "")
                {
                    PagetBx.Text = "0";
                }
                //this.mainrpForm.form_show(Convert.ToInt32(PagetBx.Text.ToString().Trim()));
                pdfform = (PdfForm)this.Owner;
                pdfform.Mainform_show(Convert.ToInt32(PagetBx.Text.ToString().Trim()));//刷新父窗体
                this.Close();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

    }
}
