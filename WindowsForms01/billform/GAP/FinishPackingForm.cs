﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class FinishPackingForm : Form
    {
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        public FinishPackingForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.FinishdGV, 40, 40, true, Color.LightSteelBlue,this);
            NewPakingShow();
        }

        private void FinishPackingForm_Load(object sender, EventArgs e)
        {

        }
        public void NewPakingShow()
        {
            try
            {
                string sqlselect = "SELECT  ROW_NUMBER() OVER (ORDER BY Entrytime,订单号码,从)AS 序号, guid,订单号码,范围,从,到,数量,内部包装的项目数量,内包装计数,箱数,finishmarknumber AS '已完成数量',净重,毛重,长,宽,高,开始序列号,截止序列号,包装代码,Sku号码,Color,扫描ID,外箱代码,Entrytime FROM dbo.PACKING WHERE ISNULL(ENDPO,0)=1 ";
                this.FinishdGV.DataSource = null;
                DataTable dt =CsFormShow.GoSqlSelect(sqlselect);
                //ds1.Tables[0].DefaultView.Sort = "从 ASC";//升序排列
                //DataTable dt1 = ds1.Tables[0].DefaultView.ToTable();//返回一个新的DataTable
                //ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                if (dt.Rows.Count>0)
                {
                    this.FinishdGV.DataSource = dt;
                    // PackingdGV.Columns["当前选择"].ReadOnly = true;
                    this.FinishdGV.Columns["guid"].Visible = false;

                    for (int i = 0; i < this.FinishdGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.FinishdGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }

        }
        public void NewPakingShow(string po)
        {
            try
            {
                this.FinishdGV.DataSource = null;
                string sqlselect = "SELECT  ROW_NUMBER() OVER (ORDER BY Entrytime,订单号码,从)AS 序号, guid,订单号码,范围,从,到,数量,内部包装的项目数量,内包装计数,箱数,finishmarknumber AS '已完成数量',净重,毛重,长,宽,高,开始序列号,截止序列号,包装代码,Sku号码,Color,扫描ID,外箱代码,Entrytime FROM dbo.PACKING WHERE 订单号码 LIKE '%'+'" + po + "'+'%' and ISNULL(ENDPO,0)=1  ";
                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                //ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                if (dt.Rows.Count>0)
                {
                    this.FinishdGV.DataSource = dt;
                    //this.FinishdGV.Columns["当前选择"].ReadOnly = true;
                    this.FinishdGV.Columns["guid"].Visible = false;

                    for (int i = 0; i < this.FinishdGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.FinishdGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }

        }
        private void SelecttSpBtn_Click(object sender, EventArgs e)
        {
            NewPakingShow(PotSpTBox.Text.ToString().Trim());
        }

        private void PotSpTBox_Click(object sender, EventArgs e)
        {

        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void 取消结束订单ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                if (FinishdGV.Rows.Count > 0 && FinishdGV.SelectedRows.Count > 0)
                {

                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(FinishdGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        string selectsql = string.Format("UPDATE dbo.PACKING SET ENDPO=0,packingtotal=0,finishmarknumber=0,finishPalletizingnumber=0,finishprintnumber=0 WHERE guid='{0}'", FinishdGV.Rows[index_v].Cells["guid"].Value.ToString().Trim());
                        content1.Select_nothing(selectsql);
                    }
                }
                else
                {
                    MessageBox.Show("提示：请选择有效订单");
                    return;
                }
                NewPakingShow();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        private void 删除数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Delet_click.Delete_TSMenuItem_Click(this, this.FinishdGV, "dbo.PACKING");
                NewPakingShow();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
    }
}
