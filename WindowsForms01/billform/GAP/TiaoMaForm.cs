﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class TiaoMaForm : Form
    {
        string packing_guid = "";
        PackingForm packingform = null;
        public TiaoMaForm()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }
        public TiaoMaForm(string packing_guid_)
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            packing_guid = packing_guid_;
        }
        private void TiaoMaForm_Load(object sender, EventArgs e)
        {

        }

        private void Okbtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool b_v = Chekin.IsInt(TiaoMatBox.Text.ToString().Trim());
                if (!b_v)
                {
                    MessageBox.Show("输入的数字格式不正确");
                    return;
                }
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET TiaoMaVal='{1}' WHERE guid = '{0}'", packing_guid, TiaoMatBox.Text.ToString().Trim()));
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET TiaoMaVal='{1}' WHERE packing_guid = '{0}'", packing_guid, TiaoMatBox.Text.ToString().Trim()));
                packingform = (PackingForm)this.Owner;
                packingform.NewPakingShow();
                //重新加载扫码队列信息
                CsGetData.UpdataScanLineUp();//更新扫码队列
                CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                MessageBox.Show("修改成功");
                this.Close();
                this.Dispose();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }
    }
}
