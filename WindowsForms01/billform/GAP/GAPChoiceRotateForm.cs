﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class GAPChoiceRotateForm : Form
    {
       // SetQtyform SetQtyform = null;
        SetQtyForm SetQtyform = null;
        //UpdateGAPDataForm UpdateGAPDataform = null;
        string MDIFormName = "";
        public GAPChoiceRotateForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }
        public GAPChoiceRotateForm(Form MDIForm_)
        {
            InitializeComponent();
            MDIFormName = MDIForm_.Name;
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void GAPChoiceRotateForm_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (MDIFormName=="SetQtyForm")
            {
                SetQtyform = (SetQtyForm)this.Owner;
                foreach (Control ctr in SetQtyform.Controls)
                {
                    if (ctr.Name.Trim() == "IsRotatetBox")
                    {
                        ctr.Text = "不旋转";
                    }
                    else if (ctr.Name.Trim() == "ColumntBox")
                    {
                        ctr.Text = "1";
                    }
                    else if (ctr.Name.Trim() == "OrientationcBox")
                    {
                        ctr.Text = "正";
                    }
                }
            }
            this.Close();
            this.Dispose();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (MDIFormName == "SetQtyForm")
            {
                SetQtyform = (SetQtyForm)this.Owner;
                foreach (Control ctr in SetQtyform.Controls)
                {
                    if (ctr.Name.Trim() == "IsRotatetBox")
                    {
                        ctr.Text = "不旋转";
                    }
                    else if (ctr.Name.Trim() == "ColumntBox")
                    {
                        ctr.Text = "2";
                    }
                    else if (ctr.Name.Trim() == "OrientationcBox")
                    {
                        ctr.Text = "正";
                    }
                }
            }
            this.Close();
            this.Dispose();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            if (MDIFormName == "SetQtyForm")
            {
                SetQtyform = (SetQtyForm)this.Owner;
                foreach (Control ctr in SetQtyform.Controls)
                {
                    if (ctr.Name.Trim() == "IsRotatetBox")
                    {
                        ctr.Text = "不旋转";
                    }
                    else if (ctr.Name.Trim() == "ColumntBox")
                    {
                        ctr.Text = "2";
                    }
                    else if (ctr.Name.Trim() == "OrientationcBox")
                    {
                        ctr.Text = "反";
                    }
                }
            }
            this.Close();
            this.Dispose();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (MDIFormName == "SetQtyForm")
            {
                SetQtyform = (SetQtyForm)this.Owner;
                foreach (Control ctr in SetQtyform.Controls)
                {
                    if (ctr.Name.Trim() == "IsRotatetBox")
                    {
                        ctr.Text = "不旋转";
                    }
                    else if (ctr.Name.Trim() == "ColumntBox")
                    {
                        ctr.Text = "3";
                    }
                    else if (ctr.Name.Trim() == "OrientationcBox")
                    {
                        ctr.Text = "反";
                    }
                }
            }
            this.Close();
            this.Dispose();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            if (MDIFormName == "SetQtyForm")
            {
                SetQtyform = (SetQtyForm)this.Owner;
                foreach (Control ctr in SetQtyform.Controls)
                {
                    if (ctr.Name.Trim() == "IsRotatetBox")
                    {
                        ctr.Text = "不旋转";
                    }
                    else if (ctr.Name.Trim() == "ColumntBox")
                    {
                        ctr.Text = "3";
                    }
                    else if (ctr.Name.Trim() == "OrientationcBox")
                    {
                        ctr.Text = "正";
                    }
                }
            }
            this.Close();
            this.Dispose();
        }
    }
}
