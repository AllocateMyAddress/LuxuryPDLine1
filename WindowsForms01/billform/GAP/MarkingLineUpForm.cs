﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class MarkingLineUpForm : Form
    {
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        public MarkingLineUpForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.MarkLineUpdGV, 40, 40, true, Color.LightSteelBlue,this);
            NewformShow();
        }

        private void MarkingLineUpForm_Load(object sender, EventArgs e)
        {

        }
        public void NewformShow()
        {
            try
            {
                this.MarkLineUpdGV.DataSource = null;
                string sqlselect =string.Format("select ROW_NUMBER() OVER (ORDER BY Entrytime) AS 序号,STOP ,guid , packing_guid ,范围,从,到,数量 ,内部包装的项目数量 ,内包装计数 ,箱数 ,净重 ,毛重 ,长 ,宽 高,开始序列号 ,截止序列号 ,包装代码 ,外箱代码 ,订单号码 ,Sku号码 ,Color ,扫描ID ,喷码完成 ,贴标完成,ID,CurrentPackingNum AS '当前装箱数量' ,FinishPackingNum AS '已完成装箱数量' ,BatchNo AS '卷号',Entrytime FROM dbo.MarkingLineUp WHERE DeviceName='{0}'", CSReadPlc.DevName);
                DataTable dt =CsFormShow.GoSqlSelect(sqlselect);
                //ds1.Tables[0].DefaultView.Sort = "从 ASC";//升序排列
                //DataTable dt1 = ds1.Tables[0].DefaultView.ToTable();//返回一个新的DataTable
                //ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                //ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(0);
                if (dt.Rows.Count>0)
                {
                    this.MarkLineUpdGV.DataSource = dt;
                    // PackingdGV.Columns["当前选择"].ReadOnly = true;
                    this.MarkLineUpdGV.Columns["guid"].Visible = false;
                    this.MarkLineUpdGV.Columns["packing_guid"].Visible = false;
                    this.MarkLineUpdGV.Columns["STOP"].Visible = false;
                    //this.MarkLineUpdGV.Columns["喷码完成"].Visible = false;
                    //this.MarkLineUpdGV.Columns["贴标完成"].Visible = false;
                    this.MarkLineUpdGV.Columns["ID"].Visible = false;
                    // this.MarkLineUpdGV.Columns["当前选择"].Visible = false;
                    for (int i = 0; i < this.MarkLineUpdGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.MarkLineUpdGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }

        }

        public void UpdateGAPPakingData()
        {
            try
            {
                DataSet ds = new DataSet();
                    string selectsql = string.Format("SELECT ISNULL(箱数,0)-ISNULL(finishmarknumber,0) as '箱数', Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数',LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '3' THEN '国内' ELSE '国外' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向',外箱代码 FROM dbo.PACKING, dbo.GAPSchedulingTable  where dbo.PACKING.guid = dbo.GAPSchedulingTable.po_guid and  dbo.PACKING.guid in(SELECT packing_guid FROM ((SELECT TOP 1 ROW_NUMBER()OVER(ORDER BY Entrytime DESC) AS 序号, *FROM dbo.MarkingLineUp WHERE  ISNULL(ENDPO, 0) <> 1)) gap)");
                    ds = content1.Select_nothing(selectsql);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                       // MessageBox.Show("当前没有订单数据");
                    }
                    else
                    {
                    string waixiang_code = ds.Tables[0].Rows[0]["外箱代码"].ToString();
                    CsPakingData.M_DicPakingData["箱子长"] = ds.Tables[0].Rows[0]["箱子长"].ToString().Trim();
                        CsPakingData.M_DicPakingData["箱子宽"] = ds.Tables[0].Rows[0]["箱子宽"].ToString().Trim();
                        CsPakingData.M_DicPakingData["箱子高"] = ds.Tables[0].Rows[0]["箱子高"].ToString().Trim();
                        CsPakingData.M_DicPakingData["距离底部"] = ds.Tables[0].Rows[0]["距离底部"].ToString().Trim();
                        CsPakingData.M_DicPakingData["距离左边"] = ds.Tables[0].Rows[0]["距离左边"].ToString().Trim();
                        CsPakingData.M_DicPakingData["重量"] = ds.Tables[0].Rows[0]["重量"].ToString().Trim();
                        CsPakingData.M_DicPakingData["产品重量"] = ds.Tables[0].Rows[0]["产品重量"].ToString().Trim();
                        CsPakingData.M_DicPakingData["列数"] = ds.Tables[0].Rows[0]["列数"].ToString().Trim();
                        CsPakingData.M_DicPakingData["行数"] = ds.Tables[0].Rows[0]["行数"].ToString().Trim();
                        CsPakingData.M_DicPakingData["箱内件数"] = ds.Tables[0].Rows[0]["箱内件数"].ToString().Trim();
                        CsPakingData.M_DicPakingData["箱数"] = ds.Tables[0].Rows[0]["箱数"].ToString().Trim();
                        CsPakingData.M_DicPakingData["产品厚度"] = ds.Tables[0].Rows[0]["产品厚度"].ToString().Trim();
                        //CsPakingData.M_DicPakingData["加减速度"] = ds.Tables[0].Rows[0]["加减速度"].ToString().Trim();
                        //CsPakingData.M_DicPakingData["抓取速度"] = ds.Tables[0].Rows[0]["抓取速度"].ToString().Trim();
                        if (waixiang_code.Contains("C2"))
                        {
                            CsPakingData.M_DicPakingData["喷码模式"] = "5";
                        }
                        else
                        {
                            CsPakingData.M_DicPakingData["喷码模式"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["喷码模式"].ToString().Trim()).ToString();
                        }
                        CsPakingData.M_DicPakingData["纸箱大小"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["是否旋转"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["是否旋转"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["贴标类型"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["贴标类型"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["隔板类型"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["隔板类型"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["正反方向"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["正反方向"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["PoTypeSignal"] = "3";
                        if (CsPakingData.M_DicPakingData["喷码模式"]=="3")
                        {
                           CsMarking.M_MarkParameter = "NONet";
                        }
                        else if (CsPakingData.M_DicPakingData["喷码模式"] == "4")
                        {
                           CsMarking.M_MarkParameter = "Net";
                        }
                    }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 删除ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Delet_click.Delete_TSMenuItem_Click(this, this.MarkLineUpdGV, "dbo.MarkingLineUp");
                //刷新喷码队列表
                CsPakingData.M_list_DicMarkingNet.Clear();//先清除再重新加载
                CsPakingData.M_list_DicMarkingNONet.Clear();//先清除再重新加载
                DataSet ds = content1.Select_nothing("SELECT ROW_NUMBER()OVER (ORDER BY ID,a.Entrytime) AS 序号,GAPSideStartNum,a.*,(SUBSTRING(b.Sku号码,0,7)+'-'+SUBSTRING(b.Sku号码,7,2)+'-'+SUBSTRING(b.Sku号码,9,1)+'-'+SUBSTRING(b.Sku号码,10,5)) AS sku,b.Size FROM dbo.MarkingLineUp AS a INNER JOIN dbo.PACKING AS b ON a.packing_guid=b.guid  WHERE ISNULL(IsEndMarking,0)<>1");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    CsMarking.SetToMarking(ds.Tables[0].Rows[i]);
                }
                NewformShow();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
    }
}
