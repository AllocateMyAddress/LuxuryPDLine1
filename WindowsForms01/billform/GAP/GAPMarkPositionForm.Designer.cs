﻿namespace WindowsForms01
{
    partial class GAPMarkPositionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.JianspeedtBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.GrabspeedtBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ThicktBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.LayernumtBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.ColumntBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.choosebtn = new System.Windows.Forms.Button();
            this.IsRotatetBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.PartitionTypecBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.LabelingTypecbox = new System.Windows.Forms.ComboBox();
            this.CatornTypecBox = new System.Windows.Forms.ComboBox();
            this.MarkPatterncBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Setbtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.weighttBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lefttBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.toptBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.heighttBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.widthtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.longtBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.PoguidtBox = new System.Windows.Forms.TextBox();
            this.qtytBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.GoodWeighttBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.CatornStyle_guidtBox = new System.Windows.Forms.TextBox();
            this.batchSetbtn = new System.Windows.Forms.Button();
            this.OrientationcBox = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.LinetBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // JianspeedtBox
            // 
            this.JianspeedtBox.Location = new System.Drawing.Point(105, 500);
            this.JianspeedtBox.Name = "JianspeedtBox";
            this.JianspeedtBox.Size = new System.Drawing.Size(148, 25);
            this.JianspeedtBox.TabIndex = 89;
            this.JianspeedtBox.TextChanged += new System.EventHandler(this.JianspeedtBox_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(37, 503);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 15);
            this.label15.TabIndex = 88;
            this.label15.Text = "减速度";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // GrabspeedtBox
            // 
            this.GrabspeedtBox.Location = new System.Drawing.Point(333, 500);
            this.GrabspeedtBox.Name = "GrabspeedtBox";
            this.GrabspeedtBox.Size = new System.Drawing.Size(148, 25);
            this.GrabspeedtBox.TabIndex = 87;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(265, 503);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 15);
            this.label16.TabIndex = 86;
            this.label16.Text = "抓取速度";
            // 
            // ThicktBox
            // 
            this.ThicktBox.Location = new System.Drawing.Point(541, 405);
            this.ThicktBox.Name = "ThicktBox";
            this.ThicktBox.Size = new System.Drawing.Size(148, 25);
            this.ThicktBox.TabIndex = 85;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(472, 408);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 15);
            this.label17.TabIndex = 84;
            this.label17.Text = "产品厚";
            // 
            // LayernumtBox
            // 
            this.LayernumtBox.Location = new System.Drawing.Point(91, 338);
            this.LayernumtBox.Name = "LayernumtBox";
            this.LayernumtBox.Size = new System.Drawing.Size(148, 25);
            this.LayernumtBox.TabIndex = 83;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 343);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 15);
            this.label18.TabIndex = 82;
            this.label18.Text = "箱内件数";
            // 
            // ColumntBox
            // 
            this.ColumntBox.Location = new System.Drawing.Point(89, 63);
            this.ColumntBox.Name = "ColumntBox";
            this.ColumntBox.Size = new System.Drawing.Size(146, 25);
            this.ColumntBox.TabIndex = 81;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 66);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 15);
            this.label13.TabIndex = 80;
            this.label13.Text = "摆货列数";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // choosebtn
            // 
            this.choosebtn.Location = new System.Drawing.Point(476, 66);
            this.choosebtn.Name = "choosebtn";
            this.choosebtn.Size = new System.Drawing.Size(145, 39);
            this.choosebtn.TabIndex = 79;
            this.choosebtn.Text = "选旋转和列数";
            this.choosebtn.UseVisualStyleBackColor = true;
            this.choosebtn.Click += new System.EventHandler(this.choosebtn_Click);
            // 
            // IsRotatetBox
            // 
            this.IsRotatetBox.Location = new System.Drawing.Point(314, 66);
            this.IsRotatetBox.Name = "IsRotatetBox";
            this.IsRotatetBox.Size = new System.Drawing.Size(148, 25);
            this.IsRotatetBox.TabIndex = 78;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(242, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 15);
            this.label14.TabIndex = 77;
            this.label14.Text = "纸箱旋转";
            // 
            // PartitionTypecBox
            // 
            this.PartitionTypecBox.FormattingEnabled = true;
            this.PartitionTypecBox.Location = new System.Drawing.Point(541, 340);
            this.PartitionTypecBox.Name = "PartitionTypecBox";
            this.PartitionTypecBox.Size = new System.Drawing.Size(148, 23);
            this.PartitionTypecBox.TabIndex = 76;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(472, 343);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 15);
            this.label12.TabIndex = 75;
            this.label12.Text = "隔板选择";
            // 
            // LabelingTypecbox
            // 
            this.LabelingTypecbox.FormattingEnabled = true;
            this.LabelingTypecbox.Location = new System.Drawing.Point(566, 503);
            this.LabelingTypecbox.Name = "LabelingTypecbox";
            this.LabelingTypecbox.Size = new System.Drawing.Size(148, 23);
            this.LabelingTypecbox.TabIndex = 74;
            // 
            // CatornTypecBox
            // 
            this.CatornTypecBox.FormattingEnabled = true;
            this.CatornTypecBox.Location = new System.Drawing.Point(91, 300);
            this.CatornTypecBox.Name = "CatornTypecBox";
            this.CatornTypecBox.Size = new System.Drawing.Size(148, 23);
            this.CatornTypecBox.TabIndex = 73;
            // 
            // MarkPatterncBox
            // 
            this.MarkPatterncBox.FormattingEnabled = true;
            this.MarkPatterncBox.Location = new System.Drawing.Point(566, 451);
            this.MarkPatterncBox.Name = "MarkPatterncBox";
            this.MarkPatterncBox.Size = new System.Drawing.Size(148, 23);
            this.MarkPatterncBox.TabIndex = 72;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(498, 506);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 71;
            this.label9.Text = "贴标选择";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 303);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 15);
            this.label10.TabIndex = 70;
            this.label10.Text = "纸箱大小";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(493, 454);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 15);
            this.label11.TabIndex = 69;
            this.label11.Text = "喷码模式";
            // 
            // Setbtn
            // 
            this.Setbtn.Location = new System.Drawing.Point(292, 131);
            this.Setbtn.Name = "Setbtn";
            this.Setbtn.Size = new System.Drawing.Size(103, 42);
            this.Setbtn.TabIndex = 68;
            this.Setbtn.Text = "设置";
            this.Setbtn.UseVisualStyleBackColor = true;
            this.Setbtn.Click += new System.EventHandler(this.Setbtn_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 15);
            this.label7.TabIndex = 67;
            this.label7.Text = "单位：mm";
            // 
            // weighttBox
            // 
            this.weighttBox.Location = new System.Drawing.Point(315, 338);
            this.weighttBox.Name = "weighttBox";
            this.weighttBox.Size = new System.Drawing.Size(148, 25);
            this.weighttBox.TabIndex = 66;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(240, 341);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 65;
            this.label4.Text = "纸箱重量";
            // 
            // lefttBox
            // 
            this.lefttBox.Location = new System.Drawing.Point(541, 298);
            this.lefttBox.Name = "lefttBox";
            this.lefttBox.Size = new System.Drawing.Size(148, 25);
            this.lefttBox.TabIndex = 64;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(468, 301);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 63;
            this.label5.Text = "距离左边";
            // 
            // toptBox
            // 
            this.toptBox.Location = new System.Drawing.Point(316, 298);
            this.toptBox.Name = "toptBox";
            this.toptBox.Size = new System.Drawing.Size(148, 25);
            this.toptBox.TabIndex = 62;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(245, 301);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 15);
            this.label6.TabIndex = 61;
            this.label6.Text = "距离底部";
            // 
            // heighttBox
            // 
            this.heighttBox.Location = new System.Drawing.Point(541, 16);
            this.heighttBox.Name = "heighttBox";
            this.heighttBox.Size = new System.Drawing.Size(148, 25);
            this.heighttBox.TabIndex = 60;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(466, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 15);
            this.label3.TabIndex = 59;
            this.label3.Text = "箱子高";
            // 
            // widthtBox
            // 
            this.widthtBox.Location = new System.Drawing.Point(313, 16);
            this.widthtBox.Name = "widthtBox";
            this.widthtBox.Size = new System.Drawing.Size(148, 25);
            this.widthtBox.TabIndex = 58;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(241, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 15);
            this.label2.TabIndex = 57;
            this.label2.Text = "箱子宽";
            // 
            // longtBox
            // 
            this.longtBox.Location = new System.Drawing.Point(88, 16);
            this.longtBox.Name = "longtBox";
            this.longtBox.Size = new System.Drawing.Size(148, 25);
            this.longtBox.TabIndex = 56;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 55;
            this.label1.Text = "箱子长";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(289, 454);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 15);
            this.label8.TabIndex = 91;
            this.label8.Text = "guid";
            this.label8.Visible = false;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // PoguidtBox
            // 
            this.PoguidtBox.Location = new System.Drawing.Point(347, 451);
            this.PoguidtBox.Name = "PoguidtBox";
            this.PoguidtBox.Size = new System.Drawing.Size(148, 25);
            this.PoguidtBox.TabIndex = 90;
            this.PoguidtBox.Visible = false;
            this.PoguidtBox.TextChanged += new System.EventHandler(this.PoguidtBox_TextChanged);
            // 
            // qtytBox
            // 
            this.qtytBox.Location = new System.Drawing.Point(315, 405);
            this.qtytBox.Name = "qtytBox";
            this.qtytBox.Size = new System.Drawing.Size(148, 25);
            this.qtytBox.TabIndex = 93;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(272, 408);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 15);
            this.label19.TabIndex = 92;
            this.label19.Text = "箱数";
            // 
            // GoodWeighttBox
            // 
            this.GoodWeighttBox.Location = new System.Drawing.Point(541, 369);
            this.GoodWeighttBox.Name = "GoodWeighttBox";
            this.GoodWeighttBox.Size = new System.Drawing.Size(148, 25);
            this.GoodWeighttBox.TabIndex = 95;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(466, 372);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 15);
            this.label20.TabIndex = 94;
            this.label20.Text = "产品重量";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(23, 433);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(135, 15);
            this.label21.TabIndex = 97;
            this.label21.Text = "CatornStyle_guid";
            this.label21.Visible = false;
            // 
            // CatornStyle_guidtBox
            // 
            this.CatornStyle_guidtBox.Location = new System.Drawing.Point(26, 451);
            this.CatornStyle_guidtBox.Name = "CatornStyle_guidtBox";
            this.CatornStyle_guidtBox.Size = new System.Drawing.Size(148, 25);
            this.CatornStyle_guidtBox.TabIndex = 96;
            this.CatornStyle_guidtBox.Visible = false;
            // 
            // batchSetbtn
            // 
            this.batchSetbtn.Location = new System.Drawing.Point(401, 131);
            this.batchSetbtn.Name = "batchSetbtn";
            this.batchSetbtn.Size = new System.Drawing.Size(103, 42);
            this.batchSetbtn.TabIndex = 98;
            this.batchSetbtn.Text = "批量设置";
            this.batchSetbtn.UseVisualStyleBackColor = true;
            this.batchSetbtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // OrientationcBox
            // 
            this.OrientationcBox.FormattingEnabled = true;
            this.OrientationcBox.Location = new System.Drawing.Point(91, 369);
            this.OrientationcBox.Name = "OrientationcBox";
            this.OrientationcBox.Size = new System.Drawing.Size(148, 23);
            this.OrientationcBox.TabIndex = 142;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(18, 375);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(67, 15);
            this.label23.TabIndex = 141;
            this.label23.Text = "正反方向";
            // 
            // LinetBox
            // 
            this.LinetBox.Location = new System.Drawing.Point(91, 405);
            this.LinetBox.Name = "LinetBox";
            this.LinetBox.Size = new System.Drawing.Size(148, 25);
            this.LinetBox.TabIndex = 140;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(21, 408);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 15);
            this.label22.TabIndex = 139;
            this.label22.Text = "摆货行数";
            // 
            // GAPMarkPositionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 206);
            this.Controls.Add(this.OrientationcBox);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.LinetBox);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.batchSetbtn);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.CatornStyle_guidtBox);
            this.Controls.Add(this.GoodWeighttBox);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.qtytBox);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.PoguidtBox);
            this.Controls.Add(this.JianspeedtBox);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.GrabspeedtBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.ThicktBox);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.LayernumtBox);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.ColumntBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.choosebtn);
            this.Controls.Add(this.IsRotatetBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.PartitionTypecBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.LabelingTypecbox);
            this.Controls.Add(this.CatornTypecBox);
            this.Controls.Add(this.MarkPatterncBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Setbtn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.weighttBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lefttBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.toptBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.heighttBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.widthtBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.longtBox);
            this.Controls.Add(this.label1);
            this.Name = "GAPMarkPositionForm";
            this.Text = "当前订单纸箱信息";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GAPMarkPositionForm_FormClosed);
            this.Load += new System.EventHandler(this.GAPMarkPositionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox JianspeedtBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox GrabspeedtBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox ThicktBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox LayernumtBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox ColumntBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button choosebtn;
        private System.Windows.Forms.TextBox IsRotatetBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox PartitionTypecBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox LabelingTypecbox;
        private System.Windows.Forms.ComboBox CatornTypecBox;
        private System.Windows.Forms.ComboBox MarkPatterncBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button Setbtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox weighttBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox lefttBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox toptBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox heighttBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox widthtBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox longtBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox PoguidtBox;
        private System.Windows.Forms.TextBox qtytBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox GoodWeighttBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox CatornStyle_guidtBox;
        private System.Windows.Forms.Button batchSetbtn;
        private System.Windows.Forms.ComboBox OrientationcBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox LinetBox;
        private System.Windows.Forms.Label label22;
    }
}