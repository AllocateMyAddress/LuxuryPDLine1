﻿namespace WindowsForms01
{
    partial class MarkStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.MarkingNumtBox = new System.Windows.Forms.TextBox();
            this.okbtn = new System.Windows.Forms.Button();
            this.PackingNumtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "起始箱号:";
            // 
            // MarkingNumtBox
            // 
            this.MarkingNumtBox.Location = new System.Drawing.Point(110, 45);
            this.MarkingNumtBox.Name = "MarkingNumtBox";
            this.MarkingNumtBox.Size = new System.Drawing.Size(142, 25);
            this.MarkingNumtBox.TabIndex = 1;
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(123, 112);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(102, 44);
            this.okbtn.TabIndex = 2;
            this.okbtn.Text = "确定";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.okbtn_Click);
            // 
            // PackingNumtBox
            // 
            this.PackingNumtBox.Location = new System.Drawing.Point(167, 271);
            this.PackingNumtBox.Name = "PackingNumtBox";
            this.PackingNumtBox.Size = new System.Drawing.Size(142, 25);
            this.PackingNumtBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 274);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "起始装箱箱号:";
            // 
            // MarkStartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 183);
            this.Controls.Add(this.PackingNumtBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.okbtn);
            this.Controls.Add(this.MarkingNumtBox);
            this.Controls.Add(this.label1);
            this.Name = "MarkStartForm";
            this.Text = "设置喷码起始箱号";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MarkStartForm_FormClosed);
            this.Load += new System.EventHandler(this.MarkStartForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox MarkingNumtBox;
        private System.Windows.Forms.Button okbtn;
        private System.Windows.Forms.TextBox PackingNumtBox;
        private System.Windows.Forms.Label label2;
    }
}