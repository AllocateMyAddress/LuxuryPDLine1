﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using A.BLL;

namespace WindowsForms01
{
    public partial class PdfForm : Form
    {
        public PageForm pageform = null;
        OpenFileDialog ofd = new OpenFileDialog();
        A.BLL.newsContent content1 = new newsContent();
        BatchImportGAPPDFForm BatchImportGAPPDFform = null;
        public PdfForm()
        {
            InitializeComponent();
            NewPdfShow();
            TypetSpCbBox.ComboBox.Items.Add("全部");
            TypetSpCbBox.ComboBox.Items.Add("已打印");
            TypetSpCbBox.ComboBox.Items.Add("未打印");
            TypetSpCbBox.ComboBox.SelectedIndex = 0;
            TypetSpCbBox.ComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            CsFormShow.SetDvgRowHeight(this.dataGridView1, 40, 40, true, Color.LightSteelBlue,this);
        }

        private void 导入PDF数据FToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ofd.Filter = "PDF文档(*.pdf)|*.pdf";
            try
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    Mainform_show(0);
                    //CsFormShow.FormShowDialog(pageform, this, "WindowsForms01.PageForm");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        #region 主窗体刷新显示
        public void Mainform_show(int page)
        {
            try
            {
                DataTable M_dt = new DataTable();
                M_dt = PDFParser.PDFParser.OnCreated(ofd.FileName, 0);
                //数据存数据库
                for (int i = 0; i < M_dt.Rows.Count; i++)
                {
                    string sqlselect = "SELECT*FROM pdfdata WHERE number=" + "'" + M_dt.Rows[i]["number"].ToString() + "'";
                    DataSet ds = new DataSet();
                    ds = content1.Select_nothing(sqlselect);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //MessageBox.Show("已存在此条码的记录");
                        continue;
                    }
                    else
                    {
                        string sqlpdf = string.Format("insert into PDFDATA (page,StyleProgram,size,number,po,style,qty,NO1,NO2,shipfrom1,shipfrom2,shipfrom3,shipfrom4,shipto1,shipto2,shipto3,shipto4,number1,Entrytime) values ({0},'{1}','{2}','{3}','{4}','{5}',{6},'{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}')",
                        M_dt.Rows[i]["page"], M_dt.Rows[i]["StyleProgram"], M_dt.Rows[i]["size"], M_dt.Rows[i]["number"], M_dt.Rows[i]["po"], M_dt.Rows[i]["style"], M_dt.Rows[i]["qty"], M_dt.Rows[i]["NO1"], M_dt.Rows[i]["NO2"], M_dt.Rows[i]["shipfrom1"], M_dt.Rows[i]["shipfrom2"], M_dt.Rows[i]["shipfrom3"], M_dt.Rows[i]["shipfrom4"], M_dt.Rows[i]["shipto1"], M_dt.Rows[i]["shipto2"], M_dt.Rows[i]["shipto3"], M_dt.Rows[i]["shipto4"], M_dt.Rows[i]["number1"].ToString().Replace(" ",""),DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString());
                        content1.Select_nothing(sqlpdf);
                    }
                }
                NewPdfShow();
                Cslogfun.WriteOperationlog("操作员：" + stcUserData.M_username + " ，执行了操作：" + "导入一次‘" + this.Text + "’数据");
                //将当前页的pdf数据写入PLC寄存器
                //for (int i = 0; i < M_dt.Columns.Count; i++)
                //{
                //    string ColumnName = M_dt.Columns[i].ColumnName;
                //    string val = M_dt.Rows[0][i].ToString();
                //    //csGetBao1.WriteToPlc(ColumnName, val,Devicename);
                //}

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        #endregion


        private void PdfForm_Load(object sender, EventArgs e)
        {

        }

        private void 置为当前ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                {
                    string po = dataGridView1.SelectedRows[i].Cells["po"].Value.ToString().Trim();
                    int count_pdf = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE 订单号码='{0}'", po));
                    if (count_pdf>0)
                    {
                        MessageBox.Show("订单"+ po + "在生产不允许现在删除，请先结束订单");
                        return;
                    }
                }
                Delet_click.Delete_TSMenuItem_Click(this, dataGridView1, "dbo.PDFDATA");
                //NewdataGridView_show();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
           // CsFormShow.DvgRowSetcurrent(dataGridView1);
            //CsPrint.SetToPrint(M_dt);
        }
        public void NewPdfShow()
        {
            try
            {
                dataGridView1.DataSource = null;
                string sqlselect = "SELECT ROW_NUMBER() OVER (ORDER BY Entrytime,po,page) as 序号,guid,page AS '箱号' ,number1 AS 条码, po AS '订单单号',NO1,NO2,shipto1+shipto2+shipto3 AS shipto,style,size,qty,Entrytime AS '录入时间',PrintCount AS '打印次数' FROM dbo.PDFDATA  ORDER BY page";
                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                //ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                if (dt.Rows.Count>0)
                {
                    this.dataGridView1.DataSource = dt;
                    //dataGridView1.Columns["当前选择"].ReadOnly = true;
                    dataGridView1.Columns["guid"].Visible = false;
                    //dataGridView1.Columns["当前选择"].Visible = false;
                    for (int i = 0; i < this.dataGridView1.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.dataGridView1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        public void NewPdfShow(string po,string page_,string Type_)
        {
            try
            {
                po = po.ToUpper();
                string sqlselect = "";
                if (Type_.Contains("全部"))
                {
                    sqlselect = string.Format("SELECT ROW_NUMBER() OVER (ORDER BY Entrytime,po,page) as 序号,guid,page AS '箱号' ,number1 AS 条码, po AS '订单单号',NO1,NO2,shipto1+shipto2+shipto3 AS shipto,style,size,qty,Entrytime AS '录入时间',PrintCount AS '打印次数' FROM dbo.PDFDATA WHERE po LIKE '%'+'{0}'+'%' AND page LIKE '%'+'{1}'+'%' ORDER BY page", po, page_);
                }
                else if (Type_.Contains("已打印"))
                {
                    sqlselect = string.Format("SELECT ROW_NUMBER() OVER (ORDER BY Entrytime,po,page) as 序号,guid,page AS '箱号' ,number1 AS 条码, po AS '订单单号',NO1,NO2,shipto1+shipto2+shipto3 AS shipto,style,size,qty,Entrytime AS '录入时间',PrintCount AS '打印次数'  FROM dbo.PDFDATA WHERE po LIKE '%'+'{0}'+'%' AND page LIKE '%'+'{1}'+'%' AND ISNULL(PrintCount,0)<>0 ORDER BY page", po, page_);
                }
                else if (Type_.Contains("未打印"))
                {
                    sqlselect = string.Format("SELECT ROW_NUMBER() OVER (ORDER BY Entrytime,po,page) as 序号,guid,page AS '箱号' ,number1 AS 条码, po AS '订单单号',NO1,NO2,shipto1+shipto2+shipto3 AS shipto,style,size,qty,Entrytime AS '录入时间',PrintCount AS '打印次数'  FROM dbo.PDFDATA WHERE po LIKE '%'+'{0}'+'%' AND page LIKE '%'+'{1}'+'%' AND ISNULL(PrintCount,0)=0 ORDER BY page", po, page_);
                }
                dataGridView1.DataSource = null;
                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                if (dt.Rows.Count>0)
                {
                   // dt.Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                    this.dataGridView1.DataSource =dt;
                   // dataGridView1.Columns["当前选择"].ReadOnly = true;
                    dataGridView1.Columns["guid"].Visible = false;
                   // dataGridView1.Columns["当前选择"].Visible = false;
                    for (int i = 0; i < this.dataGridView1.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.dataGridView1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
           NewPdfShow(PotSpTBox.TextBox.Text.ToString().Trim(), pagetSpTBox.Text.ToString().Trim(),this.TypetSpCbBox.Text.ToString().Trim());
        }

        private void 打印一次ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool isindex_v = false;int index = 0;
                CsFormShow.DvgGetcurrentIndex(this.dataGridView1, out isindex_v, out index);
                if (isindex_v)
                {
                     string guid_v = dataGridView1.Rows[index].Cells["guid"].Value.ToString().Trim();
                    DataTable dt_ = CsFormShow.GoSqlSelect(string.Format(" SELECT*FROM dbo.PDFDATA WHERE guid='{0}'", guid_v));
                     //M_stcPrintData.DicManualPrint["Poguid"] = dt_.Rows[0]["Poguid"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["Pdf_guid"] = dt_.Rows[0]["guid"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["page"] = dt_.Rows[0]["page"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["StyleProgram"] = dt_.Rows[0]["StyleProgram"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["size"] = dt_.Rows[0]["size"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["number"] = dt_.Rows[0]["number"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["po"] = dt_.Rows[0]["po"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["style"] = dt_.Rows[0]["style"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["qty"] = dt_.Rows[0]["qty"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["NO1"] = dt_.Rows[0]["NO1"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["NO2"] = dt_.Rows[0]["NO2"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["shipfrom1"] = dt_.Rows[0]["shipfrom1"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["shipfrom2"] = dt_.Rows[0]["shipfrom2"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["shipfrom3"] = dt_.Rows[0]["shipfrom3"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["shipfrom4"] = dt_.Rows[0]["shipfrom4"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["shipto1"] = dt_.Rows[0]["shipto1"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["shipto2"] = dt_.Rows[0]["shipto2"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["shipto3"] = dt_.Rows[0]["shipto3"].ToString().Trim();
                    M_stcPrintData.DicManualPrint["shipto4"] = dt_.Rows[0]["shipto4"].ToString().Trim();
                    M_stcPrintData.DicManualPrint["number1"] = dt_.Rows[0]["number1"].ToString().Trim();
                     M_stcPrintData.DicManualPrint["number2"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(0, 15);
                     M_stcPrintData.DicManualPrint["number3"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(15, 4);
                     M_stcPrintData.DicManualPrint["number4"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(19, 1);
                     M_stcPrintData.ManualFlag = "start";
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PDFDATA SET PrintCount=ISNULL(PrintCount,0)+1 WHERE po='{0}' AND page='{1}'", dt_.Rows[0]["po"].ToString().Trim(), dt_.Rows[0]["page"].ToString().Trim()));
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 批量导入ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(BatchImportGAPPDFform,this,"WindowsForms01.BatchImportGAPPDFForm");
        }
    }
}
