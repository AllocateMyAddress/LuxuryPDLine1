﻿namespace WindowsForms01
{
    partial class MarkingLineUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MarkLineUpdGV = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.暂停tSpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.重新开始tSpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.MarkLineUpdGV)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MarkLineUpdGV
            // 
            this.MarkLineUpdGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MarkLineUpdGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MarkLineUpdGV.Location = new System.Drawing.Point(0, 0);
            this.MarkLineUpdGV.Name = "MarkLineUpdGV";
            this.MarkLineUpdGV.RowTemplate.Height = 27;
            this.MarkLineUpdGV.Size = new System.Drawing.Size(800, 450);
            this.MarkLineUpdGV.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.暂停tSpMenuItem,
            this.重新开始tSpMenuItem,
            this.删除ToolStripMenuItem,
            this.删除ToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(139, 100);
            // 
            // 暂停tSpMenuItem
            // 
            this.暂停tSpMenuItem.Name = "暂停tSpMenuItem";
            this.暂停tSpMenuItem.Size = new System.Drawing.Size(138, 24);
            this.暂停tSpMenuItem.Text = "暂停";
            this.暂停tSpMenuItem.Visible = false;
            // 
            // 重新开始tSpMenuItem
            // 
            this.重新开始tSpMenuItem.Name = "重新开始tSpMenuItem";
            this.重新开始tSpMenuItem.Size = new System.Drawing.Size(138, 24);
            this.重新开始tSpMenuItem.Text = "重新开始";
            this.重新开始tSpMenuItem.Visible = false;
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.删除ToolStripMenuItem.Text = "终止";
            this.删除ToolStripMenuItem.Visible = false;
            // 
            // 删除ToolStripMenuItem1
            // 
            this.删除ToolStripMenuItem1.Name = "删除ToolStripMenuItem1";
            this.删除ToolStripMenuItem1.Size = new System.Drawing.Size(138, 24);
            this.删除ToolStripMenuItem1.Text = "删除";
            this.删除ToolStripMenuItem1.Visible = false;
            this.删除ToolStripMenuItem1.Click += new System.EventHandler(this.删除ToolStripMenuItem1_Click);
            // 
            // MarkingLineUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.MarkLineUpdGV);
            this.Name = "MarkingLineUpForm";
            this.Text = "GAP订单列队表";
            this.Load += new System.EventHandler(this.MarkingLineUpForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MarkLineUpdGV)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView MarkLineUpdGV;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 暂停tSpMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 重新开始tSpMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem1;
    }
}