﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class MarkingDateForm : Form
    {
        public MarkingDateForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void okbtn_Click(object sender, EventArgs e)
        {
            M_MarkingData.M_markingData = MarkingTimePic.Text.ToString();
            MessageBox.Show("修改成功");
        }

        private void cancelbttn_Click(object sender, EventArgs e)
        {
            M_MarkingData.M_markingData = "";
            MessageBox.Show("修改成功");
        }

        private void MarkingDateForm_Load(object sender, EventArgs e)
        {

        }
    }
}
