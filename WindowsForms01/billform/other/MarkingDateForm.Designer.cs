﻿namespace WindowsForms01
{
    partial class MarkingDateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MarkingTimePic = new System.Windows.Forms.DateTimePicker();
            this.okbtn = new System.Windows.Forms.Button();
            this.cancelbttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MarkingTimePic
            // 
            this.MarkingTimePic.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MarkingTimePic.Location = new System.Drawing.Point(21, 44);
            this.MarkingTimePic.Name = "MarkingTimePic";
            this.MarkingTimePic.Size = new System.Drawing.Size(200, 30);
            this.MarkingTimePic.TabIndex = 0;
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(243, 40);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(115, 45);
            this.okbtn.TabIndex = 1;
            this.okbtn.Text = "设置日期";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.okbtn_Click);
            // 
            // cancelbttn
            // 
            this.cancelbttn.Location = new System.Drawing.Point(376, 40);
            this.cancelbttn.Name = "cancelbttn";
            this.cancelbttn.Size = new System.Drawing.Size(115, 45);
            this.cancelbttn.TabIndex = 2;
            this.cancelbttn.Text = "取消设置";
            this.cancelbttn.UseVisualStyleBackColor = true;
            this.cancelbttn.Click += new System.EventHandler(this.cancelbttn_Click);
            // 
            // MarkingDateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 127);
            this.Controls.Add(this.cancelbttn);
            this.Controls.Add(this.okbtn);
            this.Controls.Add(this.MarkingTimePic);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MarkingDateForm";
            this.Text = "喷码日期输入";
            this.Load += new System.EventHandler(this.MarkingDateForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker MarkingTimePic;
        private System.Windows.Forms.Button okbtn;
        private System.Windows.Forms.Button cancelbttn;
    }
}