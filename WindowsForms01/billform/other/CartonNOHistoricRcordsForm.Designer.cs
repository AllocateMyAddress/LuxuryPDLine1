﻿namespace WindowsForms01
{
    partial class CartonNOHistoricRcordsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CartonNoHistoricRcordsdGV = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.删除数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.endtimetBox = new System.Windows.Forms.TextBox();
            this.starttimetBox = new System.Windows.Forms.TextBox();
            this.okbtn = new System.Windows.Forms.Button();
            this.EnddTPk = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.StartdTPk = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.CartonNotBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PotBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CartonNoHistoricRcordsdGV)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CartonNoHistoricRcordsdGV
            // 
            this.CartonNoHistoricRcordsdGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CartonNoHistoricRcordsdGV.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.CartonNoHistoricRcordsdGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CartonNoHistoricRcordsdGV.ContextMenuStrip = this.contextMenuStrip1;
            this.CartonNoHistoricRcordsdGV.Location = new System.Drawing.Point(2, 97);
            this.CartonNoHistoricRcordsdGV.Name = "CartonNoHistoricRcordsdGV";
            this.CartonNoHistoricRcordsdGV.ReadOnly = true;
            this.CartonNoHistoricRcordsdGV.RowTemplate.Height = 27;
            this.CartonNoHistoricRcordsdGV.Size = new System.Drawing.Size(1435, 444);
            this.CartonNoHistoricRcordsdGV.TabIndex = 5;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除数据ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(139, 28);
            // 
            // 删除数据ToolStripMenuItem
            // 
            this.删除数据ToolStripMenuItem.Name = "删除数据ToolStripMenuItem";
            this.删除数据ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.删除数据ToolStripMenuItem.Text = "删除数据";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.endtimetBox);
            this.panel1.Controls.Add(this.starttimetBox);
            this.panel1.Controls.Add(this.okbtn);
            this.panel1.Controls.Add(this.EnddTPk);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.StartdTPk);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.CartonNotBox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.PotBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1435, 89);
            this.panel1.TabIndex = 6;
            // 
            // endtimetBox
            // 
            this.endtimetBox.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.endtimetBox.Location = new System.Drawing.Point(1067, 32);
            this.endtimetBox.Multiline = true;
            this.endtimetBox.Name = "endtimetBox";
            this.endtimetBox.Size = new System.Drawing.Size(182, 35);
            this.endtimetBox.TabIndex = 10;
            // 
            // starttimetBox
            // 
            this.starttimetBox.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.starttimetBox.Location = new System.Drawing.Point(719, 31);
            this.starttimetBox.Multiline = true;
            this.starttimetBox.Name = "starttimetBox";
            this.starttimetBox.Size = new System.Drawing.Size(183, 35);
            this.starttimetBox.TabIndex = 9;
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(1287, 31);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(106, 39);
            this.okbtn.TabIndex = 8;
            this.okbtn.Text = "查询";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.okbtn_Click);
            // 
            // EnddTPk
            // 
            this.EnddTPk.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.EnddTPk.Location = new System.Drawing.Point(1067, 32);
            this.EnddTPk.Name = "EnddTPk";
            this.EnddTPk.Size = new System.Drawing.Size(200, 34);
            this.EnddTPk.TabIndex = 7;
            this.EnddTPk.ValueChanged += new System.EventHandler(this.EnddTPk_ValueChanged);
            this.EnddTPk.MouseEnter += new System.EventHandler(this.EnddTPk_MouseEnter);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(946, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 37);
            this.label4.TabIndex = 6;
            this.label4.Text = "结束时间";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StartdTPk
            // 
            this.StartdTPk.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.StartdTPk.Location = new System.Drawing.Point(719, 32);
            this.StartdTPk.Name = "StartdTPk";
            this.StartdTPk.Size = new System.Drawing.Size(200, 34);
            this.StartdTPk.TabIndex = 5;
            this.StartdTPk.ValueChanged += new System.EventHandler(this.StartdTPk_ValueChanged);
            this.StartdTPk.MouseDown += new System.Windows.Forms.MouseEventHandler(this.StartdTPk_MouseDown);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(598, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 37);
            this.label3.TabIndex = 4;
            this.label3.Text = "起始时间";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CartonNotBox
            // 
            this.CartonNotBox.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CartonNotBox.Location = new System.Drawing.Point(438, 29);
            this.CartonNotBox.Multiline = true;
            this.CartonNotBox.Name = "CartonNotBox";
            this.CartonNotBox.Size = new System.Drawing.Size(127, 36);
            this.CartonNotBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(332, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 37);
            this.label2.TabIndex = 2;
            this.label2.Text = "箱 号";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PotBox
            // 
            this.PotBox.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PotBox.Location = new System.Drawing.Point(98, 29);
            this.PotBox.Multiline = true;
            this.PotBox.Name = "PotBox";
            this.PotBox.Size = new System.Drawing.Size(248, 36);
            this.PotBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(11, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "单 号";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CartonNOHistoricRcordsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1438, 543);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.CartonNoHistoricRcordsdGV);
            this.Name = "CartonNOHistoricRcordsForm";
            this.Text = "装箱记录查询";
            this.Load += new System.EventHandler(this.CartonNOHistoricRcordsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CartonNoHistoricRcordsdGV)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView CartonNoHistoricRcordsdGV;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 删除数据ToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button okbtn;
        private System.Windows.Forms.DateTimePicker EnddTPk;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker StartdTPk;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CartonNotBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PotBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox endtimetBox;
        private System.Windows.Forms.TextBox starttimetBox;
    }
}