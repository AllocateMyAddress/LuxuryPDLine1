﻿
namespace WindowsForms01
{
    partial class Qty_per_setForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.qtytBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.okbtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // qtytBox
            // 
            this.qtytBox.Location = new System.Drawing.Point(84, 71);
            this.qtytBox.Name = "qtytBox";
            this.qtytBox.Size = new System.Drawing.Size(139, 25);
            this.qtytBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "件数:";
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(95, 122);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(94, 37);
            this.okbtn.TabIndex = 2;
            this.okbtn.Text = "确定";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.okbtn_Click);
            // 
            // Qty_per_setForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 181);
            this.Controls.Add(this.okbtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.qtytBox);
            this.Name = "Qty_per_setForm";
            this.Text = "输入箱内件数";
            this.Load += new System.EventHandler(this.Qty_per_setForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox qtytBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button okbtn;
    }
}