﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsForms01
{
    public partial class StockForm : Form
    {
        public StockForm()
        {
            InitializeComponent();
            this.IsValidcbBx.Items.Add("可用库存>0");
            this.IsValidcbBx.Items.Add("可用库存<=0");
            this.IsValidcbBx.SelectedIndex = 0;
            IsValidcbBx.DropDownStyle = ComboBoxStyle.DropDownList;
            NewShow();
        }

        private void StockForm_Load(object sender, EventArgs e)
        {

        }
        public void NewShow()
        {
            try
            {
                this.StockdGV.DataSource = null;
                DataTable dt = CsFormShow.GoSqlSelect("SELECT GUID,品牌,品番,色号,颜色名称,卷号,缸号,国家代码,国家名称,库存数量,已用库存,SKU,条形码,CASE when ISNULL(是否有效,0)IN(0,'') THEN '有效' ELSE '无效' END AS '是否有效',录入时间 FROM dbo.ERPEXCH where  isnull(库存数量,0)-isnull(已用库存,0)>0");
                if (dt.Rows.Count>0)
                {
                    this.StockdGV.DataSource = dt;
                    this. StockdGV.Columns["guid"].Visible = false;
                    this.StockdGV.Columns["卷号"].Visible = false;
                    this.StockdGV.Columns["缸号"].Visible = false;
                    CsFormShow.SetDvgRowHeight(this.StockdGV, 40, 40, true, Color.LightSteelBlue, this);
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
        public void NewShow(string TiaoMa_,string IsValid_)
        {
            try
            {
                this.StockdGV.DataSource = null;
                DataTable dt = new DataTable();
                if (IsValid_.Contains("可用库存>0"))
                {
                    dt = CsFormShow.GoSqlSelect(string.Format("SELECT GUID,品牌,品番,色号,颜色名称,卷号,缸号,国家代码,国家名称,库存数量,已用库存,SKU,条形码,CASE when ISNULL(是否有效,0)IN(0,'') THEN '有效' ELSE '无效' END AS '是否有效',录入时间 FROM dbo.ERPEXCH WHERE  isnull(库存数量,0)-isnull(已用库存,0)>0 AND  条形码 LIKE '%'+'{0}'+'%'",TiaoMa_));
                }
                else
                {
                    dt = CsFormShow.GoSqlSelect(string.Format("SELECT GUID,品牌,品番,色号,颜色名称,卷号,缸号,国家代码,国家名称,库存数量,已用库存,SKU,条形码,CASE when ISNULL(是否有效,0)IN(0,'') THEN '有效' ELSE '无效' END AS '是否有效',录入时间 FROM dbo.ERPEXCH WHERE  isnull(库存数量,0)-isnull(已用库存,0)<=0 AND  条形码 LIKE '%'+'{0}'+'%'", TiaoMa_));
                }
                if (dt.Rows.Count > 0)
                {
                    this.StockdGV.DataSource = dt;
                    this.StockdGV.Columns["guid"].Visible = false;
                    this.StockdGV.Columns["卷号"].Visible = false;
                    this.StockdGV.Columns["缸号"].Visible = false;
                    CsFormShow.SetDvgRowHeight(this.StockdGV, 40, 40, true, Color.LightSteelBlue, this);
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
        private void OKbtn_Click(object sender, EventArgs e)
        {
            NewShow(this.TMtBx.Text.ToString().Trim(),this.IsValidcbBx.Text.Trim());
        }

        private void 清空库存ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult RSS = MessageBox.Show(this, "确定要清空库存吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                switch (RSS)
                {
                    case DialogResult.Yes:
                        DataGridView dgv_v = StockdGV as DataGridView;
                        if (dgv_v.SelectedRows.Count == 0)
                        {
                            return;
                        }
                        else
                        {
                            string guid = "";
                            for (int i = dgv_v.SelectedRows.Count; i > 0; i--)
                            {

                                if (!string.IsNullOrEmpty(dgv_v.SelectedRows[i - 1].Cells["guid"].Value.ToString()))
                                {
                                    guid = dgv_v.SelectedRows[i - 1].Cells["guid"].Value.ToString();
                                    string tiaoma_v = dgv_v.SelectedRows[i - 1].Cells["条形码"].Value.ToString();
                                    dgv_v.Rows.RemoveAt(dgv_v.SelectedRows[i - 1].Index);
                                    //使用获得的guid删除数据库的数据 
                                    //string sqlstr = string.Format("UPDATE dbo.ERPEXCH SET  已用库存=库存数量 WHERE GUID='{0}'", guid);
                                   // CsFormShow.GoSqlUpdateInsert(sqlstr);
                                    SqlParameter[] sqlParameters = new SqlParameter[1] ;
                                    sqlParameters[0] = new SqlParameter("@TiaoMaVal", SqlDbType.VarChar, 30);
                                    sqlParameters[0].Value = tiaoma_v;
                                    CsFormShow.GoProc_ParameterNoSelect(sqlParameters, "pm_rn_ClearlksrerpERPEXCH");
                                }
                                else
                                {
                                    return;
                                }
                            }
                        }
                        break;
                    case DialogResult.No:
                        break;
                }

            }
            catch (Exception err)
            {

                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
    }
}
