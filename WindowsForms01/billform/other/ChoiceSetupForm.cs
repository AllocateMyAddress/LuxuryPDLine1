﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using Soketprint;
using MCData;
using Serialprint;
using System.IO;
using System.IO.Ports;
using Image_recognition;
using GetData;
using System.Data;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class ShieldForm : Form
    {
        MainrpForm Mainrpform = null;
        public ShieldForm()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.YamahackBox.Checked = true;
            this.MarkingckBox.Checked = true;
            this.PrintckBox.Checked = true;
            this.SmallLabelckBox.Checked = true;
            if (CSReadPlc.DevName.Contains("plc1"))
            {
                this.SerialPortcBox.Visible = false;
                label1.Visible = false;
                SmallLabelckBox.Visible = false;
            }
            else
            {
                Serialprint.serialprint serialprint = new serialprint();
                string[] strcom = SerialPort.GetPortNames();
                foreach (string comstr in strcom)
                {
                    if (comstr.Contains("COM1"))
                    {
                        this.SerialPortcBox.Items.Add(comstr);
                    }
                    else if (comstr.Contains("COM4"))
                    {
                        this.SerialPortcBox.Items.Clear();
                        this.SerialPortcBox.Items.Add(comstr);
                    }
                }
                this.SerialPortcBox.SelectedIndex = 0;
            }
        }

        private void ShieldForm_Load(object sender, EventArgs e)
        {

        }

        private void okbtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.YamahackBox.Checked)
                {
                    stcUserData.IsYamahaSetup = true;
                }
                else
                {
                    stcUserData.IsYamahaSetup = false;
                }
                if (this.MarkingckBox.Checked)
                {
                    stcUserData.IsMarkingSetup = true;
                }
                else
                {
                    stcUserData.IsMarkingSetup = false;
                }
                if (this.PrintckBox.Checked)
                {
                    stcUserData.IsPrintSetup = true;
                }
                else
                {
                    stcUserData.IsPrintSetup = false;
                }
                if (this.SmallLabelckBox.Checked)
                {

                     stcUserData.IsSmallLabelPrintSetup = true;
                     M_stcPrintData.SerialPortName = this.SerialPortcBox.Text.Trim();
                }
                else
                {
                    stcUserData.IsSmallLabelPrintSetup = false;
                }
                this.Close();
                this.Dispose();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void ShieldForm_FormClosed_1(object sender, FormClosedEventArgs e)
        {
        }
    }
}
