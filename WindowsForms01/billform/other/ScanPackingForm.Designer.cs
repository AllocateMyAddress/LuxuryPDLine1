﻿namespace WindowsForms01
{
    partial class ScanPackingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OKbtn = new System.Windows.Forms.Button();
            this.OrientationcBox = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.LinetBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.JianspeedtBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.GrabspeedtBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ThicktBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.ColumntBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.choosebtn = new System.Windows.Forms.Button();
            this.IsRotatetBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.MarkPatterncBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.toptBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.heighttBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.widthtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.longtBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.HookDirectioncBox = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.IsScanRuncBox = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.IsYaMaHaRuncBox = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.IsPackingRuncBox = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.IsPalletizingRuncBox = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.IsSealingRuncBox = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.PartitionTypecBX = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.IsPalletizingcBox = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.PrintIsshieidcBox = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.MarkingIsshieidcBox = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.PartitionTypecBox = new System.Windows.Forms.ComboBox();
            this.LayernumtBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.qtytBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.AddTmbtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // OKbtn
            // 
            this.OKbtn.Location = new System.Drawing.Point(486, 338);
            this.OKbtn.Name = "OKbtn";
            this.OKbtn.Size = new System.Drawing.Size(123, 54);
            this.OKbtn.TabIndex = 12;
            this.OKbtn.Text = "确定";
            this.OKbtn.UseVisualStyleBackColor = true;
            this.OKbtn.Click += new System.EventHandler(this.OKbtn_Click);
            // 
            // OrientationcBox
            // 
            this.OrientationcBox.FormattingEnabled = true;
            this.OrientationcBox.Location = new System.Drawing.Point(690, 214);
            this.OrientationcBox.Name = "OrientationcBox";
            this.OrientationcBox.Size = new System.Drawing.Size(127, 23);
            this.OrientationcBox.TabIndex = 183;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(617, 220);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 15);
            this.label23.TabIndex = 182;
            this.label23.Text = "正反方向:";
            // 
            // LinetBox
            // 
            this.LinetBox.Location = new System.Drawing.Point(590, 649);
            this.LinetBox.Name = "LinetBox";
            this.LinetBox.Size = new System.Drawing.Size(148, 25);
            this.LinetBox.TabIndex = 181;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(520, 652);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 15);
            this.label22.TabIndex = 180;
            this.label22.Text = "摆货行数";
            // 
            // JianspeedtBox
            // 
            this.JianspeedtBox.Location = new System.Drawing.Point(554, 494);
            this.JianspeedtBox.Name = "JianspeedtBox";
            this.JianspeedtBox.Size = new System.Drawing.Size(127, 25);
            this.JianspeedtBox.TabIndex = 175;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(456, 497);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 15);
            this.label15.TabIndex = 174;
            this.label15.Text = "加减速度:";
            // 
            // GrabspeedtBox
            // 
            this.GrabspeedtBox.Location = new System.Drawing.Point(832, 494);
            this.GrabspeedtBox.Name = "GrabspeedtBox";
            this.GrabspeedtBox.Size = new System.Drawing.Size(127, 25);
            this.GrabspeedtBox.TabIndex = 173;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(742, 494);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 15);
            this.label16.TabIndex = 172;
            this.label16.Text = "抓取速度:";
            // 
            // ThicktBox
            // 
            this.ThicktBox.Location = new System.Drawing.Point(690, 269);
            this.ThicktBox.Name = "ThicktBox";
            this.ThicktBox.Size = new System.Drawing.Size(127, 25);
            this.ThicktBox.TabIndex = 171;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(620, 272);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 15);
            this.label17.TabIndex = 170;
            this.label17.Text = "产品厚:";
            // 
            // ColumntBox
            // 
            this.ColumntBox.Location = new System.Drawing.Point(135, 214);
            this.ColumntBox.Name = "ColumntBox";
            this.ColumntBox.Size = new System.Drawing.Size(127, 25);
            this.ColumntBox.TabIndex = 167;
            this.ColumntBox.TextChanged += new System.EventHandler(this.ColumntBox_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(39, 222);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 15);
            this.label13.TabIndex = 166;
            this.label13.Text = "摆货列数:";
            // 
            // choosebtn
            // 
            this.choosebtn.Location = new System.Drawing.Point(844, 211);
            this.choosebtn.Name = "choosebtn";
            this.choosebtn.Size = new System.Drawing.Size(145, 32);
            this.choosebtn.TabIndex = 165;
            this.choosebtn.Text = "选列数和旋转方式";
            this.choosebtn.UseVisualStyleBackColor = true;
            this.choosebtn.Click += new System.EventHandler(this.choosebtn_Click);
            // 
            // IsRotatetBox
            // 
            this.IsRotatetBox.Location = new System.Drawing.Point(413, 219);
            this.IsRotatetBox.Name = "IsRotatetBox";
            this.IsRotatetBox.Size = new System.Drawing.Size(127, 25);
            this.IsRotatetBox.TabIndex = 164;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(323, 222);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 15);
            this.label14.TabIndex = 163;
            this.label14.Text = "纸箱旋转:";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(126, 523);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 15);
            this.label12.TabIndex = 161;
            this.label12.Text = "隔板选择:";
            // 
            // MarkPatterncBox
            // 
            this.MarkPatterncBox.FormattingEnabled = true;
            this.MarkPatterncBox.Location = new System.Drawing.Point(376, 646);
            this.MarkPatterncBox.Name = "MarkPatterncBox";
            this.MarkPatterncBox.Size = new System.Drawing.Size(148, 23);
            this.MarkPatterncBox.TabIndex = 158;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(303, 652);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 15);
            this.label11.TabIndex = 155;
            this.label11.Text = "喷码模式";
            // 
            // toptBox
            // 
            this.toptBox.Location = new System.Drawing.Point(150, 646);
            this.toptBox.Name = "toptBox";
            this.toptBox.Size = new System.Drawing.Size(148, 25);
            this.toptBox.TabIndex = 150;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(79, 649);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 15);
            this.label6.TabIndex = 149;
            this.label6.Text = "距离底部";
            // 
            // heighttBox
            // 
            this.heighttBox.Location = new System.Drawing.Point(690, 170);
            this.heighttBox.Name = "heighttBox";
            this.heighttBox.Size = new System.Drawing.Size(127, 25);
            this.heighttBox.TabIndex = 148;
            this.heighttBox.Validated += new System.EventHandler(this.heighttBox_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(632, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 15);
            this.label3.TabIndex = 147;
            this.label3.Text = "箱子高:";
            // 
            // widthtBox
            // 
            this.widthtBox.Location = new System.Drawing.Point(413, 164);
            this.widthtBox.Name = "widthtBox";
            this.widthtBox.Size = new System.Drawing.Size(127, 25);
            this.widthtBox.TabIndex = 146;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(338, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 15);
            this.label2.TabIndex = 145;
            this.label2.Text = "箱子宽:";
            // 
            // longtBox
            // 
            this.longtBox.Location = new System.Drawing.Point(135, 164);
            this.longtBox.Name = "longtBox";
            this.longtBox.Size = new System.Drawing.Size(127, 25);
            this.longtBox.TabIndex = 144;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 15);
            this.label1.TabIndex = 143;
            this.label1.Text = "箱子长:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 318);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 15);
            this.label7.TabIndex = 139;
            this.label7.Text = "单位：mm";
            // 
            // HookDirectioncBox
            // 
            this.HookDirectioncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.HookDirectioncBox.FormattingEnabled = true;
            this.HookDirectioncBox.Location = new System.Drawing.Point(959, 67);
            this.HookDirectioncBox.Name = "HookDirectioncBox";
            this.HookDirectioncBox.Size = new System.Drawing.Size(127, 23);
            this.HookDirectioncBox.TabIndex = 219;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(841, 70);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(112, 15);
            this.label38.TabIndex = 218;
            this.label38.Text = "勾子是否掉头：";
            // 
            // IsScanRuncBox
            // 
            this.IsScanRuncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsScanRuncBox.FormattingEnabled = true;
            this.IsScanRuncBox.Location = new System.Drawing.Point(690, 10);
            this.IsScanRuncBox.Name = "IsScanRuncBox";
            this.IsScanRuncBox.Size = new System.Drawing.Size(127, 23);
            this.IsScanRuncBox.TabIndex = 211;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(564, 13);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(112, 15);
            this.label24.TabIndex = 210;
            this.label24.Text = "是否运行扫码：";
            // 
            // IsYaMaHaRuncBox
            // 
            this.IsYaMaHaRuncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsYaMaHaRuncBox.FormattingEnabled = true;
            this.IsYaMaHaRuncBox.Location = new System.Drawing.Point(413, 10);
            this.IsYaMaHaRuncBox.Name = "IsYaMaHaRuncBox";
            this.IsYaMaHaRuncBox.Size = new System.Drawing.Size(127, 23);
            this.IsYaMaHaRuncBox.TabIndex = 209;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(292, 18);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(112, 15);
            this.label26.TabIndex = 208;
            this.label26.Text = "是否运行装箱：";
            // 
            // IsPackingRuncBox
            // 
            this.IsPackingRuncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsPackingRuncBox.FormattingEnabled = true;
            this.IsPackingRuncBox.Location = new System.Drawing.Point(690, 65);
            this.IsPackingRuncBox.Name = "IsPackingRuncBox";
            this.IsPackingRuncBox.Size = new System.Drawing.Size(127, 23);
            this.IsPackingRuncBox.TabIndex = 207;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(554, 70);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(127, 15);
            this.label27.TabIndex = 206;
            this.label27.Text = "是否运行开箱机：";
            // 
            // IsPalletizingRuncBox
            // 
            this.IsPalletizingRuncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsPalletizingRuncBox.FormattingEnabled = true;
            this.IsPalletizingRuncBox.Location = new System.Drawing.Point(413, 68);
            this.IsPalletizingRuncBox.Name = "IsPalletizingRuncBox";
            this.IsPalletizingRuncBox.Size = new System.Drawing.Size(127, 23);
            this.IsPalletizingRuncBox.TabIndex = 205;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(295, 68);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(112, 15);
            this.label28.TabIndex = 204;
            this.label28.Text = "是否运行码垛：";
            // 
            // IsSealingRuncBox
            // 
            this.IsSealingRuncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsSealingRuncBox.FormattingEnabled = true;
            this.IsSealingRuncBox.Location = new System.Drawing.Point(959, 10);
            this.IsSealingRuncBox.Name = "IsSealingRuncBox";
            this.IsSealingRuncBox.Size = new System.Drawing.Size(127, 23);
            this.IsSealingRuncBox.TabIndex = 203;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(827, 13);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(127, 15);
            this.label29.TabIndex = 202;
            this.label29.Text = "是否运行封箱机：";
            // 
            // PartitionTypecBX
            // 
            this.PartitionTypecBX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PartitionTypecBX.FormattingEnabled = true;
            this.PartitionTypecBX.Location = new System.Drawing.Point(211, 520);
            this.PartitionTypecBX.Name = "PartitionTypecBX";
            this.PartitionTypecBX.Size = new System.Drawing.Size(127, 23);
            this.PartitionTypecBX.TabIndex = 201;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(36, 18);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(97, 15);
            this.label30.TabIndex = 200;
            this.label30.Text = "天地盖类型：";
            // 
            // IsPalletizingcBox
            // 
            this.IsPalletizingcBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsPalletizingcBox.FormattingEnabled = true;
            this.IsPalletizingcBox.Location = new System.Drawing.Point(135, 116);
            this.IsPalletizingcBox.Name = "IsPalletizingcBox";
            this.IsPalletizingcBox.Size = new System.Drawing.Size(127, 23);
            this.IsPalletizingcBox.TabIndex = 199;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(17, 119);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(112, 15);
            this.label31.TabIndex = 198;
            this.label31.Text = "与上一单码垛：";
            // 
            // PrintIsshieidcBox
            // 
            this.PrintIsshieidcBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PrintIsshieidcBox.FormattingEnabled = true;
            this.PrintIsshieidcBox.Location = new System.Drawing.Point(413, 121);
            this.PrintIsshieidcBox.Name = "PrintIsshieidcBox";
            this.PrintIsshieidcBox.Size = new System.Drawing.Size(127, 23);
            this.PrintIsshieidcBox.TabIndex = 197;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(276, 124);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(142, 15);
            this.label32.TabIndex = 196;
            this.label32.Text = "是否运行侧面贴标：";
            // 
            // MarkingIsshieidcBox
            // 
            this.MarkingIsshieidcBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MarkingIsshieidcBox.FormattingEnabled = true;
            this.MarkingIsshieidcBox.Location = new System.Drawing.Point(135, 65);
            this.MarkingIsshieidcBox.Name = "MarkingIsshieidcBox";
            this.MarkingIsshieidcBox.Size = new System.Drawing.Size(127, 23);
            this.MarkingIsshieidcBox.TabIndex = 195;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(19, 68);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(112, 15);
            this.label33.TabIndex = 194;
            this.label33.Text = "是否运行喷码：";
            // 
            // PartitionTypecBox
            // 
            this.PartitionTypecBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PartitionTypecBox.FormattingEnabled = true;
            this.PartitionTypecBox.Location = new System.Drawing.Point(133, 12);
            this.PartitionTypecBox.Name = "PartitionTypecBox";
            this.PartitionTypecBox.Size = new System.Drawing.Size(127, 23);
            this.PartitionTypecBox.TabIndex = 222;
            // 
            // LayernumtBox
            // 
            this.LayernumtBox.Location = new System.Drawing.Point(413, 266);
            this.LayernumtBox.Name = "LayernumtBox";
            this.LayernumtBox.Size = new System.Drawing.Size(127, 25);
            this.LayernumtBox.TabIndex = 224;
            this.LayernumtBox.Validated += new System.EventHandler(this.LayernumtBox_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(325, 269);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 15);
            this.label4.TabIndex = 223;
            this.label4.Text = "箱内件数:";
            // 
            // qtytBox
            // 
            this.qtytBox.Location = new System.Drawing.Point(133, 266);
            this.qtytBox.Name = "qtytBox";
            this.qtytBox.Size = new System.Drawing.Size(127, 25);
            this.qtytBox.TabIndex = 226;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(69, 269);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 15);
            this.label19.TabIndex = 225;
            this.label19.Text = "箱数:";
            // 
            // AddTmbtn
            // 
            this.AddTmbtn.Location = new System.Drawing.Point(690, 112);
            this.AddTmbtn.Name = "AddTmbtn";
            this.AddTmbtn.Size = new System.Drawing.Size(127, 38);
            this.AddTmbtn.TabIndex = 227;
            this.AddTmbtn.Text = "添加条码..";
            this.AddTmbtn.UseVisualStyleBackColor = true;
            this.AddTmbtn.Click += new System.EventHandler(this.AddTmbtn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(613, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 228;
            this.label5.Text = "条码值：";
            // 
            // ScanPackingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 468);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AddTmbtn);
            this.Controls.Add(this.qtytBox);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.LayernumtBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.PartitionTypecBox);
            this.Controls.Add(this.HookDirectioncBox);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.IsScanRuncBox);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.IsYaMaHaRuncBox);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.IsPackingRuncBox);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.IsPalletizingRuncBox);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.IsSealingRuncBox);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.PartitionTypecBX);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.IsPalletizingcBox);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.PrintIsshieidcBox);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.MarkingIsshieidcBox);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.OrientationcBox);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.LinetBox);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.JianspeedtBox);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.GrabspeedtBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.ThicktBox);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.ColumntBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.choosebtn);
            this.Controls.Add(this.IsRotatetBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.MarkPatterncBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.toptBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.heighttBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.widthtBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.longtBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.OKbtn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ScanPackingForm";
            this.Text = "装箱数据录入";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ScanPackingForm_FormClosed);
            this.Load += new System.EventHandler(this.ScanPackingForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button OKbtn;
        private System.Windows.Forms.ComboBox OrientationcBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox LinetBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox JianspeedtBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox GrabspeedtBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox ThicktBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox ColumntBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button choosebtn;
        private System.Windows.Forms.TextBox IsRotatetBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox MarkPatterncBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox toptBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox heighttBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox widthtBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox longtBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox HookDirectioncBox;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox IsScanRuncBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox IsYaMaHaRuncBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox IsPackingRuncBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox IsPalletizingRuncBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox IsSealingRuncBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox PartitionTypecBX;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox IsPalletizingcBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox PrintIsshieidcBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox MarkingIsshieidcBox;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox PartitionTypecBox;
        private System.Windows.Forms.TextBox LayernumtBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox qtytBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button AddTmbtn;
        private System.Windows.Forms.Label label5;
    }
}