﻿
namespace WindowsForms01
{
    partial class ProducingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProcducingdGV = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.ProcducingdGV)).BeginInit();
            this.SuspendLayout();
            // 
            // ProcducingdGV
            // 
            this.ProcducingdGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProcducingdGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProcducingdGV.Location = new System.Drawing.Point(0, 0);
            this.ProcducingdGV.Name = "ProcducingdGV";
            this.ProcducingdGV.RowHeadersWidth = 51;
            this.ProcducingdGV.RowTemplate.Height = 27;
            this.ProcducingdGV.Size = new System.Drawing.Size(800, 450);
            this.ProcducingdGV.TabIndex = 0;
            // 
            // ProducingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ProcducingdGV);
            this.Name = "ProducingForm";
            this.Text = "正在生产";
            this.Load += new System.EventHandler(this.ProducingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ProcducingdGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ProcducingdGV;
    }
}