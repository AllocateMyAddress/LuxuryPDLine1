﻿namespace WindowsForms01
{
    partial class PlcValForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nametBox = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.okbtn = new System.Windows.Forms.Button();
            this.drtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "寄存器名称";
            // 
            // nametBox
            // 
            this.nametBox.Location = new System.Drawing.Point(100, 26);
            this.nametBox.Name = "nametBox";
            this.nametBox.Size = new System.Drawing.Size(134, 25);
            this.nametBox.TabIndex = 1;
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 15;
            this.listBox1.Location = new System.Drawing.Point(2, 76);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(935, 379);
            this.listBox1.TabIndex = 4;
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(507, 12);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(119, 48);
            this.okbtn.TabIndex = 5;
            this.okbtn.Text = "查询";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.okbtn_Click);
            // 
            // drtBox
            // 
            this.drtBox.Location = new System.Drawing.Point(331, 26);
            this.drtBox.Name = "drtBox";
            this.drtBox.Size = new System.Drawing.Size(134, 25);
            this.drtBox.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(243, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "寄存器地址";
            // 
            // PlcValForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 456);
            this.Controls.Add(this.drtBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.okbtn);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.nametBox);
            this.Controls.Add(this.label1);
            this.Name = "PlcValForm";
            this.Text = "查询寄存器";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PlcValForm_FormClosed);
            this.Load += new System.EventHandler(this.PlcValForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nametBox;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button okbtn;
        private System.Windows.Forms.TextBox drtBox;
        private System.Windows.Forms.Label label2;
    }
}