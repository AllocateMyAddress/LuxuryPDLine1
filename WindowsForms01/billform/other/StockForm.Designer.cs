﻿
namespace WindowsForms01
{
    partial class StockForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.StockdGV = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.TMtBx = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.OKbtn = new DevComponents.DotNetBar.ButtonX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.清空库存ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.IsValidcbBx = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            ((System.ComponentModel.ISupportInitialize)(this.StockdGV)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // StockdGV
            // 
            this.StockdGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StockdGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StockdGV.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.StockdGV.DefaultCellStyle = dataGridViewCellStyle1;
            this.StockdGV.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.StockdGV.Location = new System.Drawing.Point(-1, 81);
            this.StockdGV.Name = "StockdGV";
            this.StockdGV.RowHeadersWidth = 51;
            this.StockdGV.RowTemplate.Height = 27;
            this.StockdGV.Size = new System.Drawing.Size(799, 373);
            this.StockdGV.TabIndex = 0;
            // 
            // labelX1
            // 
            this.labelX1.Location = new System.Drawing.Point(12, 31);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 1;
            this.labelX1.Text = "条形码：";
            // 
            // TMtBx
            // 
            // 
            // 
            // 
            this.TMtBx.Border.Class = "TextBoxBorder";
            this.TMtBx.Location = new System.Drawing.Point(81, 29);
            this.TMtBx.Name = "TMtBx";
            this.TMtBx.Size = new System.Drawing.Size(161, 25);
            this.TMtBx.TabIndex = 2;
            // 
            // OKbtn
            // 
            this.OKbtn.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.OKbtn.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.OKbtn.Location = new System.Drawing.Point(523, 12);
            this.OKbtn.Name = "OKbtn";
            this.OKbtn.Size = new System.Drawing.Size(92, 50);
            this.OKbtn.TabIndex = 3;
            this.OKbtn.Text = "确定";
            this.OKbtn.Click += new System.EventHandler(this.OKbtn_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.清空库存ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(139, 28);
            // 
            // 清空库存ToolStripMenuItem
            // 
            this.清空库存ToolStripMenuItem.Name = "清空库存ToolStripMenuItem";
            this.清空库存ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.清空库存ToolStripMenuItem.Text = "设置失效";
            this.清空库存ToolStripMenuItem.Click += new System.EventHandler(this.清空库存ToolStripMenuItem_Click);
            // 
            // labelX2
            // 
            this.labelX2.Location = new System.Drawing.Point(265, 31);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(91, 23);
            this.labelX2.TabIndex = 5;
            this.labelX2.Text = "是否有效：";
            // 
            // IsValidcbBx
            // 
            this.IsValidcbBx.DisplayMember = "Text";
            this.IsValidcbBx.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.IsValidcbBx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsValidcbBx.FormattingEnabled = true;
            this.IsValidcbBx.ItemHeight = 19;
            this.IsValidcbBx.Location = new System.Drawing.Point(348, 29);
            this.IsValidcbBx.Name = "IsValidcbBx";
            this.IsValidcbBx.Size = new System.Drawing.Size(121, 25);
            this.IsValidcbBx.TabIndex = 6;
            // 
            // StockForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.IsValidcbBx);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.OKbtn);
            this.Controls.Add(this.TMtBx);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.StockdGV);
            this.Name = "StockForm";
            this.Text = "库存表";
            this.Load += new System.EventHandler(this.StockForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StockdGV)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.DataGridViewX StockdGV;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX TMtBx;
        private DevComponents.DotNetBar.ButtonX OKbtn;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 清空库存ToolStripMenuItem;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx IsValidcbBx;
    }
}