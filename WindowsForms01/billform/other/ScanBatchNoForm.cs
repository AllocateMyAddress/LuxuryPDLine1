﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;

namespace WindowsForms01
{
    public partial class ScanBatchNoForm : Form
    {
        string ScanFlag = "";
        string Last_TiaoMa = "";
        string po_guid = "";
        static  string BatchNo_str = "";
        public ScanBatchNoForm()
        {
            InitializeComponent();
        }
        public ScanBatchNoForm(string po_guid_)
        {
            InitializeComponent();
            po_guid = po_guid_;
            ScanlsBox.Font = new Font("宋体", 25);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            //开启读取线程
            Thread th_read = new Thread(ReadBatchNoTh);
            th_read.IsBackground = false;
            th_read.Start();
            //
            Clear();
            BatchNo_str = "";
            M_stcScanData.M_List_BatchNo.Clear();
            ScanFlag = "start";
        }
        private void StartScanbtn_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                BatchNo_str = "";
                M_stcScanData.M_List_BatchNo.Clear();
                ScanFlag = "start";
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
        void ReadBatchNoTh()
        {
            try
            {
                while (true)
                {
                    if (ScanFlag.Contains("start"))
                    {
                        Thread.Sleep(2000);
                        //查询当前扫码卷号数据
                        SqlParameter[] sqlParameter_v = new SqlParameter[2];
                        sqlParameter_v[0] = new SqlParameter("@Device", SqlDbType.NVarChar, 10);
                        sqlParameter_v[1] = new SqlParameter("@cmd", SqlDbType.NVarChar, 20);

                        sqlParameter_v[0].Value = CSReadPlc.DevName.Trim();
                        sqlParameter_v[1].Value = "查询";
                        DataTable dt = CsFormShow.GoProc_Parameter(sqlParameter_v, "pm_rn_SelectBatchNo");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string juanhao_v = dt.Rows[i]["卷号"].ToString().Trim();
                            if (!M_stcScanData.M_List_BatchNo.Contains(juanhao_v))
                            {
                                M_stcScanData.M_List_BatchNo.Add(juanhao_v);
                                ScanlsBox.Items.Add(juanhao_v);
                                if (!string.IsNullOrEmpty(BatchNo_str))
                                {
                                    BatchNo_str = BatchNo_str + "," + M_stcScanData.M_List_BatchNo.ToList()[i];
                                }
                                else
                                {
                                    BatchNo_str = M_stcScanData.M_List_BatchNo.ToList()[i];
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
        private void EndScanbtn_Click(object sender, EventArgs e)
        {
            Clear();
           // CsFormShow.MessageBoxFormShow("扫描完成");
            this.Close();
            this.Dispose();
        }

        private void ScanBatchNoForm_Load(object sender, EventArgs e)
        {

        }

        public void Clear()
        {
            try
            {
                //清除数据库数据
                SqlParameter[] sqlParameter_v = new SqlParameter[2];
                sqlParameter_v[0] = new SqlParameter("@Device", SqlDbType.NVarChar, 10);
                sqlParameter_v[1] = new SqlParameter("@cmd", SqlDbType.NVarChar, 20);

                sqlParameter_v[0].Value = CSReadPlc.DevName.Trim();
                sqlParameter_v[1].Value = "删除";
                CsFormShow.GoProc_ParameterNoSelect(sqlParameter_v, "pm_rn_SelectBatchNo");
                ScanlsBox.Items.Clear();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
        private void ScanBatchNoForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!string.IsNullOrEmpty(BatchNo_str))
            {
                DialogResult btchose = MessageBox.Show("是否将当前卷号写入当前订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    M_stcScanData.BatchNo_Now = BatchNo_str;
                    DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM (SELECT packing_guid AS 'po_guid','GAP' AS 'potype' FROM dbo.MarkingLineUp UNION ALL SELECT YK_guid AS 'po_guid', potype  FROM dbo.YYKMarkingLineUp) AS a WHERE  po_guid = '{0}'", po_guid));
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["potype"].ToString().Trim().Contains("GAP"))
                        {
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET BatchNo='{0}' WHERE packing_guid='{1}'", BatchNo_str, po_guid));
                        }
                        else
                        {
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET BatchNo='{0}' WHERE YK_guid='{1}'", BatchNo_str, po_guid));
                        }
                    }
                    Clear();
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    Clear();
                    this.Close();
                    this.Dispose();
                }
            }
            else
            {
                Clear();
                this.Close();
                this.Dispose();
            }
        }
    }
}
