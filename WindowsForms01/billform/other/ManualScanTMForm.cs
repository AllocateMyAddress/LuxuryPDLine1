﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class ManualScanTMForm : Form
    {
        public ManualScanTMForm()
        {
            InitializeComponent();
        }

        private void okbtn_Click_1(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> dic_control = new Dictionary<string, string>();
                List<Control> controls_list1 = new List<Control>();
                //List<Control> controls_list2 = new List<Control>();
                //Control[] controls_shuzu1 =new Control[] { }  /*{ TM1tBx, TM2tBx, TM1tBx, TM3tBx, TM4tBx, TM5tBx, TM6tBx, TM7tBx, TM8tBx, TM9tBx, TM10tBx }*/;
                //Control[] controls_shuzu2 = new Control[] { } /*{ Qty1tBx, Qty2tBx, Qty3tBx, Qty4tBx, Qty5tBx, Qty6tBx, Qty7tBx, Qty8tBx, Qty9tBx, Qty10tBx }*/;
                foreach (Control ctr in this.panel1.Controls)
                {
                    //Type type=  ctr.GetType();//获取类型名
                    if (ctr is TextBox)
                    {
                        if (!string.IsNullOrEmpty(ctr.Text.Trim()))
                        {
                            controls_list1.Add(ctr);
                        }
                    }
                }
                if (Chekin.Chekedin(this, controls_list1, "ManualScanTMForm"))
                {
                    M_stcScanData.M_List_WriteToPlcSku.Clear();
                    List<M_stc_WriteToPlcSku> list_v = new List<M_stc_WriteToPlcSku>();
                    foreach (Control ctrl in controls_list1)
                    {
                        M_stc_WriteToPlcSku m_Stc_ = new M_stc_WriteToPlcSku();
                        if (ctrl.Name == "TM1tBx")
                        {
                            m_Stc_.dic_sku["D5100"] = TM1tBx.Text.Trim();
                            m_Stc_.dic_Write_Layernum["D5060"] = Qty1tBx.Text.Trim();
                            list_v.Add(m_Stc_);
                        }
                        else if (ctrl.Name == "TM2tBx")
                        {
                            m_Stc_.dic_sku["D5120"] = TM2tBx.Text.Trim();
                            m_Stc_.dic_Write_Layernum["D5061"] = Qty2tBx.Text.Trim();
                            list_v.Add(m_Stc_);
                        }
                        else if (ctrl.Name == "TM3tBx")
                        {
                            m_Stc_.dic_sku["D5140"] = TM3tBx.Text.Trim();
                            m_Stc_.dic_Write_Layernum["D5062"] = Qty3tBx.Text.Trim();
                            list_v.Add(m_Stc_);
                        }
                        else if (ctrl.Name == "TM4tBx")
                        {
                            m_Stc_.dic_sku["D5160"] = TM4tBx.Text.Trim();
                            m_Stc_.dic_Write_Layernum["D5063"] = Qty4tBx.Text.Trim();
                            list_v.Add(m_Stc_);
                        }
                        else if (ctrl.Name == "TM5tBx")
                        {
                            m_Stc_.dic_sku["D5180"] = TM5tBx.Text.Trim();
                            m_Stc_.dic_Write_Layernum["D5064"] = Qty5tBx.Text.Trim();
                            list_v.Add(m_Stc_);
                        }
                        else if (ctrl.Name == "TM6tBx")
                        {
                            m_Stc_.dic_sku["D5200"] = TM6tBx.Text.Trim();
                            m_Stc_.dic_Write_Layernum["D5065"] = Qty6tBx.Text.Trim();
                            list_v.Add(m_Stc_);
                        }
                        else if (ctrl.Name == "TM7tBx")
                        {
                            m_Stc_.dic_sku["D5220"] = TM7tBx.Text.Trim();
                            m_Stc_.dic_Write_Layernum["D5066"] = Qty7tBx.Text.Trim();
                            list_v.Add(m_Stc_);
                        }
                        else if (ctrl.Name == "TM8tBx")
                        {
                            m_Stc_.dic_sku["D5240"] = TM8tBx.Text.Trim();
                            m_Stc_.dic_Write_Layernum["D5067"] = Qty8tBx.Text.Trim();
                            list_v.Add(m_Stc_);
                        }
                        else if (ctrl.Name == "TM9tBx")
                        {
                            m_Stc_.dic_sku["D5260"] = TM9tBx.Text.Trim();
                            m_Stc_.dic_Write_Layernum["D5068"] = Qty9tBx.Text.Trim();
                            list_v.Add(m_Stc_);
                        }
                        else if (ctrl.Name == "TM10tBx")
                        {
                            m_Stc_.dic_sku["D5280"] = TM10tBx.Text.Trim();
                            m_Stc_.dic_Write_Layernum["D5069"] = Qty10tBx.Text.Trim();
                            list_v.Add(m_Stc_);
                        }
                    }
                    M_stcScanData.M_List_WriteToPlcSku = list_v;
                    MessageBox.Show("设置完成");
                    this.Close();
                    this.Dispose();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
    }
}
