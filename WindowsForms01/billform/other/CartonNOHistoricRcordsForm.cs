﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class CartonNOHistoricRcordsForm : Form
    {
        public CartonNOHistoricRcordsForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.CartonNoHistoricRcordsdGV, 40, 40, true, Color.LightSteelBlue, this);
            NewShow();
        }

        private void CartonNOHistoricRcordsForm_Load(object sender, EventArgs e)
        {

        }
        public void NewShow()
        {
            try
            {
                CartonNoHistoricRcordsdGV.DataSource = null;
                DataTable dt = CsFormShow.GoSqlSelect("SELECT ROW_NUMBER() OVER(ORDER BY 单号,箱号,录入时间) AS 序号,* FROM CartonNOHistoricRcords ");
                if (dt.Rows.Count>0)
                {
                    CartonNoHistoricRcordsdGV.DataSource = dt;
                    CartonNoHistoricRcordsdGV.Columns["guid"].Visible = false;
                    CartonNoHistoricRcordsdGV.Columns["DeviceName"].Visible = false;
                    for (int i = 0; i < this.CartonNoHistoricRcordsdGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.CartonNoHistoricRcordsdGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        public void NewShow(string po_,string CartonNo_,string starttime_,string endtime)
        {
            try
            {
                CartonNoHistoricRcordsdGV.DataSource = null;
                DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT  ROW_NUMBER() OVER(ORDER BY 单号,箱号,录入时间) AS 序号,*  FROM CartonNOHistoricRcords  WHERE 单号 LIKE '%'+'{0}'+'%' AND 箱号 LIKE '%'+'{1}'+'%' AND LEFT(录入时间,10) >=CASE WHEN '{2}'='' THEN '1000-01-01' ELSE '{2}' END AND LEFT(录入时间,10) <=CASE WHEN '{3}'='' THEN CONVERT(VARCHAR(10),GETDATE(),120) ELSE '{3}' END", po_, CartonNo_, starttime_, endtime));
                if (dt.Rows.Count > 0)
                {
                    CartonNoHistoricRcordsdGV.DataSource = dt;
                    CartonNoHistoricRcordsdGV.Columns["guid"].Visible = false;
                    CartonNoHistoricRcordsdGV.Columns["DeviceName"].Visible = false;
                    for (int i = 0; i < this.CartonNoHistoricRcordsdGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.CartonNoHistoricRcordsdGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void okbtn_Click(object sender, EventArgs e)
        {
            NewShow(this.PotBox.Text.Trim(),this.CartonNotBox.Text.Trim(),this.starttimetBox.Text.Trim(),this.endtimetBox.Text.Trim());
        }

        private void StartdTPk_MouseDown(object sender, MouseEventArgs e)
        {

        }
        private void EnddTPk_MouseEnter(object sender, EventArgs e)
        {
        }

        private void StartdTPk_ValueChanged(object sender, EventArgs e)
        {
            this.starttimetBox.Text = DateTime.Parse(this.StartdTPk.Text).ToString("yyyy-MM-dd");
        }

        private void EnddTPk_ValueChanged(object sender, EventArgs e)
        {
            this.endtimetBox.Text =DateTime.Parse(this.EnddTPk.Text).ToString("yyyy-MM-dd");
        }
    }
}
