﻿
namespace WindowsForms01
{
    partial class ManualScanTMForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.okbtn = new System.Windows.Forms.Button();
            this.Qty10tBx = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.TM10tBx = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Qty9tBx = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.TM9tBx = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Qty8tBx = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.TM8tBx = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Qty7tBx = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.TM7tBx = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Qty6tBx = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TM6tBx = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Qty5tBx = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TM5tBx = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Qty4tBx = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TM4tBx = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Qty3tBx = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TM3tBx = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Qty2tBx = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TM2tBx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Qty1tBx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TM1tBx = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.okbtn);
            this.panel1.Controls.Add(this.Qty10tBx);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.TM10tBx);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.Qty9tBx);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.TM9tBx);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.Qty8tBx);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.TM8tBx);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.Qty7tBx);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.TM7tBx);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.Qty6tBx);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.TM6tBx);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.Qty5tBx);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.TM5tBx);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.Qty4tBx);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.TM4tBx);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.Qty3tBx);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.TM3tBx);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.Qty2tBx);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.TM2tBx);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.Qty1tBx);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.TM1tBx);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(529, 518);
            this.panel1.TabIndex = 41;
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(189, 446);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(108, 40);
            this.okbtn.TabIndex = 81;
            this.okbtn.Text = "确定";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.okbtn_Click_1);
            // 
            // Qty10tBx
            // 
            this.Qty10tBx.Location = new System.Drawing.Point(369, 405);
            this.Qty10tBx.Name = "Qty10tBx";
            this.Qty10tBx.Size = new System.Drawing.Size(138, 25);
            this.Qty10tBx.TabIndex = 80;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(250, 408);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(121, 15);
            this.label19.TabIndex = 79;
            this.label19.Text = "条码10箱内件数:";
            // 
            // TM10tBx
            // 
            this.TM10tBx.Location = new System.Drawing.Point(80, 405);
            this.TM10tBx.Name = "TM10tBx";
            this.TM10tBx.Size = new System.Drawing.Size(138, 25);
            this.TM10tBx.TabIndex = 78;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(21, 408);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 15);
            this.label20.TabIndex = 77;
            this.label20.Text = "条码10:";
            // 
            // Qty9tBx
            // 
            this.Qty9tBx.Location = new System.Drawing.Point(369, 366);
            this.Qty9tBx.Name = "Qty9tBx";
            this.Qty9tBx.Size = new System.Drawing.Size(138, 25);
            this.Qty9tBx.TabIndex = 76;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(250, 369);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(113, 15);
            this.label17.TabIndex = 75;
            this.label17.Text = "条码9箱内件数:";
            // 
            // TM9tBx
            // 
            this.TM9tBx.Location = new System.Drawing.Point(80, 366);
            this.TM9tBx.Name = "TM9tBx";
            this.TM9tBx.Size = new System.Drawing.Size(138, 25);
            this.TM9tBx.TabIndex = 74;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(21, 369);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 15);
            this.label18.TabIndex = 73;
            this.label18.Text = "条码9:";
            // 
            // Qty8tBx
            // 
            this.Qty8tBx.Location = new System.Drawing.Point(369, 335);
            this.Qty8tBx.Name = "Qty8tBx";
            this.Qty8tBx.Size = new System.Drawing.Size(138, 25);
            this.Qty8tBx.TabIndex = 72;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(250, 338);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 15);
            this.label15.TabIndex = 71;
            this.label15.Text = "条码8箱内件数:";
            // 
            // TM8tBx
            // 
            this.TM8tBx.Location = new System.Drawing.Point(80, 335);
            this.TM8tBx.Name = "TM8tBx";
            this.TM8tBx.Size = new System.Drawing.Size(138, 25);
            this.TM8tBx.TabIndex = 70;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(21, 338);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 15);
            this.label16.TabIndex = 69;
            this.label16.Text = "条码8:";
            // 
            // Qty7tBx
            // 
            this.Qty7tBx.Location = new System.Drawing.Point(369, 295);
            this.Qty7tBx.Name = "Qty7tBx";
            this.Qty7tBx.Size = new System.Drawing.Size(138, 25);
            this.Qty7tBx.TabIndex = 68;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(250, 298);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(113, 15);
            this.label13.TabIndex = 67;
            this.label13.Text = "条码7箱内件数:";
            // 
            // TM7tBx
            // 
            this.TM7tBx.Location = new System.Drawing.Point(80, 295);
            this.TM7tBx.Name = "TM7tBx";
            this.TM7tBx.Size = new System.Drawing.Size(138, 25);
            this.TM7tBx.TabIndex = 66;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 298);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 15);
            this.label14.TabIndex = 65;
            this.label14.Text = "条码7:";
            // 
            // Qty6tBx
            // 
            this.Qty6tBx.Location = new System.Drawing.Point(369, 252);
            this.Qty6tBx.Name = "Qty6tBx";
            this.Qty6tBx.Size = new System.Drawing.Size(138, 25);
            this.Qty6tBx.TabIndex = 64;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(250, 255);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(113, 15);
            this.label11.TabIndex = 63;
            this.label11.Text = "条码6箱内件数:";
            // 
            // TM6tBx
            // 
            this.TM6tBx.Location = new System.Drawing.Point(80, 252);
            this.TM6tBx.Name = "TM6tBx";
            this.TM6tBx.Size = new System.Drawing.Size(138, 25);
            this.TM6tBx.TabIndex = 62;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 255);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 15);
            this.label12.TabIndex = 61;
            this.label12.Text = "条码6:";
            // 
            // Qty5tBx
            // 
            this.Qty5tBx.Location = new System.Drawing.Point(369, 209);
            this.Qty5tBx.Name = "Qty5tBx";
            this.Qty5tBx.Size = new System.Drawing.Size(138, 25);
            this.Qty5tBx.TabIndex = 60;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(250, 212);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 15);
            this.label9.TabIndex = 59;
            this.label9.Text = "条码5箱内件数:";
            // 
            // TM5tBx
            // 
            this.TM5tBx.Location = new System.Drawing.Point(80, 209);
            this.TM5tBx.Name = "TM5tBx";
            this.TM5tBx.Size = new System.Drawing.Size(138, 25);
            this.TM5tBx.TabIndex = 58;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 212);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 15);
            this.label10.TabIndex = 57;
            this.label10.Text = "条码5:";
            // 
            // Qty4tBx
            // 
            this.Qty4tBx.Location = new System.Drawing.Point(369, 163);
            this.Qty4tBx.Name = "Qty4tBx";
            this.Qty4tBx.Size = new System.Drawing.Size(138, 25);
            this.Qty4tBx.TabIndex = 56;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(250, 166);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 15);
            this.label7.TabIndex = 55;
            this.label7.Text = "条码4箱内件数:";
            // 
            // TM4tBx
            // 
            this.TM4tBx.Location = new System.Drawing.Point(80, 163);
            this.TM4tBx.Name = "TM4tBx";
            this.TM4tBx.Size = new System.Drawing.Size(138, 25);
            this.TM4tBx.TabIndex = 54;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 166);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 15);
            this.label8.TabIndex = 53;
            this.label8.Text = "条码4:";
            // 
            // Qty3tBx
            // 
            this.Qty3tBx.Location = new System.Drawing.Point(369, 122);
            this.Qty3tBx.Name = "Qty3tBx";
            this.Qty3tBx.Size = new System.Drawing.Size(138, 25);
            this.Qty3tBx.TabIndex = 52;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(250, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 15);
            this.label5.TabIndex = 51;
            this.label5.Text = "条码3箱内件数:";
            // 
            // TM3tBx
            // 
            this.TM3tBx.Location = new System.Drawing.Point(80, 122);
            this.TM3tBx.Name = "TM3tBx";
            this.TM3tBx.Size = new System.Drawing.Size(138, 25);
            this.TM3tBx.TabIndex = 50;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 15);
            this.label6.TabIndex = 49;
            this.label6.Text = "条码3:";
            // 
            // Qty2tBx
            // 
            this.Qty2tBx.Location = new System.Drawing.Point(369, 80);
            this.Qty2tBx.Name = "Qty2tBx";
            this.Qty2tBx.Size = new System.Drawing.Size(138, 25);
            this.Qty2tBx.TabIndex = 48;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(250, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 15);
            this.label3.TabIndex = 47;
            this.label3.Text = "条码2箱内件数:";
            // 
            // TM2tBx
            // 
            this.TM2tBx.Location = new System.Drawing.Point(80, 80);
            this.TM2tBx.Name = "TM2tBx";
            this.TM2tBx.Size = new System.Drawing.Size(138, 25);
            this.TM2tBx.TabIndex = 46;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 45;
            this.label4.Text = "条码2:";
            // 
            // Qty1tBx
            // 
            this.Qty1tBx.Location = new System.Drawing.Point(369, 33);
            this.Qty1tBx.Name = "Qty1tBx";
            this.Qty1tBx.Size = new System.Drawing.Size(138, 25);
            this.Qty1tBx.TabIndex = 44;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(250, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 15);
            this.label2.TabIndex = 43;
            this.label2.Text = "条码1箱内件数:";
            // 
            // TM1tBx
            // 
            this.TM1tBx.Location = new System.Drawing.Point(80, 33);
            this.TM1tBx.Name = "TM1tBx";
            this.TM1tBx.Size = new System.Drawing.Size(138, 25);
            this.TM1tBx.TabIndex = 42;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 15);
            this.label1.TabIndex = 41;
            this.label1.Text = "条码1:";
            // 
            // ManualScanTMForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 518);
            this.Controls.Add(this.panel1);
            this.Name = "ManualScanTMForm";
            this.Text = "录入条码";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button okbtn;
        private System.Windows.Forms.TextBox Qty10tBx;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox TM10tBx;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Qty9tBx;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TM9tBx;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox Qty8tBx;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox TM8tBx;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Qty7tBx;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TM7tBx;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Qty6tBx;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TM6tBx;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Qty5tBx;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TM5tBx;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Qty4tBx;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TM4tBx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Qty3tBx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TM3tBx;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Qty2tBx;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TM2tBx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Qty1tBx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TM1tBx;
        private System.Windows.Forms.Label label1;
    }
}