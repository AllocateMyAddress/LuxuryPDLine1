﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MCData;

namespace WindowsForms01
{
    public partial class PlcValForm : Form
    {
        GetPlcBao csGetBao;
        MCData.MCdata mCdata = new MCdata();
        public PlcValForm()
        {
            InitializeComponent();
        }
        public PlcValForm(GetPlcBao csGetBao_, MCData.MCdata mCdata_)
        {
            InitializeComponent();
            csGetBao = csGetBao_;
        }
        private void okbtn_Click(object sender, EventArgs e)
        {
            string name = nametBox.Text.Trim();
            string dress = drtBox.Text.Trim();
            if (!string.IsNullOrEmpty(name))
            {
                string val = CSReadPlc.GetPlcString(name, CSReadPlc.DevName);
                string dressval = csGetBao.GetPlcAdrr(name, CSReadPlc.DevName);
                listBox1.Items.Add("名称：" + name + ",地址：" + dressval + ",寄存器值:" + val);
            }
            if (!string.IsNullOrEmpty(dress))
            {
                string dressname = csGetBao.GetValName(dress, CSReadPlc.DevName);
                string val = CSReadPlc.GetPlcString(dressname, CSReadPlc.DevName);
                listBox1.Items.Add("名称：" + dressname + "，地址：" + dress + "，寄存器值:" + val);
            }

        }

        private void PlcValForm_Load(object sender, EventArgs e)
        {

        }

        private void PlcValForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }
    }
}
