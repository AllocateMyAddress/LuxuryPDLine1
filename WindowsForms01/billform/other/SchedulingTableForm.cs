﻿using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace WindowsForms01
{
    public partial class SchedulingTableForm : Form
    {
        SetQtyForm setQtyform = null;
        ScanBatchNoForm ScanBatchNoform = null;
        public string RotateStyle = "";
        public string packingqty = "0";
        public string IsUnpacking = "";
        public string MarkingIsshieid = "";
        public string PrintIsshieid = "";
        public string GAPIsPrintRun = "";
        public string GAPSideIsshieid = "";
        public string IsPalletizing = "";
        public string Partitiontype = "";
        public string IsPackingRun = "";
        public string IsYaMaHaRun = "";
        public string IsSealingRun = "";
        public string IsPalletizingRun = "";
        public string IsRepeatCheck = "";
        public string IsScanRun = "";
        public string TiaoMaVal = "";
        public string HookDirection = "";
        public SchedulingTableForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.PlandGV, 45, 45, true, Color.LightSteelBlue, this);
            CsFormShow.SetDvgRowHeight(this.ProducedGV, 45, 45, true, Color.LightSteelBlue, this);
            SchedulingtabCtr.Font = new Font("楷体", 20);
            SchedulingtabCtr.SizeMode = TabSizeMode.FillToRight;
            SchedulingtabCtr.SelectedIndex = 1;
            this.PlantabPage.BorderStyle = BorderStyle.Fixed3D;
            this.ProducetabPage.BorderStyle = BorderStyle.Fixed3D;
            ProduceNewShow();
            PoTypetSpCBox.Items.Add("UNIQLO");
            PoTypetSpCBox.Items.Add("GAP/GU");
            PoTypetSpCBox.Items.Add("GU");
            PoTypetSpCBox.Items.Add("GAP");
            PoTypetSpCBox.Items.Add("所有品牌");
            if (CSReadPlc.code_v == 1)
            {
                PoTypetSpCBox.SelectedIndex = 0;
            }
            else if (CSReadPlc.code_v == 2)
            {
                PoTypetSpCBox.SelectedIndex = 1;
            }
            this.IsSatisfytSpCBx.Items.Add("部分满足/完全满足");
            this.IsSatisfytSpCBx.Items.Add("部分满足");
            this.IsSatisfytSpCBx.Items.Add("完全满足");
            this.IsSatisfytSpCBx.Items.Add("不限");
            IsSatisfytSpCBx.SelectedIndex = 0;
            this.IsChecktSpCBox.Items.Add("不限");
            this.IsChecktSpCBox.Items.Add("小于50%");
            this.IsChecktSpCBox.Items.Add("超过50%");
            IsChecktSpCBox.SelectedIndex = 0;
            PlanNewShow();
        }

        private void SchedulingDataForm_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 生产排单无参数查询显示
        /// </summary>
        public void ProduceNewShow()
        {
            try
            {
                this.ProducedGV.DataSource = null;
                string sqlselect = "";
                sqlselect = string.Format("SELECT ROW_NUMBER() OVER (ORDER BY  CASE WHEN  已选择=0 THEN 1 WHEN 已选择=1 THEN 0 END,sort) AS 序号, *FROM    (SELECT    (SELECT CASE WHEN COUNT(0) <> 0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END FROM(SELECT YK_guid AS po_guid, DeviceName FROM dbo.YYKMarkingLineUp UNION ALL SELECT packing_guid AS po_guid, DeviceName FROM dbo.MarkingLineUp) AS line WHERE  po_guid = dbo.SchedulingTable.po_guid) AS '已选择', sort, guid, po_guid, 订单号码, 从, Warehouse,SUBSTRING(Warehouse,0,5) AS '仓库代码', Set_Code, Color, packingqty AS '本次设置数量', dbo.SchedulingTable.potype, dbo.SchedulingTable.DeviceName, CatornStyle_guid, Long, Width, Heghit, TiaoMaVal ,TiaoMaVal AS '条形码',potype AS '品牌',(SELECT CASE WHEN COUNT(0) <> 0 THEN '进行中' ELSE '未生产' END  FROM(SELECT YK_guid AS po_guid, DeviceName FROM dbo.YYKMarkingLineUp UNION ALL SELECT packing_guid AS po_guid, DeviceName FROM dbo.MarkingLineUp) AS line WHERE  po_guid = dbo.SchedulingTable.po_guid) AS '状态',dbo.SchedulingTable.IsRepeatCheck FROM dbo.SchedulingTable WHERE dbo.SchedulingTable.DeviceName = '{0}' ) AS a ", CSReadPlc.DevName);
                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                if (dt.Rows.Count > 0)
                {
                    this.ProducedGV.DataSource = dt;
                    this.ProducedGV.Columns["guid"].Visible = false;
                    //this.ProducedGV.Columns["CatornStyle_guid"].Visible = false;
                    this.ProducedGV.Columns["po_guid"].Visible = false;
                    // this.ProducedGV.Columns["potype"].Visible = false;
                    this.ProducedGV.Columns["DeviceName"].Visible = false;
                    this.ProducedGV.Columns["CatornStyle_guid"].Visible = false;
                    this.ProducedGV.Columns["Long"].Visible = false;
                    this.ProducedGV.Columns["Width"].Visible = false;
                    this.ProducedGV.Columns["Heghit"].Visible = false;
                    this.ProducedGV.Columns["sort"].Visible = false;
                    this.ProducedGV.Columns["potype"].Visible = false;
                    this.ProducedGV.Columns["TiaoMaVal"].Visible = false;
                    this.ProducedGV.Columns["Warehouse"].Visible = false;
                    this.ProducedGV.Columns["状态"].Visible = false;
                    this.ProducedGV.Columns["IsRepeatCheck"].Visible = false;
                    for (int i = 0; i < this.ProducedGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.ProducedGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
                //for (int i = 0; i < ProducedGV.Rows.Count; i++)
                //{
                //    string val_v = ProducedGV.Rows[i].Cells["状态"].Value.ToString().Trim();
                //    if (val_v == "进行中")
                //    {
                //        ProducedGV.Rows[i].Cells["状态"].Style.BackColor = Color.Green;
                //    }
                //    else
                //    {
                //        ProducedGV.Rows[i].Cells["状态"].Style.BackColor = Color.Red;
                //    }

                //}
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 生产排单有条件查询显示
        /// </summary>
        /// <param name="Order_No_"></param>
        /// <param name="Warehouse_"></param>
        /// <param name="Set_Code_"></param>
        /// <param name="Sku_"></param>
        public void ProduceNewShow(string Order_No_, string Warehouse_, string Set_Code_, string Sku_)
        {
            try
            {
                this.ProducedGV.DataSource = null;
                string sqlselect = "";
                sqlselect = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY  CASE WHEN  已选择 = 0 THEN 1 WHEN 已选择 = 1 THEN 0 END, sort) AS 序号, *FROM (SELECT(SELECT CASE WHEN COUNT(0) <> 0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END FROM (SELECT YK_guid AS po_guid, DeviceName FROM dbo.YYKMarkingLineUp UNION ALL SELECT packing_guid AS po_guid, DeviceName FROM dbo.MarkingLineUp) AS line WHERE  po_guid = dbo.SchedulingTable.po_guid) AS '已选择', sort, guid, po_guid, 订单号码, 从, Warehouse,SUBSTRING(Warehouse,0,5) AS '仓库代码', Set_Code, Color, packingqty AS '本次设置数量', dbo.SchedulingTable.potype, dbo.SchedulingTable.DeviceName, CatornStyle_guid, Long, Width, Heghit, TiaoMaVal,TiaoMaVal AS '条形码',potype AS '品牌', (SELECT CASE WHEN COUNT(0) <> 0 THEN '进行中' ELSE '未生产' END  FROM(SELECT YK_guid AS po_guid, DeviceName FROM dbo.YYKMarkingLineUp UNION ALL SELECT packing_guid AS po_guid, DeviceName FROM dbo.MarkingLineUp) AS line WHERE  po_guid = dbo.SchedulingTable.po_guid) AS '状态',dbo.SchedulingTable.IsRepeatCheck  FROM dbo.SchedulingTable WHERE dbo.SchedulingTable.DeviceName = '{4}' and  订单号码 LIKE '%' + '{0}' + '%' AND Warehouse LIKE '%' + '{1}' + '%' AND Set_Code LIKE '%' + '{2}' + '%'  AND SKU LIKE '%' + '{3}' + '%') AS a", Order_No_, Warehouse_, Set_Code_, Sku_, CSReadPlc.DevName);
                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                if (dt.Rows.Count > 0)
                {
                    this.ProducedGV.DataSource = dt;
                    this.ProducedGV.Columns["guid"].Visible = false;
                    //this.ProducedGV.Columns["CatornStyle_guid"].Visible = false;
                    this.ProducedGV.Columns["po_guid"].Visible = false;
                    this.ProducedGV.Columns["potype"].Visible = false;
                    this.ProducedGV.Columns["CatornStyle_guid"].Visible = false;
                    this.ProducedGV.Columns["Long"].Visible = false;
                    this.ProducedGV.Columns["Width"].Visible = false;
                    this.ProducedGV.Columns["Heghit"].Visible = false;
                    this.ProducedGV.Columns["sort"].Visible = false;
                    this.ProducedGV.Columns["potype"].Visible = false;
                    this.ProducedGV.Columns["DeviceName"].Visible = false;
                    this.ProducedGV.Columns["TiaoMaVal"].Visible = false;
                    this.ProducedGV.Columns["Warehouse"].Visible = false;
                    this.ProducedGV.Columns["状态"].Visible = false;
                    this.ProducedGV.Columns["IsRepeatCheck"].Visible = false;
                    for (int i = 0; i < this.ProducedGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.ProducedGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 排单计划表无条件查询
        /// </summary>
        public void PlanNewShow()
        {
            try
            {
                SqlParameter[] parameter = new SqlParameter[7];
                parameter[0] = new SqlParameter("@PoType", System.Data.SqlDbType.NVarChar, 100);
                parameter[1] = new SqlParameter("@Order_No", System.Data.SqlDbType.NVarChar, 100);
                parameter[2] = new SqlParameter("@Warehouse", System.Data.SqlDbType.NVarChar, 100);
                parameter[3] = new SqlParameter("@Set_Code", System.Data.SqlDbType.NVarChar, 100);
                parameter[4] = new SqlParameter("@Sku", System.Data.SqlDbType.NVarChar, 100);
                parameter[5] = new SqlParameter("@IsSatisfy", System.Data.SqlDbType.NVarChar, 20);
                parameter[6] = new SqlParameter("@percent", System.Data.SqlDbType.NVarChar, 20);
                if (CSReadPlc.code_v == 1)
                {
                    parameter[0].Value = "UNIQLO";
                }
                else if (CSReadPlc.code_v == 2)
                {
                    parameter[0].Value = "GAP/GU";
                }
                parameter[1].Value = "";
                parameter[2].Value = "";
                parameter[3].Value = "";
                parameter[4].Value = "";
                parameter[5].Value = "部分可装箱";
                parameter[6].Value = "不限";
                PlandGV.DataSource = null;
                DataTable dt = CsFormShow.GoProc_Parameter(parameter, "pm_rp_SchedulingData");
                //DataTable dt=  CsFormShow.GoProc_NOParameter("pm_rn_Scheduling");
                if (dt.Rows.Count > 0)
                {
                    PlandGV.DataSource = dt;
                    PlandGV.Columns["guid"].Visible = false;
                    PlandGV.Columns["num"].Visible = false;
                    PlandGV.Columns["potype"].Visible = false;
                    //PlandGV.Columns["件数/箱"].Visible = false;
                    //PlandGV.Columns["完成箱数"].Visible = false;
                    PlandGV.Columns["数量"].Visible = false;
                    PlandGV.Columns["是否混色"].Visible = false;
                    PlandGV.Columns["Warehouse"].Visible = false;
                    PlandGV.Columns["WHDate"].Visible = false;
                    PlandGV.Columns["剩余件数"].Visible = false;
                    PlandGV.Columns["库存件数"].Visible = false;
                    PlandGV.Columns["占用箱数"].Visible = false;
                    PlandGV.Columns["完成比例"].Visible = false;
                    PlandGV.Columns["已选择"].Visible = false;
                    for (int i = 0; i < this.PlandGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.PlandGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                    for (int i = 0; i < PlandGV.Rows.Count; i++)
                    {
                        bool b_v = (bool)PlandGV.Rows[i].Cells["已选择"].Value;
                        if (b_v)
                        {
                            PlandGV.Rows[i].DefaultCellStyle.BackColor = Color.LimeGreen;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 排单计划表有条件查询
        /// </summary>
        /// <param name="Order_No_"></param>
        /// <param name="Warehouse_"></param>
        /// <param name="Set_Code_"></param>
        /// <param name="Sku_"></param>
        /// <param name="potype_"></param>
        private void PlanNewShow(string Order_No_, string Warehouse_, string Set_Code_, string Sku_, string potype_,string IsSatisfy_,string IsCheck_)
        {
            try
            {
                SqlParameter[] parameter = new SqlParameter[7];
                parameter[0] = new SqlParameter("@PoType", System.Data.SqlDbType.NVarChar, 100);
                parameter[1] = new SqlParameter("@Order_No", System.Data.SqlDbType.NVarChar, 100);
                parameter[2] = new SqlParameter("@Warehouse", System.Data.SqlDbType.NVarChar, 100);
                parameter[3] = new SqlParameter("@Set_Code", System.Data.SqlDbType.NVarChar, 100);
                parameter[4] = new SqlParameter("@Sku", System.Data.SqlDbType.NVarChar, 100);
                parameter[5] = new SqlParameter("@IsSatisfy", System.Data.SqlDbType.NVarChar, 20);
                parameter[6] = new SqlParameter("@percent", System.Data.SqlDbType.NVarChar, 20);
                parameter[0].Value = potype_;
                parameter[1].Value = Order_No_;
                parameter[2].Value = Warehouse_;
                parameter[3].Value = Set_Code_;
                parameter[4].Value = Sku_;
                parameter[5].Value = IsSatisfy_;
                parameter[6].Value = IsCheck_;
                PlandGV.DataSource = null;
                DataTable dt = CsFormShow.GoProc_Parameter(parameter, "pm_rp_SchedulingData");
                //DataTable dt=  CsFormShow.GoProc_NOParameter("pm_rn_Scheduling");
                if (dt.Rows.Count > 0)
                {
                    PlandGV.DataSource = dt;
                    PlandGV.Columns["guid"].Visible = false;
                    PlandGV.Columns["num"].Visible = false;
                    PlandGV.Columns["potype"].Visible = false;
                    //PlandGV.Columns["件数/箱"].Visible = false;
                   // PlandGV.Columns["完成箱数"].Visible = false;
                    PlandGV.Columns["数量"].Visible = false;
                    PlandGV.Columns["是否混色"].Visible = false;
                    PlandGV.Columns["Warehouse"].Visible = false;
                    PlandGV.Columns["WHDate"].Visible = false;
                    PlandGV.Columns["剩余件数"].Visible = false;
                    PlandGV.Columns["库存件数"].Visible = false;
                    PlandGV.Columns["占用箱数"].Visible = false;
                    PlandGV.Columns["完成比例"].Visible = false;
                    PlandGV.Columns["已选择"].Visible = false;
                    for (int i = 0; i < this.PlandGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.PlandGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                    for (int i = 0; i < PlandGV.Rows.Count; i++)
                    {
                        bool b_v = (bool)PlandGV.Rows[i].Cells["已选择"].Value;
                        if (b_v)
                        {
                            PlandGV.Rows[i].DefaultCellStyle.BackColor = Color.LimeGreen;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
        /// <summary>
        /// 排单计划表无条件查询
        /// </summary>
        public void PlanOherNewShow()
        {
            try
            {
                PlanNewShow(PotSpTBox.Text.Trim(), CktSpTBox.Text.Trim(), SetCodetSpTBox.Text.Trim(), SkutSpTBox.Text.Trim(), PoTypetSpCBox.Text.Trim(), IsSatisfytSpCBx.Text.Trim(), IsChecktSpCBox.Text.Trim());
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.Message);
            }
        }
        private void SchedulingTableForm_Load(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            PlanNewShow(PotSpTBox.Text.Trim(), CktSpTBox.Text.Trim(), SetCodetSpTBox.Text.Trim(), SkutSpTBox.Text.Trim(), PoTypetSpCBox.Text.Trim(), IsSatisfytSpCBx.Text.Trim(),IsChecktSpCBox.Text.Trim());
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            ProduceNewShow(PotSpTBox.Text.Trim(), CktSpTBox.Text.Trim(), SetCodetSpTBox.Text.Trim(), SkutSpTBox.Text.Trim());
        }

        private void tSpMe切换为当前_Click(object sender, EventArgs e)
        {
            try
            {
                if (ProducedGV.SelectedRows.Count < 1 || String.IsNullOrEmpty(ProducedGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim()))
                {
                    MessageBox.Show("请选择一行有效数据");
                    return;
                }
                bool isindex_v; int index_v;
                CsFormShow.DvgGetcurrentIndex(ProducedGV, out isindex_v, out index_v);
                if (isindex_v)
                {
                    Dictionary<string, string> dic_packing = new Dictionary<string, string>();
                    //DataTable dt_packing= CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", ProducedGV.Rows[index_v].Cells["po_guid"].Value.ToString()));
                    dic_packing["guid"] = ProducedGV.Rows[index_v].Cells["guid"].Value.ToString();
                    dic_packing["potype"] = ProducedGV.Rows[index_v].Cells["potype"].Value.ToString();
                    dic_packing["po_guid"] = ProducedGV.Rows[index_v].Cells["po_guid"].Value.ToString();
                    dic_packing["订单号码"] = ProducedGV.Rows[index_v].Cells["订单号码"].Value.ToString();
                    dic_packing["从"] = ProducedGV.Rows[index_v].Cells["从"].Value.ToString();
                    dic_packing["Set_Code"] = ProducedGV.Rows[index_v].Cells["Set_Code"].Value.ToString();
                    dic_packing["Warehouse"] = ProducedGV.Rows[index_v].Cells["Warehouse"].Value.ToString();
                    dic_packing["packingqty"] = ProducedGV.Rows[index_v].Cells["本次设置数量"].Value.ToString();
                    dic_packing["Long"] = ProducedGV.Rows[index_v].Cells["Long"].Value.ToString();
                    dic_packing["Width"] = ProducedGV.Rows[index_v].Cells["Width"].Value.ToString();
                    dic_packing["Heghit"] = ProducedGV.Rows[index_v].Cells["Heghit"].Value.ToString();
                    dic_packing["IsRepeatCheck"] = ProducedGV.Rows[index_v].Cells["IsRepeatCheck"].Value.ToString();
                    if (dic_packing["potype"].Contains("GAP"))
                    {
                        bool b_v = Chekin.IsChagePo("GAP", dic_packing);
                        if (b_v)
                        {
                            int Chose_v = CsFormShow.DvgGetChoseIndex(ProducedGV);
                            int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE DeviceName='{0}'",CSReadPlc.DevName));
                            if (count_v == 0)
                            {
                                Chose_v = -1;
                            }
                            else if (count_v>1)
                            {
                                CsFormShow.MessageBoxFormShow("生产中已经有两个订单，请等候第一个订单结束");
                                return;
                            }
                            bool IsOrverOrderNo = true;
                            if (Chose_v != -1)
                            {
                                IsOrverOrderNo = CsFormShow.OrverOrderNo(ProducedGV, Chose_v);
                            }
                            if (IsOrverOrderNo)
                            {
                                #region 扫描二维码
                                if (!dic_packing["IsRepeatCheck"].Contains("不运行"))
                                {
                                    if (ScanBatchNoform == null || ScanBatchNoform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                                    {
                                        ScanBatchNoform = new ScanBatchNoForm(ProducedGV.Rows[index_v].Cells["po_guid"].Value.ToString());
                                        ScanBatchNoform.Owner = this;
                                        ScanBatchNoform.StartPosition = FormStartPosition.CenterScreen;
                                        ScanBatchNoform.ShowDialog();
                                    }
                                    else
                                    {
                                        ScanBatchNoform.Show();
                                        ScanBatchNoform.StartPosition = FormStartPosition.CenterScreen;
                                        ScanBatchNoform.BringToFront();
                                    }
                                }
                                #endregion
                                bool b_= CsGetData.CuttingOrder(this.ProducedGV);
                                if (b_)
                                {
                                    ProduceNewShow();
                                    //this.Close();
                                    //this.Dispose();
                                }
                            }
                        }
                    }
                    else if (dic_packing["potype"].Contains("UNIQLO") || dic_packing["potype"].Contains("GU"))
                    {
                        bool b_v = Chekin.IsChagePo(dic_packing["potype"], dic_packing);
                        if (b_v)
                        {
                            int Chose_v = CsFormShow.DvgGetChoseIndex(ProducedGV);
                            int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.YYKMarkingLineUp WHERE DeviceName='{0}'",CSReadPlc.DevName));
                            if (count_v == 0)
                            {
                                Chose_v = -1;
                            }
                            else if (count_v > 1)
                            {
                                CsFormShow.MessageBoxFormShow("生产中已经有两个订单，请等候第一个订单结束");
                                return;
                            }
                            bool IsOrverOrderNo = true;
                            if (Chose_v != -1)
                            {
                                IsOrverOrderNo = CsFormShow.OrverOrderNo(ProducedGV, Chose_v);
                            }
                            if (IsOrverOrderNo)
                            {
                                #region 扫描二维码
                                if (!dic_packing["IsRepeatCheck"].Contains("不运行"))
                                {
                                    if (ScanBatchNoform == null || ScanBatchNoform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                                    {
                                        ScanBatchNoform = new ScanBatchNoForm(ProducedGV.Rows[index_v].Cells["po_guid"].Value.ToString());
                                        ScanBatchNoform.Owner = this;
                                        ScanBatchNoform.StartPosition = FormStartPosition.CenterScreen;
                                        ScanBatchNoform.ShowDialog();
                                    }
                                    else
                                    {
                                        ScanBatchNoform.Show();
                                        ScanBatchNoform.StartPosition = FormStartPosition.CenterScreen;
                                        ScanBatchNoform.BringToFront();
                                    }
                                }
                                #endregion
                                bool b_ = CsGetData.CuttingOrder(this.ProducedGV);
                                if (b_)
                                {
                                    ProduceNewShow();
                                    //this.Close();
                                    //this.Dispose();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void tSpMe置顶_Click(object sender, EventArgs e)
        {
            try
            {
                if (ProducedGV.SelectedRows.Count < 1 || String.IsNullOrEmpty(ProducedGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim()))
                {
                    MessageBox.Show("请选择一行有效数据");
                    return;
                }
                bool isindex_v; int index_v;
                CsFormShow.DvgGetcurrentIndex(ProducedGV, out isindex_v, out index_v);
                if (isindex_v)
                {
                    SqlParameter[] sqlparameter = new SqlParameter[1];
                    sqlparameter[0] = new SqlParameter("@po_guid", SqlDbType.NVarChar, 100);
                    sqlparameter[0].Value = ProducedGV.Rows[index_v].Cells["po_guid"].Value.ToString().Trim();
                    CsFormShow.GoProc_ParameterNoSelect(sqlparameter, "pm_rp_UpdateSort");
                    ProduceNewShow();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }

        private void tSpMe设置装箱参数_Click(object sender, EventArgs e)
        {
            try
            {
                if (ProducedGV.SelectedRows.Count < 1 || String.IsNullOrEmpty(ProducedGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim()))
                {
                    MessageBox.Show("请选择一行有效数据");
                    return;
                }
                bool isindex_v; int index_v;
                CsFormShow.DvgGetcurrentIndex(ProducedGV, out isindex_v, out index_v);
                if (isindex_v)
                {
                    Dictionary<string, string> dic_packing = new Dictionary<string, string>();
                    //DataTable dt_packing= CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", ProducedGV.Rows[index_v].Cells["po_guid"].Value.ToString()));
                    dic_packing["potype"] = ProducedGV.Rows[index_v].Cells["potype"].Value.ToString();
                    dic_packing["po_guid"] = ProducedGV.Rows[index_v].Cells["po_guid"].Value.ToString();
                    dic_packing["订单号码"] = ProducedGV.Rows[index_v].Cells["订单号码"].Value.ToString();
                    dic_packing["从"] = ProducedGV.Rows[index_v].Cells["从"].Value.ToString();
                    dic_packing["Set_Code"] = ProducedGV.Rows[index_v].Cells["Set_Code"].Value.ToString();
                    dic_packing["Warehouse"] = ProducedGV.Rows[index_v].Cells["Warehouse"].Value.ToString();
                    dic_packing["packingqty"] = ProducedGV.Rows[index_v].Cells["本次设置数量"].Value.ToString();
                    string Schedulinglong_v = ProducedGV.Rows[index_v].Cells["Long"].Value.ToString();
                    string Schedulingwidth_v = ProducedGV.Rows[index_v].Cells["Width"].Value.ToString();
                    string Schedulingheght_v = ProducedGV.Rows[index_v].Cells["Heghit"].Value.ToString();
                    string IsRepeatCheck_v = ProducedGV.Rows[index_v].Cells["IsRepeatCheck"].Value.ToString();
                    if (dic_packing["potype"].Contains("GAP"))
                    {
                        Dictionary<string, string> DicPaking = new Dictionary<string, string>();
                        DicPaking = CsGetData.GetCatornData(dic_packing["po_guid"]);
                        string id_v = DicPaking["装箱单guid"];
                        //string CatornStyle_guid_v= DicPaking["CatornStyle_guid"];
                        string long_v = ((int)(double.Parse(DicPaking["箱子长"].ToString().Trim()) * 10)).ToString();
                        string width_v = ((int)(double.Parse(DicPaking["箱子宽"].ToString().Trim()) * 10)).ToString();
                        string Heghit_v = ((int)(double.Parse(DicPaking["箱子高"].ToString().Trim()) * 10)).ToString();
                        if (isindex_v)
                        {
                            if (Schedulinglong_v == "0" && Schedulingwidth_v == "0" && Schedulingheght_v == "0")
                            {
                                if (setQtyform == null || setQtyform.IsDisposed)
                                {
                                    setQtyform = new SetQtyForm(DicPaking["装箱单guid"], DicPaking["箱子长"], DicPaking["箱子宽"], DicPaking["箱子高"], DicPaking["距离底部"], DicPaking["距离左边"], DicPaking["重量"], DicPaking["箱内件数"], DicPaking["喷码模式"], DicPaking["纸箱大小"], DicPaking["贴标类型"], DicPaking["隔板类型"], DicPaking["正反方向"], DicPaking["箱数"], ProducedGV, index_v, this, "箱数", M_stcPalletizingData.Palletizing_Unit.ToString());
                                    setQtyform.Owner = this;
                                    setQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                                    setQtyform.ShowDialog();
                                }
                                else
                                {
                                    setQtyform.ShowDialog();
                                    setQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                                    setQtyform.BringToFront();
                                }
                            }
                            else
                            {
                                if (setQtyform == null || setQtyform.IsDisposed)
                                {
                                    setQtyform = new SetQtyForm(this.ProducedGV, this);
                                    setQtyform.Owner = this;
                                    setQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                                    setQtyform.ShowDialog();
                                }
                                else
                                {
                                    setQtyform.ShowDialog();
                                    setQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                                    setQtyform.BringToFront();
                                }
                            }
                        }
                    }
                    else if (dic_packing["potype"].Contains("UNIQLO") || dic_packing["potype"].Contains("GU"))
                    {
                        if (Schedulinglong_v == "0" && Schedulingwidth_v == "0" && Schedulingheght_v == "0")
                        {
                            if (setQtyform == null || setQtyform.IsDisposed)
                            {
                                string GAPTiaoMa_v = "";
                                DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YOUYIKUDO WHERE guid='{0}'", ProducedGV.Rows[index_v].Cells["po_guid"].Value.ToString().Trim()));
                                int packingtotal_v = int.Parse(dt.Rows[0]["packingtotal"].ToString().Trim());
                                int total = int.Parse(dt.Rows[0]["Quantity"].ToString().Trim());
                                if (ProducedGV.Columns.Contains("TiaoMaVal"))
                                {
                                    GAPTiaoMa_v = dt.Rows[0]["TiaoMaVal"].ToString().Trim();
                                    int qty = total - packingtotal_v;
                                    setQtyform = new SetQtyForm(qty.ToString(), this, "0", GAPTiaoMa_v);
                                    setQtyform.Owner = this;
                                    setQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                                    setQtyform.ShowDialog();
                                }
                                else
                                {
                                    int qty = total - packingtotal_v;
                                    setQtyform = new SetQtyForm(qty.ToString(), this, "0", GAPTiaoMa_v, index_v, ProducedGV);
                                    setQtyform.Owner = this;
                                    setQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                                    setQtyform.ShowDialog();
                                }
                            }
                            else
                            {
                                setQtyform.ShowDialog();
                                setQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                                setQtyform.BringToFront();
                            }
                        }
                        else
                        {
                            if (setQtyform == null || setQtyform.IsDisposed)
                            {
                                setQtyform = new SetQtyForm(this.ProducedGV, this);
                                setQtyform.Owner = this;
                                setQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                                setQtyform.ShowDialog();
                            }
                            else
                            {
                                setQtyform.ShowDialog();
                                setQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                                setQtyform.BringToFront();
                            }
                        }

                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void tSpMe强制结束_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult btchose = MessageBox.Show("是否强制结束当前订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    bool isindex_v; int index_v; bool b_v;
                    CsFormShow.DvgGetcurrentIndex(this.ProducedGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        string potype = ProducedGV.Rows[index_v].Cells["potype"].Value.ToString().Trim();
                        if (potype.Contains("GU") || potype.Contains("UNIQLO"))
                        {
                            b_v = CsGetData.YYKGUCompelChange(ProducedGV.Rows[index_v].Cells["订单号码"].Value.ToString(), ProducedGV.Rows[index_v].Cells["Warehouse"].Value.ToString(), ProducedGV.Rows[index_v].Cells["Set_Code"].Value.ToString());
                        }
                        else
                        {
                            b_v = CsGetData.GAPCompelChange(ProducedGV.Rows[index_v].Cells["订单号码"].Value.ToString(), ProducedGV.Rows[index_v].Cells["从"].Value.ToString());
                        }
                        if (!b_v)
                        {
                            return;
                        }
                        MessageBox.Show("已结束当前订单");
                        ProduceNewShow();
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult btchose = MessageBox.Show("是否强制结束当前订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    bool isindex_v; int index_v; bool b_v;
                    CsFormShow.DvgGetcurrentIndex(this.ProducedGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        string potype = ProducedGV.Rows[index_v].Cells["potype"].Value.ToString().Trim();
                        if (potype.Contains("GU") || potype.Contains("UNIQLO"))
                        {
                            b_v = CsGetData.YYKGUUnusualCompelChange(ProducedGV.Rows[index_v].Cells["订单号码"].Value.ToString(), ProducedGV.Rows[index_v].Cells["Warehouse"].Value.ToString(), ProducedGV.Rows[index_v].Cells["Set_Code"].Value.ToString());
                        }
                        else
                        {
                            b_v = CsGetData.GAPUnusualCompelChange(ProducedGV.Rows[index_v].Cells["订单号码"].Value.ToString(), ProducedGV.Rows[index_v].Cells["从"].Value.ToString());
                        }
                        if (!b_v)
                        {
                            return;
                        }
                        MessageBox.Show("已结束当前订单");
                        ProduceNewShow();
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 移除多条数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (ProducedGV.SelectedRows.Count == 0)
                {
                    MessageBox.Show("请选择有效订单数据！！");
                    return;
                }
                for (int i = 0; i < ProducedGV.SelectedRows.Count; i++)
                {
                    bool b_v = (bool)ProducedGV.SelectedRows[i].Cells["已选择"].Value;
                    if (b_v)
                    {
                        MessageBox.Show("请勿选择生产中的订单数据！！");
                        return;
                    }
                }
                Delet_click.Delete_TSMenuItem_Click(this, ProducedGV, "dbo.SchedulingTable");
                ProduceNewShow();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void tSMe加入生产队列_Click(object sender, EventArgs e)
        {
            try
            {
                //bool boolval = CsFormShow.DvgRowSetcurrent(PlandGV);
                //if (!boolval)
                //    return;
                if (PlandGV.SelectedRows.Count > 1 || String.IsNullOrEmpty(PlandGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim()))
                {
                    MessageBox.Show("请选择一行有效数据");
                    return;
                }
                bool b_v = CsFormShow.IsExistenceInSql("dbo.SchedulingTable", "po_guid", PlandGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim());
                if (b_v)
                {
                    MessageBox.Show("此订单已经在排单列表中");
                    return;
                }
                string potype_v = PlandGV.SelectedRows[0].Cells["potype"].Value.ToString().Trim();
                if (potype_v.Contains("GAP"))
                {
                    // string guid_v = PlandGV.SelectedRows[0].Cells["guid"].Value.ToString();
                    Dictionary<string, string> DicPaking = new Dictionary<string, string>();
                    DicPaking = CsGetData.GetCatornData(PlandGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim());
                    string id_v = DicPaking["装箱单guid"];
                    //string CatornStyle_guid_v= DicPaking["CatornStyle_guid"];
                    string long_v = ((int)(double.Parse(DicPaking["箱子长"].ToString().Trim()) * 10)).ToString();
                    string width_v = ((int)(double.Parse(DicPaking["箱子宽"].ToString().Trim()) * 10)).ToString();
                    string Heghit_v = ((int)(double.Parse(DicPaking["箱子高"].ToString().Trim()) * 10)).ToString();
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(PlandGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        if (setQtyform == null || setQtyform.IsDisposed)
                        {
                            setQtyform = new SetQtyForm(DicPaking["装箱单guid"], DicPaking["箱子长"], DicPaking["箱子宽"], DicPaking["箱子高"], DicPaking["距离底部"], DicPaking["距离左边"], DicPaking["重量"], DicPaking["箱内件数"], DicPaking["喷码模式"], DicPaking["纸箱大小"], DicPaking["贴标类型"], DicPaking["隔板类型"], DicPaking["正反方向"], DicPaking["箱数"], PlandGV, index_v, this, "箱数", M_stcPalletizingData.Palletizing_Unit.ToString());
                            setQtyform.Owner = this;
                            setQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                            setQtyform.ShowDialog();
                        }
                        else
                        {
                            setQtyform.ShowDialog();
                            setQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                            setQtyform.BringToFront();
                        }
                    }
                }
                else if (potype_v.Contains("UNIQLO") || potype_v.Contains("GU"))
                {
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(PlandGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        //判断是否有混色号同类型在队列中
                        string sql_isexist = string.Format("SELECT*FROM dbo.SchedulingTable AS a WHERE 订单号码='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", PlandGV.Rows[index_v].Cells["Order_No"].Value.ToString(), PlandGV.Rows[index_v].Cells["Warehouse"].Value.ToString(), PlandGV.Rows[index_v].Cells["Set_Code"].Value.ToString());
                        DataTable dt = CsFormShow.GoSqlSelect(sql_isexist);
                        if (dt.Rows.Count > 0)
                        {
                            MessageBox.Show("混色号同类型已经在排单队列中，请到排单表中直接换单");
                            return;
                        }
                        #region 设置当前订单
                        DialogResult btchose = MessageBox.Show("是否将订单设置为当前", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (btchose == DialogResult.OK)
                        {
                            CsFormShow.DvgGetcurrentIndex(PlandGV, out isindex_v, out index_v);
                            //string strsql = string.Format(" UPDATE dbo.YOUYIKUDO SET CatornStyle_guid = '{0}' WHERE Order_No = '{1}'  AND Set_Code = '{2}'AND Warehouse = '{3}'", Position_guid, YouyikudGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), YouyikudGV.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim(), YouyikudGV.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim());
                            //content1.Select_nothing(strsql);
                            //判断是否设置装箱数量
                            CsFormShow csformShow = new CsFormShow();
                            csformShow.SetPackingQty(index_v, PlandGV, this, "Quantity", "0");
                        }
                        else
                        {
                            PlanNewShow();
                        }
                        #endregion
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void SchedulingtabCtr_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SchedulingtabCtr.SelectedIndex == 1)//也可以判断tabControl1.SelectedTab.Text的值
            {

                ProduceNewShow(PotSpTBox.Text.Trim(), CktSpTBox.Text.Trim(), SetCodetSpTBox.Text.Trim(), SkutSpTBox.Text.Trim());
                //执行相应的操作

            }
            else if (SchedulingtabCtr.SelectedIndex == 0)
            {
                PlanNewShow(PotSpTBox.Text.Trim(), CktSpTBox.Text.Trim(), SetCodetSpTBox.Text.Trim(), SkutSpTBox.Text.Trim(), PoTypetSpCBox.Text.Trim(), IsSatisfytSpCBx.Text.Trim(), IsChecktSpCBox.Text.Trim());
            }
        }

        private void 结束订单ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return;
                }
                DialogResult btchose = MessageBox.Show("是否终止订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    if (PlandGV.Rows.Count > 0 && PlandGV.SelectedRows.Count > 0)
                    {

                        bool isindex_v; int index_v;
                        CsFormShow.DvgGetcurrentIndex(PlandGV, out isindex_v, out index_v);
                        if (isindex_v)
                        {
                            string strpo = PlandGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim();
                            string startcode = PlandGV.Rows[index_v].Cells["从"].Value.ToString().Trim();
                            string sql1 = string.Format("UPDATE dbo.PACKING SET ENDPO=1,EndTime='{1}' WHERE guid='{0}'", PlandGV.Rows[index_v].Cells["guid"].Value.ToString().Trim(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            CsFormShow.GoSqlUpdateInsert(sql1);
                            string sql2 = string.Format("UPDATE dbo.TMOM SET ENDPO=1 WHERE guid='{0}'", PlandGV.Rows[index_v].Cells["guid"].Value.ToString().Trim());
                            CsFormShow.GoSqlUpdateInsert(sql2);
                        }
                    }
                    else
                    {
                        MessageBox.Show("提示：请选择有效订单");
                        return;
                    }
                    PlanNewShow();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 扫码获取当前卷号
        /// </summary>
        private void GetBatchNo(int index_)
        {
            try
            {
                if (ScanBatchNoform == null || ScanBatchNoform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                {
                    ScanBatchNoform = new ScanBatchNoForm(ProducedGV.Rows[index_].Cells["po_guid"].Value.ToString());
                    ScanBatchNoform.Owner = this;
                    ScanBatchNoform.StartPosition = FormStartPosition.CenterScreen;
                    ScanBatchNoform.ShowDialog();
                }
                else
                {
                    ScanBatchNoform.Show();
                    ScanBatchNoform.StartPosition = FormStartPosition.CenterScreen;
                    ScanBatchNoform.BringToFront();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }

        private void 更新卷号ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (ProducedGV.SelectedRows.Count < 1 || String.IsNullOrEmpty(ProducedGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim()))
                {
                    MessageBox.Show("请选择一行有效数据");
                    return;
                }
                bool isindex_v; int index_v;
                CsFormShow.DvgGetcurrentIndex(ProducedGV, out isindex_v, out index_v);
                if (isindex_v)
                {
                    if (ScanBatchNoform == null || ScanBatchNoform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                    {
                        ScanBatchNoform = new ScanBatchNoForm(ProducedGV.Rows[index_v].Cells["po_guid"].Value.ToString());
                        ScanBatchNoform.Owner = this;
                        ScanBatchNoform.StartPosition = FormStartPosition.CenterScreen;
                        ScanBatchNoform.ShowDialog();
                    }
                    else
                    {
                        ScanBatchNoform.Show();
                        ScanBatchNoform.StartPosition = FormStartPosition.CenterScreen;
                        ScanBatchNoform.BringToFront();
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
        private void 刷新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProduceNewShow();
        }

        private void NewtSpBtn_Click(object sender, EventArgs e)
        {
            ProduceNewShow();
        }
    }
}
