﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WindowsForms01
{
    public partial class ScanShowForm : Form
    {
        List<M_stc_WriteToPlcSku> list_v = new List<M_stc_WriteToPlcSku>();
        string ScanFlag = "";
        string Last_TiaoMa = "";
        Qty_per_setForm qty_Per_Setform = null;
        public ScanShowForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            CsPublicVariablies.Scan.AllowScanSignal = "stop";
        }
        private void ScanShowForm_Load(object sender, EventArgs e)
        {

        }
        void Scan()
        {
            try
            {
                while (true)
                {
                    if (ScanFlag == "start")
                    {
                        if (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now) && Last_TiaoMa != CsScanning.TiaoMaVal_Now &&!CsScanning.TiaoMaVal_Now.Contains("RROR") && !CsScanning.TiaoMaVal_Now.Contains("0"))
                        {
                            if ((CsScanning.TiaoMaVal_Now.Contains(Last_TiaoMa)|| Last_TiaoMa.Contains(CsScanning.TiaoMaVal_Now))&& !string.IsNullOrEmpty(Last_TiaoMa))
                            {
                                continue;
                            }
                            M_stc_WriteToPlcSku m_Stc_ = new M_stc_WriteToPlcSku();
                            Last_TiaoMa = CsScanning.TiaoMaVal_Now;
                            ScanlsBox.Items.Add(CsScanning.TiaoMaVal_Now);
                            int count_v = list_v.Count;
                            switch (count_v)
                            {
                                case 0:
                                    if (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now))
                                    {
                                        m_Stc_.dic_sku["D5100"] = CsScanning.TiaoMaVal_Now;
                                        CsFormShow.FormShowDialog(qty_Per_Setform, this, "WindowsForms01.Qty_per_setForm");
                                        Thread.Sleep(100);
                                        m_Stc_.dic_Write_Layernum["D5060"] = CsPublicVariablies.Scan.Qty_per_set.ToString();
                                        list_v.Add(m_Stc_);
                                    }
                                    break;
                                case 1:
                                    if (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now))
                                    {
                                        m_Stc_.dic_sku["D5120"] = CsScanning.TiaoMaVal_Now;
                                        CsFormShow.FormShowDialog(qty_Per_Setform, this, "WindowsForms01.Qty_per_setForm");
                                        Thread.Sleep(100);
                                        m_Stc_.dic_Write_Layernum["D5061"] = CsPublicVariablies.Scan.Qty_per_set.ToString();
                                        list_v.Add(m_Stc_);
                                    }
                                    break;
                                case 2:
                                    if (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now))
                                    {
                                        m_Stc_.dic_sku["D5140"] = CsScanning.TiaoMaVal_Now;
                                        CsFormShow.FormShowDialog(qty_Per_Setform, this, "WindowsForms01.Qty_per_setForm");
                                        Thread.Sleep(100);
                                        m_Stc_.dic_Write_Layernum["D5062"] = CsPublicVariablies.Scan.Qty_per_set.ToString();
                                        list_v.Add(m_Stc_);
                                    }
                                    break;
                                case 3:
                                    if (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now))
                                    {
                                        m_Stc_.dic_sku["D5160"] = CsScanning.TiaoMaVal_Now;
                                        CsFormShow.FormShowDialog(qty_Per_Setform, this, "WindowsForms01.Qty_per_setForm");
                                        Thread.Sleep(100);
                                        m_Stc_.dic_Write_Layernum["D5063"] = CsPublicVariablies.Scan.Qty_per_set.ToString();
                                        list_v.Add(m_Stc_);
                                    }
                                    break;
                                case 4:
                                    if (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now))
                                    {
                                        m_Stc_.dic_sku["D5180"] = CsScanning.TiaoMaVal_Now;
                                        CsFormShow.FormShowDialog(qty_Per_Setform, this, "WindowsForms01.Qty_per_setForm");
                                        Thread.Sleep(100);
                                        m_Stc_.dic_Write_Layernum["D5064"] = CsPublicVariablies.Scan.Qty_per_set.ToString();
                                        list_v.Add(m_Stc_);
                                    }
                                    break;
                                case 5:
                                    if (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now))
                                    {
                                        m_Stc_.dic_sku["D5200"] = CsScanning.TiaoMaVal_Now;
                                        CsFormShow.FormShowDialog(qty_Per_Setform, this, "WindowsForms01.Qty_per_setForm");
                                        Thread.Sleep(100);
                                        m_Stc_.dic_Write_Layernum["D5065"] = CsPublicVariablies.Scan.Qty_per_set.ToString();
                                        list_v.Add(m_Stc_);
                                    }
                                    break;
                                case 6:
                                    if (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now))
                                    {
                                        m_Stc_.dic_sku["D5220"] = CsScanning.TiaoMaVal_Now;
                                        CsFormShow.FormShowDialog(qty_Per_Setform, this, "WindowsForms01.Qty_per_setForm");
                                        Thread.Sleep(100);
                                        m_Stc_.dic_Write_Layernum["D5066"] = CsPublicVariablies.Scan.Qty_per_set.ToString();
                                        list_v.Add(m_Stc_);
                                    }
                                    break;
                                case 7:
                                    if (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now))
                                    {
                                        m_Stc_.dic_sku["D5240"] = CsScanning.TiaoMaVal_Now;
                                        CsFormShow.FormShowDialog(qty_Per_Setform, this, "WindowsForms01.Qty_per_setForm");
                                        Thread.Sleep(100);
                                        m_Stc_.dic_Write_Layernum["D5067"] = CsPublicVariablies.Scan.Qty_per_set.ToString();
                                        list_v.Add(m_Stc_);
                                    }
                                    break;
                                case 8:
                                    if (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now))
                                    {
                                        m_Stc_.dic_sku["D5260"] = CsScanning.TiaoMaVal_Now;
                                        CsFormShow.FormShowDialog(qty_Per_Setform, this, "WindowsForms01.Qty_per_setForm");
                                        Thread.Sleep(100);
                                        m_Stc_.dic_Write_Layernum["D5068"] = CsPublicVariablies.Scan.Qty_per_set.ToString();
                                        list_v.Add(m_Stc_);
                                    }
                                    break;
                                case 9:
                                    if (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now))
                                    {
                                        m_Stc_.dic_sku["D5280"] = CsScanning.TiaoMaVal_Now;
                                        CsFormShow.FormShowDialog(qty_Per_Setform, this, "WindowsForms01.Qty_per_setForm");
                                        Thread.Sleep(100);
                                        m_Stc_.dic_Write_Layernum["D5069"] = CsPublicVariablies.Scan.Qty_per_set.ToString();
                                        list_v.Add(m_Stc_);
                                    }
                                    break;
                                default: break;
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }

        private void StartScanbtn_Click(object sender, EventArgs e)
        {
            try
            {
                ScanlsBox.Items.Clear();
                list_v.Clear();
                CsPublicVariablies.Scan.AllowScanSignal = "start";
                TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                while (CsScanning.AllowScanSignal!="1")
                {
                    TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                    TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                    if (ts.Seconds > 5)
                    {
                        break;
                    }
                }
                CsPublicVariablies.Scan.ClearNowTiaoMaFlag = "start";
                timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                while (!string.IsNullOrEmpty(CsScanning.TiaoMaVal_Now))
                {
                    TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                    TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                    if (ts.Seconds > 5)
                    {
                        break;
                    }
                }
                ScanFlag = "start";
                Thread Th_Scan = new Thread(Scan);
                Th_Scan.IsBackground = true;
                Th_Scan.Start();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
            Last_TiaoMa = "";
        }
        private void EndScanbtn_Click(object sender, EventArgs e)
        {
            M_stcScanData.M_List_WriteToPlcSku = list_v;
            M_stcScanData.M_WriteToPlcSku = "start";
            CsScanning.TiaoMaVal_Now = "";
            CsPublicVariablies.Scan.AllowScanSignal ="stop";
            MessageBox.Show("操作完毕");
        }
    }
}
