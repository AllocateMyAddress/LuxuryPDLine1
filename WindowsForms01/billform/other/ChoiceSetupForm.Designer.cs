﻿namespace WindowsForms01
{
    partial class ShieldForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.YamahackBox = new System.Windows.Forms.CheckBox();
            this.MarkingckBox = new System.Windows.Forms.CheckBox();
            this.PrintckBox = new System.Windows.Forms.CheckBox();
            this.okbtn = new System.Windows.Forms.Button();
            this.SmallLabelckBox = new System.Windows.Forms.CheckBox();
            this.SerialPortcBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // YamahackBox
            // 
            this.YamahackBox.AutoSize = true;
            this.YamahackBox.Location = new System.Drawing.Point(22, 30);
            this.YamahackBox.Name = "YamahackBox";
            this.YamahackBox.Size = new System.Drawing.Size(74, 19);
            this.YamahackBox.TabIndex = 0;
            this.YamahackBox.Text = "机器人";
            this.YamahackBox.UseVisualStyleBackColor = true;
            // 
            // MarkingckBox
            // 
            this.MarkingckBox.AutoSize = true;
            this.MarkingckBox.Location = new System.Drawing.Point(102, 30);
            this.MarkingckBox.Name = "MarkingckBox";
            this.MarkingckBox.Size = new System.Drawing.Size(74, 19);
            this.MarkingckBox.TabIndex = 1;
            this.MarkingckBox.Text = "喷码机";
            this.MarkingckBox.UseVisualStyleBackColor = true;
            // 
            // PrintckBox
            // 
            this.PrintckBox.AutoSize = true;
            this.PrintckBox.Location = new System.Drawing.Point(182, 30);
            this.PrintckBox.Name = "PrintckBox";
            this.PrintckBox.Size = new System.Drawing.Size(158, 19);
            this.PrintckBox.TabIndex = 2;
            this.PrintckBox.Text = "GAP拐角标签打印机";
            this.PrintckBox.UseVisualStyleBackColor = true;
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(768, 15);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(91, 49);
            this.okbtn.TabIndex = 3;
            this.okbtn.Text = "确定";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.okbtn_Click);
            // 
            // SmallLabelckBox
            // 
            this.SmallLabelckBox.AutoSize = true;
            this.SmallLabelckBox.Location = new System.Drawing.Point(359, 31);
            this.SmallLabelckBox.Name = "SmallLabelckBox";
            this.SmallLabelckBox.Size = new System.Drawing.Size(158, 19);
            this.SmallLabelckBox.TabIndex = 4;
            this.SmallLabelckBox.Text = "GAP网单标签打印机";
            this.SmallLabelckBox.UseVisualStyleBackColor = true;
            // 
            // SerialPortcBox
            // 
            this.SerialPortcBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SerialPortcBox.FormattingEnabled = true;
            this.SerialPortcBox.Location = new System.Drawing.Point(523, 28);
            this.SerialPortcBox.Name = "SerialPortcBox";
            this.SerialPortcBox.Size = new System.Drawing.Size(121, 23);
            this.SerialPortcBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(659, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "端口号";
            // 
            // ShieldForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 82);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SerialPortcBox);
            this.Controls.Add(this.SmallLabelckBox);
            this.Controls.Add(this.okbtn);
            this.Controls.Add(this.PrintckBox);
            this.Controls.Add(this.MarkingckBox);
            this.Controls.Add(this.YamahackBox);
            this.Name = "ShieldForm";
            this.Text = "选择设备启动";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ShieldForm_FormClosed_1);
            this.Load += new System.EventHandler(this.ShieldForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox YamahackBox;
        private System.Windows.Forms.CheckBox MarkingckBox;
        private System.Windows.Forms.CheckBox PrintckBox;
        private System.Windows.Forms.Button okbtn;
        private System.Windows.Forms.CheckBox SmallLabelckBox;
        private System.Windows.Forms.ComboBox SerialPortcBox;
        private System.Windows.Forms.Label label1;
    }
}