﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class ProducingForm : Form
    {
        public ProducingForm()
        {
            InitializeComponent();
            NewShow();
        }

        private void ProducingForm_Load(object sender, EventArgs e)
        {

        }
        public void NewShow()
        {
            try
            {
                ProcducingdGV.DataSource = null;
                DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM (SELECT ROW_NUMBER() OVER (ORDER BY  CASE WHEN  已选择=0 THEN 1 WHEN 已选择=1 THEN 0 END,sort) AS 序号, *FROM    (SELECT     (SELECT CASE WHEN COUNT(0) <> 0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END FROM(SELECT YK_guid AS po_guid, DeviceName FROM dbo.YYKMarkingLineUp UNION ALL SELECT packing_guid AS po_guid, DeviceName FROM dbo.MarkingLineUp) AS line WHERE  po_guid = dbo.SchedulingTable.po_guid) AS '已选择', sort, guid, po_guid, 订单号码, 从, Warehouse,SUBSTRING(Warehouse,0,5) AS '仓库代码', Set_Code, Color, packingqty AS '本次设置数量', dbo.SchedulingTable.potype, dbo.SchedulingTable.DeviceName, CatornStyle_guid, Long, Width, Heghit, TiaoMaVal ,TiaoMaVal AS '条形码',potype AS '品牌', (SELECT BatchNo FROM(SELECT YK_guid AS po_guid,BatchNo, DeviceName FROM dbo.YYKMarkingLineUp UNION ALL SELECT packing_guid AS po_guid,BatchNo, DeviceName FROM dbo.MarkingLineUp) AS line WHERE  po_guid = dbo.SchedulingTable.po_guid) AS '卷号' FROM dbo.SchedulingTable ) AS a  ) AS b WHERE b.已选择='1' AND b.DeviceName='{0}'", CSReadPlc.DevName));
                if (dt.Rows.Count>0)
                {
                    ProcducingdGV.DataSource = dt;
                    this.ProcducingdGV.Columns["guid"].Visible = false;
                    this.ProcducingdGV.Columns["po_guid"].Visible = false;
                    this.ProcducingdGV.Columns["DeviceName"].Visible = false;
                    this.ProcducingdGV.Columns["CatornStyle_guid"].Visible = false;
                    this.ProcducingdGV.Columns["Long"].Visible = false;
                    this.ProcducingdGV.Columns["Width"].Visible = false;
                    this.ProcducingdGV.Columns["Heghit"].Visible = false;
                    this.ProcducingdGV.Columns["sort"].Visible = false;
                    this.ProcducingdGV.Columns["potype"].Visible = false;
                    this.ProcducingdGV.Columns["TiaoMaVal"].Visible = false;
                    this.ProcducingdGV.Columns["Warehouse"].Visible = false;
                    this.ProcducingdGV.Columns["已选择"].Visible = false;
                    CsFormShow.SetDvgRowHeight(this.ProcducingdGV, 45, 45, true, Color.LightSteelBlue, this);
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.ToString());
            }
        }
    }
}
