﻿
namespace WindowsForms01
{
    partial class SchedulingTableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SchedulingTableForm));
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel9 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.PlantabPage = new System.Windows.Forms.TabPage();
            this.PlandGV = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tSMe加入生产队列 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.结束订单ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.PotSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.SetCodetSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.CktSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.SkutSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.PoTypetSpCBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel10 = new System.Windows.Forms.ToolStripLabel();
            this.IsSatisfytSpCBx = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel11 = new System.Windows.Forms.ToolStripLabel();
            this.IsChecktSpCBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBox4 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel8 = new System.Windows.Forms.ToolStripLabel();
            this.SchedulingtabCtr = new System.Windows.Forms.TabControl();
            this.ProducetabPage = new System.Windows.Forms.TabPage();
            this.ProducedGV = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tSpMe切换为当前 = new System.Windows.Forms.ToolStripMenuItem();
            this.切换为当前tSpStor = new System.Windows.Forms.ToolStripSeparator();
            this.tSpMe置顶 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.tSpMe设置装箱参数 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.tSpMe强制结束 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.移除多条数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.更新卷号ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.刷新ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel6 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel7 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBox3 = new System.Windows.Forms.ToolStripTextBox();
            this.NewtSpBtn = new System.Windows.Forms.ToolStripButton();
            this.PlantabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlandGV)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SchedulingtabCtr.SuspendLayout();
            this.ProducetabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProducedGV)).BeginInit();
            this.contextMenuStrip2.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(180, 6);
            // 
            // toolStripLabel9
            // 
            this.toolStripLabel9.Name = "toolStripLabel9";
            this.toolStripLabel9.Size = new System.Drawing.Size(69, 83);
            this.toolStripLabel9.Text = "SKU号码";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(150, 71);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.AutoSize = false;
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(120, 83);
            this.toolStripButton2.Text = "查询";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // PlantabPage
            // 
            this.PlantabPage.Controls.Add(this.PlandGV);
            this.PlantabPage.Controls.Add(this.toolStrip1);
            this.PlantabPage.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PlantabPage.Location = new System.Drawing.Point(4, 25);
            this.PlantabPage.Name = "PlantabPage";
            this.PlantabPage.Padding = new System.Windows.Forms.Padding(3);
            this.PlantabPage.Size = new System.Drawing.Size(1709, 453);
            this.PlantabPage.TabIndex = 0;
            this.PlantabPage.Text = "排单计划表";
            this.PlantabPage.UseVisualStyleBackColor = true;
            // 
            // PlandGV
            // 
            this.PlandGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PlandGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PlandGV.ContextMenuStrip = this.contextMenuStrip1;
            this.PlandGV.Location = new System.Drawing.Point(-1, 82);
            this.PlandGV.Name = "PlandGV";
            this.PlandGV.RowHeadersWidth = 51;
            this.PlandGV.RowTemplate.Height = 27;
            this.PlandGV.Size = new System.Drawing.Size(1710, 371);
            this.PlandGV.TabIndex = 11;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.tSMe加入生产队列,
            this.toolStripMenuItem1,
            this.结束订单ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(184, 64);
            // 
            // tSMe加入生产队列
            // 
            this.tSMe加入生产队列.Name = "tSMe加入生产队列";
            this.tSMe加入生产队列.Size = new System.Drawing.Size(183, 24);
            this.tSMe加入生产队列.Text = "加入生产队列表";
            this.tSMe加入生产队列.Click += new System.EventHandler(this.tSMe加入生产队列_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(180, 6);
            // 
            // 结束订单ToolStripMenuItem
            // 
            this.结束订单ToolStripMenuItem.Name = "结束订单ToolStripMenuItem";
            this.结束订单ToolStripMenuItem.Size = new System.Drawing.Size(183, 24);
            this.结束订单ToolStripMenuItem.Text = "结束订单";
            this.结束订单ToolStripMenuItem.Click += new System.EventHandler(this.结束订单ToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Silver;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.PotSpTBox,
            this.toolStripLabel3,
            this.SetCodetSpTBox,
            this.toolStripLabel2,
            this.CktSpTBox,
            this.toolStripLabel4,
            this.SkutSpTBox,
            this.toolStripLabel5,
            this.PoTypetSpCBox,
            this.toolStripLabel10,
            this.IsSatisfytSpCBx,
            this.toolStripLabel11,
            this.IsChecktSpCBox,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(-1, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1714, 83);
            this.toolStrip1.TabIndex = 10;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(81, 80);
            this.toolStripLabel1.Text = "Order_No";
            // 
            // PotSpTBox
            // 
            this.PotSpTBox.AutoSize = false;
            this.PotSpTBox.Name = "PotSpTBox";
            this.PotSpTBox.Size = new System.Drawing.Size(150, 52);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(76, 80);
            this.toolStripLabel3.Text = "Set Code";
            // 
            // SetCodetSpTBox
            // 
            this.SetCodetSpTBox.AutoSize = false;
            this.SetCodetSpTBox.Name = "SetCodetSpTBox";
            this.SetCodetSpTBox.Size = new System.Drawing.Size(100, 52);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(91, 80);
            this.toolStripLabel2.Text = "Warehouse";
            // 
            // CktSpTBox
            // 
            this.CktSpTBox.AutoSize = false;
            this.CktSpTBox.Name = "CktSpTBox";
            this.CktSpTBox.Size = new System.Drawing.Size(150, 52);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(69, 80);
            this.toolStripLabel4.Text = "SKU号码";
            // 
            // SkutSpTBox
            // 
            this.SkutSpTBox.AutoSize = false;
            this.SkutSpTBox.Name = "SkutSpTBox";
            this.SkutSpTBox.Size = new System.Drawing.Size(150, 71);
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(69, 80);
            this.toolStripLabel5.Text = "品牌类型";
            // 
            // PoTypetSpCBox
            // 
            this.PoTypetSpCBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PoTypetSpCBox.Name = "PoTypetSpCBox";
            this.PoTypetSpCBox.Size = new System.Drawing.Size(121, 83);
            // 
            // toolStripLabel10
            // 
            this.toolStripLabel10.Name = "toolStripLabel10";
            this.toolStripLabel10.Size = new System.Drawing.Size(99, 80);
            this.toolStripLabel10.Text = "是否满足订单";
            // 
            // IsSatisfytSpCBx
            // 
            this.IsSatisfytSpCBx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsSatisfytSpCBx.Name = "IsSatisfytSpCBx";
            this.IsSatisfytSpCBx.Size = new System.Drawing.Size(165, 83);
            // 
            // toolStripLabel11
            // 
            this.toolStripLabel11.Name = "toolStripLabel11";
            this.toolStripLabel11.Size = new System.Drawing.Size(84, 80);
            this.toolStripLabel11.Text = "是否可抽检";
            // 
            // IsChecktSpCBox
            // 
            this.IsChecktSpCBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsChecktSpCBox.Name = "IsChecktSpCBox";
            this.IsChecktSpCBox.Size = new System.Drawing.Size(121, 83);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.AutoSize = false;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(100, 49);
            this.toolStripButton1.Text = "查询";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripTextBox4
            // 
            this.toolStripTextBox4.AutoSize = false;
            this.toolStripTextBox4.Name = "toolStripTextBox4";
            this.toolStripTextBox4.Size = new System.Drawing.Size(150, 52);
            // 
            // toolStripLabel8
            // 
            this.toolStripLabel8.Name = "toolStripLabel8";
            this.toolStripLabel8.Size = new System.Drawing.Size(161, 83);
            this.toolStripLabel8.Text = "Warehouse(仓库代码)";
            // 
            // SchedulingtabCtr
            // 
            this.SchedulingtabCtr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SchedulingtabCtr.Controls.Add(this.PlantabPage);
            this.SchedulingtabCtr.Controls.Add(this.ProducetabPage);
            this.SchedulingtabCtr.Location = new System.Drawing.Point(2, 12);
            this.SchedulingtabCtr.Name = "SchedulingtabCtr";
            this.SchedulingtabCtr.SelectedIndex = 0;
            this.SchedulingtabCtr.Size = new System.Drawing.Size(1717, 482);
            this.SchedulingtabCtr.TabIndex = 2;
            this.SchedulingtabCtr.SelectedIndexChanged += new System.EventHandler(this.SchedulingtabCtr_SelectedIndexChanged);
            // 
            // ProducetabPage
            // 
            this.ProducetabPage.Controls.Add(this.ProducedGV);
            this.ProducetabPage.Controls.Add(this.toolStrip2);
            this.ProducetabPage.Location = new System.Drawing.Point(4, 25);
            this.ProducetabPage.Name = "ProducetabPage";
            this.ProducetabPage.Padding = new System.Windows.Forms.Padding(3);
            this.ProducetabPage.Size = new System.Drawing.Size(1709, 453);
            this.ProducetabPage.TabIndex = 1;
            this.ProducetabPage.Text = "生产排单表";
            this.ProducetabPage.UseVisualStyleBackColor = true;
            // 
            // ProducedGV
            // 
            this.ProducedGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProducedGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProducedGV.ContextMenuStrip = this.contextMenuStrip2;
            this.ProducedGV.Location = new System.Drawing.Point(0, 85);
            this.ProducedGV.Name = "ProducedGV";
            this.ProducedGV.RowHeadersWidth = 51;
            this.ProducedGV.RowTemplate.Height = 27;
            this.ProducedGV.Size = new System.Drawing.Size(1713, 368);
            this.ProducedGV.TabIndex = 13;
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator2,
            this.tSpMe切换为当前,
            this.切换为当前tSpStor,
            this.tSpMe置顶,
            this.toolStripMenuItem4,
            this.tSpMe设置装箱参数,
            this.toolStripMenuItem6,
            this.tSpMe强制结束,
            this.toolStripMenuItem5,
            this.删除ToolStripMenuItem,
            this.toolStripMenuItem2,
            this.移除多条数据ToolStripMenuItem,
            this.toolStripMenuItem3,
            this.更新卷号ToolStripMenuItem,
            this.toolStripMenuItem7,
            this.刷新ToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip1";
            this.contextMenuStrip2.Size = new System.Drawing.Size(169, 244);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(165, 6);
            // 
            // tSpMe切换为当前
            // 
            this.tSpMe切换为当前.Name = "tSpMe切换为当前";
            this.tSpMe切换为当前.Size = new System.Drawing.Size(168, 24);
            this.tSpMe切换为当前.Text = "切换为当前";
            this.tSpMe切换为当前.Click += new System.EventHandler(this.tSpMe切换为当前_Click);
            // 
            // 切换为当前tSpStor
            // 
            this.切换为当前tSpStor.Name = "切换为当前tSpStor";
            this.切换为当前tSpStor.Size = new System.Drawing.Size(165, 6);
            // 
            // tSpMe置顶
            // 
            this.tSpMe置顶.Name = "tSpMe置顶";
            this.tSpMe置顶.Size = new System.Drawing.Size(168, 24);
            this.tSpMe置顶.Text = "置顶";
            this.tSpMe置顶.Click += new System.EventHandler(this.tSpMe置顶_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(165, 6);
            // 
            // tSpMe设置装箱参数
            // 
            this.tSpMe设置装箱参数.Name = "tSpMe设置装箱参数";
            this.tSpMe设置装箱参数.Size = new System.Drawing.Size(168, 24);
            this.tSpMe设置装箱参数.Text = "设置装箱参数";
            this.tSpMe设置装箱参数.Click += new System.EventHandler(this.tSpMe设置装箱参数_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(165, 6);
            // 
            // tSpMe强制结束
            // 
            this.tSpMe强制结束.Name = "tSpMe强制结束";
            this.tSpMe强制结束.Size = new System.Drawing.Size(168, 24);
            this.tSpMe强制结束.Text = "强制结束订单";
            this.tSpMe强制结束.Click += new System.EventHandler(this.tSpMe强制结束_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(165, 6);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(165, 6);
            // 
            // 移除多条数据ToolStripMenuItem
            // 
            this.移除多条数据ToolStripMenuItem.Name = "移除多条数据ToolStripMenuItem";
            this.移除多条数据ToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.移除多条数据ToolStripMenuItem.Text = "移除多条数据";
            this.移除多条数据ToolStripMenuItem.Click += new System.EventHandler(this.移除多条数据ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(165, 6);
            // 
            // 更新卷号ToolStripMenuItem
            // 
            this.更新卷号ToolStripMenuItem.Name = "更新卷号ToolStripMenuItem";
            this.更新卷号ToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.更新卷号ToolStripMenuItem.Text = "更新卷号";
            this.更新卷号ToolStripMenuItem.Click += new System.EventHandler(this.更新卷号ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(165, 6);
            // 
            // 刷新ToolStripMenuItem
            // 
            this.刷新ToolStripMenuItem.Name = "刷新ToolStripMenuItem";
            this.刷新ToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.刷新ToolStripMenuItem.Text = "刷新";
            this.刷新ToolStripMenuItem.Click += new System.EventHandler(this.刷新ToolStripMenuItem_Click);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.BackColor = System.Drawing.Color.Silver;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel6,
            this.toolStripTextBox2,
            this.toolStripLabel7,
            this.toolStripTextBox3,
            this.toolStripLabel8,
            this.toolStripTextBox4,
            this.toolStripLabel9,
            this.toolStripTextBox1,
            this.toolStripButton2,
            this.NewtSpBtn});
            this.toolStrip2.Location = new System.Drawing.Point(-4, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(1717, 86);
            this.toolStrip2.TabIndex = 6;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripLabel6
            // 
            this.toolStripLabel6.Name = "toolStripLabel6";
            this.toolStripLabel6.Size = new System.Drawing.Size(151, 83);
            this.toolStripLabel6.Text = "Order_No(订单号码)";
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.AutoSize = false;
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(150, 52);
            // 
            // toolStripLabel7
            // 
            this.toolStripLabel7.Name = "toolStripLabel7";
            this.toolStripLabel7.Size = new System.Drawing.Size(146, 83);
            this.toolStripLabel7.Text = "Set Code(混箱类型)";
            // 
            // toolStripTextBox3
            // 
            this.toolStripTextBox3.AutoSize = false;
            this.toolStripTextBox3.Name = "toolStripTextBox3";
            this.toolStripTextBox3.Size = new System.Drawing.Size(100, 52);
            // 
            // NewtSpBtn
            // 
            this.NewtSpBtn.AutoSize = false;
            this.NewtSpBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.NewtSpBtn.Image = ((System.Drawing.Image)(resources.GetObject("NewtSpBtn.Image")));
            this.NewtSpBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NewtSpBtn.Name = "NewtSpBtn";
            this.NewtSpBtn.Size = new System.Drawing.Size(120, 83);
            this.NewtSpBtn.Text = "刷新";
            this.NewtSpBtn.Click += new System.EventHandler(this.NewtSpBtn_Click);
            // 
            // SchedulingTableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1719, 494);
            this.Controls.Add(this.SchedulingtabCtr);
            this.Name = "SchedulingTableForm";
            this.Text = "排单";
            this.Load += new System.EventHandler(this.SchedulingTableForm_Load);
            this.PlantabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PlandGV)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.SchedulingtabCtr.ResumeLayout(false);
            this.ProducetabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProducedGV)).EndInit();
            this.contextMenuStrip2.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel9;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.TabPage PlantabPage;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox PotSpTBox;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripTextBox SetCodetSpTBox;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox CktSpTBox;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripTextBox SkutSpTBox;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripComboBox PoTypetSpCBox;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tSMe加入生产队列;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel8;
        private System.Windows.Forms.TabControl SchedulingtabCtr;
        private System.Windows.Forms.TabPage ProducetabPage;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tSpMe切换为当前;
        private System.Windows.Forms.ToolStripSeparator 切换为当前tSpStor;
        private System.Windows.Forms.ToolStripMenuItem tSpMe置顶;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem tSpMe设置装箱参数;
        private System.Windows.Forms.ToolStripMenuItem tSpMe强制结束;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 移除多条数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel6;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel7;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox3;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 结束订单ToolStripMenuItem;
        private System.Windows.Forms.DataGridView PlandGV;
        private System.Windows.Forms.DataGridView ProducedGV;
        private System.Windows.Forms.ToolStripLabel toolStripLabel10;
        private System.Windows.Forms.ToolStripComboBox IsSatisfytSpCBx;
        private System.Windows.Forms.ToolStripLabel toolStripLabel11;
        private System.Windows.Forms.ToolStripComboBox IsChecktSpCBox;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem 更新卷号ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem 刷新ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton NewtSpBtn;
    }
}