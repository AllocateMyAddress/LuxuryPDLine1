﻿
namespace WindowsForms01
{
    partial class ScanShowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartScanbtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.EndScanbtn = new System.Windows.Forms.Button();
            this.ScanlsBox = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // StartScanbtn
            // 
            this.StartScanbtn.Location = new System.Drawing.Point(12, 5);
            this.StartScanbtn.Name = "StartScanbtn";
            this.StartScanbtn.Size = new System.Drawing.Size(119, 48);
            this.StartScanbtn.TabIndex = 11;
            this.StartScanbtn.Text = "重新开始扫码";
            this.StartScanbtn.UseVisualStyleBackColor = true;
            this.StartScanbtn.Click += new System.EventHandler(this.StartScanbtn_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel1.Controls.Add(this.EndScanbtn);
            this.panel1.Controls.Add(this.StartScanbtn);
            this.panel1.Controls.Add(this.ScanlsBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(823, 425);
            this.panel1.TabIndex = 1;
            // 
            // EndScanbtn
            // 
            this.EndScanbtn.Location = new System.Drawing.Point(162, 5);
            this.EndScanbtn.Name = "EndScanbtn";
            this.EndScanbtn.Size = new System.Drawing.Size(119, 48);
            this.EndScanbtn.TabIndex = 12;
            this.EndScanbtn.Text = "扫码完毕";
            this.EndScanbtn.UseVisualStyleBackColor = true;
            this.EndScanbtn.Click += new System.EventHandler(this.EndScanbtn_Click);
            // 
            // ScanlsBox
            // 
            this.ScanlsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ScanlsBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ScanlsBox.FormattingEnabled = true;
            this.ScanlsBox.ItemHeight = 15;
            this.ScanlsBox.Location = new System.Drawing.Point(-3, 61);
            this.ScanlsBox.Name = "ScanlsBox";
            this.ScanlsBox.Size = new System.Drawing.Size(823, 364);
            this.ScanlsBox.TabIndex = 10;
            // 
            // ScanShowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 425);
            this.Controls.Add(this.panel1);
            this.Name = "ScanShowForm";
            this.Text = "扫码输入";
            this.Load += new System.EventHandler(this.ScanShowForm_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button StartScanbtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button EndScanbtn;
        private System.Windows.Forms.ListBox ScanlsBox;
    }
}