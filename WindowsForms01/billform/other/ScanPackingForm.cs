﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GetData;


namespace WindowsForms01
{
    public partial class ScanPackingForm : Form
    {
        MCData.MCdata mCdata;
        public static  short column ;
        public static short Layernum ;
        public static  short Long;
        public static  short Wide;
        public static short Height;
        public static short Orientation;
        public static short IsRotate;
        public static  short thick  ;
        public static  short GoodWeight;
        public static  short Grabspeed  ;
        public static short Jianspeed;
        public static short qty;
        public static short IsPalletizing;
        public static short IsTopPartition;
        public static short M_PartitionType;
        public static short IsBottomPartition;
        public static short IsYaMaHaRun;
        public static short IsSealingRun;
        public static short IsPackingRun;
        public static short MarkingIsshieid;
        public static short PrintIsshieid;
        public static short IsPalletizingRun;
        public static short IsScanRun;
        public static short HookDirection;
        public static string pakingdata = "";
        otherchoiceForm Choicerotateform = null;
        ManualScanTMForm  ManualScanTMform = null;
        public ScanPackingForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }
        public ScanPackingForm(MCData.MCdata mCdata_)
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            mCdata = mCdata_;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            try
            {
                longtBox.Text = CSReadPlc.GetPlcInt("MarkLong", CSReadPlc.DevName).ToString();
                widthtBox.Text = CSReadPlc.GetPlcInt("MarkWideth", CSReadPlc.DevName).ToString();
                heighttBox.Text = CSReadPlc.GetPlcInt("MarkHeight", CSReadPlc.DevName).ToString();
                if (CSReadPlc.GetPlcInt("Orientation", CSReadPlc.DevName).ToString().Trim()=="1")
                {
                    OrientationcBox.Text ="反";
                }
                else
                {
                    OrientationcBox.Text = "正";
                }
                ColumntBox.Text =""/* CSReadPlc.GetPlcInt("ColumnNunber", CSReadPlc.DevName).ToString()*/;
                //LayernumtBox.Text = CSReadPlc.GetPlcInt("Qty", CSReadPlc.DevName).ToString();
                ThicktBox.Text = "" /*CSReadPlc.GetPlcInt("Thick", CSReadPlc.DevName).ToString()*/;
               // GoodWeighttBox.Text = CSReadPlc.GetPlcInt("GoodWeight", CSReadPlc.DevName).ToString();
                JianspeedtBox.Text = CSReadPlc.GetPlcInt("Jianspeed", CSReadPlc.DevName).ToString();
                GrabspeedtBox.Text = CSReadPlc.GetPlcInt("Grabspeed", CSReadPlc.DevName).ToString();
                if (CSReadPlc.GetPlcInt("IsRotate", CSReadPlc.DevName).ToString().Trim() == "1")
                {
                    IsRotatetBox.Text =""/* "旋转"*/;
                }
                else
                {
                    IsRotatetBox.Text = ""/*"不旋转"*/;
                }
                toptBox.Text = CSReadPlc.GetPlcInt("Updown_distance", CSReadPlc.DevName).ToString();
                //lefttBox.Text = CSReadPlc.GetPlcInt("Leftright_distance", CSReadPlc.DevName).ToString();
               // GoodWeighttBox.Text = CSReadPlc.GetPlcInt("GoodWeight", CSReadPlc.DevName).ToString();
                LinetBox.Text = CSReadPlc.GetPlcInt("LineNum", CSReadPlc.DevName).ToString();
                // LayernumtBox.Text = CSReadPlc.GetPlcInt("Qty", CSReadPlc.DevName).ToString();
                LayernumtBox.Text = ""/* CSReadPlc.GetPlcInt("Qty", CSReadPlc.DevName).ToString()*/;
                qtytBox.Text ="" /*CSReadPlc.GetPlcInt("ScQty", CSReadPlc.DevName).ToString()*/;
                MarkPatterncBox.Text = CSReadPlc.GetPlcInt("MarkPattern", CSReadPlc.DevName).ToString();
                //CatornTypecBox.Text = CSReadPlc.GetPlcInt("CatornType", CSReadPlc.DevName).ToString();
                OrientationcBox.Enabled = false;
                IsRotatetBox.Enabled = false;
                ColumntBox.ReadOnly = true;
                ThicktBox.ReadOnly = true;
                NewShow();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }

        }
        private void OKbtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (mCdata.IsConnect())
                {
                    List<Control> controls_list = new List<Control>();
                    Control[] controls_shuzu = { ColumntBox, longtBox, widthtBox, heighttBox, ThicktBox,qtytBox,LayernumtBox};
                    foreach (Control ctrl in controls_shuzu)
                    {
                        controls_list.Add(ctrl);
                    }
                    if (Chekin.Chekedin(this, controls_list,""))
                    {
                        GetPlcBao csGetBao = new GetPlcBao(mCdata, CSReadPlc.DevName, "", 0);
                        Long = short.Parse(longtBox.Text.ToString().Trim());
                        Wide = short.Parse(widthtBox.Text.ToString().Trim());
                        Height = short.Parse(heighttBox.Text.ToString().Trim());
                        Orientation = (short)CsMarking.GetCatornParameter(OrientationcBox.Text.ToString().Trim());
                        column = short.Parse(ColumntBox.Text.ToString().Trim());
                        //Layernum = short.Parse(LayernumtBox.Text.ToString().Trim());
                        thick = short.Parse(ThicktBox.Text.ToString().Trim());
                       // GoodWeight = short.Parse(GoodWeighttBox.Text.ToString().Trim());
                        Jianspeed = short.Parse(JianspeedtBox.Text.ToString().Trim());
                        Grabspeed = short.Parse(GrabspeedtBox.Text.ToString().Trim());
                        Layernum = short.Parse(LayernumtBox.Text.ToString().Trim());
                        qty = short.Parse(qtytBox.Text.ToString().Trim());
                        IsRotate = (short)CsMarking.GetCatornParameter(IsRotatetBox.Text.ToString().Trim());
                        string PartitionType = PartitionTypecBox.Text.ToString().Trim();
                        string IsBottomPartition_v = "";
                        string IsTopPartition_v = "";
                        CsGetData.GetPartitionData(Long, Wide, PartitionType, out PartitionType, out IsTopPartition_v, out IsBottomPartition_v);
                        IsBottomPartition =short.Parse( IsBottomPartition_v.ToString().Trim());
                        IsTopPartition = short.Parse(IsTopPartition_v.ToString().Trim());
                        M_PartitionType =short.Parse(CsMarking.GetCatornParameter(PartitionType).ToString()); 
                        IsPalletizing =short.Parse( CsMarking.GetCatornParameter(IsPalletizingcBox.Text.ToString().Trim()).ToString());
                        IsPackingRun = short.Parse(CsMarking.GetCatornParameter(IsScanRuncBox.Text.ToString().Trim()).ToString());
                        IsYaMaHaRun = short.Parse(CsMarking.GetCatornParameter(IsYaMaHaRuncBox.Text.ToString().Trim()).ToString());
                        IsSealingRun = short.Parse(CsMarking.GetCatornParameter(IsSealingRuncBox.Text.ToString().Trim()).ToString());
                        MarkingIsshieid = short.Parse(CsMarking.GetCatornParameter(MarkingIsshieidcBox.Text.ToString().Trim()).ToString());
                        PrintIsshieid = short.Parse(CsMarking.GetCatornParameter(PrintIsshieidcBox.Text.ToString().Trim()).ToString());
                        IsPalletizingRun = short.Parse(CsMarking.GetCatornParameter(IsPalletizingRuncBox.Text.ToString().Trim()).ToString());
                        IsScanRun = short.Parse(CsMarking.GetCatornParameter(IsScanRuncBox.Text.ToString().Trim()).ToString());
                        HookDirection = short.Parse(CsMarking.GetCatornParameter(HookDirectioncBox.Text.ToString().Trim()).ToString());
                        if (mCdata.IsConnect() && string.IsNullOrEmpty(pakingdata.Trim()))
                        {
                            pakingdata = "start";
                            M_stcScanData.M_WriteToPlcSku = "start";
                            MessageBox.Show("修改完毕");
                            this.Close();
                            this.Dispose();
                        }
                        else
                        {
                            MessageBox.Show("提示：PLC未连接");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("请先启动设备");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }

        private void ScanPackingForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void ScanPackingForm_Load(object sender, EventArgs e)
        {

        }

        private void choosebtn_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(Choicerotateform, this, "WindowsForms01.otherchoiceForm");
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }
        public void NewShow()
        {

            //前后单码垛一起
            IsPalletizingcBox.Items.Add("不允许");
            IsPalletizingcBox.Items.Add("允许");
            if (CsGetData.GetPlcCatornData("IsPalletizing").Contains("不允许"))
            {
                IsPalletizingcBox.SelectedIndex = 0;
            }
            else
            {
                IsPalletizingcBox.SelectedIndex = 1;
            }
            //扫码
            IsScanRuncBox.Items.Add("不运行");
            IsScanRuncBox.Items.Add("运行");
            if (CsGetData.GetPlcCatornData("IsScanRun").Contains("不运行"))
            {
                IsScanRuncBox.SelectedIndex = 0;
            }
            else
            {
                IsScanRuncBox.SelectedIndex = 1;
            }
            //衣架钩子方向
            HookDirectioncBox.Items.Add("不掉头");
            HookDirectioncBox.Items.Add("掉头");
            if (CsGetData.GetPlcCatornData("HookDirection").Contains("不掉头"))
            {
                HookDirectioncBox.SelectedIndex = 0;
            }
            else
            {
                HookDirectioncBox.SelectedIndex = 1;
            }
            //开箱
            IsPackingRuncBox.Items.Add("不运行");
            IsPackingRuncBox.Items.Add("运行");
            if (CsGetData.GetPlcCatornData("IsPackingRun").Contains("不运行"))
            {
                IsPackingRuncBox.SelectedIndex = 0;
            }
            else
            {
                IsPackingRuncBox.SelectedIndex = 1;
            }
            //雅马哈装箱
            IsYaMaHaRuncBox.Items.Add("不运行");
            IsYaMaHaRuncBox.Items.Add("运行");
            if (CsGetData.GetPlcCatornData("IsYaMaHaRun").Contains("不运行"))
            {
                IsYaMaHaRuncBox.SelectedIndex = 0;
            }
            else
            {
                IsYaMaHaRuncBox.SelectedIndex = 1;
            }
            // IsYaMaHaRuncBox.SelectedIndex = 0;
            //封箱
            IsSealingRuncBox.Items.Add("不运行");
            IsSealingRuncBox.Items.Add("运行");
            if (CsGetData.GetPlcCatornData("IsSealingRun").Contains("不运行"))
            {
                IsSealingRuncBox.SelectedIndex = 0;
            }
            else
            {
                IsSealingRuncBox.SelectedIndex = 1;
            }
            //喷码
            MarkingIsshieidcBox.Items.Add("不运行");
            MarkingIsshieidcBox.Items.Add("运行");
            if (CsGetData.GetPlcCatornData("MarkingIsshieid").Contains("不运行"))
            {
                MarkingIsshieidcBox.SelectedIndex = 0;
            }
            else
            {
                MarkingIsshieidcBox.SelectedIndex = 1;
            }
            // MarkingIsshieidcBox.SelectedIndex = 0;
            //侧面贴标打印
            PrintIsshieidcBox.Items.Add("不运行");
            PrintIsshieidcBox.Items.Add("运行");
            if (CsGetData.GetPlcCatornData("PrintIsshieid").Contains("不运行"))
            {
                PrintIsshieidcBox.SelectedIndex = 0;
            }
            else
            {
                PrintIsshieidcBox.SelectedIndex = 1;
            }
            //码垛
            IsPalletizingRuncBox.Items.Add("不运行");
            IsPalletizingRuncBox.Items.Add("运行");
            if (CsGetData.GetPlcCatornData("IsPalletizingRun").Contains("不运行"))
            {
                IsPalletizingRuncBox.SelectedIndex = 0;
            }
            else
            {
                IsPalletizingRuncBox.SelectedIndex = 1;
            }
            //AB隔板类型
            PartitionTypecBX.Items.Add("A隔板");
            PartitionTypecBX.Items.Add("B隔板");
            PartitionTypecBX.Items.Add("无");
            int PartitionType_v = CSReadPlc.GetPlcInt("PartitionType", CSReadPlc.DevName);
            if (PartitionType_v == 1)
            {
                PartitionTypecBX.SelectedIndex = 0;
            }
            else if (PartitionType_v == 2)
            {
                PartitionTypecBX.SelectedIndex = 1;
            }
            else
            {
                PartitionTypecBX.SelectedIndex = 2;
            }
            //IsPalletizingRuncBox.SelectedIndex = 0;
            //天地盖类型
            string IsTopPartition_v = CsGetData.GetPlcCatornData("IsTopPartition");
            string IsBottomPartition_v = CsGetData.GetPlcCatornData("IsBottomPartition");
            PartitionTypecBox.Items.Add("无");
            PartitionTypecBox.Items.Add("天地盖");
            PartitionTypecBox.Items.Add("天盖");
            PartitionTypecBox.Items.Add("地盖");
            if (IsTopPartition_v=="无"&& IsBottomPartition_v == "无")
            {
                PartitionTypecBox.SelectedIndex = 0;
            }
            else if (IsTopPartition_v == "有" && IsBottomPartition_v == "有")
            {
                PartitionTypecBox.SelectedIndex = 1;
            }
            else if (IsTopPartition_v == "有" && IsBottomPartition_v == "无")
            {
                PartitionTypecBox.SelectedIndex = 2;
            }
            else if (IsTopPartition_v == "无" && IsBottomPartition_v == "有")
            {
                PartitionTypecBox.SelectedIndex = 3;
            }
        }

        private void label34_Click(object sender, EventArgs e)
        {

        }

        private void IsUnpackingcBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void AddTmbtn_Click(object sender, EventArgs e)
        {
            // CsFormShow.FormShowDialog(ScanShowform,this,"WindowsForms01.ScanShowForm");
            CsFormShow.FormShowDialog(ManualScanTMform, this, "WindowsForms01.ManualScanTMForm");
        }

        /// <summary>
        /// 计算厚度
        /// </summary>
        /// <param name="height_"></param>
        /// <param name="Column_"></param>
        /// <param name="Layernum_"></param>
        /// <returns></returns>
        public int GetThick(string height_,string Column_,string Layernum_)
        {
            int ThicktBox_v = 0;
            int Height = 0;
            int culumn = 0;
            int Layernum = 0;
            try
            {
                //计算厚度
                if (!string.IsNullOrEmpty(height_)&&Chekin.IsInt(height_))
                {
                     Height = int.Parse(height_.ToString().Trim());
                }
                if (!string.IsNullOrEmpty(Column_) && Chekin.IsInt(Column_))
                {
                     culumn = int.Parse(Column_.ToString().Trim());
                }
                if (!string.IsNullOrEmpty(Layernum_) && Chekin.IsInt(Layernum_))
                {
                     Layernum = int.Parse(Layernum_.ToString().Trim());
                }
                if (Layernum==0|| culumn==0|| Height==0)
                {
                    ThicktBox_v = 0;
                }
                else
                {
                    ThicktBox_v = int.Parse((Height / (Layernum / culumn)).ToString().Trim());
                }
            }
            catch (Exception err)
            {
                CsLogFun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
            return ThicktBox_v;
        }

        private void heighttBox_Validated(object sender, EventArgs e)
        {
            try
            {
                int thick_v = GetThick(heighttBox.Text.Trim(), ColumntBox.Text.Trim(), LayernumtBox.Text.Trim());
                if (thick_v == 0)
                {
                    ThicktBox.Text = "";
                }
                else
                {
                    ThicktBox.Text = thick_v.ToString();
                }
            }
            catch (Exception err)
            {
                CsLogFun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }

        private void LayernumtBox_Validated(object sender, EventArgs e)
        {
            try
            {
                int thick_v = GetThick(heighttBox.Text.Trim(), ColumntBox.Text.Trim(), LayernumtBox.Text.Trim());
                if (thick_v == 0)
                {
                    ThicktBox.Text = "";
                }
                else
                {
                    ThicktBox.Text = thick_v.ToString();
                }
            }
            catch (Exception err)
            {
                CsLogFun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }

        private void ColumntBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int thick_v = GetThick(heighttBox.Text.Trim(), ColumntBox.Text.Trim(), LayernumtBox.Text.Trim());
                if (thick_v == 0)
                {
                    ThicktBox.Text = "";
                }
                else
                {
                    ThicktBox.Text = thick_v.ToString();
                }
            }
            catch (Exception err)
            {
                CsLogFun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
    }
}
