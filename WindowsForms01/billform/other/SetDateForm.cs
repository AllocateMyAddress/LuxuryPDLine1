﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class SetDateForm : Form
    {
        BatchImportGAPForm BatchImportGAPform = null;
        public SetDateForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void SetDateForm_Load(object sender, EventArgs e)
        {

        }

        private void okbtn_Click(object sender, EventArgs e)
        {
            stcUserData.ShipDate = this.shipdatetBox.Text.ToString().Trim();
            BatchImportGAPform =(BatchImportGAPForm) this.Owner;
            BatchImportGAPform.WHDate = this.shipdatetBox.Text.ToString().Trim(); 
            //BatchImportGAPform.Close();
            //BatchImportGAPform.Dispose();
            if (string.IsNullOrEmpty(stcUserData.ShipDate))
            {
                MessageBox.Show("请选择出货日期！");
                return;
            }
            this.Close();
            this.Dispose();
        }

        private void shipdatedTPk_ValueChanged(object sender, EventArgs e)
        {
            this.shipdatetBox.Text = DateTime.Parse( this.shipdatedTPk.Text).ToString("yyyy-MM-dd");
        }
    }
}
