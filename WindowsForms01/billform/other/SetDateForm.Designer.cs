﻿namespace WindowsForms01
{
    partial class SetDateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okbtn = new System.Windows.Forms.Button();
            this.shipdatetBox = new System.Windows.Forms.TextBox();
            this.shipdatedTPk = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(384, 40);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(115, 45);
            this.okbtn.TabIndex = 4;
            this.okbtn.Text = "设置日期";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.okbtn_Click);
            // 
            // shipdatetBox
            // 
            this.shipdatetBox.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.shipdatetBox.Location = new System.Drawing.Point(151, 45);
            this.shipdatetBox.Multiline = true;
            this.shipdatetBox.Name = "shipdatetBox";
            this.shipdatetBox.Size = new System.Drawing.Size(183, 35);
            this.shipdatetBox.TabIndex = 12;
            // 
            // shipdatedTPk
            // 
            this.shipdatedTPk.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.shipdatedTPk.Location = new System.Drawing.Point(151, 46);
            this.shipdatedTPk.Name = "shipdatedTPk";
            this.shipdatedTPk.Size = new System.Drawing.Size(200, 34);
            this.shipdatedTPk.TabIndex = 11;
            this.shipdatedTPk.ValueChanged += new System.EventHandler(this.shipdatedTPk_ValueChanged);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(30, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 37);
            this.label3.TabIndex = 10;
            this.label3.Text = "出货日期";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SetDateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 123);
            this.Controls.Add(this.shipdatetBox);
            this.Controls.Add(this.shipdatedTPk);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.okbtn);
            this.Name = "SetDateForm";
            this.Text = "设置出货日期";
            this.Load += new System.EventHandler(this.SetDateForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okbtn;
        private System.Windows.Forms.TextBox shipdatetBox;
        private System.Windows.Forms.DateTimePicker shipdatedTPk;
        private System.Windows.Forms.Label label3;
    }
}