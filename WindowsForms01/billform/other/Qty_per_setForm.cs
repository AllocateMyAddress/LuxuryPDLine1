﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class Qty_per_setForm : Form
    {
        public Qty_per_setForm()
        {
            InitializeComponent();
            this.qtytBox.Focus();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }

        private void Qty_per_setForm_Load(object sender, EventArgs e)
        {
        }

        private void okbtn_Click(object sender, EventArgs e)
        {
            CsPublicVariablies.Scan.Qty_per_set = int.Parse(this.qtytBox.Text.ToString().Trim());
            this.Close();
            this.Dispose();
        }
    }
}
