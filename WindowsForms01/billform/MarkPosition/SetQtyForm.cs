﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class SetQtyForm : Form
    {
        #region 变量
        MarkPositionForm MarkPositionform = null;
       // GAPMarkPositionForm GAPMarkPositionform = null;
        GAPChoiceRotateForm  GAPChoiceRotateform = null;
        string MDIFormName = "";
        string GAPTiaoMa = "";
        string WaiCode = "";
        string PoType_v = "";
        string CatornStyle_guid_v = "";
        int MarkPattern_v = 0;
        int CatornType_v = 0;
        int IsRotate_v = 0;
        int LabelingType_v = 0;
        int PartitionType_v = 0;
        string Poguid_v = "";
        int long_v = 0;
        int width_v = 0;
        int height_v = 0;
        int top_v = 0;
        int left_v = 0;
        int weight_v = 0;
        int GoodWeight_v = 0;
        int ColumnNunber_v = 0;
        int LayernumtBox_v = 0;
        int ThicktBox_v = 0;
        int JianspeedtBox_v = 0;
        int GrabspeedtBox_v = 0;
        int Orientation_v = 0;
        string IsRepeatCheck_v = "";
        int LineNum_v = 0;
        string id_v = "";
        int M_strOrientation_v = 0;
        int M_strline_v = 0;
        int M_YYKDgv_index = 0;
        string Sample_Code_v = "";
        public string sku_v = "";
        public string ABPartitiontype_v="" ;
        public string IsBottomPartition_v = "";
        public string IsTopPartition_v = "";
        public string packingqty = "0";
        public string IsUnpacking = "";
        public string MarkingIsshieid = "";
        public string PrintIsshieid = "";
        public string GAPIsPrintRun = "";
        public string IsPalletizing = "";
        public string Partitiontype = "";
        public string IsPackingRun = "";
        public string IsYaMaHaRun = "";
        public string IsSealingRun = "";
        public string IsScanRun = "";
        public string IsPalletizingRun = "";
        public string TiaoMaVal = "";
        public string HookDirection = "";
        DataGridView PakingDvg = null;
        //DataGridView yykDgv = null;
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        GAPChoiceRotateForm GAPChoicerotateform = null;
        PackingForm Packingform = null;
        SchedulingTableForm SchedulingTableform  = null;
        YouyikuForm Youyikuform = null;
        MarkPositionForm markpositionform = null;
        DataGridView SchedulingdGV = null;
        #endregion
        public SetQtyForm()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }
        public SetQtyForm(DataGridView dvg_,Form MDIFormName_)
        {
            InitializeComponent();
            try
            {
                SchedulingdGV = dvg_;
                MDIFormName = MDIFormName_.Name;
                bool isindex_v; int index_v;
                CsFormShow.DvgGetcurrentIndex(dvg_, out isindex_v, out index_v);
                M_YYKDgv_index = index_v;
                string potype_v = dvg_.Rows[index_v].Cells["potype"].Value.ToString().Trim();
                if (isindex_v)
                {
                    DataTable dt = new DataTable();
                    if (potype_v.Contains("UNIQLO")|| potype_v.Contains("GU"))
                    {
                        dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.guid AS Scheduling_guid,po_guid, CartonWeight,CatornType,ColumnNunber,GAPIsPrintRun,GoodWeight,Heghit,HookDirection,IsPackingRun,IsPalletizing,IsPalletizingRun,IsScanRun,IsSealingRun,IsUnpacking,IsPressDown,IsYaMaHaRun,LabelingType,Layernum,LineNum,Long,MarkingIsshieid,MarkPattern,packingqty,a.Partitiontype,po_guid,PrintIsshieid,Thick,TiaoMaVal,TntervalLeft,TntervalTop,Width, CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '1' THEN '不喷色号' ELSE '喷色号' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向',potype,GAPSideIsshieid,GAPSideStartNum,IsRepeatCheck FROM dbo.SchedulingTable AS a where a.guid='{0}'", dvg_.Rows[index_v].Cells["guid"].Value.ToString().Trim()));
                    }
                    else if (potype_v.Contains("GAP"))
                    {
                        dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.guid AS Scheduling_guid,po_guid, CartonWeight,CatornType,ColumnNunber,GAPIsPrintRun,GoodWeight,Heghit,HookDirection,IsPackingRun,IsPalletizing,IsPalletizingRun,IsScanRun,IsSealingRun,IsUnpacking,IsPressDown,IsYaMaHaRun,LabelingType,Layernum,LineNum,Long,MarkingIsshieid,MarkPattern,packingqty,a.Partitiontype,po_guid,PrintIsshieid,Thick,TiaoMaVal,TntervalLeft,TntervalTop,Width, CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '3' THEN '国内' ELSE '国外' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向',potype,GAPSideIsshieid,GAPSideStartNum,IsRepeatCheck FROM dbo.SchedulingTable AS a where a.guid='{0}'", dvg_.Rows[index_v].Cells["guid"].Value.ToString().Trim()));
                    }
                    qtytBx.Text = dt.Rows[0]["packingqty"].ToString().Trim();
                    int count_line = 0;
                    if (potype_v.Contains("UNIQLO") || potype_v.Contains("GU"))
                    {
                         count_line = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.YYKMarkingLineUp WHERE YK_guid='{0}'", dt.Rows[0]["po_guid"].ToString().Trim()));
                    }
                    else
                    {
                         count_line = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE packing_guid='{0}'", dt.Rows[0]["po_guid"].ToString().Trim()));
                    }
                    if (count_line>0)
                    {
                        qtytBx.ReadOnly = true;
                    }
                    TiaomatBox.Text = dt.Rows[0]["TiaoMaVal"].ToString().Trim();
                    ColumntBox.Text = dt.Rows[0]["ColumnNunber"].ToString().Trim();
                    longtBox.Text = dt.Rows[0]["Long"].ToString().Trim();
                    widthtBox.Text = dt.Rows[0]["Width"].ToString().Trim();
                    heighttBox.Text = dt.Rows[0]["Heghit"].ToString().Trim();
                    toptBox.Text= dt.Rows[0]["TntervalTop"].ToString().Trim();
                    lefttBox.Text = dt.Rows[0]["TntervalLeft"].ToString().Trim();
                    GoodWeighttBox.Text = dt.Rows[0]["GoodWeight"].ToString().Trim();
                    ColumntBox.Text = dt.Rows[0]["ColumnNunber"].ToString().Trim();
                    LinetBox.Text = dt.Rows[0]["LineNum"].ToString().Trim();
                    LayernumtBox.Text = dt.Rows[0]["Layernum"].ToString().Trim();
                    ThicktBox.Text = dt.Rows[0]["Thick"].ToString().Trim();
                    MarkPatterncBox.Text = dt.Rows[0]["喷码模式"].ToString().Trim();
                    CatornTypecBox.Text = dt.Rows[0]["纸箱大小"].ToString().Trim();
                    IsRotatetBox.Text = dt.Rows[0]["是否旋转"].ToString().Trim();
                    LabelingTypecbox.Text = dt.Rows[0]["贴标类型"].ToString().Trim();
                    PartitionTypecBox.Text = dt.Rows[0]["隔板类型"].ToString().Trim();
                    OrientationcBox.Text = dt.Rows[0]["正反方向"].ToString().Trim();
                    GAPSideStartNumtBox.Text= dt.Rows[0]["GAPSideStartNum"].ToString().Trim();
                    // CatornStyle_guidtBox.Text = dt.Rows[0]["CatornStyle_guid"].ToString().Trim();
                    PoguidtBox.Text = dt.Rows[0]["po_guid"].ToString().Trim();
                    guidtBox.Text = dt.Rows[0]["Scheduling_guid"].ToString().Trim();
                    NewShow(dt);
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// GAP设置纸箱参数信息
        /// </summary>
        /// <param name="id_"></param>
        /// <param name="long_"></param>
        /// <param name="width_"></param>
        /// <param name="Heghit_"></param>
        /// <param name="TntervalTop_"></param>
        /// <param name="TntervalLeft_"></param>
        /// <param name="CartonWeight_"></param>
        /// <param name="MarkPattern_"></param>
        /// <param name="CatornType_"></param>
        /// <param name="IsRotate_"></param>
        /// <param name="LabelingType_"></param>
        /// <param name="PartitionType_"></param>
        public SetQtyForm(string id_, string long_, string width_, string Heghit_, string TntervalTop_, string TntervalLeft_, string CartonWeight_, string Layernum_, string MarkPattern_, string CatornType_, string LabelingType_, string PartitionType_, string Orientation_, string OrderNoQty_, DataGridView PakingDvg_, int index_, Form MDIForm_, string Qty_ColumnName_, string Palletizing_Unit_)
        {
            InitializeComponent();
            MDIFormName = MDIForm_.Name;
            SchedulingdGV = PakingDvg_;
            string GAPTiaoMa_v = "";
            int packingtotal_v = 0;
            int total = 0;
            int qty = 0;
            string GAPSideStartNum_v = "";
            if (MDIForm_.Name == "SchedulingTableForm" && PakingDvg_.Name == "ProducedGV")
            {
                DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", PakingDvg_.Rows[index_].Cells["po_guid"].Value.ToString()));
                packingtotal_v = int.Parse(dt.Rows[0]["packingtotal"].ToString().Trim());
                total = int.Parse(dt.Rows[0]["箱数"].ToString().Trim());
                qty = total - packingtotal_v;
                GAPSideStartNum_v = dt.Rows[0]["GAPSideStartNum"].ToString().Trim();
                string WaiXiangCode = dt.Rows[0]["外箱代码"].ToString().Trim();
                if (WaiXiangCode.ToUpper().Contains("C4"))
                {
                    //网单贴标
                    GAPSidecBox.Items.Add("运行");
                    GAPSidecBox.Items.Add("不运行");
                    GAPSidecBox.SelectedIndex = 0;
                }
                else
                {
                    //网单贴标
                    GAPSidecBox.Items.Add("运行");
                    GAPSidecBox.Items.Add("不运行");
                    GAPSidecBox.SelectedIndex = 1;
                }
            }
            else if (MDIForm_.Name == "SchedulingTableForm" && PakingDvg_.Name == "PlandGV")
            {
                DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT *  FROM dbo.PACKING AS a  WHERE a.guid='{0}'", PakingDvg_.Rows[index_].Cells["guid"].Value.ToString()));
                GAPTiaoMa_v = PakingDvg_.Rows[index_].Cells["条形码"].Value.ToString();
                GAPSideStartNum_v = dt.Rows[0]["GAPSideStartNum"].ToString().Trim();
                int OrderQty = int.Parse(PakingDvg_.Rows[index_].Cells["剩余箱数"].Value.ToString());
                int ckqty = int.Parse(PakingDvg_.Rows[index_].Cells["实际可装箱数"].Value.ToString());
                if (OrderQty > ckqty)
                {
                    qty = int.Parse(PakingDvg_.Rows[index_].Cells["实际可装箱数"].Value.ToString());
                }
                else
                {
                    qty = int.Parse(PakingDvg_.Rows[index_].Cells["剩余箱数"].Value.ToString());
                }
                string WaiXiangCode = dt.Rows[0]["外箱代码"].ToString().Trim();
                if (WaiXiangCode.ToUpper().Contains("C4"))
                {
                    //网单贴标
                    GAPSidecBox.Items.Add("运行");
                    GAPSidecBox.Items.Add("不运行");
                    GAPSidecBox.SelectedIndex = 0;
                }
                else
                {
                    //网单贴标
                    GAPSidecBox.Items.Add("运行");
                    GAPSidecBox.Items.Add("不运行");
                    GAPSidecBox.SelectedIndex = 1;
                }
            }
            else
            {
                packingtotal_v = int.Parse(PakingDvg_.Rows[index_].Cells["packingtotal"].Value.ToString().Trim());
                total = int.Parse(PakingDvg_.Rows[index_].Cells[Qty_ColumnName_].Value.ToString().Trim());
                qty = total - packingtotal_v;
                string WaiXiangCode = PakingDvg_.Rows[index_].Cells["外箱代码"].Value.ToString().Trim();
                if (WaiXiangCode.ToUpper().Contains("C4"))
                {
                    //网单贴标
                    GAPSidecBox.Items.Add("运行");
                    GAPSidecBox.Items.Add("不运行");
                    GAPSidecBox.SelectedIndex = 0;
                }
                else
                {
                    //网单贴标
                    GAPSidecBox.Items.Add("运行");
                    GAPSidecBox.Items.Add("不运行");
                    GAPSidecBox.SelectedIndex = 1;
                }
            }
            if (PakingDvg_.Columns.Contains("TiaoMaVal"))
            {
                GAPTiaoMa_v = PakingDvg_.Rows[index_].Cells["TiaoMaVal"].Value.ToString().Trim();
            }
            else if (PakingDvg_.Columns.Contains("条形码"))
            {
                GAPTiaoMa_v = PakingDvg_.Rows[index_].Cells["条形码"].Value.ToString().Trim();
            }
            PoguidtBox.Text = id_;
            longtBox.Text = long_;
            widthtBox.Text = width_;
            heighttBox.Text = Heghit_;
            toptBox.Text = TntervalTop_;
            lefttBox.Text = TntervalLeft_;
            weighttBox.Text = CartonWeight_;
            GoodWeighttBox.Text = "";
            ColumntBox.Text = "";
            LinetBox.Text = "";
            LayernumtBox.Text = Layernum_;
            ThicktBox.Text = "";
            //JianspeedtBox.Text = "1";
            //JianspeedtBox.Text = Jianspeed_;
            GrabspeedtBox.Text = "";
            MarkPatterncBox.Text = MarkPattern_;
            CatornTypecBox.Text = CatornType_;
            IsRotatetBox.Text = "";
            LabelingTypecbox.Text = LabelingType_;
            PartitionTypecBox.Text = PartitionType_;
            OrientationcBox.Text = Orientation_;
            qtytBox.Text = OrderNoQty_;
            CatornStyle_guidtBox.Text = "";
            PakingDvg = PakingDvg_;
            //
            GAPTiaoMa = GAPTiaoMa_v;
            IsUnpackingcBox.Items.Add("不拆包");
            IsUnpackingcBox.Items.Add("拆包");
            IsUnpackingcBox.SelectedIndex = 0;
            //前后单码垛一起
            IsPalletizingcBox.Items.Add("不允许");
            IsPalletizingcBox.Items.Add("允许");
            IsPalletizingcBox.SelectedIndex = 0;
            //扫码
            IsScanRuncBox.Items.Add("运行");
            IsScanRuncBox.Items.Add("不运行");
            IsScanRuncBox.SelectedIndex = 0;
            //衣架钩子方向
            HookDirectioncBox.Items.Add("不掉头");
            HookDirectioncBox.Items.Add("掉头");
            HookDirectioncBox.SelectedIndex = 1;
            //开箱
            IsPackingRuncBox.Items.Add("运行");
            IsPackingRuncBox.Items.Add("不运行");
            IsPackingRuncBox.SelectedIndex = 0;
            //雅马哈装箱
            IsYaMaHaRuncBox.Items.Add("运行");
            IsYaMaHaRuncBox.Items.Add("不运行");
            IsYaMaHaRuncBox.SelectedIndex = 0;
            //封箱
            IsSealingRuncBox.Items.Add("运行");
            IsSealingRuncBox.Items.Add("不运行");
            IsSealingRuncBox.SelectedIndex = 0;
            //喷码
            MarkingIsshieidcBox.Items.Add("运行");
            MarkingIsshieidcBox.Items.Add("不运行");
            MarkingIsshieidcBox.SelectedIndex = 0;
            //侧面贴标打印
            PrintIsshieidcBox.Items.Add("运行");
            PrintIsshieidcBox.Items.Add("不运行");
            PrintIsshieidcBox.SelectedIndex = 0;
            //拐角贴标打印
            GAPPrintcBox.Items.Add("运行");
            GAPPrintcBox.Items.Add("不运行");
            GAPPrintcBox.SelectedIndex = 0;
            //码垛
            IsPalletizingRuncBox.Items.Add("运行");
            IsPalletizingRuncBox.Items.Add("不运行");
            IsPalletizingRuncBox.SelectedIndex = 0;
            //是否压箱
            IsPressDowncBox.Items.Add("运行");
            IsPressDowncBox.Items.Add("不运行");
            IsPressDowncBox.SelectedIndex = 0;
            //是否验针扫二维码
            IsCheckcbBox.Items.Add("不运行");
            IsCheckcbBox.Items.Add("运行");
            IsCheckcbBox.SelectedIndex = 0;
            //天地盖类型
            PartitionTypecBox.Items.Add("天地盖");
            PartitionTypecBox.Items.Add("天盖");
            PartitionTypecBox.Items.Add("地盖");
            PartitionTypecBox.Items.Add("无");
            if (MDIForm_.Name.Trim() == "PackingForm"|| MDIForm_.Name == "SchedulingTableForm")
            {
                if (MDIForm_.Name.Trim() == "PackingForm"|| (MDIForm_.Name == "SchedulingTableForm"&& PakingDvg_.Name == "PlandGV"))
                {
                    qtytBx.Text = qty.ToString();
                }
                else
                {
                    qtytBx.Text = SchedulingdGV.SelectedRows[0].Cells["packingqty"].Value.ToString().Trim();
                }
                PartitionTypecBox.SelectedIndex = 3;
                Palletizing_UnittBox.Text = Palletizing_Unit_;
                Palletizing_UnittBox.ReadOnly = true;
                TiaomatBox.Text = GAPTiaoMa;
                GAPSideStartNumtBox.Text = GAPSideStartNum_v;
                PrintIsshieidcBox.Visible = false;
                label4.Visible = false;
                CatornSizebtn.Visible = false;
                //TiaomatBox.Enabled = false;
                GAPNewShow();
            }
            else
            {
                qtytBx.Text = qty.ToString();
                if (PakingDvg_.Rows[index_].Cells["potype"].Value.ToString().Contains("UNIQLO"))
                {
                    PartitionTypecBox.SelectedIndex = 0;
                }
                else
                {
                    PartitionTypecBox.SelectedIndex = 1;
                }
                TiaomatBox.Text = "1";
                TiaomatBox.Visible = false;
                label9.Visible = false;
                label14.Visible = false;
                GAPPrintcBox.Visible = false;
                GAPSidecBox.Visible = false;
                label42.Visible = false;
                GAPSideStartNumtBox.Visible = false;
                label43.Visible = false;
                Palletizing_UnittBox.Text = Palletizing_Unit_;
                Palletizing_UnittBox.ReadOnly = true;
                //longtBox.Visible = false;
                // widthtBox.Visible = false;
                //heighttBox.Visible = false;
                CatornSizebtn.Visible = false;
                YYKGUNewShow();
            }
            // label3.Visible = false;
            // label4.Visible = false;
            //MarkingIsshieidcBox.Visible = false;
            // PrintIsshieidcBox.Visible = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }
        /// <summary>
        /// 查询纸箱参数显示
        /// </summary>
        public SetQtyForm(string long_, string width_, string Heghit_, string TntervalTop_, string TntervalLeft_, string CartonWeight_, string GoodWeight_, string ColumnNunber_, string LineNum_, string Layernum_, string Thick_, string Jianspeed_, string Grabspeed_, string MarkPattern_, string CatornType_, string IsRotate_, string LabelingType_, string OrderNoQty_, string PartitionType_, string Orientation_)
        {
            InitializeComponent();
            //idtBox.Text = id;
            longtBox.Text = long_;
            widthtBox.Text = width_;
            heighttBox.Text = Heghit_;
            toptBox.Text = TntervalTop_;
            lefttBox.Text = TntervalLeft_;
            weighttBox.Text = CartonWeight_;
            GoodWeighttBox.Text = GoodWeight_;
            ColumntBox.Text = ColumnNunber_;
            LinetBox.Text = LineNum_;
            LayernumtBox.Text = Layernum_;
            ThicktBox.Text = Thick_;
            JianspeedtBox.Text = Jianspeed_;
            GrabspeedtBox.Text = Grabspeed_;
            MarkPatterncBox.Text = MarkPattern_;
            CatornTypecBox.Text = CatornType_;
            IsRotatetBox.Text = IsRotate_;
            LabelingTypecbox.Text = LabelingType_;
            PartitionTypecBox.Text = PartitionType_;
            OrientationcBox.Text = Orientation_;
            qtytBox.Text = OrderNoQty_;
            foreach (Control contrl in this.Controls)
            {
                contrl.Enabled = false;
            }
            //Setbtn.Visible = false;
           // batchSetbtn.Visible = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }
        public SetQtyForm(string packingqty_,Form MDIForm_,string Palletizing_Unit_,string GAPTiaoMa_)
        {
            InitializeComponent();
            qtytBx.Text = packingqty_;
            GAPTiaoMa = GAPTiaoMa_;
            IsUnpackingcBox.Items.Add("不拆包");
            IsUnpackingcBox.Items.Add("拆包");
            IsUnpackingcBox.SelectedIndex = 0;
            //前后单码垛一起
            IsPalletizingcBox.Items.Add("不允许");
            IsPalletizingcBox.Items.Add("允许");
            IsPalletizingcBox.SelectedIndex = 0;
            //扫码
            IsScanRuncBox.Items.Add("运行");
            IsScanRuncBox.Items.Add("不运行");
            IsScanRuncBox.SelectedIndex = 0;
            //衣架钩子方向
            HookDirectioncBox.Items.Add("不掉头");
            HookDirectioncBox.Items.Add("掉头");
            HookDirectioncBox.SelectedIndex = 1;
            //开箱
            IsPackingRuncBox.Items.Add("运行");
            IsPackingRuncBox.Items.Add("不运行");
            IsPackingRuncBox.SelectedIndex = 0;
            //雅马哈装箱
            IsYaMaHaRuncBox.Items.Add("运行");
            IsYaMaHaRuncBox.Items.Add("不运行");
            IsYaMaHaRuncBox.SelectedIndex = 0;
            //封箱
            IsSealingRuncBox.Items.Add("运行");
            IsSealingRuncBox.Items.Add("不运行");
            IsSealingRuncBox.SelectedIndex = 0;
            //喷码
            MarkingIsshieidcBox.Items.Add("运行");
            MarkingIsshieidcBox.Items.Add("不运行");
            MarkingIsshieidcBox.SelectedIndex = 0;
            //侧面贴标打印
            PrintIsshieidcBox.Items.Add("运行");
            PrintIsshieidcBox.Items.Add("不运行");
            PrintIsshieidcBox.SelectedIndex = 0;
            //拐角贴标打印
            GAPPrintcBox.Items.Add("运行");
            GAPPrintcBox.Items.Add("不运行");
            GAPPrintcBox.SelectedIndex = 0;
            //网单贴标
            GAPSidecBox.Items.Add("运行");
            GAPSidecBox.Items.Add("不运行");
            GAPSidecBox.SelectedIndex = 1;
            //码垛
            IsPalletizingRuncBox.Items.Add("运行");
            IsPalletizingRuncBox.Items.Add("不运行");
            IsPalletizingRuncBox.SelectedIndex = 0;
            //是否压箱
            IsPressDowncBox.Items.Add("运行");
            IsPressDowncBox.Items.Add("不运行");
            IsPressDowncBox.SelectedIndex = 0;
            //是否验针扫二维码
            IsCheckcbBox.Items.Add("不运行");
            IsCheckcbBox.Items.Add("运行");
            IsCheckcbBox.SelectedIndex = 0;
            //天地盖类型
            PartitionTypecBox.Items.Add("天地盖");
            PartitionTypecBox.Items.Add("天盖");
            PartitionTypecBox.Items.Add("地盖");
            PartitionTypecBox.Items.Add("无");
            if (MDIForm_.Name.Trim() == "PackingForm")
            {
                PartitionTypecBox.SelectedIndex = 3;
                Palletizing_UnittBox.Text = Palletizing_Unit_;
                Palletizing_UnittBox.ReadOnly = true;
                TiaomatBox.Text = GAPTiaoMa;
                label4.Visible = false;
                PrintIsshieidcBox.Visible = false;
                GAPNewShow();
            }
            else
            {

                PartitionTypecBox.SelectedIndex = 3;
                TiaomatBox.Text = "1";
                TiaomatBox.Visible = false;
                label9.Visible = false;
                label14.Visible = false;
                GAPPrintcBox.Visible = false;
                GAPSidecBox.Visible = false;
                label42.Visible = false;
                GAPSideStartNumtBox.Visible = false;
                label43.Visible = false;
                Palletizing_UnittBox.Text = Palletizing_Unit_;
                Palletizing_UnittBox.ReadOnly = true;
                OrientationcBox.Enabled = false;
                IsRotatetBox.ReadOnly = true;
                ColumntBox.ReadOnly = true;
                longtBox.ReadOnly = true;
                widthtBox.ReadOnly = true;
                heighttBox.ReadOnly = true;
                //label29.Visible = false;
                //label30.Visible = false;
                // ColumntBox.Visible = false;
                //IsRotatetBox.Visible = false;
                //choosebtn.Visible = false;
                label36.Visible = false;
                longtBox.Visible = false;
                label35.Visible = false;
                widthtBox.Visible = false;
                label34.Visible = false;
                heighttBox.Visible = false;
                label14.Visible = false;
                GAPPrintcBox.Visible = false;
                GAPSidecBox.Visible = false;
                label42.Visible = false;
                GAPSideStartNumtBox.Visible = false;
                label43.Visible = false;
                YYKGUNewShow();
            }
            MDIFormName = MDIForm_.Name;
           // label3.Visible = false;
           // label4.Visible = false;
            //MarkingIsshieidcBox.Visible = false;
           // PrintIsshieidcBox.Visible = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }
        //DataGridView ScheduingDvg;
        public SetQtyForm(string packingqty_, Form MDIForm_, string Palletizing_Unit_, string GAPTiaoMa_,int index_v_,DataGridView ScheduingDvg_)
        {
            InitializeComponent();
            SchedulingdGV = ScheduingDvg_;
            M_YYKDgv_index = index_v_;
            qtytBx.Text = packingqty_;
            GAPTiaoMa = GAPTiaoMa_;
            IsUnpackingcBox.Items.Add("不拆包");
            IsUnpackingcBox.Items.Add("拆包");
            IsUnpackingcBox.SelectedIndex = 0;
            //前后单码垛一起
            IsPalletizingcBox.Items.Add("不允许");
            IsPalletizingcBox.Items.Add("允许");
            IsPalletizingcBox.SelectedIndex = 0;
            //衣架钩子方向
            HookDirectioncBox.Items.Add("不掉头");
            HookDirectioncBox.Items.Add("掉头");
            HookDirectioncBox.SelectedIndex = 1;
            //开箱
            IsPackingRuncBox.Items.Add("运行");
            IsPackingRuncBox.Items.Add("不运行");
            IsPackingRuncBox.SelectedIndex = 0;
            //雅马哈装箱
            IsYaMaHaRuncBox.Items.Add("运行");
            IsYaMaHaRuncBox.Items.Add("不运行");
            IsYaMaHaRuncBox.SelectedIndex = 0;
            //封箱
            IsSealingRuncBox.Items.Add("运行");
            IsSealingRuncBox.Items.Add("不运行");
            IsSealingRuncBox.SelectedIndex = 0;
            //喷码
            MarkingIsshieidcBox.Items.Add("运行");
            MarkingIsshieidcBox.Items.Add("不运行");
            MarkingIsshieidcBox.SelectedIndex = 0;
            //侧面贴标打印
            PrintIsshieidcBox.Items.Add("运行");
            PrintIsshieidcBox.Items.Add("不运行");
            PrintIsshieidcBox.SelectedIndex = 0;
            //拐角贴标打印
            GAPPrintcBox.Items.Add("运行");
            GAPPrintcBox.Items.Add("不运行");
            GAPPrintcBox.SelectedIndex = 0;
            //网单贴标
            GAPSidecBox.Items.Add("运行");
            GAPSidecBox.Items.Add("不运行");
            GAPSidecBox.SelectedIndex = 1;
            //码垛
            IsPalletizingRuncBox.Items.Add("运行");
            IsPalletizingRuncBox.Items.Add("不运行");
            IsPalletizingRuncBox.SelectedIndex = 0;
            //是否压箱
            IsPressDowncBox.Items.Add("运行");
            IsPressDowncBox.Items.Add("不运行");
            IsPressDowncBox.SelectedIndex = 0;
            //是否再检针
            IsCheckcbBox.Items.Add("不运行");
            IsCheckcbBox.Items.Add("运行");
            IsCheckcbBox.SelectedIndex = 0;
            //天地盖类型
            PartitionTypecBox.Items.Add("天地盖");
            PartitionTypecBox.Items.Add("天盖");
            PartitionTypecBox.Items.Add("地盖");
            PartitionTypecBox.Items.Add("无");
            if (MDIForm_.Name.Trim() == "PackingForm")
            {
                PartitionTypecBox.SelectedIndex = 3;
                Palletizing_UnittBox.Text = Palletizing_Unit_;
                Palletizing_UnittBox.ReadOnly = true;
                TiaomatBox.Text = GAPTiaoMa;
                label4.Visible = false;
                PrintIsshieidcBox.Visible = false;
                //扫码
                IsScanRuncBox.Items.Add("运行");
                IsScanRuncBox.Items.Add("不运行");
                IsScanRuncBox.SelectedIndex = 0;
                GAPNewShow();
            }
            else
            {
                //扫码
                IsScanRuncBox.Items.Add("运行");
                IsScanRuncBox.Items.Add("不运行");
                IsScanRuncBox.SelectedIndex = 1;
                if (SchedulingdGV.SelectedRows[0].Cells["potype"].Value.ToString().Contains("UNIQLO"))
                {
                    PartitionTypecBox.SelectedIndex = 0;
                }
                else
                {
                    PartitionTypecBox.SelectedIndex = 1;
                }
                Palletizing_UnittBox.Text = Palletizing_Unit_;
                YYKGUNewShow();
            }
            MDIFormName = MDIForm_.Name;
            // label3.Visible = false;
            // label4.Visible = false;
            //MarkingIsshieidcBox.Visible = false;
            // PrintIsshieidcBox.Visible = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void okbtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool b_v = true;
                if (MDIFormName.Trim() == "YouyikuForm")
                {
                    b_v= YYKGUScheduingSet();
                }
                else if (MDIFormName.Trim()== "PackingForm")
                {
                    b_v= GAPScheduling();
                }
                else if (MDIFormName.Trim() == "SchedulingTableForm"&& SchedulingdGV.Name == "PlandGV")
                {
                    b_v = SchedulingSort();
                }
                else if (MDIFormName.Trim() == "SchedulingTableForm" && SchedulingdGV.Name == "ProducedGV" && SchedulingdGV.Rows[M_YYKDgv_index].Cells["potype"].Value.ToString().Contains("UNIQLO"))
                {
                    lock (CSReadPlc.lockScan)
                    {
                        b_v = YYKGUScheduingSet(SchedulingdGV);
                        CsInitialization.PowerOnUpdateCurrentScan();
                    }
                }
                else if (MDIFormName.Trim() == "SchedulingTableForm" && SchedulingdGV.Name == "ProducedGV" && SchedulingdGV.Rows[M_YYKDgv_index].Cells["potype"].Value.ToString().Contains("GU"))
                {
                    lock (CSReadPlc.lockScan)
                    {
                        b_v = YYKGUScheduingSet(SchedulingdGV);
                        CsInitialization.PowerOnUpdateCurrentScan();
                    }
                }
                else if (MDIFormName.Trim() == "SchedulingTableForm" && SchedulingdGV.Name == "ProducedGV" && SchedulingdGV.Rows[M_YYKDgv_index].Cells["potype"].Value.ToString().Contains("GAP"))
                {
                    lock (CSReadPlc.lockScan)
                    {
                        b_v = GAPScheduling(SchedulingdGV);
                        CsInitialization.PowerOnUpdateCurrentScan();
                    }
                }
                if (b_v)
                {
                    this.Close();
                    this.Dispose();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void SetQtyForm_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void IsUnpackingcBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public void GAPNewShow()
        {
           // longtBox.ReadOnly = true;
           /// widthtBox.ReadOnly = true;
           // heighttBox.ReadOnly = true;
            toptBox.ReadOnly = true;
            lefttBox.ReadOnly = true;
            weighttBox.ReadOnly = true;
            ColumntBox.ReadOnly = true;
            LayernumtBox.ReadOnly = true;
            qtytBox.ReadOnly = true;
            // qtytBox.ReadOnly = true;
            //ThicktBox.ReadOnly = true;
            //JianspeedtBox.ReadOnly = true;
            //GrabspeedtBox.ReadOnly = true;
            IsRotatetBox.ReadOnly = true;
            MarkPatterncBox.Enabled = false;
            CatornTypecBox.Enabled = false;
            LabelingTypecbox.Enabled = false;
            OrientationcBox.Enabled = false;
            LayernumtBox.ReadOnly = true;
            LinetBox.ReadOnly = true;
            LinetBox.Text = "1";
            GoodWeighttBox.Text = "1";
            ThicktBox.Text = "50";
            LayernumtBox.Text = "10";
            weighttBox.Text = "1";
            //JianspeedtBox.Text = "90";
        }
        public void GetContrlVal(string potype_,string po_guid_)
        {
            if (potype_.Contains("UNIQLO")|| potype_.Contains("GU"))
            {
                DataTable dtLayernum = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YOUYIKUDO WHERE guid='{0}'", po_guid_));
                if (dtLayernum.Rows.Count > 0)
                {
                    string Sample_Code_val = "";
                    LayernumtBox_v =int.Parse( CsGetData.GetLayernum(dtLayernum, IsUnpacking, "YYK").ToString().Trim());
                    if (!string.IsNullOrEmpty(dtLayernum.Rows[0]["Sample_Code"].ToString()))
                    {
                        Sample_Code_val = dtLayernum.Rows[0]["Sample_Code"].ToString().Trim().Substring(0, 2);
                    }
                    if (Sample_Code_val.Contains("03"))
                    {
                        Sample_Code_v = "2";//改变读取条码数量为2个
                    }
                    else
                    {
                        Sample_Code_v = "1";//改变读取条码数量为1个
                    }
                    //计算厚度
                    int Height = int.Parse(heighttBox.Text.ToString().Trim());
                    int culumn = int.Parse(ColumntBox.Text.ToString().Trim());
                    if (LayernumtBox_v / culumn==0)
                    {
                        ThicktBox_v=1;
                    }
                    else
                    {
                        ThicktBox_v = int.Parse((Height / (LayernumtBox_v / culumn)).ToString().Trim());
                    }
                }
            }
            else if (potype_.Contains("GAP"))
            {
                DataTable dtLayernum = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", po_guid_));
                if (dtLayernum.Rows.Count > 0)
                {
                    LayernumtBox_v =int.Parse( CsGetData.GetLayernum(dtLayernum, IsUnpacking, "GAP").ToString().Trim());
                    Sample_Code_v = "1";//改变读取条码数量为1个
                    //计算厚度
                    int Height = int.Parse(heighttBox.Text.ToString().Trim());
                    int culumn = int.Parse(ColumntBox.Text.ToString().Trim());
                    ThicktBox_v = int.Parse((Height / (LayernumtBox_v / culumn)).ToString().Trim());
                    sku_v = dtLayernum.Rows[0]["Sku号码"].ToString().Trim();
                }
            }
            packingqty= qtytBx.Text.ToString().Trim();
            Poguid_v = PoguidtBox.Text.ToString().Trim();
           // string catorn_type=CsFormShow.SqlGetVal(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", Poguid_v), "外箱代码");
            CatornStyle_guid_v = CatornStyle_guidtBox.Text.ToString().Trim();
            long_v = (int)double.Parse(longtBox.Text.ToString().Trim());
            width_v = (int)double.Parse(widthtBox.Text.ToString().Trim());
            height_v = (int)double.Parse(heighttBox.Text.ToString().Trim());
            top_v = (int)double.Parse(toptBox.Text.ToString().Trim());
            left_v = (int)double.Parse(lefttBox.Text.ToString().Trim());
           // weight_v = (int)double.Parse(weighttBox.Text.ToString().Trim());
            GoodWeight_v = (int)double.Parse(GoodWeighttBox.Text.ToString().Trim());
            ColumnNunber_v = (int)double.Parse(ColumntBox.Text.ToString().Trim());
            LineNum_v = (int)double.Parse(LinetBox.Text.ToString().Trim());
           // LayernumtBox_v = (int)double.Parse(LayernumtBox.Text.ToString().Trim());
           // ThicktBox_v = (int)double.Parse(ThicktBox.Text.ToString().Trim());
            //JianspeedtBox_v = (int)double.Parse(JianspeedtBox.Text.ToString().Trim());
            // GrabspeedtBox_v = (int)double.Parse(GrabspeedtBox.Text.ToString().Trim());
            MarkPattern_v = CsMarking.GetCatornParameter(MarkPatterncBox.Text.ToString().Trim());
            CatornType_v = CsMarking.GetCatornParameter(CatornTypecBox.Text.ToString().Trim());
            IsRotate_v = CsMarking.GetCatornParameter(IsRotatetBox.Text.ToString().Trim());
            LabelingType_v = CsMarking.GetCatornParameter(LabelingTypecbox.Text.ToString().Trim());
           // PartitionType_v = CsMarking.GetCatornParameter(PartitionTypecBox.Text.ToString().Trim());
            Orientation_v = CsMarking.GetCatornParameter(OrientationcBox.Text.ToString().Trim());
            //计算隔板类型
            CsGetData.GetPartitionData(long_v, width_v, PartitionTypecBox.Text.ToString().Trim(), out ABPartitiontype_v, out IsTopPartition_v, out IsBottomPartition_v);
            ABPartitiontype_v = CsMarking.GetCatornParameter(ABPartitiontype_v).ToString();
             IsRepeatCheck_v = IsCheckcbBox.Text.ToString().Trim();

        }

        /// <summary>
        /// 判断上一个单装箱完成
        /// </summary>
        /// <returns></returns>
        public string IsGAPPackingEnd()
        {
            try
            {
                if (!string.IsNullOrEmpty(M_stcScanData.strpo))
                {
                    string sqlstr = "";
                    int count_v = 0;
                    if (M_stcScanData.potype.Contains("GU") && !string.IsNullOrEmpty(CsPakingData.GetMarking_Colorcode("Order_No")))
                    {
                        sqlstr = string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND SKU_Code='{1}' AND Warehouse='{2}' AND CurrentPackingNum=isnull(FinishPackingNum,0) AND potype='{3}'AND Color_Code='{4}' AND Set_Code='{5}'", M_stcScanData.strpo, M_stcScanData.strPoTiaoMa, M_stcScanData.stridentification, "GU", M_stcScanData.Color_Code, M_stcScanData.Set_Code);
                        count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND SKU_Code='{1}' AND Warehouse='{2}' AND  potype='{3}'AND Color_Code='{4}' AND Set_Code='{5}'", M_stcScanData.strpo, M_stcScanData.strPoTiaoMa, M_stcScanData.stridentification, "GU", M_stcScanData.Color_Code, M_stcScanData.Set_Code));
                    }
                    else if (M_stcScanData.potype.Contains("GAP") && (!string.IsNullOrEmpty(CsPakingData.GetMarking_Net("订单号码")) || !string.IsNullOrEmpty(CsPakingData.GetMarking_NONet("订单号码"))))
                    {
                        sqlstr = string.Format("SELECT * FROM dbo.MarkingLineUp WHERE  订单号码='{0}'  AND 从='{1}' AND CurrentPackingNum=isnull(FinishPackingNum,0)", M_stcScanData.strpo, M_stcScanData.stridentification);
                         count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT * FROM dbo.MarkingLineUp WHERE  订单号码='{0}'  AND 从='{1}'", M_stcScanData.strpo, M_stcScanData.stridentification));
                    }
                    if (!string.IsNullOrEmpty(sqlstr) && count_v > 0)
                    {
                        DataSet ds = content1.Select_nothing(sqlstr);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            #region 不允许强制切单的情况
                            //MessageBox.Show("本次装箱数量未放完不能切换单");
                            //return "取消切单";
                            #endregion
                            #region 允许强制切单的情况
                            DialogResult diachose = MessageBox.Show("本次装箱数量未放完是否强制切单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                            if (diachose == DialogResult.OK)
                            {
                                MessageBox.Show("请先结束当前订单，切单时停止扫码操作并且将设备调成手动状态");
                                return "强制切单";
                            }
                            else if (diachose == DialogResult.Cancel)
                            {
                                return "取消切单";
                            }
                            #endregion
                        }
                    }
                }
                return "正常切单";
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// gap设置
        /// </summary>
        //public void GAPSet()
        //{
        //    try
        //    {
        //        //检查设备是否启动
        //        if (!CsRWPlc.mCdata.IsConnect())
        //        {
        //            MessageBox.Show("请先启动设备再切换单");
        //            return;
        //        }
        //        List<Control> controls_list1 = new List<Control>();
        //        List<Control> controls_list2 = new List<Control>();
        //        Control[] controls_shuzu1 = { longtBox, widthtBox, heighttBox, toptBox, lefttBox, LayernumtBox, GoodWeighttBox, ThicktBox, ColumntBox, qtytBox, LinetBox, TiaomatBox, LinetBox };
        //        Control[] controls_shuzu2 = { MarkPatterncBox, CatornTypecBox, IsRotatetBox, LabelingTypecbox, PartitionTypecBox, weighttBox, OrientationcBox , OrientationcBox };
        //        foreach (Control ctrl in controls_shuzu1)
        //        {
        //            controls_list1.Add(ctrl);
        //        }
        //        foreach (Control ctrl in controls_shuzu2)
        //        {
        //            controls_list2.Add(ctrl);
        //        }
        //        if (Chekin.Chekedin(this, controls_list1) && Chekin.Chekedin_notnull(this, controls_list2))
        //        {
        //            GetContrlVal();
        //            #region 计算垛板单位数量
        //            M_stcPalletizingData.strCatornLong = long_v.ToString();
        //            M_stcPalletizingData.strCatornWidth = width_v.ToString();
        //            M_stcPalletizingData.strCatornHeight = height_v.ToString();
        //            M_stcPalletizingData.strWsizeFlag = "start";
        //            TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
        //            while (M_stcPalletizingData.ReadPalletizingflag != "1")
        //            {
        //                TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
        //                TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
        //                if (ts.Seconds > 1)
        //                {
        //                    break;
        //                }
        //            }
        //            M_stcPalletizingData.strResetFlag = "start";
        //            #endregion
        //            //string selectsql = string.Format("SELECT*FROM dbo.GAPMarkingPosition WHERE guid='{0}' ", CatornStyle_guid_v);
        //            //DataSet ds = content1.Select_nothing(selectsql);
        //            int count_v = 0;
        //            if (!string.IsNullOrEmpty(CatornStyle_guid_v))
        //            {
        //                count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.GAPMarkingPosition WHERE guid='{0}' ", CatornStyle_guid_v));
        //            }
        //            if (string.IsNullOrEmpty(CatornStyle_guid_v) || count_v == 0)
        //            {
        //                string guid_v = System.Guid.NewGuid().ToString();
        //                string sqlstr = string.Format("INSERT INTO GAPMarkingPosition (Long,Width,Heghit,TntervalTop,TntervalLeft,CartonWeight,MarkPattern,CatornType,IsRotate,LabelingType,PartitionType,ColumnNunber,Layernum,Thick,Jianspeed,Grabspeed,Entrytime,GoodWeight,guid,Orientation,LineNum) VALUES ({0},{1},{2},{3},{4},{5},'{6}','{7}','{8}','{9}','{10}',{11},{12},{13},{14},{15},'{16}','{17}','{18}','{19}','{20}')", long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, PartitionType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, JianspeedtBox_v, GrabspeedtBox_v, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), GoodWeight_v, guid_v, Orientation_v, LineNum_v);
        //                content1.Select_nothing(sqlstr);
        //                //更新装箱单CatornStyle_guid
        //                string strsql = string.Format(" UPDATE dbo.PACKING SET CatornStyle_guid = '{0}' WHERE guid = '{1}'", guid_v, Poguid_v);
        //                content1.Select_nothing(strsql);
        //            }
        //            else
        //            {
        //                string sqldelete = string.Format("UPDATE dbo.GAPMarkingPosition  SET   GoodWeight='{0}' ,Thick='{1}',Jianspeed='{2}',Grabspeed='{3}', ColumnNunber='{4}',IsRotate='{5}',Orientation='{6}', LineNum='{7}',Long='{8}',Width='{9}',Heghit='{10}' WHERE guid='{11}' ", GoodWeight_v, ThicktBox_v, JianspeedtBox_v, GrabspeedtBox_v, ColumnNunber_v, IsRotate_v, Orientation_v, LineNum_v, long_v, width_v, height_v, CatornStyle_guid_v);
        //                content1.Select_nothing(sqldelete);
        //            }
        //            //Packingform = (PackingForm)this.Owner;
        //            //Packingform.NewPakingShow();
        //            //MessageBox.Show("设置完成");
        //            #region 设置为当前订单
        //            DialogResult btchose = MessageBox.Show("是否将订单设置为当前", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
        //            if (btchose == DialogResult.OK)
        //            {
        //                if (PakingDvg.Rows.Count > 0 && PakingDvg.SelectedRows.Count > 0)
        //                {
        //                    bool isindex_v; int index_v;
        //                    CsFormShow.DvgGetcurrentIndex(PakingDvg, out isindex_v, out index_v);
        //                    if (isindex_v)
        //                    {
        //                       // CsFormShow csformShow = new CsFormShow();
        //                      // csformShow.SetPackingQty(index_v, PakingDvg, this, "箱数", M_stcPalletizingData.Palletizing_Unit.ToString());
        //                        DataGridViewRow DGVROW = PakingDvg.Rows[index_v];
        //                        Packingform = (PackingForm)this.Owner;
        //                        Packingform.packingqty = qtytBx.Text.ToString().Trim();
        //                        Packingform.IsUnpacking = IsUnpackingcBox.Text.ToString().Trim();
        //                        Packingform.MarkingIsshieid = MarkingIsshieidcBox.Text.ToString().Trim();
        //                        Packingform.PrintIsshieid = PrintIsshieidcBox.Text.ToString().Trim();
        //                        Packingform.GAPIsPrintRun = GAPPrintcBox.Text.ToString().Trim();
        //                        Packingform.IsPalletizing = IsPalletizingcBox.Text.ToString().Trim();
        //                        Packingform.Partitiontype = PartitionTypecBox.Text.ToString().Trim();
        //                        Packingform.IsPackingRun = IsPackingRuncBox.Text.ToString().Trim();
        //                        Packingform.IsYaMaHaRun = IsYaMaHaRuncBox.Text.ToString().Trim();
        //                        Packingform.IsSealingRun = IsSealingRuncBox.Text.ToString().Trim();
        //                        Packingform.IsPalletizingRun = IsPalletizingRuncBox.Text.ToString().Trim();
        //                        Packingform.IsScanRun = IsScanRuncBox.Text.ToString().Trim();
        //                        Packingform.TiaoMaVal = TiaomatBox.Text.ToString().Trim();
        //                        Packingform.HookDirection = HookDirectioncBox.Text.ToString().Trim();
        //                        if (packingqty == "0")
        //                        {
        //                            MessageBox.Show("装箱数量不能为0");
        //                            return;
        //                        }
        //                        #region 判断是否超订单数
        //                        bool b_IsExceedPoNumber = IsGAPExceedPoNumber(int.Parse(packingqty), PakingDvg.Rows[index_v].Cells["订单号码"].Value.ToString().Trim(), PakingDvg.Rows[index_v].Cells["Sku号码"].Value.ToString().Trim(), PakingDvg.Rows[index_v].Cells["从"].Value.ToString().Trim());
        //                        if (b_IsExceedPoNumber)
        //                        {
        //                            DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
        //                            if (diaIsExceedPoNumber == DialogResult.Cancel)
        //                            {
        //                                return;
        //                            }
        //                        }
        //                        #endregion
        //                        bool b_v = Packingform.GetGAPMarkingPositionRowData(PakingDvg.Rows[index_v]);
        //                        if (b_v)
        //                        {
        //                            //string strsql = string.Format(" UPDATE dbo.PACKING SET packingtotal = ISNULL(packingtotal,0)+{0} WHERE guid = '{1}'", int.Parse(qtytBox.Text.ToString().Trim()), Poguid_v);
        //                            //content1.Select_nothing(strsql);
        //                            //先判断上一个单是否箱子放完
        //                            string changetype = IsGAPPackingEnd();
        //                            if (changetype.Contains("取消切单"))
        //                            {
        //                                return;
        //                            }
        //                            else if (changetype.Contains("强制切单"))
        //                            {
        //                                // CsGetData.GAPCompelChange();
        //                                return;
        //                            }
        //                            //确认可以切单后操作
        //                            Packingform.SetToMarking(DGVROW);//更新本次喷码信息
        //                            Packingform.NewPakingShow();
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    MessageBox.Show("提示：没有可执行的数据");
        //                    return;
        //                }
        //            }
        //            #endregion
        //            //注意此处一定要在这里释放窗体，不然以上设置当前订单会提示对象没有实例
        //            this.Close();
        //            this.Dispose();
        //        }
        //        else
        //        {
        //            return;
        //        }

        //    }
        //    catch (Exception err)
        //    {
        //        Cslogfun.WriteToLog(err);
        //        throw new Exception("提示：" + err.Message);
        //    }
        //}
        /// <summary>
        /// GAP排单加入队列设置
        /// </summary>
        public bool GAPScheduling()
        {
            try
            {
                //检查设备是否启动
                //if (!CsRWPlc.mCdata.IsConnect())
                //{
                //    MessageBox.Show("请先启动设备再切换单");
                //    return;
                //}
                List<Control> controls_list1 = new List<Control>();
                List<Control> controls_list2 = new List<Control>();
                Control[] controls_shuzu1 = { longtBox, widthtBox, heighttBox, toptBox, lefttBox, LayernumtBox, GoodWeighttBox, ThicktBox, ColumntBox, qtytBx, LinetBox, TiaomatBox, LinetBox };
                Control[] controls_shuzu2 = { MarkPatterncBox, CatornTypecBox, IsRotatetBox, LabelingTypecbox, PartitionTypecBox, weighttBox, OrientationcBox, OrientationcBox };
                foreach (Control ctrl in controls_shuzu1)
                {
                    controls_list1.Add(ctrl);
                }
                foreach (Control ctrl in controls_shuzu2)
                {
                    controls_list2.Add(ctrl);
                }
                string GAPPrint_v = GAPSidecBox.Text.ToString().Trim();
                if (GAPPrint_v=="运行")
                {
                    if (string.IsNullOrEmpty(GAPSideStartNumtBox.Text.ToString()))
                    {
                        CsFormShow.MessageBoxFormShow("运行网单标签必须输入起始序号");
                        return false;
                    }
                }
                if (Chekin.Chekedin(this, controls_list1,"") && Chekin.Chekedin_notnull(this, controls_list2,""))
                {
                    #region 计算垛板单位数量
                    //M_stcPalletizingData.strCatornLong = long_v.ToString();
                    //M_stcPalletizingData.strCatornWidth = width_v.ToString();
                    //M_stcPalletizingData.strCatornHeight = height_v.ToString();
                    //M_stcPalletizingData.strWsizeFlag = "start";
                    //TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                    //while (M_stcPalletizingData.ReadPalletizingflag != "1")
                    //{
                    //    TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                    //    TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                    //    if (ts.Seconds > 1)
                    //    {
                    //        break;
                    //    }
                    //}
                    //M_stcPalletizingData.strResetFlag = "start";
                    #endregion
                    #region 设置为当前订单
                    DialogResult btchose = MessageBox.Show("是否将订单加入订单队列中", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (btchose == DialogResult.OK)
                    {
                        if (PakingDvg.Rows.Count > 0 && PakingDvg.SelectedRows.Count > 0)
                        {
                            Palletizing_UnittBox.Text = M_stcPalletizingData.Palletizing_Unit.ToString();
                            bool isindex_v; int index_v;
                            CsFormShow.DvgGetcurrentIndex(PakingDvg, out isindex_v, out index_v);
                            if (isindex_v)
                            {
                                GetContrlVal("GAP", PakingDvg.Rows[index_v].Cells["guid"].Value.ToString().Trim());
                                // CsFormShow csformShow = new CsFormShow();
                                // csformShow.SetPackingQty(index_v, PakingDvg, this, "箱数", M_stcPalletizingData.Palletizing_Unit.ToString());
                                DataGridViewRow DGVROW = PakingDvg.Rows[index_v];
                                Packingform = (PackingForm)this.Owner;
                                Packingform.packingqty = qtytBx.Text.ToString().Trim();
                                Packingform.IsUnpacking = IsUnpackingcBox.Text.ToString().Trim();
                                Packingform.MarkingIsshieid = MarkingIsshieidcBox.Text.ToString().Trim();
                                Packingform.PrintIsshieid = PrintIsshieidcBox.Text.ToString().Trim();
                                Packingform.GAPIsPrintRun = GAPPrintcBox.Text.ToString().Trim();
                                Packingform.GAPSideIsshieid = GAPSidecBox.Text.ToString().Trim();
                                Packingform.IsPalletizing = IsPalletizingcBox.Text.ToString().Trim();
                                Packingform.Partitiontype = PartitionTypecBox.Text.ToString().Trim();
                                Packingform.IsPackingRun = IsPackingRuncBox.Text.ToString().Trim();
                                Packingform.IsYaMaHaRun = IsYaMaHaRuncBox.Text.ToString().Trim();
                                Packingform.IsSealingRun = IsSealingRuncBox.Text.ToString().Trim();
                                Packingform.IsPalletizingRun = IsPalletizingRuncBox.Text.ToString().Trim();
                                Packingform.IsRepeatCheck = IsCheckcbBox.Text.ToString().Trim();
                                Packingform.IsScanRun = IsScanRuncBox.Text.ToString().Trim();
                                Packingform.TiaoMaVal = TiaomatBox.Text.ToString().Trim();
                                Packingform.HookDirection = HookDirectioncBox.Text.ToString().Trim();
                                string Order_No_v = PakingDvg.Rows[index_v].Cells["订单号码"].Value.ToString().Trim();
                                string startcode_v = PakingDvg.Rows[index_v].Cells["从"].Value.ToString().Trim();
                                string Color_v = PakingDvg.Rows[index_v].Cells["Color"].Value.ToString().Trim();
                                string sku_v = PakingDvg.Rows[index_v].Cells["Sku号码"].Value.ToString().Trim();
                                int finishmarknum =int.Parse(PakingDvg.Rows[index_v].Cells["finishmarknumber"].Value.ToString().Trim());
                                int StartNum = int.Parse(startcode_v) + finishmarknum;
                                int IsPressDown_v = CsMarking.GetCatornParameter(IsPressDowncBox.Text.ToString().Trim());
                                if (IsPressDown_v != 1)
                                {
                                    IsPressDown_v = 0;
                                }
                                if (packingqty == "0")
                                {
                                    MessageBox.Show("装箱数量不能为0");
                                    return false;
                                }
                                // 判断是否超订单数
                                bool b_IsExceedPoNumber =Chekin.IsGAPExceedPoNumber(int.Parse(packingqty), PakingDvg.Rows[index_v].Cells["订单号码"].Value.ToString().Trim(), PakingDvg.Rows[index_v].Cells["从"].Value.ToString().Trim());
                                if (b_IsExceedPoNumber)
                                {
                                    //DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                    //if (diaIsExceedPoNumber == DialogResult.Cancel)
                                    //{
                                    //    return false;
                                    //}
                                    MessageBox.Show("不允许超订单数");
                                    return false;
                                }
                                //加入排单表中
                               CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO SchedulingTable (po_guid,packingqty,IsUnpacking,MarkingIsshieid,PrintIsshieid,GAPIsPrintRun,IsPalletizing,Partitiontype,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,IsScanRun,TiaoMaVal,HookDirection,Entrytime,Long,Width,Heghit,TntervalTop,TntervalLeft,CartonWeight,MarkPattern,CatornType,IsRotate,LabelingType,ColumnNunber,Layernum,Thick,GoodWeight,Orientation,LineNum,potype,DeviceName,订单号码,从,Warehouse,Set_Code,Color,起始箱号,ABPartitiontype,IsPressDown,读取条码模式,SKU,sort,PoTypeSignal,GAPSideIsshieid,GAPSideStartNum,IsRepeatCheck) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}','{43}','{44}','{45}','{46}','{47}','{48}')", Poguid_v, qtytBx.Text.ToString().Trim(), IsUnpackingcBox.Text.ToString().Trim(), MarkingIsshieidcBox.Text.ToString().Trim(), PrintIsshieidcBox.Text.ToString().Trim(), GAPPrintcBox.Text.ToString().Trim(), IsPalletizingcBox.Text.ToString().Trim(), PartitionTypecBox.Text.ToString().Trim(), IsPackingRuncBox.Text.ToString().Trim(), IsYaMaHaRuncBox.Text.ToString().Trim(), IsSealingRuncBox.Text.ToString().Trim(), IsPalletizingRuncBox.Text.ToString().Trim(), IsScanRuncBox.Text.ToString().Trim(), TiaomatBox.Text.ToString().Trim(), HookDirectioncBox.Text.ToString().Trim(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, GoodWeight_v, Orientation_v, LineNum_v,"GAP",CSReadPlc.DevName, Order_No_v, startcode_v,"","", Color_v,StartNum,PartitionType_v, IsPressDown_v,Sample_Code_v,sku_v,CsGetData.GetMaxSort(CSReadPlc.DevName),"3", GAPSidecBox.Text.ToString().Trim(),GAPSideStartNumtBox.Text.Trim(), IsRepeatCheck_v));
                                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET TiaoMaVal='{0}',GAPSideStartNum='{1}' WHERE guid='{2}'", TiaomatBox.Text.ToString().Trim(), GAPSideStartNumtBox.Text.Trim(), PakingDvg.Rows[index_v].Cells["guid"].Value.ToString().Trim()));
                                MessageBox.Show("已加入待换单队列表中");
                                Packingform.NewPakingShow();
                            }
                        }
                        else
                        {
                            MessageBox.Show("提示：没有可执行的数据");
                            return false;
                        }
                    }
                    #endregion
                    //注意此处一定要在这里释放窗体，不然以上设置当前订单会提示对象没有实例
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    return false;
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }
        /// <summary>
        /// GAP排单加入队列设置
        /// </summary>
        public bool GAPScheduling(DataGridView GAPSchedulingDvg_)
        {
            try
            {
                //检查设备是否启动
                //if (!CsRWPlc.mCdata.IsConnect())
                //{
                //    MessageBox.Show("请先启动设备再切换单");
                //    return;
                //}
                List<Control> controls_list1 = new List<Control>();
                List<Control> controls_list2 = new List<Control>();
                Control[] controls_shuzu1 = { longtBox, widthtBox, heighttBox, toptBox, lefttBox, LayernumtBox, ThicktBox, ColumntBox,TiaomatBox, qtytBx, LinetBox, LinetBox };
                Control[] controls_shuzu2 = { MarkPatterncBox, CatornTypecBox, IsRotatetBox, LabelingTypecbox, PartitionTypecBox, OrientationcBox, OrientationcBox };
                foreach (Control ctrl in controls_shuzu1)
                {
                    controls_list1.Add(ctrl);
                }
                foreach (Control ctrl in controls_shuzu2)
                {
                    controls_list2.Add(ctrl);
                }
                string GAPPrint_v = GAPSidecBox.Text.ToString().Trim();
                if (GAPPrint_v == "运行")
                {
                    if (string.IsNullOrEmpty(GAPSideStartNumtBox.Text.ToString()))
                    {
                        CsFormShow.MessageBoxFormShow("运行网单标签必须输入起始序号");
                        return false;
                    }
                }
                if (Chekin.Chekedin(this, controls_list1,"") && Chekin.Chekedin_notnull(this, controls_list2,""))
                {
                    #region 计算垛板单位数量
                    //M_stcPalletizingData.strCatornLong = long_v.ToString();
                    //M_stcPalletizingData.strCatornWidth = width_v.ToString();
                    //M_stcPalletizingData.strCatornHeight = height_v.ToString();
                    //M_stcPalletizingData.strWsizeFlag = "start";
                    //TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                    //while (M_stcPalletizingData.ReadPalletizingflag != "1")
                    //{
                    //    TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                    //    TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                    //    if (ts.Seconds > 1)
                    //    {
                    //        break;
                    //    }
                    //}
                    //M_stcPalletizingData.strResetFlag = "start";
                    #endregion
                    #region 设置为当前订单
                        if (GAPSchedulingDvg_.Rows.Count > 0 && GAPSchedulingDvg_.SelectedRows.Count > 0)
                        {
                            Palletizing_UnittBox.Text = M_stcPalletizingData.Palletizing_Unit.ToString();
                            bool isindex_v; int index_v;
                            CsFormShow.DvgGetcurrentIndex(GAPSchedulingDvg_, out isindex_v, out index_v);
                            if (isindex_v)
                            {                    
                                
                                GetContrlVal("GAP", GAPSchedulingDvg_.Rows[index_v].Cells["po_guid"].Value.ToString().Trim());
                                // CsFormShow csformShow = new CsFormShow();
                                // csformShow.SetPackingQty(index_v, GAPSchedulingDvg_, this, "箱数", M_stcPalletizingData.Palletizing_Unit.ToString());
                                DataGridViewRow DGVROW = GAPSchedulingDvg_.Rows[index_v];
                                SchedulingTableform = (SchedulingTableForm)this.Owner;
                                SchedulingTableform.packingqty = qtytBx.Text.ToString().Trim();
                                SchedulingTableform.IsUnpacking = IsUnpackingcBox.Text.ToString().Trim();
                                SchedulingTableform.MarkingIsshieid = MarkingIsshieidcBox.Text.ToString().Trim();
                                SchedulingTableform.PrintIsshieid = PrintIsshieidcBox.Text.ToString().Trim();
                                SchedulingTableform.GAPIsPrintRun = GAPPrintcBox.Text.ToString().Trim();
                                SchedulingTableform.GAPSideIsshieid = GAPSidecBox.Text.ToString().Trim();
                                SchedulingTableform.IsPalletizing = IsPalletizingcBox.Text.ToString().Trim();
                                SchedulingTableform.Partitiontype = PartitionTypecBox.Text.ToString().Trim();
                                SchedulingTableform.IsPackingRun = IsPackingRuncBox.Text.ToString().Trim();
                                SchedulingTableform.IsYaMaHaRun = IsYaMaHaRuncBox.Text.ToString().Trim();
                                SchedulingTableform.IsSealingRun = IsSealingRuncBox.Text.ToString().Trim();
                                SchedulingTableform.IsPalletizingRun = IsPalletizingRuncBox.Text.ToString().Trim();
                                SchedulingTableform.IsRepeatCheck = IsCheckcbBox.Text.ToString().Trim();
                                SchedulingTableform.IsScanRun = IsScanRuncBox.Text.ToString().Trim();
                                SchedulingTableform.TiaoMaVal = TiaomatBox.Text.ToString().Trim();
                                SchedulingTableform.HookDirection = HookDirectioncBox.Text.ToString().Trim();
                                string Order_No_v = GAPSchedulingDvg_.Rows[index_v].Cells["订单号码"].Value.ToString().Trim();
                                string startcode_v = GAPSchedulingDvg_.Rows[index_v].Cells["从"].Value.ToString().Trim();
                                string Color_v = GAPSchedulingDvg_.Rows[index_v].Cells["Color"].Value.ToString().Trim();
                                int IsPressDown_v = CsMarking.GetCatornParameter(IsPressDowncBox.Text.ToString().Trim());
                                int finishmarknum =int.Parse(CsGetData.GetGAPMarkingNumber( GAPSchedulingDvg_.Rows[index_v].Cells["po_guid"].Value.ToString().Trim()));
                                int StartNum = int.Parse(startcode_v) + finishmarknum;
                            if (IsPressDown_v != 1)
                                {
                                    IsPressDown_v = 0;
                                }
                                if (packingqty == "0")
                                {
                                    MessageBox.Show("装箱数量不能为0");
                                    return false;
                                }
                                #region 判断是否超订单数
                            bool b_IsExceedPoNumber =Chekin.IsGAPExceedPoNumber(int.Parse(packingqty), GAPSchedulingDvg_.Rows[index_v].Cells["订单号码"].Value.ToString().Trim(), GAPSchedulingDvg_.Rows[index_v].Cells["从"].Value.ToString().Trim());
                            if (b_IsExceedPoNumber)
                            {
                                //DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                //if (diaIsExceedPoNumber == DialogResult.Cancel)
                                //{
                                //    return false;
                                //}
                                MessageBox.Show("不允许超订单数量！！");
                                return false;
                            }
                            //加入带切换订单临时队列表
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.SchedulingTable set packingqty='{0}',IsUnpacking='{1}',MarkingIsshieid='{2}',PrintIsshieid='{3}',GAPIsPrintRun='{4}',IsPalletizing='{5}',Partitiontype='{6}',IsPackingRun='{7}',IsYaMaHaRun='{8}',IsSealingRun='{9}',IsPalletizingRun='{10}',IsScanRun='{11}',TiaoMaVal='{12}',HookDirection='{13}',Entrytime='{14}',Long='{15}',Width='{16}',Heghit='{17}',TntervalTop='{18}',TntervalLeft='{19}',CartonWeight='{20}',MarkPattern='{21}',CatornType='{22}',IsRotate='{23}',LabelingType='{24}',ColumnNunber='{25}',Layernum='{26}',Thick='{27}',GoodWeight='{28}',Orientation='{29}',LineNum='{30}',potype='{31}',DeviceName='{32}',ABPartitiontype='{33}',IsPressDown='{34}',起始箱号='{35}',GAPSideIsshieid='{36}',GAPSideStartNum='{37}',IsRepeatCheck='{38}' WHERE guid='{39}'", qtytBx.Text.ToString().Trim(), IsUnpackingcBox.Text.ToString().Trim(), MarkingIsshieidcBox.Text.ToString().Trim(), PrintIsshieidcBox.Text.ToString().Trim(), GAPPrintcBox.Text.ToString().Trim(), IsPalletizingcBox.Text.ToString().Trim(), PartitionTypecBox.Text.ToString().Trim(), IsPackingRuncBox.Text.ToString().Trim(), IsYaMaHaRuncBox.Text.ToString().Trim(), IsSealingRuncBox.Text.ToString().Trim(), IsPalletizingRuncBox.Text.ToString().Trim(), IsScanRuncBox.Text.ToString().Trim(), TiaomatBox.Text.ToString().Trim(), HookDirectioncBox.Text.ToString().Trim(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, GoodWeight_v, Orientation_v, LineNum_v, "GAP", CSReadPlc.DevName,ABPartitiontype_v, IsPressDown_v, StartNum,GAPSidecBox.Text.ToString().Trim(),GAPSideStartNumtBox.Text.Trim(), IsCheckcbBox.Text.ToString().Trim(), GAPSchedulingDvg_.Rows[index_v].Cells["guid"].Value.ToString().Trim()));
                                 CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET TiaoMaVal='{0}',GAPSideStartNum='{1}' WHERE guid='{2}'", TiaomatBox.Text.ToString().Trim(),GAPSideStartNumtBox.Text.Trim(), GAPSchedulingDvg_.Rows[index_v].Cells["po_guid"].Value.ToString().Trim()));
                               CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET CurrentPackingNum = '{0}', IsUnpacking = '{1}', MarkingIsshieid = '{2}', PrintIsshieid = '{3}', GAPIsPrintRun = '{4}', IsPalletizing = '{5}', Partitiontype = '{6}', IsPackingRun = '{7}', IsYaMaHaRun = '{8}', IsSealingRun = '{9}', IsPalletizingRun = '{10}', IsScanRun = '{11}', TiaoMaVal = '{12}', HookDirection = '{13}', GAPSideIsshieid = '{14}' WHERE packing_guid = '{15}'", qtytBx.Text.ToString().Trim(), IsUnpackingcBox.Text.ToString().Trim(), MarkingIsshieidcBox.Text.ToString().Trim(), PrintIsshieidcBox.Text.ToString().Trim(), GAPPrintcBox.Text.ToString().Trim(), IsPalletizingcBox.Text.ToString().Trim(), PartitionTypecBox.Text.ToString().Trim(), IsPackingRuncBox.Text.ToString().Trim(), IsYaMaHaRuncBox.Text.ToString().Trim(), IsSealingRuncBox.Text.ToString().Trim(), IsPalletizingRuncBox.Text.ToString().Trim(), IsScanRuncBox.Text.ToString().Trim(), TiaomatBox.Text.ToString().Trim(), HookDirectioncBox.Text.ToString().Trim(),GAPSidecBox.Text.ToString().Trim() ,GAPSchedulingDvg_.Rows[index_v].Cells["po_guid"].Value.ToString().Trim()));
                                 MessageBox.Show("设置完成");
                                SchedulingTableform.ProduceNewShow();
                                #endregion
                            }
                        }
                        else
                        {
                            MessageBox.Show("提示：没有可执行的数据");
                            return false;
                        }
                    #endregion
                    //注意此处一定要在这里释放窗体，不然以上设置当前订单会提示对象没有实例
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    return false;
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }
        /// <summary>
        /// YYK/GU设置
        /// </summary>
        public bool YYKGUScheduingSet()
        {
            try
            {
                List<Control> controls_list1 = new List<Control>();
                List<Control> controls_list2 = new List<Control>();
                Control[] controls_shuzu1 = { longtBox, widthtBox, heighttBox, ColumntBox, qtytBx };
                Control[] controls_shuzu2 = { IsRotatetBox, OrientationcBox };
                foreach (Control ctrl in controls_shuzu1)
                {
                    controls_list1.Add(ctrl);
                }
                foreach (Control ctrl in controls_shuzu2)
                {
                    controls_list2.Add(ctrl);
                }
                if (Chekin.Chekedin(this, controls_list1,"") && Chekin.Chekedin_notnull(this, controls_list2,""))
                {
                    if (SchedulingdGV.Rows.Count > 0 && SchedulingdGV.SelectedRows.Count > 0)
                    {
                        bool isindex_v; int index_v;
                        CsFormShow.DvgGetcurrentIndex(SchedulingdGV, out isindex_v, out index_v);
                        if (isindex_v)
                        {
                            GetContrlVal(SchedulingdGV.Rows[index_v].Cells["potype"].Value.ToString().Trim(), SchedulingdGV.Rows[index_v].Cells["guid"].Value.ToString().Trim());
                            //判断是否设置装箱数量
                            // CsFormShow csformShow = new CsFormShow();
                            // csformShow.SetPackingQty(index_v, yykDgv, this, "Quantity", M_stcPalletizingData.Palletizing_Unit.ToString());
                            if (packingqty.Trim() == "0")
                            {
                                MessageBox.Show("提示：请重新设置装箱数量");
                                return false;
                            }
                            #region 判断是否超订单数
                            bool b_IsExceedPoNumber = Chekin.IsExceedYYKGUPoNumber(int.Parse(packingqty), SchedulingdGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), SchedulingdGV.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim(), SchedulingdGV.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim());
                            if (b_IsExceedPoNumber)
                            {
                                DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                if (diaIsExceedPoNumber == DialogResult.Cancel)
                                {
                                    return false;
                                }
                            }
                            //加入带切换订单临时队列表
                            Youyikuform = (YouyikuForm)this.Owner;
                            Youyikuform.packingqty = qtytBx.Text.ToString().Trim();
                            Youyikuform.IsUnpacking = IsUnpackingcBox.Text.ToString().Trim();
                            Youyikuform.MarkingIsshieid = MarkingIsshieidcBox.Text.ToString().Trim();
                            Youyikuform.PrintIsshieid = PrintIsshieidcBox.Text.ToString().Trim();
                            Youyikuform.IsPalletizing = IsPalletizingcBox.Text.ToString().Trim();
                            Youyikuform.Partitiontype = PartitionTypecBox.Text.ToString().Trim();
                            Youyikuform.IsPackingRun = IsPackingRuncBox.Text.ToString().Trim();
                            Youyikuform.IsYaMaHaRun = IsYaMaHaRuncBox.Text.ToString().Trim();
                            Youyikuform.IsSealingRun = IsSealingRuncBox.Text.ToString().Trim();
                            Youyikuform.IsPalletizingRun = IsPalletizingRuncBox.Text.ToString().Trim();
                            Youyikuform.IsRepeatCheck = IsCheckcbBox.Text.ToString().Trim();
                            Youyikuform.IsScanRun = IsScanRuncBox.Text.ToString().Trim();
                            Youyikuform.HookDirection = HookDirectioncBox.Text.ToString().Trim();
                            int IsPressDown_v = CsMarking.GetCatornParameter( IsPressDowncBox.Text.ToString().Trim());
                            string PoTypeSignal_v = "";
                            if (IsPressDown_v!=1)
                            {
                                IsPressDown_v = 0;
                            }
                            if (SchedulingdGV.Rows[index_v].Cells["potype"].Value.ToString().Trim().Contains("UNIQLO"))
                            {
                                PoTypeSignal_v = "1";
                            }
                            else
                            {
                                PoTypeSignal_v = "2";
                            }
                            string Order_No_v = SchedulingdGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim();
                            string Warehouse_v = SchedulingdGV.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim();
                            string Set_Code_v = SchedulingdGV.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim();
                            string Color_v = SchedulingdGV.Rows[index_v].Cells["Color"].Value.ToString().Trim();
                            //CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO YYKGUSchedulingTable (po_guid,packingqty,IsUnpacking,MarkingIsshieid,PrintIsshieid,GAPIsPrintRun,IsPalletizing,Partitiontype,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,IsScanRun,TiaoMaVal,HookDirection,Entrytime,Long,Width,Heghit,TntervalTop,TntervalLeft,CartonWeight,MarkPattern,CatornType,IsRotate,LabelingType,ColumnNunber,Layernum,Thick,GoodWeight,Orientation,LineNum,DeviceName,IsRepeatCheck) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}')", SchedulingdGV.Rows[index_v].Cells["guid"].Value.ToString().Trim(), qtytBx.Text.ToString().Trim(), IsUnpackingcBox.Text.ToString().Trim(), MarkingIsshieidcBox.Text.ToString().Trim(), PrintIsshieidcBox.Text.ToString().Trim(), "不运行", IsPalletizingcBox.Text.ToString().Trim(), PartitionTypecBox.Text.ToString().Trim(), IsPackingRuncBox.Text.ToString().Trim(), IsYaMaHaRuncBox.Text.ToString().Trim(), IsSealingRuncBox.Text.ToString().Trim(), IsPalletizingRuncBox.Text.ToString().Trim(), IsScanRuncBox.Text.ToString().Trim(), SchedulingdGV.Rows[index_v].Cells["SKU_Code"].Value.ToString().Trim(), HookDirectioncBox.Text.ToString().Trim(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, GoodWeight_v, Orientation_v, LineNum_v,CSReadPlc.DevName,IsRepeatCheck_v));
                              CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO SchedulingTable (po_guid,packingqty,IsUnpacking,MarkingIsshieid,PrintIsshieid,GAPIsPrintRun,IsPalletizing,Partitiontype,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,IsScanRun,TiaoMaVal,HookDirection,Entrytime,Long,Width,Heghit,TntervalTop,TntervalLeft,CartonWeight,MarkPattern,CatornType,IsRotate,LabelingType,ColumnNunber,Layernum,Thick,GoodWeight,Orientation,LineNum,potype,DeviceName,IsRepeatCheck,订单号码,从,Warehouse,Set_Code,Color,ABPartitiontype,IsPressDown,读取条码模式,sort,PoTypeSignal,GAPSideIsshieid) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}','{43}','{44}','{45}')", SchedulingdGV.Rows[index_v].Cells["guid"].Value.ToString().Trim(), qtytBx.Text.ToString().Trim(), IsUnpackingcBox.Text.ToString().Trim(), MarkingIsshieidcBox.Text.ToString().Trim(), PrintIsshieidcBox.Text.ToString().Trim(), "不运行", IsPalletizingcBox.Text.ToString().Trim(), PartitionTypecBox.Text.ToString().Trim(), IsPackingRuncBox.Text.ToString().Trim(), IsYaMaHaRuncBox.Text.ToString().Trim(), IsSealingRuncBox.Text.ToString().Trim(), IsPalletizingRuncBox.Text.ToString().Trim(), IsScanRuncBox.Text.ToString().Trim(), SchedulingdGV.Rows[index_v].Cells["SKU_Code"].Value.ToString().Trim(), HookDirectioncBox.Text.ToString().Trim(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, GoodWeight_v, Orientation_v, LineNum_v, SchedulingdGV.Rows[index_v].Cells["potype"].Value.ToString().Trim(), CSReadPlc.DevName, IsRepeatCheck_v, Order_No_v,"", Warehouse_v, Set_Code_v, Color_v,ABPartitiontype_v, IsPressDown_v,Sample_Code_v,CsGetData.GetMaxSort(CSReadPlc.DevName), PoTypeSignal_v,"不运行"));
                            #endregion
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }
        /// <summary>
        /// YYK/GU设置
        /// </summary>
        public bool YYKGUScheduingSet(DataGridView ScheduingDvg_)
        {
            try
            {
                List<Control> controls_list1 = new List<Control>();
                List<Control> controls_list2 = new List<Control>();
                Control[] controls_shuzu1 = { longtBox, widthtBox, heighttBox, ColumntBox, qtytBx };
                Control[] controls_shuzu2 = { IsRotatetBox, OrientationcBox };
                foreach (Control ctrl in controls_shuzu1)
                {
                    controls_list1.Add(ctrl);
                }
                foreach (Control ctrl in controls_shuzu2)
                {
                    controls_list2.Add(ctrl);
                }
                if (Chekin.Chekedin(this, controls_list1,"") && Chekin.Chekedin_notnull(this, controls_list2,""))
                {
                    if (ScheduingDvg_.Rows.Count > 0 && ScheduingDvg_.SelectedRows.Count > 0)
                    {
                        bool isindex_v; int index_v;
                        CsFormShow.DvgGetcurrentIndex(ScheduingDvg_, out isindex_v, out index_v);
                        if (isindex_v)
                        {
                            GetContrlVal(ScheduingDvg_.Rows[index_v].Cells["potype"].Value.ToString().Trim(), SchedulingdGV.Rows[index_v].Cells["po_guid"].Value.ToString().Trim());
                            //判断是否设置装箱数量
                            // CsFormShow csformShow = new CsFormShow();
                            // csformShow.SetPackingQty(index_v, ScheduingDvg_, this, "Quantity", M_stcPalletizingData.Palletizing_Unit.ToString());
                            if (packingqty.Trim() == "0")
                            {
                                MessageBox.Show("提示：装箱数量不能为0，请重新设置装箱数量");
                                return false;
                            }
                            #region 判断是否超订单数
                            bool b_IsExceedPoNumber = Chekin.IsExceedYYKGUPoNumber(int.Parse(packingqty), ScheduingDvg_.Rows[index_v].Cells["订单号码"].Value.ToString().Trim(), ScheduingDvg_.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim(), ScheduingDvg_.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim());
                            if (b_IsExceedPoNumber)
                            {
                                DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                if (diaIsExceedPoNumber == DialogResult.Cancel)
                                {
                                    return false;
                                }
                            }
                            //加入带切换订单临时队列表
                            SchedulingTableform = (SchedulingTableForm)this.Owner;
                            SchedulingTableform.packingqty = qtytBx.Text.ToString().Trim();
                            SchedulingTableform.IsUnpacking = IsUnpackingcBox.Text.ToString().Trim();
                            SchedulingTableform.MarkingIsshieid = MarkingIsshieidcBox.Text.ToString().Trim();
                            SchedulingTableform.PrintIsshieid = PrintIsshieidcBox.Text.ToString().Trim();
                            SchedulingTableform.IsPalletizing = IsPalletizingcBox.Text.ToString().Trim();
                            SchedulingTableform.Partitiontype = PartitionTypecBox.Text.ToString().Trim();
                            SchedulingTableform.IsPackingRun = IsPackingRuncBox.Text.ToString().Trim();
                            SchedulingTableform.IsYaMaHaRun = IsYaMaHaRuncBox.Text.ToString().Trim();
                            SchedulingTableform.IsSealingRun = IsSealingRuncBox.Text.ToString().Trim();
                            SchedulingTableform.IsPalletizingRun = IsPalletizingRuncBox.Text.ToString().Trim();
                            SchedulingTableform.IsRepeatCheck = IsCheckcbBox.Text.ToString().Trim();
                            SchedulingTableform.IsScanRun = IsScanRuncBox.Text.ToString().Trim();
                            SchedulingTableform.HookDirection = HookDirectioncBox.Text.ToString().Trim();
                            string Order_No_v = ScheduingDvg_.Rows[index_v].Cells["订单号码"].Value.ToString().Trim();
                            string Warehouse_v = ScheduingDvg_.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim();
                            string Set_Code_v = ScheduingDvg_.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim();
                            string Color_v = ScheduingDvg_.Rows[index_v].Cells["Color"].Value.ToString().Trim();
                            int IsPressDown_v = CsMarking.GetCatornParameter(IsPressDowncBox.Text.ToString().Trim());
                            if (IsPressDown_v != 1)
                            {
                                IsPressDown_v = 0;
                            }
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.SchedulingTable set packingqty='{0}',IsUnpacking='{1}',MarkingIsshieid='{2}',PrintIsshieid='{3}',GAPIsPrintRun='{4}',IsPalletizing='{5}',Partitiontype='{6}',IsPackingRun='{7}',IsYaMaHaRun='{8}',IsSealingRun='{9}',IsPalletizingRun='{10}',IsScanRun='{11}',TiaoMaVal='{12}',HookDirection='{13}',Entrytime='{14}',Long='{15}',Width='{16}',Heghit='{17}',TntervalTop='{18}',TntervalLeft='{19}',CartonWeight='{20}',MarkPattern='{21}',CatornType='{22}',IsRotate='{23}',LabelingType='{24}',ColumnNunber='{25}',Layernum='{26}',Thick='{27}',GoodWeight='{28}',Orientation='{29}',LineNum='{30}',potype='{31}',DeviceName='{32}',IsRepeatCheck='{33}',ABPartitiontype='{35}',IsPressDown='{36}',GAPSideIsshieid='{37}' WHERE guid='{34}'", qtytBx.Text.ToString().Trim(), IsUnpackingcBox.Text.ToString().Trim(), MarkingIsshieidcBox.Text.ToString().Trim(), PrintIsshieidcBox.Text.ToString().Trim(), "不运行", IsPalletizingcBox.Text.ToString().Trim(), PartitionTypecBox.Text.ToString().Trim(), IsPackingRuncBox.Text.ToString().Trim(), IsYaMaHaRuncBox.Text.ToString().Trim(), IsSealingRuncBox.Text.ToString().Trim(), IsPalletizingRuncBox.Text.ToString().Trim(), IsScanRuncBox.Text.ToString().Trim(), ScheduingDvg_.Rows[index_v].Cells["TiaoMaVal"].Value.ToString().Trim(), HookDirectioncBox.Text.ToString().Trim(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, GoodWeight_v, Orientation_v, LineNum_v, ScheduingDvg_.Rows[index_v].Cells["potype"].Value.ToString().Trim(), CSReadPlc.DevName, IsRepeatCheck_v, ScheduingDvg_.Rows[index_v].Cells["guid"].Value.ToString().Trim(),ABPartitiontype_v,IsPressDown_v,GAPSidecBox.Text.ToString().Trim()));
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET CurrentPackingNum='{0}',IsUnpacking='{1}',MarkingIsshieid='{2}',PrintIsshieid='{3}',GAPIsPrintRun='{4}',IsPalletizing='{5}',Partitiontype='{6}',IsPackingRun='{7}',IsYaMaHaRun='{8}',IsSealingRun='{9}',IsPalletizingRun='{10}',IsScanRun='{11}',HookDirection='{12}' WHERE YK_guid='{13}'", qtytBx.Text.ToString().Trim(), IsUnpackingcBox.Text.ToString().Trim(), MarkingIsshieidcBox.Text.ToString().Trim(), PrintIsshieidcBox.Text.ToString().Trim(), "不运行", IsPalletizingcBox.Text.ToString().Trim(), PartitionTypecBox.Text.ToString().Trim(), IsPackingRuncBox.Text.ToString().Trim(), IsYaMaHaRuncBox.Text.ToString().Trim(), IsSealingRuncBox.Text.ToString().Trim(), IsPalletizingRuncBox.Text.ToString().Trim(), IsScanRuncBox.Text.ToString().Trim(), HookDirectioncBox.Text.ToString().Trim(), ScheduingDvg_.Rows[index_v].Cells["po_guid"].Value.ToString().Trim()));
                            MessageBox.Show("设置完成");
                            SchedulingTableform.ProduceNewShow();
                            #endregion
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }

        private void choosebtn_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    if (GAPChoiceRotateform == null || GAPChoiceRotateform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                    {
                        GAPChoiceRotateform = new GAPChoiceRotateForm(this);
                        GAPChoiceRotateform.Owner = this;
                        GAPChoiceRotateform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        GAPChoiceRotateform.ShowDialog();
                    }
                    else
                    {
                        GAPChoiceRotateform.Show();
                        GAPChoiceRotateform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        GAPChoiceRotateform.BringToFront();
                    }
                }
                catch (Exception ex)
                {
                    Cslogfun.WriteToLog(ex);
                    throw new Exception("提示：" + ex);
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void SetQtyForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void CatornbtnSize_Click(object sender, EventArgs e)
        {
            try
            {
                List<Control> controls_list1 = new List<Control>();
                Control[] controls_shuzu1 = { longtBox, widthtBox,heighttBox, CatornStyle_guidtBox,Palletizing_UnittBox };
                foreach (Control ctrl in controls_shuzu1)
                {
                    controls_list1.Add(ctrl);
                }
                if (markpositionform == null || markpositionform.IsDisposed)
                {
                    markpositionform = new MarkPositionForm(SchedulingdGV.Rows[M_YYKDgv_index].Cells["guid"].Value.ToString(), SchedulingdGV, /*SchedulingdGV.Rows[M_YYKDgv_index].Cells["CatornStyle_guid"].Value.ToString()*/"", controls_list1);
                    markpositionform.Owner = this;
                    markpositionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    markpositionform.ShowDialog();
                }
                else
                {
                    markpositionform.ShowDialog();
                    markpositionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    markpositionform.BringToFront();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        public void YYKGUNewShow()
        {
            MarkPatterncBox.Items.Add("不喷色号");
            MarkPatterncBox.Items.Add("喷色号");
            MarkPatterncBox.SelectedIndex = 0;

            CatornTypecBox.Items.Add("大");
            CatornTypecBox.Items.Add("小");
            CatornTypecBox.Items.Add("");
            CatornTypecBox.SelectedIndex = 0;

            LabelingTypecbox.Items.Add("小标签");
            LabelingTypecbox.Items.Add("大标签");
            LabelingTypecbox.Items.Add("");
            LabelingTypecbox.SelectedIndex = 0;

            //PartitionTypecBox.Items.Add("A隔板");
            //PartitionTypecBox.Items.Add("B隔板");
            //PartitionTypecBox.Items.Add("");
            //PartitionTypecBox.SelectedIndex = 0;

            OrientationcBox.Items.Add("正");
            OrientationcBox.Items.Add("反");
            OrientationcBox.Items.Add("");
            OrientationcBox.SelectedIndex = 0;
            IsRotatetBox.ReadOnly = true;
            ColumntBox.ReadOnly = true;
            qtytBox.ReadOnly = true;
            OrientationcBox.Enabled = false;
            LinetBox.ReadOnly = true;
            LayernumtBox.ReadOnly = true;
            MarkPatterncBox.Enabled = false;
            TiaomatBox.Text = "1";
            TiaomatBox.Visible = false;
            label9.Visible = false;
            label14.Visible = false;
            GAPPrintcBox.Visible = false;
            GAPSidecBox.Visible = false;
            label42.Visible = false;
            GAPSideStartNumtBox.Visible = false;
            label43.Visible = false;
            Palletizing_UnittBox.ReadOnly = true;
            OrientationcBox.Enabled = false;
            IsRotatetBox.ReadOnly = true;
            ColumntBox.ReadOnly = true;
            longtBox.ReadOnly = true;
            widthtBox.ReadOnly = true;
            heighttBox.ReadOnly = true;
            label14.Visible = false;
            GAPPrintcBox.Visible = false;
            LinetBox.Text = "1";
            GoodWeighttBox.Text = "1";
            ThicktBox.Text = "50";
            LayernumtBox.Text = "10";
            weighttBox.Text = "1";
            toptBox.Text = "0";
            lefttBox.Text = "0";
            JianspeedtBox.Text = "90";
            GrabspeedtBox.Text = "90";
            LabelingTypecbox.Text = "小标签";
        }
        public void NewShow(DataTable dt_)
        {
            IsUnpackingcBox.Items.Add("不拆包");
            IsUnpackingcBox.Items.Add("拆包");
            if (dt_.Rows[0]["IsUnpacking"].ToString().Contains("不拆包"))
            {
                IsUnpackingcBox.SelectedIndex = 0;
            }
            else
            {
                IsUnpackingcBox.SelectedIndex = 1;
            }
            //前后单码垛一起
            IsPalletizingcBox.Items.Add("不允许");
            IsPalletizingcBox.Items.Add("允许");
            if (dt_.Rows[0]["IsPalletizing"].ToString().Contains("不允许"))
            {
                IsPalletizingcBox.SelectedIndex = 0;
            }
            else
            {
                IsPalletizingcBox.SelectedIndex = 1;
            }
            //扫码
            IsScanRuncBox.Items.Add("不运行");
            IsScanRuncBox.Items.Add("运行");
            if (dt_.Rows[0]["IsScanRun"].ToString().Contains("不运行"))
            {
                IsScanRuncBox.SelectedIndex = 0;
            }
            else
            {
                IsScanRuncBox.SelectedIndex = 1;
            }
            //验针扫二维码
            IsCheckcbBox.Items.Add("不运行");
            IsCheckcbBox.Items.Add("运行");
            if (dt_.Rows[0]["IsRepeatCheck"].ToString().Contains("不运行"))
            {
                IsCheckcbBox.SelectedIndex = 0;
            }
            else
            {
                IsCheckcbBox.SelectedIndex = 1;
            }
            //衣架钩子方向
            HookDirectioncBox.Items.Add("不掉头");
            HookDirectioncBox.Items.Add("掉头");
            if (dt_.Rows[0]["HookDirection"].ToString().Contains("不掉头"))
            {
                HookDirectioncBox.SelectedIndex = 0;
            }
            else
            {
                HookDirectioncBox.SelectedIndex = 1;
            }
            //开箱
            IsPackingRuncBox.Items.Add("不运行");
            IsPackingRuncBox.Items.Add("运行");
            if (dt_.Rows[0]["IsPackingRun"].ToString().Contains("不运行"))
            {
                IsPackingRuncBox.SelectedIndex = 0;
            }
            else
            {
                IsPackingRuncBox.SelectedIndex = 1;
            }
            //雅马哈装箱
            IsYaMaHaRuncBox.Items.Add("不运行");
            IsYaMaHaRuncBox.Items.Add("运行");
            if (dt_.Rows[0]["IsYaMaHaRun"].ToString().Contains("不运行"))
            {
                IsYaMaHaRuncBox.SelectedIndex = 0;
            }
            else
            {
                IsYaMaHaRuncBox.SelectedIndex = 1;
            }
            // IsYaMaHaRuncBox.SelectedIndex = 0;
            //封箱
            IsSealingRuncBox.Items.Add("不运行");
            IsSealingRuncBox.Items.Add("运行");
            if (dt_.Rows[0]["IsSealingRun"].ToString().Contains("不运行"))
            {
                IsSealingRuncBox.SelectedIndex = 0;
            }
            else
            {
                IsSealingRuncBox.SelectedIndex = 1;
            }
            //喷码
            MarkingIsshieidcBox.Items.Add("不运行");
            MarkingIsshieidcBox.Items.Add("运行");
            if (dt_.Rows[0]["MarkingIsshieid"].ToString().Contains("不运行"))
            {
                MarkingIsshieidcBox.SelectedIndex = 0;
            }
            else
            {
                MarkingIsshieidcBox.SelectedIndex = 1;
            }
            // MarkingIsshieidcBox.SelectedIndex = 0;
            //侧面贴标打印
            PrintIsshieidcBox.Items.Add("不运行");
            PrintIsshieidcBox.Items.Add("运行");
            if (dt_.Rows[0]["PrintIsshieid"].ToString().Contains("不运行"))
            {
                PrintIsshieidcBox.SelectedIndex = 0;
            }
            else
            {
                PrintIsshieidcBox.SelectedIndex = 1;
            }
            //拐角贴标打印
            GAPPrintcBox.Items.Add("不运行");
            GAPPrintcBox.Items.Add("运行");
            if (dt_.Rows[0]["GAPIsPrintRun"].ToString().Contains("不运行"))
            {
                GAPPrintcBox.SelectedIndex = 0;
            }
            else
            {
                GAPPrintcBox.SelectedIndex = 1;
            }
            //网单贴标
            GAPSidecBox.Items.Add("不运行");
            GAPSidecBox.Items.Add("运行");
            if (dt_.Rows[0]["GAPSideIsshieid"].ToString().Contains("不运行"))
            {
                GAPSidecBox.SelectedIndex = 0;
            }
            else
            {
                GAPSidecBox.SelectedIndex = 1;
            }
            //码垛
            IsPalletizingRuncBox.Items.Add("不运行");
            IsPalletizingRuncBox.Items.Add("运行");
            if (dt_.Rows[0]["IsPalletizingRun"].ToString().Contains("不运行"))
            {
                IsPalletizingRuncBox.SelectedIndex = 0;
            }
            else
            {
                IsPalletizingRuncBox.SelectedIndex = 1;
            }
            //压箱
            IsPressDowncBox.Items.Add("不运行");
            IsPressDowncBox.Items.Add("运行");
            if (!bool.Parse(dt_.Rows[0]["IsPressDown"].ToString()))
            {
                IsPressDowncBox.SelectedIndex = 0;
            }
            else
            {
                IsPressDowncBox.SelectedIndex = 1;
            }
            //IsPalletizingRuncBox.SelectedIndex = 0;
            //天地盖类型
            PartitionTypecBox.Items.Add("无");
            PartitionTypecBox.Items.Add("天地盖");
            PartitionTypecBox.Items.Add("天盖");
            PartitionTypecBox.Items.Add("地盖");
            if (dt_.Rows[0]["Partitiontype"].ToString().Contains("无"))
            {
                PartitionTypecBox.SelectedIndex = 0;
            }
            else if (dt_.Rows[0]["Partitiontype"].ToString().Contains("天地盖"))
            {
                PartitionTypecBox.SelectedIndex = 1;
            }
            else if (dt_.Rows[0]["Partitiontype"].ToString().Contains("天盖"))
            {
                PartitionTypecBox.SelectedIndex = 2;
            }
            else if (dt_.Rows[0]["Partitiontype"].ToString().Contains("地盖"))
            {
                PartitionTypecBox.SelectedIndex = 3;
            }
            Palletizing_UnittBox.ReadOnly = true;
            ColumntBox.ReadOnly = true;
            IsRotatetBox.ReadOnly = true;
            OrientationcBox.Enabled = false;
            longtBox.ReadOnly = true;
            widthtBox.ReadOnly = true;
            heighttBox.ReadOnly = true;
            string potype = dt_.Rows[0]["potype"].ToString();
            if (potype.Contains("GAP"))
            {
                PrintIsshieidcBox.Visible = false;
                label4.Visible = false;
            }
            else
            {
                TiaomatBox.Visible = false;
                label9.Visible = false;
                GAPPrintcBox.Visible = false;
                label14.Visible = false;
                GAPSidecBox.Visible = false;
                label42.Visible = false;
                GAPSideStartNumtBox.Visible = false;
                label43.Visible = false;
            }
        }
        /// <summary>
        /// GAP排单加入队列设置
        /// </summary>
        public bool SchedulingSort()
        {
            try
            {
                //检查设备是否启动
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return false;
                }
                List<Control> controls_list1 = new List<Control>();
                List<Control> controls_list2 = new List<Control>();
                Control[] controls_shuzu1 = { longtBox, widthtBox, heighttBox, toptBox, lefttBox, LayernumtBox, GoodWeighttBox, ThicktBox, ColumntBox, qtytBx, LinetBox, TiaomatBox, LinetBox };
                Control[] controls_shuzu2 = { MarkPatterncBox, CatornTypecBox, IsRotatetBox, LabelingTypecbox, PartitionTypecBox, weighttBox, OrientationcBox, OrientationcBox };
                foreach (Control ctrl in controls_shuzu1)
                {
                    controls_list1.Add(ctrl);
                }
                foreach (Control ctrl in controls_shuzu2)
                {
                    controls_list2.Add(ctrl);
                }
                string GAPPrint_v = GAPSidecBox.Text.ToString().Trim();
                if (GAPPrint_v == "运行")
                {
                    if (string.IsNullOrEmpty(GAPSideStartNumtBox.Text.ToString()))
                    {
                        CsFormShow.MessageBoxFormShow("运行网单标签必须输入起始序号");
                        return false;
                    }
                }
                if (Chekin.Chekedin(this, controls_list1,"") && Chekin.Chekedin_notnull(this, controls_list2,""))
                {
                    #region 计算垛板单位数量
                    #endregion
                    #region 设置为当前订单
                    DialogResult btchose = MessageBox.Show("是否将订单加入订单队列中", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (btchose == DialogResult.OK)
                    {
                        if (SchedulingdGV.Rows.Count > 0 && SchedulingdGV.SelectedRows.Count > 0)
                        {
                            bool isindex_v; int index_v;
                            CsFormShow.DvgGetcurrentIndex(SchedulingdGV, out isindex_v, out index_v);
                            if (isindex_v)
                            {
                                string Potype_v = SchedulingdGV.Rows[index_v].Cells["potype"].Value.ToString().Trim();
                                GetContrlVal(Potype_v, SchedulingdGV.Rows[index_v].Cells["guid"].Value.ToString().Trim());
                                // CsFormShow csformShow = new CsFormShow();
                                // csformShow.SetPackingQty(index_v, PakingDvg, this, "箱数", M_stcPalletizingData.Palletizing_Unit.ToString());
                                if (Potype_v.Contains("GAP"))
                                {
                                    string Order_No_v = SchedulingdGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim();
                                    string startcode_v = SchedulingdGV.Rows[index_v].Cells["从"].Value.ToString().Trim();
                                    string Color_v = SchedulingdGV.Rows[index_v].Cells["Color"].Value.ToString().Trim();
                                    string sku_v = SchedulingdGV.Rows[index_v].Cells["SKU"].Value.ToString().Trim();
                                    int finishmarknum = int.Parse(SchedulingdGV.Rows[index_v].Cells["完成箱数"].Value.ToString().Trim());
                                    int StartNum = int.Parse(startcode_v) + finishmarknum;
                                    int IsPressDown_v = CsMarking.GetCatornParameter(IsPressDowncBox.Text.ToString().Trim());
                                    if (IsPressDown_v != 1)
                                    {
                                        IsPressDown_v = 0;
                                    }
                                    if (packingqty == "0")
                                    {
                                        MessageBox.Show("装箱数量不能为0");
                                        return false;
                                    }
                                    // 判断是否超订单数
                                    bool b_IsExceedPoNumber = Chekin.IsGAPExceedPoNumber(int.Parse(packingqty), SchedulingdGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), SchedulingdGV.Rows[index_v].Cells["从"].Value.ToString().Trim());
                                    if (b_IsExceedPoNumber)
                                    {
                                        MessageBox.Show("不允许超订单数");
                                        return false;
                                    }
                                    CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO SchedulingTable (po_guid,packingqty,IsUnpacking,MarkingIsshieid,PrintIsshieid,GAPIsPrintRun,IsPalletizing,Partitiontype,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,IsScanRun,TiaoMaVal,HookDirection,Entrytime,Long,Width,Heghit,TntervalTop,TntervalLeft,CartonWeight,MarkPattern,CatornType,IsRotate,LabelingType,ColumnNunber,Layernum,Thick,GoodWeight,Orientation,LineNum,potype,DeviceName,订单号码,从,Warehouse,Set_Code,Color,起始箱号,ABPartitiontype,IsPressDown,读取条码模式,PoTypeSignal,SKU,sort,GAPSideIsshieid,GAPSideStartNum) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}','{43}','{44}','{45}','{46}','{47}')", Poguid_v, qtytBx.Text.ToString().Trim(), IsUnpackingcBox.Text.ToString().Trim(), MarkingIsshieidcBox.Text.ToString().Trim(), PrintIsshieidcBox.Text.ToString().Trim(), GAPPrintcBox.Text.ToString().Trim(), IsPalletizingcBox.Text.ToString().Trim(), PartitionTypecBox.Text.ToString().Trim(), IsPackingRuncBox.Text.ToString().Trim(), IsYaMaHaRuncBox.Text.ToString().Trim(), IsSealingRuncBox.Text.ToString().Trim(), IsPalletizingRuncBox.Text.ToString().Trim(), IsScanRuncBox.Text.ToString().Trim(), TiaomatBox.Text.ToString().Trim(), HookDirectioncBox.Text.ToString().Trim(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, GoodWeight_v, Orientation_v, LineNum_v, "GAP", CSReadPlc.DevName, Order_No_v, startcode_v, "", "", Color_v, StartNum, PartitionType_v, IsPressDown_v, Sample_Code_v,"3", sku_v,CsGetData.GetMaxSort(CSReadPlc.DevName), GAPSidecBox.Text.ToString().Trim(),GAPSideStartNumtBox.Text.Trim()));
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET TiaoMaVal='{0}',GAPSideStartNum='{1}' WHERE guid='{2}'", TiaomatBox.Text.ToString().Trim(), GAPSideStartNumtBox.Text.Trim(), SchedulingdGV.Rows[index_v].Cells["guid"].Value.ToString().Trim()));
                                }
                                else
                                {
                                    if (packingqty.Trim() == "0")
                                    {
                                        MessageBox.Show("提示：请重新设置装箱数量");
                                        return false;
                                    }
                                    #region 判断是否超订单数
                                    bool b_IsExceedPoNumber = Chekin.IsExceedYYKGUPoNumber(int.Parse(packingqty), SchedulingdGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), SchedulingdGV.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim(), SchedulingdGV.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim());
                                    if (b_IsExceedPoNumber)
                                    {
                                        DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                        if (diaIsExceedPoNumber == DialogResult.Cancel)
                                        {
                                            return false;
                                        }
                                    }
                                    //加入带切换订单临时队列表
                                    int IsPressDown_v = CsMarking.GetCatornParameter(IsPressDowncBox.Text.ToString().Trim());
                                    if (IsPressDown_v != 1)
                                    {
                                        IsPressDown_v = 0;
                                    }
                                    string Order_No_v = SchedulingdGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim();
                                    string Warehouse_v = SchedulingdGV.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim();
                                    string Set_Code_v = SchedulingdGV.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim();
                                    string Color_v = SchedulingdGV.Rows[index_v].Cells["Color"].Value.ToString().Trim();
                                    string PoTypeSignal_v = "";
                                    if (SchedulingdGV.Rows[index_v].Cells["potype"].Value.ToString().Trim().Contains("UNIQLO"))
                                    {
                                         PoTypeSignal_v = "1";
                                    }
                                    else
                                    {
                                        PoTypeSignal_v = "2";
                                    }
                                    CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO SchedulingTable (po_guid,packingqty,IsUnpacking,MarkingIsshieid,PrintIsshieid,GAPIsPrintRun,IsPalletizing,Partitiontype,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,IsScanRun,TiaoMaVal,HookDirection,Entrytime,Long,Width,Heghit,TntervalTop,TntervalLeft,CartonWeight,MarkPattern,CatornType,IsRotate,LabelingType,ColumnNunber,Layernum,Thick,GoodWeight,Orientation,LineNum,potype,DeviceName,IsRepeatCheck,订单号码,从,Warehouse,Set_Code,Color,ABPartitiontype,IsPressDown,读取条码模式,PoTypeSignal,sort,GAPSideIsshieid) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}','{43}','{44}','{45}')", SchedulingdGV.Rows[index_v].Cells["guid"].Value.ToString().Trim(), qtytBx.Text.ToString().Trim(), IsUnpackingcBox.Text.ToString().Trim(), MarkingIsshieidcBox.Text.ToString().Trim(), PrintIsshieidcBox.Text.ToString().Trim(), "不运行", IsPalletizingcBox.Text.ToString().Trim(), PartitionTypecBox.Text.ToString().Trim(), IsPackingRuncBox.Text.ToString().Trim(), IsYaMaHaRuncBox.Text.ToString().Trim(), IsSealingRuncBox.Text.ToString().Trim(), IsPalletizingRuncBox.Text.ToString().Trim(), IsScanRuncBox.Text.ToString().Trim(), SchedulingdGV.Rows[index_v].Cells["条形码"].Value.ToString().Trim(), HookDirectioncBox.Text.ToString().Trim(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, GoodWeight_v, Orientation_v, LineNum_v, SchedulingdGV.Rows[index_v].Cells["potype"].Value.ToString().Trim(), CSReadPlc.DevName, IsRepeatCheck_v, Order_No_v, "", Warehouse_v, Set_Code_v, Color_v, ABPartitiontype_v, IsPressDown_v, Sample_Code_v, PoTypeSignal_v,CsGetData.GetMaxSort(CSReadPlc.DevName),"不运行"));
                                    #endregion
                                }
                                MessageBox.Show("已加入待生产队列表中");
                                SchedulingTableform = (SchedulingTableForm)this.Owner;
                                SchedulingTableform.PlanOherNewShow();
                            }
                        }
                        else
                        {
                            MessageBox.Show("提示：没有可执行的数据");
                            return false;
                        }
                    }
                    #endregion
                    //注意此处一定要在这里释放窗体，不然以上设置当前订单会提示对象没有实例
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    return false;
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }
    }
}
