﻿namespace WindowsForms01
{
    partial class SetQtyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.qtytBx = new System.Windows.Forms.TextBox();
            this.okbtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.IsUnpackingcBox = new System.Windows.Forms.ComboBox();
            this.MarkingIsshieidcBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PrintIsshieidcBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.IsPalletizingcBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PartitionTypecBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.IsYaMaHaRuncBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.IsPackingRuncBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.IsPalletizingRuncBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.IsSealingRuncBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TiaomatBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Palletizing_UnittBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.IsScanRuncBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.GAPPrintcBox = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.OrientationcBox = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.LinetBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.CatornStyle_guidtBox = new System.Windows.Forms.TextBox();
            this.GoodWeighttBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.qtytBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.PoguidtBox = new System.Windows.Forms.TextBox();
            this.JianspeedtBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.GrabspeedtBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.ThicktBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.LayernumtBox = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.LabelingTypecbox = new System.Windows.Forms.ComboBox();
            this.CatornTypecBox = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.weighttBox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.ColumntBox = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.choosebtn = new System.Windows.Forms.Button();
            this.IsRotatetBox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.MarkPatterncBox = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.lefttBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.toptBox = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.heighttBox = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.widthtBox = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.longtBox = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.idtBox = new System.Windows.Forms.TextBox();
            this.HookDirectioncBox = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.CatornSizebtn = new System.Windows.Forms.Button();
            this.IsCheckcbBox = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.guidtBox = new System.Windows.Forms.TextBox();
            this.IsPressDowncBox = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.GAPSidecBox = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.GAPSideStartNumtBox = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 198);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "当前装箱数量:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // qtytBx
            // 
            this.qtytBx.Location = new System.Drawing.Point(146, 196);
            this.qtytBx.Name = "qtytBx";
            this.qtytBx.Size = new System.Drawing.Size(127, 25);
            this.qtytBx.TabIndex = 1;
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(422, 348);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(109, 42);
            this.okbtn.TabIndex = 2;
            this.okbtn.Text = "设置";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.okbtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(337, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "是否拆包：";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // IsUnpackingcBox
            // 
            this.IsUnpackingcBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsUnpackingcBox.FormattingEnabled = true;
            this.IsUnpackingcBox.Location = new System.Drawing.Point(423, 20);
            this.IsUnpackingcBox.Name = "IsUnpackingcBox";
            this.IsUnpackingcBox.Size = new System.Drawing.Size(127, 23);
            this.IsUnpackingcBox.TabIndex = 4;
            this.IsUnpackingcBox.SelectedIndexChanged += new System.EventHandler(this.IsUnpackingcBox_SelectedIndexChanged);
            // 
            // MarkingIsshieidcBox
            // 
            this.MarkingIsshieidcBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MarkingIsshieidcBox.FormattingEnabled = true;
            this.MarkingIsshieidcBox.Location = new System.Drawing.Point(422, 103);
            this.MarkingIsshieidcBox.Name = "MarkingIsshieidcBox";
            this.MarkingIsshieidcBox.Size = new System.Drawing.Size(127, 23);
            this.MarkingIsshieidcBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(309, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "是否运行喷码：";
            // 
            // PrintIsshieidcBox
            // 
            this.PrintIsshieidcBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PrintIsshieidcBox.FormattingEnabled = true;
            this.PrintIsshieidcBox.Location = new System.Drawing.Point(699, 59);
            this.PrintIsshieidcBox.Name = "PrintIsshieidcBox";
            this.PrintIsshieidcBox.Size = new System.Drawing.Size(127, 23);
            this.PrintIsshieidcBox.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(562, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "是否运行侧面贴标：";
            // 
            // IsPalletizingcBox
            // 
            this.IsPalletizingcBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsPalletizingcBox.FormattingEnabled = true;
            this.IsPalletizingcBox.Location = new System.Drawing.Point(906, 558);
            this.IsPalletizingcBox.Name = "IsPalletizingcBox";
            this.IsPalletizingcBox.Size = new System.Drawing.Size(127, 23);
            this.IsPalletizingcBox.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(789, 562);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 15);
            this.label5.TabIndex = 9;
            this.label5.Text = "与上一单码垛：";
            // 
            // PartitionTypecBox
            // 
            this.PartitionTypecBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PartitionTypecBox.FormattingEnabled = true;
            this.PartitionTypecBox.Location = new System.Drawing.Point(146, 20);
            this.PartitionTypecBox.Name = "PartitionTypecBox";
            this.PartitionTypecBox.Size = new System.Drawing.Size(127, 23);
            this.PartitionTypecBox.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 15);
            this.label6.TabIndex = 11;
            this.label6.Text = "天地盖类型：";
            // 
            // IsYaMaHaRuncBox
            // 
            this.IsYaMaHaRuncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsYaMaHaRuncBox.FormattingEnabled = true;
            this.IsYaMaHaRuncBox.Location = new System.Drawing.Point(697, 18);
            this.IsYaMaHaRuncBox.Name = "IsYaMaHaRuncBox";
            this.IsYaMaHaRuncBox.Size = new System.Drawing.Size(127, 23);
            this.IsYaMaHaRuncBox.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(581, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 15);
            this.label7.TabIndex = 21;
            this.label7.Text = "是否运行装箱：";
            // 
            // IsPackingRuncBox
            // 
            this.IsPackingRuncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsPackingRuncBox.FormattingEnabled = true;
            this.IsPackingRuncBox.Location = new System.Drawing.Point(422, 61);
            this.IsPackingRuncBox.Name = "IsPackingRuncBox";
            this.IsPackingRuncBox.Size = new System.Drawing.Size(127, 23);
            this.IsPackingRuncBox.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(294, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 15);
            this.label8.TabIndex = 19;
            this.label8.Text = "是否运行开箱机：";
            // 
            // IsPalletizingRuncBox
            // 
            this.IsPalletizingRuncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsPalletizingRuncBox.FormattingEnabled = true;
            this.IsPalletizingRuncBox.Location = new System.Drawing.Point(697, 106);
            this.IsPalletizingRuncBox.Name = "IsPalletizingRuncBox";
            this.IsPalletizingRuncBox.Size = new System.Drawing.Size(127, 23);
            this.IsPalletizingRuncBox.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(579, 108);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 15);
            this.label10.TabIndex = 15;
            this.label10.Text = "是否运行码垛：";
            // 
            // IsSealingRuncBox
            // 
            this.IsSealingRuncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsSealingRuncBox.FormattingEnabled = true;
            this.IsSealingRuncBox.Location = new System.Drawing.Point(921, 413);
            this.IsSealingRuncBox.Name = "IsSealingRuncBox";
            this.IsSealingRuncBox.Size = new System.Drawing.Size(127, 23);
            this.IsSealingRuncBox.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(789, 415);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(127, 15);
            this.label11.TabIndex = 13;
            this.label11.Text = "是否运行封箱机：";
            // 
            // TiaomatBox
            // 
            this.TiaomatBox.Location = new System.Drawing.Point(424, 196);
            this.TiaomatBox.Name = "TiaomatBox";
            this.TiaomatBox.Size = new System.Drawing.Size(127, 25);
            this.TiaomatBox.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(343, 198);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 15);
            this.label9.TabIndex = 23;
            this.label9.Text = "GAP条码:";
            // 
            // Palletizing_UnittBox
            // 
            this.Palletizing_UnittBox.Location = new System.Drawing.Point(121, 647);
            this.Palletizing_UnittBox.Name = "Palletizing_UnittBox";
            this.Palletizing_UnittBox.Size = new System.Drawing.Size(127, 25);
            this.Palletizing_UnittBox.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(30, 651);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 15);
            this.label12.TabIndex = 25;
            this.label12.Text = "数量/垛板:";
            // 
            // IsScanRuncBox
            // 
            this.IsScanRuncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsScanRuncBox.FormattingEnabled = true;
            this.IsScanRuncBox.Location = new System.Drawing.Point(146, 61);
            this.IsScanRuncBox.Name = "IsScanRuncBox";
            this.IsScanRuncBox.Size = new System.Drawing.Size(127, 23);
            this.IsScanRuncBox.TabIndex = 28;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(27, 63);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 15);
            this.label13.TabIndex = 27;
            this.label13.Text = "是否运行扫码：";
            // 
            // GAPPrintcBox
            // 
            this.GAPPrintcBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GAPPrintcBox.FormattingEnabled = true;
            this.GAPPrintcBox.Location = new System.Drawing.Point(699, 61);
            this.GAPPrintcBox.Name = "GAPPrintcBox";
            this.GAPPrintcBox.Size = new System.Drawing.Size(127, 23);
            this.GAPPrintcBox.TabIndex = 30;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(551, 67);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(142, 15);
            this.label14.TabIndex = 29;
            this.label14.Text = "是否运行拐角贴标：";
            // 
            // OrientationcBox
            // 
            this.OrientationcBox.FormattingEnabled = true;
            this.OrientationcBox.Location = new System.Drawing.Point(146, 235);
            this.OrientationcBox.Name = "OrientationcBox";
            this.OrientationcBox.Size = new System.Drawing.Size(127, 23);
            this.OrientationcBox.TabIndex = 170;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(59, 242);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 15);
            this.label23.TabIndex = 169;
            this.label23.Text = "正反方向:";
            // 
            // LinetBox
            // 
            this.LinetBox.Location = new System.Drawing.Point(86, 507);
            this.LinetBox.Name = "LinetBox";
            this.LinetBox.Size = new System.Drawing.Size(148, 25);
            this.LinetBox.TabIndex = 168;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(16, 510);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 15);
            this.label22.TabIndex = 167;
            this.label22.Text = "摆货行数";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(18, 535);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(135, 15);
            this.label21.TabIndex = 166;
            this.label21.Text = "CatornStyle_guid";
            this.label21.Visible = false;
            // 
            // CatornStyle_guidtBox
            // 
            this.CatornStyle_guidtBox.Location = new System.Drawing.Point(21, 553);
            this.CatornStyle_guidtBox.Name = "CatornStyle_guidtBox";
            this.CatornStyle_guidtBox.Size = new System.Drawing.Size(148, 25);
            this.CatornStyle_guidtBox.TabIndex = 165;
            this.CatornStyle_guidtBox.Visible = false;
            // 
            // GoodWeighttBox
            // 
            this.GoodWeighttBox.Location = new System.Drawing.Point(536, 471);
            this.GoodWeighttBox.Name = "GoodWeighttBox";
            this.GoodWeighttBox.Size = new System.Drawing.Size(148, 25);
            this.GoodWeighttBox.TabIndex = 164;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(461, 474);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 15);
            this.label20.TabIndex = 163;
            this.label20.Text = "产品重量";
            // 
            // qtytBox
            // 
            this.qtytBox.Location = new System.Drawing.Point(310, 507);
            this.qtytBox.Name = "qtytBox";
            this.qtytBox.Size = new System.Drawing.Size(148, 25);
            this.qtytBox.TabIndex = 162;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(267, 510);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 15);
            this.label19.TabIndex = 161;
            this.label19.Text = "箱数";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(241, 558);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 15);
            this.label15.TabIndex = 160;
            this.label15.Text = "po_guid";
            this.label15.Visible = false;
            // 
            // PoguidtBox
            // 
            this.PoguidtBox.Location = new System.Drawing.Point(342, 553);
            this.PoguidtBox.Name = "PoguidtBox";
            this.PoguidtBox.Size = new System.Drawing.Size(148, 25);
            this.PoguidtBox.TabIndex = 159;
            this.PoguidtBox.Visible = false;
            // 
            // JianspeedtBox
            // 
            this.JianspeedtBox.Location = new System.Drawing.Point(101, 602);
            this.JianspeedtBox.Name = "JianspeedtBox";
            this.JianspeedtBox.Size = new System.Drawing.Size(148, 25);
            this.JianspeedtBox.TabIndex = 158;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(32, 605);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 15);
            this.label16.TabIndex = 157;
            this.label16.Text = "减速度";
            // 
            // GrabspeedtBox
            // 
            this.GrabspeedtBox.Location = new System.Drawing.Point(328, 602);
            this.GrabspeedtBox.Name = "GrabspeedtBox";
            this.GrabspeedtBox.Size = new System.Drawing.Size(148, 25);
            this.GrabspeedtBox.TabIndex = 156;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(261, 605);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 15);
            this.label17.TabIndex = 155;
            this.label17.Text = "抓取速度";
            // 
            // ThicktBox
            // 
            this.ThicktBox.Location = new System.Drawing.Point(536, 507);
            this.ThicktBox.Name = "ThicktBox";
            this.ThicktBox.Size = new System.Drawing.Size(148, 25);
            this.ThicktBox.TabIndex = 154;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(467, 510);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 15);
            this.label18.TabIndex = 153;
            this.label18.Text = "产品厚";
            // 
            // LayernumtBox
            // 
            this.LayernumtBox.Location = new System.Drawing.Point(86, 440);
            this.LayernumtBox.Name = "LayernumtBox";
            this.LayernumtBox.Size = new System.Drawing.Size(148, 25);
            this.LayernumtBox.TabIndex = 152;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(11, 427);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(67, 15);
            this.label24.TabIndex = 151;
            this.label24.Text = "箱内件数";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(536, 442);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(148, 23);
            this.comboBox1.TabIndex = 150;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(467, 445);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 15);
            this.label25.TabIndex = 149;
            this.label25.Text = "隔板选择";
            // 
            // LabelingTypecbox
            // 
            this.LabelingTypecbox.FormattingEnabled = true;
            this.LabelingTypecbox.Location = new System.Drawing.Point(561, 605);
            this.LabelingTypecbox.Name = "LabelingTypecbox";
            this.LabelingTypecbox.Size = new System.Drawing.Size(148, 23);
            this.LabelingTypecbox.TabIndex = 148;
            // 
            // CatornTypecBox
            // 
            this.CatornTypecBox.FormattingEnabled = true;
            this.CatornTypecBox.Location = new System.Drawing.Point(573, 556);
            this.CatornTypecBox.Name = "CatornTypecBox";
            this.CatornTypecBox.Size = new System.Drawing.Size(148, 23);
            this.CatornTypecBox.TabIndex = 147;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(493, 608);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 15);
            this.label26.TabIndex = 146;
            this.label26.Text = "贴标选择";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(504, 558);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 15);
            this.label27.TabIndex = 145;
            this.label27.Text = "纸箱大小";
            // 
            // weighttBox
            // 
            this.weighttBox.Location = new System.Drawing.Point(310, 440);
            this.weighttBox.Name = "weighttBox";
            this.weighttBox.Size = new System.Drawing.Size(148, 25);
            this.weighttBox.TabIndex = 144;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(235, 443);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(67, 15);
            this.label28.TabIndex = 143;
            this.label28.Text = "纸箱重量";
            // 
            // ColumntBox
            // 
            this.ColumntBox.Location = new System.Drawing.Point(424, 239);
            this.ColumntBox.Name = "ColumntBox";
            this.ColumntBox.Size = new System.Drawing.Size(127, 25);
            this.ColumntBox.TabIndex = 187;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(337, 243);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(75, 15);
            this.label29.TabIndex = 186;
            this.label29.Text = "摆货列数:";
            // 
            // choosebtn
            // 
            this.choosebtn.Location = new System.Drawing.Point(832, 231);
            this.choosebtn.Name = "choosebtn";
            this.choosebtn.Size = new System.Drawing.Size(115, 38);
            this.choosebtn.TabIndex = 185;
            this.choosebtn.Text = "选旋转和列数";
            this.choosebtn.UseVisualStyleBackColor = true;
            this.choosebtn.Click += new System.EventHandler(this.choosebtn_Click);
            // 
            // IsRotatetBox
            // 
            this.IsRotatetBox.Location = new System.Drawing.Point(697, 240);
            this.IsRotatetBox.Name = "IsRotatetBox";
            this.IsRotatetBox.Size = new System.Drawing.Size(129, 25);
            this.IsRotatetBox.TabIndex = 184;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(616, 243);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(75, 15);
            this.label30.TabIndex = 183;
            this.label30.Text = "纸箱旋转:";
            // 
            // MarkPatterncBox
            // 
            this.MarkPatterncBox.FormattingEnabled = true;
            this.MarkPatterncBox.Location = new System.Drawing.Point(599, 413);
            this.MarkPatterncBox.Name = "MarkPatterncBox";
            this.MarkPatterncBox.Size = new System.Drawing.Size(159, 23);
            this.MarkPatterncBox.TabIndex = 182;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(529, 417);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(67, 15);
            this.label31.TabIndex = 181;
            this.label31.Text = "喷码模式";
            // 
            // lefttBox
            // 
            this.lefttBox.Location = new System.Drawing.Point(771, 440);
            this.lefttBox.Name = "lefttBox";
            this.lefttBox.Size = new System.Drawing.Size(127, 25);
            this.lefttBox.TabIndex = 180;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(681, 443);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(67, 15);
            this.label32.TabIndex = 179;
            this.label32.Text = "距离左边";
            // 
            // toptBox
            // 
            this.toptBox.Location = new System.Drawing.Point(101, 468);
            this.toptBox.Name = "toptBox";
            this.toptBox.Size = new System.Drawing.Size(159, 25);
            this.toptBox.TabIndex = 178;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(8, 468);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(67, 15);
            this.label33.TabIndex = 177;
            this.label33.Text = "距离底部";
            // 
            // heighttBox
            // 
            this.heighttBox.Location = new System.Drawing.Point(699, 284);
            this.heighttBox.Name = "heighttBox";
            this.heighttBox.Size = new System.Drawing.Size(127, 25);
            this.heighttBox.TabIndex = 176;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(633, 287);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(60, 15);
            this.label34.TabIndex = 175;
            this.label34.Text = "箱子高:";
            // 
            // widthtBox
            // 
            this.widthtBox.Location = new System.Drawing.Point(422, 284);
            this.widthtBox.Name = "widthtBox";
            this.widthtBox.Size = new System.Drawing.Size(127, 25);
            this.widthtBox.TabIndex = 174;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(352, 287);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(60, 15);
            this.label35.TabIndex = 173;
            this.label35.Text = "箱子宽:";
            // 
            // longtBox
            // 
            this.longtBox.Location = new System.Drawing.Point(146, 284);
            this.longtBox.Name = "longtBox";
            this.longtBox.Size = new System.Drawing.Size(127, 25);
            this.longtBox.TabIndex = 172;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(75, 287);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(60, 15);
            this.label36.TabIndex = 171;
            this.label36.Text = "箱子长:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(713, 510);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(23, 15);
            this.label37.TabIndex = 189;
            this.label37.Text = "ID";
            this.label37.Visible = false;
            // 
            // idtBox
            // 
            this.idtBox.Location = new System.Drawing.Point(771, 507);
            this.idtBox.Name = "idtBox";
            this.idtBox.Size = new System.Drawing.Size(148, 25);
            this.idtBox.TabIndex = 188;
            this.idtBox.Visible = false;
            // 
            // HookDirectioncBox
            // 
            this.HookDirectioncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.HookDirectioncBox.FormattingEnabled = true;
            this.HookDirectioncBox.Location = new System.Drawing.Point(146, 149);
            this.HookDirectioncBox.Name = "HookDirectioncBox";
            this.HookDirectioncBox.Size = new System.Drawing.Size(127, 23);
            this.HookDirectioncBox.TabIndex = 191;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(27, 152);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(112, 15);
            this.label38.TabIndex = 190;
            this.label38.Text = "勾子是否掉头：";
            // 
            // CatornSizebtn
            // 
            this.CatornSizebtn.Location = new System.Drawing.Point(832, 275);
            this.CatornSizebtn.Name = "CatornSizebtn";
            this.CatornSizebtn.Size = new System.Drawing.Size(115, 38);
            this.CatornSizebtn.TabIndex = 192;
            this.CatornSizebtn.Text = "选择尺寸";
            this.CatornSizebtn.UseVisualStyleBackColor = true;
            this.CatornSizebtn.Click += new System.EventHandler(this.CatornbtnSize_Click);
            // 
            // IsCheckcbBox
            // 
            this.IsCheckcbBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsCheckcbBox.FormattingEnabled = true;
            this.IsCheckcbBox.Location = new System.Drawing.Point(422, 149);
            this.IsCheckcbBox.Name = "IsCheckcbBox";
            this.IsCheckcbBox.Size = new System.Drawing.Size(127, 23);
            this.IsCheckcbBox.TabIndex = 194;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(304, 152);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(112, 15);
            this.label39.TabIndex = 193;
            this.label39.Text = "是否验针扫描：";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(773, 614);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(39, 15);
            this.label40.TabIndex = 196;
            this.label40.Text = "guid";
            this.label40.Visible = false;
            // 
            // guidtBox
            // 
            this.guidtBox.Location = new System.Drawing.Point(873, 608);
            this.guidtBox.Name = "guidtBox";
            this.guidtBox.Size = new System.Drawing.Size(148, 25);
            this.guidtBox.TabIndex = 195;
            this.guidtBox.Visible = false;
            // 
            // IsPressDowncBox
            // 
            this.IsPressDowncBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsPressDowncBox.FormattingEnabled = true;
            this.IsPressDowncBox.Location = new System.Drawing.Point(146, 105);
            this.IsPressDowncBox.Name = "IsPressDowncBox";
            this.IsPressDowncBox.Size = new System.Drawing.Size(127, 23);
            this.IsPressDowncBox.TabIndex = 198;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(53, 108);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(82, 15);
            this.label41.TabIndex = 197;
            this.label41.Text = "是否压箱：";
            // 
            // GAPSidecBox
            // 
            this.GAPSidecBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GAPSidecBox.FormattingEnabled = true;
            this.GAPSidecBox.Location = new System.Drawing.Point(697, 152);
            this.GAPSidecBox.Name = "GAPSidecBox";
            this.GAPSidecBox.Size = new System.Drawing.Size(127, 23);
            this.GAPSidecBox.TabIndex = 200;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(555, 155);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(142, 15);
            this.label42.TabIndex = 199;
            this.label42.Text = "是否运行网单贴标：";
            // 
            // GAPSideStartNumtBox
            // 
            this.GAPSideStartNumtBox.Location = new System.Drawing.Point(698, 198);
            this.GAPSideStartNumtBox.Name = "GAPSideStartNumtBox";
            this.GAPSideStartNumtBox.Size = new System.Drawing.Size(127, 25);
            this.GAPSideStartNumtBox.TabIndex = 202;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(579, 202);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(105, 15);
            this.label43.TabIndex = 201;
            this.label43.Text = "网单起始序号:";
            // 
            // SetQtyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 407);
            this.Controls.Add(this.GAPSideStartNumtBox);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.GAPSidecBox);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.IsPressDowncBox);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.guidtBox);
            this.Controls.Add(this.IsCheckcbBox);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.CatornSizebtn);
            this.Controls.Add(this.HookDirectioncBox);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.idtBox);
            this.Controls.Add(this.ColumntBox);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.choosebtn);
            this.Controls.Add(this.IsRotatetBox);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.MarkPatterncBox);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.lefttBox);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.toptBox);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.heighttBox);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.widthtBox);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.longtBox);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.OrientationcBox);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.LinetBox);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.CatornStyle_guidtBox);
            this.Controls.Add(this.GoodWeighttBox);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.qtytBox);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.PoguidtBox);
            this.Controls.Add(this.JianspeedtBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.GrabspeedtBox);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.ThicktBox);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.LayernumtBox);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.LabelingTypecbox);
            this.Controls.Add(this.CatornTypecBox);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.weighttBox);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.GAPPrintcBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.IsScanRuncBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Palletizing_UnittBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TiaomatBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.IsYaMaHaRuncBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.IsPackingRuncBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.IsPalletizingRuncBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.IsSealingRuncBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.PartitionTypecBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.IsPalletizingcBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.PrintIsshieidcBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.MarkingIsshieidcBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.IsUnpackingcBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.okbtn);
            this.Controls.Add(this.qtytBx);
            this.Controls.Add(this.label1);
            this.Name = "SetQtyForm";
            this.Text = "设置装箱参数";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SetQtyForm_FormClosed);
            this.Load += new System.EventHandler(this.SetQtyForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox qtytBx;
        private System.Windows.Forms.Button okbtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox IsUnpackingcBox;
        private System.Windows.Forms.ComboBox MarkingIsshieidcBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox PrintIsshieidcBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox IsPalletizingcBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox PartitionTypecBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox IsYaMaHaRuncBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox IsPackingRuncBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox IsPalletizingRuncBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox IsSealingRuncBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TiaomatBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Palletizing_UnittBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox IsScanRuncBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox GAPPrintcBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox OrientationcBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox LinetBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox CatornStyle_guidtBox;
        private System.Windows.Forms.TextBox GoodWeighttBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox qtytBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox PoguidtBox;
        private System.Windows.Forms.TextBox JianspeedtBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox GrabspeedtBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox ThicktBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox LayernumtBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox LabelingTypecbox;
        private System.Windows.Forms.ComboBox CatornTypecBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox weighttBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox ColumntBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button choosebtn;
        private System.Windows.Forms.TextBox IsRotatetBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox MarkPatterncBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox lefttBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox toptBox;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox heighttBox;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox widthtBox;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox longtBox;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox idtBox;
        private System.Windows.Forms.ComboBox HookDirectioncBox;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button CatornSizebtn;
        private System.Windows.Forms.ComboBox IsCheckcbBox;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox guidtBox;
        private System.Windows.Forms.ComboBox IsPressDowncBox;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox GAPSidecBox;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox GAPSideStartNumtBox;
        private System.Windows.Forms.Label label43;
    }
}