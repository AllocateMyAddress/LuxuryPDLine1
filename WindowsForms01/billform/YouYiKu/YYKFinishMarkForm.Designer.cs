﻿namespace WindowsForms01
{
    partial class YYKFinishMarkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(YYKFinishMarkForm));
            this.YYKMarkFinishdgv = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.取消结束订单ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SelecttSpBtn = new System.Windows.Forms.ToolStripButton();
            this.PotSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.SetCodetSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.CktSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.删除数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.YYKMarkFinishdgv)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // YYKMarkFinishdgv
            // 
            this.YYKMarkFinishdgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.YYKMarkFinishdgv.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.YYKMarkFinishdgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.YYKMarkFinishdgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.YYKMarkFinishdgv.ContextMenuStrip = this.contextMenuStrip1;
            this.YYKMarkFinishdgv.Location = new System.Drawing.Point(0, 53);
            this.YYKMarkFinishdgv.Name = "YYKMarkFinishdgv";
            this.YYKMarkFinishdgv.RowTemplate.Height = 27;
            this.YYKMarkFinishdgv.Size = new System.Drawing.Size(1126, 445);
            this.YYKMarkFinishdgv.TabIndex = 1;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.取消结束订单ToolStripMenuItem,
            this.删除数据ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(211, 80);
            // 
            // 取消结束订单ToolStripMenuItem
            // 
            this.取消结束订单ToolStripMenuItem.Name = "取消结束订单ToolStripMenuItem";
            this.取消结束订单ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.取消结束订单ToolStripMenuItem.Text = "取消结束订单";
            this.取消结束订单ToolStripMenuItem.Click += new System.EventHandler(this.取消结束订单ToolStripMenuItem_Click);
            // 
            // SelecttSpBtn
            // 
            this.SelecttSpBtn.AutoSize = false;
            this.SelecttSpBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.SelecttSpBtn.Image = ((System.Drawing.Image)(resources.GetObject("SelecttSpBtn.Image")));
            this.SelecttSpBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SelecttSpBtn.Name = "SelecttSpBtn";
            this.SelecttSpBtn.Size = new System.Drawing.Size(100, 47);
            this.SelecttSpBtn.Text = "查询";
            this.SelecttSpBtn.Click += new System.EventHandler(this.SelecttSpBtn_Click);
            // 
            // PotSpTBox
            // 
            this.PotSpTBox.AutoSize = false;
            this.PotSpTBox.Name = "PotSpTBox";
            this.PotSpTBox.Size = new System.Drawing.Size(150, 50);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(121, 47);
            this.toolStripLabel1.Text = "单号(Order_No)";
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Silver;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.PotSpTBox,
            this.toolStripLabel2,
            this.SetCodetSpTBox,
            this.toolStripLabel3,
            this.CktSpTBox,
            this.SelecttSpBtn});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1126, 50);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(149, 47);
            this.toolStripLabel2.Text = "混箱类型(Set_Code)";
            // 
            // SetCodetSpTBox
            // 
            this.SetCodetSpTBox.AutoSize = false;
            this.SetCodetSpTBox.Name = "SetCodetSpTBox";
            this.SetCodetSpTBox.Size = new System.Drawing.Size(100, 50);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(131, 47);
            this.toolStripLabel3.Text = "仓库(Warehouse)";
            // 
            // CktSpTBox
            // 
            this.CktSpTBox.AutoSize = false;
            this.CktSpTBox.Name = "CktSpTBox";
            this.CktSpTBox.Size = new System.Drawing.Size(150, 50);
            // 
            // 删除数据ToolStripMenuItem
            // 
            this.删除数据ToolStripMenuItem.Name = "删除数据ToolStripMenuItem";
            this.删除数据ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.删除数据ToolStripMenuItem.Text = "删除数据";
            this.删除数据ToolStripMenuItem.Click += new System.EventHandler(this.删除数据ToolStripMenuItem_Click);
            // 
            // YYKFinishMarkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1126, 498);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.YYKMarkFinishdgv);
            this.Name = "YYKFinishMarkForm";
            this.Text = "优衣库/GU完成订单列表";
            this.Load += new System.EventHandler(this.YYKFinishMarkForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.YYKMarkFinishdgv)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView YYKMarkFinishdgv;
        private System.Windows.Forms.ToolStripButton SelecttSpBtn;
        private System.Windows.Forms.ToolStripTextBox PotSpTBox;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox SetCodetSpTBox;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripTextBox CktSpTBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 取消结束订单ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除数据ToolStripMenuItem;
    }
}