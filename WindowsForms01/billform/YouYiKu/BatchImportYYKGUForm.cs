﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using GetData;

namespace WindowsForms01
{
    public partial class BatchImportYYKGUForm : Form
    {
        List<string> list_route = new List<string>();
        DataTable dtexist = new DataTable();
        inexcel ss = new inexcel();
        YouyikuForm Youyikuform = null;
        public BatchImportYYKGUForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            routetBox.ReadOnly = true;
        }

        private void BatchImportYYKGUForm_Load(object sender, EventArgs e)
        {

        }

        private void LoadRoutebtn_Click(object sender, EventArgs e)
        {
            try
            {
                string defaultfilePath = "";
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                dialog.ShowNewFolderButton = false;
                dialog.Description = "请选择文件路径";
                //if (defaultfilePath != "")
                //{
                //    //设置此次默认目录为上一次选中目录  
                //    dialog.SelectedPath = defaultfilePath;
                //}
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK || result == DialogResult.Yes)
                {
                    string foldPath = dialog.SelectedPath;
                    routetBox.Text = defaultfilePath = foldPath;
                    string[] files = Directory.GetFiles(routetBox.Text + @"\", "*.xls");
                    foreach (string file in files)
                    {
                        list_route.Add(file);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void okbtn_Click(object sender, EventArgs e)
        {
            string FileName_ = "";
            try
            {

                foreach (string FileName_v in list_route)
                {
                    FileName_ = FileName_v;
                    CsGetData.Batch_import_YYKGU(FileName_v);
                }
                MessageBox.Show("导入成功");
                Youyikuform = (YouyikuForm)this.Owner;
                Youyikuform.NewShow();
                this.Dispose();
                this.Close();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("路径:" + FileName_ + Environment.NewLine + "文件格式或着内容不正确");
                throw new Exception("提示：" + err.Message);
            }
        }
    }
}
