﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class PositionForm : Form
    {
        int MarkPattern_v = 0;
        int CatornType_v = 0;
        int IsRotate_v = 0;
        int LabelingType_v = 0;
        int PartitionType_v = 0;
        string id_v = "";
        string PoType_v = "";
        int long_v = 0;
        int width_v = 0;
        int height_v = 0;
        int top_v = 0;
        int left_v = 0;
        int weight_v = 0;
        int GoodWeight_v = 0;
        int ColumnNunber_v = 0;
        int M_strline_v = 0;
        int M_strOrientation_v = 0;
        int LayernumtBox_v = 0;
        int ThicktBox_v = 0;
        int JianspeedtBox_v = 0;
        int GrabspeedtBox_v = 0;
        string Doguid_v = "";
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        public MarkPositionForm MarkPositionform = null;
        ChoiceRotateForm Choicerotateform = null;
        PackingForm Packingform = null;
        public PositionForm()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            NewShow();
        }
        public PositionForm(string Doguid_)
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            Doguid_v = Doguid_;
            NewShow(Doguid_v);
        }
        /// <summary>
        /// 修改纸箱参数信息
        /// </summary>
        /// <param name="id_"></param>
        /// <param name="long_"></param>
        /// <param name="width_"></param>
        /// <param name="Heghit_"></param>
        /// <param name="TntervalTop_"></param>
        /// <param name="TntervalLeft_"></param>
        /// <param name="CartonWeight_"></param>
        /// <param name="MarkPattern_"></param>
        /// <param name="CatornType_"></param>
        /// <param name="IsRotate_"></param>
        /// <param name="LabelingType_"></param>
        /// <param name="PartitionType_"></param>
        public PositionForm(string id_, string long_, string width_, string Heghit_, string TntervalTop_, string TntervalLeft_, string CartonWeight_,string GoodWeight, string ColumnNunber_, string Layernum_, string Thick_, string Jianspeed_, string Grabspeed_, string MarkPattern_, string CatornType_, string IsRotate_, string LabelingType_, string PartitionType_,string OrderNoQty_, string PoType_, string Orientation_, string LineNum_,string Doguid_)
        {
            InitializeComponent();
            idtBox.Text = id_;
            longtBox.Text = long_;
            widthtBox.Text = width_;
            heighttBox.Text = Heghit_;
            toptBox.Text = TntervalTop_;
            lefttBox.Text = TntervalLeft_;
            weighttBox.Text = CartonWeight_;
            GoodWeighttBox.Text = GoodWeight;
            ColumntBox.Text = ColumnNunber_;
            LayernumtBox.Text = Layernum_;
            ThicktBox.Text = Thick_;
            JianspeedtBox.Text = Jianspeed_;
            GrabspeedtBox.Text = Grabspeed_;
            MarkPatterncBox.Text = MarkPattern_;
            CatornTypecBox.Text = CatornType_;
            IsRotatetBox.Text = IsRotate_;
            LabelingTypecbox.Text = LabelingType_;
            PartitionTypecBox.Text = PartitionType_;
            qtytBox.Text = OrderNoQty_;
            PoType_v = PoType_;
            OrientationcBox.Text = Orientation_;
            LinetBox.Text = LineNum_;
            NewShow();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }
        /// <summary>
        /// 查询纸箱参数显示
        /// </summary>
        public PositionForm(string long_, string width_, string Heghit_, string TntervalTop_, string TntervalLeft_, string CartonWeight_, string GoodWeight_, string ColumnNunber_, string LineNum_, string Layernum_, string Thick_, string Jianspeed_, string Grabspeed_, string MarkPattern_, string CatornType_, string IsRotate_, string LabelingType_, string Quantity_, string PartitionType_, string Orientation_)
        {
            InitializeComponent();
            //idtBox.Text = id;
            longtBox.Text = long_;
            widthtBox.Text = width_;
            heighttBox.Text = Heghit_;
            toptBox.Text = TntervalTop_;
            lefttBox.Text = TntervalLeft_;
            weighttBox.Text = CartonWeight_;
            GoodWeighttBox.Text = GoodWeight_;
            ColumntBox.Text = ColumnNunber_;
            LayernumtBox.Text = Layernum_;
            ThicktBox.Text = Thick_;
            JianspeedtBox.Text = Jianspeed_;
            GrabspeedtBox.Text = Grabspeed_;
            MarkPatterncBox.Text = MarkPattern_;
            CatornTypecBox.Text = CatornType_;
            IsRotatetBox.Text = IsRotate_;
            LabelingTypecbox.Text = LabelingType_;
            PartitionTypecBox.Text = PartitionType_;
            qtytBox.Text = Quantity_;
            OrientationcBox.Text = Orientation_;
            LinetBox.Text = LineNum_;
            foreach (Control contrl in this.Controls)
            {
                contrl.Enabled = false;
            }
            Savebtn.Visible = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }
        private void Savebtn_Click(object sender, EventArgs e)
        {
            try
            {
                List<Control> controls_list1 = new List<Control>();
                List<Control> controls_list2 = new List<Control>();
                Control[] controls_shuzu1 = { longtBox, widthtBox, heighttBox, toptBox, lefttBox, weighttBox, LayernumtBox, GoodWeighttBox, ThicktBox,LinetBox };
                Control[] controls_shuzu2 = { MarkPatterncBox, CatornTypecBox, LabelingTypecbox, PartitionTypecBox };
                foreach (Control ctrl in controls_shuzu1)
                {
                    controls_list1.Add(ctrl);
                }
                foreach (Control ctrl in controls_shuzu2)
                {
                    controls_list2.Add(ctrl);
                }
                if (Chekin.Chekedin(this, controls_list1,"")&& Chekin.Chekedin_notnull(this, controls_list2,""))
                {
                    GetContrlVal();
                    if (string.IsNullOrEmpty( id_v))
                    {
                        string selectsql = string.Format("SELECT*FROM dbo.MarkingPosition WHERE Long={0} AND Width={1} AND Heghit={2} AND TntervalTop={3} AND TntervalLeft={4}", long_v, width_v, height_v, top_v, left_v);
                        DataSet ds = content1.Select_nothing(selectsql);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            MessageBox.Show("提示：数据已存在");
                            return;
                        }
                        //判断是否纸箱超尺寸
                       string IsOverrrange =   Chekin.IsOverrrange(int.Parse(longtBox.Text.ToString().Trim()), int.Parse(widthtBox.Text.ToString().Trim()), int.Parse(heighttBox.Text.ToString().Trim()));
                        if (!string.IsNullOrEmpty(IsOverrrange))
                        {
                            CsFormShow.MessageBoxFormShow(IsOverrrange);
                            return;
                        }
                        //录入数据库
                        string sqlstr = string.Format("INSERT INTO MarkingPosition (Long,Width,Heghit,TntervalTop,TntervalLeft,CartonWeight,MarkPattern,CatornType,IsRotate,LabelingType,PartitionType,ColumnNunber,Layernum,Thick,Jianspeed,Grabspeed,Entrytime,GoodWeight,PoType,Orientation,LineNum) VALUES ({0},{1},{2},{3},{4},{5},'{6}','{7}','{8}','{9}','{10}',{11},{12},{13},{14},{15},'{16}','{17}','{18}','{19}','{20}')", long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, PartitionType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, JianspeedtBox_v, GrabspeedtBox_v, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(),GoodWeight_v,PoType_v, M_strOrientation_v,M_strline_v);
                        content1.Select_nothing(sqlstr);
                        MessageBox.Show("保存成功");
                    }
                    else
                    {
                        string sqlstr = string.Format("UPDATE dbo.MarkingPosition SET Long='{0}',Width='{1}',Heghit='{2}',TntervalTop='{3}',TntervalLeft='{4}',CartonWeight='{5}',MarkPattern='{6}' ,CatornType='{7}',IsRotate='{8}',LabelingType='{9}',PartitionType='{10}',ColumnNunber='{11}' ,Layernum='{12}' ,Thick='{13}' ,Jianspeed='{14}' ,Grabspeed='{15}',GoodWeight='{16}',Orientation='{18}',LineNum='{19}' WHERE guid='{17}'", long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, PartitionType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, JianspeedtBox_v, GrabspeedtBox_v,GoodWeight_v, id_v, M_strOrientation_v,M_strline_v);
                        content1.Select_nothing(sqlstr);
                        MessageBox.Show("修改成功");
                    }
                    idtBox.Text = "";
                    longtBox.Text = "";
                    widthtBox.Text = "";
                    heighttBox.Text = "";
                    toptBox.Text = "";
                    lefttBox.Text = "";
                    weighttBox.Text = "";
                    MarkPatterncBox.Text = "";
                    CatornTypecBox.Text = "";
                    GoodWeighttBox.Text = "";
                    IsRotatetBox.Text = "";
                    LabelingTypecbox.Text = "";
                    PartitionTypecBox.Text = "";
                    ColumntBox.Text = "";
                    LayernumtBox.Text = "";
                    ThicktBox.Text = "";
                    JianspeedtBox.Text = "";
                    GrabspeedtBox.Text = "";
                    OrientationcBox.Text = "";
                    LinetBox.Text = "";
                    MarkPositionform = (MarkPositionForm)this.Owner;
                    MarkPositionform.NewShow();
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    return;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void PositionForm_Load(object sender, EventArgs e)
        {
        }

        private void PositionForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
            this.Close();
        }
        public void NewShow()
        {
            MarkPatterncBox.Items.Add("不喷色号");
            MarkPatterncBox.Items.Add("喷色号");
            MarkPatterncBox.SelectedIndex = 0;

            CatornTypecBox.Items.Add("大");
            CatornTypecBox.Items.Add("小");
            CatornTypecBox.Items.Add("");
            CatornTypecBox.SelectedIndex = 0;

            LabelingTypecbox.Items.Add("小标签");
            LabelingTypecbox.Items.Add("大标签");
            //LabelingTypecbox.Items.Add("");
            LabelingTypecbox.SelectedIndex = 0;

            PartitionTypecBox.Items.Add("A隔板");
            PartitionTypecBox.Items.Add("B隔板");
            PartitionTypecBox.Items.Add("");
            PartitionTypecBox.SelectedIndex = 0;

            OrientationcBox.Items.Add("正");
            OrientationcBox.Items.Add("反");
            OrientationcBox.Items.Add("");
            //OrientationcBox.SelectedIndex = 0;
            IsRotatetBox.ReadOnly = true;
            ColumntBox.ReadOnly = true;
            qtytBox.ReadOnly = true;
            OrientationcBox.Enabled = false;
            LinetBox.ReadOnly = true;
            LayernumtBox.ReadOnly = true;
            MarkPatterncBox.Enabled = false;
            LinetBox.Text = "1";
            GoodWeighttBox.Text = "1";
            ThicktBox.Text = "50";
            LayernumtBox.Text = "10";
            weighttBox.Text = "1";
            toptBox.Text = "0";
            lefttBox.Text = "0";
            //JianspeedtBox.Text = "90";
            //GrabspeedtBox.Text = "90";
            LabelingTypecbox.Text = "小标签";
        }
        public void NewShow(string Doguid_)
        {
            try
            {
                DataTable dt=  CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YOUYIKUDO WHERE guid='{0}'",Doguid_));
                if (dt.Rows.Count>0)
                {
                    string setcode = dt.Rows[0]["Set_Code"].ToString();
                }
                MarkPatterncBox.Items.Add("不喷色号");
                MarkPatterncBox.Items.Add("喷色号");
                MarkPatterncBox.SelectedIndex = 0;
                //if (setcode.Contains("-999"))
                //{
                //    MarkPatterncBox.SelectedIndex = 0;
                //}
                //else
                //{
                //    MarkPatterncBox.SelectedIndex = 1;
                //}
                CatornTypecBox.Items.Add("大");
                CatornTypecBox.Items.Add("小");
                CatornTypecBox.Items.Add("");
                CatornTypecBox.SelectedIndex = 0;

                LabelingTypecbox.Items.Add("小标签");
                LabelingTypecbox.Items.Add("大标签");
               // LabelingTypecbox.Items.Add("");
                LabelingTypecbox.SelectedIndex = 0;
                PartitionTypecBox.Items.Add("A隔板");
                PartitionTypecBox.Items.Add("B隔板");
                PartitionTypecBox.SelectedIndex = 0;
                PartitionTypecBox.Items.Add("");
                OrientationcBox.Items.Add("正");
                OrientationcBox.Items.Add("反");
                OrientationcBox.Items.Add("");
                // PartitionTypecBox.SelectedIndex = 0;
                IsRotatetBox.ReadOnly = true;
                MarkPatterncBox.Enabled = false;
                ColumntBox.ReadOnly = true;
                qtytBox.ReadOnly = true;
                OrientationcBox.Enabled = false;
                LinetBox.ReadOnly = true;
                LayernumtBox.ReadOnly = true;
                LinetBox.Text = "1";
                GoodWeighttBox.Text = "1";
                ThicktBox.Text = "50";
                LayernumtBox.Text = "10";
                weighttBox.Text = "1";
                //JianspeedtBox.Text = "90";
               // GrabspeedtBox.Text = "90";
                LabelingTypecbox.Text = "小标签";
                toptBox.Text = "0";
                lefttBox.Text = "0";
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        public void GetContrlVal()
        {
             id_v = idtBox.Text.ToString().Trim();
             long_v =(int)double.Parse(longtBox.Text.ToString().Trim());
             width_v = (int)double.Parse(widthtBox.Text.ToString().Trim());
             height_v = (int)double.Parse(heighttBox.Text.ToString().Trim());
             top_v = (int)double.Parse(toptBox.Text.ToString().Trim());
             left_v = (int)double.Parse(lefttBox.Text.ToString().Trim());
             weight_v = (int)double.Parse(weighttBox.Text.ToString().Trim());
            GoodWeight_v= (int)double.Parse(GoodWeighttBox.Text.ToString().Trim());
           // ColumnNunber_v = (int)double.Parse(ColumntBox.Text.ToString().Trim());
            M_strline_v = (int)double.Parse(LinetBox.Text.ToString().Trim());
            LayernumtBox_v = (int)double.Parse(LayernumtBox.Text.ToString().Trim());
            ThicktBox_v= (int)double.Parse(ThicktBox.Text.ToString().Trim());
            //JianspeedtBox_v= (int)double.Parse(JianspeedtBox.Text.ToString().Trim());
            //GrabspeedtBox_v= (int)double.Parse(GrabspeedtBox.Text.ToString().Trim());
            MarkPattern_v = CsMarking.GetCatornParameter(MarkPatterncBox.Text.ToString().Trim());
            CatornType_v = CsMarking.GetCatornParameter(CatornTypecBox.Text.ToString().Trim());
            //IsRotate_v = CsMarking.GetCatornParameter(IsRotatetBox.Text.ToString().Trim());
            LabelingType_v= CsMarking.GetCatornParameter(LabelingTypecbox.Text.ToString().Trim());
            PartitionType_v = CsMarking.GetCatornParameter(PartitionTypecBox.Text.ToString().Trim());
           // M_strOrientation_v = CsMarking.GetCatornParameter(OrientationcBox.Text.ToString().Trim());
        }
        private void choosebtn_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(Choicerotateform, this, "WindowsForms01.ChoiceRotateForm");
        }

        private void choosebtn_Click_1(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    if (Choicerotateform == null || Choicerotateform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                    {
                        Choicerotateform = new ChoiceRotateForm(this);
                        Choicerotateform.Owner = this;
                        Choicerotateform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        Choicerotateform.ShowDialog();
                    }
                    else
                    {
                        Choicerotateform.Show();
                        Choicerotateform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        Choicerotateform.BringToFront();
                    }
                }
                catch (Exception ex)
                {
                    Cslogfun.WriteToLog(ex);
                    throw new Exception("提示：" + ex);
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void PartitionTypecBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void OrientationcBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void JianspeedtBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
