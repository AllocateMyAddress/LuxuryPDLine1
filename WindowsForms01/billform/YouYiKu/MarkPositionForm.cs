﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using A.BLL;

namespace WindowsForms01
{
    public partial class MarkPositionForm : Form
    {
       public string Long = "";
        public string Width = "";
        public string Heghit = "";
        public string TntervalTop = "";
        public string TntervalLeft = "";
        public string CartonWeight = "";
        public string MarkPattern = "";
        public string CatornType = "";
        public string IsRotate = "";
        public string LabelingType = "";
      //  public string PartitionType_v = "";
        public string Po_guid = "";
        public string guid_v = "";
        public string packingqty = "0";
        public string IsUnpacking = "";
        public string Partitiontype = "";
        public string MarkingIsshieid = "";
        public string PrintIsshieid = "";
        public string IsPalletizing = "";
        public string IsPackingRun = "";
        public string IsYaMaHaRun = "";
        public string IsSealingRun = "";
        public string IsPalletizingRun = "";
        public string GAPIsPrintRun = "";
        public string IsScanRun = "";
        public string TiaoMaVal = "";
        public string HookDirection = "";
        public string Orientation = "";
        public string ColumnNunber = "";
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        List<Control> controls_list = new List<Control>();
        PositionForm positionform = null;
        DataGridView YykDvg = null;
        YouyikuForm Youyikuform = null;
        SetQtyForm SetQtyform = null;
        public MarkPositionForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.MarkPositiondGV, 40, 40, true, Color.LightSteelBlue,this);
            NewShow();
        }
        public MarkPositionForm(string Po_guid_, DataGridView yykdvg_, string CatornStyle_guid_)
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.MarkPositiondGV, 40, 40, true, Color.LightSteelBlue,this);
            Po_guid = Po_guid_;
            YykDvg = yykdvg_;
            NewShow();
            if (!string.IsNullOrEmpty(CatornStyle_guid_))
            {
                CsFormShow.DvgSetcurrentRow(this.MarkPositiondGV, CatornStyle_guid_);
            }
        }
        public MarkPositionForm(string Po_guid_,DataGridView yykdvg_,string CatornStyle_guid_, List<Control> controls_)
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.MarkPositiondGV, 40, 40, true, Color.LightSteelBlue,this);
            controls_list = controls_;
            Po_guid = Po_guid_;
            YykDvg = yykdvg_;
            NewShow();
            if (!string.IsNullOrEmpty(CatornStyle_guid_))
            {
                CsFormShow.DvgSetcurrentRow(this.MarkPositiondGV, CatornStyle_guid_);
            }
        }

        private void MarkPositionForm_Load(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void okbtn_Click(object sender, EventArgs e)
        {

        }
        public void NewShow()
        {
            try
            {
                MarkPositiondGV.DataSource = null;
                string sqlstr = "SELECT ROW_NUMBER() OVER(ORDER BY Entrytime) AS 序号, guid, Long AS '箱子长',Width AS '箱子宽',Heghit AS '箱子高',TntervalTop AS '距离底部',TntervalLeft AS '距离左边',CartonWeight AS '重量',GoodWeight AS '产品重量',ColumnNunber AS '列数',LineNum AS '行数',Layernum AS '箱内件数',Thick AS '产品厚度',CASE WHEN  MarkPattern='1' THEN '不喷色号' ELSE '喷色号' END  AS '喷码模式',  CASE WHEN  CatornType='1' THEN '大' ELSE '小' END  AS '纸箱大小',CASE WHEN  IsRotate='1' THEN '旋转' ELSE '不旋转' END AS '是否旋转',CASE WHEN  LabelingType='1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType='1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型' , CASE WHEN  Orientation='1' THEN '反' ELSE '正' END AS '正反方向' FROM dbo.MarkingPosition ";
                DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                if (dt.Rows.Count>0)
                {
                    dt.Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                    MarkPositiondGV.DataSource = dt;
                    MarkPositiondGV.Columns["当前选择"].ReadOnly = true;
                    MarkPositiondGV.Columns["guid"].Visible = false;
                    MarkPositiondGV.Columns["重量"].Visible = false;
                    MarkPositiondGV.Columns["列数"].Visible = false;
                    MarkPositiondGV.Columns["行数"].Visible = false;
                    MarkPositiondGV.Columns["箱内件数"].Visible = false;
                    MarkPositiondGV.Columns["纸箱大小"].Visible = false;
                    MarkPositiondGV.Columns["产品厚度"].Visible = false;
                    MarkPositiondGV.Columns["喷码模式"].Visible = false;
                    MarkPositiondGV.Columns["是否旋转"].Visible = false;
                    MarkPositiondGV.Columns["正反方向"].Visible = false;
                    MarkPositiondGV.Columns["贴标类型"].Visible = false;
                    MarkPositiondGV.Columns["产品重量"].Visible = false;
                    MarkPositiondGV.Columns["隔板类型"].Visible = false;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        public void NewShow(string long_,string width_,string height_)
        {
            try
            {
                MarkPositiondGV.DataSource = null;
                string sqlstr =string.Format("SELECT ROW_NUMBER() OVER(ORDER BY Entrytime) AS 序号, guid, Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量',GoodWeight AS '产品重量', ColumnNunber AS '列数',LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '1' THEN '不喷色号' ELSE '喷色号' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation='1' THEN '反' ELSE '正' END AS '正反方向' FROM dbo.MarkingPosition WHERE Long LIKE '%'+'{0}'+'' AND Width LIKE '%'+'{1}'+'%' AND Heghit LIKE '%'+'{2}'+'%'  order by Entrytime", long_, width_, height_);
                DataTable dt =CsFormShow.GoSqlSelect(sqlstr);
                if (dt.Rows.Count>0)
                {
                    dt.Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                    MarkPositiondGV.DataSource = dt;
                    MarkPositiondGV.Columns["当前选择"].ReadOnly = true;
                    MarkPositiondGV.Columns["guid"].Visible = false;
                    MarkPositiondGV.Columns["重量"].Visible = false;
                    MarkPositiondGV.Columns["列数"].Visible = false;
                    MarkPositiondGV.Columns["行数"].Visible = false;
                    MarkPositiondGV.Columns["箱内件数"].Visible = false;
                    MarkPositiondGV.Columns["纸箱大小"].Visible = false;
                    MarkPositiondGV.Columns["产品厚度"].Visible = false;
                    MarkPositiondGV.Columns["喷码模式"].Visible = false;
                    MarkPositiondGV.Columns["是否旋转"].Visible = false;
                    MarkPositiondGV.Columns["正反方向"].Visible = false;
                    MarkPositiondGV.Columns["贴标类型"].Visible = false;
                    MarkPositiondGV.Columns["产品重量"].Visible = false;
                    MarkPositiondGV.Columns["隔板类型"].Visible = false;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }

        private void inserttSpBtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool isindex_v = false; int index_v = 0; string Doguid = "";
                CsFormShow.DvgGetcurrentIndex(YykDvg, out isindex_v, out index_v);
                if (isindex_v)
                {
                    Doguid = YykDvg.Rows[index_v].Cells["guid"].Value.ToString();
                }
                if (positionform == null || positionform.IsDisposed)
                {
                    positionform = new PositionForm(Doguid);
                    positionform.Owner = this;
                    positionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    positionform.ShowDialog();
                }
                else
                {
                    positionform.ShowDialog();
                    positionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    positionform.BringToFront();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

           // CsFormShow.FormShowDialog(positionform,this,"WindowsForms01.PositionForm");
        }

        private void SelecttSpBtn_Click(object sender, EventArgs e)
        {
            NewShow(LongtSpTBox.Text.ToString().Trim(),widthtSpTBox.Text.ToString().Trim(),HeighttSpTBox.Text.ToString().Trim());
        }

        private void 修改信息ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MarkPositiondGV.SelectedRows.Count > 1 || String.IsNullOrEmpty(MarkPositiondGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim()))
                {
                    MessageBox.Show("请选择一行有效数据");
                    return;
                }
                string guid_v = MarkPositiondGV.SelectedRows[0].Cells["guid"].Value.ToString();
                string long_v = MarkPositiondGV.SelectedRows[0].Cells["箱子长"].Value.ToString();
                string width_v = MarkPositiondGV.SelectedRows[0].Cells["箱子宽"].Value.ToString();
                string Heghit_v = MarkPositiondGV.SelectedRows[0].Cells["箱子高"].Value.ToString();
                string TntervalTop_v = MarkPositiondGV.SelectedRows[0].Cells["距离底部"].Value.ToString();
                string TntervalLeft_v = MarkPositiondGV.SelectedRows[0].Cells["距离左边"].Value.ToString();
                string CartonWeight_v = MarkPositiondGV.SelectedRows[0].Cells["重量"].Value.ToString();
                string GoodWeight_v = MarkPositiondGV.SelectedRows[0].Cells["产品重量"].Value.ToString();
                string ColumnNunber_v = MarkPositiondGV.SelectedRows[0].Cells["列数"].Value.ToString();
                string LineNum_v = MarkPositiondGV.SelectedRows[0].Cells["行数"].Value.ToString();
                string Layernum_v = MarkPositiondGV.SelectedRows[0].Cells["箱内件数"].Value.ToString();
                string Thick_v = MarkPositiondGV.SelectedRows[0].Cells["产品厚度"].Value.ToString();
                //string Jianspeed_v = MarkPositiondGV.SelectedRows[0].Cells["加减速度"].Value.ToString();
               // string Grabspeed_v = MarkPositiondGV.SelectedRows[0].Cells["抓取速度"].Value.ToString();
                string MarkPattern_v = MarkPositiondGV.SelectedRows[0].Cells["喷码模式"].Value.ToString();
                string CatornType_v = MarkPositiondGV.SelectedRows[0].Cells["纸箱大小"].Value.ToString();
                string IsRotate_v = MarkPositiondGV.SelectedRows[0].Cells["是否旋转"].Value.ToString();
                string LabelingType_v = MarkPositiondGV.SelectedRows[0].Cells["贴标类型"].Value.ToString();
                string PartitionType_v = MarkPositiondGV.SelectedRows[0].Cells["隔板类型"].Value.ToString();
                string Orientation_v = MarkPositiondGV.SelectedRows[0].Cells["正反方向"].Value.ToString();
                if (positionform == null || positionform.IsDisposed)
                {
                    positionform = new PositionForm(guid_v, long_v, width_v, Heghit_v, TntervalTop_v, TntervalLeft_v, CartonWeight_v, GoodWeight_v, ColumnNunber_v, Layernum_v, Thick_v, "", "", MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, PartitionType_v, "0","YYK", Orientation_v,LineNum_v, Po_guid);
                    positionform.Owner = this;
                    positionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    positionform.ShowDialog();
                }
                else
                {
                    positionform.ShowDialog();
                    positionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    positionform.BringToFront();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 删除信息ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (stcUserData.M_username != "admin")
                {
                    MessageBox.Show("提示：非admin管理员不能进行此操作");
                    return;
                }
                Delet_click.Delete_TSMenuItem_Click(this, MarkPositiondGV, "MarkingPosition");
                //NewdataGridView_show();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }


        /// <summary>
        /// 置为当前时将当前选择的颜色范围的行数据放在当前队列最前面并清空之前的队列
        /// </summary>
        /// <param name="DgvRow"></param>
        public void SetMarkPattern(DataRow dr)
        {
            try
            {
                CsPakingData.M_DicMarking_colorcode.Clear();
                CsPakingData.M_DicMarking_colorcode["Long"] = dr["箱子长"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["Width"] = dr["箱子宽"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["Heghit"] = dr["箱子高"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["Updown_distance"] = dr["上下偏移"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["Leftright_distance"] = dr["左边偏移"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["CartonWeight"] = dr["重量"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["GoodWeight"] = dr["产品重量"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["ColumnNunber"] = dr["列数"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["LineNum"] = dr["行数"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["Layernum"] = dr["箱内件数"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["Thick"] = dr["产品厚度"].ToString().Trim();
                //CsPakingData.M_DicMarking_colorcode["Jianspeed"] = dr["加减速度"].ToString().Trim();
                //CsPakingData.M_DicMarking_colorcode["Grabspeed"] = dr["抓取速度"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["MarkPattern"] = CsMarking.GetCatornParameter(dr["喷码模式"].ToString().Trim()).ToString();
                CsPakingData.M_DicMarking_colorcode["CatornType"] = CsMarking.GetCatornParameter(dr["纸箱大小"].ToString().Trim()).ToString();
                CsPakingData.M_DicMarking_colorcode["IsRotate"] = CsMarking.GetCatornParameter(dr["是否旋转"].ToString().Trim()).ToString();
                CsPakingData.M_DicMarking_colorcode["LabelingType"] = CsMarking.GetCatornParameter(dr["贴标类型"].ToString().Trim()).ToString();
                CsPakingData.M_DicMarking_colorcode["PartitionType"] = CsMarking.GetCatornParameter(dr["隔板类型"].ToString().Trim()).ToString();
                CsPakingData.M_DicMarking_colorcode["Orientation"] = CsMarking.GetCatornParameter(dr["正反方向"].ToString().Trim()).ToString();
                CsPakingData.M_DicMarking_colorcode["IsRepeatCheck"] = CsMarking.GetCatornParameter(dr["是否再检针"].ToString().Trim()).ToString();
                CsPakingData.M_DicMarking_colorcode["Order_Qty"] = dr["Order_Qty"].ToString().Trim();
                CsPakingData.M_DicMarking_colorcode["装箱单guid"]= dr["guid"].ToString().Trim();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }

        private void MarkPositionForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void 设置为当前toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                MouseDouble();
                // YYKGUScheduling();
            }

            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 判断是否上一个单扫码完成
        /// </summary>
        /// <returns></returns>
        public string IsScanEnd(string potype_)
        {
            try
            {
                string sqlstr = "";
                if (!string.IsNullOrEmpty(M_stcScanData.strpo) && potype_.Contains("UNIQLO"))
                {
                    sqlstr = string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND SKU_Code='{1}' AND Warehouse='{2}' AND potype='{3}' AND Color_Code='{4}' AND Set_Code='{5}' AND ISNULL(IsEndScan,0)=1", M_stcScanData.strpo, M_stcScanData.strPoTiaoMa, M_stcScanData.stridentification, "UNIQLO", M_stcScanData.Color_Code, M_stcScanData.Set_Code);
                }
                else if (!string.IsNullOrEmpty(M_stcScanData.strpo) && CSReadPlc.DevName.Contains("GAP/GU"))
                {
                    if (M_stcScanData.potype.Contains("GU"))
                    {
                        sqlstr = string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND SKU_Code='{1}' AND Warehouse='{2}' AND potype='{3}' AND Color_Code='{4}' AND Set_Code='{5}' AND ISNULL(IsEndScan,0)=1'", M_stcScanData.strpo, M_stcScanData.strPoTiaoMa, M_stcScanData.stridentification, "GU", M_stcScanData.Color_Code, M_stcScanData.Set_Code);
                    }
                    else
                    {
                        sqlstr = string.Format("SELECT * FROM dbo.MarkingLineUp WHERE  订单号码='{0}' AND 从='{1}' AND ISNULL(IsEndScan,0)=1", M_stcScanData.strpo, M_stcScanData.stridentification);
                    }
                }
                if (!string.IsNullOrEmpty(sqlstr))
                {
                    DataSet ds = content1.Select_nothing(sqlstr);
                    if (ds.Tables[0].Rows.Count ==0)
                    {
                        #region 不允许强制切单的情况
                        //MessageBox.Show("本次装箱数量未放完不能切换单");
                        //return "取消切单";
                        #endregion
                        #region 允许强制切单的情况
                        DialogResult diachose = MessageBox.Show("本次扫码数量未完成是否强制切单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (diachose == DialogResult.OK)
                        {
                            MessageBox.Show("强制切单时，请停止扫码操作并且将设备调成手动状态");
                            return "强制切单";
                        }
                        else if (diachose == DialogResult.Cancel)
                        {
                            return "取消切单";
                        }
                        #endregion
                    }
                }
                return "正常切单";
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        private void 批量设置同单号装箱信息ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //检查设备是否启动
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return;
                }
                if (MarkPositiondGV.Rows.Count > 0 && MarkPositiondGV.SelectedRows.Count > 0)
                {
                    string Position_guid = "";
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(MarkPositiondGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {

                        DataGridViewRow DGVROW = MarkPositiondGV.Rows[index_v];
                        Position_guid = MarkPositiondGV.Rows[index_v].Cells["guid"].Value.ToString();
                        #region  计算垛板单位数量
                        M_stcPalletizingData.strCatornLong = MarkPositiondGV.Rows[index_v].Cells["箱子长"].Value.ToString();
                        M_stcPalletizingData.strCatornWidth = MarkPositiondGV.Rows[index_v].Cells["箱子宽"].Value.ToString();
                        M_stcPalletizingData.strCatornHeight = MarkPositiondGV.Rows[index_v].Cells["箱子高"].Value.ToString();
                        M_stcPalletizingData.strWsizeFlag = "start";
                        TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                        while (M_stcPalletizingData.ReadPalletizingflag != "1")
                        {
                            TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                            if (ts.Seconds > 1)
                            {
                                break;
                            }
                        }
                        M_stcPalletizingData.strResetFlag = "start";
                        #endregion
                        // this.Close();
                        // MessageBox.Show("装箱参数设置完成");
                    }
                    #region 设置当前订单
                    DialogResult btchose = MessageBox.Show("是否将订单设置为当前", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (btchose == DialogResult.OK)
                    {

                        #region  
                        if (YykDvg.Rows.Count > 0 && YykDvg.SelectedRows.Count > 0)
                        {
                            //bool isindex_v; int index_v;
                            CsFormShow.DvgGetcurrentIndex(YykDvg, out isindex_v, out index_v);
                            if (isindex_v)
                            {
                                //批量设置同单号纸箱参数
                                //string strsql = string.Format("  UPDATE dbo.YOUYIKUDO SET CatornStyle_guid = '{0}' WHERE po_guid ='{1}' AND NOT EXISTS(SELECT * FROM dbo.YYKMarkingLineUp AS a WHERE a.YK_guid = dbo.YOUYIKUDO.guid)", Position_guid, YykDvg.Rows[index_v].Cells["po_guid"].Value.ToString().Trim());
                                //content1.Select_nothing(strsql);
                                //判断是否设置装箱数量
                                CsFormShow csformShow = new CsFormShow();
                                csformShow. SetPackingQty(index_v, YykDvg,this, "Quantity", M_stcPalletizingData.Palletizing_Unit.ToString());
                                DataGridViewRow DGVROW = YykDvg.Rows[index_v];
                                Youyikuform = (YouyikuForm)this.Owner;
                                Youyikuform.packingqty = packingqty;
                                if (packingqty.Trim() == "0")
                                {
                                    MessageBox.Show("提示：请重新设置装箱数量");
                                    return;
                                }
                                #region 判断是否超订单数
                                bool b_IsExceedPoNumber = Chekin.IsExceedYYKGUPoNumber(int.Parse(packingqty), YykDvg.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), YykDvg.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim(), YykDvg.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim());
                                if (b_IsExceedPoNumber)
                                {
                                    DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                    if (diaIsExceedPoNumber == DialogResult.Cancel)
                                    {
                                        return;
                                    }
                                }
                                #endregion
                                bool b_v = Youyikuform.GetYYKMarkingPositionRowData(YykDvg.Rows[index_v]);
                                if (b_v)
                                {
                                    #region 先判断上一个单是否箱子放完
                                    string changetype = CsGetData. IsPackingEnd();
                                    if (changetype.Contains("取消切单"))
                                    {
                                        return;
                                    }
                                    else if (changetype.Contains("强制切单"))
                                    {
                                        CsGetData.YYKGUCompelChange();
                                    }
                                    #endregion
                                    Youyikuform.SetToMarking(DGVROW);//更新本次喷码信息
                                    Youyikuform.NewShow();
                                }
                            }
                            else
                            {
                                MessageBox.Show("提示：没有可执行的数据");
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("提示：没有可执行的数据");
                            return;
                        }
                        #endregion
                    }
                    else
                    {
                        Youyikuform = (YouyikuForm)this.Owner;
                        Youyikuform.NewShow();
                    }
                    #endregion
                    this.Close();
                    this.Dispose();
                }

            }

            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 加入优衣库GU排单队列
        /// </summary>
        public void YYKGUScheduling()
        {
            try

            {
                if (MarkPositiondGV.Rows.Count > 0 && MarkPositiondGV.SelectedRows.Count > 0)
                {
                    string Position_guid = "";
                    bool Positio_isindex_v; int Positio_index_v;
                    CsFormShow.DvgGetcurrentIndex(MarkPositiondGV, out Positio_isindex_v, out Positio_index_v);
                    if (Positio_isindex_v)
                    {
                        DataGridViewRow DGVROW = MarkPositiondGV.Rows[Positio_index_v];
                        Position_guid = MarkPositiondGV.Rows[Positio_index_v].Cells["guid"].Value.ToString();
                        #region 计算垛板单位数量
                        M_stcPalletizingData.strCatornLong = MarkPositiondGV.Rows[Positio_index_v].Cells["箱子长"].Value.ToString();
                        M_stcPalletizingData.strCatornWidth = MarkPositiondGV.Rows[Positio_index_v].Cells["箱子宽"].Value.ToString();
                        M_stcPalletizingData.strCatornHeight = MarkPositiondGV.Rows[Positio_index_v].Cells["箱子高"].Value.ToString();
                        M_stcPalletizingData.strWsizeFlag = "start";
                        TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                        while (M_stcPalletizingData.ReadPalletizingflag != "1")
                        {
                            TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                            if (ts.Seconds > 1)
                            {
                                break;
                            }
                        }
                        M_stcPalletizingData.strResetFlag = "start";
                        #endregion
                        // this.Close();
                        // MessageBox.Show("装箱参数设置完成");
                    }
                    #region 设置当前订单
                    DialogResult btchose = MessageBox.Show("是否将订单设置为当前", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (btchose == DialogResult.OK)
                    {

                        #region  
                        if (YykDvg.Rows.Count > 0 && YykDvg.SelectedRows.Count > 0)
                        {
                            bool isindex_v; int index_v;
                            CsFormShow.DvgGetcurrentIndex(YykDvg, out isindex_v, out index_v);
                            if (isindex_v)
                            {
                                //string strsql = string.Format(" UPDATE dbo.YOUYIKUDO SET CatornStyle_guid = '{0}' WHERE Order_No = '{1}'  AND Set_Code = '{2}'AND Warehouse = '{3}'", Position_guid, YykDvg.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), YykDvg.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim(), YykDvg.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim());
                                //content1.Select_nothing(strsql);
                                //判断是否设置装箱数量
                                CsFormShow csformShow = new CsFormShow();
                                csformShow.SetPackingQty(index_v, YykDvg, this, "Quantity", M_stcPalletizingData.Palletizing_Unit.ToString());
                                if (packingqty.Trim() == "0")
                                {
                                    MessageBox.Show("提示：请重新设置装箱数量");
                                    return;
                                }
                                #region 判断是否超订单数
                                bool b_IsExceedPoNumber =Chekin.IsExceedYYKGUPoNumber(int.Parse(packingqty), YykDvg.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), YykDvg.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim(), YykDvg.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim());
                                if (b_IsExceedPoNumber)
                                {
                                    DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                    if (diaIsExceedPoNumber == DialogResult.Cancel)
                                    {
                                        return;
                                    }
                                }
                                //加入带切换订单临时队列表
                                string long_v =  MarkPositiondGV.Rows[Positio_index_v].Cells["箱子长"].Value.ToString();
                                string width_v = MarkPositiondGV.Rows[Positio_index_v].Cells["箱子宽"].Value.ToString();
                                string height_v = MarkPositiondGV.Rows[Positio_index_v].Cells["箱子高"].Value.ToString();
                                string top_v = MarkPositiondGV.Rows[Positio_index_v].Cells["距离底部"].Value.ToString();
                                string left_v = MarkPositiondGV.Rows[Positio_index_v].Cells["距离左边"].Value.ToString();
                                string weight_v = MarkPositiondGV.Rows[Positio_index_v].Cells["重量"].Value.ToString();
                                string ColumnNunber_v = ColumnNunber;
                                string LayernumtBox_v = MarkPositiondGV.Rows[Positio_index_v].Cells["箱内件数"].Value.ToString();
                                string ThicktBox_v = MarkPositiondGV.Rows[Positio_index_v].Cells["产品厚度"].Value.ToString();
                                string GoodWeight_v = MarkPositiondGV.Rows[Positio_index_v].Cells["产品重量"].Value.ToString();
                                string LineNum_v = MarkPositiondGV.Rows[Positio_index_v].Cells["行数"].Value.ToString();
                                int   MarkPattern_v = CsMarking.GetCatornParameter(MarkPositiondGV.Rows[Positio_index_v].Cells["喷码模式"].Value.ToString());
                                int  CatornType_v = CsMarking.GetCatornParameter(MarkPositiondGV.Rows[Positio_index_v].Cells["纸箱大小"].Value.ToString());
                                int   IsRotate_v = CsMarking.GetCatornParameter(IsRotate);
                                int  LabelingType_v = CsMarking.GetCatornParameter(MarkPositiondGV.Rows[Positio_index_v].Cells["贴标类型"].Value.ToString());
                                int  Orientation_v = CsMarking.GetCatornParameter(Orientation);
                                CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO YYKGUSchedulingTable (po_guid,packingqty,IsUnpacking,MarkingIsshieid,PrintIsshieid,GAPIsPrintRun,IsPalletizing,Partitiontype,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,IsScanRun,TiaoMaVal,HookDirection,Entrytime,Long,Width,Heghit,TntervalTop,TntervalLeft,CartonWeight,MarkPattern,CatornType,IsRotate,LabelingType,ColumnNunber,Layernum,Thick,GoodWeight,Orientation,LineNum) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}')", YykDvg.Rows[index_v].Cells["guid"].Value.ToString().Trim(), packingqty, IsUnpacking, MarkingIsshieid, PrintIsshieid, "不运行", IsPalletizing, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, IsScanRun, YykDvg.Rows[index_v].Cells["SKU_Code"].Value.ToString().Trim(), HookDirection, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), long_v, width_v, height_v, top_v, left_v, weight_v, MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, ColumnNunber_v, LayernumtBox_v, ThicktBox_v, GoodWeight_v, Orientation_v, LineNum_v));
                                MessageBox.Show("已加入待换单队列表中");
                                Youyikuform = (YouyikuForm)this.Owner;
                                Youyikuform.NewShow();
                                #endregion
                            }
                            else
                            {
                                MessageBox.Show("提示：没有可执行的数据");
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("提示：没有可执行的数据");
                            return;
                        }
                        #endregion
                    }
                    else
                    {
                        Youyikuform = (YouyikuForm)this.Owner;
                        Youyikuform.NewShow();
                    }
                    #endregion
                    this.Close();
                    this.Dispose();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 优衣库GU切单设置
        /// </summary>
        public void YYKGUSet()
        {
            try
            {
                //检查设备是否启动
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return;
                }
                if (MarkPositiondGV.Rows.Count > 0 && MarkPositiondGV.SelectedRows.Count > 0)
                {
                    string Position_guid = "";
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(MarkPositiondGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        DataGridViewRow DGVROW = MarkPositiondGV.Rows[index_v];
                        Position_guid = MarkPositiondGV.Rows[index_v].Cells["guid"].Value.ToString();
                        #region 计算垛板单位数量
                        M_stcPalletizingData.strCatornLong = MarkPositiondGV.Rows[index_v].Cells["箱子长"].Value.ToString();
                        M_stcPalletizingData.strCatornWidth = MarkPositiondGV.Rows[index_v].Cells["箱子宽"].Value.ToString();
                        M_stcPalletizingData.strCatornHeight = MarkPositiondGV.Rows[index_v].Cells["箱子高"].Value.ToString();
                        M_stcPalletizingData.strWsizeFlag = "start";
                        TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                        while (M_stcPalletizingData.ReadPalletizingflag != "1")
                        {
                            TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                            if (ts.Seconds > 1)
                            {
                                break;
                            }
                        }
                        M_stcPalletizingData.strResetFlag = "start";
                        #endregion
                        // this.Close();
                        // MessageBox.Show("装箱参数设置完成");
                    }
                    #region 设置当前订单
                    DialogResult btchose = MessageBox.Show("是否将订单设置为当前", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (btchose == DialogResult.OK)
                    {

                        #region  
                        if (YykDvg.Rows.Count > 0 && YykDvg.SelectedRows.Count > 0)
                        {
                            //bool isindex_v; int index_v;
                            CsFormShow.DvgGetcurrentIndex(YykDvg, out isindex_v, out index_v);
                            if (isindex_v)
                            {
                                //string strsql = string.Format(" UPDATE dbo.YOUYIKUDO SET CatornStyle_guid = '{0}' WHERE Order_No = '{1}'  AND Set_Code = '{2}'AND Warehouse = '{3}'", Position_guid, YykDvg.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), YykDvg.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim(), YykDvg.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim());
                                //content1.Select_nothing(strsql);
                                //判断是否设置装箱数量
                                CsFormShow csformShow = new CsFormShow();
                                csformShow.SetPackingQty(index_v, YykDvg, this, "Quantity", M_stcPalletizingData.Palletizing_Unit.ToString());
                                DataGridViewRow DGVROW = YykDvg.Rows[index_v];
                                Youyikuform = (YouyikuForm)this.Owner;
                                Youyikuform.packingqty = packingqty;
                                Youyikuform.IsUnpacking = IsUnpacking;
                                Youyikuform.MarkingIsshieid = MarkingIsshieid;
                                Youyikuform.PrintIsshieid = PrintIsshieid;
                                Youyikuform.IsPalletizing = IsPalletizing;
                                Youyikuform.Partitiontype = Partitiontype;
                                Youyikuform.IsPackingRun = IsPackingRun;
                                Youyikuform.IsYaMaHaRun = IsYaMaHaRun;
                                Youyikuform.IsSealingRun = IsSealingRun;
                                Youyikuform.IsPalletizingRun = IsPalletizingRun;
                                Youyikuform.IsScanRun = IsScanRun;
                                Youyikuform.HookDirection = HookDirection;
                                if (packingqty.Trim() == "0")
                                {
                                    MessageBox.Show("提示：请重新设置装箱数量");
                                    return;
                                }
                                #region 判断是否超订单数
                                bool b_IsExceedPoNumber = Chekin.IsExceedYYKGUPoNumber (int.Parse(packingqty), YykDvg.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), YykDvg.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim(), YykDvg.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim());
                                if (b_IsExceedPoNumber)
                                {
                                    DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                    if (diaIsExceedPoNumber == DialogResult.Cancel)
                                    {
                                        return;
                                    }
                                }
                                #endregion
                                bool b_v = Youyikuform.GetYYKMarkingPositionRowData(YykDvg.Rows[index_v]);
                                if (b_v)
                                {
                                    #region 先判断上一个单是否箱子放完
                                    string changetype =CsGetData. IsPackingEnd();
                                    if (changetype.Contains("取消切单"))
                                    {
                                        return;
                                    }
                                    else if (changetype.Contains("强制切单"))
                                    {
                                        //CsGetData.YYKGUCompelChange();
                                        return;
                                    }
                                    #endregion
                                    Youyikuform.SetToMarking(DGVROW);//更新本次喷码信息
                                    Youyikuform.NewShow();
                                }
                            }
                            else
                            {
                                MessageBox.Show("提示：没有可执行的数据");
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("提示：没有可执行的数据");
                            return;
                        }
                        #endregion
                    }
                    else
                    {
                        Youyikuform = (YouyikuForm)this.Owner;
                        Youyikuform.NewShow();
                    }
                    #endregion
                    this.Close();
                    this.Dispose();
                }

            }

            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripSeparator2_Click(object sender, EventArgs e)
        {

        }

        private void MarkPositiondGV_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MouseDouble();
        }
        public void MouseDouble()
        {
            try
            {
                if (MarkPositiondGV.Rows.Count > 0 && MarkPositiondGV.SelectedRows.Count > 0)
                {
                    bool Position_isindex_v; int Positio_index_v;
                    CsFormShow.DvgGetcurrentIndex(MarkPositiondGV, out Position_isindex_v, out Positio_index_v);
                    if (Position_isindex_v)
                    {
                        foreach (Control control in controls_list)
                        {
                            if (control.Name == "longtBox")
                            {
                                control.Text = MarkPositiondGV.Rows[Positio_index_v].Cells["箱子长"].Value.ToString().Trim();
                            }
                            else if (control.Name == "widthtBox")
                            {
                                control.Text = MarkPositiondGV.Rows[Positio_index_v].Cells["箱子宽"].Value.ToString().Trim();
                            }
                            else if (control.Name == "heighttBox")
                            {
                                control.Text = MarkPositiondGV.Rows[Positio_index_v].Cells["箱子高"].Value.ToString().Trim();
                            }
                            else if (control.Name == "CatornStyle_guidtBox")
                            {
                                control.Text = MarkPositiondGV.Rows[Positio_index_v].Cells["guid"].Value.ToString().Trim();
                            }
                        }
                        #region 计算垛板单位数量
                        M_stcPalletizingData.strCatornLong = MarkPositiondGV.Rows[Positio_index_v].Cells["箱子长"].Value.ToString().Trim();
                        M_stcPalletizingData.strCatornWidth = MarkPositiondGV.Rows[Positio_index_v].Cells["箱子宽"].Value.ToString().Trim();
                        M_stcPalletizingData.strCatornHeight = MarkPositiondGV.Rows[Positio_index_v].Cells["箱子高"].Value.ToString().Trim();
                        M_stcPalletizingData.strWsizeFlag = "start";
                        TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                        while (M_stcPalletizingData.ReadPalletizingflag != "1")
                        {
                            TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                            if (ts.Seconds > 1)
                            {
                                break;
                            }
                        }
                        M_stcPalletizingData.strResetFlag = "start";
                        foreach (Control control in controls_list)
                        {
                            if (control.Name == "Palletizing_UnittBox")
                            {
                                control.Text = M_stcPalletizingData.Palletizing_Unit.ToString();
                            }
                        }
                        #endregion
                    }
                    this.Dispose();
                    this.Close();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
