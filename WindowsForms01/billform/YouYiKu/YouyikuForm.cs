﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GetData;
using System.Threading;

namespace WindowsForms01
{
    public partial class YouyikuForm : Form
    {
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        inexcel ss = new inexcel();
        DataTable M_dt_v = new DataTable();
        DataTable dtexist = new DataTable();
        DataSet dsget = new DataSet();
        DataTable dtget = new DataTable();
        OpenFileDialog ofd = new OpenFileDialog();
        YYKFinishMarkForm YYKFinishMarkform = null;
        MarkPositionForm markpositionform = null;
        PositionForm positionform = null;
        BatchImportYYKGUForm BatchImportYYKGUform = null;
        int index_v = 0;
        string Sample_Code = "";
        public string packingqty = "0";
        public string IsUnpacking = "";
        public string MarkingIsshieid = "";
        public string PrintIsshieid = "";
        public string IsPalletizing = "";
        public string Partitiontype = "";
        public string IsPackingRun = "";
        public string IsYaMaHaRun = "";
        public string IsSealingRun = "";
        public string IsPalletizingRun = "";
        public string IsRepeatCheck = "";
        public string IsScanRun = "";
        public string HookDirection = "";
        public bool IsPressDown =true;
        public YouyikuForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.YouyikudGV, 40, 40, true, Color.LightSteelBlue,this);
            //YouyikudGV.ClearSelection();
            NewShow();
            if (!stcUserData.M_username.Contains("starcity"))
            {
                toolStripDropDownButton2.Visible = false;
            }
        }

        private void 导入ExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        }
        public void NewShow()
        {
            try
            {
                this.YouyikudGV.DataSource = null;
                string sqlselect = "SELECT*FROM (SELECT ROW_NUMBER() OVER (ORDER BY Entrytime,Order_No) AS 序号,(SELECT CASE WHEN COUNT(0)<>0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END FROM dbo.SchedulingTable as sc WHERE  sc.订单号码=Order_No AND sc.Warehouse=dbo.YOUYIKUDO.Warehouse AND sc.Set_Code= dbo.YOUYIKUDO.Set_Code) AS '已选择', guid,CatornStyle_guid,po_guid,Order_No,DO_No,Warehouse,Color_Code,Color,Set_Code,SKU_Code,Quantity,packingtotal AS '已完成数量',packingtotal,finishmarknumber, Qty_per_Set,Picking_Unit,Entrytime,dbo.YOUYIKUDO.potype  FROM dbo.YOUYIKUDO WHERE  ISNULL(ENDPO,0)<>1 ) AS a ORDER BY 已选择 DESC ";
                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                //ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(0);
                //this.YouyikudGV.Columns["当前选择"].Visible = false;
                // this.YouyikudGV.Columns["当前选择"].ReadOnly = true;
                if (dt.Rows.Count>0)
                {
                    this.YouyikudGV.DataSource = dt;
                    // this.YouyikudGV.Columns["喷码完成"].Visible = false;
                    this.YouyikudGV.Columns["guid"].Visible = false;
                    this.YouyikudGV.Columns["CatornStyle_guid"].Visible = false;
                    this.YouyikudGV.Columns["po_guid"].Visible = false;
                    this.YouyikudGV.Columns["potype"].Visible = false;
                    YouyikudGV.Columns["finishmarknumber"].Visible = false;
                    YouyikudGV.Columns["packingtotal"].Visible = false;
                    for (int i = 0; i < this.YouyikudGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.YouyikudGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        public void NewShow(string Order_No_, string Warehouse_, string Set_Code_)
        {
            try
            {
                this.YouyikudGV.DataSource = null;
                string sqlselect = "SELECT*FROM  (SELECT ROW_NUMBER() OVER(ORDER BY Entrytime, Order_No) AS 序号,(SELECT CASE WHEN COUNT(0)<>0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END FROM dbo.SchedulingTable as sc WHERE  sc.订单号码=Order_No AND sc.Warehouse=dbo.YOUYIKUDO.Warehouse AND sc.Set_Code= dbo.YOUYIKUDO.Set_Code) AS '已选择', guid,CatornStyle_guid,po_guid,Order_No,DO_No,Warehouse,Color_Code,Color,Set_Code,SKU_Code,Quantity,packingtotal AS '已完成数量',packingtotal,finishmarknumber, Qty_per_Set,Picking_Unit,potype,Entrytime FROM dbo.YOUYIKUDO WHERE  Order_No LIKE '%' + '" + Order_No_ + "' + '%' AND Warehouse LIKE '%' + '" + Warehouse_ + "' + '%' AND Set_Code LIKE '%' + '" + Set_Code_ + "' + '%' and ISNULL(ENDPO,0)<> 1 ) AS a ORDER BY 已选择 DESC";
                DataTable dt =CsFormShow.GoSqlSelect(sqlselect);
                // ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(0);
                if (dt.Rows.Count>0)
                {
                    this.YouyikudGV.DataSource = dt;
                    //this.YouyikudGV.Columns["当前选择"].Visible = false;
                    //this.YouyikudGV.Columns["当前选择"].ReadOnly = true;
                    //this.YouyikudGV.Columns["喷码完成"].Visible = false;
                    this.YouyikudGV.Columns["guid"].Visible = false;
                    this.YouyikudGV.Columns["CatornStyle_guid"].Visible = false;
                    this.YouyikudGV.Columns["po_guid"].Visible = false;
                    YouyikudGV.Columns["finishmarknumber"].Visible = false;
                    YouyikudGV.Columns["packingtotal"].Visible = false;
                    for (int i = 0; i < this.YouyikudGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.YouyikudGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 删除数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //if (stcUserData.M_username != "admin")
                //{
                //    MessageBox.Show("提示：非admin管理员不能进行此操作");
                //    return;
                //}
                Delet_click.Delete_TSMenuItem_Click(this, YouyikudGV, "dbo.YOUYIKUDO");
                NewShow();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        /// <summary>
        /// 置为当前时将当前选择的颜色范围的行数据放在当前队列最前面并清空之前的队列
        /// </summary>
        /// <param name="DgvRow"></param>
        public void SetToMarking(DataGridViewRow DgvRow)
        {
            try
            {
                string Order_No = DgvRow.Cells["Order_No"].Value.ToString().Trim();
                string Color_Code = DgvRow.Cells["Color_Code"].Value.ToString().Trim();
                string SKU_Code = DgvRow.Cells["SKU_Code"].Value.ToString().Trim();
                string Warehouse = DgvRow.Cells["Warehouse"].Value.ToString().Trim();
                string Set_Code = DgvRow.Cells["Set_Code"].Value.ToString().Trim();
                string Po_guid = DgvRow.Cells["guid"].Value.ToString().Trim();
                string sqlselect = string.Format("select*from dbo.YYKMarkingLineUp where ISNULL(IsEndMarking,0)<>1 and Order_No='{0}' and Color_Code='{1}' and Warehouse='{2}' and Set_Code='{3}' ", Order_No, Color_Code, Warehouse, Set_Code);
                DataSet ds = content1.Select_nothing(sqlselect);
                if (ds.Tables[0].Rows.Count>0)
                {
                    MessageBox.Show("当前订单已经在队列中");
                    return;   
                }
                Dictionary<string, string> DicMarking_colorcode = new Dictionary<string, string>();
                //优衣库喷码信息
                DicMarking_colorcode.Clear();
                DicMarking_colorcode["Color_Code"] = DgvRow.Cells["Color_Code"].Value.ToString().Trim();
                DicMarking_colorcode["Order_No"] = DgvRow.Cells["Order_No"].Value.ToString().Trim();
                DicMarking_colorcode["Quantity"] =packingqty /*DgvRow.Cells["Quantity"].Value.ToString().Trim()*/;
                DicMarking_colorcode["YK_guid"] = DgvRow.Cells["guid"].Value.ToString().Trim();
                DicMarking_colorcode["SKU_Code"] = DgvRow.Cells["SKU_Code"].Value.ToString().Trim();
                DicMarking_colorcode["Warehouse"] = DgvRow.Cells["Warehouse"].Value.ToString().Trim();
                DicMarking_colorcode["Set_Code"] = DgvRow.Cells["Set_Code"].Value.ToString().Trim();
                DicMarking_colorcode["Qty_per_Set"] = DgvRow.Cells["Qty_per_Set"].Value.ToString().Trim();
                DicMarking_colorcode["Picking_Unit"] = DgvRow.Cells["Picking_Unit"].Value.ToString().Trim();
                DicMarking_colorcode["qty"] = "1";
                DicMarking_colorcode["FinishPalletizingNum"] ="0";
                DicMarking_colorcode["END"] = "";
                DicMarking_colorcode["IsPalletizing"] = CsMarking.GetCatornParameter(IsPalletizing).ToString();
                string potype_v = DgvRow.Cells["potype"].Value.ToString().Trim();
                DataTable dt_carton = CsFormShow.GoSqlSelect(string.Format("SELECT yyk.Order_No ,SUM(qty) AS finishmarknumberTotal FROM (SELECT Order_No,Set_Code,(SUM(ISNULL(finishmarknumber,0))/COUNT(*)) AS qty,Warehouse,COUNT(*) AS num FROM dbo.YOUYIKUDO WHERE Order_No='{0}'  GROUP BY Order_No,Set_Code,Warehouse) AS yyk GROUP BY yyk.Order_No", DgvRow.Cells["Order_No"].Value.ToString().Trim()));
                DicMarking_colorcode["finishmarknumberTotal"] =(int.Parse( dt_carton.Rows[0]["finishmarknumberTotal"].ToString().Trim())+1).ToString();
                DicMarking_colorcode["BatchNo"] = M_stcScanData.BatchNo_Now;
                CsPakingData.M_list_Dic_colorcode.Add(DicMarking_colorcode);
                //订单队列录入数据库
                string sqlstr = string.Format("INSERT INTO YYKMarkingLineUp (YK_guid,Order_No,Color_Code,Order_Qty,Entrytime,SKU_Code,Warehouse,potype,CurrentPackingNum,Cartoncount,Set_Code,MarkingIsshieid,IsUnpacking,Qty_per_Set,Picking_Unit,IsPalletizing,PartitionType,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,PrintIsshieid,IsScanRun,HookDirection,DeviceName) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}')", DicMarking_colorcode["YK_guid"], DicMarking_colorcode["Order_No"], DicMarking_colorcode["Color_Code"], DicMarking_colorcode["Quantity"], DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), DicMarking_colorcode["SKU_Code"], DicMarking_colorcode["Warehouse"], potype_v, packingqty,1, DicMarking_colorcode["Set_Code"], MarkingIsshieid, IsUnpacking, DicMarking_colorcode["Qty_per_Set"], DicMarking_colorcode["Picking_Unit"],IsPalletizing, Partitiontype,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,PrintIsshieid,IsScanRun, HookDirection,CSReadPlc.DevName);

                CsFormShow.GoSqlUpdateInsert(sqlstr);
                //加入历史记录表
                int count_v = CsFormShow.GoSqlSelectCount(string.Format("select*from dbo.YYKGUHistoricRcords where  Order_No='{0}' and Warehouse='{1}' and Set_Code='{2}'", Order_No, Warehouse, Set_Code));
                if (count_v == 0)
                {
                    string yyklineup_guid = CsFormShow.SqlGetVal(string.Format("SELECT*FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", DicMarking_colorcode["Order_No"], DicMarking_colorcode["Warehouse"], DicMarking_colorcode["Set_Code"]), "guid");

                    CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO YYKGUHistoricRcords(po_guid, LineUp_guid, Order_No, Set_Code, Warehouse, StartTime, Total) VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", Po_guid, yyklineup_guid, DicMarking_colorcode["Order_No"], DicMarking_colorcode["Set_Code"], DicMarking_colorcode["Warehouse"], DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") , DgvRow.Cells["Quantity"].Value.ToString().Trim()));
                }
                //更新扫码队列
                CsGetData.UpdataScanLineUp();
                Cslogfun.WriteToCartornLog("" + Environment.NewLine);
                Cslogfun.WriteToCartornLog("切换订单："+ Order_No+ ",Warehouse:"+ Warehouse+ ",Set_Code:"+ Set_Code+",当前设置数量："+ packingqty);
                CsMarking.M_MarkParameter = "ColorCode";
                MessageBox.Show("已切换为当前订单");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            NewShow(PotSpTBox.TextBox.Text.ToString().Trim(), CktSpTBox.TextBox.Text.ToString().Trim(), SetCodetSpTBox.TextBox.Text.ToString().Trim());
        }

        private void YouyikuForm_Load(object sender, EventArgs e)
        {

        }

        private void MarkendtSpBtn_Click(object sender, EventArgs e)
        {
            DialogResult dia = MessageBox.Show("是否手动执行一次喷码动作？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dia == DialogResult.No)
            {
                return;
            }
            else
            {
                CsMarking.ManualUpdatePackingPage();
            }
        }

        private void FinishmarktSpBtn_Click(object sender, EventArgs e)
        {
             CsFormShow.FormShowDialog(YYKFinishMarkform,this,"WindowsForms01.YYKFinishMarkForm");
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            try
            {
                if (YouyikudGV.Rows.Count > 0 && YouyikudGV.SelectedRows.Count > 0)
                {
                    bool isindex_v;int index_v;
                    CsFormShow.DvgGetcurrentIndex(YouyikudGV,out isindex_v,out index_v);
                    if (isindex_v)
                    {
                        DataGridViewRow DGVROW = YouyikudGV.Rows[index_v];
                        bool b_v = GetYYKMarkingPositionRowData(YouyikudGV.Rows[index_v]);
                        if (b_v)
                        {
                            SetToMarking(DGVROW);//更新本次喷码信息
                            NewShow();
                        }
                    }
                    else
                    {
                        MessageBox.Show("提示：没有可执行的数据");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("提示：没有可执行的数据");
                    return;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }
        /// <summary>
        /// 将当前选择写入PLC
        /// </summary>
        /// <param name="DgvRow"></param>
        /// <returns></returns>
        public bool GetYYKMarkingPositionRowData(DataGridViewRow DgvRow)
        {
            bool bool_ = false;
            try
            {
                DataSet ds = new DataSet();
                string Poguid_= DgvRow.Cells["guid"].Value.ToString().Trim();
                #region 计算箱件数
                string Layernum = "";
                string Set_Code = DgvRow.Cells["Set_Code"].Value.ToString().Trim();
                DataTable dtLayernum = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YOUYIKUDO WHERE guid='{0}'", Poguid_));
                Layernum = CsGetData.GetLayernum(dtLayernum, IsUnpacking, "YYK").ToString();
                #endregion
                if (!string.IsNullOrEmpty(Poguid_))
                {
                    string selectsql = "";

                   selectsql = string.Format("SELECT Quantity, Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '1' THEN '不喷色号' ELSE '喷色号' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向',CASE WHEN  IsRepeatCheck = '1' THEN '是' ELSE '否' END AS '是否再检针',packingqty FROM dbo.YOUYIKUDO, dbo.YYKGUSchedulingTable  where  dbo.YOUYIKUDO.guid = dbo.YYKGUSchedulingTable.po_guid and  dbo.YOUYIKUDO.guid='{0}'", Poguid_);
                    ds = content1.Select_nothing(selectsql);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        MessageBox.Show("当前订单没有设置纸箱参数");
                        return bool_;
                    }
                    else
                    {
                        CsPakingData.M_DicMarking_colorcode.Clear();
                        //计算厚度
                        int Height = int.Parse(ds.Tables[0].Rows[0]["箱子高"].ToString().Trim());
                        int culumn = int.Parse(ds.Tables[0].Rows[0]["列数"].ToString().Trim());
                        string Thick = (Height / (int.Parse(Layernum) / culumn)).ToString();
                        //计算隔板类型
                        int catornlong = int.Parse(ds.Tables[0].Rows[0]["箱子长"].ToString().Trim());
                        int catornwidth = int.Parse(ds.Tables[0].Rows[0]["箱子宽"].ToString().Trim());
                        string PartitionType = "";
                        string IsBottomPartition = "";
                        string IsTopPartition = "";
                        CsGetData.GetPartitionData(catornlong, catornwidth, Partitiontype,out PartitionType, out IsTopPartition,out IsBottomPartition);
                        CsPakingData.M_DicMarking_colorcode["IsTopPartition"] = IsTopPartition;
                        CsPakingData.M_DicMarking_colorcode["IsBottomPartition"] = IsBottomPartition;
                        CsPakingData.M_DicMarking_colorcode["Long"] = ds.Tables[0].Rows[0]["箱子长"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Width"] = ds.Tables[0].Rows[0]["箱子宽"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Heghit"] = ds.Tables[0].Rows[0]["箱子高"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Updown_distance"] = ds.Tables[0].Rows[0]["距离底部"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Leftright_distance"] = ds.Tables[0].Rows[0]["距离左边"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["CartonWeight"] = ds.Tables[0].Rows[0]["重量"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["GoodWeight"] = ds.Tables[0].Rows[0]["产品重量"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["ColumnNunber"] = ds.Tables[0].Rows[0]["列数"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["LineNum"] = ds.Tables[0].Rows[0]["行数"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Layernum"] = Layernum;
                        CsPakingData.M_DicMarking_colorcode["Thick"] = Thick;
                        //CsPakingData.M_DicMarking_colorcode["Jianspeed"] = ds.Tables[0].Rows[0]["加减速度"].ToString().Trim();
                        //CsPakingData.M_DicMarking_colorcode["Grabspeed"] = ds.Tables[0].Rows[0]["抓取速度"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["MarkPattern"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["喷码模式"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["CatornType"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsRotate"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["是否旋转"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["LabelingType"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["贴标类型"].ToString().Trim()).ToString();
                        if (string.IsNullOrEmpty(PartitionType))
                        {
                            CsPakingData.M_DicMarking_colorcode["PartitionType"] = "0";
                        }
                        else
                        {
                            CsPakingData.M_DicMarking_colorcode["PartitionType"] = CsMarking.GetCatornParameter(PartitionType).ToString();
                        }
                        CsPakingData.M_DicMarking_colorcode["Orientation"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["正反方向"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsRepeatCheck"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["是否再检针"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsPalletizing"] = CsMarking.GetCatornParameter(IsPalletizing).ToString();
                        CsPakingData.M_DicMarking_colorcode["Quantity"] = ds.Tables[0].Rows[0]["packingqty"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["IsPackingRun"] = CsMarking.GetCatornParameter(IsPackingRun).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsYaMaHaRun"] = CsMarking.GetCatornParameter(IsYaMaHaRun).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsSealingRun"] = CsMarking.GetCatornParameter(IsSealingRun).ToString();
                        CsPakingData.M_DicMarking_colorcode["MarkingIsshieid"] = CsMarking.GetCatornParameter(MarkingIsshieid).ToString();
                        CsPakingData.M_DicMarking_colorcode["PrintIsshieid"] = CsMarking.GetCatornParameter(PrintIsshieid).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsPalletizingRun"] = CsMarking.GetCatornParameter(IsPalletizingRun).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsScanRun"] = CsMarking.GetCatornParameter(IsScanRun).ToString();
                        CsPakingData.M_DicMarking_colorcode["HookDirection"] = CsMarking.GetCatornParameter(HookDirection).ToString();
                        //  CsPakingData.M_DicMarking_colorcode["装箱单guid"] = ds.Tables[0].Rows[0]["guid"].ToString().Trim();
                        bool_ = true;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return bool_;
        }


        private void YouyikudGV_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            // CsFormShow.SetSelectRow(YouyikudGV);
            //YouyikudGV.ClearSelection();
            //YouyikudGV.Rows[index_v].Selected = true;
           // CsFormShow.DvgSetcurrent(YouyikudGV);
        }

        private void 设置纸箱参数ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //bool boolval = CsFormShow.DvgRowSetcurrent(YouyikudGV);
                //if (!boolval)
                //    return;
                if (YouyikudGV.Rows.Count > 0 && YouyikudGV.SelectedRows.Count > 0)
                {
                    bool b_v = CsFormShow.IsExistenceInSql("dbo.YYKGUSchedulingTable", "po_guid", YouyikudGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim());
                    if (b_v)
                    {
                        MessageBox.Show("此订单已经在排单队列中，请重新选择");
                        return;
                    }
                    bool isindex_v;int index_v;
                    CsFormShow.DvgGetcurrentIndex(YouyikudGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        //判断是否有混色号同类型在队列中
                        string sql_isexist = string.Format("SELECT*FROM dbo.YYKGUSchedulingTable AS a WHERE EXISTS(SELECT *FROM dbo.YOUYIKUDO WHERE guid=a.po_guid and Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}')", YouyikudGV.Rows[index_v].Cells["Order_No"].Value.ToString(), YouyikudGV.Rows[index_v].Cells["Warehouse"].Value.ToString(), YouyikudGV.Rows[index_v].Cells["Set_Code"].Value.ToString());
                        DataTable dt = content1.Select_nothing(sql_isexist).Tables[0];
                        if (dt.Rows.Count>0)
                        {
                            MessageBox.Show("混色号同类型已经在排单队列中，请到排单表中直接换单");
                            return;
                        }

                        if (markpositionform == null || markpositionform.IsDisposed)
                        {
                            markpositionform = new MarkPositionForm(YouyikudGV.Rows[index_v].Cells["guid"].Value.ToString(), YouyikudGV, YouyikudGV.Rows[index_v].Cells["CatornStyle_guid"].Value.ToString());
                            markpositionform.Owner = this;
                            markpositionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                            markpositionform.ShowDialog();
                        }
                        else
                        {
                            markpositionform.ShowDialog();
                            markpositionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                            markpositionform.BringToFront();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("提示：没有可执行的数据");
                    return;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 查看当前纸箱参数toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                if (YouyikudGV.Rows.Count > 0 && YouyikudGV.SelectedRows.Count > 0)
                {

                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(YouyikudGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        string selectsql = string.Format("SELECT Quantity,packingtotal AS '箱数',Long AS '箱子长',Width AS '箱子宽',Heghit AS '箱子高',TntervalTop AS '距离底部',TntervalLeft AS '距离左边',CartonWeight AS '重量',ColumnNunber AS '列数',LineNum AS '行数',Layernum AS '箱内件数',Thick AS '产品厚度',CASE WHEN  MarkPattern='1' THEN '不喷色号' ELSE '喷色号' END  AS '喷码模式',  CASE WHEN  CatornType='1' THEN '大' ELSE '小' END  AS '纸箱大小',CASE WHEN  IsRotate='1' THEN '旋转' ELSE '不旋转' END AS '是否旋转',CASE WHEN  LabelingType='1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType='1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型' , CASE WHEN  Orientation='1' THEN '反' ELSE '正' END AS '正反方向' FROM dbo.YOUYIKUDO , dbo.YYKGUSchedulingTable where dbo.YOUYIKUDO.guid=dbo.YYKGUSchedulingTable.po_guid and  dbo.YOUYIKUDO.guid='{0}'", YouyikudGV.Rows[index_v].Cells["guid"].Value.ToString().Trim());
                        ds = content1.Select_nothing(selectsql);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            MessageBox.Show("当前订单没有设置纸箱参数");
                            return;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("提示：请选择有效订单");
                    return;
                }

                string long_v = ds.Tables[0].Rows[0]["箱子长"].ToString();
                string width_v = ds.Tables[0].Rows[0]["箱子宽"].ToString();
                string Heghit_v = ds.Tables[0].Rows[0]["箱子高"].ToString();
                string TntervalTop_v = ds.Tables[0].Rows[0]["距离底部"].ToString();
                string TntervalLeft_v = ds.Tables[0].Rows[0]["距离左边"].ToString();
                string CartonWeight_v = ds.Tables[0].Rows[0]["重量"].ToString();
                string GoodWeight_v = ds.Tables[0].Rows[0]["重量"].ToString();
                string ColumnNunber_v = ds.Tables[0].Rows[0]["列数"].ToString();
                string Layernum_v = ds.Tables[0].Rows[0]["箱内件数"].ToString();
                string Thick_v = ds.Tables[0].Rows[0]["产品厚度"].ToString();
                //string Jianspeed_v = ds.Tables[0].Rows[0]["加减速度"].ToString();
                //string Grabspeed_v = ds.Tables[0].Rows[0]["抓取速度"].ToString();
                string MarkPattern_v = ds.Tables[0].Rows[0]["喷码模式"].ToString();
                string CatornType_v = ds.Tables[0].Rows[0]["纸箱大小"].ToString();
                string IsRotate_v = ds.Tables[0].Rows[0]["是否旋转"].ToString();
                string LabelingType_v = ds.Tables[0].Rows[0]["贴标类型"].ToString();
                string PartitionType_v = ds.Tables[0].Rows[0]["隔板类型"].ToString();
                string Quantity_v = ds.Tables[0].Rows[0]["箱数"].ToString();
                string Orientation_v = ds.Tables[0].Rows[0]["正反方向"].ToString();
                string LineNum_v = ds.Tables[0].Rows[0]["行数"].ToString();
                if (positionform == null || positionform.IsDisposed)
                {
                    positionform = new PositionForm(long_v, width_v, Heghit_v, TntervalTop_v, TntervalLeft_v, CartonWeight_v, GoodWeight_v, ColumnNunber_v, Layernum_v, Thick_v, "", "", MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, PartitionType_v, Quantity_v,Orientation_v, LineNum_v);
                    positionform.Owner = this;
                    positionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    positionform.ShowDialog();
                }
                else
                {
                    positionform.ShowDialog();
                    positionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    positionform.BringToFront();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 导入ExcelDoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ofd.Filter = "EXCEL文档(*.xls)|*.xls";
                string fileName = "";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    fileName = ofd.FileName;
                    CsGetData.Batch_import_YYKGU(fileName);
                }
                else
                {
                    return;
                }
                NewShow();
                MessageBox.Show("导入成功！");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }

        private void gU数据导入ExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// 优衣库po数据
        /// </summary>
        /// <param name="fileName_"></param>
        public Dictionary<string, string> InPoData(string fileName_)
        {
            Dictionary<string, string> dic_v = new Dictionary<string, string>();
            try
            {
                DataTable M_dt = new DataTable();
                GetData.GetExcelData getData = new GetExcelData();
                M_dt = getData.bind(fileName_, "PO", "A2", "BD");
                #region 录入订单数据到数据库
                if (M_dt.Rows.Count > 0)
                {
                    DataTable dt_insert = new DataTable();
                    dt_insert = M_dt.Clone();
                    for (int i = 0; i < M_dt.Rows.Count; i++)
                    {
                        string sqlstr = string.Format("SELECT*FROM dbo.YOUYIKUPO WHERE  Order_No='{0}' AND SKU_Code='{1}' AND Color_Code='{2}'", M_dt.Rows[i]["Order No#"].ToString().Trim(), M_dt.Rows[i]["SKU Code"].ToString().Trim(), M_dt.Rows[i]["Color Code"].ToString().Trim());
                        DataTable dt_old = CsFormShow.GoSqlSelect(sqlstr);
                        dtexist = dt_old;
                        if (dtexist.Rows.Count == 0)
                        {
                            dt_insert.ImportRow(M_dt.Rows[i]);
                        }
                    }

                    dt_insert.Columns["Order No#"].ColumnName = "Order_No";
                    dt_insert.Columns["Revision No#"].ColumnName = "Revision_No";
                    dt_insert.Columns["Order Plan Number"].ColumnName = "Order_Plan_Number";
                    dt_insert.Columns["Item Code"].ColumnName = "Item_Code";
                    dt_insert.Columns["Color Code"].ColumnName = "Color_Code";
                    dt_insert.Columns["Color"].ColumnName = "Color";
                    dt_insert.Columns["Size Code"].ColumnName = "Size_Code";
                    dt_insert.Columns["Size"].ColumnName = "Size";
                    dt_insert.Columns["SKU Code"].ColumnName = "SKU_Code";
                    dt_insert.Columns["Order Qty(pcs)"].ColumnName = "Order_Qty";
                    dt_insert.Columns["Item Brand"].ColumnName = "Item_Brand";
                    dt_insert.Columns["Sample Code"].ColumnName = "Sample_Code";
                    dt_insert.Columns.Add("Entrytime");
                    for (int i = 0; i < dt_insert.Rows.Count; i++)
                    {
                        dt_insert.Rows[i]["Entrytime"] = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.Millisecond.ToString();
                    }
                    ss.insertToSql(dt_insert, "YOUYIKUPO");
                    if (M_dt.Rows.Count > 0)
                    {
                        dic_v["Sample_Code"] = M_dt.Rows[0]["Sample Code"].ToString().Trim();
                        dic_v["Item_Brand"] = M_dt.Rows[0]["Item Brand"].ToString().Trim();
                    }
                }
                else
                {
                    MessageBox.Show("没有数据！");
                }
                #endregion
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
            return dic_v;
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return ;
                }
                DialogResult btchose = MessageBox.Show("是否终止订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    DataSet ds = new DataSet();
                    if (YouyikudGV.Rows.Count > 0 && YouyikudGV.SelectedRows.Count > 0)
                    {
                        bool isindex_v; int index_v;
                        CsFormShow.DvgGetcurrentIndex(YouyikudGV, out isindex_v, out index_v);
                        if (isindex_v)
                        {
                            string potype = YouyikudGV.Rows[index_v].Cells["potype"].Value.ToString().Trim();
                            string strpo = YouyikudGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim();
                            string stridentification = YouyikudGV.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim();
                            string Set_Code = YouyikudGV.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim();
                            string Color_Code = YouyikudGV.Rows[index_v].Cells["Color_Code"].Value.ToString().Trim();
                            string strPoTiaoMa = YouyikudGV.Rows[index_v].Cells["SKU_Code"].Value.ToString().Trim();
                            string selectsql = string.Format("UPDATE dbo.YOUYIKUDO SET ENDPO=1,EndTime='{3}' WHERE Order_No = '{0}'  AND Set_Code = '{1}'AND Warehouse = '{2}'", YouyikudGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), YouyikudGV.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim(), YouyikudGV.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            content1.Select_nothing(selectsql);
                            //清除本次队列
                            string sqlstrlineup = string.Format("DELETE FROM  dbo.YYKMarkingLineUp WHERE Order_No='{0}'AND SKU_Code='{1}' AND Warehouse='{2}' AND potype='{3}'AND Color_Code='{4}' AND Set_Code='{5}'", strpo, strPoTiaoMa, stridentification, potype, Color_Code, Set_Code);
                            CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                            CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM  dbo.PalletizingtypeTable  WHERE potype = '{0}' AND DeviceName='{1}'", potype,CSReadPlc.DevName.Trim()));
                            CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM  dbo.YYK_GU_Current WHERE po='{0}'AND Warehouse='{1}' AND potype='{2}'AND Set_Code='{3}'", strpo, stridentification, potype, Set_Code));
                            //重新加载喷码队列
                            if (M_stcScanData.potype.Contains("GU")|| M_stcScanData.potype.Contains("UNIQLO"))
                            {
                                CsInitialization.UpdateYYKGULineUp();
                                CsInitialization.UpdateYYKGUPalletizingLineUp();
                            }
                            //重新加载扫码队列信息
                            CsGetData.UpdataScanLineUp();//更新扫码队列
                            CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                        }
                    }
                    else
                    {
                        MessageBox.Show("提示：请选择有效订单");
                        return;
                    }
                    NewShow();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 手动执行喷码ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dia = MessageBox.Show("是否手动执行一次喷码动作？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dia == DialogResult.No)
            {
                return;
            }
            else
            {
                CsMarking.ManualUpdatePackingPage();
                MessageBox.Show("喷码完成一次");
            }
        }

        private void 手动执行码垛ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //CsPalletizing.YYKGUPalletizingLineUpGoOnce();
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult btchose = MessageBox.Show("是否强制结束当前订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(this.YouyikudGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        bool b_v=   CsGetData.YYKGUCompelChange(this.YouyikudGV, index_v);
                        if (!b_v)
                        {
                            return;
                        }
                        MessageBox.Show("已结束当前订单");
                        NewShow();
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult btchose = MessageBox.Show("是否强制结束当前订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(this.YouyikudGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        bool b_v=  CsGetData.YYKGUUnusualCompelChange(this.YouyikudGV, index_v);
                        if (!b_v)
                        {
                            return;
                        }
                        MessageBox.Show("已结束当前订单");
                        NewShow();
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 批量导入ExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(BatchImportYYKGUform,this,"WindowsForms01.BatchImportYYKGUForm");
        }

        private void toolStripMenuItem3_Click_1(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                if (YouyikudGV.Rows.Count > 0 && YouyikudGV.SelectedRows.Count > 0)
                {

                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(YouyikudGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        string selectsql = string.Format("SELECT Quantity,packingtotal AS '箱数',Long AS '箱子长',Width AS '箱子宽',Heghit AS '箱子高',TntervalTop AS '距离底部',TntervalLeft AS '距离左边',CartonWeight AS '重量',ColumnNunber AS '列数',LineNum AS '行数',Layernum AS '箱内件数',Thick AS '产品厚度',CASE WHEN  MarkPattern='1' THEN '不喷色号' ELSE '喷色号' END  AS '喷码模式',  CASE WHEN  CatornType='1' THEN '大' ELSE '小' END  AS '纸箱大小',CASE WHEN  IsRotate='1' THEN '旋转' ELSE '不旋转' END AS '是否旋转',CASE WHEN  LabelingType='1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType='1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型' , CASE WHEN  Orientation='1' THEN '反' ELSE '正' END AS '正反方向' FROM dbo.YOUYIKUDO , dbo.YYKGUSchedulingTable where dbo.YOUYIKUDO.guid=dbo.YYKGUSchedulingTable.po_guid and  dbo.YOUYIKUDO.guid='{0}'", YouyikudGV.Rows[index_v].Cells["guid"].Value.ToString().Trim());
                        ds = content1.Select_nothing(selectsql);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            MessageBox.Show("当前订单没有设置纸箱参数");
                            return;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("提示：请选择有效订单");
                    return;
                }

                string long_v = ds.Tables[0].Rows[0]["箱子长"].ToString();
                string width_v = ds.Tables[0].Rows[0]["箱子宽"].ToString();
                string Heghit_v = ds.Tables[0].Rows[0]["箱子高"].ToString();
                string TntervalTop_v = ds.Tables[0].Rows[0]["距离底部"].ToString();
                string TntervalLeft_v = ds.Tables[0].Rows[0]["距离左边"].ToString();
                string CartonWeight_v = ds.Tables[0].Rows[0]["重量"].ToString();
                string GoodWeight_v = ds.Tables[0].Rows[0]["重量"].ToString();
                string ColumnNunber_v = ds.Tables[0].Rows[0]["列数"].ToString();
                string Layernum_v = ds.Tables[0].Rows[0]["箱内件数"].ToString();
                string Thick_v = ds.Tables[0].Rows[0]["产品厚度"].ToString();
                //string Jianspeed_v = ds.Tables[0].Rows[0]["加减速度"].ToString();
                //string Grabspeed_v = ds.Tables[0].Rows[0]["抓取速度"].ToString();
                string MarkPattern_v = ds.Tables[0].Rows[0]["喷码模式"].ToString();
                string CatornType_v = ds.Tables[0].Rows[0]["纸箱大小"].ToString();
                string IsRotate_v = ds.Tables[0].Rows[0]["是否旋转"].ToString();
                string LabelingType_v = ds.Tables[0].Rows[0]["贴标类型"].ToString();
                string PartitionType_v = ds.Tables[0].Rows[0]["隔板类型"].ToString();
                string Quantity_v = ds.Tables[0].Rows[0]["箱数"].ToString();
                string Orientation_v = ds.Tables[0].Rows[0]["正反方向"].ToString();
                string LineNum_v = ds.Tables[0].Rows[0]["行数"].ToString();
                if (positionform == null || positionform.IsDisposed)
                {
                    positionform = new PositionForm(long_v, width_v, Heghit_v, TntervalTop_v, TntervalLeft_v, CartonWeight_v, GoodWeight_v, ColumnNunber_v, Layernum_v, Thick_v, "", "", MarkPattern_v, CatornType_v, IsRotate_v, LabelingType_v, PartitionType_v, Quantity_v, Orientation_v, LineNum_v);
                    positionform.Owner = this;
                    positionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    positionform.ShowDialog();
                }
                else
                {
                    positionform.ShowDialog();
                    positionform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    positionform.BringToFront();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 加入列队ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //bool boolval = CsFormShow.DvgRowSetcurrent(YouyikudGV);
                //if (!boolval)
                //    return;
                if (YouyikudGV.Rows.Count > 0 && YouyikudGV.SelectedRows.Count > 0)
                {
                    bool b_v = CsFormShow.IsExistenceInSql("dbo.SchedulingTable", "po_guid", YouyikudGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim());
                    if (b_v)
                    {
                        MessageBox.Show("此订单已经在排单队列中，请重新选择");
                        return;
                    }
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(YouyikudGV, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        //判断是否有混色号同类型在队列中
                        string sql_isexist = string.Format("SELECT*FROM dbo.YYKGUSchedulingTable AS a WHERE EXISTS(SELECT *FROM dbo.YOUYIKUDO WHERE guid=a.po_guid and Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}')", YouyikudGV.Rows[index_v].Cells["Order_No"].Value.ToString(), YouyikudGV.Rows[index_v].Cells["Warehouse"].Value.ToString(), YouyikudGV.Rows[index_v].Cells["Set_Code"].Value.ToString());
                        DataTable dt = CsFormShow.GoSqlSelect(sql_isexist); 
                        if (dt.Rows.Count > 0)
                        {
                            MessageBox.Show("混色号同类型已经在排单队列中，请到排单表中直接换单");
                            return;
                        }
                        #region 设置当前订单
                        DialogResult btchose = MessageBox.Show("是否将订单设置为当前", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (btchose == DialogResult.OK)
                        {
                            CsFormShow.DvgGetcurrentIndex(YouyikudGV, out isindex_v, out index_v);
                                //string strsql = string.Format(" UPDATE dbo.YOUYIKUDO SET CatornStyle_guid = '{0}' WHERE Order_No = '{1}'  AND Set_Code = '{2}'AND Warehouse = '{3}'", Position_guid, YouyikudGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), YouyikudGV.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim(), YouyikudGV.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim());
                                //content1.Select_nothing(strsql);
                                //判断是否设置装箱数量
                            CsFormShow csformShow = new CsFormShow();
                            csformShow.SetPackingQty(index_v, YouyikudGV, this, "Quantity","0");
                            if (packingqty.Trim() == "0")
                            {
                                MessageBox.Show("提示：请重新设置装箱数量");
                                return;
                            }
                            #region 判断是否超订单数
                            bool b_IsExceedPoNumber = Chekin.IsExceedYYKGUPoNumber(int.Parse(packingqty), YouyikudGV.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), YouyikudGV.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim(), YouyikudGV.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim());
                            if (b_IsExceedPoNumber)
                            {
                                DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                if (diaIsExceedPoNumber == DialogResult.Cancel)
                                {
                                    return;
                                }
                            }
                            MessageBox.Show("已加入待换单队列表中");
                            NewShow();
                            #endregion
                        }
                        else
                        {
                            NewShow();
                        }
                        #endregion
                    }
                }
                else
                {
                    MessageBox.Show("提示：没有可执行的数据");
                    return;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
    }
}
