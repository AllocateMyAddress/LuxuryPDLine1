﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class YYKFinishMarkForm : Form
    {
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        public YYKFinishMarkForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.YYKMarkFinishdgv, 40, 40, true, Color.LightSteelBlue,this);
            NewPakingShow();
        }

        private void SelecttSpBtn_Click(object sender, EventArgs e)
        {
            NewPakingShow(PotSpTBox.TextBox.Text.ToString().Trim(), CktSpTBox.TextBox.Text.ToString().Trim(), SetCodetSpTBox.TextBox.Text.ToString().Trim());
        }
        public void NewPakingShow(string Order_No_, string Warehouse_, string Set_Code_)
        {
            try
            {
                this.YYKMarkFinishdgv.DataSource = null;
                string sqlselect = "SELECT ROW_NUMBER() OVER (ORDER BY Entrytime,DO_No) AS 序号, guid,Order_No,DO_No,Warehouse,Color_Code,Color,Set_Code,SKU_Code,Quantity,finishmarknumber AS '已完成数量', Qty_per_Set,Picking_Unit,Entrytime FROM dbo.YOUYIKUDO  WHERE  Order_No LIKE '%' + '" + Order_No_ + "' + '%' AND Warehouse LIKE '%' + '" + Warehouse_ + "' + '%' AND Set_Code LIKE '%' + '" + Set_Code_ + "' + '%' and ISNULL(ENDPO,0)=1 ";

                DataTable dt =CsFormShow.GoSqlSelect(sqlselect);
                if (dt.Rows.Count>0)
                {
                    //ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                    this.YYKMarkFinishdgv.DataSource =dt;
                    //this.FinishdGV.Columns["当前选择"].ReadOnly = true;
                    this.YYKMarkFinishdgv.Columns["guid"].Visible = false;

                    for (int i = 0; i < this.YYKMarkFinishdgv.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.YYKMarkFinishdgv.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }

        }
        public void NewPakingShow()
        {
            try
            {
                this.YYKMarkFinishdgv.DataSource = null;
                string sqlselect = "SELECT ROW_NUMBER() OVER (ORDER BY Entrytime,Order_No) AS 序号, guid,Order_No,DO_No,Warehouse,Color_Code,Color,Set_Code,SKU_Code,Quantity,finishmarknumber AS '已完成数量', Qty_per_Set,Picking_Unit,Entrytime FROM dbo.YOUYIKUDO  WHERE ISNULL(ENDPO,0)=1";

                DataTable dt =CsFormShow.GoSqlSelect(sqlselect);
                if (dt.Rows.Count>0)
                {
                    //ds1.Tables[0].DefaultView.Sort = "从 ASC";//升序排列
                    //DataTable dt1 = ds1.Tables[0].DefaultView.ToTable();//返回一个新的DataTable
                    //ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                    this.YYKMarkFinishdgv.DataSource = dt;
                    // PackingdGV.Columns["当前选择"].ReadOnly = true;
                    this.YYKMarkFinishdgv.Columns["guid"].Visible = false;

                    for (int i = 0; i < this.YYKMarkFinishdgv.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.YYKMarkFinishdgv.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        private void YYKFinishMarkForm_Load(object sender, EventArgs e)
        {

        }

        private void 取消结束订单ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                if (YYKMarkFinishdgv.Rows.Count > 0 && YYKMarkFinishdgv.SelectedRows.Count > 0)
                {

                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(YYKMarkFinishdgv, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        string selectsql = string.Format("UPDATE dbo.YOUYIKUDO SET ENDPO = 0 WHERE Order_No = '{0}'  AND Set_Code = '{1}'AND Warehouse = '{2}'", YYKMarkFinishdgv.Rows[index_v].Cells["Order_No"].Value.ToString().Trim(), YYKMarkFinishdgv.Rows[index_v].Cells["Set_Code"].Value.ToString().Trim(), YYKMarkFinishdgv.Rows[index_v].Cells["Warehouse"].Value.ToString().Trim());
                        content1.Select_nothing(selectsql);
                    }
                }
                else
                {
                    MessageBox.Show("提示：请选择有效订单");
                    return;
                }
                NewPakingShow();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 删除数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //if (stcUserData.M_username != "admin")
                //{
                //    MessageBox.Show("提示：非admin管理员不能进行此操作");
                //    return;
                //}
                Delet_click.Delete_TSMenuItem_Click(this, this.YYKMarkFinishdgv, "dbo.YOUYIKUDO");
                NewPakingShow();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
    }
}
