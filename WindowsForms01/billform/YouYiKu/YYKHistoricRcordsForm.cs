﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class YYKHistoricRcordsForm : Form
    {
        public YYKHistoricRcordsForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.YYKHistoriRcordDGV, 40, 40, true, Color.LightSteelBlue,this);
            NewShow();
        }

        private void YYKHistoricRcordsForm_Load(object sender, EventArgs e)
        {

        }
        public void NewShow()
        {
            try
            {
                YYKHistoriRcordDGV.DataSource = null;
                string sqlselect = string.Format("SELECT a.guid,b.Order_No,b.Warehouse,b.Set_Code,b.Color_Code,b.Quantity , a.po_guid,a.LineUp_guid,a.StartTime AS '开始时间' ,a.EndTime AS '结束时间',Total AS '箱数',Yield AS '已装箱数量' FROM dbo.YYKGUHistoricRcords AS a INNER JOIN (SELECT*FROM dbo.YOUYIKUDO  ) AS b ON a.po_guid=b.guid  ORDER BY a.EndTime DESC");

                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                if (dt.Rows.Count > 0)
                {
                    this.YYKHistoriRcordDGV.DataSource = dt;
                    this.YYKHistoriRcordDGV.Columns["guid"].Visible = false;
                    this.YYKHistoriRcordDGV.Columns["po_guid"].Visible = false;
                    this.YYKHistoriRcordDGV.Columns["LineUp_guid"].Visible = false;
                }
                for (int i = 0; i < this.YYKHistoriRcordDGV.Columns.Count; i++)//防止单击列标题触发排序
                {
                    this.YYKHistoriRcordDGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        public void NewShow(string Order_No_, string Warehouse_, string Set_Code_)
        {
            try
            {
                this.YYKHistoriRcordDGV.DataSource = null;
                string sqlselect = string.Format("SELECT a.guid,b.Order_No,b.Warehouse,b.Set_Code,b.Color_Code,b.Quantity , a.po_guid,a.LineUp_guid,a.StartTime AS '开始时间' ,a.EndTime AS '结束时间',Total AS '订单总箱数',Yield AS '已装箱数量' FROM dbo.YYKGUHistoricRcords AS a LEFT JOIN (SELECT*FROM dbo.YOUYIKUDO  ) AS b ON a.po_guid=b.guid WHERE b.Order_No LIKE '%'+ '{0}'+ '%' AND b.Warehouse LIKE '%'+'{1}'+'%' AND  b.Set_Code LIKE '%'+'{2}'+'%'  ORDER BY a.EndTime DESC", Order_No_, Warehouse_, Set_Code_);

                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                if (dt.Rows.Count > 0)
                {
                    this.YYKHistoriRcordDGV.DataSource = dt;
                    this.YYKHistoriRcordDGV.Columns["guid"].Visible = false;
                    this.YYKHistoriRcordDGV.Columns["po_guid"].Visible = false;
                    this.YYKHistoriRcordDGV.Columns["LineUp_guid"].Visible = false;
                }
                for (int i = 0; i < this.YYKHistoriRcordDGV.Columns.Count; i++)//防止单击列标题触发排序
                {
                    this.YYKHistoriRcordDGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                NewShow(this.PotSpTBox.Text.ToString().Trim(),this.CktSpTBox.Text.ToString().Trim(),this.SetCodetSpTBox.Text.ToString().Trim());
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void 删除数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Delet_click.Delete_TSMenuItem_Click(this, this.YYKHistoriRcordDGV, "dbo.YYKGUHistoricRcords");
                NewShow();
                //NewdataGridView_show();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
    }
}
