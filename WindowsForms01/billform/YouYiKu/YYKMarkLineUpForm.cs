﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class YYKMarkLineUpForm : Form
    {
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        public YYKMarkLineUpForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.YYKLinedGV, 40, 40, true, Color.LightSteelBlue,this);
            NewformShow();
        }

        public void NewformShow()
        {
            try
            {
                this.YYKLinedGV.DataSource = null;
                string sqlselect = string.Format("SELECT ROW_NUMBER() OVER (ORDER BY ID,Entrytime) AS 序号,STOP as 暂停,guid,YK_guid,Order_No,Color_Code,Warehouse,Set_Code,Order_Qty,CurrentPackingNum AS '当前装箱数量',FinishPackingNum AS '已完成装箱数量',BatchNo AS '卷号',Entrytime FROM dbo.YYKMarkingLineUp  where YK_guid IN(SELECT guid FROM dbo.YOUYIKUDO ) and ISNULL(ENDPO,0)<>1  AND  DeviceName='{0}'", CSReadPlc.DevName);

                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                //ds1.Tables[0].DefaultView.Sort = "从 ASC";//升序排列
                //DataTable dt1 = ds1.Tables[0].DefaultView.ToTable();//返回一个新的DataTable
                //ds1.Tables[0].Columns.Add("当前选择", typeof(bool)).SetOrdinal(1);
                if (dt.Rows.Count>0)
                {
                    dt.Columns.Add("当前选择", typeof(bool)).SetOrdinal(0);
                    this.YYKLinedGV.DataSource =dt;
                    // PackingdGV.Columns["当前选择"].ReadOnly = true;
                    this.YYKLinedGV.Columns["guid"].Visible = false;
                    this.YYKLinedGV.Columns["YK_guid"].Visible = false;
                    this.YYKLinedGV.Columns["当前选择"].Visible = false;
                    for (int i = 0; i < this.YYKLinedGV.Columns.Count; i++)//防止单击列标题触发排序
                    {
                        this.YYKLinedGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }

        }

        private void YYKMarkLineUpForm_Load(object sender, EventArgs e)
        {

        }


        public void UpdateYYKPakingData()
        {
            try
            {
                DataSet ds = new DataSet();
                string selectsql = "SELECT Quantity, Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '1' THEN '不喷色号' ELSE '喷色号' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向',CASE WHEN  IsRepeatCheck = '1' THEN '是' ELSE '否' END AS '是否再检针' FROM dbo.YOUYIKUDO, dbo.YYKGUSchedulingTable  WHERE potype  LIKE '%UNIQLO%' and dbo.YOUYIKUDO.guid = dbo.YYKGUSchedulingTable.po_guid AND dbo.YOUYIKUDO.guid IN (SELECT YK_guid FROM (SELECT TOP 1 ROW_NUMBER()OVER (ORDER BY Entrytime DESC) AS 序号,* FROM dbo.YYKMarkingLineUp WHERE  ISNULL(STOP,0)<>1 AND ISNULL(ENDPO,0)<>1 ) AS a)";

                ds = content1.Select_nothing(selectsql);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    MessageBox.Show("当前订单没有切换的订单数据");
                }
                else
                {
                    CsPakingData.M_DicMarking_colorcode.Clear();
                    CsPakingData.M_DicMarking_colorcode["Long"] = ds.Tables[0].Rows[0]["箱子长"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["Width"] = ds.Tables[0].Rows[0]["箱子宽"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["Heghit"] = ds.Tables[0].Rows[0]["箱子高"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["Updown_distance"] = ds.Tables[0].Rows[0]["距离底部"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["Leftright_distance"] = ds.Tables[0].Rows[0]["距离左边"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["CartonWeight"] = ds.Tables[0].Rows[0]["重量"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["GoodWeight"] = ds.Tables[0].Rows[0]["产品重量"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["ColumnNunber"] = ds.Tables[0].Rows[0]["列数"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["行数"] = ds.Tables[0].Rows[0]["行数"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["Layernum"] = ds.Tables[0].Rows[0]["箱内件数"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["Thick"] = ds.Tables[0].Rows[0]["产品厚度"].ToString().Trim();
                    //CsPakingData.M_DicMarking_colorcode["Jianspeed"] = ds.Tables[0].Rows[0]["加减速度"].ToString().Trim();
                    //CsPakingData.M_DicMarking_colorcode["Grabspeed"] = ds.Tables[0].Rows[0]["抓取速度"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["MarkPattern"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["喷码模式"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["CatornType"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["IsRotate"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["是否旋转"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["LabelingType"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["贴标类型"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["PartitionType"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["隔板类型"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["正反方向"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["正反方向"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["IsRepeatCheck"] = CsMarking.GetCatornParameter(ds.Tables[0].Rows[0]["是否再检针"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["Quantity"] = ds.Tables[0].Rows[0]["Quantity"].ToString().Trim();
                    if (ds.Tables[0].Rows[0]["potype"].ToString().Trim().Contains("UNIQLO"))
                    {
                        CsPakingData.M_DicMarking_colorcode["PoTypeSignal"] = "1";
                    }
                    else
                    {
                        CsPakingData.M_DicMarking_colorcode["PoTypeSignal"] = "2";
                    }
                    if (CsPakingData.M_DicMarking_colorcode.Count > 0)
                    {
                        CsMarking.M_MarkParameter = "ColorCode";
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }

        private void 删除ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Delet_click.Delete_TSMenuItem_Click(this, this.YYKLinedGV, "dbo.YYKMarkingLineUp");
                //刷新喷码队列表
                CsPakingData.M_list_Dic_colorcode.Clear();//先清除再重新加载
                string sqlselect = "";
                DataSet ds = new DataSet();
                ds = content1.Select_nothing("SELECT ROW_NUMBER() OVER (ORDER BY Entrytime,Order_No) AS 序号,*FROM dbo.YYKMarkingLineUp WHERE  YK_guid IN(SELECT guid FROM dbo.YOUYIKUDO ");

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    CsGetData.SetToYYKMarking(ds.Tables[0].Rows[i]);
                }
                NewformShow();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
    }
}
