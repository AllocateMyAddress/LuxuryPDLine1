﻿namespace WindowsForms01
{
    partial class YYKMarkLineUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.YYKLinedGV = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.暂停tSpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.重新开始tSpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.YYKLinedGV)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // YYKLinedGV
            // 
            this.YYKLinedGV.BackgroundColor = System.Drawing.SystemColors.ButtonShadow;
            this.YYKLinedGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.YYKLinedGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.YYKLinedGV.Location = new System.Drawing.Point(0, 0);
            this.YYKLinedGV.Name = "YYKLinedGV";
            this.YYKLinedGV.RowTemplate.Height = 27;
            this.YYKLinedGV.Size = new System.Drawing.Size(800, 450);
            this.YYKLinedGV.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.暂停tSpMenuItem,
            this.重新开始tSpMenuItem,
            this.删除ToolStripMenuItem,
            this.删除ToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(139, 100);
            // 
            // 暂停tSpMenuItem
            // 
            this.暂停tSpMenuItem.Name = "暂停tSpMenuItem";
            this.暂停tSpMenuItem.Size = new System.Drawing.Size(138, 24);
            this.暂停tSpMenuItem.Text = "暂停";
            this.暂停tSpMenuItem.Visible = false;
            // 
            // 重新开始tSpMenuItem
            // 
            this.重新开始tSpMenuItem.Name = "重新开始tSpMenuItem";
            this.重新开始tSpMenuItem.Size = new System.Drawing.Size(138, 24);
            this.重新开始tSpMenuItem.Text = "重新开始";
            this.重新开始tSpMenuItem.Visible = false;
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.删除ToolStripMenuItem.Text = "终止";
            this.删除ToolStripMenuItem.Visible = false;
            // 
            // 删除ToolStripMenuItem1
            // 
            this.删除ToolStripMenuItem1.Name = "删除ToolStripMenuItem1";
            this.删除ToolStripMenuItem1.Size = new System.Drawing.Size(138, 24);
            this.删除ToolStripMenuItem1.Text = "删除";
            this.删除ToolStripMenuItem1.Visible = false;
            this.删除ToolStripMenuItem1.Click += new System.EventHandler(this.删除ToolStripMenuItem1_Click);
            // 
            // YYKMarkLineUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.YYKLinedGV);
            this.Name = "YYKMarkLineUpForm";
            this.Text = "优衣库/GU订单队列表";
            this.Load += new System.EventHandler(this.YYKMarkLineUpForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.YYKLinedGV)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView YYKLinedGV;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 暂停tSpMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 重新开始tSpMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem1;
    }
}