﻿namespace WindowsForms01
{
    partial class MarkPositionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MarkPositionForm));
            this.MarkPositiondGV = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.设置为当前toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.修改信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.删除信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.批量设置同单号装箱信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.inserttSpBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.LongtSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.widthtSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.HeighttSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.SelecttSpBtn = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.MarkPositiondGV)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MarkPositiondGV
            // 
            this.MarkPositiondGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MarkPositiondGV.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.MarkPositiondGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MarkPositiondGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MarkPositiondGV.ContextMenuStrip = this.contextMenuStrip1;
            this.MarkPositiondGV.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.MarkPositiondGV.Location = new System.Drawing.Point(1, 61);
            this.MarkPositiondGV.Name = "MarkPositiondGV";
            this.MarkPositiondGV.RowTemplate.Height = 27;
            this.MarkPositiondGV.Size = new System.Drawing.Size(834, 356);
            this.MarkPositiondGV.TabIndex = 13;
            this.MarkPositiondGV.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.MarkPositiondGV_MouseDoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.设置为当前toolStripMenuItem2,
            this.toolStripSeparator2,
            this.修改信息ToolStripMenuItem,
            this.toolStripMenuItem1,
            this.删除信息ToolStripMenuItem,
            this.toolStripMenuItem2,
            this.批量设置同单号装箱信息ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(244, 118);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // 设置为当前toolStripMenuItem2
            // 
            this.设置为当前toolStripMenuItem2.Name = "设置为当前toolStripMenuItem2";
            this.设置为当前toolStripMenuItem2.Size = new System.Drawing.Size(243, 24);
            this.设置为当前toolStripMenuItem2.Text = "设置为当前";
            this.设置为当前toolStripMenuItem2.Click += new System.EventHandler(this.设置为当前toolStripMenuItem2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(240, 6);
            this.toolStripSeparator2.Click += new System.EventHandler(this.toolStripSeparator2_Click);
            // 
            // 修改信息ToolStripMenuItem
            // 
            this.修改信息ToolStripMenuItem.Name = "修改信息ToolStripMenuItem";
            this.修改信息ToolStripMenuItem.Size = new System.Drawing.Size(243, 24);
            this.修改信息ToolStripMenuItem.Text = "修改信息";
            this.修改信息ToolStripMenuItem.Click += new System.EventHandler(this.修改信息ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(240, 6);
            // 
            // 删除信息ToolStripMenuItem
            // 
            this.删除信息ToolStripMenuItem.Name = "删除信息ToolStripMenuItem";
            this.删除信息ToolStripMenuItem.Size = new System.Drawing.Size(243, 24);
            this.删除信息ToolStripMenuItem.Text = "删除信息";
            this.删除信息ToolStripMenuItem.Click += new System.EventHandler(this.删除信息ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(240, 6);
            // 
            // 批量设置同单号装箱信息ToolStripMenuItem
            // 
            this.批量设置同单号装箱信息ToolStripMenuItem.Enabled = false;
            this.批量设置同单号装箱信息ToolStripMenuItem.Name = "批量设置同单号装箱信息ToolStripMenuItem";
            this.批量设置同单号装箱信息ToolStripMenuItem.Size = new System.Drawing.Size(243, 24);
            this.批量设置同单号装箱信息ToolStripMenuItem.Text = "批量设置同单号装箱信息";
            this.批量设置同单号装箱信息ToolStripMenuItem.Click += new System.EventHandler(this.批量设置同单号装箱信息ToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.LightGray;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inserttSpBtn,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.LongtSpTBox,
            this.toolStripLabel2,
            this.widthtSpTBox,
            this.toolStripLabel3,
            this.HeighttSpTBox,
            this.SelecttSpBtn});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(835, 58);
            this.toolStrip1.TabIndex = 15;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // inserttSpBtn
            // 
            this.inserttSpBtn.BackColor = System.Drawing.Color.LightGray;
            this.inserttSpBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.inserttSpBtn.Image = ((System.Drawing.Image)(resources.GetObject("inserttSpBtn.Image")));
            this.inserttSpBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.inserttSpBtn.Name = "inserttSpBtn";
            this.inserttSpBtn.Size = new System.Drawing.Size(73, 55);
            this.inserttSpBtn.Text = "新增信息";
            this.inserttSpBtn.Click += new System.EventHandler(this.inserttSpBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 58);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.AutoSize = false;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(80, 55);
            this.toolStripLabel1.Text = "箱子长";
            // 
            // LongtSpTBox
            // 
            this.LongtSpTBox.AutoSize = false;
            this.LongtSpTBox.Name = "LongtSpTBox";
            this.LongtSpTBox.Size = new System.Drawing.Size(120, 58);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.AutoSize = false;
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(80, 55);
            this.toolStripLabel2.Text = "箱子宽";
            // 
            // widthtSpTBox
            // 
            this.widthtSpTBox.AutoSize = false;
            this.widthtSpTBox.Name = "widthtSpTBox";
            this.widthtSpTBox.Size = new System.Drawing.Size(120, 58);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.AutoSize = false;
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(80, 55);
            this.toolStripLabel3.Text = "箱子高";
            // 
            // HeighttSpTBox
            // 
            this.HeighttSpTBox.AutoSize = false;
            this.HeighttSpTBox.Name = "HeighttSpTBox";
            this.HeighttSpTBox.Size = new System.Drawing.Size(120, 58);
            // 
            // SelecttSpBtn
            // 
            this.SelecttSpBtn.AutoSize = false;
            this.SelecttSpBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.SelecttSpBtn.Image = ((System.Drawing.Image)(resources.GetObject("SelecttSpBtn.Image")));
            this.SelecttSpBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SelecttSpBtn.Name = "SelecttSpBtn";
            this.SelecttSpBtn.Size = new System.Drawing.Size(100, 55);
            this.SelecttSpBtn.Text = "查询";
            this.SelecttSpBtn.Click += new System.EventHandler(this.SelecttSpBtn_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.BackColor = System.Drawing.Color.LightGray;
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 420);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(835, 37);
            this.statusStrip1.TabIndex = 16;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(71, 32);
            this.toolStripStatusLabel1.Text = "单位:mm";
            // 
            // MarkPositionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 457);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.MarkPositiondGV);
            this.Name = "MarkPositionForm";
            this.Text = "装箱信息列表";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MarkPositionForm_FormClosed);
            this.Load += new System.EventHandler(this.MarkPositionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MarkPositiondGV)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView MarkPositiondGV;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton inserttSpBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox LongtSpTBox;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox widthtSpTBox;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripTextBox HeighttSpTBox;
        private System.Windows.Forms.ToolStripButton SelecttSpBtn;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 修改信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 删除信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 设置为当前toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 批量设置同单号装箱信息ToolStripMenuItem;
    }
}