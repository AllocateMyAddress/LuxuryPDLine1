﻿namespace WindowsForms01
{
    partial class BatchImportYYKGUForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okbtn = new System.Windows.Forms.Button();
            this.routetBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LoadRoutebtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(425, 116);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(138, 46);
            this.okbtn.TabIndex = 7;
            this.okbtn.Text = "批量导入";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.okbtn_Click);
            // 
            // routetBox
            // 
            this.routetBox.Location = new System.Drawing.Point(107, 24);
            this.routetBox.Multiline = true;
            this.routetBox.Name = "routetBox";
            this.routetBox.Size = new System.Drawing.Size(770, 35);
            this.routetBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "文件夹路径:";
            // 
            // LoadRoutebtn
            // 
            this.LoadRoutebtn.Location = new System.Drawing.Point(905, 24);
            this.LoadRoutebtn.Name = "LoadRoutebtn";
            this.LoadRoutebtn.Size = new System.Drawing.Size(131, 35);
            this.LoadRoutebtn.TabIndex = 4;
            this.LoadRoutebtn.Text = "选择文件路径";
            this.LoadRoutebtn.UseVisualStyleBackColor = true;
            this.LoadRoutebtn.Click += new System.EventHandler(this.LoadRoutebtn_Click);
            // 
            // BatchImportYYKGUForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 187);
            this.Controls.Add(this.okbtn);
            this.Controls.Add(this.routetBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LoadRoutebtn);
            this.Name = "BatchImportYYKGUForm";
            this.Text = "批量导入";
            this.Load += new System.EventHandler(this.BatchImportYYKGUForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okbtn;
        private System.Windows.Forms.TextBox routetBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button LoadRoutebtn;
    }
}