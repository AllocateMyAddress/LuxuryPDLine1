﻿namespace WindowsForms01
{
    partial class PositionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label7 = new System.Windows.Forms.Label();
            this.Savebtn = new System.Windows.Forms.Button();
            this.idtBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.GoodWeighttBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.qtytBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.JianspeedtBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.GrabspeedtBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ThicktBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.LayernumtBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.ColumntBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.choosebtn = new System.Windows.Forms.Button();
            this.IsRotatetBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.PartitionTypecBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.LabelingTypecbox = new System.Windows.Forms.ComboBox();
            this.CatornTypecBox = new System.Windows.Forms.ComboBox();
            this.MarkPatterncBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.weighttBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lefttBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.toptBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.heighttBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.widthtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.longtBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LinetBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.OrientationcBox = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 15);
            this.label7.TabIndex = 25;
            this.label7.Text = "单位：mm";
            // 
            // Savebtn
            // 
            this.Savebtn.Location = new System.Drawing.Point(290, 170);
            this.Savebtn.Name = "Savebtn";
            this.Savebtn.Size = new System.Drawing.Size(112, 54);
            this.Savebtn.TabIndex = 26;
            this.Savebtn.Text = "保存";
            this.Savebtn.UseVisualStyleBackColor = true;
            this.Savebtn.Click += new System.EventHandler(this.Savebtn_Click);
            // 
            // idtBox
            // 
            this.idtBox.Location = new System.Drawing.Point(560, 398);
            this.idtBox.Name = "idtBox";
            this.idtBox.Size = new System.Drawing.Size(148, 25);
            this.idtBox.TabIndex = 27;
            this.idtBox.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(502, 401);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 15);
            this.label8.TabIndex = 28;
            this.label8.Text = "ID";
            this.label8.Visible = false;
            // 
            // GoodWeighttBox
            // 
            this.GoodWeighttBox.Location = new System.Drawing.Point(324, 323);
            this.GoodWeighttBox.Name = "GoodWeighttBox";
            this.GoodWeighttBox.Size = new System.Drawing.Size(148, 25);
            this.GoodWeighttBox.TabIndex = 132;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(249, 326);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 15);
            this.label20.TabIndex = 131;
            this.label20.Text = "产品重量";
            // 
            // qtytBox
            // 
            this.qtytBox.Location = new System.Drawing.Point(321, 364);
            this.qtytBox.Name = "qtytBox";
            this.qtytBox.Size = new System.Drawing.Size(148, 25);
            this.qtytBox.TabIndex = 130;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(260, 367);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 15);
            this.label19.TabIndex = 129;
            this.label19.Text = "箱数";
            // 
            // JianspeedtBox
            // 
            this.JianspeedtBox.Location = new System.Drawing.Point(318, 431);
            this.JianspeedtBox.Name = "JianspeedtBox";
            this.JianspeedtBox.Size = new System.Drawing.Size(148, 25);
            this.JianspeedtBox.TabIndex = 128;
            this.JianspeedtBox.TextChanged += new System.EventHandler(this.JianspeedtBox_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(250, 434);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 15);
            this.label15.TabIndex = 127;
            this.label15.Text = "加减速度";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // GrabspeedtBox
            // 
            this.GrabspeedtBox.Location = new System.Drawing.Point(560, 434);
            this.GrabspeedtBox.Name = "GrabspeedtBox";
            this.GrabspeedtBox.Size = new System.Drawing.Size(148, 25);
            this.GrabspeedtBox.TabIndex = 126;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(489, 437);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 15);
            this.label16.TabIndex = 125;
            this.label16.Text = "抓取速度";
            // 
            // ThicktBox
            // 
            this.ThicktBox.Location = new System.Drawing.Point(90, 364);
            this.ThicktBox.Name = "ThicktBox";
            this.ThicktBox.Size = new System.Drawing.Size(148, 25);
            this.ThicktBox.TabIndex = 124;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(21, 367);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 15);
            this.label17.TabIndex = 123;
            this.label17.Text = "产品厚";
            // 
            // LayernumtBox
            // 
            this.LayernumtBox.Location = new System.Drawing.Point(324, 278);
            this.LayernumtBox.Name = "LayernumtBox";
            this.LayernumtBox.Size = new System.Drawing.Size(148, 25);
            this.LayernumtBox.TabIndex = 122;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(249, 283);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 15);
            this.label18.TabIndex = 121;
            this.label18.Text = "箱内件数";
            // 
            // ColumntBox
            // 
            this.ColumntBox.Location = new System.Drawing.Point(270, 247);
            this.ColumntBox.Name = "ColumntBox";
            this.ColumntBox.Size = new System.Drawing.Size(146, 25);
            this.ColumntBox.TabIndex = 120;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(194, 250);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 15);
            this.label13.TabIndex = 119;
            this.label13.Text = "摆货列数";
            // 
            // choosebtn
            // 
            this.choosebtn.Location = new System.Drawing.Point(654, 241);
            this.choosebtn.Name = "choosebtn";
            this.choosebtn.Size = new System.Drawing.Size(145, 32);
            this.choosebtn.TabIndex = 118;
            this.choosebtn.Text = "选列数和旋转方式";
            this.choosebtn.UseVisualStyleBackColor = true;
            this.choosebtn.Click += new System.EventHandler(this.choosebtn_Click_1);
            // 
            // IsRotatetBox
            // 
            this.IsRotatetBox.Location = new System.Drawing.Point(492, 247);
            this.IsRotatetBox.Name = "IsRotatetBox";
            this.IsRotatetBox.Size = new System.Drawing.Size(148, 25);
            this.IsRotatetBox.TabIndex = 117;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(420, 250);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 15);
            this.label14.TabIndex = 116;
            this.label14.Text = "纸箱旋转";
            // 
            // PartitionTypecBox
            // 
            this.PartitionTypecBox.FormattingEnabled = true;
            this.PartitionTypecBox.Location = new System.Drawing.Point(552, 323);
            this.PartitionTypecBox.Name = "PartitionTypecBox";
            this.PartitionTypecBox.Size = new System.Drawing.Size(148, 23);
            this.PartitionTypecBox.TabIndex = 115;
            this.PartitionTypecBox.SelectedIndexChanged += new System.EventHandler(this.PartitionTypecBox_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(483, 326);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 15);
            this.label12.TabIndex = 114;
            this.label12.Text = "隔板选择";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // LabelingTypecbox
            // 
            this.LabelingTypecbox.FormattingEnabled = true;
            this.LabelingTypecbox.Location = new System.Drawing.Point(90, 323);
            this.LabelingTypecbox.Name = "LabelingTypecbox";
            this.LabelingTypecbox.Size = new System.Drawing.Size(148, 23);
            this.LabelingTypecbox.TabIndex = 113;
            // 
            // CatornTypecBox
            // 
            this.CatornTypecBox.FormattingEnabled = true;
            this.CatornTypecBox.Location = new System.Drawing.Point(90, 399);
            this.CatornTypecBox.Name = "CatornTypecBox";
            this.CatornTypecBox.Size = new System.Drawing.Size(148, 23);
            this.CatornTypecBox.TabIndex = 112;
            // 
            // MarkPatterncBox
            // 
            this.MarkPatterncBox.FormattingEnabled = true;
            this.MarkPatterncBox.Location = new System.Drawing.Point(588, 359);
            this.MarkPatterncBox.Name = "MarkPatterncBox";
            this.MarkPatterncBox.Size = new System.Drawing.Size(148, 23);
            this.MarkPatterncBox.TabIndex = 111;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 326);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 110;
            this.label9.Text = "贴标选择";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 402);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 15);
            this.label10.TabIndex = 109;
            this.label10.Text = "纸箱大小";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(515, 365);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 15);
            this.label11.TabIndex = 108;
            this.label11.Text = "喷码模式";
            // 
            // weighttBox
            // 
            this.weighttBox.Location = new System.Drawing.Point(552, 278);
            this.weighttBox.Name = "weighttBox";
            this.weighttBox.Size = new System.Drawing.Size(148, 25);
            this.weighttBox.TabIndex = 107;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(477, 281);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 106;
            this.label4.Text = "纸箱重量";
            // 
            // lefttBox
            // 
            this.lefttBox.Location = new System.Drawing.Point(324, 73);
            this.lefttBox.Name = "lefttBox";
            this.lefttBox.Size = new System.Drawing.Size(148, 25);
            this.lefttBox.TabIndex = 105;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(251, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 104;
            this.label5.Text = "距离左边";
            // 
            // toptBox
            // 
            this.toptBox.Location = new System.Drawing.Point(99, 73);
            this.toptBox.Name = "toptBox";
            this.toptBox.Size = new System.Drawing.Size(148, 25);
            this.toptBox.TabIndex = 103;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 15);
            this.label6.TabIndex = 102;
            this.label6.Text = "距离底部";
            // 
            // heighttBox
            // 
            this.heighttBox.Location = new System.Drawing.Point(552, 23);
            this.heighttBox.Name = "heighttBox";
            this.heighttBox.Size = new System.Drawing.Size(148, 25);
            this.heighttBox.TabIndex = 101;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(477, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 15);
            this.label3.TabIndex = 100;
            this.label3.Text = "箱子高";
            // 
            // widthtBox
            // 
            this.widthtBox.Location = new System.Drawing.Point(324, 23);
            this.widthtBox.Name = "widthtBox";
            this.widthtBox.Size = new System.Drawing.Size(148, 25);
            this.widthtBox.TabIndex = 99;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(252, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 15);
            this.label2.TabIndex = 98;
            this.label2.Text = "箱子宽";
            // 
            // longtBox
            // 
            this.longtBox.Location = new System.Drawing.Point(99, 23);
            this.longtBox.Name = "longtBox";
            this.longtBox.Size = new System.Drawing.Size(148, 25);
            this.longtBox.TabIndex = 97;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 96;
            this.label1.Text = "箱子长";
            // 
            // LinetBox
            // 
            this.LinetBox.Location = new System.Drawing.Point(90, 278);
            this.LinetBox.Name = "LinetBox";
            this.LinetBox.Size = new System.Drawing.Size(148, 25);
            this.LinetBox.TabIndex = 136;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(20, 281);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 15);
            this.label22.TabIndex = 135;
            this.label22.Text = "摆货行数";
            // 
            // OrientationcBox
            // 
            this.OrientationcBox.FormattingEnabled = true;
            this.OrientationcBox.Location = new System.Drawing.Point(90, 428);
            this.OrientationcBox.Name = "OrientationcBox";
            this.OrientationcBox.Size = new System.Drawing.Size(148, 23);
            this.OrientationcBox.TabIndex = 138;
            this.OrientationcBox.SelectedIndexChanged += new System.EventHandler(this.OrientationcBox_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(17, 434);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(67, 15);
            this.label23.TabIndex = 137;
            this.label23.Text = "正反方向";
            this.label23.Click += new System.EventHandler(this.label23_Click);
            // 
            // PositionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 236);
            this.Controls.Add(this.OrientationcBox);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.LinetBox);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.GoodWeighttBox);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.qtytBox);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.JianspeedtBox);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.GrabspeedtBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.ThicktBox);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.LayernumtBox);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.ColumntBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.choosebtn);
            this.Controls.Add(this.IsRotatetBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.PartitionTypecBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.LabelingTypecbox);
            this.Controls.Add(this.CatornTypecBox);
            this.Controls.Add(this.MarkPatterncBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.weighttBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lefttBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.toptBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.heighttBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.widthtBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.longtBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.idtBox);
            this.Controls.Add(this.Savebtn);
            this.Controls.Add(this.label7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "PositionForm";
            this.Text = "纸箱信息输入";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PositionForm_FormClosed);
            this.Load += new System.EventHandler(this.PositionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button Savebtn;
        private System.Windows.Forms.TextBox idtBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox GoodWeighttBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox qtytBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox JianspeedtBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox GrabspeedtBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox ThicktBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox LayernumtBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox ColumntBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button choosebtn;
        private System.Windows.Forms.TextBox IsRotatetBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox PartitionTypecBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox LabelingTypecbox;
        private System.Windows.Forms.ComboBox CatornTypecBox;
        private System.Windows.Forms.ComboBox MarkPatterncBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox weighttBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox lefttBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox toptBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox heighttBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox widthtBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox longtBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LinetBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox OrientationcBox;
        private System.Windows.Forms.Label label23;
    }
}