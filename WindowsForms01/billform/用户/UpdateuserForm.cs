﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Security;  //安全加密

namespace WindowsForms01
{
    public partial class UpdateuserForm : Form
    {
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        public userForm userform = null;
        string oldpwd_va = "";
        public UpdateuserForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
        }
        /// <summary>
        /// 窗体接收值重载
        /// </summary>
        /// <param name="ID">用户ID</param>
        /// <param name="username_v">用户名</param>
        /// <param name="empname_v">员工姓名</param>
        /// <param name="oldpwd_v">原始密码</param>
        public UpdateuserForm(string ID, string username_v, string usercode, string oldpwd_v)
        {
            InitializeComponent();
            OldpwdTBOX.Text = "";
            NewpwdTBOX.Text = "";
            SurepwdTBOX.Text = "";
            this.useridtextBox.Text = ID.ToString();
            this.codeTBOX.Text = usercode;
            this.nameTBOX.Text = username_v;
            oldpwd_va = oldpwd_v;
            codeTBOX.ReadOnly=true;
            nameTBOX.ReadOnly = true;
            useridtextBox.ReadOnly = true;
            this.MaximizeBox = false;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        #region 修改确定按钮触发事件
        private void okbtt_Click(object sender, EventArgs e)
        {
            this.Focus();

            if (this.Checkin() && Checkpwd())
            {
                //获取用户输入的密码
                string password = this.NewpwdTBOX.Text;
                //对密码进行加密
                string pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "md5");
                //创建SQL语句,将加密后的密码保存到数据库
                string insCmd = string.Format("update UserInfo set password='{0}' where ucode='{1}'", pwd, codeTBOX.Text.Trim());
                DataSet ds_save = content1.Select_nothing(insCmd);
                MessageBox.Show("修改成功");
                userform = (userForm)this.Owner;
                userform.NewdataGridView_show();
                this.Close();
                this.Dispose();
            }

        }
        #endregion

        /// <summary>
        /// 检查输入是否为空的判断方法
        /// </summary>
        /// <returns></returns>
        private bool Checkin()
        {
            if (string.IsNullOrEmpty(this.codeTBOX.Text))
            {
                MessageBox.Show("用户编号不能为空！");
                this.codeTBOX.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(this.nameTBOX.Text))
            {
                MessageBox.Show("员工姓名不能为空！");
                this.nameTBOX.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(this.OldpwdTBOX.Text))
            {
                MessageBox.Show("原始密码不能为空！");
                this.OldpwdTBOX.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(this.NewpwdTBOX.Text))
            {
                MessageBox.Show("请输入新密码！");
                this.NewpwdTBOX.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(this.SurepwdTBOX.Text))
            {
                MessageBox.Show("请输入确认密码！");
                this.SurepwdTBOX.Focus();
                return false;
            }

            if (this.NewpwdTBOX.Text != this.SurepwdTBOX.Text)
            {
                MessageBox.Show("确认密码和设置密码不一致，请重新输入");
                this.NewpwdTBOX.Focus();
                this.SurepwdTBOX.Text = "";
                this.NewpwdTBOX.Text = "";
                return false;
            }

            return true;
        }
        /// <summary>
        /// 检查密码输入是否正确的判断方法
        /// </summary>
        /// <returns></returns>
        private bool Checkpwd()
        {

            DataSet ds_selectpwd = content1.Select_nothing("select password from UserInfo where ucode =" +"'"+ useridtextBox.Text+"'");

            string pwd = ds_selectpwd.Tables[0].Rows[0]["password"].ToString().Trim();
            //string pwd_jiami = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "md5");
            string oldpwd_v = this.OldpwdTBOX.Text;
            string oldpwd_vjiami = FormsAuthentication.HashPasswordForStoringInConfigFile(oldpwd_v, "md5");

            if (oldpwd_vjiami == pwd)
            {
                return true;
            }
            else
            {
                MessageBox.Show("原始密码错误！");
                this.OldpwdTBOX.Focus();
                return false;
            }
        }

        private void UpdateuserForm_Load(object sender, EventArgs e)
        {

        }

        private void UpdateuserForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }
    }
}
