﻿namespace WindowsForms01
{
    partial class UpdateuserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.codeTBOX = new System.Windows.Forms.TextBox();
            this.nameTBOX = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.OldpwdTBOX = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.NewpwdTBOX = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SurepwdTBOX = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.okbtt = new System.Windows.Forms.Button();
            this.useridtextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "用户编号";
            // 
            // codeTBOX
            // 
            this.codeTBOX.Location = new System.Drawing.Point(102, 21);
            this.codeTBOX.Name = "codeTBOX";
            this.codeTBOX.Size = new System.Drawing.Size(146, 25);
            this.codeTBOX.TabIndex = 1;
            // 
            // nameTBOX
            // 
            this.nameTBOX.Location = new System.Drawing.Point(102, 65);
            this.nameTBOX.Name = "nameTBOX";
            this.nameTBOX.Size = new System.Drawing.Size(146, 25);
            this.nameTBOX.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "用 户 名";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // OldpwdTBOX
            // 
            this.OldpwdTBOX.Location = new System.Drawing.Point(102, 109);
            this.OldpwdTBOX.Name = "OldpwdTBOX";
            this.OldpwdTBOX.Size = new System.Drawing.Size(146, 25);
            this.OldpwdTBOX.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "原始密码";
            // 
            // NewpwdTBOX
            // 
            this.NewpwdTBOX.Location = new System.Drawing.Point(102, 153);
            this.NewpwdTBOX.Name = "NewpwdTBOX";
            this.NewpwdTBOX.Size = new System.Drawing.Size(146, 25);
            this.NewpwdTBOX.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "新 密 码";
            // 
            // SurepwdTBOX
            // 
            this.SurepwdTBOX.Location = new System.Drawing.Point(102, 197);
            this.SurepwdTBOX.Name = "SurepwdTBOX";
            this.SurepwdTBOX.Size = new System.Drawing.Size(146, 25);
            this.SurepwdTBOX.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "确认密码";
            // 
            // okbtt
            // 
            this.okbtt.Location = new System.Drawing.Point(125, 245);
            this.okbtt.Name = "okbtt";
            this.okbtt.Size = new System.Drawing.Size(75, 36);
            this.okbtt.TabIndex = 10;
            this.okbtt.Text = "确定";
            this.okbtt.UseVisualStyleBackColor = true;
            this.okbtt.Click += new System.EventHandler(this.okbtt_Click);
            // 
            // useridtextBox
            // 
            this.useridtextBox.Location = new System.Drawing.Point(225, 291);
            this.useridtextBox.Name = "useridtextBox";
            this.useridtextBox.Size = new System.Drawing.Size(57, 25);
            this.useridtextBox.TabIndex = 11;
            this.useridtextBox.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(164, 301);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "id";
            this.label6.Visible = false;
            // 
            // UpdateuserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 325);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.useridtextBox);
            this.Controls.Add(this.okbtt);
            this.Controls.Add(this.SurepwdTBOX);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.NewpwdTBOX);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.OldpwdTBOX);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nameTBOX);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.codeTBOX);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "UpdateuserForm";
            this.Text = "修改用户密码";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.UpdateuserForm_FormClosed);
            this.Load += new System.EventHandler(this.UpdateuserForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox codeTBOX;
        private System.Windows.Forms.TextBox nameTBOX;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox OldpwdTBOX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NewpwdTBOX;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox SurepwdTBOX;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button okbtt;
        private System.Windows.Forms.TextBox useridtextBox;
        private System.Windows.Forms.Label label6;
    }
}