﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class userForm : Form
    {
        A.BLL.newsContent content = new A.BLL.newsContent();
        insertuserForm insertuserform1 = null;
        UpdateuserForm updateform = null;
        public userForm()
        {
            InitializeComponent();
            CsFormShow.SetDvgRowHeight(this.userDGV, 40, 40, true, Color.LightSteelBlue,this);
            NewdataGridView_show();
        }


        public void NewdataGridView_show()//刷新显示数据
        {
            userDGV.DataSource = null;
            //userDGV.AutoGenerateColumns = false;//禁止自动创建列
            string sql_select = "SELECT ROW_NUMBER() OVER(ORDER BY dDate) as 编号,guid,ID, ucode as 用户ID,uName as 用户名,dDate as 录入日期 from dbo.UserInfo";
            DataTable dt = CsFormShow.GoSqlSelect(sql_select);
            if (dt.Rows.Count>0)
            {
                userDGV.DataSource = dt;
                userDGV.Columns["ID"].Visible = false;
                userDGV.Columns["guid"].Visible = false;
            }
        }
        private void userDGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void userForm_Load(object sender, EventArgs e)
        {

        }
        #region // 新增按钮触发事件
        private void insertTSButton_Click(object sender, EventArgs e)
        {
            if (stcUserData.M_username!="admin")
            {
                MessageBox.Show("非管理员不能进行此操作");
                return;
            }
            CsFormShow.FormShowDialog(insertuserform1, this, "WindowsForms01.insertuserForm");
        }
        #endregion
        #region //删除用户按钮触发事件
        private void deleteTSButton_Click(object sender, EventArgs e)
        {
            try
            {
                Delet_click.Delete_bttn_click(this, userDGV);
                NewdataGridView_show();
                //deleteTSButton.Enabled = false;
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        #endregion

        private void selectTSButton_Click(object sender, EventArgs e)
        {
            string empid_v = "";
            string username_v = this.nameTSTextBox.Text;

            if (codeTSTextBox.Text != "")
            {
                empid_v = this.codeTSTextBox.Text;
            }

            string sql_str = "SELECT ROW_NUMBER() OVER(ORDER BY dDate) as 编号,ucode as 用户ID,uName as 用户名,dDate as 录入日期 from dbo.UserInfo where 1=1 ";
            string sql_where = "";
            if (username_v != "")
            {
                sql_where = "and uName=" + "'" + username_v + "'";
            }
            if (empid_v != "")
            {
                sql_where = sql_where + "and ID=" + empid_v;
            }
            sql_str = sql_str + sql_where;
            DataTable dt =CsFormShow.GoSqlSelect(sql_str);
            userDGV.DataSource = null;
            userDGV.DataSource = dt;
            this.userDGV.Enabled = true;
        }

        private void nameTSTextBox_Click(object sender, EventArgs e)
        {

        }

        private void codeTSTextBox_Click(object sender, EventArgs e)
        {

        }
        #region //右键修改触发事件
        private void 修改ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                string username_v = "";
                string usercode = "";
                string oldpwd_v = "";
                string ucode = "";
                if (userDGV.SelectedRows.Count>1|| userDGV.SelectedRows[0].Cells["用户ID"].Value.ToString().Trim() == "")
                {
                    MessageBox.Show("提示：请选择正确的一行数据");
                    return;
                }
                int index = userDGV.CurrentRow.Index;
                username_v = userDGV.Rows[index].Cells[2].Value.ToString().Trim();//获得员工ID
                if (stcUserData.M_username != username_v&& stcUserData.M_username!="admin")
                {
                    MessageBox.Show("提示：非当前用户或管理员不能修改信息");
                    return;
                }

                ucode = userDGV.Rows[index].Cells["用户ID"].Value.ToString().Trim();//获得员工ID
                string sql_select = "select ID,uName,ucode,password,dDate  from UserInfo where ucode =  "+"'"+ ucode + "'" ;
                DataSet ds_selectshow = content.Select_nothing(sql_select);

                if (ds_selectshow.Tables[0].Rows.Count != 0)
                {
                    username_v = ds_selectshow.Tables[0].Rows[0]["uName"].ToString();
                    usercode = ds_selectshow.Tables[0].Rows[0]["ucode"].ToString();
                    oldpwd_v = ds_selectshow.Tables[0].Rows[0]["password"].ToString();

                }

                if (updateform == null || updateform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                {
                    updateform = new UpdateuserForm(ucode, username_v, usercode, oldpwd_v);
                    updateform.Owner = this;
                    updateform.StartPosition = FormStartPosition.CenterScreen;
                    updateform.ShowDialog();
                }
                else
                {
                    updateform.Show();
                    updateform.StartPosition = FormStartPosition.CenterScreen;
                    updateform.BringToFront();
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        #endregion
        #region //右键删除按钮触发事件
        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool isindex_v=false;int index_v = 0;
                CsFormShow.DvgGetcurrentIndex(userDGV, out isindex_v,out index_v);
                if (isindex_v)
                {
                    string uname_v = userDGV.Rows[index_v].Cells["用户名"].Value.ToString().Trim();
                    if (uname_v.Contains("starcity"))
                    {
                        MessageBox.Show("此用户不能被删除");
                        return;
                    }
                }
                if (stcUserData.M_username!="admin")
                {
                    MessageBox.Show("提示：非admin管理员不能进行此操作");
                    return;
                }
                Delet_click.Delete_TSMenuItem_Click(this, userDGV, "dbo.UserInfo");
                //NewdataGridView_show();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }



        #endregion

        private void userForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }
    }
}
