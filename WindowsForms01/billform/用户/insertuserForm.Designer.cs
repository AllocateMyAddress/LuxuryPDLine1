﻿namespace WindowsForms01
{
    partial class insertuserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usercodelabel = new System.Windows.Forms.Label();
            this.usercodetextBox = new System.Windows.Forms.TextBox();
            this.usernametextBox = new System.Windows.Forms.TextBox();
            this.usernamelabel = new System.Windows.Forms.Label();
            this.pwdtextBox = new System.Windows.Forms.TextBox();
            this.pwdlabel = new System.Windows.Forms.Label();
            this.okpwdtextBox = new System.Windows.Forms.TextBox();
            this.okpwdlabel = new System.Windows.Forms.Label();
            this.okuserbtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // usercodelabel
            // 
            this.usercodelabel.AutoSize = true;
            this.usercodelabel.Location = new System.Drawing.Point(43, 29);
            this.usercodelabel.Name = "usercodelabel";
            this.usercodelabel.Size = new System.Drawing.Size(67, 15);
            this.usercodelabel.TabIndex = 0;
            this.usercodelabel.Text = "用户编号";
            // 
            // usercodetextBox
            // 
            this.usercodetextBox.Location = new System.Drawing.Point(116, 26);
            this.usercodetextBox.Name = "usercodetextBox";
            this.usercodetextBox.Size = new System.Drawing.Size(129, 25);
            this.usercodetextBox.TabIndex = 1;
            // 
            // usernametextBox
            // 
            this.usernametextBox.Location = new System.Drawing.Point(116, 72);
            this.usernametextBox.Name = "usernametextBox";
            this.usernametextBox.Size = new System.Drawing.Size(129, 25);
            this.usernametextBox.TabIndex = 3;
            // 
            // usernamelabel
            // 
            this.usernamelabel.AutoSize = true;
            this.usernamelabel.Location = new System.Drawing.Point(43, 75);
            this.usernamelabel.Name = "usernamelabel";
            this.usernamelabel.Size = new System.Drawing.Size(60, 15);
            this.usernamelabel.TabIndex = 2;
            this.usernamelabel.Text = " 用户名";
            // 
            // pwdtextBox
            // 
            this.pwdtextBox.Location = new System.Drawing.Point(116, 118);
            this.pwdtextBox.Name = "pwdtextBox";
            this.pwdtextBox.Size = new System.Drawing.Size(129, 25);
            this.pwdtextBox.TabIndex = 5;
            // 
            // pwdlabel
            // 
            this.pwdlabel.AutoSize = true;
            this.pwdlabel.Location = new System.Drawing.Point(43, 121);
            this.pwdlabel.Name = "pwdlabel";
            this.pwdlabel.Size = new System.Drawing.Size(53, 15);
            this.pwdlabel.TabIndex = 4;
            this.pwdlabel.Text = "  密码";
            // 
            // okpwdtextBox
            // 
            this.okpwdtextBox.Location = new System.Drawing.Point(116, 164);
            this.okpwdtextBox.Name = "okpwdtextBox";
            this.okpwdtextBox.Size = new System.Drawing.Size(129, 25);
            this.okpwdtextBox.TabIndex = 7;
            // 
            // okpwdlabel
            // 
            this.okpwdlabel.AutoSize = true;
            this.okpwdlabel.Location = new System.Drawing.Point(43, 167);
            this.okpwdlabel.Name = "okpwdlabel";
            this.okpwdlabel.Size = new System.Drawing.Size(67, 15);
            this.okpwdlabel.TabIndex = 6;
            this.okpwdlabel.Text = "确认密码";
            // 
            // okuserbtn
            // 
            this.okuserbtn.Location = new System.Drawing.Point(116, 209);
            this.okuserbtn.Name = "okuserbtn";
            this.okuserbtn.Size = new System.Drawing.Size(75, 32);
            this.okuserbtn.TabIndex = 8;
            this.okuserbtn.Text = "确定";
            this.okuserbtn.UseVisualStyleBackColor = true;
            this.okuserbtn.Click += new System.EventHandler(this.okuserbtn_Click);
            // 
            // insertuserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 253);
            this.Controls.Add(this.okuserbtn);
            this.Controls.Add(this.okpwdtextBox);
            this.Controls.Add(this.okpwdlabel);
            this.Controls.Add(this.pwdtextBox);
            this.Controls.Add(this.pwdlabel);
            this.Controls.Add(this.usernametextBox);
            this.Controls.Add(this.usernamelabel);
            this.Controls.Add(this.usercodetextBox);
            this.Controls.Add(this.usercodelabel);
            this.Name = "insertuserForm";
            this.Text = "用户注册";
            this.Load += new System.EventHandler(this.insertuserForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label usercodelabel;
        private System.Windows.Forms.TextBox usercodetextBox;
        private System.Windows.Forms.TextBox usernametextBox;
        private System.Windows.Forms.Label usernamelabel;
        private System.Windows.Forms.TextBox pwdtextBox;
        private System.Windows.Forms.Label pwdlabel;
        private System.Windows.Forms.TextBox okpwdtextBox;
        private System.Windows.Forms.Label okpwdlabel;
        private System.Windows.Forms.Button okuserbtn;
    }
}