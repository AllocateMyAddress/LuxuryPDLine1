﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Security;  //安全加密
using System.Data.SqlClient;

namespace WindowsForms01
{
    public partial class insertuserForm : Form
    {
        A.BLL.newsContent content = new A.BLL.newsContent();
        public userForm userform = null;
        public insertuserForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }
        #region//点击确定按钮触发事件
        private void okuserbtn_Click(object sender, EventArgs e)
        {
            this.Focus();
            List<Control> controls_list = new List<Control>();
            Control[] controls_shuzu = { this.usercodetextBox, this.usernametextBox, this.pwdtextBox, this.okpwdtextBox };
            foreach (Control ctrl in controls_shuzu)
            {
                controls_list.Add(ctrl);
            }
            if (this.checkinuserzc())
            {
                //获取用户输入的密码
                string password = this.pwdtextBox.Text;
                //对密码进行加密
                string pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "md5");
                //创建SQL语句,将加密后的密码保存到数据库
                string insCmd = string.Format("insert into UserInfo (ucode, uName ,password, dDate) values ('{0}','{1}','{2}','{3}')",
                    this.usercodetextBox.Text.Trim(), this.usernametextBox.Text.Trim(), pwd,DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                A.DAL.SqlHelper CONN_sql = new A.DAL.SqlHelper();
                string CONNn_STRsql = CONN_sql.CONN_sql();
                using (SqlConnection CONN = new SqlConnection(CONNn_STRsql))
                {
                    CONN.Open();
                    SqlCommand cmd = new SqlCommand(insCmd, CONN);

                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        this.usercodetextBox.Text = "";
                        this.usernametextBox.Text = "";
                        this.pwdtextBox.Text = "";
                        this.okpwdtextBox.Text = "";
                        MessageBox.Show("恭喜您，注册成功！");
                        userform = (userForm)this.Owner;
                        userform.NewdataGridView_show();//关闭前刷新父窗体显示
                        this.Close();
                        this.Dispose();
                    }
                    else
                    {
                        this.pwdtextBox.Text = "";
                        this.okpwdtextBox.Text = "";
                        MessageBox.Show("对不起，注册失败请重新输入");
                    }
                }

            }
        }
        #endregion

        #region //检查输入是否存在未输入情况
        /// <summary>
        /// 检查用户输入的判断方法
        /// </summary>
        /// <returns></returns>
        private bool checkinuserzc()
        {
            if (string.IsNullOrEmpty(this.usercodetextBox.Text))
            {
                MessageBox.Show("用户编号不能为空！");
                this.usercodetextBox.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(this.usernametextBox.Text))
            {
                MessageBox.Show("用户不能为空！");
                this.usernametextBox.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(this.pwdtextBox.Text))
            {
                MessageBox.Show("密码不能为空！");
                this.pwdtextBox.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(this.okpwdtextBox.Text))
            {
                MessageBox.Show("请输入确认密码！");
                this.okpwdtextBox.Focus();
                return false;
            }
            string sql_selectuser = "select ucode from UserInfo where ucode= " + "'" + usercodetextBox.Text.Trim() + "'";
            string sql_selectname = "select uName from UserInfo where uName= " + "'" + usernametextBox.Text.Trim() + "'";

            DataSet ds_user = content.Select_nothing(sql_selectuser);
            DataSet ds_name = content.Select_nothing(sql_selectname);
            if (ds_user.Tables[0].Rows.Count > 0)
            {
                MessageBox.Show("用户名已经被使用！");
                this.usercodetextBox.Text = "";
                this.usernametextBox.Text = "";
                this.okpwdtextBox.Text = "";
                this.pwdtextBox.Text = "";
                this.pwdtextBox.Focus();
                return false;
            }
            if (ds_name.Tables[0].Rows.Count > 0)
            {
                MessageBox.Show("该员工已经注册！");
                this.usercodetextBox.Text = "";
                this.usernametextBox.Text = "";
                this.okpwdtextBox.Text = "";
                this.pwdtextBox.Text = "";
                this.pwdtextBox.Focus();
                return false;
            }
            if (this.pwdtextBox.Text != this.okpwdtextBox.Text)
            {
                MessageBox.Show("确认密码和设置密码不一致，请重新输入");
                this.pwdtextBox.Focus();
                this.okpwdtextBox.Text = "";
                this.pwdtextBox.Text = "";
                return false;
            }

            return true;
        }
        #endregion

        private void insertuserForm_Load(object sender, EventArgs e)
        {

        }
    }
}
