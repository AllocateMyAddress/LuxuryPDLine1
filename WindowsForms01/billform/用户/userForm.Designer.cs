﻿namespace WindowsForms01
{
    partial class userForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(userForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.insertTSButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.nametSLabel = new System.Windows.Forms.ToolStripLabel();
            this.nameTSTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.codeTSLabel = new System.Windows.Forms.ToolStripLabel();
            this.codeTSTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.selectTSButton = new System.Windows.Forms.ToolStripButton();
            this.userDGV = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.修改ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userDGV)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Silver;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insertTSButton,
            this.toolStripSeparator1,
            this.nametSLabel,
            this.nameTSTextBox,
            this.codeTSLabel,
            this.codeTSTextBox,
            this.selectTSButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 52);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // insertTSButton
            // 
            this.insertTSButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.insertTSButton.Image = ((System.Drawing.Image)(resources.GetObject("insertTSButton.Image")));
            this.insertTSButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.insertTSButton.Name = "insertTSButton";
            this.insertTSButton.Size = new System.Drawing.Size(73, 49);
            this.insertTSButton.Text = "新增用户";
            this.insertTSButton.Click += new System.EventHandler(this.insertTSButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 52);
            // 
            // nametSLabel
            // 
            this.nametSLabel.Name = "nametSLabel";
            this.nametSLabel.Size = new System.Drawing.Size(54, 49);
            this.nametSLabel.Text = "用户名";
            // 
            // nameTSTextBox
            // 
            this.nameTSTextBox.AutoSize = false;
            this.nameTSTextBox.Name = "nameTSTextBox";
            this.nameTSTextBox.Size = new System.Drawing.Size(130, 52);
            this.nameTSTextBox.Click += new System.EventHandler(this.nameTSTextBox_Click);
            // 
            // codeTSLabel
            // 
            this.codeTSLabel.Name = "codeTSLabel";
            this.codeTSLabel.Size = new System.Drawing.Size(69, 49);
            this.codeTSLabel.Text = "用户编号";
            // 
            // codeTSTextBox
            // 
            this.codeTSTextBox.AutoSize = false;
            this.codeTSTextBox.Name = "codeTSTextBox";
            this.codeTSTextBox.Size = new System.Drawing.Size(130, 52);
            this.codeTSTextBox.Click += new System.EventHandler(this.codeTSTextBox_Click);
            // 
            // selectTSButton
            // 
            this.selectTSButton.AutoSize = false;
            this.selectTSButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.selectTSButton.Image = ((System.Drawing.Image)(resources.GetObject("selectTSButton.Image")));
            this.selectTSButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.selectTSButton.Name = "selectTSButton";
            this.selectTSButton.Size = new System.Drawing.Size(80, 49);
            this.selectTSButton.Text = "查询";
            this.selectTSButton.Click += new System.EventHandler(this.selectTSButton_Click);
            // 
            // userDGV
            // 
            this.userDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userDGV.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.userDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userDGV.ContextMenuStrip = this.contextMenuStrip1;
            this.userDGV.Location = new System.Drawing.Point(0, 55);
            this.userDGV.Name = "userDGV";
            this.userDGV.RowTemplate.Height = 27;
            this.userDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.userDGV.Size = new System.Drawing.Size(800, 393);
            this.userDGV.TabIndex = 1;
            this.userDGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.userDGV_CellContentClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.修改ToolStripMenuItem,
            this.删除ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(139, 52);
            // 
            // 修改ToolStripMenuItem
            // 
            this.修改ToolStripMenuItem.Name = "修改ToolStripMenuItem";
            this.修改ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.修改ToolStripMenuItem.Text = "修改密码";
            this.修改ToolStripMenuItem.Click += new System.EventHandler(this.修改ToolStripMenuItem_Click);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.删除ToolStripMenuItem.Text = "删除用户";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // userForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.userDGV);
            this.Controls.Add(this.toolStrip1);
            this.Name = "userForm";
            this.Text = "用户管理";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.userForm_FormClosed);
            this.Load += new System.EventHandler(this.userForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userDGV)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton insertTSButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel nametSLabel;
        private System.Windows.Forms.ToolStripTextBox nameTSTextBox;
        private System.Windows.Forms.ToolStripLabel codeTSLabel;
        private System.Windows.Forms.ToolStripTextBox codeTSTextBox;
        private System.Windows.Forms.ToolStripButton selectTSButton;
        private System.Windows.Forms.DataGridView userDGV;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 修改ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
    }
}