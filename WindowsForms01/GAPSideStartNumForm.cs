﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public partial class GAPSideStartNumForm : Form
    {
        //22
        PackingForm Packingform = null;
        public GAPSideStartNumForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void GAPSideStartNumForm_Load(object sender, EventArgs e)
        {

        }

        private void okbtn_Click(object sender, EventArgs e)
        {
            string str = this.SideSatrtNumtBx.Text.Trim();
            if (string.IsNullOrEmpty(str))
            {
                MessageBox.Show("内容不能为空");
                return;
            }
            else
            {
                Packingform = (PackingForm)this.Owner;
                Packingform.GAPSideStartNum_v = this.SideSatrtNumtBx.Text.Trim();
            }
            this.Close();
            this.Dispose();
        }
    }
}
