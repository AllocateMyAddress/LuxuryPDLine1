﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using Soketprint;
using MCData;
using Image_recognition;
using System.IO;
//using GetData;
using A.BLL;
using System.Data;
using System.Windows.Forms;
namespace WindowsForms01
{
    public class CsMarking
    {
        #region 变量
        Dictionary<int, string> M_Dic_MarkingConTent = new Dictionary<int, string>();//喷码内容记录
        public static string M_MarkParameter = "";//喷码参数
        public static int M_Lastmarknum = 0;//相对喷码完成后的前一次的喷码编号
        public static int M_MarkCount = 0;//本次喷码编号
        public static int M_iMarkendnum = 0;//最后喷码完成的编号
        public static string M_recstr = "";//发送命令喷码机返回信息
        public static string M_MarkSetUp = "";//喷码机反馈的喷码完成信息
        public static string M_Markend = "";//喷码机反馈的喷码完成信息
        public static string M_Markstr = "";//喷码内容
        public static string M_PackingType = "";//喷码内容
        public static bool M_Ismarkingconnet = false;//喷码机是否连接
        public static string M_toplcmoving = "";//切换内容后启动喷码机移动信号标志
        public static string M_endflag = "";//切换内容后启动喷码机移动信号标志
        public static string M_Markchange = "";
        public static string M_toplcmarkend = "";//喷码完成标志信号
        public static string M_toplcstop = "";//喷码机停止信号标志
        public static string M_marklogprint = "";//输出喷码过程日志
        public static string M_RPlclogprint = "";//输出读PLC日志信息
        public static string M_WPlclogprint = "";//输出写PLC日志信息
        public static object M_lockertrg = new object();
        public static string StrNgChange = "";
        public static string readtest_flag = "";
        public static string writetest_flag = "";
        public static string YesLabelsignalFlag = "";
        public static string IsLabelsignal = "0";
        string surplus = "";
        public static Dictionary<string, object> M_dic_alert = new Dictionary<string, object>();//PLC喷码警报信息
        public static Dictionary<string, int> M_DicZb = new Dictionary<string, int>();//拍照坐标
        public static string MarkIP = "";//相机IP
        public static string MarkIPoint = "";//相机IP
        public static Dictionary<string, string> M_dicmarkcontent = new Dictionary<string, string>();//喷码内容
        public static Soketprint.SoketPrint M_soketprint = new SoketPrint(CSReadPlc.DevName);
        public static MCData.MCdata mCdata = new MCdata();
        GetPlcBao csGetBao;
        public static int currentCount = 0;
        public static string M_path = @"D:\" + CSReadPlc.DevName + @"\marklog\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
        static A.BLL.newsContent content1 = new A.BLL.newsContent();
        #endregion
        public CsMarking(MCdata mcdata, GetPlcBao bao)
        {
            mCdata = mcdata;
            csGetBao = bao;
        }

        public void MarkingSetup(string MarkIP_, string MarkIPoint_)
        {
            #region 喷码机模块
            #region 连接PLC和喷码机
            try
            {
                M_Ismarkingconnet = M_soketprint.MarkingConnetion(MarkIP_, MarkIPoint_);//连接喷码机
                if (!M_Ismarkingconnet)
                {
                    M_Ismarkingconnet = M_soketprint.MarkingConnetion(MarkIP, MarkIPoint);//连接喷码机
                    if (M_soketprint.MarkIsOpen() == false)
                    {
                        M_soketprint.Maketing(1);
                        Cslogfun.WriteTomarkingLog("连接喷码机成功");
                    }
                }
                if (M_soketprint.MarkIsOpen() == false)
                {
                    string rec1 = M_soketprint.Maketing(1);
                    if (M_soketprint.MarkIsOpen() == false)
                    {
                        MessageBox.Show("请到喷码机屏幕上解除警报，再重新启动软件！！");
                        System.Environment.Exit(0);
                    }
                }
                surplus = M_soketprint.MarkInksurplus();
                if (!string.IsNullOrEmpty(surplus))
                {
                    if (int.Parse(surplus.Trim()) < 20)
                        MessageBox.Show("喷码墨盒中剩余不足%" + surplus + ",请准备更换墨盒");
                }
                if (M_Ismarkingconnet)
                {
                    Cslogfun.WriteToLog("连接喷码机成功ip:" + MarkIP + "端口:" + MarkIPoint);
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.Message);
            }
            #endregion
            //创建线程
            Thread Th_Mark = new Thread(Mark);
            Th_Mark.IsBackground = true;
            Th_Mark.Start();
            #endregion
        }
        //喷码切换
        void Mark()
        {
            while (true)
            {
                #region 喷码过程
                try
                {
                    Thread.Sleep(10);

                    if (mCdata.IsConnect())
                    {
                        //判断是否有标签
                        if (IsLabelsignal == "1"&&string.IsNullOrEmpty(YesLabelsignalFlag))
                        {
                            bool b_v = true;
                            if (string.IsNullOrEmpty(CSReadPlc.EditionFlag))
                            {
                                b_v = Chekin.IsLabel();
                            }
                            else
                            {
                                b_v = Chekin.IsGAPLabel();
                            }
                            if (b_v)
                            {
                                Cslogfun.WriteTomarkingLog("判断是有标签");
                                YesLabelsignalFlag = "start";
                                TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                while (IsLabelsignal != "0")
                                {
                                    TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                    TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                    if (ts.Seconds > 5)
                                    {
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                Cslogfun.WriteTomarkingLog("判断没有标签");
                                YesLabelsignalFlag = "";
                                Thread.Sleep(10000);
                            }
                        }
                        //响应喷码请求
                        if (string.IsNullOrEmpty(CSReadPlc.EditionFlag))
                        {
                            if (M_Markchange!="0")
                            AnswerMarking(M_MarkCount);
                        }
                        else
                        {
                            SetMarkingContent(M_MarkCount);
                        }
                    }
                }
                catch (Exception err)
                {
                    Cslogfun.WriteToLog(err);
                    MessageBox.Show("喷码通信出现异常，"+err.ToString());
                   // throw new Exception("提示：" + err.Message);
                }
                #endregion
            }
        }
        /// <summary>
        ///切换喷印模板和内容
        /// </summary>
        /// <param name="code">喷印编号</param>
        /// <param name="str">喷印内容</param>
        /// <param name="soketprint">喷印类对象</param>
        public void ChangeMark(int marknum_, string str, SoketPrint soketprint)
        {
            bool Ismarkingconnet = soketprint.MarkingConnetion(CSReadPlc.MarkIP, CSReadPlc.MarkIPoint);//连接喷码机
            if (!Ismarkingconnet)
            {
                M_Ismarkingconnet = M_soketprint.MarkingConnetion(MarkIP, MarkIPoint);//连接喷码机
                if (!M_Ismarkingconnet)
                {
                    CsFormShow.MessageBoxFormShow("喷码机网络连接断开,请检查网络是否通畅或者尝试重启软件！");
                }
                else
                {
                    CsFormShow.MessageBoxFormShow("喷码机重新连接成功");
                }
            }
            else
            {
                M_Markstr = str;
                if (!string.IsNullOrEmpty(CSReadPlc.EditionFlag))
                {
                    M_Markstr = ".";
                }
                int marknum = marknum_;
                string PackingCode = M_MarkingData.M_PackingCode;
                TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                while (M_MarkSetUp.Trim() != "0")
                {
                    TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                    TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                    if (ts.Seconds > 5)
                    {
                        break;
                    }
                }
                if (str == "stop")
                {
                    DialogResult btchose = MessageBox.Show("喷码内容为空，是否关闭软件重新启动", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (btchose == DialogResult.OK)
                    {
                        System.Environment.Exit(0);
                    }
                    else
                    {
                        return;
                    }
                }
                else if (marknum != M_Lastmarknum && M_MarkSetUp != "1")//一次喷印内容允许喷印一次
                {
                    M_Lastmarknum = marknum;
                    try
                    {
                        //readtest_flag = "start";
                        //writetest_flag = "start";
                        //Cslogfun.WriteTomarkingLog("本次喷码模式编号：" + PackingCode + "，生产线编号：" + M_PackingType);
                        Cslogfun.WriteTomarkingLog("" + Environment.NewLine);
                        Cslogfun.WriteTomarkingLog("收到PLC发来的喷码请求：" + marknum);
                        //切换模板
                        if (marknum == 12)
                        {
                            M_recstr = soketprint.Maketing("11");//切换模板
                        }
                        else
                        {
                            M_recstr = soketprint.Maketing(marknum.ToString().Trim());//切换模板
                        }
                        //切换内容
                        if (marknum == 2)
                        {
                            M_recstr = soketprint.Maketing("13", M_Markstr);//设置模板喷印内容
                        }
                        else if (marknum == 3)
                        {
                            M_recstr = soketprint.Maketing("14", M_Markstr);//设置模板喷印内容
                        }
                        else if (marknum == 4)
                        {
                            M_recstr = soketprint.Maketing("15", M_Markstr);//设置模板喷印内容
                        }
                        else if (marknum == 12)
                        {
                            M_recstr = soketprint.Maketing("11", M_Markstr);//设置模板喷印内容
                        }
                        else
                        {
                            M_recstr = soketprint.Maketing(marknum.ToString().Trim(), M_Markstr);//设置模板喷印内容
                        }
                        if (M_recstr == "06")//判断是否喷码机设置内容成功
                        {
                            Cslogfun.WriteTomarkingLog("上位机切换模板:" + marknum + "(本次喷码内容：" + str + ")" + ", 喷码内容设置完成");
                            //M_recstr = soketprint.Maketing(2);//喷印启动信号
                            //if (M_recstr == "06")//喷印启动成功
                            //{
                            M_toplcmoving = "toplcmoving";
                            //Cslogfun.WriteTomarkingLog("内容:" + marknum + "喷码启动");
                            //}
                            //Thread.Sleep(10);
                            //M_endstr = soketprint.IsEnd(); //喷印完成并反馈喷印完成信息
                            timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                            while (M_MarkSetUp != "1")
                            {
                                TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                if (ts.Seconds > 5)
                                {
                                    break;
                                }
                            }
                            if (M_MarkSetUp == "1" /*"02 31 03"*/)//判断是否喷印完成
                            {
                                // M_Lastmarknum = marknum;
                                M_toplcmarkend = "markend";
                                M_MarkSetUp = "yes";
                                Cslogfun.WriteTomarkingLog("收到PLC发来的喷码请求:" + marknum + "的喷码启动信号(当做喷码完成信号)");
                                //1-四行小箱，2-四行大箱，3-六行小箱，4-六行大箱，5-拍照盖章小箱，6-拍照盖章大箱，7-盖章
                                if ((PackingCode == "1" && marknum == 4) || (PackingCode == "2" && marknum == 4) || (PackingCode == "8" && marknum == 4) || (PackingCode == "3" && marknum == 6) || (PackingCode == "4" && marknum == 6) || (PackingCode == "5" && marknum == 12) || (PackingCode == "6" && marknum == 12) || (PackingCode == "7" && marknum == 12) || (PackingCode == "5" && marknum == 11) || (PackingCode == "6" && marknum == 11) || (PackingCode == "7" && marknum == 11))
                                {
                                    //M_Lastmarknum = 0;
                                    // timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                    //while (CsMarking.M_Markend != "1")
                                    //{
                                    //    TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                    //    TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                    //    if (ts.Seconds > 5)
                                    //    {
                                    //        break;
                                    //    }
                                    //}
                                    //M_endflag = "start";
                                    //Cslogfun.WriteTomarkingLog("全部喷码完成");
                                    UpdatePackingPage();
                                    #region 与关工对接
                                    //EndMarking();
                                    #endregion
                                    if (!M_stcScanData.MarkingIsshieid.Trim().Contains("不运行"))
                                    {
                                        if (!string.IsNullOrEmpty(surplus) && int.Parse(surplus.Trim()) < 5)
                                        {
                                            surplus = M_soketprint.MarkInksurplus();
                                            if (int.Parse(surplus.Trim()) < 2)
                                            {

                                                DialogResult btchose = MessageBox.Show("喷码墨盒中剩余不足%" + surplus + ",是否继续喷码，选择否软件将被关闭", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                                if (btchose != DialogResult.OK)
                                                {
                                                    System.Environment.Exit(0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            M_recstr = "yes";
                        }
                    }
                    catch (Exception err)
                    {
                        Cslogfun.WriteToLog(err);
                        throw new Exception("提示：" + err.Message);
                    }
                }
            }
        }
        /// <summary>
        /// 更新喷码箱号并加入打印标签队列
        /// </summary>
        public static void UpdatePackingPage()
        {
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    if (M_MarkingData.M_PackingCode == "1" || M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8")
                    {
                        if (CsPakingData.M_list_DicMarkingNONet.ToList().Count > 0)
                        {
                            if (CsPakingData.M_list_DicMarkingNONet.Count == 0)
                            {
                                CsFormShow.MessageBoxFormShow("没有喷码订单数据，请检查是否有订单，按确定继续!!");
                            }
                            int CurrentPackingNum = int.Parse(CsPakingData.GetMarking_NONet("CurrentPackingNum"));
                            int Cartoncount = int.Parse(CsPakingData.GetMarking_NONet("Cartoncount"));
                            string po = CsPakingData.GetMarking_NONet("po");
                            string Poguid = CsPakingData.GetMarking_NONet("packing_guid");
                            string finishmarknumber = CsGetData.UpdateGAPMarkingNumber(CsPakingData.GetMarking_NONet("packing_guid"));
                            int EndNum = int.Parse(CsPakingData.GetMarking_NONet("到"));
                            int StartNum = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_NONet("从"));
                            if (Cartoncount == 1)
                            {
                                CsPakingData.M_list_Dic_GAPPalletizing.Add(CsPakingData.M_list_DicMarkingNONet.ToList()[0]);//加入码垛队列
                                 // Cslogfun.WriteTomarkingLog("本次喷码订单:" + po + ",从:" + CsPakingData.GetMarking_Net("从") + ",加入码垛队列集合中");
                            }
                            //喷码打印计数
                            if (Cartoncount < CurrentPackingNum)
                            {
                                //更新喷码完成计数
                                CsPakingData.M_list_DicMarkingNONet.ToList()[0]["finishmarknumber"] = finishmarknumber;
                                //更新数据库队列中下次喷码的箱号Carton
                                CsPakingData.M_list_DicMarkingNONet.ToList()[0]["Cartoncount"] = (int.Parse(CsPakingData.GetMarking_NONet("Cartoncount")) + 1).ToString();
                                CsPakingData.M_list_DicMarkingNONet.ToList()[0]["Carton1"] = int.Parse(finishmarknumber).ToString() + int.Parse(CsPakingData.GetMarking_NONet("从"));
                                CsPakingData.M_list_DicMarkingNONet.ToList()[0]["Carton"] = int.Parse(finishmarknumber) + int.Parse(CsPakingData.GetMarking_NONet("从")) + "        " + CsPakingData.GetMarking_NONet("订单总箱数");
                                string sqlupdate = string.Format("UPDATE dbo.MarkingLineUp SET Cartoncount=Cartoncount+1 WHERE 订单号码='{0}' AND Color='{1}' AND 开始序列号='{2}'", CsPakingData.GetMarking_NONet("po"), CsPakingData.GetMarking_NONet("Color"), CsPakingData.GetMarking_NONet("开始序列号"));
                                CsFormShow.GoSqlUpdateInsert(sqlupdate);
                                if (!CsPakingData.GetMarking_NONet("GAPIsPrintRun").Contains("不运行"))//是否屏蔽打印
                                {
                                    int page = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_NONet("从"));
                                    int sort = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_NONet("开始序列号"));
                                    setprint(Poguid, po, page, sort);//加入打印列队
                                }
                            }
                            else if (Cartoncount == CurrentPackingNum || Cartoncount > CurrentPackingNum || StartNum == EndNum || StartNum > EndNum)
                            {
                                Cslogfun.WriteToCartornLog("当前订单喷码结束，订单：" + po + "从：" + CsPakingData.GetMarking_NONet("从") + ",当前箱号：" + StartNum.ToString());
                                //更新喷码完成计数
                                CsPakingData.M_list_DicMarkingNONet.ToList()[0]["finishmarknumber"] = finishmarknumber;
                                if (!CsPakingData.GetMarking_NONet("GAPIsPrintRun").Contains("不运行"))//是否屏蔽打印
                                {
                                    int page = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_NONet("从"));
                                    int sort = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_NONet("开始序列号"));
                                    setprint(Poguid, po, page, sort);//加入打印列队
                                }
                                //删除已完成的喷码记录
                                DeleteMarkingLineUpFromSql(CsPakingData.M_list_DicMarkingNONet.ToList()[0]);
                                CsPakingData.M_list_DicMarkingNONet.Remove(CsPakingData.M_list_DicMarkingNONet.ToList()[0]);//删除列队缓存中的本次
                                CsPakingData.M_list_DicMarkingNONet.Clear();//先清除再重新加载
                                CsInitialization.UpdateGAPLineUp();
                            }
                            else
                            {
                                MessageBox.Show("超当前设置数量！！");
                            }
                        }
                    }
                    else if (M_MarkingData.M_PackingCode == "3" || M_MarkingData.M_PackingCode == "4")
                    {
                        if (CsPakingData.M_list_DicMarkingNet.ToList().Count > 0)
                        {
                            if (CsPakingData.M_list_DicMarkingNet.Count == 0)
                            {
                                CsFormShow.MessageBoxFormShow("没有喷码订单数据，请检查是否有订单，按确定继续!!");
                            }
                            int CurrentPackingNum = int.Parse(CsPakingData.GetMarking_Net("CurrentPackingNum"));
                            int Cartoncount = int.Parse(CsPakingData.GetMarking_Net("Cartoncount"));
                            string po = CsPakingData.GetMarking_Net("po");
                            string Poguid = CsPakingData.GetMarking_Net("packing_guid");
                            //更新喷码完成计数
                            string finishmarknumber = CsGetData.UpdateGAPMarkingNumber(CsPakingData.GetMarking_Net("packing_guid"));
                            int EndNum = int.Parse(CsPakingData.GetMarking_Net("到"));
                            int StartNum = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_Net("从"));
                            //更新码垛打印队列
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET Palletizingstarttime='{1}' WHERE packing_guid='{0}'", Poguid, DateTime.Now.ToString() + " " + DateTime.Now.Millisecond.ToString()));
                            if (Cartoncount == 1)
                            {
                                CsPakingData.M_list_Dic_GAPPalletizing.Add(CsPakingData.M_list_DicMarkingNet.ToList()[0]);//加入码垛队列
                            }
                            //喷码打印计数
                            if (Cartoncount < CurrentPackingNum)
                            {
                                CsPakingData.M_list_DicMarkingNet.ToList()[0]["finishmarknumber"] = finishmarknumber;
                                //更新数据库队列中下次喷码的箱号Carton
                                CsPakingData.M_list_DicMarkingNet.ToList()[0]["Cartoncount"] = (int.Parse(CsPakingData.GetMarking_Net("Cartoncount")) + 1).ToString();
                                CsPakingData.M_list_DicMarkingNet.ToList()[0]["Carton1"] = int.Parse(finishmarknumber).ToString() + int.Parse(CsPakingData.GetMarking_Net("从"));
                                CsPakingData.M_list_DicMarkingNet.ToList()[0]["Carton"] = int.Parse(finishmarknumber) + int.Parse(CsPakingData.GetMarking_Net("从")) + "                " + CsPakingData.GetMarking_Net("订单总箱数");
                                string sqlupdate = string.Format("UPDATE dbo.MarkingLineUp SET Cartoncount=Cartoncount+1 WHERE 订单号码='{0}' AND Color='{1}' AND 开始序列号='{2}'", CsPakingData.GetMarking_Net("po"), CsPakingData.GetMarking_Net("Color"), CsPakingData.GetMarking_Net("开始序列号"));
                                CsFormShow.GoSqlUpdateInsert(sqlupdate);
                                if (!CsPakingData.GetMarking_Net("GAPIsPrintRun").Contains("不运行"))//是否屏蔽打印
                                {
                                    int page = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_Net("从"));
                                    int sort = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_Net("开始序列号"));
                                    setprint(Poguid, po, page, sort);//加入打印列队
                                                                     //int finishmarknumber_v = int.Parse(CsGetData.GetGAPMarkingNumber(Poguid)) - 1 + int.Parse(CsPakingData.GetMarking_Net("从"));
                                }
                                //M_stcPrintData.strPrint_po = po;
                                //M_stcPrintData.iPage = int.Parse(finishmarknumber);
                                //M_stcPrintData.iTotal = int.Parse(CsPakingData.GetMarking_Net("到").Trim());
                            }
                            else if (Cartoncount == CurrentPackingNum || Cartoncount > CurrentPackingNum || StartNum == EndNum || StartNum > EndNum)
                            {
                                Cslogfun.WriteToCartornLog("当前订单喷码结束，订单：" + po + "从：" + CsPakingData.GetMarking_Net("从") + ",当前箱号：" + StartNum.ToString());
                                CsPakingData.M_list_DicMarkingNet.ToList()[0]["finishmarknumber"] = finishmarknumber;
                                if (!CsPakingData.GetMarking_Net("GAPIsPrintRun").Contains("不运行"))//是否屏蔽打印
                                {
                                    int page = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_Net("从"));
                                    int sort = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_Net("开始序列号"));
                                    setprint(Poguid, po, page, sort);//加入打印列队
                                }
                                //删除已完成的喷码记录
                                DeleteMarkingLineUpFromSql(CsPakingData.M_list_DicMarkingNet.ToList()[0]);
                                CsPakingData.M_list_DicMarkingNet.Remove(CsPakingData.M_list_DicMarkingNet.ToList()[0]);//删除列队缓存中的本次
                                CsInitialization.UpdateGAPLineUp();
                            }
                            else
                            {
                                MessageBox.Show("超当前设置数量！！");
                            }
                        }
                    }
                    else if ((M_MarkingData.M_PackingCode == "5" || M_MarkingData.M_PackingCode == "6" || M_MarkingData.M_PackingCode == "7") && CsPakingData.M_list_Dic_colorcode.ToList().Count > 0)
                    {
                        int total = int.Parse(CsPakingData.GetMarking_Colorcode("Quantity"));
                        int qty = int.Parse(CsPakingData.GetMarking_Colorcode("qty"));
                        string Order_No = CsPakingData.GetMarking_Colorcode("Order_No");
                        string Color_Code = CsPakingData.GetMarking_Colorcode("Color_Code");
                        string Warehouse = CsPakingData.GetMarking_Colorcode("Warehouse");
                        string Set_Code = CsPakingData.GetMarking_Colorcode("Set_Code");
                        string YK_guid = CsPakingData.GetMarking_Colorcode("YK_guid");
                        if (CsPakingData.M_list_Dic_colorcode.ToList()[0].ToList().Count > 0)
                        {
                            if (CsPakingData.M_list_Dic_colorcode.Count == 0)
                            {
                                CsFormShow.MessageBoxFormShow("没有喷码订单数据，请检查是否有订单，按确定继续!!");
                            }
                            CsPakingData.M_list_Dic_colorcode.ToList()[0]["finishmarknumberTotal"] = (int.Parse(CsPakingData.GetMarking_Colorcode("finishmarknumberTotal")) + 1).ToString();
                            UpdateYYKGUMarkingNumber(Order_No, Set_Code, Warehouse, YK_guid, qty.ToString(), total.ToString());
                            if (qty == 1)
                            {
                                CsPakingData.M_list_Dic_YYKGUPalletizing.Add(CsPakingData.M_list_Dic_colorcode.ToList()[0]);//加入码垛队列
                                Cslogfun.WriteTomarkingLog("本次喷码订单:" + Order_No + ",Set_Code:" + Set_Code + ",加入码垛队列集合中");
                            }
                            //喷码打印计数
                            if (qty < total)
                            {
                                CsPakingData.M_list_Dic_colorcode.ToList()[0]["qty"] = (int.Parse(CsPakingData.GetMarking_Colorcode("qty")) + 1).ToString();
                                //更新数据库队列中下次喷码的箱号Carton
                                string sqlupdate = string.Format("UPDATE dbo.YYKMarkingLineUp SET Cartoncount=Cartoncount+1 WHERE Order_No='{1}' AND Color_Code='{2}' AND Warehouse='{3}'AND Set_Code='{4}'", CsPakingData.GetMarking_Colorcode("qty"), CsPakingData.GetMarking_Colorcode("Order_No"), CsPakingData.GetMarking_Colorcode("Color_Code"), CsPakingData.GetMarking_Colorcode("Warehouse"), CsPakingData.GetMarking_Colorcode("Set_Code"));
                                CsFormShow.GoSqlUpdateInsert(sqlupdate);
                            }
                            else if (qty == total)
                            {
                                Cslogfun.WriteToCartornLog("当前订单喷码结束，订单：" + Order_No + ",Warehouse:" + Warehouse + ",Set_Code:" + Set_Code + ",当前第：" + qty.ToString() + "箱" );
                                CsPakingData.M_list_Dic_colorcode.ToList()[0]["END"] = "完成";
                                //删除已完成的喷码记录
                                DeleteYYKMarkingLineUpFromSql(CsPakingData.M_list_Dic_colorcode.ToList()[0]);
                                CsPakingData.M_list_Dic_colorcode.Remove(CsPakingData.M_list_Dic_colorcode.ToList()[0]);//删除列队缓存中的本次
                                CsInitialization.UpdateYYKGULineUp();
                            }
                            else
                            {
                                MessageBox.Show("超当前设置数量！！");
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }
        /// <summary>
        /// 响应喷码请求
        /// </summary>
        /// <param name="MarkCount_"></param>
        public void AnswerMarking(int MarkCount_)
        {
            try
            {
                switch (MarkCount_)
                {
                    case 1:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {

                            if (M_MarkingData.M_PackingCode == "1" && CsPakingData.GetMarking_NONet("订单号码").Trim() != "")
                            {
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_NONet("订单号码"), M_soketprint);
                            }
                            else if ((M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8") && CsPakingData.GetMarking_NONet("订单号码").Trim() != "")
                            {
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_NONet("订单号码"), M_soketprint);
                            }
                            else if (M_MarkingData.M_PackingCode == "3" && CsPakingData.GetMarking_Net("Carton").Trim() != "")
                            {
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("Carton"), M_soketprint);
                            }
                            else if (M_MarkingData.M_PackingCode == "4" && CsPakingData.GetMarking_Net("Carton").Trim() != "")
                            {
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("Carton"), M_soketprint);
                            }
                        }
                        break;
                    case 2:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {
                            if (M_MarkingData.M_PackingCode == "1" && CsPakingData.GetMarking_NONet("SKU/Item").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_NONet("SKU/Item"), M_soketprint);
                            else if ((M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8") && CsPakingData.GetMarking_NONet("SKU/Item").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_NONet("SKU/Item"), M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "3" && CsPakingData.GetMarking_Net("订单号码").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("订单号码"), M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "4" && CsPakingData.GetMarking_Net("订单号码").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("订单号码"), M_soketprint);
                        }
                        break;
                    case 3:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {
                            if (M_MarkingData.M_PackingCode == "1" && CsPakingData.GetMarking_NONet("Unit/Prepack").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_NONet("Unit/Prepack"), M_soketprint);
                            else if ((M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8") && CsPakingData.GetMarking_NONet("Unit/Prepack").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_NONet("Unit/Prepack"), M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "3" && CsPakingData.GetMarking_Net("style/size").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("style/size"), M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "4" && CsPakingData.GetMarking_Net("style/size").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("style/size"), M_soketprint);
                        }
                        break;
                    case 4:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {
                            if (M_MarkingData.M_PackingCode == "1" && CsPakingData.GetMarking_NONet("Carton").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_NONet("Carton"), M_soketprint);
                            else if ((M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8") && CsPakingData.GetMarking_NONet("Carton").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_NONet("Carton"), M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "3" && CsPakingData.GetMarking_Net("qty").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("qty"), M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "4" && CsPakingData.GetMarking_Net("qty").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("qty"), M_soketprint);
                        }
                        break;
                    case 5:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {
                            if (M_MarkingData.M_PackingCode == "3" && CsPakingData.GetMarking_Net("GrossWeight").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("GrossWeight"), M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "4" && CsPakingData.GetMarking_Net("GrossWeight").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("GrossWeight"), M_soketprint);
                        }
                        break;
                    case 6:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {
                            if (M_MarkingData.M_PackingCode == "3" && CsPakingData.GetMarking_Net("NetWeight").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("NetWeight"), M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "4" && CsPakingData.GetMarking_Net("NetWeight").Trim() != "")
                                ChangeMark(MarkCount_, CsPakingData.GetMarking_Net("NetWeight"), M_soketprint);
                        }
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                    case 9:
                        //M_soketprint.Maketing("9", CsPakingData.GetMarking_Colorcode(0));//设置模板喷印内容
                        //M_soketprint.Maketing("10", CsPakingData.GetMarking_Colorcode(0));//设置模板喷印内容
                        if ((M_MarkingData.M_PackingCode == "5" || M_MarkingData.M_PackingCode == "6") && CsPakingData.GetMarking_Colorcode(0) != "" && CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1")
                            ChangeMark(MarkCount_, CsPakingData.GetMarking_Colorcode(0), M_soketprint);
                        else if ((M_MarkingData.M_PackingCode == "5" || M_MarkingData.M_PackingCode == "6") && CsPakingData.GetMarking_Colorcode(0) != "" && CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2")
                            ChangeMark(MarkCount_, CsPakingData.GetMarking_Colorcode(0), M_soketprint);
                        break;
                    case 10:
                        if ((M_MarkingData.M_PackingCode == "5" || M_MarkingData.M_PackingCode == "6") && CsPakingData.GetMarking_Colorcode(0) != "" && (CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1"))
                            ChangeMark(MarkCount_, CsPakingData.GetMarking_Colorcode(0), M_soketprint);
                        else if ((M_MarkingData.M_PackingCode == "5" || M_MarkingData.M_PackingCode == "6") && CsPakingData.GetMarking_Colorcode(0) != "" && (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                            ChangeMark(MarkCount_, CsPakingData.GetMarking_Colorcode(0), M_soketprint);
                        break;
                    case 11:
                            if ( CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1")
                            {
                                if (!string.IsNullOrEmpty(M_MarkingData.M_markingData))
                                {
                                    if (stcUserData.IsScanBarcode)
                                    {
                                        ChangeMark(MarkCount_, "(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" + M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                    }
                                    else
                                    {
                                        ChangeMark(MarkCount_, M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                    }
                                }
                                else
                                {
                                    if (stcUserData.IsScanBarcode)
                                    {
                                        ChangeMark(MarkCount_, "(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" + DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//客户设置日期
                                    }
                                    else
                                    {
                                        ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//客户设置日期
                                    }
                                }
                            }
                            else if ( CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2")
                            {
                                if (!string.IsNullOrEmpty(M_MarkingData.M_markingData))
                                {
                                    if (stcUserData.IsScanBarcode)
                                    {
                                        ChangeMark(MarkCount_, "(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" + M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                    }
                                    else
                                    {
                                        ChangeMark(MarkCount_,M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                    }
                                }
                                else
                                {
                                    if (stcUserData.IsScanBarcode)
                                    {
                                        ChangeMark(MarkCount_, "(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" + DateTime.Now.ToString("yyyy年MM月dd日")  + "  已检针", M_soketprint);//客户设置日期
                                    }
                                    else
                                    {
                                        ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日")  + "  已检针", M_soketprint);//客户设置日期
                                    }
                                }
                            }
                        break;
                    case 12:
                            if (CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1")
                            {
                                if (!string.IsNullOrEmpty(M_MarkingData.M_markingData))
                                {
                                    if (stcUserData.IsScanBarcode)
                                    {
                                        ChangeMark(MarkCount_, "(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" + M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                    }
                                    else
                                    {
                                        ChangeMark(MarkCount_, M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                    }
                                }
                                else
                                {
                                    if (stcUserData.IsScanBarcode)
                                    {
                                        ChangeMark(MarkCount_, "(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" + DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//客户设置日期
                                    }
                                    else
                                    {
                                        ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//客户设置日期
                                    }
                                }
                            }
                            else if ( CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2")
                            {
                                if (!string.IsNullOrEmpty(M_MarkingData.M_markingData))
                                {
                                    if (stcUserData.IsScanBarcode)
                                    {
                                        ChangeMark(MarkCount_, "(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" + M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                    }
                                    else
                                    {
                                        ChangeMark(MarkCount_, M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                    }
                                }
                                else
                                {
                                    if (stcUserData.IsScanBarcode)
                                    {
                                        ChangeMark(MarkCount_, "(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" + DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//客户设置日期
                                    }
                                    else
                                    {
                                        ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//客户设置日期
                                    }
                                }
                            }
                        break;
                    case 0:
                        M_Lastmarknum = 0;
                        break;
                    default: break;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
        #region 与关工对接
        /// <summary>
        /// 响应喷码请求
        /// </summary>
        public void SetMarkingContent(int MarkCount_)
        {
            try
            {
                switch (MarkCount_)
                {
                    case 1:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {
                            string catorn = CsPublicVariablies.Marking.Cartorn+ "                " + CsPublicVariablies.Marking.PoQty;
                            if (M_MarkingData.M_PackingCode == "1" && !string.IsNullOrEmpty(CsPublicVariablies.Marking.Po.Trim()))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.Po, M_soketprint);
                            else if ((M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8") && !string.IsNullOrEmpty(CsPublicVariablies.Marking.Po.Trim()))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.Po, M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "3" && catorn.Trim() != "")
                                ChangeMark(MarkCount_, catorn, M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "4" && catorn.Trim() != "")
                                ChangeMark(MarkCount_, catorn, M_soketprint);
                        }
                        break;
                    case 2:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {
                            if (M_MarkingData.M_PackingCode == "1" && !string.IsNullOrEmpty(CsPublicVariablies.Marking.SkuItem))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.SkuItem, M_soketprint);
                            else if ((M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8") && !string.IsNullOrEmpty(CsPublicVariablies.Marking.SkuItem))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.SkuItem, M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "3" && !string.IsNullOrEmpty(CsPublicVariablies.Marking.Po.Trim()))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.Po, M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "4" && !string.IsNullOrEmpty(CsPublicVariablies.Marking.Po.Trim()))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.Po, M_soketprint);
                        }
                        break;
                    case 3:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {
                            if (M_MarkingData.M_PackingCode == "1" &&!string.IsNullOrEmpty(CsPublicVariablies.Marking.UnitPrepack))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.UnitPrepack, M_soketprint);
                            else if ((M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8") && !string.IsNullOrEmpty(CsPublicVariablies.Marking.UnitPrepack))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.UnitPrepack, M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "3" && !string.IsNullOrEmpty(CsPublicVariablies.Marking.StyleSize))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.StyleSize, M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "4" && !string.IsNullOrEmpty(CsPublicVariablies.Marking.StyleSize))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.StyleSize, M_soketprint);
                        }
                        break;
                    case 4:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {
                           string catorn = CsPublicVariablies.Marking.Cartorn + "           " + CsPublicVariablies.Marking.PoQty;
                            if (M_MarkingData.M_PackingCode == "1" && !string.IsNullOrEmpty(catorn))
                                ChangeMark(MarkCount_, catorn, M_soketprint);
                            else if ((M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8") && !string.IsNullOrEmpty(catorn))
                                ChangeMark(MarkCount_, catorn, M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "3" && !string.IsNullOrEmpty(CsPublicVariablies.Marking.Qty))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.Qty, M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "4" && !string.IsNullOrEmpty(CsPublicVariablies.Marking.Qty))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.Qty, M_soketprint);
                        }
                        break;
                    case 5:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {
                            if (M_MarkingData.M_PackingCode == "3" && !string.IsNullOrEmpty(CsPublicVariablies.Marking.GrossWeight))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.GrossWeight, M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "4" &&!string.IsNullOrEmpty(CsPublicVariablies.Marking.GrossWeight))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.GrossWeight, M_soketprint);
                        }
                        break;
                    case 6:
                        if ((CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                        {
                            if (M_MarkingData.M_PackingCode == "3" && !string.IsNullOrEmpty(CsPublicVariablies.Marking.NetWeight))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.NetWeight, M_soketprint);
                            else if (M_MarkingData.M_PackingCode == "4" && !string.IsNullOrEmpty(CsPublicVariablies.Marking.NetWeight))
                                ChangeMark(MarkCount_, CsPublicVariablies.Marking.NetWeight, M_soketprint);
                        }
                        break;
                    case 7:
                        break;
                    case 8:
                        break;
                    case 11:
                        if (/*CsPakingData.M_list_Dic_colorcode.Count > 0*/true)
                        {
                            if (!string.IsNullOrEmpty(M_MarkingData.M_markingData) && (CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1"))
                            {
                                if (string.IsNullOrEmpty(M_MarkingData.IsMarkingNum))
                                {
                                    ChangeMark(MarkCount_, /*"(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" +*/ M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                }
                                else
                                {
                                    ChangeMark(MarkCount_, M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                }
                            }
                            else if (!string.IsNullOrEmpty(M_MarkingData.M_markingData) && (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                            {
                                if (string.IsNullOrEmpty(M_MarkingData.IsMarkingNum))
                                {
                                    ChangeMark(MarkCount_, /*"(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" + */M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                }
                                else
                                {
                                    ChangeMark(MarkCount_, M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                }
                            }
                            else if (string.IsNullOrEmpty(M_MarkingData.M_markingData) && (CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1"))
                            {
                                if (string.IsNullOrEmpty(M_MarkingData.IsMarkingNum))
                                {
                                    ChangeMark(MarkCount_, /*"(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" +*/ DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//当前日期
                                }
                                else
                                {
                                    ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//当前日期
                                }
                                // ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日") , M_soketprint);//当前日期
                            }
                            else if (string.IsNullOrEmpty(M_MarkingData.M_markingData) && (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                            {
                                if (string.IsNullOrEmpty(M_MarkingData.IsMarkingNum))
                                {
                                    ChangeMark(MarkCount_, /*"(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" +*/ DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//当前日期
                                }
                                else
                                {
                                    ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//当前日期
                                }
                                // ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日") , M_soketprint);//当前日期
                            }
                        }
                        break;
                    case 12:
                        if (/*CsPakingData.M_list_Dic_colorcode.Count > 0*/true)
                        {
                            if (!string.IsNullOrEmpty(M_MarkingData.M_markingData) && (CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1"))
                            {
                                if (string.IsNullOrEmpty(M_MarkingData.IsMarkingNum))
                                {
                                    ChangeMark(MarkCount_, /*"(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" +*/ M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                }
                                else
                                {
                                    ChangeMark(MarkCount_, M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                }
                            }
                            else if (!string.IsNullOrEmpty(M_MarkingData.M_markingData) && (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                            {
                                if (string.IsNullOrEmpty(M_MarkingData.IsMarkingNum))
                                {
                                    ChangeMark(MarkCount_, /*"(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" +*/ M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                }
                                else
                                {
                                    ChangeMark(MarkCount_, M_MarkingData.M_markingData + "  已检针", M_soketprint);//客户设置日期
                                }
                            }
                            else if (string.IsNullOrEmpty(M_MarkingData.M_markingData) && (CSReadPlc.DevName.Trim() == "plc1" && M_PackingType == "1"))
                            {
                                if (string.IsNullOrEmpty(M_MarkingData.IsMarkingNum))
                                {
                                    ChangeMark(MarkCount_, /*"(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" + */DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//当前日期
                                }
                                else
                                {
                                    ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//当前日期
                                }
                                // ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日") , M_soketprint);//当前日期
                            }
                            else if (string.IsNullOrEmpty(M_MarkingData.M_markingData) && (CSReadPlc.DevName.Trim() == "plc2" && M_PackingType == "2"))
                            {
                                if (string.IsNullOrEmpty(M_MarkingData.IsMarkingNum))
                                {
                                    ChangeMark(MarkCount_, /*"(" + CsPakingData.GetMarking_Colorcode("finishmarknumberTotal") + ")" + */DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//当前日期
                                }
                                else
                                {
                                    ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日") + "  已检针", M_soketprint);//当前日期
                                }
                                // ChangeMark(MarkCount_, DateTime.Now.ToString("yyyy年MM月dd日") , M_soketprint);//当前日期
                            }
                        }
                        break;
                    case 0:
                        M_Lastmarknum = 0;
                        break;
                    default: break;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show(err.ToString());
            }
        }
        public void EndMarking()
        {
            try
            {
                if (M_MarkingData.M_PackingCode == "1" || M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8")
                {
                    string finishmarknumber = CsGetData.UpdateGAPMarkingNumber(CsPublicVariablies.Marking.Po_guid);
                }
                else if (M_MarkingData.M_PackingCode == "3" || M_MarkingData.M_PackingCode == "4")
                {
                    string finishmarknumber = CsGetData.UpdateGAPMarkingNumber(CsPublicVariablies.Marking.Po_guid);
                }
                else if ((M_MarkingData.M_PackingCode == "5" || M_MarkingData.M_PackingCode == "6" || M_MarkingData.M_PackingCode == "7") && CsPakingData.M_list_Dic_colorcode.ToList().Count > 0)
                {
                    UpdateYYKGUMarkingNumber(CsPublicVariablies.Marking.OrderNo, CsPublicVariablies.Marking.Set_Code, CsPublicVariablies.Marking.Warehouse, CsPublicVariablies.Marking.Po_guid, "0","0");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show(err.ToString());
            }
        }
        #endregion
        /// <summary>
        /// 更新GAP订单喷码完成计数
        /// </summary>
        /// <param name="packingguid_"></param>
        /// <param name="po_"></param>
        /// <returns></returns>
        //public static string UpdateGAPMarkingNumber(string packingguid_)
        //{
        //    try
        //    {
        //        //更新码垛类型队列和数据库
        //        CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO dbo.PalletizingtypeTable (  DeviceName, potype, Entrytime,po_guid )VALUES  ( '{0}','{1}','{2}' ,'{3}')", CSReadPlc.DevName, "GAP", DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss") + " " + DateTime.Now.Millisecond.ToString(), packingguid_));
        //        CsInitialization.UpdatePalletizingtype();
        //        //更新订单喷码完成数量并返回完成的总箱数序号
        //        CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET finishmarknumber=isnull(finishmarknumber,0)+1 WHERE guid='{0}'", packingguid_));
        //        DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT finishmarknumber FROM dbo.PACKING WHERE guid='{0}' ", packingguid_));
        //        if (dt.Rows.Count > 0)
        //        {
        //            return dt.Rows[0]["finishmarknumber"].ToString().Trim();
        //        }
        //        return "";
        //    }
        //    catch (Exception err)
        //    {
        //        Cslogfun.WriteToLog(err);
        //        throw new Exception("提示：" + err.Message);
        //    }
        //}
        /// <summary>
        /// 更新GU/优衣库订单喷码完成计数
        /// </summary>
        /// <param name="packingguid_"></param>
        /// <param name="po_"></param>
        /// <returns></returns>
        public static void UpdateYYKGUMarkingNumber(string po_, string Set_Code_, string Warehouse_, string YK_guid_,string Num_,string total_)
        {
            try
            {        
                //更新码垛类型队列和数据库
                DataTable dt_potype = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", po_, Warehouse_, Set_Code_));
                string potype = dt_potype.Rows[0]["potype"].ToString();
                CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO dbo.PalletizingtypeTable (DeviceName, potype, Entrytime,po_guid )VALUES  ( '{0}','{1}' ,'{2}','{3}' )", CSReadPlc.DevName, potype, DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss") + " " + DateTime.Now.Millisecond.ToString(), YK_guid_));
                  CsInitialization.UpdatePalletizingtype();
                //更新订单喷码完成数量
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YOUYIKUDO SET  finishmarknumber=isnull(finishmarknumber,0)+1,packingtotal= CASE WHEN ISNULL(packingtotal,0)<ISNULL(finishmarknumber,0) THEN finishmarknumber ELSE packingtotal END WHERE Order_No = '{0}'  AND Set_Code = '{1}'AND Warehouse = '{2}'", po_, Set_Code_, Warehouse_));
                //更新排单表
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.SchedulingTable SET  CurrentPackingNum=isnull(CurrentPackingNum,0)+1 WHERE  订单号码 = '{0}'  AND Set_Code = '{1}'AND Warehouse = '{2}'", po_, Set_Code_, Warehouse_));
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET finishmarknumber=ISNULL(finishmarknumber,0)+1 WHERE YK_guid='{0}'", YK_guid_));
                //码垛队列更新
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET Palletizingstarttime='{3}' WHERE Order_No = '{0}'  AND Set_Code = '{1}'AND Warehouse = '{2}'", po_, Set_Code_, Warehouse_, DateTime.Now.ToString() + " " + DateTime.Now.Millisecond.ToString()));
                if (stcUserData.IsScanBarcode)
                {
                    //增加箱号到记录
                    CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO dbo.CartonNOHistoricRcords(单号,卷号,箱号,录入时间,DeviceName)VALUES('{0}','{1}','{2}','{3}','{4}')", po_, M_stcScanData.BatchNo_Now, CsPakingData.GetMarking_Colorcode("finishmarknumberTotal"), DateTime.Now.ToString("yyyy-MM-dd"), CSReadPlc.DevName));
                }
                //获取喷码完成数量
                Cslogfun.WriteToCartornLog("喷码完成一箱，订单：" + po_ +",本次第："+ Num_+"箱"+",订单本次共"+ total_+"箱");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 将喷码完成的箱子加入到打印列队中等待打印标签
        /// </summary>
        /// <param name="po_">单号</param>
        /// <param name="Carton1_">第几箱</param>
        private static void setprint(string Poguid_, string po_, int page_,int sort_)
        {
            try
            {
                string sqlstr1 = string.Format("SELECT *FROM( SELECT ROW_NUMBER() OVER(ORDER BY num) AS xuhao,* FROM ( SELECT  *,number1 AS num FROM dbo.PRINTLineuUp WHERE po='{0}') pdf) pdftable WHERE po='{0}' AND (page='{1}' OR xuhao='{1}')", po_, page_);
                DataTable dt1 = CsFormShow.GoSqlSelect(sqlstr1);
                if (dt1.Rows.Count > 0)
                    return;
                string sqlstr2 = string.Format("SELECT '{2}' as Poguid,*FROM ( SELECT ROW_NUMBER() OVER(ORDER BY num) AS xuhao, *FROM(SELECT *, number1 AS num, SUBSTRING(number1, LEN(number1) - 4, 4)AS sort  FROM dbo.PDFDATA WHERE po = '{0}') pdf) pdftable WHERE po = '{0}' AND sort=SUBSTRING('{3}',LEN('{3}')-3,4)  AND (page = '{1}' OR xuhao='{1}')", po_, page_, Poguid_,sort_);
                DataTable dt2 = CsFormShow.GoSqlSelect(sqlstr2);
                if (dt2.Rows.Count > 0)
                {
                    CsPrint.PrintLineuUp(dt2);
                }
                else
                {
                    MessageBox.Show("提示：没有相应的PDF标签数据");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 删除已完成记录
        /// </summary>
        /// <param name="dic_"></param>
        public static void DeleteMarkingLineUpFromSql(Dictionary<string, string> dic_)
        {
            try
            {
                //string sqlstr = string.Format("DELETE FROM dbo.MarkingLineUp WHERE packing_guid='{0}'", dic_["packing_guid"]);
                if (!string.IsNullOrEmpty(dic_["packing_guid"]))
                {
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE  dbo.MarkingLineUp SET IsEndMarking=1,IsEndPacking=1 WHERE packing_guid='{0}'", dic_["packing_guid"]));
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET  packingtotal=finishmarknumber WHERE guid='{0}'", dic_["packing_guid"]));
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 删除已完成记录
        /// </summary>
        /// <param name="dic_"></param>
        public static void DeleteYYKMarkingLineUpFromSql(Dictionary<string, string> dic_)
        {
            try
            {
                // sqlstr = string.Format("DELETE FROM dbo.YYKMarkingLineUp WHERE YK_guid='{0}'", dic_["YK_guid"]);
                if (!string.IsNullOrEmpty(dic_["YK_guid"]))
                {
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET IsEndMarking=1,IsEndPacking=1 WHERE YK_guid='{0}'", dic_["YK_guid"]));
                }
                //重新加载码垛队列
                //if (CSReadPlc.PoType.Contains("UNIQLO"))
                //{
                //    CsInitialization.UpdateYYKLineUp();
                //}
                //else if (CSReadPlc.PoType.Contains("GU"))
                //{
                //    CsInitialization.UpdateGULineUp();
                //}
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 加载喷码列队
        /// </summary>
        /// <param name="dr"></param>
        public static void SetToMarking(DataRow dr)
        {
            try
            {
                #region 获取dr列值
                string guid = dr["guid"].ToString().Trim();
                string fanwei = dr["范围"].ToString().Trim();
                string startcode = dr["从"].ToString().Trim();
                string endcode = dr["到"].ToString().Trim();
                string qty = dr["数量"].ToString().Trim();
                string inqty = dr["内部包装的项目数量"].ToString().Trim();
                string innum = dr["内包装计数"].ToString().Trim();
                string Cartonqty = dr["箱数"].ToString().Trim();
                string netweight = dr["净重"].ToString().Trim();
                string grossweight = dr["毛重"].ToString().Trim();
                string cartonlong = dr["长"].ToString().Trim();
                string cartonwidth = dr["宽"].ToString().Trim();
                string cartonheight = dr["高"].ToString().Trim();
                string startnum = dr["开始序列号"].ToString().Trim();
                string endnum = dr["截止序列号"].ToString().Trim();
                string baocode = dr["包装代码"].ToString().Trim();
                string wai_code = dr["外箱代码"].ToString().Trim();
                string po = dr["订单号码"].ToString().Trim();
                string Skucode = dr["Sku号码"].ToString().Trim();
                string Color = dr["Color"].ToString().Trim();
                string ScanID = dr["扫描ID"].ToString().Trim();
                string markend = dr["喷码完成"].ToString().Trim();
                string labelend = dr["贴标完成"].ToString().Trim();
                string Cartoncount = dr["Cartoncount"].ToString().Trim();
                string GAPSideIsshieid = dr["GAPSideIsshieid"].ToString().Trim();
                string finishmarknum = CsGetData.GetGAPMarkingNumber(dr["packing_guid"].ToString().Trim());
                #endregion
                Dictionary<string, string> DicMarking = new Dictionary<string, string>();
                if (wai_code.ToUpper() != "C4" && wai_code.ToUpper() != "C6")
                {
                    #region //国外普通单喷码信息
                    DicMarking["订单号码"] = dr["订单号码"].ToString().Trim().Substring(0, 5) + "    " + dr["订单号码"].ToString().Trim().Substring(5, dr["订单号码"].ToString().Length - 5);
                    // CsPakingData.DicMarking["订单号码1"] = dr["订单号码"].ToString().Trim().Substring(0, 5);
                    //CsPakingData.DicMarking["订单号码2"] = dr["订单号码"].ToString().Trim().Substring(5, 2);
                    string a = dr["内包装计数"].ToString().Trim();
                    if (!string.IsNullOrEmpty(dr["内包装计数"].ToString().Trim()) && dr["内包装计数"].ToString().Trim() != "0")//是多条装一包
                    {
                        DicMarking["SKU/Item"] = dr["包装代码"].ToString().Trim();
                        int Unit_v = int.Parse(dr["数量"].ToString().Trim()) / int.Parse(dr["箱数"].ToString().Trim());
                        DicMarking["Unit/Prepack"] = Unit_v + "/" + dr["内包装计数"].ToString().Trim();
                    }
                    else//一条装一包
                    {
                        int len = dr["Sku号码"].ToString().Trim().Length;
                        if (len < 14)
                        {
                            for (int i = 0; i < 14 - len + 1; i++)
                            {
                                dr["Sku号码"] = dr["Sku号码"].ToString().Trim() + "0";
                            }
                        }
                        DicMarking["SKU/Item"] = dr["Sku号码"].ToString().Trim().Substring(0, 9) + "-" + dr["Sku号码"].ToString().Trim().Substring(9, 4);
                        DicMarking["Unit/Prepack"] = dr["内部包装的项目数量"].ToString().Trim() + "/0";
                    }
                    int Carton_v = int.Parse(dr["从"].ToString().Trim()) + int.Parse(finishmarknum);
                    DicMarking["Carton"] = Carton_v.ToString() + "        "+ dr["PoQty"].ToString().Trim();
                    DicMarking["从"] = dr["从"].ToString().Trim();
                    DicMarking["到"] = dr["到"].ToString().Trim();
                    DicMarking["箱数"] = dr["箱数"].ToString().Trim();
                    DicMarking["订单总箱数"] = dr["PoQty"].ToString().Trim();
                    DicMarking["END"] = "";
                    DicMarking["po"] = dr["订单号码"].ToString().Trim();
                    DicMarking["Color"] = dr["Color"].ToString().Trim();
                    DicMarking["开始序列号"] = dr["开始序列号"].ToString().Trim();
                    DicMarking["Carton1"] = Carton_v.ToString();//上次结束时的喷码计数
                    DicMarking["Carton2"] = dr["到"].ToString().Trim();
                    DicMarking["Cartoncount"] = dr["Cartoncount"].ToString().Trim();//上次结束时的喷码计数
                    DicMarking["CurrentPackingNum"] = dr["CurrentPackingNum"].ToString().Trim();
                    DicMarking["packing_guid"] = dr["packing_guid"].ToString().Trim();
                    DicMarking["长"] = dr["长"].ToString().Trim();
                    DicMarking["宽"] = dr["宽"].ToString().Trim();
                    DicMarking["高"] = dr["高"].ToString().Trim();
                    DicMarking["外箱代码"] = dr["外箱代码"].ToString().Trim();
                    DicMarking["PrintIsshieid"] = dr["PrintIsshieid"].ToString().Trim();
                    DicMarking["GAPIsPrintRun"] = dr["GAPIsPrintRun"].ToString().Trim();
                    DicMarking["GAPSideIsshieid"] = GAPSideIsshieid;
                    DicMarking["MarkingIsshieid"] = dr["MarkingIsshieid"].ToString().Trim();
                    DicMarking["IsUnpacking"] = dr["IsUnpacking"].ToString().Trim();
                    DicMarking["IsPackingRun"] = dr["IsPackingRun"].ToString().Trim();
                    DicMarking["IsYaMaHaRun"] = dr["IsYaMaHaRun"].ToString().Trim();
                    DicMarking["IsSealingRun"] = dr["IsSealingRun"].ToString().Trim();
                    DicMarking["IsPalletizingRun"] = dr["IsPalletizingRun"].ToString().Trim();
                    DicMarking["IsScanRun"] = dr["IsScanRun"].ToString().Trim();
                    DicMarking["PartitionType"] = dr["PartitionType"].ToString().Trim();
                    DicMarking["HookDirection"] = dr["HookDirection"].ToString().Trim();
                    DicMarking["FinishPalletizingNum"] = dr["FinishPalletizingNum"].ToString().Trim();
                    DicMarking["BatchNo"] = dr["BatchNo"].ToString().Trim();
                    if (wai_code.Trim().ToUpper() == "C1")
                    {
                        DicMarking["上下距离"] = "88";
                        DicMarking["左右距离"] = "190";
                        DicMarking["纸箱大小类型"] = "1";
                    }
                    else if (wai_code.Trim().ToUpper() == "C2")
                    {
                        DicMarking["上下距离"] = "80";
                        DicMarking["左右距离"] = "196";
                        DicMarking["纸箱大小类型"] = "1";
                    }
                    else if (wai_code.Trim().ToUpper() == "C3")
                    {
                        DicMarking["上下距离"] = "67";
                        DicMarking["左右距离"] = "123";
                        DicMarking["纸箱大小类型"] = "2";
                    }
                    CsPakingData.M_list_DicMarkingNONet.Add(DicMarking);
                    #endregion
                }
                else if (wai_code.ToUpper() == "C4" || wai_code.ToUpper() == "C6")
                {
                    #region //国外网单喷码信息
                    int Carton_v = int.Parse(dr["从"].ToString().Trim()) + int.Parse(finishmarknum);
                    //int Carton_v = int.Parse(dr["从"].ToString().Trim()) + int.Parse(dr["Cartoncount"].ToString().Trim()) - 1;
                    DicMarking["Carton"] = Carton_v.ToString() + "                " + dr["PoQty"].ToString().Trim();
                    DicMarking["订单号码"] = dr["订单号码"].ToString().Trim().Substring(0, 5) + "   " + dr["订单号码"].ToString().Trim().Substring(5, dr["订单号码"].ToString().Length - 5);
                    //CsPakingData.M_DicMarking_Netlist["订单号码1"] = dr["订单号码"].ToString().Trim().Substring(0, 5);
                    //DicMarking["订单号码2"] = dr["订单号码"].ToString().Trim().Substring(5, 2);n
                    DicMarking["style/size"] = dr["Sku号码"].ToString().Trim().Substring(0, 9) + "        " + dr["Sku号码"].ToString().Trim().Substring(9, 4);
                    //CsPakingData.M_DicMarking_Netlist["style"] = dr["Sku号码"].ToString().Trim().Substring(0, 6);
                    //CsPakingData.M_DicMarking_Netlist["size"] = dr["Sku号码"].ToString().Trim().Substring(9, 4);
                    if (!string.IsNullOrEmpty(dr["内包装计数"].ToString().Trim()) && dr["内包装计数"].ToString().Trim() != "0")//是多条装一包
                    {
                        int Unit_v = int.Parse(dr["数量"].ToString().Trim()) / int.Parse(dr["箱数"].ToString().Trim());
                        DicMarking["qty"] = Unit_v + "/" + dr["内包装计数"].ToString().Trim();
                    }
                    else//一条装一包
                    {
                        DicMarking["qty"] = dr["内部包装的项目数量"].ToString().Trim();
                    }
                    DicMarking["GrossWeight"] = dr["毛重"].ToString().Trim();
                    DicMarking["NetWeight"] = dr["净重"].ToString().Trim();
                    DicMarking["从"] = dr["从"].ToString().Trim();
                    DicMarking["到"] = dr["到"].ToString().Trim();
                    DicMarking["箱数"] = dr["箱数"].ToString().Trim();
                    DicMarking["订单总箱数"] = dr["PoQty"].ToString().Trim();
                    DicMarking["END"] = "";
                    DicMarking["po"] = dr["订单号码"].ToString().Trim();
                    DicMarking["Color"] = dr["Color"].ToString().Trim();
                    DicMarking["Size"] = dr["Size"].ToString().Trim();
                    DicMarking["StartNum"] = Carton_v.ToString();
                    DicMarking["开始序列号"] = dr["开始序列号"].ToString().Trim();
                    DicMarking["Carton1"] = Carton_v.ToString();//上次结束时的喷码计数
                    DicMarking["Carton2"] = dr["到"].ToString().Trim();
                    DicMarking["Cartoncount"] = dr["Cartoncount"].ToString().Trim();//上次结束时的喷码计数
                    DicMarking["CurrentPackingNum"] = dr["CurrentPackingNum"].ToString().Trim();
                    DicMarking["packing_guid"] = dr["packing_guid"].ToString().Trim();
                    DicMarking["长"] = dr["长"].ToString().Trim();
                    DicMarking["宽"] = dr["宽"].ToString().Trim();
                    DicMarking["高"] = dr["高"].ToString().Trim();
                    DicMarking["外箱代码"] = dr["外箱代码"].ToString().Trim();
                    DicMarking["PrintIsshieid"] = dr["PrintIsshieid"].ToString().Trim();
                    DicMarking["GAPIsPrintRun"] = dr["GAPIsPrintRun"].ToString().Trim();
                    DicMarking["MarkingIsshieid"] = dr["MarkingIsshieid"].ToString().Trim();
                    DicMarking["IsUnpacking"] = dr["IsUnpacking"].ToString().Trim();
                    DicMarking["IsPackingRun"] = dr["IsPackingRun"].ToString().Trim();
                    DicMarking["IsYaMaHaRun"] = dr["IsYaMaHaRun"].ToString().Trim();
                    DicMarking["IsSealingRun"] = dr["IsSealingRun"].ToString().Trim();
                    DicMarking["IsPalletizingRun"] = dr["IsPalletizingRun"].ToString().Trim();
                    DicMarking["IsScanRun"] = dr["IsScanRun"].ToString().Trim();
                    DicMarking["PartitionType"] = dr["PartitionType"].ToString().Trim();
                    DicMarking["HookDirection"] = dr["HookDirection"].ToString().Trim();
                    DicMarking["FinishPalletizingNum"] = dr["FinishPalletizingNum"].ToString().Trim();
                    DicMarking["TiaoMaVal"] = dr["TiaoMaVal"].ToString().Trim();
                    DicMarking["GAPSideStartNum"] = dr["GAPSideStartNum"].ToString().Trim();
                    DicMarking["sku"] = dr["sku"].ToString().Trim();
                    DicMarking["BatchNo"] = dr["BatchNo"].ToString().Trim();
                    if (wai_code.Trim().ToUpper() == "C4")
                    {
                        DicMarking["上下距离"] = "110";
                        DicMarking["左右距离"] = "200";
                        DicMarking["纸箱大小类型"] = "1";
                    }
                    else if (wai_code.Trim().ToUpper() == "C6")
                    {
                        DicMarking["上下距离"] = "82";
                        DicMarking["左右距离"] = "172";
                        DicMarking["纸箱大小类型"] = "2";
                    }
                    CsPakingData.M_list_DicMarkingNet.Add(DicMarking);
                    #endregion
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }
        /// <summary>
        /// 获取纸箱参数的
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int GetCatornParameter(string str)
        {
            switch (str.Trim())
            {
                case "不喷色号":
                    return 1;
                case "喷色号":
                    return 2;
                case "大":
                    return 1;
                case "小":
                    return 2;
                case "旋转":
                    return 1;
                case "不旋转":
                    return 2;
                case "小标签":
                    return 1;
                case "大标签":
                    return 2;
                case "A隔板":
                    return 1;
                case "B隔板":
                    return 2;
                case "国内":
                    return 3;
                case "国外":
                    return 4;
                case "正":
                    return 0;
                case "反":
                    return 1;
                case "不允许":
                    return 1;
                case "允许":
                    return 2;
                case "运行":
                    return 1;
                case "不运行":
                    return 2;
                case "掉头":
                    return 1;
                case "不掉头":
                    return 0;
                case "是":
                    return 1;
                case "否":
                    return 0;
                default: break;
            }
            return 0;
        }
        /// <summary>
        /// 手动执行更新喷码Carton编号并加入打印标签队列
        /// </summary>
        public static void ManualUpdatePackingPage()
        {
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1*FROM (SELECT 外箱代码 AS '单据类型',Entrytime FROM dbo.MarkingLineUp WHERE DeviceName='{0}' UNION ALL SELECT potype AS '单据类型',Entrytime FROM dbo.YYKMarkingLineUp WHERE DeviceName='{0}') AS LINE ORDER BY LINE.Entrytime", CSReadPlc.DevName.Trim()));
                    if (dt.Rows.Count > 0)
                    {
                        string potype = dt.Rows[0]["单据类型"].ToString().Trim();
                        if (potype.ToUpper().Contains("C4") || potype.ToUpper().Contains("C6"))
                        {
                            if (CsPakingData.M_list_DicMarkingNet.ToList().Count > 0)
                            {
                                int CurrentPackingNum = int.Parse(CsPakingData.GetMarking_Net("CurrentPackingNum"));
                                int Cartoncount = int.Parse(CsPakingData.GetMarking_Net("Cartoncount"));
                                string po = CsPakingData.GetMarking_Net("po");
                                string Poguid = CsPakingData.GetMarking_Net("packing_guid");
                                //更新喷码完成计数
                                string finishmarknumber = CsGetData.UpdateGAPMarkingNumber(CsPakingData.GetMarking_Net("packing_guid"));
                                int EndNum = int.Parse(CsPakingData.GetMarking_Net("到"));
                                int StartNum = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_Net("从"));
                                if (Cartoncount == 1)
                                {
                                    CsPakingData.M_list_Dic_GAPPalletizing.Add(CsPakingData.M_list_DicMarkingNet.ToList()[0]);//加入码垛队列
                                    Cslogfun.WriteTomarkingLog("本次喷码订单:" + po + ",从:" + CsPakingData.GetMarking_Net("从") + ",加入码垛队列集合中");
                                }
                                //喷码打印计数
                                if (Cartoncount < CurrentPackingNum)
                                {
                                    CsPakingData.M_list_DicMarkingNet.ToList()[0]["finishmarknumber"] = finishmarknumber;
                                    CsPakingData.M_list_DicMarkingNet.ToList()[0]["Cartoncount"] = (int.Parse(CsPakingData.GetMarking_Net("Cartoncount")) + 1).ToString();
                                    CsPakingData.M_list_DicMarkingNet.ToList()[0]["Carton1"] = int.Parse(finishmarknumber).ToString() + int.Parse(CsPakingData.GetMarking_Net("从"));
                                    CsPakingData.M_list_DicMarkingNet.ToList()[0]["Carton"] = int.Parse(finishmarknumber) + int.Parse(CsPakingData.GetMarking_Net("从")) + "                " + CsPakingData.GetMarking_Net("订单总箱数");
                                    string sqlupdate = string.Format("UPDATE dbo.MarkingLineUp SET Cartoncount=Cartoncount+1 WHERE 订单号码='{0}' AND Color='{1}' AND 开始序列号='{2}'", CsPakingData.GetMarking_Net("po"), CsPakingData.GetMarking_Net("Color"), CsPakingData.GetMarking_Net("开始序列号"));
                                    CsFormShow.GoSqlUpdateInsert(sqlupdate);
                                    if (!CsPakingData.GetMarking_Net("GAPIsPrintRun").Contains("不运行"))//是否屏蔽打印
                                    {
                                        int page = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_Net("从"));
                                        int sort = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_Net("开始序列号"));
                                        setprint(Poguid, po, page, sort);//加入打印列队
                                    }
                                    //更新数据库队列中下次喷码的箱号Carton

                                }
                                else if (Cartoncount == CurrentPackingNum || Cartoncount == CurrentPackingNum || EndNum == StartNum || StartNum > EndNum)
                                {
                                    Cslogfun.WriteToCartornLog("当前订单喷码结束，订单：" + po + "从：" + CsPakingData.GetMarking_Net("从") + ",当前箱号：" + StartNum.ToString());
                                    CsPakingData.M_list_DicMarkingNet.ToList()[0]["finishmarknumber"] = finishmarknumber;
                                    if (!CsPakingData.GetMarking_Net("GAPIsPrintRun").Contains("不运行"))//是否屏蔽打印
                                    {
                                        int page = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_Net("从"));
                                        int sort = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_Net("开始序列号"));
                                        setprint(Poguid, po, page, sort);//加入打印列队
                                    }
                                    //删除已完成的喷码记录
                                    DeleteMarkingLineUpFromSql(CsPakingData.M_list_DicMarkingNet.ToList()[0]);
                                    CsPakingData.M_list_DicMarkingNet.Remove(CsPakingData.M_list_DicMarkingNet.ToList()[0]);//删除列队缓存中的本次
                                    CsInitialization.UpdateGAPLineUp();
                                }
                            }
                        }
                        else if (potype.ToUpper().Contains("C1") || potype.ToUpper().Contains("C2") || potype.ToUpper().Contains("C3") || potype.ToUpper().Contains("C5"))
                        {

                            if (CsPakingData.M_list_DicMarkingNONet.ToList().Count > 0)
                            {
                                int CurrentPackingNum = int.Parse(CsPakingData.GetMarking_NONet("CurrentPackingNum"));
                                int Cartoncount = int.Parse(CsPakingData.GetMarking_NONet("Cartoncount"));
                                string po = CsPakingData.GetMarking_NONet("po");
                                string Poguid = CsPakingData.GetMarking_NONet("packing_guid");
                                string finishmarknumber = CsGetData.UpdateGAPMarkingNumber(CsPakingData.GetMarking_NONet("packing_guid"));
                                int EndNum = int.Parse(CsPakingData.GetMarking_NONet("到"));
                                int StartNum = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_NONet("从"));
                                if (Cartoncount == 1)
                                {
                                    CsPakingData.M_list_Dic_GAPPalletizing.Add(CsPakingData.M_list_DicMarkingNONet.ToList()[0]);//加入码垛队列
                                    Cslogfun.WriteTomarkingLog("本次喷码订单:" + po + ",从:" + CsPakingData.GetMarking_Net("从") + ",加入码垛队列集合中");
                                }
                                //喷码打印计数
                                if (Cartoncount < CurrentPackingNum)
                                {
                                    //更新喷码完成计数
                                    CsPakingData.M_list_DicMarkingNONet.ToList()[0]["finishmarknumber"] = finishmarknumber;
                                    //更新数据库队列中下次喷码的箱号Carton
                                    CsPakingData.M_list_DicMarkingNONet.ToList()[0]["Cartoncount"] = (int.Parse(CsPakingData.GetMarking_NONet("Cartoncount")) + 1).ToString();
                                    CsPakingData.M_list_DicMarkingNONet.ToList()[0]["Carton1"] = int.Parse(finishmarknumber).ToString() + int.Parse(CsPakingData.GetMarking_NONet("从"));
                                    CsPakingData.M_list_DicMarkingNONet.ToList()[0]["Carton"] = int.Parse(finishmarknumber) + int.Parse(CsPakingData.GetMarking_NONet("从")) + "        " + CsPakingData.GetMarking_NONet("订单总箱数");
                                    string sqlupdate = string.Format("UPDATE dbo.MarkingLineUp SET Cartoncount=Cartoncount+1 WHERE 订单号码='{0}' AND Color='{1}' AND 开始序列号='{2}'", CsPakingData.GetMarking_NONet("po"), CsPakingData.GetMarking_NONet("Color"), CsPakingData.GetMarking_NONet("开始序列号"));
                                    CsFormShow.GoSqlUpdateInsert(sqlupdate);
                                    if (!CsPakingData.GetMarking_NONet("GAPIsPrintRun").Contains("不运行"))//是否屏蔽打印
                                    {
                                        int page = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_NONet("从"));
                                        int sort = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_NONet("开始序列号"));
                                        setprint(Poguid, po, page, sort);//加入打印列队
                                    }
                                }
                                else if (Cartoncount == CurrentPackingNum || Cartoncount > CurrentPackingNum || EndNum == StartNum || StartNum > EndNum)
                                {
                                    Cslogfun.WriteToCartornLog("当前订单喷码结束，订单：" + po + "从：" + CsPakingData.GetMarking_NONet("从") + ",当前箱号：" + StartNum.ToString());
                                    CsPakingData.M_list_DicMarkingNONet.ToList()[0]["finishmarknumber"] = finishmarknumber;
                                    if (!CsPakingData.GetMarking_NONet("GAPIsPrintRun").Contains("不运行"))//是否屏蔽打印
                                    {
                                        int page = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_NONet("从"));
                                        int sort = int.Parse(finishmarknumber) - 1 + int.Parse(CsPakingData.GetMarking_NONet("开始序列号"));
                                        setprint(Poguid, po, page, sort);//加入打印列队
                                    }
                                    //删除已完成的喷码记录
                                    DeleteMarkingLineUpFromSql(CsPakingData.M_list_DicMarkingNONet.ToList()[0]);
                                    CsPakingData.M_list_DicMarkingNONet.Remove(CsPakingData.M_list_DicMarkingNONet.ToList()[0]);//删除列队缓存中的本次
                                    CsInitialization.UpdateGAPLineUp();
                                }
                            }
                        }
                        else if (potype.ToUpper().Contains("GU") || potype.ToUpper().Contains("UNIQLO"))
                        {
                            if (CsPakingData.M_list_Dic_colorcode.ToList().Count > 0)
                            {
                                int total = int.Parse(CsPakingData.GetMarking_Colorcode("Quantity"));
                                int qty = int.Parse(CsPakingData.GetMarking_Colorcode("qty"));
                                string Order_No = CsPakingData.GetMarking_Colorcode("Order_No");
                                string Color_Code = CsPakingData.GetMarking_Colorcode("Color_Code");
                                string Warehouse = CsPakingData.GetMarking_Colorcode("Warehouse");
                                string Set_Code = CsPakingData.GetMarking_Colorcode("Set_Code");
                                string YK_guid = CsPakingData.GetMarking_Colorcode("YK_guid");
                                if (CsPakingData.M_list_Dic_colorcode.ToList()[0].ToList().Count > 0)
                                {
                                    UpdateYYKGUMarkingNumber(Order_No, Set_Code, Warehouse, YK_guid, qty.ToString(), total.ToString());
                                    CsPakingData.M_list_Dic_colorcode.ToList()[0]["finishmarknumberTotal"] = (int.Parse(CsPakingData.GetMarking_Colorcode("finishmarknumberTotal")) + 1).ToString();
                                    if (qty == 1)
                                    {
                                        CsPakingData.M_list_Dic_YYKGUPalletizing.Add(CsPakingData.M_list_Dic_colorcode.ToList()[0]);//加入码垛队列
                                        Cslogfun.WriteTomarkingLog("本次喷码订单:" + Order_No + ",Set_Code:" + Set_Code + ",加入码垛队列集合中");
                                    }
                                    //喷码打印计数
                                    if (qty < total)
                                    {
                                        CsPakingData.M_list_Dic_colorcode.ToList()[0]["qty"] = (int.Parse(CsPakingData.GetMarking_Colorcode("qty")) + 1).ToString();
                                        //更新数据库队列中下次喷码的箱号Carton
                                        string sqlupdate = string.Format("UPDATE dbo.YYKMarkingLineUp SET Cartoncount='{0}' WHERE Order_No='{1}' AND Color_Code='{2}' AND Warehouse='{3}'AND Set_Code='{4}'", CsPakingData.GetMarking_Colorcode("qty"), CsPakingData.GetMarking_Colorcode("Order_No"), CsPakingData.GetMarking_Colorcode("Color_Code"), CsPakingData.GetMarking_Colorcode("Warehouse"), CsPakingData.GetMarking_Colorcode("Set_Code"));
                                        CsFormShow.GoSqlUpdateInsert(sqlupdate);
                                    }
                                    else if (qty == total)
                                    {
                                        Cslogfun.WriteToCartornLog("当前订单喷码结束，订单：" + Order_No + ",Warehouse:" + Warehouse + ",Set_Code:" + Set_Code + ",当前第：" + qty.ToString() + "箱");
                                        CsPakingData.M_list_Dic_colorcode.ToList()[0]["END"] = "完成";
                                        //删除已完成的喷码记录
                                        DeleteYYKMarkingLineUpFromSql(CsPakingData.M_list_Dic_colorcode.ToList()[0]);
                                        CsPakingData.M_list_Dic_colorcode.Remove(CsPakingData.M_list_Dic_colorcode.ToList()[0]);//删除列队缓存中的本次
                                        CsInitialization.UpdateYYKGULineUp();
                                    }
                                    Cslogfun.WriteTomarkingLog("更新一次优衣库/GU喷码和打印记录qty：" + qty + ",total:" + total);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }
        public bool JudgeChangeConTent(int MarkCount__, string val_)
        {
            try
            {
                bool iskey = M_Dic_MarkingConTent.ContainsKey(MarkCount__);
                if (iskey)
                {
                    string content_v = M_Dic_MarkingConTent[MarkCount__];
                    if (content_v == val_)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    M_Dic_MarkingConTent.Add(MarkCount__, val_);
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }
    }
}
