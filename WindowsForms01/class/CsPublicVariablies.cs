﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace WindowsForms01
{
    public class CsPublicVariablies
    {
        public struct Marking
        {
            /// <summary>
            /// paking表guid
            /// </summary>
            private static string po_guid;
            public static string Po_guid
            {
                get
                {
                    if (string.IsNullOrEmpty(po_guid))
                    {
                        po_guid = "";
                    }
                    return po_guid;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        po_guid = value;
                }
            }
            /// <summary>
            /// 排单表GUID
            /// </summary>
            private static string schedulingytable_guid;
            public static string SchedulingyTable_guid
            {
                get
                {
                    if (string.IsNullOrEmpty(schedulingytable_guid))
                    {
                        schedulingytable_guid = "";
                    }
                    return schedulingytable_guid;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        schedulingytable_guid = value;
                }
            }
            /// <summary>
            /// paking表guid
            /// </summary>
            private static string potype;
            public static string PoType
            {
                get
                {
                    if (string.IsNullOrEmpty(potype))
                    {
                        potype = "";
                    }
                    return potype;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        potype = value;
                }
            }
            /// <summary>
            /// 起始箱号
            /// </summary>
            private static int startnum;
            public static int StartNum
            {
                get
                {
                    if (string.IsNullOrEmpty(startnum.ToString()))
                    {
                        startnum = -1;
                    }
                    return startnum;
                }
                set { startnum = value; }
            }
            /// <summary>
            /// 当前箱号计数
            /// </summary>
            private static int cartornnum;
            public static int CartornNum
            {
                get
                {
                    if (string.IsNullOrEmpty(cartornnum.ToString()))
                    {
                        cartornnum = -1;
                    }
                    return cartornnum;
                }
                set
                { cartornnum = value; }
            }
            /// <summary>
            /// 当前箱号
            /// </summary>
            private static int cartorn;
            public static int Cartorn
            {
                get
                {
                    cartorn = StartNum + CartornNum - 1;
                    return cartorn;
                }
            }
            /// <summary>
            /// 喷码单号
            /// </summary>
            private static string po;
            public static string Po
            {
                get
                {
                    if (string.IsNullOrEmpty(OrderNo))
                    {
                        return "";
                    }
                    else
                    {
                        po = OrderNo.ToString().Trim();
                        po = po.Substring(0, 5) + "    " + po.Substring(5, po.Length - 5);
                        return po;
                    }
                }
            }
            /// <summary>
            /// 单号
            /// </summary>
            private static string orderno = "";
            public static string OrderNo
            {
                get
                {
                    if (string.IsNullOrEmpty(orderno))
                    {
                        orderno = "";
                    }
                    return orderno;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        orderno = value;
                }
            }
            /// <summary>
            /// 条码
            /// </summary>
            private static string tiaomaval = "";
            public static string TiaoMaVal
            {
                get
                {
                    if (string.IsNullOrEmpty(tiaomaval))
                    {
                        tiaomaval = "";
                    }
                    return tiaomaval;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        tiaomaval = value;
                }
            }
            /// <summary>
            /// 网单标签起始号
            /// </summary>
            private static string gapsidestartnum = "";
            public static string GAPSideStartNum
            {
                get
                {
                    if (string.IsNullOrEmpty(gapsidestartnum))
                    {
                        gapsidestartnum = "";
                    }
                    return gapsidestartnum;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        gapsidestartnum = value;
                }
            }
            /// <summary>
            /// 颜色
            /// </summary>
            private static string color = "";
            public static string Color
            {
                get
                {
                    if (string.IsNullOrEmpty(color))
                    {
                        color = "";
                    }
                    return color;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        color = value;
                }
            }
            /// <summary>
            /// 颜色
            /// </summary>
            private static string size = "";
            public static string Size
            {
                get
                {
                    if (string.IsNullOrEmpty(size))
                    {
                        size = "";
                    }
                    return size;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        size = value;
                }
            }
            /// <summary>
            /// 订单箱数
            /// </summary>
            private static int poqty;
            public static int PoQty
            {
                get
                {
                    if (string.IsNullOrEmpty(poqty.ToString()))
                    {
                        poqty = -1;
                    }
                    return poqty;
                }
                set { poqty = value; }
            }
            /// <summary>
            /// 外箱代码
            /// </summary>
            private static string waitcode;
            public static string WaiCode
            {
                get
                {
                    if (string.IsNullOrEmpty(waitcode))
                    {
                        waitcode = "";
                    }
                    return waitcode;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        waitcode = value;
                }
            }
            /// <summary>
            /// Sku号码
            /// </summary>
            private static string sku号码;
            public static string Sku号码
            {
                get
                {
                    if (string.IsNullOrEmpty(sku号码))
                    {
                        sku号码 = "";
                    }
                    return sku号码;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        sku号码 = value;
                }
            }
            /// <summary>
            /// 内部包装的项目数量
            /// </summary>
            private static string packing内部包装的项目数量;
            public static string Packing内部包装的项目数量
            {
                get
                {
                    if (string.IsNullOrEmpty(packing内部包装的项目数量))
                    {
                        packing内部包装的项目数量 = "";
                    }
                    return packing内部包装的项目数量;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        packing内部包装的项目数量 = value;
                }
            }
            /// <summary>
            /// 内包装计数
            /// </summary>
            private static string packing内包装计数;
            public static string Packing内包装计数
            {
                get
                {
                    if (string.IsNullOrEmpty(packing内包装计数))
                    {
                        packing内包装计数 = "";
                    }
                    return packing内包装计数;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        packing内包装计数 = value;
                }
            }
            /// <summary>
            /// 内包装计数
            /// </summary>
            private static string paking包装代码;
            public static string Packing包装代码
            {
                get
                {
                    if (string.IsNullOrEmpty(paking包装代码))
                    {
                        paking包装代码 = "";
                    }
                    return paking包装代码;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        paking包装代码 = value;
                }
            }
            /// <summary>
            /// 从
            /// </summary>
            private static int packing从;
            public static int Packing从
            {
                get
                {
                    if (string.IsNullOrEmpty(packing从.ToString()))
                    {
                        packing从 = -1;
                    }
                    return packing从;
                }
                set { packing从 = value; }
            }
            /// <summary>
            /// 数量
            /// </summary>
            private static int packing开始序列号;
            public static int Packing开始序列号
            {
                get
                {
                    if (string.IsNullOrEmpty(packing开始序列号.ToString()))
                    {
                        packing开始序列号 = -1;
                    }
                    return packing开始序列号;
                }
                set { packing开始序列号 = value; }
            }
            /// <summary>
            /// 数量
            /// </summary>
            private static int packing当前序列号;
            public static int Packing当前序列号
            {
                get
                {
                    if (string.IsNullOrEmpty(packing当前序列号.ToString()))
                    {
                        packing当前序列号 = -1;
                    }
                    return packing当前序列号;
                }
                set { packing当前序列号 = value; }
            }
            /// <summary>
            /// 数量
            /// </summary>
            private static int packing数量;
            public static int Packing数量
            {
                get
                {
                    if (string.IsNullOrEmpty(packing数量.ToString()))
                    {
                        packing数量 = -1;
                    }
                    return packing数量;
                }
                set { packing数量 = value; }
            }
            /// <summary>
            /// 箱数
            /// </summary>
            private static int packing箱数;
            public static int Packing箱数
            {
                get
                {
                    if (string.IsNullOrEmpty(packing箱数.ToString()))
                    {
                        packing箱数 = -1;
                    }
                    return packing箱数;
                }
                set { packing箱数 = value; }
            }
            /// <summary>
            /// SKU/Item
            /// </summary>
            private static string skuitem = "";
            public static string SkuItem
            {
                get
                {
                    if (WaiCode.ToUpper() != "C4" && WaiCode.ToUpper() != "C6")
                    {
                        if (!string.IsNullOrEmpty(Packing内包装计数) && Packing内包装计数 != "0")//是多条装一包
                        {
                            skuitem = Packing包装代码;
                        }
                        else
                        {
                            int len = Sku号码.Length;
                            string strsku = Sku号码;
                            if (len < 14)
                            {
                                for (int i = 0; i < 14 - len; i++)
                                {
                                    strsku = strsku.Trim() + "0";
                                }
                            }
                            skuitem = strsku.ToString().Trim().Substring(0, 9) + "-" + strsku.ToString().Trim().Substring(9, 4);
                        }
                    }
                    return skuitem;
                }
            }
            /// <summary>
            /// Unit/Prepack
            /// </summary>
            private static string unitprepack = "";
            public static string UnitPrepack
            {
                get
                {
                    if (WaiCode.ToUpper() != "C4" && WaiCode.ToUpper() != "C6")
                    {
                        if (!string.IsNullOrEmpty(Packing内包装计数) && Packing内包装计数 != "0")//是多条装一包
                        {
                            int Unit_v = Packing数量 / Packing箱数;
                            unitprepack = Unit_v.ToString() + "/" + Packing内包装计数.ToString().Trim();
                        }
                        else
                        {
                            unitprepack = Packing内部包装的项目数量.ToString().Trim() + "/0";
                        }
                    }
                    return unitprepack;
                }
            }
            /// <summary>
            /// style/size
            /// </summary>
            private static string stylesize = "";
            public static string StyleSize
            {
                get
                {
                    if (WaiCode.ToUpper() == "C4" || WaiCode.ToUpper() == "C6")
                    {
                        stylesize = Sku号码.ToString().Trim().Substring(0, 9) + "              " + Sku号码.ToString().Trim().Substring(9, 4);
                    }
                    return stylesize;
                }
            }
            /// <summary>
            /// 网单箱内件数
            /// </summary>
            private static string qty = "0";
            public static string Qty
            {
                get
                {
                    if (WaiCode.ToUpper() == "C4" || WaiCode.ToUpper() == "C6")
                    {
                        if (!string.IsNullOrEmpty(Packing内包装计数) && Packing内包装计数.ToString().Trim() != "0")//是多条装一包
                        {
                            int Unit_v = Packing数量 / Packing箱数;
                            qty = Unit_v.ToString() + "/" + Packing内包装计数;
                        }
                        else//一条装一包
                        {
                            qty = Packing内部包装的项目数量;
                        }
                    }
                    return qty;
                }
            }
            /// <summary>
            /// 毛重
            /// </summary>
            private static string grossweight;
            public static string GrossWeight
            {
                get
                {
                    if (string.IsNullOrEmpty(grossweight))
                    {
                        grossweight = "";
                    }
                    return grossweight;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        grossweight = value;
                }
            }
            /// <summary>
            /// 净重
            /// </summary>
            private static string netweight;
            public static string NetWeight
            {
                get
                {
                    if (string.IsNullOrEmpty(netweight))
                    {
                        netweight = "";
                    }
                    return netweight;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        netweight = value;
                }
            }
            /// <summary>
            /// 是否打印
            /// </summary>
            private static string isprint;
            public static string IsPrint
            {
                get
                {
                    if (string.IsNullOrEmpty(isprint))
                    {
                        isprint = "";
                    }
                    return isprint;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        isprint = value;
                }
            }
            /// <summary>
            /// 是否打印
            /// </summary>
            private static string set_code;
            public static string Set_Code
            {
                get
                {
                    if (string.IsNullOrEmpty(set_code))
                    {
                        set_code = "";
                    }
                    return set_code;
                }
                set { set_code = value; }
            }
            /// <summary>
            /// 是否打印
            /// </summary>
            private static string warehouse;
            public static string Warehouse
            {
                get
                {
                    if (string.IsNullOrEmpty(warehouse))
                    {
                        warehouse = "";
                    }
                    return warehouse;
                }
                set { warehouse = value; }
            }
        }
        public struct PrintData
        {
            /// <summary>
            /// paking表guid
            /// </summary>
            private static string po_guid;
            public static string Po_guid
            {
                get
                {
                    if (string.IsNullOrEmpty(po_guid))
                    {
                        po_guid = "";
                    }
                    return po_guid;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        po_guid = value;
                }
            }
            /// <summary>
            /// 排单表GUID
            /// </summary>
            private static string schedulingytable_guid;
            public static string SchedulingyTable_guid
            {
                get
                {
                    if (string.IsNullOrEmpty(schedulingytable_guid))
                    {
                        schedulingytable_guid = "";
                    }
                    return schedulingytable_guid;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        schedulingytable_guid = value;
                }
            }
            /// <summary>
            /// paking表guid
            /// </summary>
            private static string potype;
            public static string PoType
            {
                get
                {
                    if (string.IsNullOrEmpty(potype))
                    {
                        potype = "";
                    }
                    return potype;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        potype = value;
                }
            }
            /// <summary>
            /// 起始箱号
            /// </summary>
            private static int startnum;
            public static int StartNum
            {
                get
                {
                    if (string.IsNullOrEmpty(startnum.ToString()))
                    {
                        startnum = -1;
                    }
                    return startnum; }
                set { startnum = value; }
            }
            /// <summary>
            /// 当前箱号计数
            /// </summary>
            private static int cartornnum;
            public static int CartornNum
            {
                get
                {
                    if (string.IsNullOrEmpty(cartornnum.ToString()))
                    {
                        cartornnum = -1;
                    }
                    return cartornnum; }
                set
                { cartornnum = value; }
            }
            /// <summary>
            /// 当前箱号
            /// </summary>
            private static int cartorn;
            public static int Cartorn
            {
                get
                {
                    cartorn = StartNum + CartornNum - 1;
                    return cartorn;
                }
            }
            /// <summary>
            /// 喷码单号
            /// </summary>
            private static string po;
            public static string Po
            {
                get
                {
                    if (string.IsNullOrEmpty(OrderNo))
                    {
                        return "";
                    }
                    else
                    {
                        po = OrderNo.ToString().Trim();
                        po = po.Substring(0, 5) + "    " + po.Substring(5, po.Length - 5);
                        return po;
                    }
                }
            }
            /// <summary>
            /// 单号
            /// </summary>
            private static string orderno = "";
            public static string OrderNo
            {
                get
                {
                    if (string.IsNullOrEmpty(orderno))
                    {
                        orderno = "";
                    }
                    return orderno; }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        orderno = value;
                }
            }
            /// <summary>
            /// 订单箱数
            /// </summary>
            private static int poqty;
            public static int PoQty
            {
                get
                {
                    if (string.IsNullOrEmpty(poqty.ToString()))
                    {
                        poqty = -1;
                    }
                    return poqty; }
                set { poqty = value; }
            }
            /// <summary>
            /// 外箱代码
            /// </summary>
            private static string waitcode;
            public static string WaiCode
            {
                get
                {
                    if (string.IsNullOrEmpty(waitcode))
                    {
                        waitcode = "";
                    }
                    return waitcode;
                }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        waitcode = value;
                }
            }
            /// <summary>
            /// Sku号码
            /// </summary>
            private static string sku号码;
            public static string Sku号码
            {
                get
                {
                    if (string.IsNullOrEmpty(sku号码))
                    {
                        sku号码 = "";
                    }
                    return sku号码; }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        sku号码 = value;
                }
            }
            /// <summary>
            /// 内部包装的项目数量
            /// </summary>
            private static string packing内部包装的项目数量;
            public static string Packing内部包装的项目数量
            {
                get
                {
                    if (string.IsNullOrEmpty(packing内部包装的项目数量))
                    {
                        packing内部包装的项目数量 = "";
                    }
                    return packing内部包装的项目数量; }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        packing内部包装的项目数量 = value;
                }
            }
            /// <summary>
            /// 内包装计数
            /// </summary>
            private static string packing内包装计数;
            public static string Packing内包装计数
            {
                get
                {
                    if (string.IsNullOrEmpty(packing内包装计数))
                    {
                        packing内包装计数 = "";
                    }
                    return packing内包装计数; }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        packing内包装计数 = value;
                }
            }
            /// <summary>
            /// 内包装计数
            /// </summary>
            private static string paking包装代码;
            public static string Packing包装代码
            {
                get
                {
                    if (string.IsNullOrEmpty(paking包装代码))
                    {
                        paking包装代码 = "";
                    }
                    return paking包装代码; }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        paking包装代码 = value;
                }
            }
            /// <summary>
            /// 从
            /// </summary>
            private static int packing从;
            public static int Packing从
            {
                get
                {
                    if (string.IsNullOrEmpty(packing从.ToString()))
                    {
                        packing从 = -1;
                    }
                    return packing从; }
                set { packing从 = value; }
            }
            /// <summary>
            /// 数量
            /// </summary>
            private static int packing开始序列号;
            public static int Packing开始序列号
            {
                get
                {
                    if (string.IsNullOrEmpty(packing开始序列号.ToString()))
                    {
                        packing开始序列号 = -1;
                    }
                    return packing开始序列号; }
                set { packing开始序列号 = value; }
            }
            /// <summary>
            /// 数量
            /// </summary>
            private static int packing当前序列号;
            public static int Packing当前序列号
            {
                get
                {
                    if (string.IsNullOrEmpty(packing当前序列号.ToString()))
                    {
                        packing当前序列号 = -1;
                    }
                    return packing当前序列号; }
                set { packing当前序列号 = value; }
            }
            /// <summary>
            /// 数量
            /// </summary>
            private static int packing数量;
            public static int Packing数量
            {
                get
                {
                    if (string.IsNullOrEmpty(packing数量.ToString()))
                    {
                        packing数量 = -1;
                    }
                    return packing数量; }
                set { packing数量 = value; }
            }
            /// <summary>
            /// 箱数
            /// </summary>
            private static int packing箱数;
            public static int Packing箱数
            {
                get
                {
                    if (string.IsNullOrEmpty(packing箱数.ToString()))
                    {
                        packing箱数 = -1;
                    }
                    return packing箱数; }
                set { packing箱数 = value; }
            }
            /// <summary>
            /// SKU/Item
            /// </summary>
            private static string skuitem = "";
            public static string SkuItem
            {
                get
                {
                    if (WaiCode.ToUpper() != "C4" && WaiCode.ToUpper() != "C6")
                    {
                        if (!string.IsNullOrEmpty(Packing内包装计数) && Packing内包装计数 != "0")//是多条装一包
                        {
                            skuitem = Packing包装代码;
                        }
                        else
                        {
                            int len = Sku号码.Length;
                            string strsku = Sku号码;
                            if (len < 14)
                            {
                                for (int i = 0; i < 14 - len; i++)
                                {
                                    strsku = strsku.Trim() + "0";
                                }
                            }
                            skuitem = strsku.ToString().Trim().Substring(0, 9) + "-" + strsku.ToString().Trim().Substring(9, 4);
                        }
                    }
                    return skuitem;
                }
            }
            /// <summary>
            /// Unit/Prepack
            /// </summary>
            private static string unitprepack = "";
            public static string UnitPrepack
            {
                get
                {
                    if (WaiCode.ToUpper() != "C4" && WaiCode.ToUpper() != "C6")
                    {
                        if (!string.IsNullOrEmpty(Packing内包装计数) && Packing内包装计数 != "0")//是多条装一包
                        {
                            int Unit_v = Packing数量 / Packing箱数;
                            unitprepack = Unit_v.ToString() + "/" + Packing内包装计数.ToString().Trim();
                        }
                        else
                        {
                            unitprepack = Packing内部包装的项目数量.ToString().Trim() + "/0";
                        }
                    }
                    return unitprepack;
                }
            }
            /// <summary>
            /// style/size
            /// </summary>
            private static string stylesize = "";
            public static string StyleSize
            {
                get
                {
                    if (WaiCode.ToUpper() == "C4" || WaiCode.ToUpper() == "C6")
                    {
                        stylesize = Sku号码.ToString().Trim().Substring(0, 9) + "              " + Sku号码.ToString().Trim().Substring(9, 4);
                    }
                    return stylesize;
                }
            }
            /// <summary>
            /// 网单箱内件数
            /// </summary>
            private static string qty = "0";
            public static string Qty
            {
                get
                {
                    if (WaiCode.ToUpper() == "C4" || WaiCode.ToUpper() == "C6")
                    {
                        if (!string.IsNullOrEmpty(Packing内包装计数) && Packing内包装计数.ToString().Trim() != "0")//是多条装一包
                        {
                            int Unit_v = Packing数量 / Packing箱数;
                            qty = Unit_v.ToString() + "/" + Packing内包装计数;
                        }
                        else//一条装一包
                        {
                            qty = Packing内部包装的项目数量;
                        }
                    }
                    return qty;
                }
            }
            /// <summary>
            /// 毛重
            /// </summary>
            private static string grossweight;
            public static string GrossWeight
            {
                get
                {
                    if (string.IsNullOrEmpty(grossweight))
                    {
                        grossweight = "";
                    }
                    return grossweight; }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        grossweight = value;
                }
            }
            /// <summary>
            /// 净重
            /// </summary>
            private static string netweight;
            public static string NetWeight
            {
                get
                {
                    if (string.IsNullOrEmpty(netweight))
                    {
                        netweight = "";
                    }
                    return netweight; }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        netweight = value;
                }
            }
            /// <summary>
            /// 是否打印
            /// </summary>
            private static string isprint;
            public static string IsPrint
            {
                get
                {
                    if (string.IsNullOrEmpty(isprint))
                    {
                        isprint = "";
                    }
                    return isprint; }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        isprint = value;
                }
            }
            /// <summary>
            /// 是否打印
            /// </summary>
            private static string set_code;
            public static string Set_Code
            {
                get
                {
                    if (string.IsNullOrEmpty(set_code))
                    {
                        set_code = "";
                    }
                    return set_code; }
                set { set_code = value; }
            }
            /// <summary>
            /// 是否打印
            /// </summary>
            private static string warehouse;
            public static string Warehouse
            {
                get
                {
                    if (string.IsNullOrEmpty(warehouse))
                    {
                        warehouse = "";
                    }
                    return warehouse; }
                set { warehouse = value; }
            }
        }

        public struct Scan
        {
            /// <summary>
            /// 条码每箱件数
            /// </summary>
            private static int qty_per_set;
            public static int Qty_per_set
            {
                get
                {
                    if (string.IsNullOrEmpty(qty_per_set.ToString()))
                    {
                        qty_per_set = -1;
                    }
                    return qty_per_set;
                }
                set { qty_per_set = value; }
            }
            /// <summary>
            /// 清理当前条码寄存器标志信号
            /// </summary>
            private static string clearnowtiaomaflag;
            public static string ClearNowTiaoMaFlag
            {
                get
                {
                    if (string.IsNullOrEmpty(clearnowtiaomaflag))
                    {
                        clearnowtiaomaflag = "";
                    }
                    return clearnowtiaomaflag;
                }
                set { clearnowtiaomaflag = value; }
            }
            /// <summary>
            /// 允许扫码信号
            /// </summary>
            private static string allowscansignal;
            public static string AllowScanSignal
            {
                get
                {
                    if (string.IsNullOrEmpty(allowscansignal))
                    {
                        allowscansignal = "";
                    }
                    return allowscansignal;
                }
                set { allowscansignal = value; }
            }
        }
        public struct Panlletzing
        {
            /// <summary>
            /// GAP码垛完成计数
            /// </summary>
            private static string gapPalletizingIsEnd;
            public static string GAPPalletizingIsEnd
            {
                get
                {
                    if (string.IsNullOrEmpty(gapPalletizingIsEnd.ToString()))
                    {
                        gapPalletizingIsEnd = "";
                    }
                    return gapPalletizingIsEnd;
                }
                set { gapPalletizingIsEnd = value; }
            }
        }
    }
}
