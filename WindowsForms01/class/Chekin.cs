﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Web.UI;

namespace WindowsForms01
{
    public class Chekin
    {
        //A.BLL.newsContent content1 = new A.BLL.newsContent();
        /// <summary>
        /// 检查是否为空并且为非数字
        /// </summary>
        /// <param name="form"></param>
        /// <param name="controls"></param>
        /// <returns></returns>
        public static bool Chekedin(Form form, List<Control> controls,string FormName_) //如果有不为空的值未填写，进一步判断是哪个值未填写，提示录入
        {

            if (form.Name == "ScanPackingForm")
            {
                for (int i = 0; i < controls.Count; i++)
                {
                    if (!IsNumeric(controls[i].Text))
                    {
                        MessageBox.Show("提示：内容不能为空或者非数字" + "(" + controls[i].Name + ")");
                        controls[i].Focus();
                        return false;
                    }
                }
            }
            else if (form.Name == "GAPMarkPositionForm")
            {
                for (int i = 0; i < controls.Count; i++)
                {
                    if (!IsInt(controls[i].Text))
                    {
                        MessageBox.Show("提示：内容不能为空或者非数字" + "(" + controls[i].Name + ")");
                        controls[i].Focus();
                        return false;
                    }
                }
            }
            else if (form.Name == "PositionForm")
            {
                for (int i = 0; i < controls.Count; i++)
                {
                    if (!IsInt(controls[i].Text))
                    {
                        MessageBox.Show("提示：内容不能为空或者非数字" + "(" + controls[i].Name + ")");
                        controls[i].Focus();
                        return false;
                    }
                }
            }
            else if (form.Name == "SetQtyForm")
            {
                for (int i = 0; i < controls.Count; i++)
                {
                    if (!IsInt(controls[i].Text))
                    {
                        MessageBox.Show("提示：内容不能为空或者非数字" + "(" + controls[i].Name + ")");
                        controls[i].Focus();
                        return false;
                    }
                }
            }
            else if (form.Name == FormName_)
            {
                for (int i = 0; i < controls.Count; i++)
                {
                    if (!IsInt(controls[i].Text))
                    {
                        MessageBox.Show("提示：内容不能为空或者非数字" + "(" + controls[i].Name + ")");
                        controls[i].Focus();
                        return false;
                    }
                }
            }

            return true;
        }
        /// <summary>
        /// 检查是否为空
        /// </summary>
        /// <param name="form"></param>
        /// <param name="controls"></param>
        /// <returns></returns>
        public static bool Chekedin_notnull(Form form, List<Control> controls, string FormName_) //如果有不为空的值未填写，进一步判断是哪个值未填写，提示录入
        {

            if (form.Name == "PositionForm")
            {
                for (int i = 0; i < controls.Count; i++)
                {
                    if (string.IsNullOrEmpty(controls[i].Text))
                    {
                        MessageBox.Show("提示：内容不能为空" + "(" + controls[i].Name + ")");
                        controls[i].Focus();
                        return false;
                    }
                }
            }
            else if (form.Name == "GAPMarkPositionForm")
            {
                for (int i = 0; i < controls.Count; i++)
                {
                    if (string.IsNullOrEmpty(controls[i].Text))
                    {
                        MessageBox.Show("提示：内容不能为空" + "(" + controls[i].Name + ")");
                        controls[i].Focus();
                        return false;
                    }
                }
            }
            else if (form.Name == "SetQtyForm")
            {
                for (int i = 0; i < controls.Count; i++)
                {
                    if (string.IsNullOrEmpty(controls[i].Text))
                    {
                        MessageBox.Show("提示：内容不能为空" + "(" + controls[i].Name + ")");
                        controls[i].Focus();
                        return false;
                    }
                }
            }
            else if (form.Name == FormName_)
            {
                for (int i = 0; i < controls.Count; i++)
                {
                    if (string.IsNullOrEmpty(controls[i].Text))
                    {
                        MessageBox.Show("提示：内容不能为空" + "(" + controls[i].Name + ")");
                        controls[i].Focus();
                        return false;
                    }
                }
            }

            return true;
        }
        //判断值不存在重新输入
        public static bool Judge_notExist(Form form, Control crl)
        {
            string where_values = "";
            string sqlwhere = "";
            A.BLL.newsContent content = new A.BLL.newsContent();
            if (form.Name == "EmployesForm")
            {
                if (crl.Name == "dept_comboBox")
                {
                    where_values = "where 科室=" + "'" + crl.Text + "'";
                    sqlwhere = "select*from dept " + where_values;
                }
            }
            DataSet ds_selectup = content.Select_nothing(sqlwhere);
            if (ds_selectup.Tables[0].Rows.Count == 0)
            {
                return false;
            }
            return true;

        }
        //判断值存在重新输入
        public static bool Judge_Exist(Form form, Control crl)
        {
            string where_values = "";
            string sqlwhere = "";
            A.BLL.newsContent content = new A.BLL.newsContent();
            if (form.Name == "DeptForm")
            {
                if (crl.Name == "dept_comboBox")
                {
                    where_values = "where 科室=" + "'" + crl.Text + "'";
                    sqlwhere = "select*from dept " + where_values;
                }
            }
            DataSet ds_selectup = content.Select_nothing(sqlwhere);
            if (ds_selectup.Tables[0].Rows.Count > 0)
            {
                return false;
            }
            return true;

        }
        //判断是否是修改的情况输出的部门id号
        public static int Selectid(string deptname, out int id)
        {
            id = 0;
            string where_values = "where 科室=" + "'" + deptname + "'";
            string sqlwhere = "select 序号 from dept " + where_values;
            A.BLL.newsContent content = new A.BLL.newsContent();
            DataSet ds_selectup = content.Select_nothing(sqlwhere);
            if (ds_selectup.Tables[0].Rows.Count > 0)
            {
                id = int.Parse(ds_selectup.Tables[0].Rows[0]["序号"].ToString());
                return id;
            }
            return id;
        }
        //判断手机号是否为整数和位数是否正确
        public static bool Chekin_phone(Control crl)
        {

            //判断是否为整数字符串
            //是的话则将其转换为数字并将其设为out类型的输出值、返回true, 否则为false
            try
            {
                Int64 result = -1;   //result 定义为out 用来输出值
                string phone_text = crl.Text.ToString().Trim();
                result = Int64.Parse(phone_text);//判断是否都是整数数字，这里如果是int32会报错提示值太太
                Int64 phone_int = Convert.ToInt64(phone_text);
                int phone_length = Math.Abs(phone_int).ToString().Length;//获取绝对值的整数位数
                if (phone_length != 11)//判断手机号位数是否是11位
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsNumeric(string str) //接收一个string类型的参数,保存到str里
        {
            if (str == null || str.Length == 0)    //验证这个参数是否为空
                return false;                           //是，就返回False
            str = ((int)double.Parse(str.ToString().Trim())).ToString();
            ASCIIEncoding ascii = new ASCIIEncoding();//new ASCIIEncoding 的实例
            byte[] bytestr = ascii.GetBytes(str);         //把string类型的参数保存到数组里

            foreach (byte c in bytestr)                   //遍历这个数组里的内容
            {
                if (c < 48 || c > 57)                          //判断是否为数字
                {
                    return false;                              //不是，就返回False
                }
            }
            return true;                                        //是，就返回True
        }
        public static bool IsInt(string str) //接收一个string类型的参数,保存到str里
        {
            if (str == null || str.Length == 0)    //验证这个参数是否为空
                return false;                           //是，就返回False
            ASCIIEncoding ascii = new ASCIIEncoding();//new ASCIIEncoding 的实例
            byte[] bytestr = ascii.GetBytes(str);         //把string类型的参数保存到数组里
            foreach (byte c in bytestr)                   //遍历这个数组里的内容
            {
                if (c < 48 || c > 57)                          //判断是否为数字
                {
                    return false;                              //不是，就返回False
                }
            }
            return true;                                        //是，就返回True
        }
        public void TiaoMaInsertTosql(string Device_, string TiaoMa_, string identification_, string po_, string potype_, string CurrentPackingNum_, string Color_Code_, string Set_Code_, string IsUnpacking_, string MarkingIsshieid_)
        {
            string sqlstr = string.Format("SELECT*FROM TiaoMa WHERE DeviceName='{0}' ", Device_);
            DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
            if (dt.Rows.Count > 0)
            {
                sqlstr = string.Format("UPDATE TiaoMa SET TiaoMaVal='{0}',identification='{1}',po='{2}',potype='{3}',IsEndPacking=0,CurrentPackingNum={4},Color_Code='{5}',Set_Code='{6}',IsUnpacking='{8}',MarkingIsshieid='{9}' WHERE ISNULL(DeviceName,'')='{7}'", TiaoMa_, identification_, po_, potype_, CurrentPackingNum_, Color_Code_, Set_Code_, Device_, IsUnpacking_, MarkingIsshieid_);
                CsFormShow.GoSqlUpdateInsert(sqlstr);
            }
            else
            {
                sqlstr = string.Format("INSERT INTO TiaoMa (DeviceName,TiaoMaVal,identification,po,potype,CurrentPackingNum,Color_Code,Set_Code,IsUnpacking,MarkingIsshieid) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')", Device_, TiaoMa_, identification_, po_, potype_, CurrentPackingNum_, Color_Code_, Set_Code_, IsUnpacking_, MarkingIsshieid_);
                 CsFormShow.GoSqlUpdateInsert(sqlstr);
            }
        }
        /// <summary>
        /// 判断是否超订单数
        /// </summary>
        /// <param name="qty_"></param>
        /// <param name="po_"></param>
        /// <param name="SKU_Code_"></param>
        /// <param name="Warehouse_"></param>
        /// <returns></returns>
        public static bool IsExceedYYKGUPoNumber(int qty_, string po_, string Warehouse_, string Set_Code_)
        {
            try
            {
                string sqlstr = string.Format("SELECT*FROM dbo.YOUYIKUDO WHERE Order_No='{0}'  AND Warehouse='{1}' AND Set_Code='{2}' AND Quantity<packingtotal+{3} ", po_, Warehouse_, Set_Code_, qty_);
                DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                if (dt.Rows.Count > 0)
                {
                    return true;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return false;
        }
        public static bool IsChagePo(string PoType_, Dictionary<string, string> Dic_data_)
        {
            try
            {
                //判断是否纸箱超尺寸
                string IsOverrrange = Chekin.IsOverrrange(int.Parse(Dic_data_["Long"]), int.Parse(Dic_data_["Width"]), int.Parse(Dic_data_["Heghit"]));
                if (!string.IsNullOrEmpty(IsOverrrange))
                {
                    CsFormShow.MessageBoxFormShow(IsOverrrange);
                    return false;
                }
                if (PoType_.Contains("UNIQLO") || PoType_.Contains("GU"))
                {
                    //bool b_v = CsFormShow.IsExistenceInSql("dbo.YYKGUSchedulingTable", "po_guid", YouyikudGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim());
                    int countline = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", Dic_data_["订单号码"], Dic_data_["Warehouse"], Dic_data_["Set_Code"]));
                    if (countline > 0)
                    {
                        MessageBox.Show("此订单已经在队列中，请重新选择");
                        return false;
                    }
                    //判断是否有混色号同类型在队列中
                    int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.YYKMarkingLineUp AS a WHERE EXISTS(SELECT *FROM dbo.YOUYIKUDO WHERE guid=a.YK_guid and Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}')", Dic_data_["订单号码"], Dic_data_["Warehouse"], Dic_data_["Set_Code"]));
                    if (count_v > 0)
                    {
                        MessageBox.Show("混色号同类型已经在队列中，请重新选择");
                        return false;
                    }
                    #region 设置当前订单
                    DialogResult btchose = MessageBox.Show("是否将订单设置为当前", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (btchose == DialogResult.OK)
                    {
                        #region 判断是否超订单数
                        bool b_IsExceedPoNumber = Chekin.IsExceedYYKGUPoNumber(int.Parse(Dic_data_["packingqty"]), Dic_data_["订单号码"], Dic_data_["Warehouse"], Dic_data_["Set_Code"]);
                        if (b_IsExceedPoNumber)
                        {
                            DialogResult diaIsExceedPoNumber = MessageBox.Show("超订单数是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                            if (diaIsExceedPoNumber == DialogResult.Cancel)
                            {
                                return false;
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
                else if (PoType_.Contains("GAP"))
                {
                    //bool b_v = CsFormShow.IsExistenceInSql("dbo.YYKGUSchedulingTable", "po_guid", YouyikudGV.SelectedRows[0].Cells["guid"].Value.ToString().Trim());
                    int countline = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE 订单号码='{0}' AND 从='{1}'", Dic_data_["订单号码"], Dic_data_["从"]));
                    if (countline > 0)
                    {
                        MessageBox.Show("此订单已经在队列中，请重新选择");
                        return false;
                    }
                    // 判断是否超订单数
                    bool b_IsExceedPoNumber = Chekin.IsGAPExceedPoNumber(int.Parse(Dic_data_["packingqty"]), Dic_data_["订单号码"], Dic_data_["从"]);
                    if (b_IsExceedPoNumber)
                    {
                        MessageBox.Show("超订单数，请重新设置");
                        return false;
                    }
                    //判断是否有相应的标签数据
                    int count_pdf = CsFormShow.GoSqlSelectCount(string.Format("SELECT *FROM dbo.PDFDATA WHERE po='{0}'", Dic_data_["订单号码"].Trim()));
                    if (count_pdf == 0)
                    {
                        // MessageBox.Show("没有相应的标签数据，不允许切换订单");
                        DialogResult bt_v = MessageBox.Show("没有相应的标签数据,是否任然要切换订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (bt_v == DialogResult.Cancel)
                        {
                            return false;
                        }
                        else
                        {
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.SchedulingTable SET GAPIsPrintRun='不运行' WHERE guid='{0}'", Dic_data_["guid"]));
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }
        /// <summary>
        /// 判断是否还有订单要做
        /// </summary>
        /// <returns></returns>
        public static bool IsExistPo()
        {
            try
            {
                //判断是否有单要做
                int YYK_COUNT = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.YYKMarkingLineUp WHERE DeviceName ='{0}'", CSReadPlc.DevName));
                int GAP_COUNT = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE  DeviceName ='{0}'", CSReadPlc.DevName));
                if (YYK_COUNT == 0 && GAP_COUNT == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 纸箱是否超尺寸范围
        /// </summary>
        /// <param name="long_"></param>
        /// <param name="width_"></param>
        /// <param name="heghit_"></param>
        /// <returns></returns>
        public static string IsOverrrange(int long_, int width_, int heghit_)
        {
            try
            {
                if (long_ < 400 || long_ > 800)
                {
                    return "长度超出范围";
                }
                else if (width_ < 280 || width_ > 530)
                {
                    return "宽度超出范围";
                }
                else if (heghit_ < 80 || heghit_ > 500)
                {
                    return "高度超出范围";
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return "";
        }
        /// <summary>
        /// 判断是否超订单数
        /// </summary>
        /// <param name="qty_"></param>
        /// <param name="po_"></param>
        /// <param name="SKU_Code_"></param>
        /// <param name="starcode_"></param>
        /// <returns></returns>
        public static bool IsGAPExceedPoNumber(int qty_, string po_, string starcode_)
        {
            try
            {
                string sqlstr = string.Format("SELECT*FROM dbo.PACKING WHERE  订单号码='{0}'  AND 从='{1}' AND  箱数<finishmarknumber+{2} ", po_, starcode_, qty_);
                DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                if (dt.Rows.Count > 0)
                {
                    return true;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Poguid_"></param>
        /// <param name="po_"></param>
        /// <param name="page_"></param>
        /// <param name="sort_"></param>
        /// <returns></returns>
        public static bool IsExistPdf(string Poguid_, string po_, int page_, int sort_)
        {
            try
            {
                if (string.IsNullOrEmpty(Poguid_))
                {
                    CsFormShow.MessageBoxFormShow("提示：没有相应的PDF标签数据");
                    return false;
                }
                string sqlstr = string.Format("SELECT '{2}' as Poguid,*FROM ( SELECT ROW_NUMBER() OVER(ORDER BY num) AS xuhao, *FROM(SELECT *, number1 AS num, SUBSTRING(number1, LEN(number1) - 4, 4)AS sort  FROM dbo.PDFDATA WHERE po = '{0}') pdf) pdftable WHERE po = '{0}'  AND sort=SUBSTRING('{3}',LEN('{3}')-3,4)AND (page = '{1}' OR xuhao='{1}')", po_, page_, Poguid_, sort_);
                DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                if (dt.Rows.Count == 0)
                {
                    CsFormShow.MessageBoxFormShow("提示：没有相应的PDF标签数据");
                    return false;
                }
                //根据标签打印次数判断
                //if (!string.IsNullOrEmpty(dt.Rows[0]["PrintCount"].ToString()))
                //{
                //    int PrintCount = int.Parse(dt.Rows[0]["PrintCount"].ToString().Trim());
                //    if (PrintCount>0)
                //    {
                //        CsFormShow.MessageBoxFormShow("箱号"+ page_ + "已经喷码打印过一次");
                //        DialogResult btchose = MessageBox.Show("箱号" + page_ + "已经喷码打印过一次，是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                //        if (btchose != DialogResult.OK)
                //        {
                //            return false;
                //        }
                //    }
                //}
                //根据喷码箱号记录判断是否之前此箱号已做过
                DataTable dt_line = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE packing_guid='{0}'", Poguid_));
                if (dt_line.Rows.Count>0)
                {
                    int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM CartonNOHistoricRcords WHERE 单号='{0}' AND 箱号='{1}'", po_, page_));
                    if (count_v>0)
                    {
                        CsFormShow.MessageBoxFormShow("箱号" + page_ + "已经喷码打印过一次");
                        DialogResult btchose = MessageBox.Show("箱号" + page_ + "已经喷码打印过一次，是否继续", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (btchose != DialogResult.OK)
                        {
                            return false;
                        }
                       Cslogfun.WriteToScaningLog("订单"+ po_ + ",箱号" + page_ + "重复打印喷码一次");
                       Cslogfun.WriteToCartornLog("订单" + po_ + ",箱号" + page_ + "重复打印喷码一次");
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.ToString());
            }
            return true;
        }
        /// <summary>
        /// 判断是否有GAP小标签
        /// </summary>
        /// <returns></returns>
        public static bool IsLabel()
        {
            try
            {
                if ((CSReadPlc.DevName.Trim() == "plc1" && CsMarking.M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && CsMarking.M_PackingType == "2"))
                {
                    if ((M_MarkingData.M_PackingCode == "1" || M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8") && CsPakingData.GetMarking_NONet("订单号码").Trim() != "")
                    {
                        if (!CsPakingData.GetMarking_NONet("GAPIsPrintRun").Contains("不运行"))//是否屏蔽打印
                        {
                            string Poguid = CsPakingData.GetMarking_NONet("packing_guid");
                            if (!string.IsNullOrEmpty(Poguid) && Poguid != "stop")
                            {
                                string po = CsPakingData.GetMarking_NONet("po");
                                int page = int.Parse(CsGetData.GetGAPMarkingNumber(CsPakingData.GetMarking_NONet("packing_guid"))) + int.Parse(CsPakingData.GetMarking_NONet("从"));
                                int sort = int.Parse(CsGetData.GetGAPMarkingNumber(CsPakingData.GetMarking_NONet("packing_guid"))) + int.Parse(CsPakingData.GetMarking_NONet("开始序列号"));
                                bool b_v = Chekin.IsExistPdf(Poguid, po, page, sort);
                                if (!b_v)
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                CsFormShow.MessageBoxFormShow("没有喷码订单数据");
                                return false;
                            }
                        }
                    }
                    else if ((M_MarkingData.M_PackingCode == "3" || M_MarkingData.M_PackingCode == "4") && CsPakingData.GetMarking_Net("Carton").Trim() != "")
                    {
                        if (!CsPakingData.GetMarking_Net("GAPIsPrintRun").Contains("不运行"))//是否屏蔽打印
                        {
                            string Poguid = CsPakingData.GetMarking_Net("packing_guid");
                            if (!string.IsNullOrEmpty(Poguid)&& Poguid!="stop")
                            {
                                string po = CsPakingData.GetMarking_Net("po");
                                int page = int.Parse(CsGetData.GetGAPMarkingNumber(CsPakingData.GetMarking_Net("packing_guid"))) + int.Parse(CsPakingData.GetMarking_Net("从"));
                                int sort = int.Parse(CsGetData.GetGAPMarkingNumber(CsPakingData.GetMarking_Net("packing_guid"))) + int.Parse(CsPakingData.GetMarking_Net("开始序列号"));
                                bool b_v = Chekin.IsExistPdf(Poguid, po, page, sort);//加入打印列队
                                if (!b_v)
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                CsFormShow.MessageBoxFormShow("没有喷码订单数据");
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.ToString());
            }
            return true;
        }
        #region 与关工对接程序
        /// <summary>
        /// 判断是否有GAP小标签
        /// </summary>
        /// <returns></returns>
        public static bool IsGAPLabel()
        {
            try
            {
                if ((CSReadPlc.DevName.Trim() == "plc1" && CsMarking.M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && CsMarking.M_PackingType == "2"))
                {
                    if ((M_MarkingData.M_PackingCode == "1" || M_MarkingData.M_PackingCode == "2" || M_MarkingData.M_PackingCode == "8") &&!string.IsNullOrEmpty( CsPublicVariablies.Marking.Po))
                    {
                        if (!CsPublicVariablies.Marking.IsPrint.Contains("不运行"))//是否屏蔽打印
                        {
                            string Poguid = CsPublicVariablies.Marking.Po_guid;
                            string po = CsPublicVariablies.Marking.Po;
                            int page = int.Parse(CsGetData.GetGAPMarkingNumber(Poguid)) + CsPublicVariablies.Marking.Packing从;
                            int sort = int.Parse(CsGetData.GetGAPMarkingNumber(Poguid)) +CsPublicVariablies.Marking.Packing开始序列号;
                            bool b_v = Chekin.IsExistPdf(Poguid, po, page, sort);
                            if (!b_v)
                            {
                                return false;
                            }
                        }
                    }
                    else if ((M_MarkingData.M_PackingCode == "3" || M_MarkingData.M_PackingCode == "4") &&!string.IsNullOrEmpty( CsPublicVariablies.Marking.Cartorn.ToString()))
                    {
                        if (!CsPublicVariablies.Marking.IsPrint.Contains("不运行"))//是否屏蔽打印
                        {
                            string Poguid = CsPublicVariablies.Marking.Po_guid;
                            string po = CsPublicVariablies.Marking.Po;
                            int page = int.Parse(CsGetData.GetGAPMarkingNumber(Poguid)) + CsPublicVariablies.Marking.Packing从;
                            int sort = int.Parse(CsGetData.GetGAPMarkingNumber(Poguid)) + CsPublicVariablies.Marking.Packing开始序列号;
                            bool b_v = Chekin.IsExistPdf(Poguid, po, page, sort);
                            if (!b_v)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.ToString());
            }
            return true;
        }
        #endregion
    }
}
