﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
namespace WindowsForms01
{
    public class CsInitialization
    {
        /// <summary>
        /// 开机加载打印喷码列队
        /// </summary>
        public static void SetupLineUp()
        {
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    //清除缓存
                    CsPakingData.M_list_DicMarkingNONet.Clear();
                    CsPakingData.M_list_DicMarkingNet.Clear();
                    CsPakingData.M_list_Dic_colorcode.Clear();
                    CsPakingData.M_list_Dic_GAPPalletizing.Clear();
                    CsPakingData.M_list_Dic_YYKGUPalletizing.Clear();
                    CsPakingData.M_list_Dic_palletizingtype.Clear();
                    M_stcPrintData.lis_printdata.Clear();
                    #region GAP队列初始化
                    //将异常未能打印完成的标签加入队列中
                     UpdatePrintLine();
                    //GAP喷码未完成记录加载到列队
                     UpdateGAPLineUp();
                    //GAP码垛未完成记录加载到列队
                    UpdateGAPPalletizingLineUp();
                    #region 优衣库喷码队列初始化
                    //喷码未完成记录加载到列队
                    UpdateYYKGULineUp();
                    #endregion
                    #region 优衣库GU码垛队列初始化
                    //码垛未完成记录加载到列队
                    UpdateYYKGUPalletizingLineUp();
                    #endregion
                    //码垛类型队列初始化
                    UpdatePalletizingtype();
                    #region 加载当前单条码信息
                    CsGetData.UpdataScanLineUp();//更新扫码队列
                    PowerOnUpdateCurrentScan();//获取当前扫码信息
                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

        }
        /// <summary>
        /// 重新加载GAP喷码队列
        /// </summary>
        public static void UpdateGAPLineUp()
        {
            try
            {
                #region GAP喷码队列初始化
                //将异常未能打印完成的标签加入队列中
                CsPakingData.M_list_DicMarkingNONet.Clear();
                CsPakingData.M_list_DicMarkingNet.Clear();
                //GAP喷码未完成记录加载到列队
                DataTable dtmarking = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 a.*,(SUBSTRING(b.Sku号码,0,7)+'-'+SUBSTRING(b.Sku号码,7,2)+'-'+SUBSTRING(b.Sku号码,9,1)+'-'+SUBSTRING(b.Sku号码,10,5)) AS sku,b.Size,GAPSideStartNum FROM dbo.MarkingLineUp AS a INNER JOIN dbo.PACKING AS b ON a.packing_guid=b.guid WHERE ISNULL(IsEndMarking,'')<>1 AND ISNULL(DeviceName,'')='{0}' ORDER BY a.Entrytime ", CSReadPlc.DevName.Trim()));
                for (int i = 0; i < dtmarking.Rows.Count; i++)
                {
                    DataRow dr = dtmarking.Rows[i];
                    CsMarking.SetToMarking(dr);
                }
                Cslogfun.WriteToCartornLog("重新加载GAP喷码队列一次");
                #endregion
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 重新加载优衣库和GU喷码队列
        /// </summary>
        public static void UpdateYYKGULineUp()
        {
            try
            {
                #region 优衣库喷码队列初始化
                //喷码未完成记录加载到列队
                CsPakingData.M_list_Dic_colorcode.Clear();
                DataTable dtyykmarking = new DataTable();
                dtyykmarking = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 *FROM dbo.YYKMarkingLineUp WHERE YK_guid IN(SELECT guid FROM dbo.YOUYIKUDO )AND ISNULL(IsEndMarking,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                for (int i = 0; i < dtyykmarking.Rows.Count; i++)
                {
                    DataRow dr = dtyykmarking.Rows[i];
                    CsGetData.SetToYYKMarking(dr);
                }
                Cslogfun.WriteToCartornLog("重新加载GAP喷码队列一次");
                #endregion
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 重新加载优衣库码垛队列
        /// </summary>
        public static void UpdateYYKGUPalletizingLineUp()
        {
            try
            {
                #region 重新加载优衣库码垛队列
                CsPakingData.M_list_Dic_YYKGUPalletizing.Clear();
                DataTable dtyykPalletizin = new DataTable();
                dtyykPalletizin = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM dbo.YYKMarkingLineUp WHERE YK_guid IN(SELECT guid FROM dbo.YOUYIKUDO ) AND DeviceName='{0}' ORDER BY dbo.YYKMarkingLineUp.Entrytime", CSReadPlc.DevName.Trim()));
                for (int i = 0; i < dtyykPalletizin.Rows.Count; i++)
                {
                    DataRow dr = dtyykPalletizin.Rows[i];
                    SetToYYKGUPalletizing(dr);
                }
                #endregion
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 加载GAP码垛队列
        /// </summary>
        public static void UpdateGAPPalletizingLineUp()
        {
            try
            {
                #region 重新加载GAP码垛队列
                CsPakingData.M_list_Dic_GAPPalletizing.Clear();
                DataTable dtGAPPalletizin = new DataTable();
                dtGAPPalletizin = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1*FROM MarkingLineUp WHERE  DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                for (int i = 0; i < dtGAPPalletizin.Rows.Count; i++)
                {
                    DataRow dr = dtGAPPalletizin.Rows[i];
                    CsPalletizing.SetToGAPPalletizing(dr);
                }
                #endregion
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 优衣库/GU开机自动切单
        /// </summary>
        /// <param name="po_"></param>
        /// <param name="strPoTiaoMa_"></param>
        /// <param name="stridentification_"></param>
        /// <param name="CurrentPackingNum_"></param>
        public static bool  SetYYKGUWritePlcData(string po_, string strPoTiaoMa_, string stridentification_, string CurrentPackingNum_, string Color_Code_, string Set_Code_, string Layernum_, string Partitiontype_, string IsPackingRun_, string IsYaMaHaRun_, string IsSealingRun_, string MarkingIsshieid_, string PrintIsshieid_, string IsPalletizingRun_,string IsScanRun_,string HookDirection_)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = CsFormShow.GoSqlSelect(string.Format("SELECT Quantity, Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '1' THEN '不喷色号' ELSE '喷色号' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向',CASE WHEN  IsRepeatCheck = '1' THEN '是' ELSE '否' END AS '是否再检针',IsPalletizing,potype FROM dbo.YOUYIKUDO, dbo.YYKGUSchedulingTable  where dbo.YOUYIKUDO.guid = dbo.YYKGUSchedulingTable.po_guid and  dbo.YOUYIKUDO.guid IN(SELECT guid FROM dbo.YOUYIKUDO WHERE Order_No='{0}' AND SKU_Code='{1}' AND Warehouse='{2}'AND Color_Code='{3}'AND Set_Code='{4}')", po_, strPoTiaoMa_, stridentification_, Color_Code_, Set_Code_));
                if (dt.Rows.Count > 0)
                {
                    CsPakingData.M_DicMarking_colorcode.Clear();
                    //计算产品厚度
                    int Height = int.Parse(dt.Rows[0]["箱子高"].ToString().Trim());
                    int culumn = int.Parse(dt.Rows[0]["列数"].ToString().Trim());
                    string potype_v= dt.Rows[0]["potype"].ToString().Trim();
                    if (culumn==0)
                    {
                        MessageBox.Show("列数不能为0");
                        return false;
                    }
                    string Thick = (Height / (int.Parse(Layernum_) / culumn)).ToString();
                    //计算隔板类型
                    int catornlong = int.Parse(dt.Rows[0]["箱子长"].ToString().Trim());
                    int catornwidth = int.Parse(dt.Rows[0]["箱子宽"].ToString().Trim());
                    string PartitionType = "";
                    string IsBottomPartition = "";
                    string IsTopPartition = "";
                    CsGetData.GetPartitionData(catornlong, catornwidth, Partitiontype_, out PartitionType, out IsTopPartition, out IsBottomPartition);
                    CsPakingData.M_DicMarking_colorcode["IsTopPartition"] = IsTopPartition;
                    CsPakingData.M_DicMarking_colorcode["IsBottomPartition"] = IsBottomPartition;
                    CsPakingData.M_DicMarking_colorcode["Long"] = dt.Rows[0]["箱子长"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["Width"] = dt.Rows[0]["箱子宽"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["Heghit"] = dt.Rows[0]["箱子高"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["Updown_distance"] = dt.Rows[0]["距离底部"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["Leftright_distance"] = dt.Rows[0]["距离左边"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["CartonWeight"] = dt.Rows[0]["重量"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["GoodWeight"] = dt.Rows[0]["产品重量"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["ColumnNunber"] = dt.Rows[0]["列数"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["LineNum"] = dt.Rows[0]["行数"].ToString().Trim();
                    CsPakingData.M_DicMarking_colorcode["Layernum"] = Layernum_;
                    CsPakingData.M_DicMarking_colorcode["Thick"] = Thick;
                    /*CsPakingData.M_DicMarking_colorcode["Jianspeed"] = dt.Rows[0]["加减速度"].ToString().Trim();*/
                    /*CsPakingData.M_DicMarking_colorcode["Grabspeed"] = dt.Rows[0]["抓取速度"].ToString().Trim();*/
                    CsPakingData.M_DicMarking_colorcode["MarkPattern"] = CsMarking.GetCatornParameter(dt.Rows[0]["喷码模式"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["CatornType"] = CsMarking.GetCatornParameter(dt.Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["IsRotate"] = CsMarking.GetCatornParameter(dt.Rows[0]["是否旋转"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["LabelingType"] = CsMarking.GetCatornParameter(dt.Rows[0]["贴标类型"].ToString().Trim()).ToString();
                    if (string.IsNullOrEmpty(PartitionType))
                    {
                        CsPakingData.M_DicMarking_colorcode["PartitionType"] = "0";
                    }
                    else
                    {
                        CsPakingData.M_DicMarking_colorcode["PartitionType"] = CsMarking.GetCatornParameter(PartitionType).ToString();
                    }
                    CsPakingData.M_DicMarking_colorcode["Orientation"] = CsMarking.GetCatornParameter(dt.Rows[0]["正反方向"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["IsRepeatCheck"] = CsMarking.GetCatornParameter(dt.Rows[0]["是否再检针"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["Quantity"] = CurrentPackingNum_;
                    CsPakingData.M_DicMarking_colorcode["IsPalletizing"] = CsMarking.GetCatornParameter(dt.Rows[0]["IsPalletizing"].ToString().Trim()).ToString();
                    CsPakingData.M_DicMarking_colorcode["IsPackingRun"] = CsMarking.GetCatornParameter(IsPackingRun_).ToString();
                    CsPakingData.M_DicMarking_colorcode["IsYaMaHaRun"] = CsMarking.GetCatornParameter(IsYaMaHaRun_).ToString();
                    CsPakingData.M_DicMarking_colorcode["IsSealingRun"] = CsMarking.GetCatornParameter(IsSealingRun_).ToString();
                    CsPakingData.M_DicMarking_colorcode["MarkingIsshieid"] = CsMarking.GetCatornParameter(MarkingIsshieid_).ToString();
                    CsPakingData.M_DicMarking_colorcode["PrintIsshieid"] = CsMarking.GetCatornParameter(PrintIsshieid_).ToString();
                    CsPakingData.M_DicMarking_colorcode["IsPalletizingRun"] = CsMarking.GetCatornParameter(IsPalletizingRun_).ToString();
                    CsPakingData.M_DicMarking_colorcode["IsScanRun"] = CsMarking.GetCatornParameter(IsScanRun_).ToString();
                    CsPakingData.M_DicMarking_colorcode["HookDirection"] = CsMarking.GetCatornParameter(HookDirection_).ToString();
                    if (potype_v.Contains("UNIQLO"))
                    {
                        CsPakingData.M_DicMarking_colorcode["PoTypeSignal"] = "1";
                    }
                    else
                    {
                        CsPakingData.M_DicMarking_colorcode["PoTypeSignal"] = "2";
                    }
                    if (CsRWPlc.mCdata.IsConnect())
                    {
                        CsMarking.M_MarkParameter = "ColorCode";
                    }
                    else
                    {
                        MessageBox.Show("设备未启动，请先启动设备");
                        return false;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }
        /// <summary>
/// 装箱参数写入PLC
/// </summary>
/// <param name="po_"></param>
/// <param name="strPoTiaoMa_"></param>
/// <param name="stridentification_"></param>
/// <param name="CurrentPackingNum_"></param>
/// <param name="Color_Code_"></param>
/// <param name="Set_Code_"></param>
/// <param name="Layernum_"></param>
/// <param name="Partitiontype_"></param>
/// <param name="IsPackingRun_"></param>
/// <param name="IsYaMaHaRun_"></param>
/// <param name="IsSealingRun_"></param>
/// <param name="MarkingIsshieid_"></param>
/// <param name="PrintIsshieid_"></param>
/// <param name="IsPalletizingRun_"></param>
/// <param name="IsScanRun_"></param>
/// <param name="HookDirection_"></param>
/// <returns></returns>
        public static bool SetWritePlcData(string po_, string strPoTiaoMa_, string stridentification_, string CurrentPackingNum_, string Color_Code_, string Set_Code_, string Layernum_, string Partitiontype_, string IsPackingRun_, string IsYaMaHaRun_, string IsSealingRun_, string MarkingIsshieid_, string PrintIsshieid_, string IsPalletizingRun_, string IsScanRun_, string HookDirection_,string GAPIsPrintRun_, string GAPSideIsshieid_, string potype_)
        {
            try
            {
                if (potype_.Contains("UNIQLO")|| potype_.Contains("GU"))
                {
                    DataTable dt = new DataTable();
                     dt = CsFormShow.GoSqlSelect(string.Format("SELECT Quantity, Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '1' THEN '不喷色号' ELSE '喷色号' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向',CASE WHEN  IsRepeatCheck = '1' THEN '是' ELSE '否' END AS '是否再检针',IsPalletizing,读取条码模式,Thick,Layernum FROM dbo.YOUYIKUDO, dbo.SchedulingTable  where  dbo.YOUYIKUDO.guid = dbo.SchedulingTable.po_guid and  dbo.YOUYIKUDO.guid IN(SELECT guid FROM dbo.YOUYIKUDO WHERE Order_No='{0}' AND SKU_Code='{1}' AND Warehouse='{2}'AND Color_Code='{3}'AND Set_Code='{4}' )", po_, strPoTiaoMa_, stridentification_, Color_Code_, Set_Code_));
                    if (dt.Rows.Count > 0)
                    {
                        CsPakingData.M_DicMarking_colorcode.Clear();
                        //计算产品厚度
                        int Height = int.Parse(dt.Rows[0]["箱子高"].ToString().Trim());
                        int culumn = int.Parse(dt.Rows[0]["列数"].ToString().Trim());
                        if (culumn == 0)
                        {
                            MessageBox.Show("列数不能为0");
                            return false;
                        }
                        string Thick = (Height / (int.Parse(Layernum_) / culumn)).ToString();
                        //计算隔板类型
                        int catornlong = int.Parse(dt.Rows[0]["箱子长"].ToString().Trim());
                        int catornwidth = int.Parse(dt.Rows[0]["箱子宽"].ToString().Trim());
                        string PartitionType = "";
                        string IsBottomPartition = "";
                        string IsTopPartition = "";
                        CsGetData.GetPartitionData(catornlong, catornwidth, Partitiontype_, out PartitionType, out IsTopPartition, out IsBottomPartition);
                        CsPakingData.M_DicMarking_colorcode["IsTopPartition"] = IsTopPartition;
                        CsPakingData.M_DicMarking_colorcode["IsBottomPartition"] = IsBottomPartition;
                        CsPakingData.M_DicMarking_colorcode["Long"] = dt.Rows[0]["箱子长"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Width"] = dt.Rows[0]["箱子宽"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Heghit"] = dt.Rows[0]["箱子高"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Updown_distance"] = dt.Rows[0]["距离底部"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Leftright_distance"] = dt.Rows[0]["距离左边"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["CartonWeight"] = dt.Rows[0]["重量"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["GoodWeight"] = dt.Rows[0]["产品重量"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["ColumnNunber"] = dt.Rows[0]["列数"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["LineNum"] = dt.Rows[0]["行数"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Layernum"] = dt.Rows[0]["Layernum"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Thick"] = dt.Rows[0]["Thick"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["读取条码模式"] = dt.Rows[0]["读取条码模式"].ToString().Trim();
                        /*CsPakingData.M_DicMarking_colorcode["Jianspeed"] = dt.Rows[0]["加减速度"].ToString().Trim();*/
                        /*CsPakingData.M_DicMarking_colorcode["Grabspeed"] = dt.Rows[0]["抓取速度"].ToString().Trim();*/
                        CsPakingData.M_DicMarking_colorcode["MarkPattern"] = CsMarking.GetCatornParameter(dt.Rows[0]["喷码模式"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["CatornType"] = CsMarking.GetCatornParameter(dt.Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsRotate"] = CsMarking.GetCatornParameter(dt.Rows[0]["是否旋转"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["LabelingType"] = CsMarking.GetCatornParameter(dt.Rows[0]["贴标类型"].ToString().Trim()).ToString();
                        if (string.IsNullOrEmpty(PartitionType))
                        {
                            CsPakingData.M_DicMarking_colorcode["PartitionType"] = "0";
                        }
                        else
                        {
                            CsPakingData.M_DicMarking_colorcode["PartitionType"] = CsMarking.GetCatornParameter(PartitionType).ToString();
                        }
                        CsPakingData.M_DicMarking_colorcode["Orientation"] = CsMarking.GetCatornParameter(dt.Rows[0]["正反方向"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsRepeatCheck"] = CsMarking.GetCatornParameter(dt.Rows[0]["是否再检针"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["Quantity"] = CurrentPackingNum_;
                        CsPakingData.M_DicMarking_colorcode["IsPalletizing"] = CsMarking.GetCatornParameter(dt.Rows[0]["IsPalletizing"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsPackingRun"] = CsMarking.GetCatornParameter(IsPackingRun_).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsYaMaHaRun"] = CsMarking.GetCatornParameter(IsYaMaHaRun_).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsSealingRun"] = CsMarking.GetCatornParameter(IsSealingRun_).ToString();
                        CsPakingData.M_DicMarking_colorcode["MarkingIsshieid"] = CsMarking.GetCatornParameter(MarkingIsshieid_).ToString();
                        CsPakingData.M_DicMarking_colorcode["PrintIsshieid"] = CsMarking.GetCatornParameter(PrintIsshieid_).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsPalletizingRun"] = CsMarking.GetCatornParameter(IsPalletizingRun_).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsScanRun"] = CsMarking.GetCatornParameter(IsScanRun_).ToString();
                        CsPakingData.M_DicMarking_colorcode["HookDirection"] = CsMarking.GetCatornParameter(HookDirection_).ToString();
                        if (potype_.Contains("UNIQLO"))
                        {
                            CsPakingData.M_DicMarking_colorcode["PoTypeSignal"] = "1";
                        }
                        else
                        {
                            CsPakingData.M_DicMarking_colorcode["PoTypeSignal"] = "2";
                        }
                        if (CsRWPlc.mCdata.IsConnect())
                        {
                            CsMarking.M_MarkParameter = "ColorCode";
                        }
                        else
                        {
                            MessageBox.Show("设备未启动，请先启动设备");
                            return false;
                        }
                    }
                }
                else if (potype_.Contains("GAP"))
                {
                    DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT ISNULL(箱数,0)-ISNULL(finishmarknumber,0) as '箱数', Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数',LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '3' THEN '国内' ELSE '国外' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向' ,IsPalletizing,外箱代码,读取条码模式,Thick,Layernum FROM dbo.PACKING, dbo.SchedulingTable  where dbo.PACKING.guid = dbo.SchedulingTable.po_guid and  dbo.PACKING.guid IN(SELECT guid FROM dbo.PACKING WHERE 订单号码='{0}'  AND 从='{1}')", po_, stridentification_));
                    if (dt.Rows.Count > 0)
                    {
                        CsPakingData.M_DicPakingData.Clear();
                        //计算产品厚度
                        int Height = int.Parse(dt.Rows[0]["箱子高"].ToString().Trim());
                        int culumn = int.Parse(dt.Rows[0]["列数"].ToString().Trim());
                        string waixiang_code = dt.Rows[0]["外箱代码"].ToString().Trim();
                        if (culumn == 0)
                        {
                            MessageBox.Show("列数不能为0");
                            return false;
                        }
                        string Thick = (Height / (int.Parse(Layernum_) / culumn)).ToString();
                        //计算隔板类型
                        int catornlong = int.Parse(dt.Rows[0]["箱子长"].ToString().Trim());
                        int catornwidth = int.Parse(dt.Rows[0]["箱子宽"].ToString().Trim());
                        string PartitionType = "";
                        string IsBottomPartition = "";
                        string IsTopPartition = "";
                        CsGetData.GetPartitionData(catornlong, catornwidth, Partitiontype_, out PartitionType, out IsTopPartition, out IsBottomPartition);
                        CsPakingData.M_DicPakingData["IsTopPartition"] = IsTopPartition;
                        CsPakingData.M_DicPakingData["IsBottomPartition"] = IsBottomPartition;
                        CsPakingData.M_DicPakingData["箱子长"] = dt.Rows[0]["箱子长"].ToString().Trim();
                        CsPakingData.M_DicPakingData["箱子宽"] = dt.Rows[0]["箱子宽"].ToString().Trim();
                        CsPakingData.M_DicPakingData["箱子高"] = dt.Rows[0]["箱子高"].ToString().Trim();
                        CsPakingData.M_DicPakingData["距离底部"] = dt.Rows[0]["距离底部"].ToString().Trim();
                        CsPakingData.M_DicPakingData["距离左边"] = dt.Rows[0]["距离左边"].ToString().Trim();
                        CsPakingData.M_DicPakingData["重量"] = dt.Rows[0]["重量"].ToString().Trim();
                        CsPakingData.M_DicPakingData["产品重量"] = dt.Rows[0]["产品重量"].ToString().Trim();
                        CsPakingData.M_DicPakingData["列数"] = dt.Rows[0]["列数"].ToString().Trim();
                        CsPakingData.M_DicPakingData["行数"] = dt.Rows[0]["行数"].ToString().Trim();
                        CsPakingData.M_DicPakingData["箱内件数"] = dt.Rows[0]["Layernum"].ToString().Trim();
                        CsPakingData.M_DicPakingData["箱数"] = dt.Rows[0]["箱数"].ToString().Trim();
                        CsPakingData.M_DicPakingData["产品厚度"] = dt.Rows[0]["Thick"].ToString().Trim();
                        CsPakingData.M_DicPakingData["读取条码模式"] = dt.Rows[0]["读取条码模式"].ToString().Trim();
                        //CsPakingData.M_DicPakingData["加减速度"] = dt.Rows[0]["加减速度"].ToString().Trim();
                        // CsPakingData.M_DicPakingData["抓取速度"] = dt.Rows[0]["抓取速度"].ToString().Trim();
                        if (waixiang_code.Contains("C2"))
                        {
                            CsPakingData.M_DicPakingData["喷码模式"] = "5";
                        }
                        else
                        {
                            CsPakingData.M_DicPakingData["喷码模式"] = CsMarking.GetCatornParameter(dt.Rows[0]["喷码模式"].ToString().Trim()).ToString();
                        }
                        CsPakingData.M_DicPakingData["纸箱大小"] = CsMarking.GetCatornParameter(dt.Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["是否旋转"] = CsMarking.GetCatornParameter(dt.Rows[0]["是否旋转"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["贴标类型"] = CsMarking.GetCatornParameter(dt.Rows[0]["贴标类型"].ToString().Trim()).ToString();
                        if (string.IsNullOrEmpty(PartitionType))
                        {
                            CsPakingData.M_DicPakingData["PartitionType"] = "0";
                        }
                        else
                        {
                            CsPakingData.M_DicPakingData["PartitionType"] = CsMarking.GetCatornParameter(dt.Rows[0]["隔板类型"].ToString().Trim()).ToString();
                        }
                        CsPakingData.M_DicPakingData["正反方向"] = CsMarking.GetCatornParameter(dt.Rows[0]["正反方向"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["IsPalletizing"] = CsMarking.GetCatornParameter(dt.Rows[0]["IsPalletizing"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["IsPackingRun"] = CsMarking.GetCatornParameter(IsPackingRun_).ToString();
                        CsPakingData.M_DicPakingData["IsYaMaHaRun"] = CsMarking.GetCatornParameter(IsYaMaHaRun_).ToString();
                        CsPakingData.M_DicPakingData["IsSealingRun"] = CsMarking.GetCatornParameter(IsSealingRun_).ToString();
                        CsPakingData.M_DicPakingData["MarkingIsshieid"] = CsMarking.GetCatornParameter(MarkingIsshieid_).ToString();
                        CsPakingData.M_DicPakingData["PrintIsshieid"] = CsMarking.GetCatornParameter(PrintIsshieid_).ToString();
                        CsPakingData.M_DicPakingData["GAPIsPrintRun"] = CsMarking.GetCatornParameter(GAPIsPrintRun_).ToString();
                        CsPakingData.M_DicPakingData["GAPSideIsshieid"] = CsMarking.GetCatornParameter(GAPSideIsshieid_).ToString();
                        CsPakingData.M_DicPakingData["IsPalletizingRun"] = CsMarking.GetCatornParameter(IsPalletizingRun_).ToString();
                        CsPakingData.M_DicPakingData["IsScanRun"] = CsMarking.GetCatornParameter(IsScanRun_).ToString();
                        CsPakingData.M_DicPakingData["HookDirection"] = CsMarking.GetCatornParameter(HookDirection_).ToString();
                        CsPakingData.M_DicPakingData["PoTypeSignal"] = "3";
                        if (CsRWPlc.mCdata.IsConnect())
                        {
                            if (dt.Rows[0]["喷码模式"].ToString().Trim().Contains("国内"))
                            {
                                CsMarking.M_MarkParameter = "NONet";
                            }
                            else if (dt.Rows[0]["喷码模式"].ToString().Trim().Contains("国外"))
                            {
                                CsMarking.M_MarkParameter = "Net";
                            }
                        }
                        else
                        {
                            MessageBox.Show("设备未启动，请先启动设备");
                            return false;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }

        /// <summary>
        /// GAP开机自动切单
        /// </summary>
        /// <param name="po_"></param>
        /// <param name="strPoTiaoMa_"></param>
        /// <param name="stridentification_"></param>
        /// <param name="CurrentPackingNum_"></param>
        public static bool SetGAPWritePlcData(string po_, string strPoTiaoMa_, string stridentification_, string CurrentPackingNum_, string Layernum_, string Partitiontype_, string IsPackingRun_, string IsYaMaHaRun_, string IsSealingRun_, string MarkingIsshieid_, string PrintIsshieid_, string IsPalletizingRun_,string IsScanRun_,string  GAPIsPrintRun_,string HookDirection_)
        {
            bool b_v = true; ;
            try
            {
                DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT ISNULL(箱数,0)-ISNULL(finishmarknumber,0) as '箱数', Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数',LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '3' THEN '国内' ELSE '国外' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向' ,IsPalletizing,外箱代码 FROM dbo.PACKING, dbo.GAPSchedulingTable  where dbo.PACKING.guid = dbo.GAPSchedulingTable.po_guid and  dbo.PACKING.guid IN(SELECT guid FROM dbo.PACKING WHERE 订单号码='{0}'  AND 从='{1}')", po_, stridentification_));
                if (dt.Rows.Count > 0)
                {
                    CsPakingData.M_DicPakingData.Clear();
                    //计算产品厚度
                    int Height = int.Parse(dt.Rows[0]["箱子高"].ToString().Trim());
                    int culumn = int.Parse(dt.Rows[0]["列数"].ToString().Trim());
                    string waixiang_code = dt.Rows[0]["外箱代码"].ToString().Trim();
                    if (culumn==0)
                    {
                        MessageBox.Show("列数不能为0");
                        return false;
                    }
                    string Thick = (Height / (int.Parse(Layernum_) / culumn)).ToString();
                    //计算隔板类型
                    int catornlong = int.Parse(dt.Rows[0]["箱子长"].ToString().Trim());
                    int catornwidth = int.Parse(dt.Rows[0]["箱子宽"].ToString().Trim());
                    string PartitionType = "";
                    string IsBottomPartition = "";
                    string IsTopPartition = "";
                    CsGetData.GetPartitionData(catornlong, catornwidth, Partitiontype_, out PartitionType, out IsTopPartition, out IsBottomPartition);
                    CsPakingData.M_DicPakingData["IsTopPartition"] = IsTopPartition;
                    CsPakingData.M_DicPakingData["IsBottomPartition"] = IsBottomPartition;
                    CsPakingData.M_DicPakingData["箱子长"] = dt.Rows[0]["箱子长"].ToString().Trim();
                    CsPakingData.M_DicPakingData["箱子宽"] = dt.Rows[0]["箱子宽"].ToString().Trim();
                    CsPakingData.M_DicPakingData["箱子高"] = dt.Rows[0]["箱子高"].ToString().Trim();
                    CsPakingData.M_DicPakingData["距离底部"] = dt.Rows[0]["距离底部"].ToString().Trim();
                    CsPakingData.M_DicPakingData["距离左边"] = dt.Rows[0]["距离左边"].ToString().Trim();
                    CsPakingData.M_DicPakingData["重量"] = dt.Rows[0]["重量"].ToString().Trim();
                    CsPakingData.M_DicPakingData["产品重量"] = dt.Rows[0]["产品重量"].ToString().Trim();
                    CsPakingData.M_DicPakingData["列数"] = dt.Rows[0]["列数"].ToString().Trim();
                    CsPakingData.M_DicPakingData["行数"] = dt.Rows[0]["行数"].ToString().Trim();
                    CsPakingData.M_DicPakingData["箱内件数"] = Layernum_;
                    CsPakingData.M_DicPakingData["箱数"] = dt.Rows[0]["箱数"].ToString().Trim();
                    CsPakingData.M_DicPakingData["产品厚度"] = Thick;
                    //CsPakingData.M_DicPakingData["加减速度"] = dt.Rows[0]["加减速度"].ToString().Trim();
                   // CsPakingData.M_DicPakingData["抓取速度"] = dt.Rows[0]["抓取速度"].ToString().Trim();
                    if (waixiang_code.Contains("C2"))
                    {
                        CsPakingData.M_DicPakingData["喷码模式"] = "5";
                    }
                    else
                    {
                        CsPakingData.M_DicPakingData["喷码模式"] = CsMarking.GetCatornParameter(dt.Rows[0]["喷码模式"].ToString().Trim()).ToString();
                    }
                    CsPakingData.M_DicPakingData["纸箱大小"] = CsMarking.GetCatornParameter(dt.Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                    CsPakingData.M_DicPakingData["是否旋转"] = CsMarking.GetCatornParameter(dt.Rows[0]["是否旋转"].ToString().Trim()).ToString();
                    CsPakingData.M_DicPakingData["贴标类型"] = CsMarking.GetCatornParameter(dt.Rows[0]["贴标类型"].ToString().Trim()).ToString();
                    if (string.IsNullOrEmpty(PartitionType))
                    {
                        CsPakingData.M_DicPakingData["PartitionType"] = "0";
                    }
                    else
                    {
                        CsPakingData.M_DicPakingData["PartitionType"] = CsMarking.GetCatornParameter(dt.Rows[0]["隔板类型"].ToString().Trim()).ToString();
                    }
                    CsPakingData.M_DicPakingData["正反方向"] = CsMarking.GetCatornParameter(dt.Rows[0]["正反方向"].ToString().Trim()).ToString();
                    CsPakingData.M_DicPakingData["IsPalletizing"] = CsMarking.GetCatornParameter(dt.Rows[0]["IsPalletizing"].ToString().Trim()).ToString();
                    CsPakingData.M_DicPakingData["IsPackingRun"] = CsMarking.GetCatornParameter(IsPackingRun_).ToString();
                    CsPakingData.M_DicPakingData["IsYaMaHaRun"] = CsMarking.GetCatornParameter(IsYaMaHaRun_).ToString();
                    CsPakingData.M_DicPakingData["IsSealingRun"] = CsMarking.GetCatornParameter(IsSealingRun_).ToString();
                    CsPakingData.M_DicPakingData["MarkingIsshieid"] = CsMarking.GetCatornParameter(MarkingIsshieid_).ToString();
                    CsPakingData.M_DicPakingData["PrintIsshieid"] = CsMarking.GetCatornParameter(PrintIsshieid_).ToString();
                    CsPakingData.M_DicPakingData["GAPIsPrintRun"] = CsMarking.GetCatornParameter(GAPIsPrintRun_).ToString();
                    CsPakingData.M_DicPakingData["IsPalletizingRun"] = CsMarking.GetCatornParameter(IsPalletizingRun_).ToString();
                    CsPakingData.M_DicPakingData["IsScanRun"] = CsMarking.GetCatornParameter(IsScanRun_).ToString();
                    CsPakingData.M_DicPakingData["HookDirection"] = CsMarking.GetCatornParameter(HookDirection_).ToString();
                    CsPakingData.M_DicPakingData["PoTypeSignal"] ="3";
                    if (CsRWPlc.mCdata.IsConnect())
                    {
                        if (dt.Rows[0]["喷码模式"].ToString().Trim().Contains("国内"))
                        {
                            CsMarking.M_MarkParameter = "NONet";
                        }
                        else if (dt.Rows[0]["喷码模式"].ToString().Trim().Contains("国外"))
                        {
                            CsMarking.M_MarkParameter = "Net";
                        }
                    }
                    else
                    {
                        MessageBox.Show("设备未启动，请先启动设备");
                        return false;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }
        /// <summary>
        /// 完成码垛后切换到下一批码垛箱子的数据行提取码垛数据
        /// </summary>
        /// <param name="dr"></param>
        /// 
        public static void SetToYYKGUPalletizing(DataRow dr)
        {
            try
            {
                Dictionary<string, string> DicMarking_colorcode = new Dictionary<string, string>();
                //优衣库喷码信息
                DicMarking_colorcode.Clear();//清除之前
                DicMarking_colorcode["Color_Code"] = dr["Color_Code"].ToString().Trim();
                DicMarking_colorcode["Order_No"] = dr["Order_No"].ToString().Trim();
                DicMarking_colorcode["Order_Qty"] = dr["Order_Qty"].ToString().Trim();
                DicMarking_colorcode["YK_guid"] = dr["YK_guid"].ToString().Trim();
                DicMarking_colorcode["Quantity"] = dr["CurrentPackingNum"].ToString().Trim();
                DicMarking_colorcode["Warehouse"] = dr["Warehouse"].ToString().Trim();
                DicMarking_colorcode["Set_Code"] = dr["Set_Code"].ToString().Trim();
                DicMarking_colorcode["FinishPalletizingNum"] = dr["FinishPalletizingNum"].ToString().Trim();
                DicMarking_colorcode["END"] = "";
                CsPakingData.M_list_Dic_YYKGUPalletizing.Add(DicMarking_colorcode);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 重新加载码垛类型队列
        /// </summary>
        public static void UpdatePalletizingtype()
        {
            try
            {
                #region 重新加载码垛类型队列
                CsPakingData.M_list_Dic_palletizingtype.Clear();
                DataTable dtPalletizingtype = CsFormShow.GoSqlSelect( string.Format("SELECT*FROM dbo.PalletizingtypeTable WHERE DeviceName='{0}'  ORDER BY Entrytime  ", CSReadPlc.DevName));
                if (dtPalletizingtype.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPalletizingtype.Rows.Count; i++)
                    {
                        DataRow dr = dtPalletizingtype.Rows[i];
                        SetToPalletizingtype(dr);
                    }
                }
                #endregion
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 码垛类型队列初始化
        /// </summary>
        /// <param name="dr"></param>
        public static void SetToPalletizingtype(DataRow dr)
        {
            try
            {
                Dictionary<string, string> Dic_Palletizingtype = new Dictionary<string, string>();
                //优衣库喷码信息
                Dic_Palletizingtype.Clear();//清除之前
                Dic_Palletizingtype["DeviceName"] = dr["DeviceName"].ToString().Trim();
                Dic_Palletizingtype["potype"] = dr["potype"].ToString().Trim();
                Dic_Palletizingtype["guid"] = dr["guid"].ToString().Trim();
                Dic_Palletizingtype["Entrytime"] = dr["Entrytime"].ToString().Trim();
                CsPakingData.M_list_Dic_palletizingtype.Add(Dic_Palletizingtype);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 更新当前扫码信息
        /// </summary>
        public static void UpdateCurrentScan()
        {
            try
            {
                DataTable dt_now = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM(SELECT potype,Entrytime FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}' UNION ALL SELECT 'GAP' potype, Entrytime FROM dbo.MarkingLineUp WHERE ISNULL(IsEndScan, 0) <> 1 AND DeviceName='{0}') AS LINE ORDER BY LINE.Entrytime", CSReadPlc.DevName.Trim()));
                if (dt_now.Rows.Count > 0)
                {
                    if (dt_now.Rows[0]["potype"].ToString().Contains("GU") || dt_now.Rows[0]["potype"].ToString().Contains("UNIQLO"))
                    {
                        DataTable dt_line = CsFormShow.GoSqlSelect(string.Format(" SELECT TOP 1 * FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        DataTable dt_Markline = CsFormShow.GoSqlSelect(string.Format(" SELECT TOP 1 * FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        M_stcScanData.strPoTiaoMa = dt_line.Rows[0]["SKU_Code"].ToString().Trim();
                        M_stcScanData.strpo = dt_line.Rows[0]["Order_No"].ToString().Trim();
                        M_stcScanData.stridentification = dt_line.Rows[0]["Warehouse"].ToString().Trim();
                        M_stcScanData.potype = dt_line.Rows[0]["potype"].ToString().Trim();
                        M_stcScanData.CurrentPackingNum = dt_line.Rows[0]["CurrentPackingNum"].ToString().Trim();
                        M_stcScanData.Color_Code = dt_line.Rows[0]["Color_Code"].ToString().Trim();
                        M_stcScanData.Set_Code = dt_line.Rows[0]["Set_Code"].ToString().Trim();
                        M_stcScanData.IsUnpacking = dt_line.Rows[0]["IsUnpacking"].ToString().Trim();
                        M_stcScanData.MarkingIsshieid = dt_Markline.Rows[0]["MarkingIsshieid"].ToString().Trim();
                        M_stcScanData.IsPalletizing = dt_line.Rows[0]["IsPalletizing"].ToString().Trim();
                        M_stcScanData.IsEndScan = bool.Parse(dt_line.Rows[0]["IsEndScan"].ToString().Trim());
                        M_stcScanData.IsEndPacking = bool.Parse(dt_line.Rows[0]["IsEndPacking"].ToString().Trim());
                        M_stcScanData.FinishPackingNum = dt_line.Rows[0]["FinishPackingNum"].ToString().Trim();
                        DataTable dtyykgucurrent = CsFormShow.GoSqlSelect(string.Format("SELECT *FROM dbo.YYK_GU_Current WHERE  DeviceName = '{0}'", CSReadPlc.DevName.Trim()));
                        M_stcScanData.dtCurrentData = dtyykgucurrent;
                    }
                    else
                    {
                        DataTable dt_line = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 *,(SELECT TOP 1 CASE when ISNULL(finishmarknumber,'') IN ('',NULL) THEN 0 ELSE finishmarknumber END FROM dbo.PACKING AS p WHERE p.guid=dbo.MarkingLineUp.packing_guid) AS 'finishmarknumber' FROM dbo.MarkingLineUp  WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        DataTable dt_Markline = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM dbo.MarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        DataTable dtGAPcurrent = CsFormShow.GoSqlSelect(string.Format("SELECT *FROM dbo.GAP_Current WHERE DeviceName='{0}'", CSReadPlc.DevName.Trim()));
                        //int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                        M_stcScanData.IsEndScan = bool.Parse(dt_line.Rows[0]["IsEndScan"].ToString().Trim());
                        M_stcScanData.IsEndPacking = bool.Parse(dt_line.Rows[0]["IsEndPacking"].ToString().Trim());
                        M_stcScanData.strPoTiaoMa = dt_line.Rows[0]["TiaoMaVal"].ToString().Trim();
                        M_stcScanData.strpo = dt_line.Rows[0]["订单号码"].ToString().Trim();
                        M_stcScanData.stridentification = dt_line.Rows[0]["从"].ToString().Trim();
                        M_stcScanData.FinishmarknumberTotal =int.Parse( dt_line.Rows[0]["finishmarknumber"].ToString().Trim());
                        string wai_code = dt_line.Rows[0]["外箱代码"].ToString().Trim();
                        if (wai_code.ToUpper() != "C4" && wai_code.ToUpper() != "C6")
                        {
                            M_stcScanData.potype = "GAP国内";
                        }
                        else
                        {
                            M_stcScanData.potype = "GAP国外";
                        }
                        M_stcScanData.Color_Code = "";
                        M_stcScanData.Set_Code = "";
                        M_stcScanData.IsUnpacking = dt_line.Rows[0]["IsUnpacking"].ToString().Trim();
                        M_stcScanData.MarkingIsshieid = dt_Markline.Rows[0]["MarkingIsshieid"].ToString().Trim();
                        M_stcScanData.IsPalletizing = dt_line.Rows[0]["IsPalletizing"].ToString().Trim();
                        M_stcScanData.CurrentPackingNum = dt_line.Rows[0]["CurrentPackingNum"].ToString().Trim();
                        M_stcScanData.FinishPackingNum = dt_line.Rows[0]["FinishPackingNum"].ToString().Trim();
                        M_stcScanData.dtCurrentData = dtGAPcurrent;
                    }
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
        }
        /// <summary>
        /// 开机更新当前扫码信息
        /// </summary>
        public static bool PowerOnUpdateCurrentScan()
        {
            try
            {
                DataTable dt_now = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM(SELECT  potype,Entrytime FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}'  UNION ALL SELECT 'GAP' potype, Entrytime FROM dbo.MarkingLineUp WHERE ISNULL(IsEndScan, 0) <> 1 AND DeviceName='{0}')  AS LINE ORDER BY LINE.Entrytime", CSReadPlc.DevName.Trim()));
                if (dt_now.Rows.Count > 0)
                {
                    string Layernum = "";
                    if (dt_now.Rows[0]["potype"].ToString().Contains("GU") || dt_now.Rows[0]["potype"].ToString().Contains("UNIQLO"))
                    {
                        DataTable dt_line = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndScan,0)<>1  AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        DataTable dt_Markline = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1  AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        M_stcScanData.strPoTiaoMa = dt_line.Rows[0]["SKU_Code"].ToString().Trim();
                        M_stcScanData.strpo = dt_line.Rows[0]["Order_No"].ToString().Trim();
                        M_stcScanData.stridentification = dt_line.Rows[0]["Warehouse"].ToString().Trim();
                        M_stcScanData.potype = dt_line.Rows[0]["potype"].ToString().Trim();
                        M_stcScanData.CurrentPackingNum = dt_line.Rows[0]["CurrentPackingNum"].ToString().Trim();
                        M_stcScanData.FinishPackingNum = dt_line.Rows[0]["FinishPackingNum"].ToString().Trim();
                        M_stcScanData.Color_Code = dt_line.Rows[0]["Color_Code"].ToString().Trim();
                        M_stcScanData.Set_Code = dt_line.Rows[0]["Set_Code"].ToString().Trim();
                        M_stcScanData.IsUnpacking = dt_line.Rows[0]["IsUnpacking"].ToString().Trim();
                        M_stcScanData.IsPalletizing = dt_line.Rows[0]["IsPalletizing"].ToString().Trim();
                        M_stcScanData.IsPackingRun = dt_line.Rows[0]["IsPackingRun"].ToString().Trim();
                        M_stcScanData.IsYaMaHaRun = dt_line.Rows[0]["IsYaMaHaRun"].ToString().Trim();
                        M_stcScanData.IsSealingRun = dt_line.Rows[0]["IsSealingRun"].ToString().Trim();
                        M_stcScanData.IsScanRun = dt_line.Rows[0]["IsScanRun"].ToString().Trim();
                        M_stcScanData.HookDirection = dt_line.Rows[0]["HookDirection"].ToString().Trim();
                        M_stcScanData.PartitionType = dt_line.Rows[0]["PartitionType"].ToString().Trim();
                        M_stcScanData.IsEndScan = bool.Parse(dt_line.Rows[0]["IsEndScan"].ToString().Trim());
                        M_stcScanData.IsEndPacking = bool.Parse(dt_line.Rows[0]["IsEndPacking"].ToString().Trim());
                        DataTable dtyykgucurrent = CsFormShow.GoSqlSelect(string.Format("SELECT *FROM dbo.YYK_GU_Current WHERE  DeviceName = '{0}'", CSReadPlc.DevName.Trim()));
                        M_stcScanData.dtCurrentData = dtyykgucurrent;
                        if (dt_Markline.Rows.Count > 0)
                        {
                            M_stcScanData.MarkingIsshieid = dt_Markline.Rows[0]["MarkingIsshieid"].ToString().Trim();
                            M_stcScanData.PrintIsshieid = dt_Markline.Rows[0]["PrintIsshieid"].ToString().Trim();
                            M_stcScanData.PrintIsshieid = dt_Markline.Rows[0]["PrintIsshieid"].ToString().Trim();
                        }
                        #region 计算箱件数
                        string Set_Code = dt_line.Rows[0]["Set_Code"].ToString().Trim();
                        string IsUnpacking = dt_line.Rows[0]["IsUnpacking"].ToString().Trim();
                        DataTable dtLayernum = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YOUYIKUDO WHERE guid='{0}'", dt_line.Rows[0]["YK_guid"].ToString().Trim()));
                        if (dtLayernum.Rows.Count > 0)
                        {
                            Layernum = CsGetData.GetLayernum(dtLayernum, IsUnpacking, "YYK").ToString();
                            M_stcScanData.Layernum = int.Parse(Layernum.Trim());
                        }
                        #endregion
                        //设置切单写入plc参数
                        string CurrentPackingNum_v = (int.Parse(M_stcScanData.CurrentPackingNum) - int.Parse(M_stcScanData.FinishPackingNum)).ToString();
                        bool b_v = SetWritePlcData(M_stcScanData.strpo, M_stcScanData.strPoTiaoMa, M_stcScanData.stridentification, CurrentPackingNum_v, M_stcScanData.Color_Code, M_stcScanData.Set_Code, Layernum, M_stcScanData.PartitionType, M_stcScanData.IsPackingRun, M_stcScanData.IsYaMaHaRun, M_stcScanData.IsSealingRun, M_stcScanData.MarkingIsshieid, M_stcScanData.PrintIsshieid, M_stcScanData.IsPalletizingRun, M_stcScanData.IsScanRun, M_stcScanData.HookDirection, "不运行", "不运行", M_stcScanData.potype);
                        if (!b_v)
                        {
                            return false;
                        }
                        //写入条码
                        M_stcScanData.M_List_WriteToPlcSku = CsGetData.GetSkuWriteToPlc(M_stcScanData.dtCurrentData, dt_line.Rows[0]["potype"].ToString().Trim(), dt_line.Rows[0]["IsUnpacking"].ToString().Trim());
                        M_stcScanData.M_WriteToPlcSku = "start";
                    }
                    else
                    {
                        DataTable dt_line = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM dbo.MarkingLineUp WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        DataTable dt_Markline = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM dbo.MarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        DataTable dtGAPcurrent = CsFormShow.GoSqlSelect(string.Format("SELECT *FROM dbo.GAP_Current WHERE DeviceName='{0}'", CSReadPlc.DevName.Trim()));
                        //int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                        if (dt_line.Rows.Count > 0)
                        {
                            M_stcScanData.IsEndScan = bool.Parse(dt_line.Rows[0]["IsEndScan"].ToString().Trim());
                            M_stcScanData.IsEndPacking = bool.Parse(dt_line.Rows[0]["IsEndPacking"].ToString().Trim());
                            M_stcScanData.strPoTiaoMa = dt_line.Rows[0]["TiaoMaVal"].ToString().Trim();
                            M_stcScanData.strpo = dt_line.Rows[0]["订单号码"].ToString().Trim();
                            M_stcScanData.stridentification = dt_line.Rows[0]["从"].ToString().Trim();
                            string wai_code = dt_line.Rows[0]["外箱代码"].ToString().Trim();
                            if (wai_code.ToUpper() != "C4" && wai_code.ToUpper() != "C6")
                            {
                                M_stcScanData.potype = "GAP国内";
                            }
                            else
                            {
                                M_stcScanData.potype = "GAP国外";
                            }
                        }
                        M_stcScanData.Color_Code = "";
                        M_stcScanData.Set_Code = "";
                        M_stcScanData.IsUnpacking = dt_line.Rows[0]["IsUnpacking"].ToString().Trim();
                        if (dt_Markline.Rows.Count > 0)
                        {
                            M_stcScanData.MarkingIsshieid = dt_Markline.Rows[0]["MarkingIsshieid"].ToString().Trim();
                            M_stcScanData.PrintIsshieid = dt_Markline.Rows[0]["PrintIsshieid"].ToString().Trim();
                            M_stcScanData.GAPIsPrintRun = dt_Markline.Rows[0]["GAPIsPrintRun"].ToString().Trim();
                            M_stcScanData.GAPSideIsshieid = dt_Markline.Rows[0]["GAPSideIsshieid"].ToString().Trim();
                            M_stcScanData.IsPalletizingRun = dt_Markline.Rows[0]["IsPalletizingRun"].ToString().Trim();
                        }
                        M_stcScanData.IsPalletizing = dt_line.Rows[0]["IsPalletizing"].ToString().Trim();
                        M_stcScanData.IsPackingRun = dt_line.Rows[0]["IsPackingRun"].ToString().Trim();
                        M_stcScanData.IsYaMaHaRun = dt_line.Rows[0]["IsYaMaHaRun"].ToString().Trim();
                        M_stcScanData.IsSealingRun = dt_line.Rows[0]["IsSealingRun"].ToString().Trim();
                        M_stcScanData.IsScanRun = dt_line.Rows[0]["IsScanRun"].ToString().Trim();
                        M_stcScanData.HookDirection = dt_line.Rows[0]["HookDirection"].ToString().Trim();
                        M_stcScanData.PartitionType = dt_line.Rows[0]["PartitionType"].ToString().Trim();
                        M_stcScanData.CurrentPackingNum = dt_line.Rows[0]["CurrentPackingNum"].ToString().Trim();
                        M_stcScanData.FinishPackingNum = dt_line.Rows[0]["FinishPackingNum"].ToString().Trim();
                        M_stcScanData.dtCurrentData = dtGAPcurrent;
                        #region 计算箱件数
                        string count_bao = dt_line.Rows[0]["内包装计数"].ToString().Trim();
                        string IsUnpacking = dt_line.Rows[0]["IsUnpacking"].ToString().Trim();
                        DataTable dtLayernum = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", dt_line.Rows[0]["packing_guid"].ToString().Trim()));
                        if (dtLayernum.Rows.Count > 0)
                        {
                            Layernum = CsGetData.GetLayernum(dtLayernum, IsUnpacking, "GAP").ToString();
                            M_stcScanData.Layernum = int.Parse(Layernum.Trim());
                        }
                        #endregion
                        //设置切单写入plc参数
                        string CurrentPackingNum_v = (int.Parse(M_stcScanData.CurrentPackingNum) - int.Parse(M_stcScanData.FinishPackingNum)).ToString();

                        bool b_v = SetWritePlcData(M_stcScanData.strpo, M_stcScanData.strPoTiaoMa, M_stcScanData.stridentification, CurrentPackingNum_v, M_stcScanData.Color_Code, M_stcScanData.Set_Code, Layernum, M_stcScanData.PartitionType, M_stcScanData.IsPackingRun, M_stcScanData.IsYaMaHaRun, M_stcScanData.IsSealingRun, M_stcScanData.MarkingIsshieid, M_stcScanData.PrintIsshieid, M_stcScanData.IsPalletizingRun, M_stcScanData.IsScanRun, M_stcScanData.HookDirection, M_stcScanData.GAPIsPrintRun, M_stcScanData.GAPSideIsshieid, M_stcScanData.potype);
                        if (!b_v)
                        {
                            return false;
                        }
                        //写入条码
                        M_stcScanData.M_List_WriteToPlcSku = CsGetData.GetSkuWriteToPlc(dt_line, "GAP".ToString().Trim(), dt_line.Rows[0]["IsUnpacking"].ToString().Trim());
                        M_stcScanData.M_WriteToPlcSku = "start";
                    }
                }
                DataTable dt_Marknow = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM(SELECT  potype,Entrytime FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1 AND DeviceName='{0}'  UNION ALL SELECT 'GAP' potype, Entrytime FROM dbo.MarkingLineUp WHERE ISNULL(IsEndMarking, 0) <> 1 AND DeviceName='{0}')  AS LINE ORDER BY LINE.Entrytime", CSReadPlc.DevName.Trim()));
                if (dt_Marknow.Rows.Count > 0)
                {
                    DataTable dt_Markline = new DataTable();
                    if (dt_Marknow.Rows[0]["potype"].ToString().Contains("GU") || dt_Marknow.Rows[0]["potype"].ToString().Contains("UNIQLO"))
                    {
                        dt_Markline = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1  AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                    }
                    else
                    {
                        dt_Markline = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM dbo.MarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1  AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                    }
                    M_stcScanData.MarkingIsshieid = dt_Markline.Rows[0]["MarkingIsshieid"].ToString().Trim();
                    M_stcScanData.PrintIsshieid = dt_Markline.Rows[0]["PrintIsshieid"].ToString().Trim();
                    M_stcScanData.IsPalletizingRun = dt_Markline.Rows[0]["IsPalletizingRun"].ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
            return true;
        }
        /// <summary>
        /// 扫描重新计数
        /// </summary>
        public static void ScanningCountInit()
        {
            try
            {
                if (!M_stcScanData.potype.Contains("GAP"))
                {
                    for (int j = 0; j < M_stcScanData.dtCurrentData.Rows.Count; j++)//判断是否本箱全部放完
                    {
                        M_stcScanData.dtCurrentData.Rows[j]["IsEndPacking"] = 0;
                        M_stcScanData.dtCurrentData.Rows[j]["FinishPackingNum"] = "0";//重新计数
                    }
                }
                CsScanning.scaningend = "";
                CsScanning.count_unit = 0;
                //MessageBox.Show("开始重新计数");
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
        }
        /// <summary>
        /// 手动设置GAP起始喷码箱号
        /// </summary>
        /// <param name="StartNum_"></param>
        public static void SetMarkingStartNum(string packing_guid_,int MarkingNum_)
        {
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    //更新表数据
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET  packingtotal={0}-isnull(从,0), finishmarknumber={0}-isnull(从,0),finishPalletizingnumber={0}-isnull(从,0) WHERE  guid='{1}'", MarkingNum_, packing_guid_));
                    //更新订单队列表
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET /*FinishPackingNum={2}-isnull(从,0),*/ Cartoncount={0}-isnull(从,0)+1,finishmarknumber={0}-isnull(从,0),FinishPalletizingNum={0}-isnull(从,0) WHERE packing_guid='{1}'", MarkingNum_, packing_guid_, MarkingNum_));
                    
                    //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET IsEndScan=CASE WHEN FinishPackingNum=ISNULL(CurrentPackingNum,0) THEN 1 ELSE 0 END,IsEndPacking=CASE WHEN FinishPackingNum=ISNULL(CurrentPackingNum,0) THEN 1 ELSE 0 END,IsEndMarking=CASE WHEN finishmarknumber=ISNULL(CurrentPackingNum,0) THEN 1 ELSE 0 END WHERE packing_guid='{0}'", packing_guid_));

                    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.PRINTLineuUp WHERE DeviceName='{0}'",CSReadPlc.DevName.Trim()));
                    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM PalletizingtypeTable WHERE DeviceName='{0}' AND potype='GAP'", CSReadPlc.DevName.Trim()));       
                    //清除缓存
                    CsPakingData.M_list_DicMarkingNONet.Clear();
                    CsPakingData.M_list_DicMarkingNet.Clear();
                    CsPakingData.M_list_Dic_GAPPalletizing.Clear();
                    CsPakingData.M_list_Dic_palletizingtype.Clear();
                    M_stcPrintData.lis_printdata.Clear();
                    //喷码队列初始化      
                    UpdateGAPLineUp();
                    //将异常未能打印完成的标签加入队列中
                    UpdatePrintLine();
                    //码垛队列初始化
                    UpdateGAPPalletizingLineUp();
                    UpdatePalletizingtype();
                }
                MessageBox.Show("设置完成");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 更新打印队列
        /// </summary>
        public static void UpdatePrintLine()
        {
            try
            {
                //将异常未能打印完成的标签加入队列中
                M_stcPrintData.lis_printdata.Clear();
                DataTable dtprint = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM PRINTLineuUp WHERE DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                for (int i = 0; i < dtprint.Rows.Count; i++)
                {
                    CsPrint.PrintLineuUp(dtprint.Rows[i]);
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
        /// <summary>
        /// 启动关工程序
        /// </summary>
        public static void SetupIps()
        {
            try
            {
                bool b_v = false;
                System.Diagnostics.Process[] processList = System.Diagnostics.Process.GetProcesses();
                foreach (System.Diagnostics.Process process in processList)
                {
                    if (process.ProcessName == "IPSMainProgram")
                    {
                        b_v = true;
                    }
                }
                if (!b_v)
                {
                    System.Diagnostics.Process.Start(@"C:\Users\Administrator.USER-20190414RK\Desktop\ips\IPSMainProgram\IPSMainProgram\bin\Debug\IPSMainProgram.exe");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
    }
}
