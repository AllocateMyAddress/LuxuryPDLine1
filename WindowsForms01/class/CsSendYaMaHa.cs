﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using Soketprint;
using MCData;
using Image_recognition;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.InteropServices;
using SoketYaMaHa;
using GetData;
namespace WindowsForms01
{
    public class CsSendYaMaHa
    {
       public  struct PoData
        {
            public static string Orientation = "";//摆放方向
            public static string LineNum = "";//行
            public static string M_strcolumn = "";//列
            public static string M_strLayernum = "";//层数
            public static string M_strLong = "";//箱子长
            public static string M_strWide = "";//箱宽
            public static string M_strHeghit = "";//箱宽
            public static string M_strThick = "";//箱高
            public static string GoodWeight = "";//重量
            public static string M_strcomingtype = "";//来料方式
            public static string M_strSwitchsignal = "";//切换信号
            public static string M_Grabspeed = "";//抓取速度
            public static string Jianspeed = "";//抓取速度
            public static string IsRotate = "";//旋转
            public static string HookDirection = "";//旋转
            public static string M_strNowLong = "";//箱子长
            public static string M_strNowWide = "";//箱宽
            public static string M_strNowHeghit = "";//箱宽
        }
        public static int IsLeakage = 0;//是否漏料
        public static int YaMaHaCount = 0;//雅马哈抓取计数
        public static bool M_IsSendOk = false;
        public static bool M_IsGetYaMaHaData = false;
        public static short M_GetYaMaHaData = 0;
        //public static short M_GetYaMaHaNum = 0;
        public static bool M_strIsSwitch = false;//切换信号
       public static object M_lockerTrg = new object();
        public static string recval = "";
        public static string M_sendflag = "";
        public static string M_GetNumflag = "";
        public static string M_YaMaHaToPlcflag = "";
        public static int count_connnet = 0;
        //public static string M_path = @"D:\"+CSReadPlc.DevName+@"\log\marklog\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
        public static int flag_switch = 0;
        static MCdata mCdata = new MCdata();
        public static bool Isconnetion=false;
        GetPlcBao csGetBao;
        public  CsSendYaMaHa(MCdata mcdata, GetPlcBao bao)
        {
            mCdata = mcdata;
            csGetBao = bao;
        }
      
        public  void Sendchange(string YaMaHaIP,string YaMaHaIPoint)
        {
            SoketYaMaHa.SoketYaMaHa SoketYaMaHa = new SoketYaMaHa.SoketYaMaHa(CSReadPlc.DevName);
            Dictionary<string, object> DicPodata = new Dictionary<string, object>();
            Thread ThSendYaMaHa = new Thread(SendYaMaHa);
            ThSendYaMaHa.IsBackground = true;
            ThSendYaMaHa.Start();
            void SendYaMaHa()
            {
                Cslogfun.WriteToLog("开启发送机器人信息线程ip:"+ YaMaHaIP+"端口:"+ YaMaHaIPoint);
                while (true)
                {
                    try
                    {
                        Isconnetion = SoketYaMaHa.SoketConnetion(YaMaHaIP, YaMaHaIPoint);
                        if (Isconnetion)
                        {
                            #region 发送装箱数据
                            if (PoData.M_strSwitchsignal == "1" && M_strIsSwitch == false && string.IsNullOrEmpty(recval))
                            {
                                string str = PoData.Orientation + "," + PoData.LineNum + "," + PoData.M_strcolumn + "," + PoData.M_strLayernum + "," + PoData.M_strLong + "," + PoData.M_strWide + "," + PoData.M_strThick + "," + PoData.GoodWeight + "," + PoData.M_strcomingtype + "," + PoData.M_Grabspeed + "," + PoData.Jianspeed + "," + PoData.IsRotate + "," + PoData.HookDirection + ",";
                                recval = SoketYaMaHa.Gocmd(str);
                                if (!recval.Contains("A1"))
                                {
                                    recval = "";
                                    Cslogfun.WriteToyamahaLog("发送失败一次"+",信息："+ str+",获取反馈信息："+ recval);
                                    Thread.Sleep(2000);
                                }
                                if (recval == "A1")
                                {
                                    M_strIsSwitch = true;
                                    Cslogfun.WriteToyamahaLog("发送机器人成功" + ",信息：" + str);
                                    TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                    while (PoData.M_strSwitchsignal != "0")
                                    {
                                        TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                        TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                        if (ts.Seconds > 5)
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region 读取机器人计数
                            //string val_v = "";
                            //if (string.IsNullOrEmpty(M_GetNumflag))
                            //{
                            //    val_v = SoketYaMaHa.Gocmd("@?SGI3");
                            //    M_GetNumflag = "start";
                            //}
                            //if (!val_v.Contains( "超时") && !val_v .Contains("A1") && !val_v.Contains( "断开") && !string.IsNullOrEmpty(val_v))
                            //{
                            //    // Cslogfun.WriteToyamahaLog("执行发送指令一次" + ",收到信息：" + val_v);
                            //    M_YaMaHaToPlcflag = "start";
                            //    M_GetNumflag = "";
                            //    M_GetYaMaHaData = short.Parse(val_v);
                            //}
                            //if (string.IsNullOrEmpty(val_v))
                            //{
                            //    M_GetNumflag = "";
                            //}
                            #endregion
                        }
                        else
                        {
                            M_GetNumflag = "";
                            Isconnetion = SoketYaMaHa.SoketConnetion(YaMaHaIP, YaMaHaIPoint);
                            if (!Isconnetion)
                            {
                                CsFormShow.MessageBoxFormShow("机器人网络通信失败，请检查网络是否通畅或者尝试重启软件");
                            }
                            else
                            {
                                CsFormShow.MessageBoxFormShow("机器人重新连接成功");
                            }
                        }
                        Thread.Sleep(500);
                    }
                    catch (Exception err)
                    {
                        Cslogfun.WriteToLog(err.ToString());
                        CsFormShow.MessageBoxFormShow("提示：" + err.ToString());
                    }
                }
            }
        }
    }

}
