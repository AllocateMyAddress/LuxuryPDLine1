﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using Soketprint;
using MCData;
//using Serialprint;
using Serial;
using System.IO;
using System.IO.Ports;
using Image_recognition;
using GetData;
using System.Data;
using System.Windows.Forms;
using A.BLL;

namespace WindowsForms01
{
    public class CsPrint
    {
        static MCdata mCdata = new MCdata();
        GetPlcBao csGetBao;
        Soketprint.SoketPrint soketprint;
        static A.BLL.newsContent content1 = new newsContent();
        public static object oblock = new object();
        public static int M_printsignal = 0;//reset
        public static int M_Testprintsignal = 0;
        public static int PrintLineType = 0;
        public static string M_printsignalreset = "";
        public static string M_Testprintsignalreset = "";
        public static string M_serialsignalreset = "";
        public static int M_serialsignal = 0;
        public static bool Isprintconnent = false;//喷码机是否连接
        string PrintIP = "";
        string PrintIPoint = "";
        TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
        public CsPrint(MCdata mcdata, GetPlcBao bao)
        {
            mCdata = mcdata;
            csGetBao = bao;
        }

        #region 以太网打印
        //以太网打印
        public void tcpprint(string PrintIP_, string PrintIPoint_)
        {
            try
            {
                PrintIP = PrintIP_;
                PrintIPoint = PrintIPoint_;
                soketprint = new SoketPrint(CSReadPlc.DevName);
                Isprintconnent = soketprint.SoketprintConnetion(PrintIP, PrintIPoint);
                Thread th1 = new Thread(TcpSend);
                th1.IsBackground = true;
                th1.Start();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.Message);
            }
            void TcpSend()
            {
                try
                {
                    while (true)
                    {
                        Thread.Sleep(10);
                        Isprintconnent = soketprint.SoketprintConnetion(PrintIP, PrintIPoint);
                        if (mCdata.IsConnect()&& Isprintconnent)
                        {
                            if (M_printsignal == 1 && CsPrint.PrintLineType == 2 && CSReadPlc.DevName.Trim().Contains("plc2") && string.IsNullOrEmpty(M_printsignalreset))
                            {
                                if (string.IsNullOrEmpty(CSReadPlc.EditionFlag))
                                {
                                    if (M_stcPrintData.lis_printdata.ToList().Count > 0)
                                    {
                                        M_printsignalreset = "start";
                                        SetvalToPrint(soketprint, PrintIP, PrintIPoint, M_stcPrintData.M_PrintPath);//赋值打印
                                        DeletePrintLineUpFromSql(M_stcPrintData.lis_printdata.ToList()[0]);//从数据库删除列队中本次打印页
                                        M_stcPrintData.lis_printdata.Remove(M_stcPrintData.lis_printdata.ToList()[0]);//删除列队缓存中的本次打印 
                                        TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                        while (M_printsignal != 0)
                                        {
                                            TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                            TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                            if (ts.Seconds > 5)
                                            {
                                                break;
                                            }
                                        }
                                        Cslogfun.WriteToprintgLog(CsPrint.PrintLineType + "号线，" + "开始以太网打印一次");
                                    }
                                }
                                else
                                {
                                    #region 关工对接
                                    CsGetData.SetToPrintVaria(CsPublicVariablies.PrintData.Po_guid, CsPublicVariablies.PrintData.Cartorn);
                                    if (CsPublicVariablies.PrintData.IsPrint.Contains("运行"))
                                    {
                                        SetPrintConnect(CsPublicVariablies.PrintData.Po_guid, CsPublicVariablies.PrintData.OrderNo, CsPublicVariablies.PrintData.Cartorn, CsPublicVariablies.PrintData.Packing当前序列号);
                                        if (M_stcPrintData.lis_printdata.ToList().Count > 0)
                                        {
                                            M_printsignalreset = "start";
                                            SetvalToPrint(soketprint, PrintIP, PrintIPoint, M_stcPrintData.M_PrintPath);//赋值打印
                                            M_stcPrintData.lis_printdata.Remove(M_stcPrintData.lis_printdata.ToList()[0]);//删除列队缓存中的本次打印 
                                            TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                            while (M_printsignal != 0)
                                            {
                                                TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                                TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                                if (ts.Seconds > 5)
                                                {
                                                    break;
                                                }
                                            }
                                            Cslogfun.WriteToprintgLog(CsPrint.PrintLineType + "号线，" + "开始以太网打印一次");
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else if (M_printsignal == 1 && CsPrint.PrintLineType != 2 && CSReadPlc.DevName.Trim().Contains("plc2") && string.IsNullOrEmpty(M_printsignalreset))
                            {
                                Thread.Sleep(1500);
                                if (string.IsNullOrEmpty(CSReadPlc.EditionFlag))
                                {
                                    M_stcPrintData.lis_OtherLine_printdata.Clear();
                                    DataTable dtOtherprint = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM PRINTLineuUp WHERE DeviceName<>'{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                                    for (int i = 0; i < dtOtherprint.Rows.Count; i++)
                                    {
                                        CsPrint.OtherLinePrintLineuUp(dtOtherprint.Rows[i]);
                                    }
                                    if (M_stcPrintData.lis_OtherLine_printdata.ToList().Count > 0)
                                    {
                                        M_printsignalreset = "start";
                                        SetvalToOtherPrint(soketprint, PrintIP, PrintIPoint, M_stcPrintData.M_PrintPath);//赋值打印
                                        DeletePrintLineUpFromSql(M_stcPrintData.lis_OtherLine_printdata.ToList()[0]);//从数据库删除列队中本次打印页
                                        M_stcPrintData.lis_OtherLine_printdata.Remove(M_stcPrintData.lis_OtherLine_printdata.ToList()[0]);//删除列队缓存中的本次打印 
                                        TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                        while (M_printsignal != 0)
                                        {
                                            TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                            TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                            if (ts.Seconds > 5)
                                            {
                                                break;
                                            }
                                        }
                                        Cslogfun.WriteToprintgLog(CsPrint.PrintLineType + "号线，" + "开始以太网打印一次");
                                    }
                                }
                                else//关工对接
                                {
                                    DataTable dt = CsFormShow.GoSqlSelect("SELECT TOP 1 * FROM tPrintData");
                                    if (dt.Rows.Count > 0)
                                    {
                                        CsPublicVariablies.PrintData.Po_guid = dt.Rows[0]["po_guid"].ToString().Trim();
                                        CsPublicVariablies.PrintData.CartornNum = int.Parse(dt.Rows[0]["CartornNum"].ToString().Trim());
                                        CsGetData.SetToPrintVaria(CsPublicVariablies.PrintData.Po_guid, CsPublicVariablies.PrintData.CartornNum);
                                        if (CsPublicVariablies.PrintData.IsPrint.Contains("运行"))
                                        {
                                            SetPrintConnect(CsPublicVariablies.PrintData.Po_guid, CsPublicVariablies.PrintData.OrderNo, CsPublicVariablies.PrintData.Cartorn, CsPublicVariablies.PrintData.Packing当前序列号);
                                            if (M_stcPrintData.lis_printdata.ToList().Count > 0)
                                            {
                                                M_printsignalreset = "start";
                                                SetvalToPrint(soketprint, PrintIP, PrintIPoint, M_stcPrintData.M_PrintPath);//赋值打印
                                                CsFormShow.GoSqlUpdateInsert("DELETE FROM tPrintData");
                                                M_stcPrintData.lis_printdata.Remove(M_stcPrintData.lis_printdata.ToList()[0]);//删除列队缓存中的本次打印 
                                                TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                                while (M_printsignal != 0)
                                                {
                                                    TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                                    TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                                    if (ts.Seconds > 5)
                                                    {
                                                        break;
                                                    }
                                                }
                                                Cslogfun.WriteToprintgLog(CsPrint.PrintLineType + "号线，" + "开始以太网打印一次");
                                            }
                                        }
                                    }
                                }
                            }
                            //测试打印
                            if (M_Testprintsignal == 1 && M_Testprintsignalreset == "" && M_stcPrintData.TestPrintFlag.Contains("start"))
                            {
                                M_Testprintsignalreset = "start";
                                TestToPrint(soketprint, PrintIP, PrintIPoint, M_stcPrintData.M_PrintPath);
                                Thread.Sleep(3000);
                                Cslogfun.WriteTomarkingLog("开始以太网测试打印");
                            }
                            if (M_stcPrintData.ManualFlag.Contains("start"))
                            {
                                M_printsignalreset = "start";
                                M_stcPrintData.ManualFlag = "";
                                ManualToPrint(soketprint, PrintIP, PrintIPoint, M_stcPrintData.M_PrintPath);
                                Thread.Sleep(3000);
                                Cslogfun.WriteTomarkingLog(" 开始手动执行打印");
                            }
                        }
                        if (!Isprintconnent)
                        {
                            Isprintconnent = soketprint.SoketprintConnetion(PrintIP, PrintIPoint);
                            if (!Isprintconnent)
                            {
                                CsFormShow.MessageBoxFormShow("打印机网络连接断开，请检查网络是否通畅或者尝试重启打印机和软件");
                            }
                            else
                            {
                                CsFormShow.MessageBoxFormShow("打印机重新连接成功");
                            }
                        }
                    }
                }
                catch (Exception err)
                {
                    Cslogfun.WriteToLog(err);
                    MessageBox.Show("提示：" + err.ToString());
                }
            }
        }
        #endregion 
        #region 串口打印
        public void serial_print()
        {
            try
            {
                Serial.CsSerial serialprint = new Serial.CsSerial();
                //Dictionary<string, object> dic2 = new Dictionary<string, object>();
                string sendstr = "";
                //string[] str = SerialPort.GetPortNames();
                //serialprint.SerialOpen(str[0], "9600", "8");
                bool b_SerialOpen= serialprint.SerialOpen(M_stcPrintData.SerialPortName, "9600", "8");
                if (!b_SerialOpen)
                {
                    CsFormShow.MessageBoxFormShow("提示：网单标签打印机未连接成功,请检查打印机是否异常！");
                }
                sendstr = System.IO.File.ReadAllText(M_stcPrintData.M_bmpringpath);
                Thread Serialth = new Thread(SerialSend);
                Serialth.IsBackground = true;
                Serialth.Start();
                void SerialSend()
                {
                    try
                    {
                        while (true)
                        {
                            Thread.Sleep(100);
                            if (mCdata.IsConnect())
                            {
                                if (M_serialsignal == 1 && string.IsNullOrEmpty(M_serialsignalreset) && CSReadPlc.DevName.Trim().Contains("plc2")&& CsMarking.M_PackingType == "2")
                                {
                                    b_SerialOpen = serialprint.SerialOpen(M_stcPrintData.SerialPortName, "9600", "8");
                                    if (!b_SerialOpen)
                                    {
                                        CsFormShow.MessageBoxFormShow("提示：网单标签打印机未连接成功,请检查打印机是否异常！");
                                        Thread.Sleep(2000);
                                        continue;
                                    }
                                    Dictionary<string, string> dic_v = new Dictionary<string, string>();
                                    dic_v = GetGAPSmallLabelData();
                                    sendstr = M_stcPrintData.GetPrintNewStr(dic_v, M_stcPrintData.M_bmpringpath);
                                    serialprint.Senddata(sendstr, false);
                                    Cslogfun.WriteToprintgLog(" 开始串口打印,sku号码：" + dic_v["$sku"]);
                                    M_serialsignalreset = "start";
                                    timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                    while (M_serialsignal != 0)
                                    {
                                        TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                        TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                        if (ts.Seconds > 5)
                                        {
                                            break;
                                        }
                                    }
                                }
                                else if (CSReadPlc.serialTestFlag.Contains("start") && CSReadPlc.DevName.Trim().Contains("plc2"))
                                {
                                    b_SerialOpen = serialprint.SerialOpen(M_stcPrintData.SerialPortName, "9600", "8");
                                    if (!b_SerialOpen)
                                    {
                                        CsFormShow.MessageBoxFormShow("提示：网单标签打印机未连接成功,请检查打印机是否异常！");
                                        Thread.Sleep(2000);
                                        continue;
                                    }
                                    Dictionary<string, string> dic_v = new Dictionary<string, string>();
                                    dic_v["$TiaoMaText"] = "11111";
                                    dic_v["$TiaoMaVal"] = "11111";
                                    dic_v["$sku"] = "2222-0000";
                                    dic_v["$clor"] = "Color";
                                    dic_v["$ONESIZE"] = "Size";
                                    dic_v["$BianHaoVal"] = "333333333";
                                    dic_v["$BianHaoText"] = "333333333";
                                    sendstr = M_stcPrintData.GetPrintNewStr(dic_v, M_stcPrintData.M_bmpringpath);
                                    serialprint.Senddata(sendstr, false);
                                    CSReadPlc.serialTestFlag = "";
                                    timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                    while (M_serialsignal != 0)
                                    {
                                        TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                        TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                        if (ts.Seconds > 5)
                                        {
                                            break;
                                        }
                                    }
                                    Cslogfun.WriteToprintgLog(" 开始串口测试打印" );
                                }
                                else if (CSReadPlc.serialManualFlag.Contains("start") && CSReadPlc.DevName.Trim().Contains("plc2"))
                                {
                                    b_SerialOpen = serialprint.SerialOpen(M_stcPrintData.SerialPortName, "9600", "8");
                                    if (!b_SerialOpen)
                                    {
                                        CsFormShow.MessageBoxFormShow("提示：网单标签打印机未连接成功,请检查打印机是否异常！");
                                        Thread.Sleep(2000);
                                        continue;
                                    }
                                    Dictionary<string, string> dic_v = new Dictionary<string, string>();
                                    dic_v["$TiaoMaText"] = M_stcPrintData.DicSerialManualPrint["TiaoMaVal"];
                                    dic_v["$TiaoMaVal"] = M_stcPrintData.DicSerialManualPrint["TiaoMaVal"];
                                    dic_v["$sku"] = M_stcPrintData.DicSerialManualPrint["sku"];
                                    dic_v["$clor"] = M_stcPrintData.DicSerialManualPrint["Color"];
                                    dic_v["$ONESIZE"] = M_stcPrintData.DicSerialManualPrint["Size"];
                                    dic_v["$BianHaoVal"] = M_stcPrintData.DicSerialManualPrint["GAPSideStartNum"];
                                    dic_v["$BianHaoText"] = M_stcPrintData.DicSerialManualPrint["GAPSideStartNum"];
                                    sendstr = M_stcPrintData.GetPrintNewStr(dic_v, M_stcPrintData.M_bmpringpath);
                                    serialprint.Senddata(sendstr, false);
                                    CSReadPlc.serialManualFlag = "";
                                    timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                    while (M_serialsignal != 0)
                                    {
                                        TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                        TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                        if (ts.Seconds > 5)
                                        {
                                            break;
                                        }
                                    }
                                    Cslogfun.WriteToprintgLog(" 开始手动串口打印");
                                }

                            }
                        }
                    }
                    catch (Exception err)
                    {
                        Cslogfun.WriteToLog(err);
                        MessageBox.Show("提示：" + err.ToString());
                        //throw new Exception("提示：" + err.Message);
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                if (err.ToString().Contains("未解析为有效的串行端口"))
                {
                   MessageBox.Show("请尝试拔掉上位机上的打印机串口线重新插上，再重新开启上位机软件");
                    System.Environment.Exit(0);
                }
                else
                {
                    MessageBox.Show("提示：" + err.ToString());
                }
                //throw new Exception("提示：" + err.Message);
            }
        }
        #endregion
        /// <summary>
        /// 将喷码完成的加入到打印标签列队中
        /// </summary>
        /// <param name="dt_"></param>
        public static void PrintLineuUp(DataTable dt_)
        {
            try
            {
                if (dt_.Rows.Count > 0)
                {
                    PrintLineuUpToSql(dt_);//写入数据库
                    Dictionary<string, string> DicPdfData = new Dictionary<string, string>();
                    DicPdfData["Poguid"] = dt_.Rows[0]["Poguid"].ToString().Trim();
                    DicPdfData["Pdf_guid"] = dt_.Rows[0]["guid"].ToString().Trim();
                    DicPdfData["page"] = dt_.Rows[0]["page"].ToString().Trim();
                    DicPdfData["StyleProgram"] = dt_.Rows[0]["StyleProgram"].ToString().Trim();
                    DicPdfData["size"] = dt_.Rows[0]["size"].ToString().Trim();
                    DicPdfData["number"] = dt_.Rows[0]["number"].ToString().Trim();
                    DicPdfData["po"] = dt_.Rows[0]["po"].ToString().Trim();
                    DicPdfData["style"] = dt_.Rows[0]["style"].ToString().Trim();
                    //DicPdfData["style"] = System.Text.RegularExpressions.Regex.Replace(dt_.Rows[0]["style"].ToString().Trim(), @"[^0-9]+", "");
                    DicPdfData["qty"] = dt_.Rows[0]["qty"].ToString().Trim();
                    DicPdfData["NO1"] = dt_.Rows[0]["NO1"].ToString().Trim();
                    DicPdfData["NO2"] = dt_.Rows[0]["NO2"].ToString().Trim();
                    DicPdfData["shipfrom1"] = dt_.Rows[0]["shipfrom1"].ToString().Trim();
                    DicPdfData["shipfrom2"] = dt_.Rows[0]["shipfrom2"].ToString().Trim();
                    DicPdfData["shipfrom3"] = dt_.Rows[0]["shipfrom3"].ToString().Trim();
                    DicPdfData["shipfrom4"] = dt_.Rows[0]["shipfrom4"].ToString().Trim();
                    DicPdfData["shipto1"] = dt_.Rows[0]["shipto1"].ToString().Trim();
                    DicPdfData["shipto2"] = dt_.Rows[0]["shipto2"].ToString().Trim();
                    DicPdfData["shipto3"] = dt_.Rows[0]["shipto3"].ToString().Trim();
                    DicPdfData["shipto4"] = dt_.Rows[0]["shipto4"].ToString().Trim();
                    DicPdfData["number1"] = dt_.Rows[0]["number1"].ToString().Trim();
                    DicPdfData["number2"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(0, 15);
                    DicPdfData["number3"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(15, 4);
                    DicPdfData["number4"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(19, 1);
                    //DicPdfData["xuhao"] = dt_.Rows[0]["xuhao"].ToString().Trim();
                    M_stcPrintData.lis_printdata.Add(DicPdfData);
                    // Cslogfun.WriteTomarkingLog(DicPdfData["po"]+","+ DicPdfData["page"] + "页" + "加入打印队列");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }


        }
        /// <summary>
        /// 将异常未打印完成的标签加入队列
        /// </summary>
        /// <param name="dt_"></param>
        public static void PrintLineuUp(DataRow dr)
        {
            try
            {
                Dictionary<string, string> DicPdfData = new Dictionary<string, string>();
                DicPdfData["guid"] = dr["guid"].ToString().Trim();
                DicPdfData["Poguid"] = dr["Poguid"].ToString().Trim();
                DicPdfData["Pdf_guid"] = dr["Pdf_guid"].ToString().Trim();
                DicPdfData["page"] = dr["page"].ToString().Trim();
                DicPdfData["StyleProgram"] = dr["StyleProgram"].ToString().Trim();
                DicPdfData["size"] = dr["size"].ToString().Trim();
                DicPdfData["number"] = dr["number"].ToString().Trim();
                DicPdfData["po"] = dr["po"].ToString().Trim();
                DicPdfData["style"] = dr["style"].ToString().Trim();
                //DicPdfData["style"] = System.Text.RegularExpressions.Regex.Replace(dr["style"].ToString().Trim(), @"[^0-9]+", "");
                DicPdfData["qty"] = dr["qty"].ToString().Trim();
                DicPdfData["NO1"] = dr["NO1"].ToString().Trim();
                DicPdfData["NO2"] = dr["NO2"].ToString().Trim();
                DicPdfData["shipfrom1"] = dr["shipfrom1"].ToString().Trim();
                DicPdfData["shipfrom2"] = dr["shipfrom2"].ToString().Trim();
                DicPdfData["shipfrom3"] = dr["shipfrom3"].ToString().Trim();
                DicPdfData["shipfrom4"] = dr["shipfrom4"].ToString().Trim();
                DicPdfData["shipto1"] = dr["shipto1"].ToString().Trim();
                DicPdfData["shipto2"] = dr["shipto2"].ToString().Trim();
                DicPdfData["shipto3"] = dr["shipto3"].ToString().Trim();
                DicPdfData["shipto4"] = dr["shipto4"].ToString().Trim();
                DicPdfData["number1"] = dr["number1"].ToString().Trim();
                DicPdfData["number2"] = dr["number1"].ToString().Trim().Substring(0, 15);
                DicPdfData["number3"] = dr["number1"].ToString().Trim().Substring(15, 4);
                DicPdfData["number4"] = dr["number1"].ToString().Trim().Substring(19, 1);
                M_stcPrintData.lis_printdata.Add(DicPdfData);
                // Cslogfun.WriteTomarkingLog(DicPdfData["po"]+","+ DicPdfData["page"] + "页" + "加入打印队列");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }


        }
        /// <summary>
        /// 其他生产线将异常未打印完成的标签加入队列
        /// </summary>
        /// <param name="dt_"></param>
        public static void OtherLinePrintLineuUp(DataRow dr)
        {
            try
            {
                Dictionary<string, string> DicPdfData = new Dictionary<string, string>();
                DicPdfData["guid"] = dr["guid"].ToString().Trim();
                DicPdfData["Poguid"] = dr["Poguid"].ToString().Trim();
                DicPdfData["Pdf_guid"] = dr["Pdf_guid"].ToString().Trim();
                DicPdfData["page"] = dr["page"].ToString().Trim();
                DicPdfData["StyleProgram"] = dr["StyleProgram"].ToString().Trim();
                DicPdfData["size"] = dr["size"].ToString().Trim();
                DicPdfData["number"] = dr["number"].ToString().Trim();
                DicPdfData["po"] = dr["po"].ToString().Trim();
                DicPdfData["style"] = dr["style"].ToString().Trim();
                //DicPdfData["style"] = System.Text.RegularExpressions.Regex.Replace(dr["style"].ToString().Trim(), @"[^0-9]+", "");
                DicPdfData["qty"] = dr["qty"].ToString().Trim();
                DicPdfData["NO1"] = dr["NO1"].ToString().Trim();
                DicPdfData["NO2"] = dr["NO2"].ToString().Trim();
                DicPdfData["shipfrom1"] = dr["shipfrom1"].ToString().Trim();
                DicPdfData["shipfrom2"] = dr["shipfrom2"].ToString().Trim();
                DicPdfData["shipfrom3"] = dr["shipfrom3"].ToString().Trim();
                DicPdfData["shipfrom4"] = dr["shipfrom4"].ToString().Trim();
                DicPdfData["shipto1"] = dr["shipto1"].ToString().Trim();
                DicPdfData["shipto2"] = dr["shipto2"].ToString().Trim();
                DicPdfData["shipto3"] = dr["shipto3"].ToString().Trim();
                DicPdfData["shipto4"] = dr["shipto4"].ToString().Trim();
                DicPdfData["number1"] = dr["number1"].ToString().Trim();
                DicPdfData["number2"] = dr["number1"].ToString().Trim().Substring(0, 15);
                DicPdfData["number3"] = dr["number1"].ToString().Trim().Substring(15, 4);
                DicPdfData["number4"] = dr["number1"].ToString().Trim().Substring(19, 1);
                M_stcPrintData.lis_OtherLine_printdata.Add(DicPdfData);
                // Cslogfun.WriteTomarkingLog(DicPdfData["po"]+","+ DicPdfData["page"] + "页" + "加入打印队列");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 手动打印的标签加入队列
        /// </summary>
        /// <param name="dt_"></param>
        public static void ManualPrintLineuUp(DataRow dr)
        {
            try
            {
                Dictionary<string, string> DicPdfData = new Dictionary<string, string>();
                DicPdfData["guid"] = dr["Pdf_guid"].ToString().Trim();
                DicPdfData["page"] = dr["page"].ToString().Trim();
                DicPdfData["StyleProgram"] = dr["StyleProgram"].ToString().Trim();
                DicPdfData["size"] = dr["size"].ToString().Trim();
                DicPdfData["number"] = dr["number"].ToString().Trim();
                DicPdfData["po"] = dr["po"].ToString().Trim();
                DicPdfData["style"] = dr["style"].ToString().Trim();
                //DicPdfData["style"] = System.Text.RegularExpressions.Regex.Replace(dr["style"].ToString().Trim(), @"[^0-9]+", "");
                DicPdfData["qty"] = dr["qty"].ToString().Trim();
                DicPdfData["NO1"] = dr["NO1"].ToString().Trim();
                DicPdfData["NO2"] = dr["NO2"].ToString().Trim();
                DicPdfData["shipfrom1"] = dr["shipfrom1"].ToString().Trim();
                DicPdfData["shipfrom2"] = dr["shipfrom2"].ToString().Trim();
                DicPdfData["shipfrom3"] = dr["shipfrom3"].ToString().Trim();
                DicPdfData["shipfrom4"] = dr["shipfrom4"].ToString().Trim();
                DicPdfData["shipto1"] = dr["shipto1"].ToString().Trim();
                DicPdfData["shipto2"] = dr["shipto2"].ToString().Trim();
                DicPdfData["shipto3"] = dr["shipto3"].ToString().Trim();
                DicPdfData["shipto4"] = dr["shipto4"].ToString().Trim();
                DicPdfData["number1"] = dr["number1"].ToString().Trim();
                DicPdfData["number2"] = dr["number1"].ToString().Trim().Substring(0, 15);
                DicPdfData["number3"] = dr["number1"].ToString().Trim().Substring(15, 4);
                DicPdfData["number4"] = dr["number1"].ToString().Trim().Substring(19, 1);
                //将手动打印的放在第一个，其他重新排序
                List<Dictionary<string, string>> listdic = new List<Dictionary<string, string>>();
                listdic.Add(DicPdfData);
                //int index = M_stcPrintData.lis_printdata.FindIndex(item => item.Equals(DicPdfData));查找指定元素的索引
                lock (oblock)
                {
                    for (int i = 0; i < M_stcPrintData.lis_printdata.Count; i++)
                    {
                        string M_pid = M_stcPrintData.lis_printdata.ToList()[i]["guid"];
                        string pid = dr["Pdf_guid"].ToString().Trim();
                        if (M_pid == pid)
                        {
                            M_stcPrintData.lis_printdata.Remove(M_stcPrintData.lis_printdata.ToList()[i]);
                        }

                    }
                    if (M_stcPrintData.lis_printdata.Count > 0)
                    {
                        for (int i = 0; i < M_stcPrintData.lis_printdata.Count; i++)
                        {
                            listdic.Add(M_stcPrintData.lis_printdata.ToList()[i]);
                        }
                    }
                    if (listdic.Count > 0)
                    {
                        M_stcPrintData.lis_printdata.Clear();
                        M_stcPrintData.lis_printdata = new List<Dictionary<string, string>>(listdic.GetRange(0, listdic.Count));
                    }

                }
                // Cslogfun.WriteTomarkingLog(DicPdfData["po"]+","+ DicPdfData["page"] + "页" + "加入打印队列");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// /替换赋值打印内容
        /// </summary>
        /// <param name="soketprint_"></param>
        /// <param name="PrintIP_"></param>
        /// <param name="PrintIPoint_"></param>
        /// <param name="printpath_"></param>
        public static void SetvalToPrint(SoketPrint soketprint_, string PrintIP_, string PrintIPoint_, string printpath_)
        {
            try
            {
                if (M_stcPrintData.lis_printdata.ToList().Count > 0)
                {
                    soketprint_.Setval("$AAAA", M_stcPrintData.lis_printdata.ToList()[0]["number1"]);
                    soketprint_.Setval("$BBBB", M_stcPrintData.lis_printdata.ToList()[0]["po"]);
                    soketprint_.Setval("$CCCC", M_stcPrintData.lis_printdata.ToList()[0]["number1"]);
                    soketprint_.Setval("$NO1", M_stcPrintData.lis_printdata.ToList()[0]["NO1"]);
                    soketprint_.Setval("$number1", M_stcPrintData.lis_printdata.ToList()[0]["number"]);
                    soketprint_.Setval("$NO2", M_stcPrintData.lis_printdata.ToList()[0]["NO2"]);
                    soketprint_.Setval("$shipfrom1", M_stcPrintData.lis_printdata.ToList()[0]["shipfrom1"]);
                    soketprint_.Setval("$shipfrom2", M_stcPrintData.lis_printdata.ToList()[0]["shipfrom2"]);
                    soketprint_.Setval("$shipfrom3", M_stcPrintData.lis_printdata.ToList()[0]["shipfrom3"]);
                    soketprint_.Setval("$shipfrom4", M_stcPrintData.lis_printdata.ToList()[0]["shipfrom4"]);
                    soketprint_.Setval("$shipto1", M_stcPrintData.lis_printdata.ToList()[0]["shipto1"]);
                    soketprint_.Setval("$shipto2", M_stcPrintData.lis_printdata.ToList()[0]["shipto2"]);
                    soketprint_.Setval("$shipto3", M_stcPrintData.lis_printdata.ToList()[0]["shipto3"]);
                    soketprint_.Setval("$shipto4", M_stcPrintData.lis_printdata.ToList()[0]["shipto4"]);
                    soketprint_.Setval("$StyleProgram", M_stcPrintData.lis_printdata.ToList()[0]["StyleProgram"]);
                    soketprint_.Setval("$po", M_stcPrintData.lis_printdata.ToList()[0]["po"]);
                    soketprint_.Setval("$style", M_stcPrintData.lis_printdata.ToList()[0]["style"]);
                    soketprint_.Setval("$size", M_stcPrintData.lis_printdata.ToList()[0]["size"]);
                    soketprint_.Setval("$qty", M_stcPrintData.lis_printdata.ToList()[0]["qty"]);
                    soketprint_.Setval("$number2", M_stcPrintData.lis_printdata.ToList()[0]["number2"]);
                    soketprint_.Setval("$number3", M_stcPrintData.lis_printdata.ToList()[0]["number3"]);
                    soketprint_.Setval("$number4", M_stcPrintData.lis_printdata.ToList()[0]["number4"]);
                    soketprint_.Print(PrintIP_, PrintIPoint_, printpath_);//开始打印

                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET finishprintnumber=ISNULL(finishprintnumber,0)+1 WHERE  guid='{0}'", M_stcPrintData.lis_printdata.ToList()[0]["Poguid"]));
                    //DataTable dt =CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", M_stcPrintData.lis_printdata.ToList()[0]["Poguid"]));
                    if (M_stcPrintData.lis_printdata.Count > 0)
                    {
                        string tiaoma = M_stcPrintData.lis_printdata.ToList()[0]["number1"];
                        CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PDFDATA SET PrintCount=ISNULL(PrintCount,0)+1 WHERE po='{0}' AND page='{1}'", M_stcPrintData.lis_printdata.ToList()[0]["po"], M_stcPrintData.lis_printdata.ToList()[0]["page"]));

                        Cslogfun.WriteToCartornLog("单号:" + M_stcPrintData.lis_printdata.ToList()[0]["po"] + ",本次打印箱号：" + M_stcPrintData.lis_printdata.ToList()[0]["page"] + "，打印条码：" + tiaoma);
                    }
                }
                else
                {
                    MessageBox.Show("提示：没有可打印的数据！");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：没有可打印的数据！" + err.ToString());
                //throw new Exception("提示：" + err.Message);
            }

        }
        /// <summary>
        /// /替换赋值打印内容
        /// </summary>
        /// <param name="soketprint_"></param>
        /// <param name="PrintIP_"></param>
        /// <param name="PrintIPoint_"></param>
        /// <param name="printpath_"></param>
        public static void SetvalToOtherPrint(SoketPrint soketprint_, string PrintIP_, string PrintIPoint_, string printpath_)
        {
            try
            {
                if (M_stcPrintData.lis_OtherLine_printdata.ToList().Count > 0)
                {
                    soketprint_.Setval("$AAAA", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["number1"]);
                    soketprint_.Setval("$BBBB", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["po"]);
                    soketprint_.Setval("$CCCC", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["number1"]);
                    soketprint_.Setval("$NO1", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["NO1"]);
                    soketprint_.Setval("$number1", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["number"]);
                    soketprint_.Setval("$NO2", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["NO2"]);
                    soketprint_.Setval("$shipfrom1", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["shipfrom1"]);
                    soketprint_.Setval("$shipfrom2", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["shipfrom2"]);
                    soketprint_.Setval("$shipfrom3", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["shipfrom3"]);
                    soketprint_.Setval("$shipfrom4", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["shipfrom4"]);
                    soketprint_.Setval("$shipto1", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["shipto1"]);
                    soketprint_.Setval("$shipto2", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["shipto2"]);
                    soketprint_.Setval("$shipto3", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["shipto3"]);
                    soketprint_.Setval("$shipto4", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["shipto4"]);
                    soketprint_.Setval("$StyleProgram", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["StyleProgram"]);
                    soketprint_.Setval("$po", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["po"]);
                    soketprint_.Setval("$style", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["style"]);
                    soketprint_.Setval("$size", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["size"]);
                    soketprint_.Setval("$qty", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["qty"]);
                    soketprint_.Setval("$number2", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["number2"]);
                    soketprint_.Setval("$number3", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["number3"]);
                    soketprint_.Setval("$number4", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["number4"]);
                    soketprint_.Print(PrintIP_, PrintIPoint_, printpath_);//开始打印

                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET finishprintnumber=ISNULL(finishprintnumber,0)+1 WHERE  guid='{0}'", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["Poguid"]));
                    //DataTable dt =CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["Poguid"]));
                    if (M_stcPrintData.lis_OtherLine_printdata.Count > 0)
                    {
                        //int count = int.Parse(dt.Rows[0]["finishPalletizingnumber"].ToString().Trim());
                        // int startcode = int.Parse(dt.Rows[0]["从"].ToString().Trim());
                        // string po = dt.Rows[0]["订单号码"].ToString().Trim();
                        //  int finishcode = count + startcode ;
                        string tiaoma = M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["number1"];
                        CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PDFDATA SET PrintCount=ISNULL(PrintCount,0)+1 WHERE po='{0}' AND page='{1}'", M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["po"], M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["page"]));
                        if (M_MarkingData.M_PackingCode == "1" || M_MarkingData.M_PackingCode == "2")
                        {
                            Cslogfun.WriteToprintgLog("单号:" + M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["po"] + ",本次喷码箱号：" + M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["page"] + "，打印条码：" + tiaoma);
                        }
                        else
                        {
                            Cslogfun.WriteToprintgLog("单号:" + M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["po"] + ",本次喷码箱号：" + M_stcPrintData.lis_OtherLine_printdata.ToList()[0]["page"] + "，打印条码：" + tiaoma);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("提示：没有可打印的数据！");
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }
        /// <summary>
        /// 将本次标签队列数据写入数据库
        /// </summary>
        /// <param name="dt_"></param>
        public static void PrintLineuUpToSql(DataTable dt_)
        {
            try
            {
                string sqlstr = string.Format("INSERT INTO dbo.PRINTLineuUp (Pdf_guid,page,StyleProgram,size,number,po,style,qty, NO1,NO2,shipfrom1,shipfrom2,shipfrom3,shipfrom4,shipto1, shipto2,shipto3,shipto4, number1,Entrytime,Poguid,DeviceName)SELECT guid,page,StyleProgram,size,number,po,	style,qty,NO1,NO2,shipfrom1,shipfrom2,shipfrom3,shipfrom4,shipto1,shipto2,shipto3,shipto4,number1,'{1}','{2}','{3}' FROM dbo.PDFDATA WHERE guid = '{0}' AND NOT EXISTS(SELECT*FROM dbo.PRINTLineuUp WHERE dbo.PDFDATA.guid=Pdf_guid)", dt_.Rows[0]["guid"].ToString().Trim(), DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), dt_.Rows[0]["Poguid"].ToString().Trim(), CSReadPlc.DevName.Trim());
                content1.Select_nothing(sqlstr);
                //Cslogfun.WriteTomarkingLog(dt_.Rows[0]["po"] +","+ dt_.Rows[0]["page"] + "页" + "写入数据库");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }
        public void DeletePrintLineUpFromSql(Dictionary<string, string> dic_)
        {
            try
            {
                string sqlstr = string.Format("DELETE FROM dbo.PRINTLineuUp WHERE Pdf_guid='{0}'", dic_["Pdf_guid"]);
                content1.Select_nothing(sqlstr);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }
        public static void deleteprint(SoketPrint soketprint_, string PrintIP_, string PrintIPoint_)//调式用
        {
            try
            {
                if (M_stcPrintData.lis_printdata.ToList().Count > 0)
                {
                    soketprint_.Setval("$AAAA", M_stcPrintData.lis_printdata.ToList()[0]["number1"]);
                    soketprint_.Setval("$BBBB", M_stcPrintData.lis_printdata.ToList()[0]["po"]);
                    soketprint_.Setval("$CCCC", M_stcPrintData.lis_printdata.ToList()[0]["number1"]);
                    soketprint_.Setval("$NO1", M_stcPrintData.lis_printdata.ToList()[0]["NO1"]);
                    soketprint_.Setval("$number1", M_stcPrintData.lis_printdata.ToList()[0]["number"]);
                    soketprint_.Setval("$NO2", M_stcPrintData.lis_printdata.ToList()[0]["NO2"]);
                    soketprint_.Setval("$shipfrom1", M_stcPrintData.lis_printdata.ToList()[0]["shipfrom1"]);
                    soketprint_.Setval("$shipfrom2", M_stcPrintData.lis_printdata.ToList()[0]["shipfrom2"]);
                    soketprint_.Setval("$shipfrom3", M_stcPrintData.lis_printdata.ToList()[0]["shipfrom3"]);
                    soketprint_.Setval("$shipfrom4", M_stcPrintData.lis_printdata.ToList()[0]["shipfrom4"]);
                    soketprint_.Setval("$shipto1", M_stcPrintData.lis_printdata.ToList()[0]["shipto1"]);
                    soketprint_.Setval("$shipto2", M_stcPrintData.lis_printdata.ToList()[0]["shipto2"]);
                    soketprint_.Setval("$shipto3", M_stcPrintData.lis_printdata.ToList()[0]["shipto3"]);
                    soketprint_.Setval("$shipto4", M_stcPrintData.lis_printdata.ToList()[0]["shipto4"]);
                    soketprint_.Setval("$StyleProgram", M_stcPrintData.lis_printdata.ToList()[0]["StyleProgram"]);
                    soketprint_.Setval("$po", M_stcPrintData.lis_printdata.ToList()[0]["po"]);
                    soketprint_.Setval("$style", M_stcPrintData.lis_printdata.ToList()[0]["style"]);
                    soketprint_.Setval("$size", M_stcPrintData.lis_printdata.ToList()[0]["size"]);
                    soketprint_.Setval("$qty", M_stcPrintData.lis_printdata.ToList()[0]["qty"]);
                    soketprint_.Setval("$number2", M_stcPrintData.lis_printdata.ToList()[0]["number2"]);
                    soketprint_.Setval("$number3", M_stcPrintData.lis_printdata.ToList()[0]["number3"]);
                    soketprint_.Setval("$number4", M_stcPrintData.lis_printdata.ToList()[0]["number4"]);
                    string printtxt = soketprint_.gettxt(M_stcPrintData.M_PrintPath);
                    string M_sqlprintend = string.Format("UPDATE dbo.PACKING SET 贴标完成=1 WHERE 订单号码= '{0}' AND 从={1}", M_stcPrintData.lis_printdata.ToList()[0]["po"], int.Parse(M_stcPrintData.lis_printdata.ToList()[0]["page"]));
                    content1.Select_nothing(M_sqlprintend);
                    M_stcPrintData.lis_printdata.Remove(M_stcPrintData.lis_printdata.ToList()[0]);
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }


        }
        /// <summary>
        /// /替换赋值打印内容
        /// </summary>
        /// <param name="soketprint_"></param>
        /// <param name="PrintIP_"></param>
        /// <param name="PrintIPoint_"></param>
        /// <param name="printpath_"></param>
        public static void TestToPrint(SoketPrint soketprint_, string PrintIP_, string PrintIPoint_, string printpath_)
        {
            try
            {
                soketprint_.Setval("$AAAA", "000");
                soketprint_.Setval("$BBBB", "000");
                soketprint_.Setval("$CCCC", "000");
                soketprint_.Setval("$NO1", "000");
                soketprint_.Setval("$number1", "000");
                soketprint_.Setval("$NO2", "000");
                soketprint_.Setval("$shipfrom1", "000");
                soketprint_.Setval("$shipfrom2", "000");
                soketprint_.Setval("$shipfrom3", "000");
                soketprint_.Setval("$shipfrom4", "000");
                soketprint_.Setval("$shipto1", "000");
                soketprint_.Setval("$shipto2", "000");
                soketprint_.Setval("$shipto3", "000");
                soketprint_.Setval("$shipto4", "000");
                soketprint_.Setval("$StyleProgram", "000");
                soketprint_.Setval("$po", "000");
                soketprint_.Setval("$style", "000");
                soketprint_.Setval("$size", "000");
                soketprint_.Setval("$qty", "000");
                soketprint_.Setval("$number2", "000");
                soketprint_.Setval("$number3", "000");
                soketprint_.Setval("$number4", "000");
                soketprint_.Print(PrintIP_, PrintIPoint_, printpath_);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }
        /// <summary>
        /// 手动任意页打印内容
        /// </summary>
        /// <param name="soketprint_"></param>
        /// <param name="PrintIP_"></param>
        /// <param name="PrintIPoint_"></param>
        /// <param name="printpath_"></param>
        public static void ManualToPrint(SoketPrint soketprint_, string PrintIP_, string PrintIPoint_, string printpath_)
        {
            try
            {
                if (M_stcPrintData.DicManualPrint.Count > 0)
                {
                    soketprint_.Setval("$AAAA", M_stcPrintData.DicManualPrint["number1"]);
                    soketprint_.Setval("$BBBB", M_stcPrintData.DicManualPrint["po"]);
                    soketprint_.Setval("$CCCC", M_stcPrintData.DicManualPrint["number1"]);
                    soketprint_.Setval("$NO1", M_stcPrintData.DicManualPrint["NO1"]);
                    soketprint_.Setval("$number1", M_stcPrintData.DicManualPrint["number"]);
                    soketprint_.Setval("$NO2", M_stcPrintData.DicManualPrint["NO2"]);
                    soketprint_.Setval("$shipfrom1", M_stcPrintData.DicManualPrint["shipfrom1"]);
                    soketprint_.Setval("$shipfrom2", M_stcPrintData.DicManualPrint["shipfrom2"]);
                    soketprint_.Setval("$shipfrom3", M_stcPrintData.DicManualPrint["shipfrom3"]);
                    soketprint_.Setval("$shipfrom4", M_stcPrintData.DicManualPrint["shipfrom4"]);
                    soketprint_.Setval("$shipto1", M_stcPrintData.DicManualPrint["shipto1"]);
                    soketprint_.Setval("$shipto2", M_stcPrintData.DicManualPrint["shipto2"]);
                    soketprint_.Setval("$shipto3", M_stcPrintData.DicManualPrint["shipto3"]);
                    soketprint_.Setval("$shipto4", M_stcPrintData.DicManualPrint["shipto4"]);
                    soketprint_.Setval("$StyleProgram", M_stcPrintData.DicManualPrint["StyleProgram"]);
                    soketprint_.Setval("$po", M_stcPrintData.DicManualPrint["po"]);
                    soketprint_.Setval("$style", M_stcPrintData.DicManualPrint["style"]);
                    soketprint_.Setval("$size", M_stcPrintData.DicManualPrint["size"]);
                    soketprint_.Setval("$qty", M_stcPrintData.DicManualPrint["qty"]);
                    soketprint_.Setval("$number2", M_stcPrintData.DicManualPrint["number2"]);
                    soketprint_.Setval("$number3", M_stcPrintData.DicManualPrint["number3"]);
                    soketprint_.Setval("$number4", M_stcPrintData.DicManualPrint["number4"]);
                    soketprint_.Print(PrintIP_, PrintIPoint_, printpath_);//开始打印
                }
                else
                {
                    MessageBox.Show("提示：没有可打印的数据！");
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }
        public static void DeleteLastPrint(int page_)
        {
            try
            {
                if (M_stcPrintData.lis_printdata.Count > 0)
                {
                    for (int i = 0; i < M_stcPrintData.lis_printdata.Count; i++)
                    {
                        int page = int.Parse(M_stcPrintData.lis_printdata.ToList()[i]["page"].Trim());

                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 赋值当前打印内容
        /// </summary>
        /// <param name="Poguid_"></param>
        /// <param name="po_"></param>
        /// <param name="page_"></param>
        public static void SetPrintConnect(string Poguid_, string po_, int page_,int sort_)
        {
            try
            {
                string sqlstr = string.Format("SELECT '{2}' as Poguid,*FROM ( SELECT ROW_NUMBER() OVER(ORDER BY num) AS xuhao, *FROM(SELECT *, number1 AS num, SUBSTRING(number1, LEN(number1) - 4, 4)AS sort  FROM dbo.PDFDATA WHERE po = '{0}') pdf) pdftable WHERE po = '{0}' AND xuhao = '{1}'AND sort=SUBSTRING('{3}',LEN('{3}')-3,4)", po_, page_, Poguid_,sort_);
                DataTable dt_ = CsFormShow.GoSqlSelect(sqlstr);
                if (dt_.Rows.Count > 0)
                {
                    Dictionary<string, string> DicPdfData = new Dictionary<string, string>();
                    DicPdfData["Poguid"] = dt_.Rows[0]["Poguid"].ToString().Trim();
                    DicPdfData["Pdf_guid"] = dt_.Rows[0]["guid"].ToString().Trim();
                    DicPdfData["page"] = dt_.Rows[0]["page"].ToString().Trim();
                    DicPdfData["StyleProgram"] = dt_.Rows[0]["StyleProgram"].ToString().Trim();
                    DicPdfData["size"] = dt_.Rows[0]["size"].ToString().Trim();
                    DicPdfData["number"] = dt_.Rows[0]["number"].ToString().Trim();
                    DicPdfData["po"] = dt_.Rows[0]["po"].ToString().Trim();
                    DicPdfData["style"] = dt_.Rows[0]["style"].ToString().Trim();
                    DicPdfData["qty"] = dt_.Rows[0]["qty"].ToString().Trim();
                    DicPdfData["NO1"] = dt_.Rows[0]["NO1"].ToString().Trim();
                    DicPdfData["NO2"] = dt_.Rows[0]["NO2"].ToString().Trim();
                    DicPdfData["shipfrom1"] = dt_.Rows[0]["shipfrom1"].ToString().Trim();
                    DicPdfData["shipfrom2"] = dt_.Rows[0]["shipfrom2"].ToString().Trim();
                    DicPdfData["shipfrom3"] = dt_.Rows[0]["shipfrom3"].ToString().Trim();
                    DicPdfData["shipfrom4"] = dt_.Rows[0]["shipfrom4"].ToString().Trim();
                    DicPdfData["shipto1"] = dt_.Rows[0]["shipto1"].ToString().Trim();
                    DicPdfData["shipto2"] = dt_.Rows[0]["shipto2"].ToString().Trim();
                    DicPdfData["shipto3"] = dt_.Rows[0]["shipto3"].ToString().Trim();
                    DicPdfData["shipto4"] = dt_.Rows[0]["shipto4"].ToString().Trim();
                    DicPdfData["number1"] = dt_.Rows[0]["number1"].ToString().Trim();
                    DicPdfData["number2"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(0, 15);
                    DicPdfData["number3"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(15, 4);
                    DicPdfData["number4"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(19, 1);
                    //DicPdfData["xuhao"] = dt_.Rows[0]["xuhao"].ToString().Trim();
                    M_stcPrintData.lis_printdata.Clear();
                    M_stcPrintData.lis_printdata.Add(DicPdfData);
                }
                else
                {
                    MessageBox.Show("提示：没有相应的PDF标签数据");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
        /// <summary>
        /// GAP侧面贴标数据获取
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetGAPSmallLabelData()
        {
            Dictionary<string, string> dic_v = new Dictionary<string, string>();
            try
            {
                if (string.IsNullOrEmpty(CSReadPlc.EditionFlag))
                {
                    if (CSReadPlc.DevName.Trim().Contains("plc2") && CsMarking.M_PackingType == "2")
                    {
                        dic_v["$TiaoMaText"] = CsPakingData.GetMarking_Net("TiaoMaVal");
                        dic_v["$TiaoMaVal"] = CsPakingData.GetMarking_Net("TiaoMaVal");
                        dic_v["$sku"] = CsPakingData.GetMarking_Net("sku");
                        dic_v["$clor"] = CsPakingData.GetMarking_Net("Color");
                        dic_v["$ONESIZE"] = CsPakingData.GetMarking_Net("Size");
                        string StartNum = CsPakingData.GetMarking_Net("StartNum");
                        string str = CsPakingData.GetMarking_Net("GAPSideStartNum"); 
                        Int64 strnum = Int64.Parse("1"+CsGetData.GetNumFromStr(str)) +int.Parse(StartNum) -1 ;
                        dic_v["$BianHaoVal"] = str.Replace(CsGetData.GetNumFromStr(str).ToString(), "") + strnum.ToString().Substring(1, strnum.ToString().Length-1);
                        dic_v["$BianHaoText"] = str.Replace(CsGetData.GetNumFromStr(str).ToString(), "") + strnum.ToString().Substring(1, strnum.ToString().Length - 1);
                        CsPakingData.M_list_DicMarkingNet.ToList()[0]["StartNum"] =(int.Parse( CsPakingData.M_list_DicMarkingNet.ToList()[0]["StartNum"])+1).ToString();
                    }
                }
                else 
                {
                    if (CSReadPlc.DevName.Trim().Contains("plc2") && CsMarking.M_PackingType == "2")
                    {
                        dic_v["$TiaoMaText"] = CsPublicVariablies.Marking.TiaoMaVal;
                        dic_v["$TiaoMaVal"] = CsPublicVariablies.Marking.TiaoMaVal;
                        dic_v["$sku"] = CsPublicVariablies.Marking.Sku号码;
                        dic_v["$clor"] = CsPublicVariablies.Marking.Color;
                        dic_v["$ONESIZE"] = CsPublicVariablies.Marking.Size;
                        string str = CsPublicVariablies.Marking.GAPSideStartNum;
                        Int64 strnum = Int64.Parse("1"+CsGetData.GetNumFromStr(str))+CsPublicVariablies.Marking.StartNum+CsPublicVariablies.Marking.CartornNum-2;
                        dic_v["$BianHaoVal"] = str.Replace(CsGetData.GetNumFromStr(str).ToString(), "") + strnum.ToString().Substring(1, strnum.ToString().Length - 1);
                        dic_v["$BianHaoText"] = str.Replace(CsGetData.GetNumFromStr(str).ToString(), "") + strnum.ToString().Substring(1, strnum.ToString().Length - 1);
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
            return dic_v;
        }

    }
}
