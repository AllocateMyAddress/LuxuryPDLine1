﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Keyence.AutoID.SDK;
using System.Threading;
using MCData;
//using GetData;
using System.Windows.Forms;
using System.Data;
namespace WindowsForms01
{
    public class CsScanning
    {

        static MCdata mCdata = new MCdata();
        GetPlcBao csGetBao;
        public static int M_scanning_trg = 0;
        public static int EndscanningRising = 0;
        public static int EndscanningFalling = 0;
        public static string M_scaningsetup = "";
        public static string scaningend = "";
        public static string scaningfalse = "";
        public static int scaningsetup = 0;
        public static int MotorStaus = 1;
        public static string Countflag = "";
        public static string endscanningflag = "";
        public static string ispassflag = "";
        public static string LastScanflag = "";
        public static string TiaoMaVal_Now = "start";
        public static string AllowScanSignal = "";
        public static int count_unit = 0;
        public static int M_ScanOk = 0;
        public static int M_ScanBack = 0;
        static bool connet = false;
        bool M_IsScanOk;
        //int M_scancount = 0;
        static string IP = "";
        static int IPonit = 0;
        Keyence.AutoID.SDK.ReaderAccessor m_reader = new ReaderAccessor();
        static CsScaning.SoketScaning Soketscaning = new CsScaning.SoketScaning(CSReadPlc.DevName);
        List<string> list_ScanLine = new List<string>();
        public CsScanning(MCdata mcdata, GetPlcBao bao)
        {
            mCdata = mcdata;
            csGetBao = bao;
        }

        public void Scanning(string IpAdd, int Port)
        {
            try
            {
                IP = IpAdd;
                IPonit = Port;
                Thread thscan = new Thread(scan);
                thscan.IsBackground = true;
                thscan.Start();
                scaningend = "";
                Countflag = "start";
                if (MotorStaus != 1)
                {
                    M_scaningsetup = "start";
                }
                connet = Soketscaning.SoketConnetion(IP, IPonit.ToString().Trim());
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        void scan()
        {
            while (true)
            {
                Thread.Sleep(10);
                try
                {
                    if (mCdata.IsConnect() && !M_stcScanData.IsScanRun.Contains("不运行"))
                    {
                        if (true)
                        {
                            connet = Soketscaning.SoketConnetion(IP, IPonit.ToString().Trim());
                            if (connet)
                            {
                                if (!string.IsNullOrEmpty(Soketscaning.recstr) && !Soketscaning.recstr.ToUpper().Contains("OK") && !Soketscaning.recstr.Contains("ERROR")/*&& M_ScanBack==0*/)
                                {
                                    //第一次触发或者换了另一个条码物件触发扫描
                                    if (string.IsNullOrEmpty(M_stcScanData.strTiaoMa) || M_stcScanData.strTiaoMa != Soketscaning.recstr)
                                    {
                                        M_stcScanData.strTiaoMa = Soketscaning.recstr;
                                    }
                                    if (!string.IsNullOrEmpty(M_stcScanData.strTiaoMa) && string.IsNullOrEmpty(LastScanflag))
                                    {
                                        LastScanflag = M_stcScanData.strTiaoMa;
                                        //混色装箱扫码
                                        if (!M_stcScanData.Set_Code.Contains("-999") && !M_stcScanData.potype.Contains("GAP"))
                                        {
                                            M_IsScanOk = IsPassColorMixingPcking();
                                        }
                                        //单色扫码
                                        else
                                        {
                                            M_IsScanOk = IsPassMonochromePacking();
                                        }
                                        Soketscaning.recstr = "";
                                    }
                                }
                                //扫描成功并且有有效条码，如果同一物件重复扫只执行一次就不进入重复执行
                                if (M_IsScanOk && M_ScanOk == 1 && !string.IsNullOrEmpty(M_stcScanData.strTiaoMa.Trim()) && string.IsNullOrEmpty(endscanningflag))
                                {
                                    endscanningflag = "start";
                                    TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                    while (M_ScanOk == 1)
                                    {
                                        TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                        TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                        if (ts.Seconds > 2)
                                        {
                                            break;
                                        }
                                    }
                                    ispassflag = "";
                                    if (list_ScanLine.Count < 2)
                                    {
                                        list_ScanLine.Add(M_stcScanData.strTiaoMa);
                                        Cslogfun.WriteToScaningLog("储存条码数量:" + list_ScanLine.Count);
                                    }
                                    else
                                    {
                                        list_ScanLine.Remove(list_ScanLine.ToList()[1]);
                                        list_ScanLine.Add(M_stcScanData.strTiaoMa);
                                    }
                                    // M_stcScanData.strTiaoMa = "";
                                }
                            }
                        }
                        if (string.IsNullOrEmpty(scaningend) && !string.IsNullOrEmpty(M_stcScanData.strTiaoMa) && M_scanning_trg == 1 && list_ScanLine.Count > 0)
                        {
                            scaningend = "start";
                            Countflag = "";
                            Cslogfun.WriteToScaningLog("此次条码信息：" + list_ScanLine.ToList()[0]);
                            //混色装箱扫码
                            if (!M_stcScanData.Set_Code.Contains("-999") && !M_stcScanData.potype.Contains("GAP"))
                            {
                                ColorMixingPcking(list_ScanLine.ToList()[0]);
                                list_ScanLine.Remove(list_ScanLine.ToList()[0]);
                                Cslogfun.WriteToScaningLog("收到离开信号" + Environment.NewLine);
                            }
                            //单色扫码
                            else
                            {
                                MonochromePacking(list_ScanLine.ToList()[0]);
                                list_ScanLine.Remove(list_ScanLine.ToList()[0]);
                                Cslogfun.WriteToScaningLog("收到离开信号" + Environment.NewLine);
                            }
                            LastScanflag = "";
                            Soketscaning.recstr = "";
                            M_stcScanData.strTiaoMa = "";
                        }
                        if (M_stcScanData.FinishLayernum == 0 && string.IsNullOrEmpty(Countflag) && M_ScanBack == 1)
                        {
                            CsInitialization.ScanningCountInit();
                            Countflag = "start";
                            TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                            while (M_ScanBack == 0)
                            {
                                TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                if (ts.Seconds > 2)
                                {
                                    break;
                                }
                            }
                            Cslogfun.WriteToScaningLog("重新计数一次");
                        }
                    }
                }
                catch (Exception err)
                {
                    Cslogfun.WriteToLog(err);
                    throw new Exception("提示：" + err);
                }
            }

        }
        /// <summary>
        /// 混色装箱扫码
        /// </summary>
        public void ColorMixingPcking(string TiaoMaVal_)
        {
            try
            {
                bool existtioama = false;
                bool endpacking = true;
                if (M_stcScanData.IsEndScan)
                {
                    scaningfalse = "start";
                    MessageBox.Show("已完成本次装箱数量，请换到下一单的货品");
                }
                else
                {
                    for (int i = 0; i < M_stcScanData.dtCurrentData.Rows.Count; i++)
                    {
                        if (TiaoMaVal_.Contains(M_stcScanData.dtCurrentData.Rows[i]["TiaoMaVal"].ToString().Trim()))
                        {
                            int Qty_per_Set = int.Parse(M_stcScanData.dtCurrentData.Rows[i]["Qty_per_Set"].ToString().Trim());//色号每箱应装包数
                            int FinishPackingNum = int.Parse(M_stcScanData.dtCurrentData.Rows[i]["FinishPackingNum"].ToString().Trim());//本箱已装包数
                            if (FinishPackingNum < Qty_per_Set)//数量小于箱应装包数未装完
                            {
                                M_stcScanData.dtCurrentData.Rows[i]["FinishPackingNum"] = (FinishPackingNum + 1).ToString();
                                if (M_stcScanData.dtCurrentData.Rows[i]["Qty_per_Set"].ToString().Trim() == M_stcScanData.dtCurrentData.Rows[i]["FinishPackingNum"].ToString().Trim())
                                {
                                    M_stcScanData.dtCurrentData.Rows[i]["IsEndPacking"] = 1;//箱内此色号放完标记
                                                                                            //将当前完成扫码的箱数写入数据库
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYK_GU_Current SET FinishPackingTotalNum=isnull(FinishPackingTotalNum,0) +1  WHERE Doguid='{0}'", M_stcScanData.dtCurrentData.Rows[i]["Doguid"].ToString().Trim()));
                                    DataTable dt_isend = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YYK_GU_Current WHERE Doguid='{0}' AND CurrentPackingNum=FinishPackingTotalNum", M_stcScanData.dtCurrentData.Rows[i]["Doguid"].ToString().Trim()));
                                    if (dt_isend.Rows.Count > 0)
                                    {
                                        //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET IsEndScan=1 WHERE YK_guid='{0}'", M_stcScanData.dtCurrentData.Rows[i]["Doguid"].ToString().Trim()));
                                        //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYK_GU_Current SET IsEndScan=1 WHERE Doguid='{0}'", M_stcScanData.dtCurrentData.Rows[i]["Doguid"].ToString().Trim()));
                                        //M_stcScanData.dtCurrentData.Rows[i]["IsEndScan"] = 1;
                                    }
                                }
                                int a = FinishPackingNum + 1;
                                Cslogfun.WriteToScaningLog("本箱颜色：" + M_stcScanData.dtCurrentData.Rows[i]["Color_Code"] + "，应装包数：" + Qty_per_Set + "，已经装" + a + "包");
                            }
                            else//数量大于箱应装包数
                            {
                                string IsEndPacking = "";
                                for (int j = 0; j < M_stcScanData.dtCurrentData.Rows.Count; j++)//判断是否本箱全部放完
                                {
                                    IsEndPacking = M_stcScanData.dtCurrentData.Rows[j]["IsEndPacking"].ToString().Trim();
                                    if (!IsEndPacking.ToUpper().Contains("TRUE"))
                                    {
                                        endpacking = false;//未放完本箱
                                    }
                                }
                                if (!endpacking)//还未放完本箱
                                {
                                    scaningfalse = "start";
                                    MessageBox.Show("此色号已经放完请换下一种色号");
                                    Cslogfun.WriteToScaningLog("此色号已经放完请换下一种色号");
                                }
                                else//放完了本箱
                                {
                                    Cslogfun.WriteToScaningLog("上一箱放完");
                                    Cslogfun.WriteToScaningLog("本箱颜色：" + M_stcScanData.dtCurrentData.Rows[i]["Color_Code"] + "，已经装1包");
                                    for (int j = 0; j < M_stcScanData.dtCurrentData.Rows.Count; j++)//本箱全部放完重新计数
                                    {
                                        M_stcScanData.dtCurrentData.Rows[j]["IsEndPacking"] = 0;
                                        M_stcScanData.dtCurrentData.Rows[j]["FinishPackingNum"] = "0";
                                        M_stcScanData.dtCurrentData.Rows[j]["FinishPackingTotalNum"] = int.Parse(M_stcScanData.dtCurrentData.Rows[j]["FinishPackingTotalNum"].ToString().Trim()) + 1;
                                    }
                                    //将当前完成扫码的箱数写入数据库
                                    //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYK_GU_Current SET FinishPackingTotalNum='{0}' WHERE DeviceName='{1}'", M_stcScanData.dtCurrentData.Rows[i]["FinishPackingTotalNum"].ToString().Trim(), CSReadPlc.DevName.Trim()));

                                    if (M_stcScanData.dtCurrentData.Rows[i]["FinishPackingTotalNum"].ToString().Trim() == M_stcScanData.dtCurrentData.Rows[i]["CurrentPackingNum"].ToString().Trim())//刚好达到装箱数量
                                    {
                                        //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET IsEndScan=1 WHERE YK_guid='{0}'",  M_stcScanData.dtCurrentData.Rows[i]["Doguid"].ToString().Trim()));
                                        //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYK_GU_Current SET IsEndScan=1 WHERE DeviceName='{0}'",CSReadPlc.DevName.Trim()));
                                        //M_stcScanData.dtCurrentData.Rows[i]["IsEndScan"] = 1;
                                       // M_stcScanData.IsEndScan = true;
                                       // MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                                        CsGetData.UpdataScanLineUp();
                                    }
                                    else//还未放完订单数量
                                    {
                                        //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET FinishPackingTotalNum='{0}' WHERE YK_guid='{1}'", M_stcScanData.dtCurrentData.Rows[i]["FinishPackingTotalNum"].ToString().Trim(), M_stcScanData.dtCurrentData.Rows[i]["Doguid"].ToString().Trim()));

                                        M_stcScanData.dtCurrentData.Rows[i]["FinishPackingNum"] = "1";//重新计数
                                        if (M_stcScanData.dtCurrentData.Rows[i]["Qty_per_Set"].ToString().Trim() == M_stcScanData.dtCurrentData.Rows[i]["FinishPackingNum"].ToString().Trim())//色号只有一包的情况
                                        {
                                            M_stcScanData.dtCurrentData.Rows[i]["IsEndPacking"] = 1;//箱内此色号放完标记
                                        }
                                    }
                                }
                            }
                            existtioama = true;
                            //更新总包数
                            //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE YK_guid='{0}'", M_stcScanData.dtCurrentData.Rows[i]["YK_guid"].ToString().Trim()));
                            //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYK_GU_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE Doguid='{0}'", M_stcScanData.dtCurrentData.Rows[i]["YK_guid"].ToString().Trim()));
                        }
                    }
                    if (!existtioama)
                    {
                        scaningfalse = "start";
                        MessageBox.Show("此条码:" + M_stcScanData.strTiaoMa + ",与当前订单的条码信息不一致，请检查！！");
                        Cslogfun.WriteToScaningLog("此条码:" + M_stcScanData.strTiaoMa + ",与当前订单的条码信息不一致，请检查！！");
                    }
                    else if (string.IsNullOrEmpty(M_stcScanData.strPoTiaoMa.Trim()))
                    {
                        scaningfalse = "start";
                        MessageBox.Show("无可执行订单，请检查！！");
                        Cslogfun.WriteToScaningLog("无可执行订单，请检查！！");
                    }
                    else
                    {
                        if (MotorStaus != 1)
                        {
                            M_scaningsetup = "start";
                        }
                    }
                    if (count_unit < M_stcScanData.Layernum)
                    {
                        count_unit++;
                        Cslogfun.WriteToScaningLog("本箱共" + M_stcScanData.Layernum + "件，已经装箱" + count_unit + "件");
                    }
                    if (count_unit == M_stcScanData.Layernum)
                    {
                        count_unit = 0;
                    }
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// 单色装箱扫码
        /// </summary>
        public void MonochromePacking(string TiaoMaVal_)
        {
            try
            {

                //扫码有误
                if (!TiaoMaVal_.Contains(M_stcScanData.strPoTiaoMa.Trim()))
                {
                    scaningfalse = "start";
                    MessageBox.Show("此条码:" + M_stcScanData.strTiaoMa + ",与当前订单的条码信息不一致，请检查！！");
                    Cslogfun.WriteToScaningLog("此条码:" + M_stcScanData.strTiaoMa + ",与当前订单的条码信息不一致，请检查！！");
                }
                else if (string.IsNullOrEmpty(M_stcScanData.strPoTiaoMa.Trim()))
                {
                    scaningfalse = "start";
                    MessageBox.Show("无可执行订单，请检查！！");
                    Cslogfun.WriteToScaningLog("无可执行订单，请检查！！");
                }
                //扫码正确
                else
                {
                    int baonum_unit = 0;
                    //GAP订单
                    if (M_stcScanData.potype.Contains("GAP"))
                    {
                        int qty = 0; int qtytotal = 0;
                        string str = M_stcScanData.dtCurrentData.Rows[0]["内包装计数"].ToString().Trim();
                        if (string.IsNullOrEmpty(str))
                        {
                            str = "0";
                        }
                        if (str.Trim() == "0")
                        {
                            qty = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["内部包装的项目数量"].ToString().Trim());//单条一包每箱应装条数
                            baonum_unit = qty;
                        }
                        else
                        {
                            qty = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["内包装计数"].ToString().Trim()) * int.Parse(M_stcScanData.dtCurrentData.Rows[0]["内部包装的项目数量"].ToString().Trim());//多条一包每箱应装条数
                            baonum_unit = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["内包装计数"].ToString().Trim());
                        }
                        int CurrentPackingNum = int.Parse(M_stcScanData.CurrentPackingNum.ToString().Trim());//当前总共
                        qtytotal = qty * CurrentPackingNum;//当前需总条数
                        M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"] = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim()) + 1;
                        //单条一包不拆包
                        if (M_stcScanData.IsUnpacking.Contains("不拆包") && str.Trim() == "0")
                        {
                            int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                            if (FinishBaoTotalNum > qtytotal)
                            {
                                //scaningfalse = "start";
                                //MessageBox.Show("已经超设置的订单数，请切换订单！！");
                                //Cslogfun.WriteToScaningLog("已经超设置的订单数，请切换订单！！");
                            }
                            else
                            {
                                if (MotorStaus != 1)
                                {
                                    M_scaningsetup = "start";
                                }
                                if (FinishBaoTotalNum == qtytotal)//刚好达到本次订单数量
                                {
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1/,*IsEndScan=1*/ WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.GAP_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 /*,IsEndScan=1*/ WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                    //scaningfalse = "start";
                                    //MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                                    CsGetData.UpdataScanLineUp();
                                }
                                else
                                {
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.GAP_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                }
                            }
                        }
                        //多条一包不拆包
                        else if (M_stcScanData.IsUnpacking.Contains("不拆包") && str != "0")
                        {
                            int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                            int Picking_Unit = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["内包装计数"].ToString().Trim());
                            if (FinishBaoTotalNum * Picking_Unit > qtytotal)
                            {
                                //scaningfalse = "start";
                                //MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                                CsGetData.UpdataScanLineUp();
                            }
                            else
                            {
                                if (MotorStaus != 1)
                                {
                                    M_scaningsetup = "start";
                                }
                                if (FinishBaoTotalNum * Picking_Unit == qtytotal)//刚好达到本次订单数量
                                {
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1/*,IsEndScan=1*/ WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.GAP_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 /*,IsEndScan=1*/ WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                    //scaningfalse = "start";
                                    //MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                                    CsGetData.UpdataScanLineUp();
                                }
                                else
                                {
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.GAP_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                }
                            }
                        }
                        //拆包
                        else
                        {
                            int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                            if (FinishBaoTotalNum > qtytotal)
                            {
                                //scaningfalse = "start";
                                //MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                                CsGetData.UpdataScanLineUp();
                            }
                            else
                            {
                                if (MotorStaus != 1)
                                {
                                    M_scaningsetup = "start";
                                }
                                if (FinishBaoTotalNum == qtytotal)//刚好达到本次订单数量
                                {
                                    //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1,IsEndScan=1 WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                   // MessageBox.Show("已经达到订单数，请切换订单！！");
                                   // CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.GAP_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 ,IsEndScan=1 WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                   // scaningfalse = "start";
                                   // MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                                    CsGetData.UpdataScanLineUp();
                                }
                                else
                                {
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.GAP_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE packing_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["packing_guid"].ToString().Trim()));
                                }
                            }
                        }
                    }
                    //优衣库和GU订单
                    else
                    {
                        int Qty_per_Set = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["Qty_per_Set"].ToString().Trim());//色号每箱应装条数
                        int Picking_Unit = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["Picking_Unit"].ToString().Trim());//色号每包条数
                        int CurrentPackingNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["CurrentPackingNum"].ToString().Trim());//总箱数
                        int qtytotal = Qty_per_Set * CurrentPackingNum;//当前共需条数
                        M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"] = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim()) + 1;
                        //优衣库和GU订单不拆包
                        if (M_stcScanData.IsUnpacking.Contains("不拆包"))
                        {
                            baonum_unit = Qty_per_Set / Picking_Unit;
                            int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                            if (FinishBaoTotalNum * Picking_Unit > qtytotal)
                            {
                              //  scaningfalse = "start";
                               // MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                                CsGetData.UpdataScanLineUp();
                            }
                            else
                            {
                                if (MotorStaus != 1)
                                {
                                    M_scaningsetup = "start";
                                }
                                if (FinishBaoTotalNum * Picking_Unit == qtytotal)//刚好达到本次订单数量
                                {
                                    //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 ,IsEndScan=1 WHERE YK_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["Doguid"].ToString().Trim()));
                                    //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYK_GU_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1,IsEndScan=1  WHERE Doguid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["Doguid"].ToString().Trim()));
                                    //MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                                    CsGetData.UpdataScanLineUp();
                                }
                                else
                                {
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE YK_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["Doguid"].ToString().Trim()));
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYK_GU_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE Doguid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["Doguid"].ToString().Trim()));
                                }
                            }
                        }
                        //优衣库和GU订单拆包
                        else
                        {
                            baonum_unit = Qty_per_Set;
                            int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                            if (FinishBaoTotalNum > qtytotal)
                            {
                                scaningfalse = "start";
                                MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                                CsGetData.UpdataScanLineUp();
                            }
                            else
                            {
                                if (MotorStaus != 1)
                                {
                                    M_scaningsetup = "start";
                                }
                                if (FinishBaoTotalNum == qtytotal)//刚好达到本次订单数量
                                {
                                   // CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 ,IsEndScan=1 WHERE YK_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["Doguid"].ToString().Trim()));
                                  //  CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYK_GU_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE Doguid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["Doguid"].ToString().Trim()));
                                    //MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                                    CsGetData.UpdataScanLineUp();
                                }
                                else
                                {
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE YK_guid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["Doguid"].ToString().Trim()));
                                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYK_GU_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE Doguid='{0}'", M_stcScanData.dtCurrentData.Rows[0]["Doguid"].ToString().Trim()));
                                }
                            }
                        }
                    }
                    if (count_unit < baonum_unit)
                    {
                        count_unit++;
                        Cslogfun.WriteToScaningLog("本箱共：" + baonum_unit + "包" + ",本次扫码第" + count_unit + "包");
                    }
                    else
                    {
                        count_unit = 1;
                        Cslogfun.WriteToScaningLog("本箱共：" + baonum_unit + "包" + ",本次扫码第" + count_unit + "包");
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// 混色是否正确扫码信息
        /// </summary>
        public bool IsPassColorMixingPcking()
        {
            try
            {
                bool bool_val = true;
                bool existtioama = false;
                bool endpacking = true;
                //if (M_stcScanData.IsEndScan)
                //{
                //    scaningfalse = "start";
                //    scaningend = "start";
                //    MessageBox.Show("已完成本次装箱数量，请换到下一单的货品");
                //}
                //else
                //{
                for (int i = 0; i < M_stcScanData.dtCurrentData.Rows.Count; i++)
                {
                    string tiaoma = M_stcScanData.dtCurrentData.Rows[i]["TiaoMaVal"].ToString().Trim();
                    if (M_stcScanData.strTiaoMa.Contains(M_stcScanData.dtCurrentData.Rows[i]["TiaoMaVal"].ToString().Trim()))
                    {
                        int Qty_per_Set = int.Parse(M_stcScanData.dtCurrentData.Rows[i]["Qty_per_Set"].ToString().Trim());//色号每箱应装包数
                        int FinishPackingNum = int.Parse(M_stcScanData.dtCurrentData.Rows[i]["FinishPackingNum"].ToString().Trim());//本箱已装包数
                        if (FinishPackingNum < Qty_per_Set)//数量小于箱应装包数未装完
                        {
                        }
                        else//数量大于箱应装包数
                        {
                            string IsEndPacking = "";
                            for (int j = 0; j < M_stcScanData.dtCurrentData.Rows.Count; j++)//判断是否本箱全部放完
                            {
                                IsEndPacking = M_stcScanData.dtCurrentData.Rows[j]["IsEndPacking"].ToString().Trim();
                                if (!IsEndPacking.ToUpper().Contains("TRUE"))
                                {
                                    endpacking = false;//未放完本箱
                                }
                            }
                            if (!endpacking)//还未放完本箱
                            {
                                scaningfalse = "start";
                                LastScanflag = "";
                                MessageBox.Show("此色号已经放完请换下一种色号");
                                Cslogfun.WriteToScaningLog("此色号已经放完请换下一种色号");
                                return false;
                            }
                        }
                        existtioama = true;
                        //更新总包数
                        //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKMarkingLineUp SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE YK_guid='{0}'", M_stcScanData.dtCurrentData.Rows[i]["YK_guid"].ToString().Trim()));
                        //CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYK_GU_Current SET FinishBaoTotalNum=ISNULL(FinishBaoTotalNum,0)+1 WHERE Doguid='{0}'", M_stcScanData.dtCurrentData.Rows[i]["YK_guid"].ToString().Trim()));
                    }
                }
                if (!existtioama)
                {
                    scaningfalse = "start";
                    if (string.IsNullOrEmpty(endscanningflag) && string.IsNullOrEmpty(ispassflag))
                    {
                        LastScanflag = "";
                        MessageBox.Show("此条码:" + M_stcScanData.strTiaoMa + ",与当前订单的条码信息不一致，请检查！！");
                        Cslogfun.WriteToScaningLog("此条码:" + M_stcScanData.strTiaoMa+",与当前订单的条码信息不一致，请检查！！");
                    }
                    return false;
                }
                else if (string.IsNullOrEmpty(M_stcScanData.strPoTiaoMa.Trim()))
                {
                    LastScanflag = "";
                    scaningfalse = "start";
                    MessageBox.Show("无可执行订单，请检查！！");
                    Cslogfun.WriteToScaningLog("无可执行订单，请检查！！");
                    return false;
                }
                else
                {
                    if (MotorStaus != 1)
                    {
                        M_scaningsetup = "start";
                    }
                }
                //}
                return bool_val;
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// 单色是否正确扫码信息
        /// </summary>
        public bool IsPassMonochromePacking()
        {
            try
            {
                bool bool_val = true;
                //扫码有误
                if (!M_stcScanData.strTiaoMa.Contains(M_stcScanData.strPoTiaoMa.Trim()))
                {
                    scaningfalse = "start";
                    LastScanflag = "";
                    MessageBox.Show("此条码:" + M_stcScanData.strTiaoMa + ",与当前订单的条码信息不一致，请检查！！");
                    Cslogfun.WriteToScaningLog("此条码:" + M_stcScanData.strTiaoMa + ",与当前订单的条码信息不一致，请检查！！");
                    return false;
                }
                else if (string.IsNullOrEmpty(M_stcScanData.strPoTiaoMa.Trim()))
                {
                    LastScanflag = "";
                    scaningfalse = "start";
                    MessageBox.Show("无可执行订单，请检查！");
                    Cslogfun.WriteToScaningLog("无可执行订单，请检查！");
                    return false;
                }
                //扫码正确
                else
                {
                    //GAP订单
                    if (M_stcScanData.potype.Contains("GAP"))
                    {
                        int qty = 0; int qtytotal = 0;
                        string str = M_stcScanData.dtCurrentData.Rows[0]["内包装计数"].ToString().Trim();
                        if (string.IsNullOrEmpty(str))
                        {
                            str = "0";
                        }
                        if (str.Trim() == "0")
                        {
                            qty = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["内部包装的项目数量"].ToString().Trim());//单条一包每箱应装条数
                        }
                        else
                        {
                            qty = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["内包装计数"].ToString().Trim()) * int.Parse(M_stcScanData.dtCurrentData.Rows[0]["内部包装的项目数量"].ToString().Trim());//多条一包每箱应装条数
                        }
                        int CurrentPackingNum = int.Parse(M_stcScanData.CurrentPackingNum.ToString().Trim());//当前总箱数
                        qtytotal = qty * CurrentPackingNum;//当前需总条数
                        //单条一包不拆包
                        if (M_stcScanData.IsUnpacking.Contains("不拆包") && str.Trim() == "0")
                        {
                            int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                            //if (FinishBaoTotalNum > qtytotal)
                            //{
                            //    scaningfalse = "start";
                            //    MessageBox.Show("已经超设置的订单数，请切换订单！！");
                            //    bool_val = false;
                            //}
                            //else
                            //{
                            if (MotorStaus != 1)
                            {
                                M_scaningsetup = "start";
                            }
                            //}
                        }
                        //多条一包不拆包
                        else if (M_stcScanData.IsUnpacking.Contains("不拆包") && str != "0")
                        {
                            int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                            int Picking_Unit = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["内包装计数"].ToString().Trim());
                            //if (FinishBaoTotalNum * Picking_Unit > qtytotal)
                            //{
                            //    scaningfalse = "start";
                            //    MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                            //    CsGetData.UpdataScanLineUp();
                            //    bool_val = false;
                            //}
                            //else
                            //{
                            if (MotorStaus != 1)
                            {
                                M_scaningsetup = "start";
                            }
                            //}
                        }
                        //拆包
                        else
                        {
                            int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                            //if (FinishBaoTotalNum > qtytotal)
                            //{
                            //    scaningfalse = "start";
                            //    MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                            //    CsGetData.UpdataScanLineUp();
                            //    bool_val = false;
                            //}
                            //else
                            //{
                            if (MotorStaus != 1)
                            {
                                M_scaningsetup = "start";
                            }
                            //}
                        }
                    }
                    //优衣库和GU订单
                    else
                    {
                        int Qty_per_Set = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["Qty_per_Set"].ToString().Trim());//色号每箱应装条数
                        int Picking_Unit = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["Picking_Unit"].ToString().Trim());//色号每包条数
                        int CurrentPackingNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["CurrentPackingNum"].ToString().Trim());//总箱数
                        int qtytotal = Qty_per_Set * CurrentPackingNum;//当前共需条数
                        //优衣库和GU订单不拆包
                        if (M_stcScanData.IsUnpacking.Contains("不拆包"))
                        {
                            int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                            //if (FinishBaoTotalNum * Picking_Unit > qtytotal)
                            //{
                            //scaningfalse = "start";
                            //MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                            //CsGetData.UpdataScanLineUp();
                            //bool_val = false;
                            //}
                            //else
                            //{
                            if (MotorStaus != 1)
                            {
                                M_scaningsetup = "start";
                            }
                            //}
                        }
                        //优衣库和GU订单拆包
                        else
                        {
                            int FinishBaoTotalNum = int.Parse(M_stcScanData.dtCurrentData.Rows[0]["FinishBaoTotalNum"].ToString().Trim());
                            //if (FinishBaoTotalNum > qtytotal)
                            //{
                            //    scaningfalse = "start";
                            //    MessageBox.Show("已经达到订单数，请切换下一单的扫码！！");
                            //    CsGetData.UpdataScanLineUp();
                            //    bool_val = false;
                            //}
                            //else
                            //{
                            if (MotorStaus != 1)
                            {
                                M_scaningsetup = "start";
                            }
                            //}
                        }
                    }
                }
                return bool_val;
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// 切换读取扫码个数
        /// </summary>
        /// <param name="Sample_Code_"></param>
        public static void ChangeScan(string Sample_Code_)
        {
            try
            {
                if (connet)
                {
                    if (Sample_Code_.Contains("03"))
                    {
                        string str = Soketscaning.Gocmd("WP,250,2");//改变读取条码数量为2个
                    }
                    else
                    {
                        string str = Soketscaning.Gocmd("WP,250,1");//改变读取条码数量为2个
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }

    }
}
