﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WindowsForms01
{
    public class Cslogfun
    {
        public static object locktrg = new object();
        public static object locker1 = new object();
        public static object locker2 = new object();
        public static object lockmarking = new object();
        public static object lockprint = new object();
        public static object lockyamaha = new object();
        public static object lockscaning = new object();
        public static object lockerpc = new object();
        public static object locker = new object();
        public static object lockerCartorn = new object();
        /// <summary>
        /// 生成错误信息记录
        /// </summary>
        /// <param name="err"></param>
        public static void WriteToLog(Exception err)
        {
            try
            {
                string path = @"D:\" + CSReadPlc.DevName + @"\log\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(@"D:\" + CSReadPlc.DevName + @"\Log"))
                {
                    Directory.CreateDirectory(@"D:\" + CSReadPlc.DevName + @"\log");
                }
                lock (locker1)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine("报错时间:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss") + " " + DateTime.Now.Millisecond.ToString());
                            sw.WriteLine(CSReadPlc.DevName + "出错文件：" + "原因：" + err.ToString());
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

        }
        public static void WriteToLog(string err)
        {
            try
            {
                string path = @"D:\" + CSReadPlc.DevName + @"\log\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(@"D:\" + CSReadPlc.DevName + @"\Log"))
                {
                    Directory.CreateDirectory(@"D:\" + CSReadPlc.DevName + @"\log");
                }
                lock (locker1)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine("报错时间:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss") + " " + DateTime.Now.Millisecond.ToString());
                            sw.WriteLine(CSReadPlc.DevName + "出错文件：" + "原因：" + err.ToString());
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

        }
        public static void WriteToyamahaLog(string err)
        {
            try
            {
                string path = @"D:\" + CSReadPlc.DevName + @"\YamahaLog\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(@"D:\" + CSReadPlc.DevName + @"\YamahaLog"))
                {
                    Directory.CreateDirectory(@"D:\" + CSReadPlc.DevName + @"\YamahaLog");
                }
                lock (lockyamaha)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine(CSReadPlc.DevName + "执行任务:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss ") + " " + DateTime.Now.Millisecond.ToString() + err);
                            // sw.WriteLine("执行任务："  + err);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

        }
        public static void WriteTomarkingLog(string err)
        {
            string path = @"D:\" + CSReadPlc.DevName + @"\MarkingLog\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
            if (!Directory.Exists(@"D:\" + CSReadPlc.DevName + @"\MarkingLog"))
            {
                Directory.CreateDirectory(@"D:\" + CSReadPlc.DevName + @"\MarkingLog");
            }
            lock (lockmarking)
            {
                using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.BaseStream.Seek(0, SeekOrigin.End);
                        if (err==""+ Environment.NewLine)
                        {
                            sw.WriteLine(Environment.NewLine);
                        }
                        else
                        {
                            sw.WriteLine(CSReadPlc.DevName + "执行任务:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss ") + " " + DateTime.Now.Millisecond.ToString() + "  " + err);
                        }
                        // sw.WriteLine("执行任务："  + err);
                        sw.Flush();
                        sw.Close();
                    }
                }
            }

        }
        public static void WriteToprintgLog(string err)
        {
            try
            {
                string path = @"D:\" + CSReadPlc.DevName + @"\printLog\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(@"D:\" + CSReadPlc.DevName + @"\printLog"))
                {
                    Directory.CreateDirectory(@"D:\" + CSReadPlc.DevName + @"\printLog");
                }
                lock (lockprint)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine(CSReadPlc.DevName + "执行任务:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss ") + " " + DateTime.Now.Millisecond.ToString() + "  " + err);
                            // sw.WriteLine("执行任务："  + err);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

        }
        public static void WriteToScaningLog(string err)
        {
            try
            {
                string path = @"D:\" + CSReadPlc.DevName + @"\ScaningLog\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(@"D:\" + CSReadPlc.DevName + @"\ScaningLog"))
                {
                    Directory.CreateDirectory(@"D:\" + CSReadPlc.DevName + @"\ScaningLog");
                }
                lock (lockscaning)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine(CSReadPlc.DevName + "执行任务:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss ") + " " + DateTime.Now.Millisecond.ToString() + "  " + err);
                            // sw.WriteLine("执行任务："  + err);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

        }
        public static void WriteOperationlog(string err)
        {
            try
            {
                string path = @"D:\" + CSReadPlc.DevName + @"\Operationlog\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(@"D:\" + CSReadPlc.DevName + @"\Operationlog"))
                {
                    Directory.CreateDirectory(@"D:\" + CSReadPlc.DevName + @"\Operationlog");
                }
                lock (locker)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine(CSReadPlc.DevName + "执行任务:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss ")  + DateTime.Now.Millisecond.ToString() + "  " + err);
                            // sw.WriteLine("执行任务："  + err);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

        }
        public static void WriteToPCsoketLog(string err)
        {
            try
            {
                string path = @"D:\" + CSReadPlc.DevName + @"\ToPCsoket\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(@"D:\" + CSReadPlc.DevName + @"\ToPCsoket"))
                {
                    Directory.CreateDirectory(@"D:\" + CSReadPlc.DevName + @"\ToPCsoket");
                }
                lock (lockerpc)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine(CSReadPlc.DevName + "执行任务:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss ") + " " + DateTime.Now.Millisecond.ToString() + "  " + err);
                            // sw.WriteLine("执行任务："  + err);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

        }
        public static void WriteToPalletizingLog(string err)
        {
            try
            {
                string path = @"D:\" + CSReadPlc.DevName + @"\Palletizinglog\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(@"D:\" + CSReadPlc.DevName + @"\Palletizinglog"))
                {
                    Directory.CreateDirectory(@"D:\" + CSReadPlc.DevName + @"\Palletizinglog");
                }
                lock (lockerpc)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine(CSReadPlc.DevName + "执行任务:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss ") + " " + DateTime.Now.Millisecond.ToString() + "  " + err);
                            // sw.WriteLine("执行任务："  + err);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

        }
        public static void WriteToCartornLog(string err)
        {
            try
            {
                string path = @"D:\" + CSReadPlc.DevName + @"\CartornLog\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(@"D:\" + CSReadPlc.DevName + @"\CartornLog"))
                {
                    Directory.CreateDirectory(@"D:\" + CSReadPlc.DevName + @"\CartornLog");
                }
                lock (lockerCartorn)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            if (err == "" + Environment.NewLine)
                            {
                                sw.WriteLine(Environment.NewLine);
                            }
                            else
                            {
                                sw.WriteLine(CSReadPlc.DevName + "执行任务:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss ") + " " + DateTime.Now.Millisecond.ToString() + "  " + err);
                            }
                            // sw.WriteLine("执行任务："  + err);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

        }
        public static void DeleteLog(int days_)
        {
            #region//清理日志文件
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\YamahaLog", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\MarkingLog", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\log", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\ScaningLog", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\Operationlog", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\Palletizinglog", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\logother", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\printLog", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\ToPCsoket", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\CsScaning", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\Image_recognition", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\SoketYaMaHa", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\Soketprint", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\GetData", days_);
            Delet_click.DeleteLogTxt(@"D:\" + CSReadPlc.DevName + @"\CartornLog", 60);
            #endregion
        }
    }
}
