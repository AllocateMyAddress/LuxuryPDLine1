﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCData;
using System.Threading;
using Image_recognition;

namespace WindowsForms01
{
    public class CsTrg
    {
        #region 变量
        public static string M_StartTrg = "";
        public static string StrNgChange = "";
        public static Dictionary<string, int> M_DicZb = new Dictionary<string, int>();//拍照坐标
        public static bool Iscamera_sendconnet = false;//相机是否连接
        public static bool Iscamera_recconnet = false;//相机是否连接
        public static string CameraIP = "";//相机IP
        public static string CameraSendIPoint = "";//相机IP
        public static string CameraRecIPoint = "";//相机IP
        public static MCData.MCdata mCdata = new MCdata();
        public static Image_recognition.C_GetZb M_C_GetZb = new C_GetZb(CSReadPlc.DevName);
        public static int M_trgcount = 0;
        public static string M_ngfalseflag = "";
        public static string M_ngtrueflag = "";
        public static int M_ngfalseval = 0;
        public static string M_ngfalsevalRest = "";
        public static string M_ngfalsecountflag = "";
        public static string M_UpdateTrgXYflag = "";
        public static int[] Ararr_zb = new int[2];
        public static int M_Trg = 0;//拍照启动信号
        GetPlcBao csGetBao;
        static A.BLL.newsContent content1 = new A.BLL.newsContent();
        #endregion
        public CsTrg(MCdata mcdata, GetPlcBao bao)
        {
            mCdata = mcdata;
            csGetBao = bao;
        }
        public void TrgSetup(string CameraIP_, string CameraRecIPoint_, string CameraSendIPoint_)
        {
            #region 相机拍照模块
            CameraIP = CameraIP_;
            CameraRecIPoint = CameraRecIPoint_;
            CameraSendIPoint = CameraSendIPoint_;
            Iscamera_sendconnet = M_C_GetZb.CameraConnetionSend(CameraIP_, CameraSendIPoint_);
            Iscamera_recconnet = M_C_GetZb.CameraConnetionRecive(CameraIP_, CameraRecIPoint_);
            Thread Th_Trg = new Thread(Trg);
            Th_Trg.IsBackground = true;
            Th_Trg.Start();
            if (Iscamera_sendconnet && Iscamera_recconnet)
            {
               Cslogfun.WriteToLog("启动连接相机成功ip:" + CameraIP_);
            }
            #endregion
        }
        //启动拍照
        void Trg()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(10);
                    Iscamera_sendconnet = M_C_GetZb.CameraConnetionSend(CameraIP, CameraSendIPoint);
                    Iscamera_recconnet = M_C_GetZb.CameraConnetionRecive(CameraIP, CameraRecIPoint);
                    if (!Iscamera_recconnet || !Iscamera_sendconnet)
                    {
                        Iscamera_sendconnet = M_C_GetZb.CameraConnetionSend(CameraIP, CameraSendIPoint);
                        Iscamera_recconnet = M_C_GetZb.CameraConnetionRecive(CameraIP, CameraRecIPoint);
                        if (!Iscamera_recconnet || !Iscamera_sendconnet)
                        {
                            CsFormShow.MessageBoxFormShow("相机通信断开，请检查网络是否通畅或者尝试重启软件");
                        }
                        else
                        {
                            CsFormShow.MessageBoxFormShow("相机重新连接成功");
                        }
                    }
                    else
                    {
                        if (M_MarkingData.M_PackingCode == "1" && StrNgChange != "1")//四行小箱
                        {
                            M_C_GetZb.Clientrec("CJB001");
                            //StrNgChange = "END";
                            StrNgChange = "1";
                            M_StartTrg = "";
                            Cslogfun.WriteTomarkingLog("切换到拍照模板一");
                        }
                        else if (M_MarkingData.M_PackingCode == "2" && StrNgChange != "2")//四行大箱58*38*29
                        {
                            M_C_GetZb.Clientrec("CJB002");
                            //StrNgChange = "END";
                            StrNgChange = "2";
                            M_StartTrg = "";
                            M_UpdateTrgXYflag = "";
                            Cslogfun.WriteTomarkingLog("切换到拍照模板二(C1)");
                            M_MarkingData.M_Trg_x =int.Parse( CsFormShow.SqlGetVal(string.Format("SELECT*FROM Trg_xy WHERE Trg_Code='{0}'","C1"), "XP"));
                            M_MarkingData.M_Trg_y = int.Parse(CsFormShow.SqlGetVal(string.Format("SELECT*FROM Trg_xy WHERE Trg_Code='{0}'", "C1"), "YP"));
                        }
                        else if (M_MarkingData.M_PackingCode == "3" && StrNgChange != "3")//六行小箱
                        {
                            M_C_GetZb.Clientrec("CJB003");
                            StrNgChange = "3";
                            M_StartTrg = "";
                            M_UpdateTrgXYflag = "";
                            Cslogfun.WriteTomarkingLog("切换到拍照模板三(C6)");
                            M_MarkingData.M_Trg_x = int.Parse(CsFormShow.SqlGetVal(string.Format("SELECT*FROM Trg_xy WHERE Trg_Code='{0}'", "C6"), "XP"));
                            M_MarkingData.M_Trg_y = int.Parse(CsFormShow.SqlGetVal(string.Format("SELECT*FROM Trg_xy WHERE Trg_Code='{0}'", "C6"), "YP"));
                        }
                        else if (M_MarkingData.M_PackingCode == "4" && StrNgChange != "4")//六行大箱
                        {
                            M_C_GetZb.Clientrec("CJB004");
                            StrNgChange = "4";
                            M_StartTrg = "";
                            M_UpdateTrgXYflag = "";
                            Cslogfun.WriteTomarkingLog("切换到拍照模板四(C4)");
                            M_MarkingData.M_Trg_x = int.Parse(CsFormShow.SqlGetVal(string.Format("SELECT*FROM Trg_xy WHERE Trg_Code='{0}'", "C4"), "XP"));
                            M_MarkingData.M_Trg_y = int.Parse(CsFormShow.SqlGetVal(string.Format("SELECT*FROM Trg_xy WHERE Trg_Code='{0}'", "C4"), "YP"));
                        }
                        else if (M_MarkingData.M_PackingCode == "8" && StrNgChange != "8")//四行小箱58*38*22
                        {
                            M_C_GetZb.Clientrec("CJB005");
                            StrNgChange = "8";
                            M_StartTrg = "";
                            M_UpdateTrgXYflag = "";
                            Cslogfun.WriteTomarkingLog("切换到拍照模板五(C2)");
                            M_MarkingData.M_Trg_x = int.Parse(CsFormShow.SqlGetVal(string.Format("SELECT*FROM Trg_xy WHERE Trg_Code='{0}'", "C2"), "XP"));
                            M_MarkingData.M_Trg_y = int.Parse(CsFormShow.SqlGetVal(string.Format("SELECT*FROM Trg_xy WHERE Trg_Code='{0}'", "C2"), "YP"));
                        }
                        else if (M_MarkingData.M_PackingCode == "6" && StrNgChange != "6")//999大箱特征二
                        {
                            M_C_GetZb.Clientrec("CJB006");
                            StrNgChange = "6";
                            M_StartTrg = "";
                            Cslogfun.WriteTomarkingLog("切换到拍照模板六");
                        }
                        //拍照执行操作
                        if (M_Trg == 1 && !string.IsNullOrEmpty(StrNgChange) && string.IsNullOrEmpty(M_StartTrg))
                        {
                            if (CSReadPlc.DevName.Trim() == "plc1")
                            {
                                M_C_GetZb.Clientsend("TRG");//拍照
                                M_StartTrg = "starttrg";
                                Cslogfun.WriteTomarkingLog("触发拍照成功");
                                //拍照后执行操作
                                if (M_ngfalseflag.Trim() == "" && M_ngtrueflag.Trim() == "")
                                {
                                    Thread.Sleep(500);
                                    //获取坐标
                                    if (M_C_GetZb.ZBDIC.Count == 4)//判断是否有拍照，并判断是否坐标信息收集完毕
                                    {
                                        Ararr_zb[0] = (int)M_C_GetZb.ZBDIC["XP"];
                                        Ararr_zb[1] = (int)M_C_GetZb.ZBDIC["YP"];
                                        int x = (int)M_C_GetZb.ZBDIC["X"];
                                        int y = (int)M_C_GetZb.ZBDIC["Y"];
                                        //拍照失败
                                        if (Ararr_zb[0] == 0 && Ararr_zb[1] == 0 && x == 0 && y == 0)
                                        {
                                            Ararr_zb[0] = M_MarkingData.M_Trg_x;
                                            Ararr_zb[1] = M_MarkingData.M_Trg_y;
                                            M_ngfalseflag = "start";
                                            Cslogfun.WriteToScaningLog("拍照失败");
                                            Cslogfun.WriteToScaningLog("执行参考坐标偏移x:" + M_MarkingData.M_Trg_x + ",y:" + M_MarkingData.M_Trg_y);
                                            Cslogfun.WriteToCartornLog("拍照失败");
                                            Cslogfun.WriteToCartornLog("执行参考坐标偏移x:" + M_MarkingData.M_Trg_x + ",y:" + M_MarkingData.M_Trg_y);
                                        }
                                        //拍照成功
                                        else
                                        {
                                            #region 复位失败信号
                                            if (CsTrg.M_ngfalseval == 1)
                                            {
                                                CsTrg.M_ngfalsevalRest = "start";
                                                TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                                while (CsTrg.M_ngfalseval == 1)
                                                {
                                                    TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                                    TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                                    if (ts.Seconds > 5)
                                                    {
                                                        break;
                                                    }
                                                }
                                            }
                                            #endregion
                                            M_ngtrueflag = "start";
                                            if (M_MarkingData.M_PackingCode == "2" && string.IsNullOrEmpty(M_UpdateTrgXYflag))//四行大箱58*38*29
                                            {
                                                M_UpdateTrgXYflag = "start";
                                                CsTrg.M_ngfalsevalRest = "";
                                                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE Trg_xy SET XP={0},YP={1} WHERE Trg_Code='{2}'",
                                                    Ararr_zb[0], Ararr_zb[1],"C1"));
                                            }
                                            else if (M_MarkingData.M_PackingCode == "8" && string.IsNullOrEmpty(M_UpdateTrgXYflag))//四行小箱58*38*22
                                            {
                                                M_UpdateTrgXYflag = "start";
                                                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE Trg_xy SET XP={0},YP={1} WHERE Trg_Code='{2}'", 
                                                    Ararr_zb[0], Ararr_zb[1], "C2"));
                                            }
                                            else if (M_MarkingData.M_PackingCode == "3" && string.IsNullOrEmpty(M_UpdateTrgXYflag))//六行小箱
                                            {
                                                M_UpdateTrgXYflag = "start";
                                                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE Trg_xy SET XP={0},YP={1} WHERE Trg_Code='{2}'",
                                                    Ararr_zb[0], Ararr_zb[1], "C6"));
                                            }
                                            else if (M_MarkingData.M_PackingCode == "4" && string.IsNullOrEmpty(M_UpdateTrgXYflag))//六行大箱
                                            {
                                                M_UpdateTrgXYflag = "start";
                                                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE Trg_xy SET XP={0},YP={1} WHERE Trg_Code='{2}'",
                                                    Ararr_zb[0], Ararr_zb[1], "C4"));
                                            }
                                        }
                                        M_C_GetZb.ZBDIC.Clear();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                CsLogFun.WriteToLog(err.ToString());
            }
        }
    }
}
