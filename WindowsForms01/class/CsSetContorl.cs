﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms01
{
    public class CsSetContorl:Button
    {
        public static void SetButton(Control Button_,FlatStyle flatStyle_, System.Drawing.Color Downcolor_, System.Drawing.Color Overcolor_, System.Drawing.Color BackColor_,int FontSize_)
        {
            Button buton_v = (Button)Button_;
            //设置边框
            buton_v.FlatStyle = flatStyle_;
            buton_v.FlatAppearance.BorderSize = 0;
            //背景颜色
            buton_v.FlatAppearance.MouseDownBackColor = Downcolor_;
            buton_v.FlatAppearance.MouseOverBackColor = Overcolor_;
            buton_v.BackColor = BackColor_;
            //字体
            buton_v.Font = new System.Drawing.Font("宋体", FontSize_);
        }
    }
}
