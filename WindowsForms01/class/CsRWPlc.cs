﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using Soketprint;
using MCData;
using Image_recognition;
using System.IO;
//using GetData;
using A.BLL;
using System.Data;
using System.Windows.Forms;

namespace WindowsForms01
{
    public class CsRWPlc
    {
        public static MCData.MCdata mCdata = new MCdata();
        GetPlcBao csGetBao;
        public static int setupsighg = 0;
        public static int setupWPlcsignal = 0;
        public static string PakingCount = "0";
        public static string NoOrderSignal = "";
        public static string PakingRest = "";
        public static string PlcChangePo = "0";
        public static string Palletizingflag = "";
        public static int ManualMarkingSignal = 0;
        public static int PlcManualSignal = 0;
        public static string PlcManualSignalFlag = "";
        public static string ManualMarkingSignalFlag = "";
       // public static string YesLabelsignalFlag = "";
        int readplc = 0;
        int writeplc = 0;
        Control pic1;
        Control pic2;
        MainrpForm Mainrpform = null;
        public CsRWPlc(MCdata mcdata, GetPlcBao bao)
        {
            mCdata = mcdata;
            csGetBao = bao;
        }
        /// <summary>
        /// /读取plc寄存器线程
        /// </summary>
        public void ReadPlcAdr()
        {
            //Thread Th_RPlc = new Thread(ReadPlc);
            //Th_RPlc.IsBackground = true;
            //Th_RPlc.Start();
        }
        /// <summary>
        /// 读取寄存器中的变量值线程
        /// </summary>
        /// <param name="pic1_"></param>
        /// <param name="pic2_"></param>
        public void ReadPlcTh(Control pic1_, Control pic2_)
        {
            Thread Th_RPlc = new Thread(ReadPlcThread);
            Th_RPlc.IsBackground = true;
            Th_RPlc.Start();
            pic1 = pic1_;
            pic2 = pic2_;
        }
        /// <summary>
        /// 写入plc线程
        /// </summary>
        public void WritePlcTh()
        {
            Thread Th_WPlc = new Thread(WritePlc);
            Th_WPlc.IsBackground = true;
            Th_WPlc.Start();
        }
        /// <summary>
        /// 装箱计数码垛计数线程
        /// </summary>
        public void ReadPackingNumber()
        {
            Thread Th_ReadPackingNumber = new Thread(ReadNumber);
            Th_ReadPackingNumber.IsBackground = true;
            Th_ReadPackingNumber.Start();
        }
        void ReadPlc()
        {
            try
            {
                while (true)
                {
                    lock (CSReadPlc.lockrwplc)
                    {
                        Thread.Sleep(10);
                        if (mCdata.IsConnect())
                        {

                            //readplc++;
                            //if (readplc == 400)
                            //{
                            //    readplc = 0;
                            //    Cslogfun.WriteToLog("监测读plc线程一次");
                            //}
                            bool IsReadFalse;
                            CSReadPlc.Dic_val_vlue[CSReadPlc.DevName] = csGetBao.GetVal(CSReadPlc.DevName, out IsReadFalse);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
        }
        void ReadPlcThread()
        {
            while (true)
            {
                lock (CSReadPlc.lockrwplc)
                {
                    Thread.Sleep(10);
                    if (mCdata.IsConnect())
                    {
                        bool IsReadFalse;
                        CSReadPlc.Dic_val_vlue[CSReadPlc.DevName] = csGetBao.GetVal(CSReadPlc.DevName, out IsReadFalse);
                        if (CsMarking.M_toplcmoving != "toplcmoving")
                        {
                            //扫描
                            CsScanning.M_scanning_trg = CSReadPlc.GetPlcInt("M_scanning_trg", CSReadPlc.DevName);
                            CsScanning.TiaoMaVal_Now = csGetBao.ReadPlcString("D1704",20);
                            CsScanning.EndscanningRising = CSReadPlc.GetPlcInt("EndscanningRising", CSReadPlc.DevName);
                            M_stcScanData.FinishLayernum = CSReadPlc.GetPlcInt("FinishLayernum", CSReadPlc.DevName);
                            int M_engine = CSReadPlc.GetPlcInt("M_engine", CSReadPlc.DevName);
                            CsScanning.MotorStaus = CSReadPlc.GetPlcInt("MotorStaus", CSReadPlc.DevName);
                            CsScanning.M_ScanOk = CSReadPlc.GetPlcInt("ScanOk", CSReadPlc.DevName);
                            CsScanning.M_ScanBack = CSReadPlc.GetPlcInt("ScanBack", CSReadPlc.DevName);
                            CsScanning.AllowScanSignal= CSReadPlc.GetPlcInt("AllowScanSignal", CSReadPlc.DevName).ToString();
                            #region yamaha
                            CsSendYaMaHa.PoData.Orientation = CSReadPlc.GetPlcString("Orientation", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.LineNum = "1" /*CSReadPlc.GetPlcString("LineNum", CSReadPlc.DevName)*/;
                            CsSendYaMaHa.PoData.M_strcolumn = CSReadPlc.GetPlcString("ColumnNunber", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.M_strLayernum = CSReadPlc.GetPlcString("M_strLayernum", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.M_strLong = CSReadPlc.GetPlcString("MarkLong", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.M_strWide = CSReadPlc.GetPlcString("MarkWideth", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.M_strNowLong = CSReadPlc.GetPlcString("NowLong", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.M_strNowWide = CSReadPlc.GetPlcString("NowWidth", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.M_strNowHeghit = CSReadPlc.GetPlcString("NowHeghit", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.M_strThick = CSReadPlc.GetPlcString("Thick", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.GoodWeight = CSReadPlc.GetPlcString("GoodWeight", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.M_strcomingtype = "1";
                            CsSendYaMaHa.PoData.M_strSwitchsignal = CSReadPlc.GetPlcString("M_strSwitchsignal", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.M_Grabspeed = CSReadPlc.GetPlcString("Grabspeed", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.Jianspeed = CSReadPlc.GetPlcString("Jianspeed", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.IsRotate = CSReadPlc.GetPlcString("IsRotate", CSReadPlc.DevName);
                            CsSendYaMaHa.PoData.HookDirection = CSReadPlc.GetPlcString("HookDirection", CSReadPlc.DevName);
                            CsSendYaMaHa.IsLeakage = CSReadPlc.GetPlcInt("IsLeakage", CSReadPlc.DevName);
                            #endregion
                            #region 喷码
                            CsTrg.M_Trg = CSReadPlc.GetPlcInt("M_Trg", CSReadPlc.DevName);//获取拍照信号
                            CsMarking.M_MarkCount = CSReadPlc.GetPlcInt("M_MarkCount", CSReadPlc.DevName);//喷码请求信号
                            M_MarkingData.M_PackingCode = CSReadPlc.GetPlcInt("M_PackingCode", CSReadPlc.DevName).ToString().Trim();//喷码类型信号
                            M_MarkingData.IsRepeatCheck = CSReadPlc.GetPlcInt("IsRepeatCheck", CSReadPlc.DevName).ToString().Trim();//喷码类型信号
                            CsMarking.M_PackingType = CSReadPlc.GetPlcInt("M_PackingType", CSReadPlc.DevName).ToString().Trim();//喷码类型信号
                            CsMarking.M_MarkSetUp = CSReadPlc.GetPlcString("M_Markendsignal", CSReadPlc.DevName).ToString().Trim();//喷码启动
                            CsMarking.M_Markend = CSReadPlc.GetPlcString("M_Markend", CSReadPlc.DevName).ToString().Trim();//喷码完成
                            M_MarkingData.M_MarkPattern = CSReadPlc.GetPlcInt("MarkPattern", CSReadPlc.DevName).ToString().Trim();//喷码模式
                            M_MarkingData.M_CatornType = CSReadPlc.GetPlcInt("CatornType", CSReadPlc.DevName).ToString().Trim();//箱子大小类型
                            CsRWPlc.PakingCount = CSReadPlc.GetPlcInt("PakingCount", CSReadPlc.DevName).ToString().Trim();//当前单装箱数量
                            CsRWPlc.PlcChangePo = CSReadPlc.GetPlcInt("PlcChangePo", CSReadPlc.DevName).ToString().Trim();//plc换单信号
                            CsMarking.M_Markchange = CSReadPlc.GetPlcInt("M_Markchange", CSReadPlc.DevName).ToString().Trim();//喷码切换内容写入plc信号
                            ManualMarkingSignal = CSReadPlc.GetPlcInt("ManualMarkingSignal", CSReadPlc.DevName);//
                            CsMarking.IsLabelsignal = CSReadPlc.GetPlcInt("IsLabelsignal", CSReadPlc.DevName).ToString().Trim();
                            #endregion
                            //打印
                            CsPrint.M_printsignal = CSReadPlc.GetPlcInt("M_printsignal", CSReadPlc.DevName);
                            CsPrint.M_Testprintsignal = CSReadPlc.GetPlcInt("M_Testprintsignal", CSReadPlc.DevName);
                            CsPrint.PrintLineType = CSReadPlc.GetPlcInt("PrintLineType", CSReadPlc.DevName);
                            //拍照
                            CsTrg.M_ngfalseval = CSReadPlc.GetPlcInt("M_ngfalse", CSReadPlc.DevName);
                            //码垛PalletizingCount
                            CsPalletizing.PalletizingCount = CSReadPlc.GetPlcInt("PalletizingCount", CSReadPlc.DevName);
                            M_stcPalletizingData.Palletizing_Unit = CSReadPlc.GetPlcInt("Palletizing_Unit", CSReadPlc.DevName);
                            M_stcPalletizingData.ReadPalletizingflag = CSReadPlc.GetPlcString("ReadPalletizingflag", CSReadPlc.DevName);
                            M_stcPalletizingData.PalletizingLineType = CSReadPlc.GetPlcString("PalletizingLineType", CSReadPlc.DevName);
                            CsPrint.M_serialsignal = CSReadPlc.GetPlcInt("M_serialsignal", CSReadPlc.DevName);
                            //开机启动信号
                            setupsighg = CSReadPlc.GetPlcInt("SetupReadPlc", CSReadPlc.DevName);
                            PlcManualSignal = CSReadPlc.GetPlcInt("PlcManualSignal", CSReadPlc.DevName);//PLC手动信号
                        }
                    }
                    else
                    {
                        if (MainrpForm.Devicename.Trim() == "plc2")
                        {
                            mCdata.NewTcp(MainrpForm.Plc2IP, int.Parse(MainrpForm.Plc2IPoint));
                        }
                        else if (MainrpForm.Devicename.Trim() == "plc1")
                        {
                            mCdata.NewTcp(MainrpForm.Plc1IP, int.Parse(MainrpForm.Plc1IPoint));
                        }
                        if (!mCdata.IsConnect())
                        {
                            pic1.Visible = false;
                           // golabel.Visible = false;
                            pic2.Visible = true;
                            CsFormShow.MessageBoxFormShow("PLC连接断开请检查网络是否通畅或者尝试重启软件");
                        }
                        else
                        {
                            pic1.Visible = true;
                            pic2.Visible = false;
                           // stoplabel.Visible = false;
                            CsFormShow.MessageBoxFormShow("提示：设备再次启动完毕");
                        }
                    }
                }
            }
        }
        void WritePlc()
        {
            while (true)
            {
                try
                {
                    lock (CSReadPlc.lockrwplc)
                    {
                        Thread.Sleep(1);

                        if (mCdata.IsConnect())
                        {
                            //writeplc++;
                            //if (writeplc == 200)
                            //{
                            //    writeplc = 0;
                            //    Cslogfun.WriteToLog("监测写plc线程一次");
                            //}
                            //开箱计数
                            if (PakingRest.Contains("start"))
                            {
                                csGetBao.WriteToPlc("PakingCount", 0, CSReadPlc.DevName);//复位计数
                                PakingRest = "";
                            }
                            #region 扫描
                            if (CsScanning.scaningend.Contains("start"))
                            {
                                CsScanning.scaningend = "";
                                csGetBao.WriteToPlc("M_scanning_trg", 0, CSReadPlc.DevName);
                                //Thread.Sleep(10);
                            }
                            if (CsScanning.endscanningflag.Contains("start"))
                            {
                                CsScanning.endscanningflag = "";
                                csGetBao.WriteToPlc("ScanOk", 0, CSReadPlc.DevName);
                            }
                            if (CsScanning.scaningfalse.Contains("start"))
                            {
                                CsScanning.scaningfalse = "";
                                csGetBao.WriteToPlc("M_engine", 1, CSReadPlc.DevName);//电机停止
                                //csGetBao.WriteToPlc("M_scanning_trg", 0, CSReadPlc.DevName);
                                //CsScanning.M_scanning_trg = 0;
                                // Cslogfun.WriteToScaningLog("扫描失败:" + M_stcScanData.strTiaoMa);
                            }
                            if (CsScanning.M_scaningsetup.Contains("start"))
                            {
                                CsScanning.M_scaningsetup = "";
                                csGetBao.WriteToPlc("M_engine", 2, CSReadPlc.DevName);//电机启动
                                                                                      // CsScanning.MotorStaus = 1;
                            }
                            if (CsScanning.Countflag.Contains("start"))
                            {
                                CsScanning.Countflag = "";
                                csGetBao.WriteToPlc("ScanBack", 0, CSReadPlc.DevName);//验针计回退信号复位
                                Cslogfun.WriteToScaningLog("复位验针机回退信号");
                            }
                            if (CsPublicVariablies.Scan.ClearNowTiaoMaFlag == "start")
                            {
                                CsPublicVariablies.Scan.ClearNowTiaoMaFlag = "";
                                int[] arr_tiaoma = new int[20];
                                for (int i = 0; i < arr_tiaoma.Length; i++)
                                {
                                    arr_tiaoma[i] = 0;
                                }
                                csGetBao.WriteToPlc("D5000", 20, arr_tiaoma);
                                csGetBao.WriteToPlc("D1704", 20, arr_tiaoma);
                            }
                            if (M_stcScanData.M_WriteToPlcSku.Contains("start"))
                            {
                                M_stcScanData.M_WriteToPlcSku = "";
                                int[] arr_tiaoma = new int[181];
                                int[] arr_Qty_per_Set = new int[10];
                                for (int i = 0; i < arr_tiaoma.Length; i++)
                                {
                                    arr_tiaoma[i] = 0;
                                }
                                for (int i = 0; i < arr_Qty_per_Set.Length; i++)
                                {
                                    arr_Qty_per_Set[i] = 0;
                                }
                                csGetBao.WriteToPlc("D5100", 181, arr_tiaoma);
                                csGetBao.WriteToPlc("D5060", 10, arr_Qty_per_Set);
                                //写入
                                for (int i = 0; i < M_stcScanData.M_List_WriteToPlcSku.ToList().Count; i++)
                                {
                                    string sku_dress = M_stcScanData.M_List_WriteToPlcSku.ToList()[i].dic_sku.ToList()[0].Key;
                                    string sku_val = M_stcScanData.M_List_WriteToPlcSku.ToList()[i].dic_sku.ToList()[0].Value;
                                    string Layernum_dress = M_stcScanData.M_List_WriteToPlcSku.ToList()[i].dic_Write_Layernum.ToList()[0].Key;
                                    string Layernum_val = M_stcScanData.M_List_WriteToPlcSku.ToList()[i].dic_Write_Layernum.ToList()[0].Value;
                                    csGetBao.WriteStringToPlc(sku_dress, sku_val);
                                    csGetBao.WriteToPlc(Layernum_dress, Layernum_val);
                                }
                                if (M_stcScanData.M_List_WriteToPlcSku.Count>1)
                                {
                                    csGetBao.WriteToPlc("IsMixing", "1",CSReadPlc.DevName);//混色信号
                                    Cslogfun.WriteToScaningLog("发送混色信号1到寄存器IsMixing");
                                }
                                else
                                {
                                    csGetBao.WriteToPlc("IsMixing", "0",CSReadPlc.DevName);//单色信号
                                    Cslogfun.WriteToScaningLog("发送单色信号0到寄存器IsMixing");
                                }
                            }
                            //允许扫码时写入
                            if (CsPublicVariablies.Scan.AllowScanSignal == "start")
                            {
                                CsPublicVariablies.Scan.AllowScanSignal = "";
                                csGetBao.WriteToPlc("AllowScanSignal", 1, CSReadPlc.DevName);//复位PLC的换款信号
                            }
                            //手动状态不允许写入
                            if (CsPublicVariablies.Scan.AllowScanSignal == "stop")
                            {
                                csGetBao.WriteToPlc("AllowScanSignal", 0, CSReadPlc.DevName);//复位PLC的换款信号
                            }
                            #endregion

                            #region  yamaha
                            if (CsSendYaMaHa.M_strIsSwitch)
                            {
                                csGetBao.WriteToPlc("M_strSwitchsignal", 0, CSReadPlc.DevName);//复位PLC的换款信号
                                CsSendYaMaHa.M_strIsSwitch = false;
                                CsSendYaMaHa.recval = "";
                                CsSendYaMaHa.M_IsSendOk = true;
                                Cslogfun.WriteToyamahaLog("复位换款信号成功");
                            }
                            if (CsSendYaMaHa.M_IsSendOk)
                            {
                                csGetBao.WriteToPlc("M_strIsSwitchok", 1, CSReadPlc.DevName);//机器人回复成功接收信号之后给PLC发送完成信号
                                CsSendYaMaHa.M_IsSendOk = false;
                                Cslogfun.WriteToyamahaLog("换款完成信号给PLC成功" + Environment.NewLine);
                            }
                            if (CsSendYaMaHa.M_YaMaHaToPlcflag =="start")
                            {
                                CsSendYaMaHa.M_YaMaHaToPlcflag = "";
                                csGetBao.WriteToPlc("IsLeakage", 0, CSReadPlc.DevName);//复位
                                csGetBao.WriteToPlc("YaMaHaCount", CsSendYaMaHa.M_GetYaMaHaData, CSReadPlc.DevName);//复位
                            }
                            #endregion
                            #region 喷码拍照
                            //拍照
                            if (CsTrg.M_StartTrg.Contains("start"))
                            {
                                csGetBao.WriteToPlc("M_Trg", 0, CSReadPlc.DevName);//复位拍照信号
                                Thread.Sleep(10);
                                if (CsTrg.M_ngfalseflag.Trim().Contains("start"))
                                {
                                    csGetBao.WriteToPlc("M_ngfalse", 1, CSReadPlc.DevName);//拍照失败信号
                                    csGetBao.WriteToPlc("M_ingxp", 2, CsTrg.Ararr_zb, CSReadPlc.DevName);//发送偏移坐标值
                                    csGetBao.WriteToPlc("M_ngend", 1, CSReadPlc.DevName);//发送拍照完成信号
                                    CsTrg.M_Trg = 0;
                                    CsTrg.M_StartTrg = "";
                                    CsTrg.M_ngfalseflag = "";
                                    Cslogfun.WriteToCartornLog("发送参考坐标偏移:x偏移" + CsTrg.Ararr_zb[0] + "y偏移" + CsTrg.Ararr_zb[1] + Environment.NewLine);
                                }
                                if (CsTrg.M_ngtrueflag.Trim().Contains("start"))
                                {
                                    csGetBao.WriteToPlc("M_ingxp", 2, CsTrg.Ararr_zb, CSReadPlc.DevName);//发送偏移坐标值
                                    csGetBao.WriteToPlc("M_ngend", 1, CSReadPlc.DevName);//发送拍照完成信号
                                    CsTrg.M_Trg = 0;
                                    CsTrg.M_StartTrg = "";
                                    CsTrg.M_ngtrueflag = "";
                                    Cslogfun.WriteTomarkingLog("发送坐标和完成信号至plc:x偏移" + CsTrg.Ararr_zb[0] + "y偏移" + CsTrg.Ararr_zb[1] + Environment.NewLine);
                                }
                                if (CsTrg.M_ngfalsevalRest == "start")
                                {
                                    CsTrg.M_ngfalsevalRest = "";
                                    csGetBao.WriteToPlc("M_ngfalse", 0, CSReadPlc.DevName);//拍照失败信号复位
                                }
                            }
                            //喷码
                            if (CsMarking.M_MarkParameter == "NONet")//普通单喷码纸箱参数
                            {
                                csGetBao.WriteToPlc("PlcChangePo", 0, CSReadPlc.DevName);//plc的切单信号置0
                                //设置喷码纸箱参数
                                csGetBao.WriteToPlc("MarkPattern", (short)double.Parse(CsPakingData.M_DicPakingData["喷码模式"]), CSReadPlc.DevName);//喷码模式四行
                                csGetBao.WriteToPlc("ScQty", (short)double.Parse(CsPakingData.M_DicPakingData["箱数"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkLong", (short)double.Parse(CsPakingData.M_DicPakingData["箱子长"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkWideth", (short)double.Parse(CsPakingData.M_DicPakingData["箱子宽"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkHeight", (short)double.Parse(CsPakingData.M_DicPakingData["箱子高"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("CatornWeight", (short)double.Parse(CsPakingData.M_DicPakingData["重量"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("GoodWeight", (short)double.Parse(CsPakingData.M_DicPakingData["产品重量"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ColumnNunber", short.Parse(CsPakingData.M_DicPakingData["列数"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("LineNum", short.Parse(CsPakingData.M_DicPakingData["行数"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Qty", short.Parse(CsPakingData.M_DicPakingData["箱内件数"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Thick", short.Parse(CsPakingData.M_DicPakingData["产品厚度"]), CSReadPlc.DevName);
                                // csGetBao.WriteToPlc("Jianspeed", short.Parse(CsPakingData.M_DicPakingData["加减速度"]), CSReadPlc.DevName);
                                //  csGetBao.WriteToPlc("Grabspeed", short.Parse(CsPakingData.M_DicPakingData["抓取速度"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Updown_distance", short.Parse(CsPakingData.M_DicPakingData["距离底部"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Leftright_distance", short.Parse(CsPakingData.M_DicPakingData["距离左边"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsRotate", short.Parse(CsPakingData.M_DicPakingData["是否旋转"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("PartitionType", short.Parse(CsPakingData.M_DicPakingData["PartitionType"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("LabelingType", short.Parse(CsPakingData.M_DicPakingData["贴标类型"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("CatornType", short.Parse(CsPakingData.M_DicPakingData["纸箱大小"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Orientation", short.Parse(CsPakingData.M_DicPakingData["正反方向"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPalletizing", short.Parse(CsPakingData.M_DicPakingData["IsPalletizing"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsBottomPartition", short.Parse(CsPakingData.M_DicPakingData["IsBottomPartition"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsYaMaHaRun", short.Parse(CsPakingData.M_DicPakingData["IsYaMaHaRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsTopPartition", short.Parse(CsPakingData.M_DicPakingData["IsTopPartition"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsSealingRun", short.Parse(CsPakingData.M_DicPakingData["IsSealingRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPackingRun", short.Parse(CsPakingData.M_DicPakingData["IsPackingRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkingIsshieid", short.Parse(CsPakingData.M_DicPakingData["MarkingIsshieid"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("PrintIsshieid", short.Parse(CsPakingData.M_DicPakingData["PrintIsshieid"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("GAPIsPrintRun", short.Parse(CsPakingData.M_DicPakingData["GAPIsPrintRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPalletizingRun", short.Parse(CsPakingData.M_DicPakingData["IsPalletizingRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsScanRun", short.Parse(CsPakingData.M_DicPakingData["IsScanRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("HookDirection", short.Parse(CsPakingData.M_DicPakingData["HookDirection"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("PoTypeSignal", short.Parse(CsPakingData.M_DicPakingData["PoTypeSignal"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ReadTiaoMaType", short.Parse(CsPakingData.M_DicPakingData["读取条码模式"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ChangeOrderEnd", 1, CSReadPlc.DevName);//换单完成信号
                                CsMarking.M_MarkParameter = "";
                            }
                            else if (CsMarking.M_MarkParameter == "Net")//网单喷码纸箱参数
                            {
                                //csGetBao.WriteToPlc("PlcChangePo", 0, CSReadPlc.DevName);//plc的切单信号置0
                                //设置喷码纸箱参数
                                csGetBao.WriteToPlc("MarkPattern", 4, CSReadPlc.DevName);//喷码模式六行
                                csGetBao.WriteToPlc("ScQty", (short)double.Parse(CsPakingData.M_DicPakingData["箱数"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkLong", (short)double.Parse(CsPakingData.M_DicPakingData["箱子长"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkWideth", (short)double.Parse(CsPakingData.M_DicPakingData["箱子宽"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkHeight", (short)double.Parse(CsPakingData.M_DicPakingData["箱子高"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("CatornWeight", (short)double.Parse(CsPakingData.M_DicPakingData["重量"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("GoodWeight", (short)double.Parse(CsPakingData.M_DicPakingData["产品重量"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ColumnNunber", short.Parse(CsPakingData.M_DicPakingData["列数"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("LineNum", short.Parse(CsPakingData.M_DicPakingData["行数"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Qty", short.Parse(CsPakingData.M_DicPakingData["箱内件数"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Thick", short.Parse(CsPakingData.M_DicPakingData["产品厚度"]), CSReadPlc.DevName);
                                // csGetBao.WriteToPlc("Jianspeed", short.Parse(CsPakingData.M_DicPakingData["加减速度"]), CSReadPlc.DevName);
                                // csGetBao.WriteToPlc("Grabspeed", short.Parse(CsPakingData.M_DicPakingData["抓取速度"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Updown_distance", short.Parse(CsPakingData.M_DicPakingData["距离底部"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Leftright_distance", short.Parse(CsPakingData.M_DicPakingData["距离左边"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsRotate", short.Parse(CsPakingData.M_DicPakingData["是否旋转"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("PartitionType", short.Parse(CsPakingData.M_DicPakingData["PartitionType"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("LabelingType", short.Parse(CsPakingData.M_DicPakingData["贴标类型"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("CatornType", short.Parse(CsPakingData.M_DicPakingData["纸箱大小"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Orientation", short.Parse(CsPakingData.M_DicPakingData["正反方向"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPalletizing", short.Parse(CsPakingData.M_DicPakingData["IsPalletizing"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsBottomPartition", short.Parse(CsPakingData.M_DicPakingData["IsBottomPartition"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsYaMaHaRun", short.Parse(CsPakingData.M_DicPakingData["IsYaMaHaRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsTopPartition", short.Parse(CsPakingData.M_DicPakingData["IsTopPartition"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsSealingRun", short.Parse(CsPakingData.M_DicPakingData["IsSealingRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPackingRun", short.Parse(CsPakingData.M_DicPakingData["IsPackingRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkingIsshieid", short.Parse(CsPakingData.M_DicPakingData["MarkingIsshieid"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("PrintIsshieid", short.Parse(CsPakingData.M_DicPakingData["PrintIsshieid"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("GAPIsPrintRun", short.Parse(CsPakingData.M_DicPakingData["GAPIsPrintRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("GAPSideIsshieid", short.Parse(CsPakingData.M_DicPakingData["GAPSideIsshieid"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPalletizingRun", short.Parse(CsPakingData.M_DicPakingData["IsPalletizingRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsScanRun", short.Parse(CsPakingData.M_DicPakingData["IsScanRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("HookDirection", short.Parse(CsPakingData.M_DicPakingData["HookDirection"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("PoTypeSignal", short.Parse(CsPakingData.M_DicPakingData["PoTypeSignal"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ReadTiaoMaType", short.Parse(CsPakingData.M_DicPakingData["读取条码模式"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ChangeOrderEnd", 1, CSReadPlc.DevName);//换单完成信号
                                CsMarking.M_MarkParameter = "";
                            }
                            else if (CsMarking.M_MarkParameter == "ColorCode")//GU和优衣库喷码纸箱参数
                            {
                                csGetBao.WriteToPlc("PlcChangePo", 0, CSReadPlc.DevName);//plc的切单信号置
                                //设置喷码纸箱参数
                                csGetBao.WriteToPlc("MarkPattern", short.Parse(CsPakingData.M_DicMarking_colorcode["MarkPattern"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ScQty", short.Parse(CsPakingData.M_DicMarking_colorcode["Quantity"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkLong", (short)double.Parse(CsPakingData.M_DicMarking_colorcode["Long"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkWideth", (short)double.Parse(CsPakingData.M_DicMarking_colorcode["Width"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkHeight", (short)double.Parse(CsPakingData.M_DicMarking_colorcode["Heghit"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Updown_distance", (short)double.Parse(CsPakingData.M_DicMarking_colorcode["Updown_distance"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Leftright_distance", (short)double.Parse(CsPakingData.M_DicMarking_colorcode["Leftright_distance"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("CatornWeight", short.Parse(CsPakingData.M_DicMarking_colorcode["CartonWeight"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("GoodWeight", (short)double.Parse(CsPakingData.M_DicMarking_colorcode["GoodWeight"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ColumnNunber", short.Parse(CsPakingData.M_DicMarking_colorcode["ColumnNunber"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("LineNum", short.Parse(CsPakingData.M_DicMarking_colorcode["LineNum"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Qty", short.Parse(CsPakingData.M_DicMarking_colorcode["Layernum"]), CSReadPlc.DevName);
                                /*csGetBao.WriteToPlc("Qty", 24, CSReadPlc.DevName);*/
                                csGetBao.WriteToPlc("Thick", short.Parse(CsPakingData.M_DicMarking_colorcode["Thick"]), CSReadPlc.DevName);
                                /*csGetBao.WriteToPlc("Jianspeed", short.Parse(CsPakingData.M_DicMarking_colorcode["Jianspeed"]), CSReadPlc.DevName);*/
                                /*csGetBao.WriteToPlc("Grabspeed", short.Parse(CsPakingData.M_DicMarking_colorcode["Grabspeed"]), CSReadPlc.DevName);*/
                                csGetBao.WriteToPlc("CatornType", short.Parse(CsPakingData.M_DicMarking_colorcode["CatornType"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsRotate", short.Parse(CsPakingData.M_DicMarking_colorcode["IsRotate"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("PartitionType", short.Parse(CsPakingData.M_DicMarking_colorcode["PartitionType"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("LabelingType", short.Parse(CsPakingData.M_DicMarking_colorcode["LabelingType"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Orientation", short.Parse(CsPakingData.M_DicMarking_colorcode["Orientation"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPalletizing", short.Parse(CsPakingData.M_DicMarking_colorcode["IsPalletizing"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsBottomPartition", short.Parse(CsPakingData.M_DicMarking_colorcode["IsBottomPartition"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsYaMaHaRun", short.Parse(CsPakingData.M_DicMarking_colorcode["IsYaMaHaRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsTopPartition", short.Parse(CsPakingData.M_DicMarking_colorcode["IsTopPartition"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsSealingRun", short.Parse(CsPakingData.M_DicMarking_colorcode["IsSealingRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPackingRun", short.Parse(CsPakingData.M_DicMarking_colorcode["IsPackingRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkingIsshieid", short.Parse(CsPakingData.M_DicMarking_colorcode["MarkingIsshieid"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("PrintIsshieid", short.Parse(CsPakingData.M_DicMarking_colorcode["PrintIsshieid"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPalletizingRun", short.Parse(CsPakingData.M_DicMarking_colorcode["IsPalletizingRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsScanRun", short.Parse(CsPakingData.M_DicMarking_colorcode["IsScanRun"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("HookDirection", short.Parse(CsPakingData.M_DicMarking_colorcode["HookDirection"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("PoTypeSignal", short.Parse(CsPakingData.M_DicMarking_colorcode["PoTypeSignal"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ReadTiaoMaType", short.Parse(CsPakingData.M_DicMarking_colorcode["读取条码模式"]), CSReadPlc.DevName);
                                // csGetBao.WriteToPlc("IsRepeatCheck", short.Parse(CsPakingData.M_DicMarking_colorcode["IsRepeatCheck"]), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ChangeOrderEnd", 1, CSReadPlc.DevName);//换单完成信号
                                CsPakingData.M_DicMarking_colorcode.Clear();
                                CsMarking.M_MarkParameter = "";
                            }
                            if (CsMarking.YesLabelsignalFlag.Contains("start"))
                            {
                                CsMarking.YesLabelsignalFlag = "";
                                csGetBao.WriteToPlc("IsLabelsignal", 0, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("YesLabelsignal", 1, CSReadPlc.DevName);
                                Cslogfun.WriteTomarkingLog("复位标签判断信号并通知PLC");
                            }
                            if (CsMarking.M_toplcmoving == "toplcmoving")//判断是否喷码机设置内容成功
                            {
                                Cslogfun.WriteTomarkingLog("发送喷码请求：" + CsMarking.M_MarkCount + "启动信号至PLC寄存器(非读时写入)");
                                csGetBao.WriteToPlc("M_Markchange", 1, CSReadPlc.DevName);//发送信号告诉PLC已经切换模板了(PLC读取信息后要复位)
                                csGetBao.WriteToPlc("M_MarkCount", 0, CSReadPlc.DevName);
                                //Cslogfun.WriteTomarkingLog("喷码请求：" + CsMarking.M_MarkCount + "复位喷码请求信号至PLC寄存器(非读时写入)");
                                CsMarking.M_toplcmoving = "";
                            }
                            if (CsMarking.M_toplcmarkend == "markend")//判断是否喷印完成发信号到plc
                            {
                                //csGetBao.WriteToPlc("M_iMarkendnum", short.Parse(CsMarking.M_MarkCount.ToString().Trim()), CSReadPlc.DevName);//喷印完成发送完成内容信号给PLC寄存器(PLC读取信息后要复位) 
                                //csGetBao.WriteToPlc("M_Markend", 1, CSReadPlc.DevName);//喷印完成发送信号给PLC寄存器(PLC读取信息后要复位) 
                                csGetBao.WriteToPlc("M_Markendsignal", 0, CSReadPlc.DevName);//喷印完成发送信号给PLC寄存器(PLC读取信息后要复位) 
                                CsMarking.M_toplcmarkend = "";
                                Cslogfun.WriteTomarkingLog("复位喷码请求：" + CsMarking.M_Lastmarknum + "启动信号已发送至PLC寄存器");
                            }
                            if (CsMarking.M_endflag == "start")
                            {
                                csGetBao.WriteToPlc("M_Markend", 0, CSReadPlc.DevName);//喷印完成发送信号给PLC寄存器(PLC读取信息后要复位) 
                                CsMarking.M_endflag = "";
                            }
                            if (ManualMarkingSignalFlag.Contains("start"))
                            {
                                ManualMarkingSignalFlag = "";
                                csGetBao.WriteToPlc("ManualMarkingSignal", 0, CSReadPlc.DevName);
                            }
                            #endregion

                            #region 打印
                            if (CsPrint.M_printsignalreset == "start")
                            {
                                csGetBao.WriteToPlc("M_printsignal", 0, CSReadPlc.DevName);//复位
                                CsPrint.M_printsignal = 0;
                                CsPrint.M_printsignalreset = "";
                                //Cslogfun.WriteToprintgLog("打印标签信号复位寄存器ok");
                                // Cslogfun.WriteToprintgLog("打印标签信号已发送至寄存器");
                            }
                            if (CsPrint.M_Testprintsignalreset == "start")
                            {
                                csGetBao.WriteToPlc("M_Testprintsignal", 0, CSReadPlc.DevName);//复位
                                CsPrint.M_Testprintsignal = 0;
                                CsPrint.M_Testprintsignalreset = "";
                                //Cslogfun.WriteToprintgLog("打印标签信号复位寄存器ok");
                                // Cslogfun.WriteToprintgLog("打印标签测试信号已发送至寄存器");
                            }
                            if (PlcManualSignalFlag== "start")
                            {
                                PlcManualSignalFlag = "";
                                csGetBao.WriteToPlc("PlcManualSignal", 0, CSReadPlc.DevName);//复位
                            }
                            //串口打印
                            if (CsPrint. M_serialsignalreset.Contains("start"))
                            {
                                
                                CsPrint.M_serialsignalreset = "";
                                csGetBao.WriteToPlc("M_serialsignal", 0, CSReadPlc.DevName);
                            }
                            #endregion
                            #region 装箱规格参数
                            if (ScanPackingForm.pakingdata.Trim() == "start")
                            {
                                csGetBao.WriteToPlc("MarkLong", ScanPackingForm.Long, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkWideth", ScanPackingForm.Wide, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkHeight", ScanPackingForm.Height, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Orientation", ScanPackingForm.Orientation, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ColumnNunber", ScanPackingForm.column, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Qty", ScanPackingForm.Layernum, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Thick", ScanPackingForm.thick, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ScQty", ScanPackingForm.qty, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("Layernum", ScanPackingForm.Layernum, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("GoodWeight", ScanPackingForm.GoodWeight, CSReadPlc.DevName);
                                //csGetBao.WriteToPlc("Jianspeed", ScanPackingForm.Jianspeed, CSReadPlc.DevName);
                                // csGetBao.WriteToPlc("Grabspeed", ScanPackingForm.Grabspeed, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsRotate", ScanPackingForm.IsRotate, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPalletizing", ScanPackingForm.IsPalletizing, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsBottomPartition", ScanPackingForm.IsBottomPartition, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsYaMaHaRun", ScanPackingForm.IsYaMaHaRun, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsTopPartition", ScanPackingForm.IsTopPartition, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsSealingRun", ScanPackingForm.IsSealingRun, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPackingRun", ScanPackingForm.IsPackingRun, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkingIsshieid", ScanPackingForm.MarkingIsshieid, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("PrintIsshieid", ScanPackingForm.PrintIsshieid, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsPalletizingRun", ScanPackingForm.IsPalletizingRun, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("IsScanRun", ScanPackingForm.IsScanRun, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("HookDirection", ScanPackingForm.HookDirection, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("PartitionType", ScanPackingForm.M_PartitionType, CSReadPlc.DevName);
                                csGetBao.WriteToPlc("ChangeOrderEnd", 1, CSReadPlc.DevName);//换单完成信号
                                ScanPackingForm.pakingdata = "";
                            }
                            #endregion
                            //码垛
                            if (Palletizingflag.Contains("start"))
                            {
                                csGetBao.WriteToPlc("PalletizingCount", 0, CSReadPlc.DevName);
                                //csGetBao.WriteToPlc("PalletizingLineType", 0, CSReadPlc.DevName);
                                Palletizingflag = "";
                               // Cslogfun.WriteToPalletizingLog("复位码垛计数寄存器一次" + Environment.NewLine);
                            }
                            if (M_stcPalletizingData.strWsizeFlag == "start")
                            {
                                csGetBao.WriteToPlc("MarkLong", (short)double.Parse(M_stcPalletizingData.strCatornLong), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkWideth", (short)double.Parse(M_stcPalletizingData.strCatornWidth), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("MarkHeight", (short)double.Parse(M_stcPalletizingData.strCatornHeight), CSReadPlc.DevName);
                                csGetBao.WriteToPlc("WritePalletizingflag", 1, CSReadPlc.DevName);
                                M_stcPalletizingData.strWsizeFlag = "";
                               // Cslogfun.WriteToPalletizingLog("计算垛板数量写入纸箱尺寸");
                            }
                            if (M_stcPalletizingData.strResetFlag == "start")
                            {
                                csGetBao.WriteToPlc("ReadPalletizingflag", 0, CSReadPlc.DevName);
                            }
                            //开机启动信号写入
                            if (setupsighg == 1)
                            {
                                csGetBao.WriteToPlc("SetupWritePlc", 1, CSReadPlc.DevName);
                                Thread.Sleep(50);
                                csGetBao.WriteToPlc("SetupReadPlc", 0, CSReadPlc.DevName);
                               // Cslogfun.WriteToLog("开机启动信号写入PLC一次");
                            }
                            //无单信号发送
                            if (NoOrderSignal=="start" )
                            {
                                Cslogfun.WriteToLog("发送一次无单信号至plc");
                                csGetBao.WriteToPlc("SetupWritePlc", 2, CSReadPlc.DevName);
                                NoOrderSignal = "";
                            }
                        }
                    }
                }
                catch (Exception err)
                {
                    Cslogfun.WriteToLog(err);
                    throw new Exception("提示：" + err.Message);
                }

            }

        }
        void ReadNumber()
        {
            while (true)
            {
                Thread.Sleep(10);
                try
                {
                    //更新进箱数量
                    lock (CSReadPlc.lockScan)
                    {
                        GetCurrentPakingNumber();
                    }
                    //更新喷码
                    if ((CSReadPlc.DevName.Trim() == "plc1" &&CsMarking.M_PackingType == "1") || (CSReadPlc.DevName.Trim() == "plc2" && CsMarking.M_PackingType == "2"))
                    {
                        if (M_stcScanData.MarkingIsshieid.Trim().Contains("不运行") && ManualMarkingSignal == 1 && string.IsNullOrEmpty(ManualMarkingSignalFlag))
                        {
                            CsMarking.ManualUpdatePackingPage();
                            ManualMarkingSignalFlag = "start";
                            TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                            while (ManualMarkingSignal != 0)
                            {
                                TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                if (ts.Seconds > 5)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    //更新码垛计数
                    if ( !string.IsNullOrEmpty(CsPalletizing.PalletizingCount.ToString().Trim()) && CsPalletizing.PalletizingCount.ToString().Trim() != "0" && string.IsNullOrEmpty(Palletizingflag))
                    {
                        if (string.IsNullOrEmpty(CSReadPlc.EditionFlag))
                        {
                            if (CsPakingData.GetPalletizingtype("potype").Contains("YYK") || CsPakingData.GetPalletizingtype("potype").Contains("GU") || CsPakingData.GetPalletizingtype("potype").Contains("UNIQLO"))
                            {
                                lock (CSReadPlc.lockScan)
                                {
                                    CsPalletizing.YYKGUPalletizingLineUpGoOnce();
                                    Palletizingflag = "start";
                                    TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                    while (CsPalletizing.PalletizingCount.ToString().Trim() != "0")
                                    {
                                        TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                        TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                        if (ts.Seconds > 5)
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                            else if (CsPakingData.GetPalletizingtype("potype").Contains("GAP"))
                            {
                                lock (CSReadPlc.lockScan)
                                {
                                    CsPalletizing.GAPPalletizingLineUpGoOnce();
                                    Palletizingflag = "start";
                                    TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                    while (CsPalletizing.PalletizingCount.ToString().Trim() != "0")
                                    {
                                        TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                        TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                        if (ts.Seconds > 5)
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else 
                        {
                            #region 关工对接
                            if (CsPublicVariablies.PrintData.PoType.Contains("UNIQLO") || CsPublicVariablies.PrintData.PoType.Contains("GU"))
                            {
                                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE a SET finishPalletizingnumber=isnull(finishPalletizingnumber,0)+1 FROM YOUYIKUDO AS a WHERE EXISTS(SELECT*FROM dbo.YOUYIKUDO WHERE  guid='{0}' AND Order_No=a.Order_No AND Warehouse=a.Warehouse AND Set_Code=a.Set_Code)", CsPublicVariablies.PrintData.Po_guid));
                                Palletizingflag = "start";
                                TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                while (CsPalletizing.PalletizingCount.ToString().Trim() != "0")
                                {
                                    TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                    TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                    if (ts.Seconds > 5)
                                    {
                                        break;
                                    }
                                }
                            }
                            else if (CsPublicVariablies.PrintData.PoType.Contains("GAP"))
                            {
                                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET finishPalletizingnumber=isnull(finishPalletizingnumber,0)+1 WHERE guid='{0}'", CsPublicVariablies.PrintData.Po_guid));
                                Palletizingflag = "start";
                                TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                                while (CsPalletizing.PalletizingCount.ToString().Trim() != "0")
                                {
                                    TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                    TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                    if (ts.Seconds > 5)
                                    {
                                        break;
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                    if (CsRWPlc.PlcManualSignal == 1)
                    {
                        lock (CSReadPlc.lockScan)
                        {
                            Cslogfun.WriteToLog("喷码后段打手动，清理标签队列一次");
                            CsRWPlc.PlcManualSignalFlag = "start";
                            CsPrint.M_printsignalreset = "";
                            ManualMarkingSignalFlag = "";
                            Palletizingflag = "";
                            CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.PRINTLineuUp "));
                            CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.PalletizingtypeTable"));
                            CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.tPrintData"));
                            CsInitialization.UpdatePrintLine();
                            //CsInitialization.UpdateGAPLineUp();
                            //CsInitialization.UpdateGAPPalletizingLineUp();
                            //CsInitialization.UpdateYYKGUPalletizingLineUp();
                            CsInitialization.UpdatePalletizingtype();
                            TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                            while (CsRWPlc.PlcManualSignal == 1)
                            {
                                TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                                TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                                if (ts.Seconds > 5)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    //拍照失败记录日志
                    if (CsTrg.M_ngfalseval == 1 && string.IsNullOrEmpty(CsTrg.M_ngfalsevalRest))
                    {
                        try
                        {
                            Cslogfun.WriteToCartornLog("订单号：" + M_stcScanData.strpo +
                              "，箱号" + (int.Parse(M_stcScanData.stridentification == "" ? "0" : M_stcScanData.stridentification) + M_stcScanData.FinishmarknumberTotal + int.Parse(M_stcScanData.FinishPackingNum) - 1).ToString());
                        }
                        catch (Exception)
                        {

                        }
                           

                        CsTrg.M_ngfalsevalRest = "end";
                    }
                    //调试用
                    //else if (M_stcPalletizingData.PalletizingTest == "start")
                    //{
                    //    M_stcPalletizingData.PalletizingTest = "";
                    //    CsPalletizing.YYKGUPalletizingLineUpGoOnce();
                    //    Palletizingflag = "start";
                    //    TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                    //    while (CsPalletizing.PalletizingCount.ToString().Trim() != "0")
                    //    {
                    //        TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                    //        TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                    //        if (ts.Seconds > 5)
                    //        {
                    //            break;
                    //        }
                    //    }
                    //}
                }
                catch (Exception ex)
                {
                    Cslogfun.WriteToLog(ex);
                    throw new Exception("提示：" + ex);
                }
            }
        }
        /// <summary>
        /// 更新当前装箱订单的进箱数量
        /// </summary>
        public void GetCurrentPakingNumber()
        {
            try
            {
                if (PakingCount.Contains("1") && string.IsNullOrEmpty(PakingRest))
                {
                    bool b_Ispo = Chekin.IsExistPo();
                    if (!b_Ispo )
                    {
                        Thread.Sleep(10000);
                        NoOrderSignal = "start";
                        PakingRest = "start";
                        return;
                       // MessageBox.Show("装箱无订单数据");
                    }
                    PakingRest = "start";
                    //确认复位寄存器
                    TimeSpan timeSpanstart = new TimeSpan(DateTime.Now.Ticks);
                    while (PakingCount != "0")
                    {
                        TimeSpan timeSpanstop = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan ts = timeSpanstart.Subtract(timeSpanstop).Duration();
                        if (ts.Seconds > 5)
                        {
                            break;
                        }
                    }
                    #region 执行计数
                    string strsql = "";
                    //优衣库/GU
                    if (M_stcScanData.potype.Contains("UNIQLO") || M_stcScanData.potype.Contains("GU"))
                    {
                        if (M_stcScanData.IsEndPacking)
                        {
                            //MessageBox.Show("装箱数量不允许超订单设置数量请切换订单");
                            return;
                        }
                        if (!string.IsNullOrEmpty(M_stcScanData.strpo))
                        {
                            //更新进箱数量
                           CsFormShow.GoSqlUpdateInsert( string.Format("UPDATE dbo.YYKMarkingLineUp SET FinishPackingNum=isnull(FinishPackingNum,0)+1 WHERE Order_No='{0}'AND Warehouse='{1}'  AND Set_Code='{2}'", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim(), M_stcScanData.Set_Code));
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YOUYIKUDO SET packingtotal=isnull(packingtotal,0)+1 WHERE Order_No='{0}' AND Warehouse='{1}' AND  Set_Code='{2}'", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim(), M_stcScanData.Set_Code));

                            M_stcScanData.FinishPackingNum = (int.Parse(M_stcScanData.FinishPackingNum) + 1).ToString();
                            if (M_stcScanData.FinishPackingNum=="1")
                            {
                                Cslogfun.WriteToCartornLog("" + Environment.NewLine);
                                Cslogfun.WriteToCartornLog(string.Format("订单号码:{0},Set_Code:{1},Warehouse:{2},本次设置数量:{3}", M_stcScanData.strpo, M_stcScanData.Set_Code, M_stcScanData.stridentification.Trim().Substring(0, 5), M_stcScanData.CurrentPackingNum));
                                Cslogfun.WriteToCartornLog("po:" + M_stcScanData.strpo + ",Set_Code:" + M_stcScanData.Set_Code + ",Warehouse:" + M_stcScanData.stridentification.Trim().Substring(0, 5) + ",装箱" + M_stcScanData.FinishPackingNum + "次:");
                            }
                            else
                            {
                                Cslogfun.WriteToCartornLog("po:" + M_stcScanData.strpo + ",Set_Code:" + M_stcScanData.Set_Code + ",Warehouse:" + M_stcScanData.stridentification.Trim().Substring(0, 5) + ",装箱" + M_stcScanData.FinishPackingNum + "次:");
                            }
                            //更新历史记录表
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKGUHistoricRcords SET EndTime ='{0}',Yield=isnull(Yield,0)+1 WHERE Order_No='{1}' AND Set_Code='{2}' AND Warehouse='{3}'", DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss") , M_stcScanData.strpo, M_stcScanData.Set_Code, M_stcScanData.stridentification));
                            //判断是否进箱完成
                            string sqlstr = "";
                            sqlstr = string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}' AND CurrentPackingNum=isnull(FinishPackingNum,0) ", M_stcScanData.strpo, M_stcScanData.strPoTiaoMa, M_stcScanData.stridentification, M_stcScanData.Set_Code);
                            DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                            if (dt.Rows.Count > 0)
                            {
                                M_stcScanData.IsEndPacking = true;
                                sqlstr = string.Format("UPDATE dbo.YYKMarkingLineUp SET IsEndPacking=1,IsEndScan=1 WHERE Order_No='{0}'AND SKU_Code='{1}'AND Warehouse='{2}' ", M_stcScanData.strpo, M_stcScanData.strPoTiaoMa, M_stcScanData.stridentification);
                                CsFormShow.GoSqlUpdateInsert(sqlstr);
                                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYK_GU_Current SET IsEndScan=1 WHERE po='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", M_stcScanData.strpo, M_stcScanData.stridentification, M_stcScanData.Set_Code));
                                CsGetData.UpdataScanLineUp();//更新扫码队列
                              //  MessageBox.Show("装箱数量已达到订单设置数量请切换订单");
                                return;
                            }
                        }
                    }
                    //GAP
                    else 
                    {
                        if (M_stcScanData.IsEndPacking)
                        {

                            //MessageBox.Show("装箱数量不允许超订单设置数量请切换订单");
                            return;
                        }
                        if (!string.IsNullOrEmpty(M_stcScanData.strpo))
                        {
                            //进箱数量更新
                            CsFormShow.GoSqlUpdateInsert( string.Format("UPDATE dbo.MarkingLineUp SET FinishPackingNum=isnull(FinishPackingNum,0)+1 WHERE  订单号码='{0}' AND 从='{1}'", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim()));
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET packingtotal =ISNULL(packingtotal,0)+1 WHERE 订单号码='{0}' AND 从='{1}'", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim()));
                            M_stcScanData.FinishPackingNum = (int.Parse(M_stcScanData.FinishPackingNum) + 1).ToString();
                            if (M_stcScanData.FinishPackingNum == "1")
                            {
                                Cslogfun.WriteToCartornLog("" + Environment.NewLine);
                                Cslogfun.WriteToCartornLog(string.Format("订单号码:{0},从:{1},起始箱号:{2},本次设置数量:{3}", M_stcScanData.strpo, M_stcScanData.stridentification, int.Parse(M_stcScanData.stridentification) + M_stcScanData.FinishmarknumberTotal, M_stcScanData.CurrentPackingNum));
                                Cslogfun.WriteToCartornLog("po:" + M_stcScanData.strpo + ",从:" + M_stcScanData.stridentification + ",装箱" + M_stcScanData.FinishPackingNum + "次:");
                            }
                            else
                            {
                                Cslogfun.WriteToCartornLog("po:" + M_stcScanData.strpo + ",从:" + M_stcScanData.stridentification + ",装箱" + M_stcScanData.FinishPackingNum + "次:");
                            }
                            //更新历史记录表
                            string gap_guid = CsFormShow.SqlGetVal(string.Format("SELECT*FROM dbo.PACKING WHERE 订单号码='{0}' AND 从='{1}'", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim()), "guid");
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.HistoricRcords SET EndTime ='{0}',Yield=isnull(Yield,0)+1 WHERE po_guid='{1}'", DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss") , gap_guid));
                            //判断是否进箱完成
                            string sqlstr = "";
                            sqlstr = string.Format("SELECT * FROM dbo.MarkingLineUp WHERE  订单号码='{0}' AND 从='{1}' AND CurrentPackingNum=isnull(FinishPackingNum,0)", M_stcScanData.strpo, M_stcScanData.stridentification);
                            DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                            if (dt.Rows.Count > 0)
                            {
                                M_stcScanData.IsEndPacking = true;
                                sqlstr = string.Format("UPDATE dbo.MarkingLineUp SET IsEndPacking=1,IsEndScan=1 WHERE 订单号码='{0}'AND 从='{1}'", M_stcScanData.strpo, M_stcScanData.stridentification);
                               CsFormShow.GoSqlUpdateInsert(sqlstr);
                                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.GAP_Current SET IsEndScan=1 WHERE  订单号码='{0}' AND  从='{1}' ", M_stcScanData.strpo, M_stcScanData.stridentification));
                                CsGetData.UpdataScanLineUp();//更新扫码队列
                                //if (M_stcScanData.MarkingIsshieid.Trim().Contains("不运行"))
                                //{
                                //    CsMarking.ManualUpdatePackingPage();
                                //}
                              // MessageBox.Show("装箱数量已达到订单设置数量请切换订单");
                                return;
                            }
                        }
                    }
                    #endregion
                    //if (M_stcScanData.MarkingIsshieid.Trim() == "不运行")
                    //{
                    //    CsMarking.ManualUpdatePackingPage();
                    //}
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
    }
}
