﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
namespace WindowsForms01
{
    public class CsFormShow
    {
        SetQtyForm SetQtyform = null;
        /// <summary>
        /// 创建窗体实例
        /// </summary>
        /// <param name="formshow">要显示的窗体的实例名称</param>
        /// <param name="MDIForm">父窗体名称</param>
        /// <param name="typeform">窗体类型字符串</param>
        public static Form FormShow(Form formshow, Form MDIForm, string typeform)
        {
            try
            {
                System.Type t = System.Type.GetType(typeform);//获取窗体类型
                if (formshow == null || formshow.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                {
                    formshow = (Form)Activator.CreateInstance(t, null);//相当于创建窗体实例insertuserform1 = new insertuserForm();
                    if (MDIForm != null)
                    {
                        formshow.Owner = MDIForm;
                    }
                    formshow.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    formshow.Show();
                }
                else
                {
                    formshow.Show();
                    formshow.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    formshow.BringToFront();
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

            return formshow;
        }
        /// <summary>
        /// 创建对话框窗体实例
        /// </summary>
        /// <param name="formshow">要显示的窗体的实例名称</param>
        /// <param name="MDIForm">父窗体名称</param>
        /// <param name="typeform">窗体类型字符串</param>
        public static void FormShowDialog(Form formshow, Form MDIForm, string typeform)
        {
            try
            {
                System.Type t = System.Type.GetType(typeform);//获取窗体类型
                if (formshow == null || formshow.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                {
                    formshow = (Form)Activator.CreateInstance(t, null);//相当于创建窗体实例insertuserform1 = new insertuserForm();
                    if (MDIForm != null)
                    {
                        formshow.Owner = MDIForm;
                    }
                    formshow.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    formshow.ShowDialog();
                }
                else
                {
                    formshow.Show();
                    formshow.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    formshow.BringToFront();
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }

        }
        public static bool DvgRowSetcurrent(DataGridView dvg)
        {
            try
            {
                if (dvg.Rows.Count > 0)
                {
                    int index_val = dvg.SelectedRows[0].Index;
                    if (dvg.Rows.Count == index_val || dvg.SelectedRows.Count > 1)
                    {
                        MessageBox.Show("请选择正确的数据");
                        return false;
                    }
                    for (int i = 0; i < dvg.Rows.Count; i++)
                    {
                       dvg.Rows[i].Cells["当前选择"].Value = false;
                    }
                    if (dvg.SelectedRows.Count == 1)
                    {
                        //dataGridView2.Rows[index_val].Cells["当前选择"].Value =true;
                       dvg.SelectedRows[0].Cells["当前选择"].Value = true;
                    }
                }

            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
            return true;
        }
        /// <summary>
        /// 指定当前行
        /// </summary>
        /// <param name="dvg"></param>
        public static bool DvgSetcurrentRow(DataGridView dvg, string guid_)
        {
            try
            {
                if (dvg.Rows.Count > 0)
                {
                    for (int i = 0; i < dvg.Rows.Count; i++)
                    {
                        dvg.Rows[i].Cells["当前选择"].Value = false;
                        if (dvg.Rows[i].Cells["guid"].Value.ToString().Trim()== guid_.Trim())
                        {
                            dvg.Rows[i].Cells["当前选择"].Value = true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
            return true;
        }
        //public static int DvgSetcurrent(DataGridView dvg)
        //{
        //    try
        //    {
        //        A.BLL.newsContent content1 = new A.BLL.newsContent();
        //        if (dvg.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dvg.Rows.Count; i++)
        //            {
        //                for (int j = 0; j < dvg.Columns.Count; j++)
        //                {
        //                    if (dvg.Columns[j].Name == "Sku号码")
        //                    {
        //                        string po = dvg.Rows[i].Cells["订单号码"].Value.ToString().Trim();
        //                        string identification = dvg.Rows[i].Cells["从"].Value.ToString().Trim();
        //                        DataSet ds = content1.Select_nothing(string.Format("SELECT*FROM dbo.TiaoMa WHERE po='{0}'AND identification='{1}'", po, identification));
        //                        if (ds.Tables[0].Rows.Count > 0)
        //                        {
        //                            //dvg.Rows[i].Cells["当前选择"].Value = true;
        //                            return i;
        //                        }
        //                    }
        //                    else if (dvg.Columns[j].Name == "SKU_Code")
        //                    {
        //                        string po = dvg.Rows[i].Cells["Order_No"].Value.ToString().Trim();
        //                        string identification = dvg.Rows[i].Cells["Color_Code"].Value.ToString().Trim();
        //                        DataSet ds = content1.Select_nothing(string.Format("SELECT*FROM dbo.TiaoMa WHERE po='{0}'AND identification='{1}'", po, identification));
        //                        if (ds.Tables[0].Rows.Count > 0)
        //                        {
        //                            //dvg.Rows[i].Cells["当前选择"].Value = true;
        //                            return i;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Cslogfun.WriteToLog(ex);
        //        throw new Exception("提示：" + ex);
        //    }
        //    return 0;
        //}
        /// <summary>
        /// 设置DataGridView显示参数
        /// </summary>
        /// <param name="dvg_"></param>
        /// <param name="headHeight_"></param>
        /// <param name="rowHeight_"></param>
        /// <param name="ReadOnly_"></param>
        /// <param name="color_"></param>
        public static void SetDvgRowHeight(DataGridView dvg_, int headHeight_, int rowHeight_, bool ReadOnly_, Color color_,Form form_)
        {
            dvg_.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvg_.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            //数据行字体样式
            dvg_.RowsDefaultCellStyle.Font = new Font("宋体", 12, FontStyle.Regular);
            //选择行颜色设置
           // dvg_.DefaultCellStyle.SelectionBackColor = Color.SandyBrown;
            //标题列字体样式
            dvg_.ColumnHeadersDefaultCellStyle.Font = new Font("宋体", 15, FontStyle.Regular);
            dvg_.ReadOnly = true;
            dvg_.ColumnHeadersHeight = headHeight_;
            dvg_.RowTemplate.Height = rowHeight_;
            dvg_.RowsDefaultCellStyle.BackColor = color_;
            dvg_.BackgroundColor = Color.DarkGray;
            //不显示出dataGridView1的最后一行空白   
            dvg_.AllowUserToAddRows = false;
            form_.MinimizeBox = false;
            //防止单击列标题触发排序
            for (int i = 0; i < dvg_.Columns.Count; i++)
            {
                dvg_.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }
        /// <summary>
        /// 设置选定行
        /// </summary>
        /// <param name="dvg"></param>
        public static void SetSelectRow(DataGridView dvg)
        {
            try
            {
                if (dvg.Rows.Count > 0)
                {
                    for (int i = 0; i < dvg.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(dvg.Rows[i].Cells["当前选择"].Value.ToString()))
                        {
                            // string bo = dvg.Rows[i].Cells["当前选择"].Value.ToString();
                            if ((bool)dvg.Rows[i].Cells["当前选择"].Value)
                            {
                                dvg.ClearSelection();
                                dvg.Rows[i].Selected = true;
                                return;
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("提示：没有可执行的数据");
                    return;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 获取选择行的索引
        /// </summary>
        /// <param name="dvg"></param>
        public static void DvgGetcurrentIndex(DataGridView dvg, out bool Isindex, out int Index_)
        {
            Index_ = 0;
            Isindex = false;
            try
            {
                if (dvg.SelectedRows.Count > 0)
                {
                    if (dvg.SelectedRows.Count != 1)
                    {
                        MessageBox.Show("请选择正确的数据");
                        Isindex = false;
                    }
                    if (dvg.SelectedRows.Count == 1)
                    {
                        //dataGridView2.Rows[index_val].Cells["当前选择"].Value =true;
                        //dvg.SelectedRows[0].Cells["当前选择"].Value = true;
                        Index_ = dvg.SelectedRows[0].Index;
                        Isindex = true;
                    }
                }
                else
                {
                    Isindex = false;
                    MessageBox.Show("没有选择有效行数据！");
                }

            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
        }
        public static bool IsExistenceInSql(string TableName, string ColunmName_,string ColunmVal_)
        {
            try
            {
                A.BLL.newsContent content1 = new A.BLL.newsContent();
                string sqlstr = string.Format("SELECT*FROM  {0} WHERE {1}='{2}'", TableName, ColunmName_, ColunmVal_);
                DataSet ds = content1.Select_nothing(sqlstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        public static void  GoSqlUpdateInsert(string sqlstr_)
        {
            try
            {
                A.BLL.newsContent content1 = new A.BLL.newsContent();
                content1.Select_nothing(sqlstr_);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlstr_"></param>
        /// <returns></returns>
        public static DataTable GoSqlSelect(string sqlstr_)
        {
            try
            {
                A.BLL.newsContent content1 = new A.BLL.newsContent();
                DataTable dt = content1.Select_nothing(sqlstr_).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    DataTable dt_ = new DataTable();
                    return dt_;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        public static int GoSqlUpdateInsertDelete(string sqlstr_)
        {
            try
            {
                A.BLL.newsContent content1 = new A.BLL.newsContent();
                int count_v = content1.UpdateInsetDelete(sqlstr_);
                return count_v;
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// 无参数存储过程
        /// </summary>
        /// <param name="ProcName_"></param>
        /// <returns></returns>
        public static DataTable GoProc_NOParameter(string ProcName_)
        {
            try
            {
                A.BLL.newsContent content1 = new A.BLL.newsContent();
                DataSet ds = content1.GoProc_NOParameter(ProcName_);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0];
                }
                else
                {
                    DataTable dt_ = new DataTable();
                    return dt_;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        public static DataTable GoProc_Parameter(SqlParameter[] parameters_, string PmName_)
        {
            try
            {
                A.BLL.newsContent content1 = new A.BLL.newsContent();
                DataSet ds = content1.Go_PROC_Parameter(parameters_, PmName_);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0];
                }
                else
                {
                    DataTable dt_ = new DataTable();
                    return dt_;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        public static void GoProc_ParameterNoSelect(SqlParameter[] parameters_, string PmName_)
        {
            try
            {
                A.BLL.newsContent content1 = new A.BLL.newsContent();
                content1.Go_PROC_ParameterNoSelect(parameters_, PmName_);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// /获取表中某列值
        /// </summary>
        /// <param name="sqlstr_"></param>
        /// <param name="ColumnName_"></param>
        /// <returns></returns>
        public static string SqlGetVal(string sqlstr_, string ColumnName_)
        {
            try
            {
              
                DataTable dt =GoSqlSelect(sqlstr_);
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0][ColumnName_].ToString().Trim();
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        public static int GoSqlSelectCount(string sqlstr_)
        {
            try
            {
                A.BLL.newsContent content1 = new A.BLL.newsContent();
                DataTable dt = content1.Select_nothing(sqlstr_).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows.Count;
                }
                else
                {   
                    return 0;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// 装箱数量设置窗口实例化
        /// </summary>
        /// <param name="index_"></param>
        public  void SetPackingQty(int index_, DataGridView dvg_,Form MDIForm_,string Qty_ColumnName_,string Palletizing_Unit_)
        {
            if (SetQtyform == null || SetQtyform.IsDisposed)
            {
                string GAPTiaoMa_v = "";
                int packingtotal_v =0;
                int total=0;
                int qty = 0;
                string potype = "";
                if ((dvg_.Columns.Contains("TiaoMaVal")&& MDIForm_.Name != "SchedulingTableForm" && dvg_.Name == "ProducedGV") || (MDIForm_.Name== "SchedulingTableForm" && dvg_.Name == "ProducedGV" && dvg_.Rows[index_].Cells["potype"].Value.ToString().Contains("GAP")) || (MDIForm_.Name == "SchedulingTableForm" && dvg_.Name == "PlandGV" && dvg_.Rows[index_].Cells["potype"].Value.ToString().Contains("GAP")))
                {
                    if (MDIForm_.Name == "SchedulingTableForm"&& dvg_.Name== "ProducedGV")
                    {
                        DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", dvg_.Rows[index_].Cells["po_guid"].Value.ToString()));
                        packingtotal_v= int.Parse(dt.Rows[0]["packingtotal"].ToString().Trim());
                        total = int.Parse(dt.Rows[0]["箱数"].ToString().Trim());
                        GAPTiaoMa_v = dvg_.Rows[index_].Cells["TiaoMaVal"].Value.ToString().Trim();
                        qty = total - packingtotal_v;
                    }
                    else if (MDIForm_.Name == "SchedulingTableForm" && dvg_.Name == "PlandGV")
                    {
                        //DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.*,(a.数量/a.箱数) AS '件数/箱',ISNULL(finishmarknumber,0)*(a.数量/a.箱数) AS '已完成件数', b.库存数量,(ISNULL(b.库存数量,0)/(a.数量/a.箱数)) AS '库存箱数',((a.箱数- ISNULL(finishmarknumber,0))*(a.数量/a.箱数)) AS '剩余需求件数',((a.箱数- ISNULL(finishmarknumber,0))) AS '剩余需求箱数'  FROM dbo.PACKING AS a INNER JOIN dbo.ERPEXCH AS b ON a.TiaoMaVal=b.条形码 WHERE a.guid='{0}'", dvg_.Rows[index_].Cells["guid"].Value.ToString()));
                        GAPTiaoMa_v = dvg_.Rows[index_].Cells["条形码"].Value.ToString();
                        int OrderQty =int.Parse( dvg_.Rows[index_].Cells["剩余箱数"].Value.ToString());
                        int ckqty = int.Parse(dvg_.Rows[index_].Cells["实际可装箱数"].Value.ToString());
                        if (OrderQty> ckqty)
                        {
                            qty = int.Parse(dvg_.Rows[index_].Cells["实际可装箱数"].Value.ToString());
                        }
                        else
                        {
                            qty = int.Parse(dvg_.Rows[index_].Cells["剩余箱数"].Value.ToString());
                            if (qty < 0)
                            {
                                qty = 0;
                            }
                        }
                    }
                    else
                    {
                        packingtotal_v = int.Parse(dvg_.Rows[index_].Cells["packingtotal"].Value.ToString().Trim());
                        total = int.Parse(dvg_.Rows[index_].Cells[Qty_ColumnName_].Value.ToString().Trim());
                        GAPTiaoMa_v = dvg_.Rows[index_].Cells["TiaoMaVal"].Value.ToString().Trim();
                        qty = total - packingtotal_v;
                    }
                    SetQtyform = new SetQtyForm(qty.ToString(), MDIForm_, Palletizing_Unit_, GAPTiaoMa_v);
                    SetQtyform.Owner = MDIForm_;
                    SetQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    SetQtyform.ShowDialog();
                }
                else if(MDIForm_.Name == "YouyikuForm"||(MDIForm_.Name == "SchedulingTableForm" && dvg_.Name == "ProducedGV" && dvg_.Rows[index_].Cells["potype"].Value.ToString().Contains("UNIQLO")) || (MDIForm_.Name == "SchedulingTableForm" && dvg_.Name == "PlandGV" && dvg_.Rows[index_].Cells["品牌"].Value.ToString().Contains("UNIQLO")))//
                {
                    if (MDIForm_.Name == "SchedulingTableForm" && dvg_.Name == "ProducedGV")
                    {
                        DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YOUYIKUDO WHERE guid='{0}'", dvg_.Rows[index_].Cells["po_guid"].Value.ToString()));
                        packingtotal_v = int.Parse(dt.Rows[0]["packingtotal"].ToString().Trim());
                        total = int.Parse(dt.Rows[0]["Quantity"].ToString().Trim());
                        qty = total - packingtotal_v;
                    }
                    else if (MDIForm_.Name == "SchedulingTableForm" && dvg_.Name == "PlandGV")
                    {
                        //DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.*,Qty_per_Set AS '件数/箱',ISNULL(finishmarknumber,0)*Qty_per_Set AS '已完成件数', b.库存数量,(ISNULL(b.库存数量,0)/Qty_per_Set) AS '库存箱数',((a.Quantity- ISNULL(finishmarknumber,0))*Qty_per_Set) AS '剩余需求件数',((a.Quantity- ISNULL(finishmarknumber,0))) AS '剩余需求箱数'  FROM dbo.YOUYIKUDO AS a INNER JOIN dbo.ERPEXCH AS b ON a.SKU_Code=b.条形码 WHERE a.guid='{0}'", dvg_.Rows[index_].Cells["guid"].Value.ToString()));
                        int OrderQty = int.Parse(dvg_.Rows[index_].Cells["剩余箱数"].Value.ToString());
                        int ckqty = int.Parse(dvg_.Rows[index_].Cells["实际可装箱数"].Value.ToString());
                        if (OrderQty > ckqty)
                        {
                            qty = int.Parse(dvg_.Rows[index_].Cells["实际可装箱数"].Value.ToString());
                        }
                        else
                        {
                            qty = int.Parse(dvg_.Rows[index_].Cells["剩余箱数"].Value.ToString());
                            if (qty<0)
                            {
                                qty = 0;
                            }
                        }
                    }
                    else
                    {
                        packingtotal_v = int.Parse(dvg_.Rows[index_].Cells["packingtotal"].Value.ToString().Trim());
                        total = int.Parse(dvg_.Rows[index_].Cells[Qty_ColumnName_].Value.ToString().Trim());
                        qty = total - packingtotal_v;
                    }
                    SetQtyform = new SetQtyForm(qty.ToString(), MDIForm_, Palletizing_Unit_, GAPTiaoMa_v, index_, dvg_);
                    SetQtyform.Owner = MDIForm_;
                    SetQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    SetQtyform.ShowDialog();
                }
                else if (MDIForm_.Name == "YouyikuForm" || (MDIForm_.Name == "SchedulingTableForm" && dvg_.Name == "ProducedGV" && dvg_.Rows[index_].Cells["potype"].Value.ToString().Contains("GU")) || (MDIForm_.Name == "SchedulingTableForm" && dvg_.Name == "PlandGV" && dvg_.Rows[index_].Cells["potype"].Value.ToString().Contains("GU")))//
                {
                    if (MDIForm_.Name == "SchedulingTableForm" && dvg_.Name == "ProducedGV")
                    {
                        DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YOUYIKUDO WHERE guid='{0}'", dvg_.Rows[index_].Cells["po_guid"].Value.ToString()));
                        packingtotal_v = int.Parse(dt.Rows[0]["packingtotal"].ToString().Trim());
                        total = int.Parse(dt.Rows[0]["Quantity"].ToString().Trim());
                        qty = total - packingtotal_v;
                    }
                    else if (MDIForm_.Name == "SchedulingTableForm" && dvg_.Name == "PlandGV")
                    {
                        DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.*,Qty_per_Set AS '件数/箱',ISNULL(finishmarknumber,0)*Qty_per_Set AS '已完成件数', b.库存数量,(ISNULL(b.库存数量,0)/Qty_per_Set) AS '库存箱数',((a.Quantity- ISNULL(finishmarknumber,0))*Qty_per_Set) AS '剩余需求件数',((a.Quantity- ISNULL(finishmarknumber,0))) AS '剩余需求箱数'  FROM dbo.YOUYIKUDO AS a INNER JOIN dbo.ERPEXCH AS b ON a.SKU_Code=b.条形码 WHERE a.guid='{0}'", dvg_.Rows[index_].Cells["guid"].Value.ToString()));
                        int OrderQty = int.Parse(dt.Rows[0]["剩余需求件数"].ToString().Trim());
                        int ckqty = int.Parse(dt.Rows[0]["库存数量"].ToString().Trim());
                        if (OrderQty > ckqty)
                        {
                            qty = int.Parse(dt.Rows[0]["库存箱数"].ToString().Trim());
                            if (qty < 0)
                            {
                                qty = 0;
                            }
                        }
                        else
                        {
                            qty = int.Parse(dt.Rows[0]["剩余需求箱数"].ToString().Trim());
                            if (qty < 0)
                            {
                                qty = 0;
                            }
                        }
                    }
                    else
                    {
                        packingtotal_v = int.Parse(dvg_.Rows[index_].Cells["packingtotal"].Value.ToString().Trim());
                        total = int.Parse(dvg_.Rows[index_].Cells[Qty_ColumnName_].Value.ToString().Trim());
                        qty = total - packingtotal_v;
                    }
                    SetQtyform = new SetQtyForm(qty.ToString(), MDIForm_, Palletizing_Unit_, GAPTiaoMa_v, index_, dvg_);
                    SetQtyform.Owner = MDIForm_;
                    SetQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    SetQtyform.ShowDialog();
                }
            }
            else
            {
                SetQtyform.ShowDialog();
                SetQtyform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                SetQtyform.BringToFront();
            }
        }
        public static void MessageBoxFormShow(string str_)
        {
            try
            {
                MessageBoxForm MessageBoxform = null;
                if (MessageBoxform == null || MessageBoxform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                {
                    MessageBoxform = new MessageBoxForm(str_);
                    MessageBoxform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    MessageBoxform.ShowDialog();
                    MessageBoxform.TopMost = true;
                }
                else
                {
                    MessageBoxform.ShowDialog();
                    MessageBoxform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    MessageBoxform.BringToFront();
                    MessageBoxform.TopMost = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// 获取已选择的项目索引
        /// </summary>
        /// <param name="dvg_"></param>
        /// <returns></returns>
        public static int  DvgGetChoseIndex(DataGridView dvg_)
        {
            try
            {
                if (dvg_.Rows.Count==0)
                {
                    return -1;
                }
                for (int i = 0; i < dvg_.Rows.Count; i++)
                {
                    bool b_v = (bool)dvg_.Rows[i].Cells["已选择"].Value;
                    if (b_v)
                    {
                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
            return -1;
        }
        /// <summary>
        /// 强制结束订单
        /// </summary>
        /// <param name="dvg_"></param>
        /// <param name="index_"></param>
        public static bool OrverOrderNo(DataGridView dvg_,int index_)
        {
            try
            {
                DialogResult btchose = MessageBox.Show("是否强制结束当前订单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    bool b_v = true;
                    string potype = dvg_.Rows[index_].Cells["potype"].Value.ToString().Trim();
                    if (potype.Contains("GU") || potype.Contains("UNIQLO"))
                    {
                        b_v = CsGetData.YYKGUCompelChange(dvg_.Rows[index_].Cells["订单号码"].Value.ToString(), dvg_.Rows[index_].Cells["Warehouse"].Value.ToString(), dvg_.Rows[index_].Cells["Set_Code"].Value.ToString());
                    }
                    else
                    {
                        b_v = CsGetData.GAPCompelChange(dvg_.Rows[index_].Cells["订单号码"].Value.ToString(), dvg_.Rows[index_].Cells["从"].Value.ToString());
                    }
                    if (!b_v)
                    {
                        return false;
                    }
                    MessageBox.Show("已结束当前订单");
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }
        /// <summary>
        /// 扣减库存计算
        /// </summary>
        /// <param name="po_guid_"></param>
        public static void ComputeStock(string po_guid_)
        {
            try
            {
                SqlParameter[] parameter = new SqlParameter[1];
                parameter[0] = new SqlParameter("@Po_guid", System.Data.SqlDbType.NVarChar, 100);
                parameter[0].Value = po_guid_;
                CsFormShow.GoProc_ParameterNoSelect(parameter, "pm_rp_ComputeStock");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
    }
}
