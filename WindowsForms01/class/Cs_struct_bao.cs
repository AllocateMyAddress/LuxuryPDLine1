﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace WindowsForms01
{
    public  class struct_bao
    {
        public struct stc_bao_tag
        {
            public int iTAG_ID;
            public string strTAG_NAME;
            public string strLL_TEXT;
            public int iLL_TEXT_num;
            public string strLL_TEXT_TYPE;
            public string strVAL_VALUES;
            public string strDeviceName;
        };
        public struct stc_bao
        {
            public List<stc_bao_tag> listbao_tag;
            public int iHeadNum;
            public string strHeadval;
            public string strType;
            public int iLenRead;
            public string strDeviceName;
        };
        public struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public int cbData;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }
    }
}
