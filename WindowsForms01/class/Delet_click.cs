﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Collections;

namespace WindowsForms01
{
    class Delet_click
    {
        //右键删除DataGridView选定行方法
        public static void Delete_TSMenuItem_Click(Form form,Control dgv,string TableName_)
        {
            try
            {
                DialogResult RSS = MessageBox.Show(form, "确定要删除选中行数据码？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                switch (RSS)
                {
                    case DialogResult.Yes:
                        DataGridView dgv_v = dgv as DataGridView;
                        if (dgv_v.SelectedRows.Count==0)
                        {
                            //if (stcUserData.M_username != "admin")
                            //{
                            //    MessageBox.Show("提示：非admin管理员不能进行此操作");
                            //    return;
                            //}
                           // MessageBox.Show("未选中任何数据！！");
                            return;
                        }
                        else
                        {
                            string guid = "";
                            A.BLL.newsContent content = new A.BLL.newsContent();
                            for (int i = dgv_v.SelectedRows.Count; i > 0; i--)
                            {
                                if (!string.IsNullOrEmpty(dgv_v.SelectedRows[i - 1].Cells["guid"].Value.ToString()))
                                {
                                    if (form.Name == "userForm")
                                    {
                                        guid = dgv_v.SelectedRows[i - 1].Cells["guid"].Value.ToString();
                                        string ucode = dgv_v.SelectedRows[i - 1].Cells["用户ID"].Value.ToString();
                                        if (ucode == "admin")
                                        {
                                            MessageBox.Show("提示：管理员用户不能被删除！！");
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        guid = dgv_v.SelectedRows[i - 1].Cells["guid"].Value.ToString();
                                        #region 删除列队后执行操作
                                        //if (form.Name == "YYKMarkLineUpForm")
                                        //{
                                        //    int packingtotal = int.Parse(dgv_v.SelectedRows[i - 1].Cells["CurrentPackingNum"].Value.ToString().Trim());
                                        //    string YK_guid = dgv_v.SelectedRows[i - 1].Cells["YK_guid"].Value.ToString().Trim();
                                        //    string sqlyyk = string.Format("update dbo.YOUYIKUDO set packingtotal=packingtotal-{0} where guid='{1}'", packingtotal, YK_guid);
                                        //    content.Select_nothing(sqlyyk);
                                        //    string sqlupdatetiaoma = string.Format("UPDATE dbo.TiaoMa SET identification='',po='',potype='', TiaoMaVal='' WHERE DeviceName='{0}'", CSReadPlc.DevName);
                                        //    content.Select_nothing(sqlupdatetiaoma);
                                        //    M_stcScanData.strPoTiaoMa = "";
                                        //    M_stcScanData.strpo = "";
                                        //    M_stcScanData.stridentification = "";
                                        //    M_stcScanData.potype = "";
                                        //}
                                        //else if (form.Name == "MarkingLineUpForm")
                                        //{
                                        //    int packingtotal = int.Parse(dgv_v.SelectedRows[i - 1].Cells["CurrentPackingNum"].Value.ToString().Trim());
                                        //    string packing_guid = dgv_v.SelectedRows[i - 1].Cells["packing_guid"].Value.ToString().Trim();
                                        //    string sqlgap = string.Format("update dbo.PACKING set packingtotal=packingtotal-{0} where guid='{1}'", packingtotal, packing_guid);
                                        //    content.Select_nothing(sqlgap);
                                        //    string sqlupdatetiaoma = string.Format("UPDATE dbo.TiaoMa SET identification='',po='',potype='', TiaoMaVal='' WHERE DeviceName='{0}'", CSReadPlc.DevName);
                                        //    content.Select_nothing(sqlupdatetiaoma);
                                        //    M_stcScanData.strPoTiaoMa = "";
                                        //    M_stcScanData.strpo = "";
                                        //    M_stcScanData.stridentification = "";
                                        //    M_stcScanData.potype = "";
                                        //}
                                        #endregion
                                    }

                                    dgv_v.Rows.RemoveAt(dgv_v.SelectedRows[i - 1].Index);
                                    //使用获得的guid删除数据库的数据 
                                    if (TableName_.Contains("dbo.PACKING"))
                                    {
                                        CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.TMOM WHERE guid='{0}'", guid));
                                    }
                                    else if (TableName_.Contains("dbo.YOUYIKUDO"))
                                    {
                                        CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.TMOM WHERE guid='{0}'", guid));
                                    }
                                    string sqlstr = string.Format("DELETE FROM {0} where guid='{1}'", TableName_.Trim(), guid);
                                    content.Select_nothing(sqlstr);
                                }
                                else
                                {
                                    return;
                                }
                            }
                            Cslogfun.WriteOperationlog("操作员：" + stcUserData.M_username + " ，执行了操作：" + "删除一次‘" + form.Text + "’数据");
                        }
                        break;
                    case DialogResult.No:
                        break;
                }

            }
            catch (Exception err)
            {

                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        //菜单栏删除DataGridView所有显示数据方法
        public static void Delete_bttn_click(Form form, Control dgv)
        {
            try
            {

                DialogResult RSS = MessageBox.Show(form, "确定要删除当前显示的所有数据码？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                switch (RSS)
                {

                    case DialogResult.Yes:
                        DataGridView dgv_v = dgv as DataGridView;
                        List<int> intList = new List<int>();//定义存储要删除数据的ID的集合数组
                        for (int i = 0; i <= dgv_v.Rows.Count - 2; i++)
                        {
                            int ID = Convert.ToInt32(dgv_v.Rows[i].Cells[0].Value);
                            intList.Add(ID);//循环添加元素
                        }
                        for (int i = 0; i < intList.Count; i++)
                        {
                            //使用获得的ID删除数据库的数据 
                            A.BLL.newsContent content = new A.BLL.newsContent();
                            if (form.Name== "wageForm")
                            {
                                string id_delete = "where 序号=" + intList[i] + "";
                                string delete_sql = "delete from dbo.Wage_Classes" + " " + id_delete;
                                DataSet DS_delete = content.Select_nothing(delete_sql);
                            }
                            else if (form.Name == "DeptForm")
                            {
                                string id_delete = "where 序号=" + intList[i] + "";
                                string delete_sql = "delete from dbo.dept" + " " + id_delete;
                                DataSet DS_deleteshow = content.Select_nothing(delete_sql);
                            }
                            else if (form.Name == "EmployesForm")
                            {
                                string id_delete = "where 序号=" + intList[i] + "";
                                string delete_sql = "delete from dbo.Employes" + " " + id_delete;
                                DataSet DS_delete = content.Select_nothing(delete_sql);
                            }
                            else if (form.Name == "userForm")
                            {
                                string id_delete = "where ID=" + intList[i] + "";
                                string delete_sql = "delete from dbo.UserInfo" + " " + id_delete;
                                DataSet DS_delete = content.Select_nothing(delete_sql);
                            }

                        }
                        dgv_v.DataSource = null;//删除完毕清空显示数据
                        break;
                    case DialogResult.No:
                        break;
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        //删除指定的txt文件 @"D:\"+CSReadPlc.DevName+@"\log\YamahaLog\"
        public static void DeleteLogTxt(string strpath, int days)
        {
            try
            {
                string path = strpath /*+ DateTime.Now.ToString("yyyy-MM-dd") + ".txt"*/;//日志路径
                int n = days;
                string[] strOldLog_FileName = new string[n];
                if (Directory.Exists(strpath))
                {
                    for (int i = 0; i < n; i++)
                    {
                        strOldLog_FileName[i] = strpath + @"\" + DateTime.Now.AddDays(-i).ToString("yyyy-MM-dd") + ".txt";//保留n天记录
                    }
                    string pattern = "*.txt";
                    string[] strFileName = Directory.GetFiles(path, pattern);
                    var newarr = strFileName.Except(strOldLog_FileName);//获取差集
                    foreach (var item in newarr)
                    {
                        File.Delete(item);
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }

        }
        /// <summary>
        /// 暂停订单
        /// </summary>
        /// <param name="form"></param>
        /// <param name="dgv"></param>
        /// <param name="TableName_"></param>
        public static void Stop_TSMenuItem_Click(Form form, Control dgv, string TableName_)
        {
            try
            {
                DialogResult RSS = MessageBox.Show(form, "确定要停止选中订单？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                switch (RSS)
                {
                    case DialogResult.Yes:
                        DataGridView dgv_v = dgv as DataGridView;
                        if (dgv_v.SelectedRows.Count == 0)
                        {
                            MessageBox.Show("未选中任何数据！！");
                            return;
                        }
                        string guid = "";
                        for (int i = dgv_v.SelectedRows.Count; i > 0; i--)
                        {

                            if (!string.IsNullOrEmpty(dgv_v.SelectedRows[i - 1].Cells["guid"].Value.ToString()))
                            {
                                guid = dgv_v.SelectedRows[i - 1].Cells["guid"].Value.ToString();
                                dgv_v.Rows.RemoveAt(dgv_v.SelectedRows[i - 1].Index);
                                //使用获得的guid删除数据库的数据 
                                A.BLL.newsContent content = new A.BLL.newsContent();
                                string sqlstr = string.Format("UPDATE {0} SET STOP=1 WHERE guid='{1}'", TableName_.Trim(), guid);
                                content.Select_nothing(sqlstr);
                            }
                            else
                            {
                                return;
                            }
                        }
                        break;
                    case DialogResult.No:
                        break;
                }

            }
            catch (Exception err)
            {

                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 重新启动订单
        /// </summary>
        /// <param name="form"></param>
        /// <param name="dgv"></param>
        /// <param name="TableName_"></param>
        public static void Restart_TSMenuItem_Click(Form form, Control dgv, string TableName_)
        {
            try
            {
                DialogResult RSS = MessageBox.Show(form, "确定要重新开始选中订单？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                switch (RSS)
                {
                    case DialogResult.Yes:
                        DataGridView dgv_v = dgv as DataGridView;
                        if (dgv_v.SelectedRows.Count == 0)
                        {
                            MessageBox.Show("未选中任何数据！！");
                            return;
                        }
                        string guid = "";
                        for (int i = dgv_v.SelectedRows.Count; i > 0; i--)
                        {

                            if (!string.IsNullOrEmpty(dgv_v.SelectedRows[i - 1].Cells["guid"].Value.ToString()))
                            {
                                guid = dgv_v.SelectedRows[i - 1].Cells["guid"].Value.ToString();
                                dgv_v.Rows.RemoveAt(dgv_v.SelectedRows[i - 1].Index);
                                //使用获得的guid删除数据库的数据 
                                A.BLL.newsContent content = new A.BLL.newsContent();
                                string sqlstr = string.Format("UPDATE {0} SET STOP=0 WHERE guid='{1}'", TableName_.Trim(), guid);
                                content.Select_nothing(sqlstr);
                            }
                            else
                            {
                                return;
                            }
                        }
                        break;
                    case DialogResult.No:
                        break;
                }

            }
            catch (Exception err)
            {

                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 结束订单
        /// </summary>
        /// <param name="form"></param>
        /// <param name="dgv"></param>
        /// <param name="TableName_"></param>
        public static void End_TSMenuItem_Click(Form form, Control dgv, string TableName_)
        {
            try
            {
                DialogResult RSS = MessageBox.Show(form, "确定要终止选中订单？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                switch (RSS)
                {
                    case DialogResult.Yes:
                        DataGridView dgv_v = dgv as DataGridView;
                        if (dgv_v.SelectedRows.Count == 0)
                        {
                            MessageBox.Show("未选中任何数据！！");
                            return;
                        }
                        string guid = "";
                        for (int i = dgv_v.SelectedRows.Count; i > 0; i--)
                        {

                            if (!string.IsNullOrEmpty(dgv_v.SelectedRows[i - 1].Cells["guid"].Value.ToString()))
                            {
                                guid = dgv_v.SelectedRows[i - 1].Cells["guid"].Value.ToString();
                                dgv_v.Rows.RemoveAt(dgv_v.SelectedRows[i - 1].Index);
                                //使用获得的guid删除数据库的数据 
                                A.BLL.newsContent content = new A.BLL.newsContent();
                                string sqlstr = string.Format("UPDATE {0} SET ENDPO=1 WHERE guid='{1}'", TableName_.Trim(), guid);
                                content.Select_nothing(sqlstr);
                            }
                            else
                            {
                                return;
                            }
                        }
                        break;
                    case DialogResult.No:
                        break;
                }

            }
            catch (Exception err)
            {

                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 删除文件目录下的文件和文件夹
        /// </summary>
        /// <param name="file"></param>
        public static void DeleteDir(string file)
        {
            try
            {
                //去除文件夹和子文件的只读属性
                //去除文件夹的只读属性
                System.IO.DirectoryInfo fileInfo = new DirectoryInfo(file);
                fileInfo.Attributes = FileAttributes.Normal & FileAttributes.Directory;

                //去除文件的只读属性
                System.IO.File.SetAttributes(file, System.IO.FileAttributes.Normal);

                //判断文件夹是否还存在
                if (Directory.Exists(file))
                {
                    foreach (string f in Directory.GetFileSystemEntries(file))
                    {
                        if (File.Exists(f))
                        {
                            //如果有子文件删除文件
                            File.Delete(f);
                        }
                        else
                        {
                            //循环递归删除子文件夹
                            DeleteDir(f);
                        }
                    }
                    //删除空文件夹
                   // Directory.Delete(file);
                }

            }
            catch (Exception ex) // 异常处理
            {
                MessageBox.Show(ex.Message.ToString());// 异常信息
            }
        }
    }
}
