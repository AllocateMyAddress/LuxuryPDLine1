﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using GetData;
using System.IO;

namespace WindowsForms01
{
    public class CsGetData
    {

        /// <summary>
        /// 获取字符串中的数字字符串
        /// </summary>
        /// <param name="str_"></param>
        /// <returns></returns>
        public static string GetNumFromStr(string str_)
        {
            try
            {
                string str = "";
                str = System.Text.RegularExpressions.Regex.Replace(str_, @"[^0-9]+", "");
                // str = System.Text.RegularExpressions.Regex.Replace(str_, "", ",");
                return str;
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
        }
        public static string GetLeft0FromStr(string str_)
        {
            string str_v = "";
            try
            {
                if (str_ == null || str_.Length == 0)    //验证这个参数是否为空
                    return "";                           //是，就返回False

                for (int i = 0; i < str_.Length; i++)
                {
                    string a = str_.Substring(i,1);
                    if (a == "0")
                    {
                        str_v = str_v + "0";
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.ToString());
            }
            return str_v;
        }
        /// <summary>
        /// 更新扫码队列信息
        /// </summary>
        public static void UpdataScanLineUp()
        {
            try
            {
                int gucount_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.YYK_GU_Current WHERE ISNULL(IsEndScan,0)<>1 and DeviceName = '{0}'", CSReadPlc.DevName.Trim()));
                int gapcount_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.GAP_Current WHERE ISNULL(IsEndScan,0)<>1  AND DeviceName='{0}'", CSReadPlc.DevName.Trim()));
                DataTable dt_line = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM(SELECT  potype,Entrytime FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndScan,0)<>1  AND DeviceName='{0}' UNION ALL SELECT 'GAP' potype, Entrytime FROM dbo.MarkingLineUp WHERE ISNULL(IsEndScan, 0) <> 1 AND DeviceName='{0}') AS LINE  ORDER BY LINE.Entrytime", CSReadPlc.DevName.Trim()));
                //判断订单队列是否和扫码队列一致
                int isexist = 0;
                if (dt_line.Rows.Count > 0)
                {
                    string potype = dt_line.Rows[0]["potype"].ToString().Trim();
                    if (potype.Contains("GU") || potype.Contains("UNIQLO"))
                    {
                        DataTable dt_topline = CsFormShow.GoSqlSelect(string.Format("SELECT  TOP 1   DeviceName, Order_No, Warehouse, Set_Code  FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndScan,0)<>1  AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        if (dt_topline.Rows.Count > 0)
                        {
                            isexist = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.YYK_GU_Current WHERE  DeviceName = '{0}' AND po='{1}' AND Warehouse='{2}' AND Set_Code='{3}' and isnull(IsEndScan,0)<>1", CSReadPlc.DevName, dt_topline.Rows[0]["Order_No"].ToString().Trim(), dt_topline.Rows[0]["Warehouse"].ToString().Trim(), dt_topline.Rows[0]["Set_Code"].ToString().Trim()));
                        }
                    }
                    else
                    {
                        DataTable dt_topline = CsFormShow.GoSqlSelect(string.Format("SELECT  TOP 1   DeviceName,订单号码,  从  FROM dbo.MarkingLineUp WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        isexist = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.GAP_Current WHERE  订单号码='{0}' AND  从='{1}'", dt_topline.Rows[0]["订单号码"].ToString().Trim(), dt_topline.Rows[0]["从"].ToString().Trim()));
                    }
                }
                //执行更新条码信息
                if ((gucount_v == 0 && gapcount_v == 0 && dt_line.Rows.Count > 0) || (isexist == 0 && dt_line.Rows.Count > 0))
                {
                    if (dt_line.Rows[0]["potype"].ToString().Contains("GU") || dt_line.Rows[0]["potype"].ToString().Contains("UNIQLO"))
                    {
                        CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.YYK_GU_Current WHERE DeviceName='{0}'", CSReadPlc.DevName.Trim()));
                        DataTable dtsetcode = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 YK_guid, DeviceName, SKU_Code, Order_No, Warehouse,CurrentPackingNum, 0, Color_Code, Set_Code,Qty_per_Set,Picking_Unit  FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO dbo.YYK_GU_Current (Doguid,DeviceName,TiaoMaVal,po,Warehouse,CurrentPackingNum,FinishPackingNum,Color_Code,Set_Code,Qty_per_Set,Picking_Unit,potype)  SELECT  guid, '{0}', SKU_Code, Order_No, Warehouse,'{1}', 0, Color_Code, Set_Code,Qty_per_Set,Picking_Unit,potype  FROM dbo.YOUYIKUDO WHERE   Order_No='{2}' AND Warehouse='{3}' AND Set_Code='{4}' ", CSReadPlc.DevName.Trim(), dtsetcode.Rows[0]["CurrentPackingNum"], dtsetcode.Rows[0]["Order_No"], dtsetcode.Rows[0]["Warehouse"], dtsetcode.Rows[0]["Set_Code"]));
                        //将当前条码数据写入PLC
                        M_stcScanData.dtCurrentData = CsFormShow.GoSqlSelect(string.Format("SELECT *FROM dbo.YYK_GU_Current WHERE  DeviceName='{0}' ORDER BY Color_Code", CSReadPlc.DevName));
                        DataTable dt_yykline = CsFormShow.GoSqlSelect(string.Format(" SELECT TOP 1 * FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndScan,0)<>1  AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        M_stcScanData.M_List_WriteToPlcSku = GetSkuWriteToPlc(M_stcScanData.dtCurrentData, dt_line.Rows[0]["potype"].ToString(), dt_yykline.Rows[0]["IsUnpacking"].ToString().Trim());
                        M_stcScanData.M_WriteToPlcSku = "start";

                    }
                    else
                    {
                        CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.GAP_Current WHERE DeviceName='{0}'", CSReadPlc.DevName.Trim()));
                        CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO dbo.GAP_Current(potype ,packing_guid ,范围 ,从 ,到 ,数量 ,内部包装的项目数量 ,内包装计数 ,箱数 ,净重 ,毛重 ,长 ,宽 , 高 ,开始序列号 , 截止序列号 , 包装代码, 订单号码 , Sku号码 ,Color ,扫描ID ,外箱代码 , Entrytime  ,packingtotal ,ENDPO ,finishmarknumber ,finishprintnumber ,CurrentPackingNum ,TiaoMaVal,DeviceName )SELECT TOP 1  'GAP' ,packing_guid ,范围 ,从 ,到 ,数量 ,内部包装的项目数量 ,内包装计数 ,箱数 ,净重 ,毛重 ,长 ,宽 , 高 ,开始序列号 , 截止序列号 , 包装代码, 订单号码 , Sku号码 ,Color ,扫描ID ,外箱代码 , Entrytime  ,0 ,ENDPO ,0 ,0 ,CurrentPackingNum,TiaoMaVal,DeviceName FROM dbo.MarkingLineUp WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        //将当前条码数据写入PLC
                        M_stcScanData.dtCurrentData = CsFormShow.GoSqlSelect(String.Format("SELECT TOP 1 * FROM dbo.MarkingLineUp WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}'  ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        DataTable dt_Gapline = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 a.*,b.TiaoMaVal FROM dbo.MarkingLineUp AS a INNER JOIN dbo.PACKING AS b ON a.packing_guid=b.guid  WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        M_stcScanData.M_List_WriteToPlcSku = GetSkuWriteToPlc(M_stcScanData.dtCurrentData, "GAP", dt_Gapline.Rows[0]["IsUnpacking"].ToString().Trim());
                        M_stcScanData.M_WriteToPlcSku = "start";
                    }
                }
                if (dt_line.Rows.Count > 0)
                {
                    if (dt_line.Rows[0]["potype"].ToString().Contains("GU") || dt_line.Rows[0]["potype"].ToString().Contains("UNIQLO"))
                    {
                        //获取当前单据的存储表
                        DataTable dt_curerent = CsFormShow.GoSqlSelect(string.Format("SELECT *FROM dbo.YYK_GU_Current WHERE  DeviceName = '{0}'", CSReadPlc.DevName.Trim()));
                        DataTable dt_linetop = CsFormShow.GoSqlSelect(string.Format(" SELECT TOP 1 * FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndScan,0)<>1  AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        DataTable dt_Marklinetop = CsFormShow.GoSqlSelect(string.Format(" SELECT TOP 1 * FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1  AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        if (dt_linetop.Rows.Count > 0)
                        {
                            DataTable dtpo = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YOUYIKUPO WHERE guid IN(SELECT po_guid FROM dbo.YOUYIKUDO WHERE guid='{0}')", dt_linetop.Rows[0]["YK_guid"].ToString()));
                            if (dtpo.Rows.Count > 0)
                            {
                                M_stcScanData.Sample_Code = dtpo.Rows[0]["Sample_Code"].ToString().Trim().Substring(0, 2);
                            }
                            //更新当前条码缓存变量信息
                            M_stcScanData.strPoTiaoMa = dt_linetop.Rows[0]["SKU_Code"].ToString().Trim();
                            M_stcScanData.strpo = dt_linetop.Rows[0]["Order_No"].ToString().Trim();
                            M_stcScanData.stridentification = dt_linetop.Rows[0]["Warehouse"].ToString().Trim();
                            M_stcScanData.Color_Code = dt_linetop.Rows[0]["Color_Code"].ToString().Trim();
                            M_stcScanData.Set_Code = dt_linetop.Rows[0]["Set_Code"].ToString().Trim();
                            M_stcScanData.dtCurrentData = dt_curerent;
                            M_stcScanData.IsUnpacking = dt_linetop.Rows[0]["IsUnpacking"].ToString().Trim();
                            M_stcScanData.IsPalletizing = dt_linetop.Rows[0]["IsPalletizing"].ToString().Trim();
                            M_stcScanData.IsPackingRun = dt_linetop.Rows[0]["IsPackingRun"].ToString().Trim();
                            M_stcScanData.IsYaMaHaRun = dt_linetop.Rows[0]["IsYaMaHaRun"].ToString().Trim();
                            M_stcScanData.IsSealingRun = dt_linetop.Rows[0]["IsSealingRun"].ToString().Trim();
                            M_stcScanData.IsScanRun = dt_linetop.Rows[0]["IsScanRun"].ToString().Trim();
                            M_stcScanData.HookDirection = dt_linetop.Rows[0]["HookDirection"].ToString().Trim();
                            M_stcScanData.CurrentPackingNum = dt_linetop.Rows[0]["CurrentPackingNum"].ToString().Trim();
                            M_stcScanData.FinishPackingNum = dt_linetop.Rows[0]["FinishPackingNum"].ToString().Trim();
                            M_stcScanData.IsEndScan = false;
                            M_stcScanData.IsEndPacking = false;
                            M_stcScanData.potype = dt_line.Rows[0]["potype"].ToString();
                            if (dt_Marklinetop.Rows.Count > 0)
                            {
                                M_stcScanData.MarkingIsshieid = dt_Marklinetop.Rows[0]["MarkingIsshieid"].ToString().Trim();
                                M_stcScanData.IsPalletizingRun = dt_Marklinetop.Rows[0]["IsPalletizingRun"].ToString().Trim();
                            }
                            //设置扫描读取条码数量
                            // CsScanning.ChangeScan(M_stcScanData.Sample_Code);
                            //将当前条码数据写入PLC
                            M_stcScanData.dtCurrentData = CsFormShow.GoSqlSelect(string.Format("SELECT *FROM dbo.YYK_GU_Current WHERE  DeviceName='{0}' ORDER BY Color_Code", CSReadPlc.DevName));
                            DataTable dt_yykline = CsFormShow.GoSqlSelect(string.Format(" SELECT TOP 1 * FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndScan,0)<>1  AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                            M_stcScanData.M_List_WriteToPlcSku = GetSkuWriteToPlc(M_stcScanData.dtCurrentData, dt_line.Rows[0]["potype"].ToString(), dt_yykline.Rows[0]["IsUnpacking"].ToString().Trim());
                            M_stcScanData.M_WriteToPlcSku = "start";
                        }
                    }
                    else
                    {
                        //获取当前单据的存储表
                        DataTable dt_Gaplinetop = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 *,(SELECT TOP 1 CASE when ISNULL(finishmarknumber,'') IN('',null) THEN 0 ELSE finishmarknumber END FROM dbo.PACKING AS p WHERE p.guid=dbo.MarkingLineUp.packing_guid) AS 'finishmarknumber'  FROM dbo.MarkingLineUp WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        DataTable dt_MarkGaplinetop = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM dbo.MarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                        if (dt_Gaplinetop.Rows.Count > 0)
                        {
                            M_stcScanData.strPoTiaoMa = dt_Gaplinetop.Rows[0]["TiaoMaVal"].ToString().Trim();
                            M_stcScanData.strpo = dt_Gaplinetop.Rows[0]["订单号码"].ToString().Trim();
                            M_stcScanData.stridentification = dt_Gaplinetop.Rows[0]["从"].ToString().Trim();
                            M_stcScanData.FinishmarknumberTotal =int.Parse( dt_Gaplinetop.Rows[0]["finishmarknumber"].ToString().Trim());
                            string wai_code = dt_Gaplinetop.Rows[0]["外箱代码"].ToString().Trim();
                            if (wai_code.ToUpper() != "C4" && wai_code.ToUpper() != "C6")
                            {
                                M_stcScanData.potype = "GAP国内";
                            }
                            else
                            {
                                M_stcScanData.potype = "GAP国外";
                            }
                            M_stcScanData.Color_Code = "";
                            M_stcScanData.Set_Code = "";
                            M_stcScanData.IsEndScan = false;
                            M_stcScanData.IsEndPacking = false;
                            M_stcScanData.dtCurrentData = dt_Gaplinetop;
                            M_stcScanData.IsUnpacking = dt_Gaplinetop.Rows[0]["IsUnpacking"].ToString().Trim();
                            if (dt_MarkGaplinetop.Rows.Count > 0)
                            {
                                M_stcScanData.MarkingIsshieid = dt_MarkGaplinetop.Rows[0]["MarkingIsshieid"].ToString().Trim();
                                M_stcScanData.IsPalletizingRun = dt_MarkGaplinetop.Rows[0]["IsPalletizingRun"].ToString().Trim();
                            }
                            M_stcScanData.IsPalletizing = dt_Gaplinetop.Rows[0]["IsPalletizing"].ToString().Trim();
                            M_stcScanData.IsPackingRun = dt_Gaplinetop.Rows[0]["IsPackingRun"].ToString().Trim();
                            M_stcScanData.IsYaMaHaRun = dt_Gaplinetop.Rows[0]["IsYaMaHaRun"].ToString().Trim();
                            M_stcScanData.IsSealingRun = dt_Gaplinetop.Rows[0]["IsSealingRun"].ToString().Trim();

                            M_stcScanData.IsScanRun = dt_Gaplinetop.Rows[0]["IsScanRun"].ToString().Trim();
                            M_stcScanData.HookDirection = dt_Gaplinetop.Rows[0]["HookDirection"].ToString().Trim();
                            M_stcScanData.CurrentPackingNum = dt_Gaplinetop.Rows[0]["CurrentPackingNum"].ToString().Trim();
                            M_stcScanData.FinishPackingNum = dt_Gaplinetop.Rows[0]["FinishPackingNum"].ToString().Trim();
                            //设置扫描读取条码数量
                            //CsScanning.ChangeScan(M_stcScanData.Sample_Code);
                            //将当前条码数据写入PLC
                            M_stcScanData.dtCurrentData = CsFormShow.GoSqlSelect(String.Format("SELECT TOP 1 * FROM dbo.MarkingLineUp WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}'  ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                            DataTable dt_Gapline = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 a.*,b.TiaoMaVal FROM dbo.MarkingLineUp AS a INNER JOIN dbo.PACKING AS b ON a.packing_guid=b.guid  WHERE ISNULL(IsEndScan,0)<>1 AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                            M_stcScanData.M_List_WriteToPlcSku = GetSkuWriteToPlc(M_stcScanData.dtCurrentData, "GAP", dt_Gapline.Rows[0]["IsUnpacking"].ToString().Trim());
                            M_stcScanData.M_WriteToPlcSku = "start";
                        }
                    }
                }
                DataTable dt_Marknow = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM(SELECT  potype,Entrytime FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1 AND DeviceName='{0}'  UNION ALL SELECT 'GAP' potype, Entrytime FROM dbo.MarkingLineUp WHERE ISNULL(IsEndMarking, 0) <> 1 AND DeviceName='{0}')  AS LINE ORDER BY LINE.Entrytime", CSReadPlc.DevName.Trim()));
                if (dt_Marknow.Rows.Count > 0)
                {
                    DataTable dt_Markline = new DataTable();
                    if (dt_Marknow.Rows[0]["potype"].ToString().Contains("GU") || dt_Marknow.Rows[0]["potype"].ToString().Contains("UNIQLO"))
                    {
                        dt_Markline = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM dbo.YYKMarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1  AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                    }
                    else
                    {
                        dt_Markline = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM dbo.MarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1  AND DeviceName='{0}' ORDER BY Entrytime", CSReadPlc.DevName.Trim()));
                    }
                    if (dt_Markline.Rows.Count > 0)
                    {
                        M_stcScanData.MarkingIsshieid = dt_Markline.Rows[0]["MarkingIsshieid"].ToString().Trim();
                        M_stcScanData.PrintIsshieid = dt_Markline.Rows[0]["PrintIsshieid"].ToString().Trim();
                        M_stcScanData.IsPalletizingRun = dt_Markline.Rows[0]["IsPalletizingRun"].ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
        }
        public static void GetPartitionData(int CatornLong_, int CatornWidth_, string Partitiontype_, out string ABPartitionType, out string IsTopPartition, out string IsBottomPartition)
        {

            if (CatornLong_ > 600 && CatornWidth_ > 300 && !Partitiontype_.Contains("无"))
            {
                ABPartitionType = "B隔板";
                if (Partitiontype_.Contains("天地盖"))
                {
                    IsTopPartition = "1";
                    IsBottomPartition = "1";
                }
                else if (Partitiontype_ == "天盖")
                {
                    IsTopPartition = "1";
                    IsBottomPartition = "2";
                }
                else if (Partitiontype_ == "地盖")
                {
                    IsTopPartition = "2";
                    IsBottomPartition = "1";
                }
                else
                {
                    IsTopPartition = "2";
                    IsBottomPartition = "2";
                }
            }
            else if (!Partitiontype_.Contains("无"))
            {
                ABPartitionType = "A隔板";
                if (Partitiontype_.Contains("天地盖"))
                {
                    IsTopPartition = "1";
                    IsBottomPartition = "1";
                }
                else if (Partitiontype_ == "天盖")
                {
                    IsTopPartition = "1";
                    IsBottomPartition = "2";
                }
                else if (Partitiontype_ == "地盖")
                {
                    IsTopPartition = "2";
                    IsBottomPartition = "1";
                }
                else
                {
                    IsTopPartition = "2";
                    IsBottomPartition = "2";
                }
            }
            else
            {
                ABPartitionType = "";
                IsTopPartition = "2";
                IsBottomPartition = "2";
            }

        }
        /// <summary>
        /// 获取箱内件数
        /// </summary>
        /// <param name="dt_"></param>
        /// <param name="IsUnpacking_"></param>
        /// <param name="PoType_"></param>
        /// <returns></returns>
        public static int GetLayernum(DataTable dt_, string IsUnpacking_, string PoType_)
        {
            int Layernum = 0;
            try
            {
                #region 计算优衣库和GU箱件数
                if (!PoType_.Contains("GAP"))
                {
                    string Order_No = dt_.Rows[0]["Order_No"].ToString();
                    string Warehouse = dt_.Rows[0]["Warehouse"].ToString();
                    string Set_Code = dt_.Rows[0]["Set_Code"].ToString();
                    if (Set_Code.Contains("-999"))//单色装箱计算箱件数
                    {
                        //if (IsUnpacking_ == "拆包")
                        //{
                        //    Layernum = int.Parse(dt_.Rows[0]["Qty_per_Set"].ToString());
                        //}
                        //else
                        //{
                        int Qty_per_Set = int.Parse(dt_.Rows[0]["Qty_per_Set"].ToString());
                        int Picking_Unit = int.Parse(dt_.Rows[0]["Picking_Unit"].ToString());
                        if (Picking_Unit != 0)
                        {
                            Layernum = int.Parse((Qty_per_Set / Picking_Unit).ToString());
                        }
                        else
                        {
                            Layernum = int.Parse(Qty_per_Set.ToString());
                        }
                        // }
                    }
                    else//混色装箱计算箱件数
                    {
                        DataTable dtsetcode = CsFormShow.GoSqlSelect(string.Format("SELECT Color_Code, Qty_per_Set,Picking_Unit FROM dbo.YOUYIKUDO WHERE Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", Order_No, Warehouse, Set_Code));
                        for (int i = 0; i < dtsetcode.Rows.Count; i++)
                        {
                            Layernum = int.Parse(dtsetcode.Rows[i]["Qty_per_Set"].ToString().Trim()) + Layernum;
                        }
                    }
                }

                #endregion
                else
                {
                    #region 计算GAP箱件数
                    string Poguid_ = dt_.Rows[0]["guid"].ToString().Trim();
                    string count_bao = dt_.Rows[0]["内包装计数"].ToString().Trim();
                    if (!string.IsNullOrEmpty(count_bao) && count_bao != "0")//多条包装计算箱件数
                    {
                        //if (IsUnpacking_ == "拆包")
                        //{
                        //    int Picking_Unit = int.Parse(dt_.Rows[0]["内包装计数"].ToString().Trim());
                        //    int Picking_count = int.Parse(dt_.Rows[0]["内部包装的项目数量"].ToString().Trim());
                        //    Layernum = int.Parse((Picking_count * Picking_Unit).ToString());
                        //}
                        //else
                        //{
                        Layernum = int.Parse(dt_.Rows[0]["内包装计数"].ToString().Trim());
                        //}
                    }
                    else//单条包装计算箱件数
                    {
                        Layernum = int.Parse(dt_.Rows[0]["内部包装的项目数量"].ToString().Trim());
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
            return Layernum;
        }
        /// <summary>
        /// 获取指定颜色的箱内件数
        /// </summary>
        /// <param name="dt_"></param>
        /// <param name="IsUnpacking_"></param>
        /// <param name="PoType_"></param>
        /// <returns></returns>
        public static int GetColorLayernum(DataTable dt_, string IsUnpacking_, string PoType_, string Color_Code_)
        {
            int Layernum = 0;
            try
            {
                #region 计算优衣库和GU箱件数
                if (!PoType_.Contains("GAP"))
                {
                    string Order_No = dt_.Rows[0]["po"].ToString();
                    string Warehouse = dt_.Rows[0]["Warehouse"].ToString();
                    string Set_Code = dt_.Rows[0]["Set_Code"].ToString();
                    if (Set_Code.Contains("-999"))//单色装箱计算箱件数
                    {
                        if (IsUnpacking_ == "拆包")
                        {
                            Layernum = int.Parse(dt_.Rows[0]["Qty_per_Set"].ToString());
                        }
                        else
                        {
                            int Qty_per_Set = int.Parse(dt_.Rows[0]["Qty_per_Set"].ToString());
                            int Picking_Unit = int.Parse(dt_.Rows[0]["Picking_Unit"].ToString());
                            Layernum = int.Parse((Qty_per_Set / Picking_Unit).ToString());
                        }
                    }
                    else//混色装箱计算箱件数
                    {
                        DataTable dtsetcode = CsFormShow.GoSqlSelect(string.Format("SELECT Color_Code, Qty_per_Set,Picking_Unit FROM dbo.YOUYIKUDO WHERE Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", Order_No, Warehouse, Set_Code));
                        for (int i = 0; i < dtsetcode.Rows.Count; i++)
                        {
                            if (dtsetcode.Rows[i]["Color_Code"].ToString().Contains(Color_Code_))
                            {
                                Layernum = int.Parse(dtsetcode.Rows[i]["Qty_per_Set"].ToString().Trim());
                            }
                        }
                    }
                }

                #endregion
                else
                {
                    #region 计算GAP箱件数
                    string Poguid_ = dt_.Rows[0]["guid"].ToString().Trim();
                    string count_bao = dt_.Rows[0]["内包装计数"].ToString().Trim();
                    if (!string.IsNullOrEmpty(count_bao) && count_bao != "0")//多条包装计算箱件数
                    {
                        if (IsUnpacking_ == "拆包")
                        {
                            int Picking_Unit = int.Parse(dt_.Rows[0]["内包装计数"].ToString().Trim());
                            int Picking_count = int.Parse(dt_.Rows[0]["内部包装的项目数量"].ToString().Trim());
                            Layernum = int.Parse((Picking_count * Picking_Unit).ToString());
                        }
                        else
                        {
                            Layernum = int.Parse(dt_.Rows[0]["内包装计数"].ToString().Trim());
                        }
                    }
                    else//单条包装计算箱件数
                    {
                        Layernum = int.Parse(dt_.Rows[0]["内部包装的项目数量"].ToString().Trim());
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
            return Layernum;
        }
        /// <summary>
        /// 优衣库GU强制切单后操作
        /// </summary>
        public static bool YYKGUCompelChange()
        {
            bool b_v = true;
            try
            {
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return false;
                }
                if (!string.IsNullOrEmpty(M_stcScanData.strpo) && (M_stcScanData.potype.Contains("GU") || M_stcScanData.potype.Contains("UNIQLO")))
                {
                    //修改订单装箱总数
                    string sqlstrdo = string.Format("UPDATE a SET packingtotal = ISNULL(b.FinishPackingNum, 0) FROM dbo.YOUYIKUDO AS a, dbo.YYKMarkingLineUp AS b WHERE b.guid IN(SELECT guid FROM dbo.YYKMarkingLineUp AS c WHERE Order_No = '{0}' AND Warehouse = '{1}' AND potype = '{2}' AND Set_Code = '{3}') AND EXISTS(SELECT * FROM dbo.YYKMarkingLineUp AS c WHERE Order_No = a.Order_No AND Warehouse = a.Warehouse  AND Set_Code = a.Set_Code AND Order_No = '{0}' AND Warehouse = '{1}' AND potype = '{2}' AND Set_Code = '{3}') ", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim(), M_stcScanData.potype.Trim(), M_stcScanData.Set_Code);
                    CsFormShow.GoSqlUpdateInsert(sqlstrdo);
                    //修改当前扫码队列的信息
                    string sqlstr2ineup = string.Format("UPDATE a SET CurrentPackingNum=ISNULL(b.FinishPackingNum,0) FROM YYK_GU_Current AS a, dbo.YYKMarkingLineUp AS b  WHERE b.Order_No='{0}'AND b.Warehouse='{1}' AND b.Color_Code='{2}' AND b.Set_Code='{3}' AND po=b.Order_No AND a.Warehouse=b.Warehouse  AND a.Set_Code=b.Set_Code", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim(), M_stcScanData.Color_Code, M_stcScanData.Set_Code);
                    CsFormShow.GoSqlUpdateInsert(sqlstr2ineup);
                    //修改列队当前喷码总数
                    string sqlstrlineup = string.Format("UPDATE dbo.YYKMarkingLineUp SET CurrentPackingNum=ISNULL(FinishPackingNum,0)  WHERE Order_No='{0}'AND SKU_Code='{1}' AND Warehouse='{2}' AND potype='{3}'AND Color_Code='{4}' AND Set_Code='{5}'", M_stcScanData.strpo.Trim(), M_stcScanData.strPoTiaoMa.Trim(), M_stcScanData.stridentification.Trim(), M_stcScanData.potype.Trim(), M_stcScanData.Color_Code, M_stcScanData.Set_Code);
                    CsFormShow.GoSqlUpdateInsert(sqlstrlineup);                
                    //重新加载喷码队列
                    if (M_stcScanData.potype.Contains("GU")|| M_stcScanData.potype.Contains("UNIQLO"))
                    {
                        CsInitialization.UpdateYYKGULineUp();
                        CsInitialization.UpdateYYKGUPalletizingLineUp();
                    }
                    //重新加载扫码队列信息
                    CsGetData.UpdataScanLineUp();//更新扫码队列
                    CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return b_v;
        }
        /// <summary>
        /// 优衣库GU强制切单后操作
        /// </summary>
        public static bool YYKGUCompelChange(DataGridView dvg, int index_)
        {
            bool b_v = true;
            try
            {
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return false;
                }
                bool b_line = CsFormShow.IsExistenceInSql(string.Format("(SELECT*FROM dbo.YYKMarkingLineUp WHERE isnull(DeviceName,'')<>'{0}') as a", CSReadPlc.DevName), "YK_guid", dvg.Rows[index_].Cells["po_guid"].Value.ToString().Trim());
                if (b_line)
                {
                    MessageBox.Show("此单不属于本生产线，不允许操作");
                    return false;
                }
                DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.*, b.* FROM   (SELECT guid AS Scheduling_guid, po_guid, packingqty, IsUnpacking, MarkingIsshieid, PrintIsshieid, GAPIsPrintRun, IsPalletizing, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, IsScanRun, HookDirection,TiaoMaVal AS Scheduling_TiaoMaVal FROM dbo.YYKGUSchedulingTable  ) AS a INNER JOIN dbo.YOUYIKUDO AS b ON a.po_guid = b.guid WHERE Scheduling_guid='{0}'", dvg.Rows[index_].Cells["guid"].Value.ToString().Trim()));
                string potype = dt.Rows[0]["potype"].ToString().Trim();
                string strpo = dt.Rows[0]["Order_No"].ToString().Trim();
                string stridentification = dt.Rows[0]["Warehouse"].ToString().Trim();
                string Set_Code = dt.Rows[0]["Set_Code"].ToString().Trim();
                string Color_Code = dt.Rows[0]["Color_Code"].ToString().Trim();
                string strPoTiaoMa = dt.Rows[0]["SKU_Code"].ToString().Trim();
                if (potype.Contains("GU") || potype.Contains("UNIQLO"))
                {
                    //修改订单装箱总数
                    DataTable dtline = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YYKMarkingLineUp  WHERE Order_No='{0}'AND SKU_Code='{1}' AND Warehouse='{2}' AND potype='{3}'AND Color_Code='{4}' AND Set_Code='{5}'", strpo, strPoTiaoMa, stridentification, potype, Color_Code, Set_Code));
                    //修改订单装箱总数
                    string sqlstrdo = "";
                    sqlstrdo = string.Format("UPDATE a SET packingtotal = finishmarknumber FROM dbo.YOUYIKUDO AS a where Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", strpo, stridentification, Set_Code);
                    CsFormShow.GoSqlUpdateInsert(sqlstrdo);
                    //修改当前扫码队列的信息
                    string sqlstr2ineup = string.Format("UPDATE a SET CurrentPackingNum=ISNULL(b.FinishPackingNum,0) FROM YYK_GU_Current AS a, dbo.YYKMarkingLineUp AS b  WHERE b.Order_No='{0}'AND b.Warehouse='{1}' AND b.Color_Code='{2}' AND b.Set_Code='{3}' AND po=b.Order_No AND a.Warehouse=b.Warehouse  AND a.Set_Code=b.Set_Code", strpo, stridentification, Color_Code, Set_Code);
                    CsFormShow.GoSqlUpdateInsert(sqlstr2ineup);
                    //修改列队当前喷码总数
                    string sqlstrlineup = string.Format("UPDATE a SET a.CurrentPackingNum=ISNULL(a.FinishPackingNum,0),a.IsEndScan=1,a.IsEndMarking=CASE WHEN a.finishmarknumber=a.FinishPackingNum THEN 1 END FROM YOUYIKUDO AS b, YYKMarkingLineUp AS a WHERE b.guid = a.YK_guid AND a.Order_No = '{0}' AND a.Warehouse = '{1}' AND a.potype = '{2}' AND a.Set_Code = '{3}'", strpo, stridentification, potype, Set_Code);
                    CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                    //更新条码信息表
                    string sqlscan = string.Format("UPDATE dbo.YYK_GU_Current SET IsEndScan=1  WHERE po='{0}' AND Warehouse='{1}' AND DeviceName='{2}' AND Set_Code='{3}'", strpo, stridentification, CSReadPlc.DevName, Set_Code);
                    CsFormShow.GoSqlUpdateInsert(sqlscan);
                    //清除排单表对应数据
                    //int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.YYKMarkingLineUp WHERE FinishPackingNum=ISNULL(FinishPalletizingNum ,0) AND Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", strpo, stridentification, Set_Code));
                    //if (count_v > 0)
                    //{
                    //    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.YYKGUSchedulingTable   WHERE guid ='{0}'", dt.Rows[0]["Scheduling_guid"].ToString().Trim()));
                    //}
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE a SET packingqty=CASE when (b.Quantity-ISNULL(b.packingtotal,0))<0 THEN 0 ELSE (b.Quantity-ISNULL(b.packingtotal,0)) END FROM dbo.YYKGUSchedulingTable AS a,dbo.YOUYIKUDO AS b WHERE a.po_guid=b.guid AND a.guid='{0}'", dvg.Rows[index_].Cells["guid"].Value.ToString().Trim()));
              
                    //重新加载喷码队列
                    if (M_stcScanData.potype.Contains("GU")|| M_stcScanData.potype.Contains("UNIQLO"))
                    {
                        CsInitialization.UpdateYYKGULineUp();
                        CsInitialization.UpdateYYKGUPalletizingLineUp();
                    }
                    //重新加载扫码队列信息
                    CsGetData.UpdataScanLineUp();//更新扫码队列
                    CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return b_v;
        }
        /// <summary>
        /// 优衣库GU强制切单后操作
        /// </summary>
        public static bool YYKGUCompelChange(string po_, string Warehouse_, string Set_Code_)
        {
            bool b_v = true;
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    if (!CsRWPlc.mCdata.IsConnect())
                    {
                        MessageBox.Show("请先启动设备再切换单");
                        return false;
                    }
                    // DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.*, b.* FROM   (SELECT guid AS Scheduling_guid, po_guid, packingqty, IsUnpacking, MarkingIsshieid, PrintIsshieid, GAPIsPrintRun, IsPalletizing, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, IsScanRun, HookDirection,TiaoMaVal AS Scheduling_TiaoMaVal FROM dbo.YYKGUSchedulingTable  ) AS a INNER JOIN dbo.YOUYIKUDO AS b ON a.po_guid = b.guid WHERE Scheduling_guid='{0}'", dvg.Rows[index_].Cells["guid"].Value.ToString().Trim()));
                    DataTable dt_line = CsFormShow.GoSqlSelect(string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND Set_Code='{1}' AND Warehouse='{2}'", po_, Set_Code_, Warehouse_));
                    if (dt_line.Rows.Count > 0)
                    {
                        string potype = dt_line.Rows[0]["potype"].ToString().Trim();
                        string strpo = dt_line.Rows[0]["Order_No"].ToString().Trim();
                        string Warehouse = dt_line.Rows[0]["Warehouse"].ToString().Trim();
                        string Set_Code = dt_line.Rows[0]["Set_Code"].ToString().Trim();
                        string Color_Code = dt_line.Rows[0]["Color_Code"].ToString().Trim();
                        string strPoTiaoMa = dt_line.Rows[0]["SKU_Code"].ToString().Trim();
                        string FinishPackingNum = dt_line.Rows[0]["FinishPackingNum"].ToString().Trim();
                        int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.YYKMarkingLineUp  WHERE FinishPackingNum-finishmarknumber<>0 AND Order_No='{0}' AND Warehouse='{1}'  AND Set_Code='{2}'", strpo, Warehouse, Set_Code));
                        if (count_v > 0)
                        {
                            CsFormShow.MessageBoxFormShow("上一个订单最后一箱喷码未结束不允许切单！");
                            return false;
                        }
                        if (potype.Contains("GU") || potype.Contains("UNIQLO"))
                        {
                            //修改订单装箱总数
                            DataTable dtline = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YYKMarkingLineUp  WHERE Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", strpo, Warehouse, Set_Code));
                            //修改订单装箱总数
                            //string sqlstrdo = "";
                            //sqlstrdo = string.Format("UPDATE a SET packingtotal = finishmarknumber FROM dbo.YOUYIKUDO AS a where Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", strpo, stridentification, Set_Code);
                            //CsFormShow.GoSqlUpdateInsert(sqlstrdo);
                            //修改当前扫码队列的信息
                            string sqlstr2ineup = string.Format("UPDATE a SET CurrentPackingNum=ISNULL(b.FinishPackingNum,0) FROM YYK_GU_Current AS a, dbo.YYKMarkingLineUp AS b  WHERE b.Order_No='{0}'AND b.Warehouse='{1}'  AND b.Set_Code='{2}' AND po=b.Order_No AND a.Warehouse=b.Warehouse  AND a.Set_Code=b.Set_Code", strpo, Warehouse, Set_Code);
                            CsFormShow.GoSqlUpdateInsert(sqlstr2ineup);
                            //修改列队当前喷码总数
                            string sqlstrlineup = string.Format("UPDATE a SET a.CurrentPackingNum=ISNULL(a.FinishPackingNum,0),a.IsEndScan=1,a.IsEndMarking=1  FROM YOUYIKUDO AS b, YYKMarkingLineUp AS a WHERE b.guid = a.YK_guid AND a.Order_No = '{0}' AND a.Warehouse = '{1}' AND a.Set_Code = '{2}'", strpo, Warehouse, Set_Code);
                            CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                            //更新条码信息表
                            string sqlscan = string.Format("UPDATE dbo.YYK_GU_Current SET IsEndScan=1  WHERE po='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", strpo, Warehouse, Set_Code);
                            CsFormShow.GoSqlUpdateInsert(sqlscan);
                            //排单表设置数量更新
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE a SET packingqty=CASE when (b.Quantity-ISNULL(b.packingtotal,0))<0 THEN 0 ELSE (b.Quantity-ISNULL(b.packingtotal,0)) END FROM dbo.SchedulingTable AS a,dbo.YOUYIKUDO AS b WHERE a.po_guid=b.guid AND b.guid='{0}'", dt_line.Rows[0]["YK_guid"].ToString().Trim()));
                            //更新库存数量
                            CsFormShow.ComputeStock(dt_line.Rows[0]["YK_guid"].ToString().Trim());
                            //CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.SchedulingTable WHERE po_guid='{0}'", dt_line.Rows[0]["YK_guid"].ToString().Trim()));
                            Cslogfun.WriteToCartornLog("强制结束订单：" + strpo + ",Set_Code：" + Set_Code + ",Warehouse:" + Warehouse + "，本次实际设置数量：" + FinishPackingNum);
                        }
                        //重新加载扫码队列信息
                        CsGetData.UpdataScanLineUp();//更新扫码队列
                        CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                        //重新加载喷码队列
                        CsInitialization.UpdateYYKGULineUp();
                        CsInitialization.UpdateYYKGUPalletizingLineUp();
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return b_v;
        }
        /// <summary>
        /// 优衣库GU异常结束删除后操作
        /// </summary>
        public static bool YYKGUUnusualCompelChange(DataGridView dvg, int index_)
        {
            bool b_v = true;
            try
            {
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return false;
                }
                bool b_line = CsFormShow.IsExistenceInSql(string.Format("(SELECT*FROM dbo.YYKMarkingLineUp WHERE isnull(DeviceName,'')<>'{0}') as a", CSReadPlc.DevName), "YK_guid", dvg.Rows[index_].Cells["po_guid"].Value.ToString().Trim());
                if (b_line)
                {
                    MessageBox.Show("此单不属于本生产线，不允许操作");
                    return false;
                }
                DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.*, b.* FROM   (SELECT guid AS Scheduling_guid, po_guid, packingqty, IsUnpacking, MarkingIsshieid, PrintIsshieid, GAPIsPrintRun, IsPalletizing, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, IsScanRun, HookDirection,TiaoMaVal AS Scheduling_TiaoMaVal,potype FROM dbo.YYKGUSchedulingTable  ) AS a INNER JOIN dbo.YOUYIKUDO AS b ON a.po_guid = b.guid WHERE Scheduling_guid='{0}'", dvg.Rows[index_].Cells["guid"].Value.ToString().Trim()));
                string potype = dt.Rows[0]["potype"].ToString().Trim();
                string strpo = dt.Rows[0]["Order_No"].ToString().Trim();
                string stridentification = dt.Rows[0]["Warehouse"].ToString().Trim();
                string Set_Code = dt.Rows[0]["Set_Code"].ToString().Trim();
                string Color_Code = dt.Rows[0]["Color_Code"].ToString().Trim();
                string strPoTiaoMa = dt.Rows[0]["SKU_Code"].ToString().Trim();
                if (potype.Contains("GU") || potype.Contains("UNIQLO"))
                {
                    //修改订单装箱总数
                    DataTable dtline = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YYKMarkingLineUp  WHERE Order_No='{0}'AND SKU_Code='{1}' AND Warehouse='{2}' AND potype='{3}'AND Color_Code='{4}' AND Set_Code='{5}'", strpo, strPoTiaoMa, stridentification, potype, Color_Code, Set_Code));
                    //修改订单装箱总数
                    string sqlstrdo = "";
                    sqlstrdo = string.Format("UPDATE a SET packingtotal = finishmarknumber FROM dbo.YOUYIKUDO AS a where Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", strpo, stridentification, Set_Code);
                    CsFormShow.GoSqlUpdateInsert(sqlstrdo);
                    //修改当前扫码队列的信息
                    string sqlstr2ineup = string.Format("UPDATE a SET CurrentPackingNum=ISNULL(b.FinishPackingNum,0),IsEndScan=1  FROM YYK_GU_Current AS a, dbo.YYKMarkingLineUp AS b  WHERE b.Order_No='{0}'AND b.Warehouse='{1}' AND b.Color_Code='{2}' AND b.Set_Code='{3}' AND po=b.Order_No AND a.Warehouse=b.Warehouse  AND a.Set_Code=b.Set_Code", strpo, stridentification, Color_Code, Set_Code);
                    CsFormShow.GoSqlUpdateInsert(sqlstr2ineup);
                    //清除本次队列
                    string sqlstrlineup = string.Format("DELETE FROM  dbo.YYKMarkingLineUp WHERE Order_No='{0}'AND SKU_Code='{1}' AND Warehouse='{2}' AND potype='{3}'AND Color_Code='{4}' AND Set_Code='{5}'", strpo, strPoTiaoMa, stridentification, potype, Color_Code, Set_Code);
                    CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM  dbo.PalletizingtypeTable  WHERE potype = '{0}' AND DeviceName='{1}'", potype, CSReadPlc.DevName.Trim()));
                    //清除排单表对应数据
                    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.YYKGUSchedulingTable   WHERE guid ='{0}'", dt.Rows[0]["Scheduling_guid"].ToString().Trim()));
                    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.SchedulingTable   WHERE guid ='{0}'", dt.Rows[0]["Scheduling_guid"].ToString().Trim()));
                    //重新加载喷码队列
                    CsInitialization.UpdateYYKGULineUp();
                    CsInitialization.UpdateYYKGUPalletizingLineUp();
                    //重新加载扫码队列信息
                    CsGetData.UpdataScanLineUp();//更新扫码队列
                    CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return b_v;
        }
        /// <summary>
        /// 优衣库GU异常结束删除后操作
        /// </summary>
        public static bool YYKGUUnusualCompelChange(string po_, string Warehouse_, string Set_Code_)
        {
            bool b_v = true;
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    if (!CsRWPlc.mCdata.IsConnect())
                    {
                        MessageBox.Show("请先启动设备再切换单");
                        return false;
                    }
                    int num = CsFormShow.GoSqlSelectCount(string.Format("SELECT * FROM dbo.YYKMarkingLineUp   WHERE isnull(DeviceName, '') <> '{0}' AND Order_No = '{1}' AND Warehouse = '{2}'AND Set_Code = '{3}'", CSReadPlc.DevName, po_, Warehouse_, Set_Code_));
                    if (num > 0)
                    {
                        MessageBox.Show("此单不属于本生产线，不允许操作");
                        return false;
                    }
                    // DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.*, b.* FROM   (SELECT guid AS Scheduling_guid, po_guid, packingqty, IsUnpacking, MarkingIsshieid, PrintIsshieid, GAPIsPrintRun, IsPalletizing, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, IsScanRun, HookDirection,TiaoMaVal AS Scheduling_TiaoMaVal FROM dbo.YYKGUSchedulingTable  ) AS a INNER JOIN dbo.YOUYIKUDO AS b ON a.po_guid = b.guid WHERE Scheduling_guid='{0}'", dvg.Rows[index_].Cells["guid"].Value.ToString().Trim()));
                    DataTable dt_line = CsFormShow.GoSqlSelect(string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND Set_Code='{1}' AND Warehouse='{2}'", po_, Set_Code_, Warehouse_));
                    if (dt_line.Rows.Count > 0)
                    {
                        string po_guid = dt_line.Rows[0]["YK_guid"].ToString().Trim();
                        string potype = dt_line.Rows[0]["potype"].ToString().Trim();
                        string strpo = dt_line.Rows[0]["Order_No"].ToString().Trim();
                        string stridentification = dt_line.Rows[0]["Warehouse"].ToString().Trim();
                        string Set_Code = dt_line.Rows[0]["Set_Code"].ToString().Trim();
                        string Color_Code = dt_line.Rows[0]["Color_Code"].ToString().Trim();
                        string strPoTiaoMa = dt_line.Rows[0]["SKU_Code"].ToString().Trim();
                        if (potype.Contains("GU") || potype.Contains("UNIQLO"))
                        {
                            //修改订单装箱总数
                            DataTable dtline = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YYKMarkingLineUp  WHERE Order_No='{0}'AND SKU_Code='{1}' AND Warehouse='{2}' AND potype='{3}'AND Color_Code='{4}' AND Set_Code='{5}'", strpo, strPoTiaoMa, stridentification, potype, Color_Code, Set_Code));
                            //修改订单装箱总数
                            string sqlstrdo = "";
                            sqlstrdo = string.Format("UPDATE a SET packingtotal = finishmarknumber,finishPalletizingnumber=finishmarknumber FROM dbo.YOUYIKUDO AS a where Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", strpo, stridentification, Set_Code);
                            CsFormShow.GoSqlUpdateInsert(sqlstrdo);
                            //修改当前扫码队列的信息
                            string sqlstr2ineup = string.Format("UPDATE a SET CurrentPackingNum=ISNULL(b.FinishPackingNum,0),IsEndScan=1  FROM YYK_GU_Current AS a, dbo.YYKMarkingLineUp AS b  WHERE b.Order_No='{0}'AND b.Warehouse='{1}'  AND b.Set_Code='{2}' AND po=b.Order_No AND a.Warehouse=b.Warehouse  AND a.Set_Code=b.Set_Code", strpo, stridentification, Set_Code);
                            CsFormShow.GoSqlUpdateInsert(sqlstr2ineup);
                            //清除本次队列
                            string sqlstrlineup = string.Format("DELETE FROM  dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", strpo, stridentification, Set_Code);
                            CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                            CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM  dbo.PalletizingtypeTable  WHERE potype = '{0}' AND DeviceName='{1}'", potype, CSReadPlc.DevName.Trim()));
                            //更新库存
                            CsFormShow.ComputeStock(po_guid);
                        }
                    }
                    //重新加载扫码队列信息
                    CsGetData.UpdataScanLineUp();//更新扫码队列
                    CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                    //重新加载喷码队列
                    CsInitialization.UpdateYYKGULineUp();
                    CsInitialization.UpdateYYKGUPalletizingLineUp();
                    //清除排单表对应数据
                    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.SchedulingTable  WHERE 订单号码='{0}' AND Set_Code='{1}' AND Warehouse='{2}'", po_, Set_Code_, Warehouse_));
                }
 
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return b_v;
        }
        /// <summary>
        /// GAP强制切单后操作
        /// </summary>
        public static void GAPCompelChange()
        {
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    if (M_stcScanData.potype.Trim().Contains("GU"))
                    {
                        //修改订单装箱总数
                        string sqlstrdo = string.Format("UPDATE a SET packingtotal = ISNULL(packingtotal, 0) - ISNULL(b.CurrentPackingNum, 0) + ISNULL(b.FinishPackingNum, 0) FROM dbo.YOUYIKUDO AS a, dbo.YYKMarkingLineUp AS b WHERE b.guid IN(SELECT guid FROM dbo.YYKMarkingLineUp AS c WHERE Order_No = '{0}' AND Warehouse = '{1}' AND potype = '{2}' AND Set_Code = '{3}') AND EXISTS(SELECT * FROM dbo.YYKMarkingLineUp AS c WHERE Order_No = a.Order_No AND Warehouse = a.Warehouse  AND Set_Code = a.Set_Code AND Order_No = '{0}' AND Warehouse = '{1}' AND potype = '{2}' AND Set_Code = '{3}') ", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim(), M_stcScanData.potype.Trim(), M_stcScanData.Set_Code);
                        CsFormShow.GoSqlUpdateInsert(sqlstrdo);
                        //修改当前扫码队列的信息
                        string sqlstr2ineup = string.Format("UPDATE a SET CurrentPackingNum=ISNULL(b.FinishPackingNum,0) FROM YYK_GU_Current AS a, dbo.YYKMarkingLineUp AS b  WHERE b.Order_No='{0}'AND b.Warehouse='{1}' AND b.Color_Code='{2}' AND b.Set_Code='{3}' AND po=b.Order_No AND a.Warehouse=b.Warehouse  AND a.Set_Code=b.Set_Code", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim(), M_stcScanData.Color_Code, M_stcScanData.Set_Code);
                        CsFormShow.GoSqlUpdateInsert(sqlstr2ineup);
                        //修改列队当前喷码总数
                        string sqlstrlineup = string.Format("UPDATE dbo.YYKMarkingLineUp SET CurrentPackingNum=ISNULL(FinishPackingNum,0)  WHERE Order_No='{0}'AND SKU_Code='{1}' AND Warehouse='{2}' AND potype='{3}'AND Color_Code='{4}' AND Set_Code='{5}'", M_stcScanData.strpo.Trim(), M_stcScanData.strPoTiaoMa.Trim(), M_stcScanData.stridentification.Trim(), M_stcScanData.potype.Trim(), M_stcScanData.Color_Code, M_stcScanData.Set_Code);
                        CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                        //重新加载喷码队列
                        CsInitialization.UpdateYYKGULineUp();
                        //重新加载扫码队列信息
                        CsGetData.UpdataScanLineUp();//更新扫码队列
                        CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                    }
                    else
                    {
                        //修改订单装箱总数
                        string sqlstrdo = string.Format("UPDATE a SET packingtotal=ISNULL(packingtotal,0)-ISNULL(b.CurrentPackingNum,0)+ISNULL(b.FinishPackingNum,0) FROM dbo.PACKING AS a ,dbo.MarkingLineUp AS b WHERE a.guid=b.packing_guid AND EXISTS(SELECT*FROM dbo.MarkingLineUp AS c WHERE b.guid=c.guid and 订单号码='{0}' AND 从='{1}')", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim());
                        CsFormShow.GoSqlUpdateInsert(sqlstrdo);
                        //修改当前扫码队列的信息
                        string sqlstr2lineup = string.Format("UPDATE a SET CurrentPackingNum=ISNULL(b.FinishPackingNum,0) FROM dbo.GAP_Current AS a,dbo.MarkingLineUp AS b  WHERE a.订单号码='{0}' AND a.从='{1}' AND a.订单号码=b.订单号码 AND a.Sku号码=b.Sku号码 AND a.从=b.从", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim());
                        CsFormShow.GoSqlUpdateInsert(sqlstr2lineup);
                        //修改列队当前喷码总数
                        string sqlstrlineup = string.Format("UPDATE dbo.MarkingLineUp SET CurrentPackingNum=ISNULL(FinishPackingNum,0)  WHERE 订单号码='{0}' AND 从='{1}'", M_stcScanData.strpo.Trim(), M_stcScanData.stridentification.Trim());
                        CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                        //重新加载喷码队列
                        CsInitialization.UpdateGAPLineUp();
                        //重新加载扫码队列信息
                        CsGetData.UpdataScanLineUp();//更新扫码队列
                        CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                    }
                }
               
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// GAP强制切单后操作
        /// </summary>
        public static bool GAPCompelChange(DataGridView dvg, int index_)
        {
            bool b_v = true;
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    if (!CsRWPlc.mCdata.IsConnect())
                    {
                        MessageBox.Show("请先启动设备再切换单");
                        return false;
                    }
                    bool b_line = CsFormShow.IsExistenceInSql(string.Format("(SELECT*FROM dbo.MarkingLineUp WHERE isnull(DeviceName,'')<>'{0}') as a", CSReadPlc.DevName), "packing_guid", dvg.Rows[index_].Cells["po_guid"].Value.ToString().Trim());
                    if (b_line)
                    {
                        MessageBox.Show("此单不属于本生产线，不允许操作");
                        return false;
                    }
                    //string potype = dvg.Rows[index_].Cells["potype"].Value.ToString().Trim();
                    DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", dvg.Rows[index_].Cells["po_guid"].Value.ToString().Trim()));
                    string strpo = dt.Rows[0]["订单号码"].ToString().Trim();
                    string stridentification = dt.Rows[0]["从"].ToString().Trim();
                    string strPoTiaoMa = dt.Rows[0]["Sku号码"].ToString().Trim();
                    DataTable dtline = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE 订单号码='{0}' AND 从='{1}'", strpo, stridentification));
                    //修改订单装箱总数
                    string sqlstrdo = "";
                    sqlstrdo = string.Format("UPDATE a SET  packingtotal =a.finishmarknumber FROM dbo.PACKING AS a WHERE 订单号码 = '{0}' AND 从 = '{1}'", strpo, stridentification);
                    CsFormShow.GoSqlUpdateInsert(sqlstrdo);
                    //修改当前扫码队列的信息
                    string sqlstr2lineup = string.Format("UPDATE a SET CurrentPackingNum=ISNULL(b.FinishPackingNum,0) FROM dbo.GAP_Current AS a,dbo.MarkingLineUp AS b  WHERE a.订单号码='{0}' AND a.从='{1}' AND a.订单号码=b.订单号码 AND a.Sku号码=b.Sku号码 AND a.从=b.从", strpo, stridentification);
                    CsFormShow.GoSqlUpdateInsert(sqlstr2lineup);
                    //修改列队当前喷码总数
                    string sqlstrlineup = string.Format("UPDATE a SET a.CurrentPackingNum = ISNULL(a.FinishPackingNum, 0), a.IsEndScan = 1, a.IsEndMarking = CASE WHEN a.finishmarknumber = a.FinishPackingNum THEN 1 END FROM dbo.PACKING AS b, dbo.MarkingLineUp AS a WHERE b.guid = a.packing_guid AND a.订单号码 = '{0}' AND a.从 = '{1}'", strpo, stridentification);
                    CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                    //清除排单表对应数据
                    //int count_v = CsFormShow.GoSqlSelectCount("SELECT*FROM dbo.MarkingLineUp WHERE FinishPackingNum=ISNULL(FinishPalletizingNum ,0)");
                    //if (count_v>0)
                    //{
                    //    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.GAPSchedulingTable WHERE guid ='{0}'", dvg.Rows[index_].Cells["guid"].Value.ToString().Trim()));
                    //}
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE a SET packingqty=CASE WHEN (b.箱数-ISNULL(b.packingtotal,0))<0 THEN 0 ELSE  (b.箱数-ISNULL(b.packingtotal,0)) END FROM dbo.GAPSchedulingTable AS a,dbo.PACKING AS b WHERE a.po_guid=b.guid AND a.guid='{0}'", dvg.Rows[index_].Cells["guid"].Value.ToString().Trim()));
                    //重新加载喷码队列
                    CsInitialization.UpdateGAPLineUp();
                    //重新加载扫码队列信息
                    CsGetData.UpdataScanLineUp();//更新扫码队列
                    CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                    //更新码垛队列数量
                    CsInitialization.UpdateGAPPalletizingLineUp();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return b_v;
        }
        /// <summary>
        /// GAP强制切单后操作
        /// </summary>
        public static bool GAPCompelChange(string po_, string stridentification_)
        {
            bool b_v = true;
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    if (!CsRWPlc.mCdata.IsConnect())
                    {
                        MessageBox.Show("请先启动设备再切换单");
                        return false;
                    }
                    DataTable dt_line = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE 订单号码='{0}' AND 从='{1}'", po_, stridentification_));
                    //string potype = dvg.Rows[index_].Cells["potype"].Value.ToString().Trim();
                    //DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", dvg.Rows[index_].Cells["po_guid"].Value.ToString().Trim()));
                    if (dt_line.Rows.Count > 0)
                    {
                        string strpo = dt_line.Rows[0]["订单号码"].ToString().Trim();
                        string stridentification = dt_line.Rows[0]["从"].ToString().Trim();
                        string strPoTiaoMa = dt_line.Rows[0]["Sku号码"].ToString().Trim();
                        string FinishPackingNum = dt_line.Rows[0]["FinishPackingNum"].ToString().Trim();
                        //判断上一单的最后一箱是否结束喷码
                        int count_v = CsFormShow.GoSqlSelectCount(string.Format ("SELECT*FROM dbo.MarkingLineUp WHERE FinishPackingNum-finishmarknumber<>0 AND  订单号码='{0}' AND 从='{1}'", strpo, stridentification));
                        if (count_v>0)
                        {
                            CsFormShow.MessageBoxFormShow("上一单最后一箱喷码未结束不允许切单！");
                            return false;
                        }
                        //
                        //修改订单装箱总数
                        //string sqlstrdo = "";
                        //sqlstrdo = string.Format("UPDATE a SET  packingtotal =a.finishmarknumber FROM dbo.PACKING AS a WHERE 订单号码 = '{0}' AND 从 = '{1}'", strpo, stridentification);
                        //CsFormShow.GoSqlUpdateInsert(sqlstrdo);
                        //修改当前扫码队列的信息
                        string sqlstr2lineup = string.Format("UPDATE a SET CurrentPackingNum=ISNULL(b.FinishPackingNum,0) FROM dbo.GAP_Current AS a,dbo.MarkingLineUp AS b  WHERE a.订单号码='{0}' AND a.从='{1}' AND a.订单号码=b.订单号码 AND a.Sku号码=b.Sku号码 AND a.从=b.从", strpo, stridentification);
                        CsFormShow.GoSqlUpdateInsert(sqlstr2lineup);
                        //修改列队当前喷码总数
                        string sqlstrlineup = string.Format("UPDATE a SET a.CurrentPackingNum = ISNULL(a.FinishPackingNum, 0), a.IsEndScan = 1, a.IsEndMarking =1 FROM  dbo.MarkingLineUp AS a WHERE a.订单号码 = '{0}' AND a.从 = '{1}'", strpo, stridentification);
                        CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                        //清除排单表对应数据
                        //int count_v = CsFormShow.GoSqlSelectCount("SELECT*FROM dbo.MarkingLineUp WHERE FinishPackingNum=ISNULL(FinishPalletizingNum ,0)");
                        //if (count_v>0)
                        //{
                        //    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.GAPSchedulingTable WHERE guid ='{0}'", dvg.Rows[index_].Cells["guid"].Value.ToString().Trim()));
                        //}
                        CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE a SET packingqty=CASE WHEN (b.箱数-ISNULL(b.packingtotal,0))<0 THEN 0 ELSE  (b.箱数-ISNULL(b.packingtotal,0)) END FROM dbo.SchedulingTable AS a,dbo.PACKING AS b WHERE a.po_guid=b.guid AND b.guid='{0}'", dt_line.Rows[0]["packing_guid"].ToString().Trim()));
                        //更新库存数量
                        CsFormShow.ComputeStock(dt_line.Rows[0]["packing_guid"].ToString().Trim());
                        // CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.SchedulingTable WHERE po_guid='{0}'", dt_line.Rows[0]["packing_guid"].ToString().Trim()));
                        Cslogfun.WriteToCartornLog("********************************");
                        Cslogfun.WriteToCartornLog(string.Format("强制结束，订单号码:{0},从:{1},本次实际设置数量:{2}", strpo, stridentification, FinishPackingNum));
                        Cslogfun.WriteToCartornLog("********************************");
                    }      
                    
                    //重新加载扫码队列信息
                    CsGetData.UpdataScanLineUp();//更新扫码队列
                    CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                    //重新加载喷码队列
                    CsInitialization.UpdateGAPLineUp();
                    //更新码垛队列数量
                    CsInitialization.UpdateGAPPalletizingLineUp();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return b_v;
        }
        /// <summary>
        /// GAP异常结束删除后操作
        /// </summary>
        public static bool GAPUnusualCompelChange(DataGridView dvg, int index_)
        {
            bool b_v = true;
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    if (!CsRWPlc.mCdata.IsConnect())
                    {
                        MessageBox.Show("请先启动设备再切换单");
                        return false;
                    }
                    bool b_line = CsFormShow.IsExistenceInSql(string.Format("(SELECT*FROM dbo.MarkingLineUp WHERE isnull(DeviceName,'')<>'{0}') as a", CSReadPlc.DevName), "packing_guid", dvg.Rows[index_].Cells["po_guid"].Value.ToString().Trim());
                    if (b_line)
                    {
                        MessageBox.Show("此单不属于本生产线，不允许操作");
                        return false;
                    }
                    //string potype = dvg.Rows[index_].Cells["potype"].Value.ToString().Trim();
                    DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", dvg.Rows[index_].Cells["po_guid"].Value.ToString().Trim()));
                    string strpo = dt.Rows[0]["订单号码"].ToString().Trim();
                    string stridentification = dt.Rows[0]["从"].ToString().Trim();
                    string strPoTiaoMa = dt.Rows[0]["Sku号码"].ToString().Trim();
                    DataTable dtline = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE 订单号码='{0}' AND 从='{1}'", strpo, stridentification));
                    //修改订单装箱总数
                    string sqlstrdo = "";
                    sqlstrdo = string.Format("UPDATE a SET  packingtotal =a.finishmarknumber FROM dbo.PACKING AS a WHERE 订单号码 = '{0}' AND 从 = '{1}'", strpo, stridentification);
                    CsFormShow.GoSqlUpdateInsert(sqlstrdo);
                    //修改当前扫码队列的信息
                    string sqlstr2lineup = string.Format("UPDATE a SET CurrentPackingNum=ISNULL(b.FinishPackingNum,0) FROM dbo.GAP_Current AS a,dbo.MarkingLineUp AS b  WHERE a.订单号码='{0}' AND a.从='{1}' AND a.订单号码=b.订单号码 AND a.Sku号码=b.Sku号码 AND a.从=b.从", strpo, stridentification);
                    CsFormShow.GoSqlUpdateInsert(sqlstr2lineup);
                    //清除本次队列
                    string sqlstrlineup = string.Format("DELETE FROM  dbo.MarkingLineUp  WHERE 订单号码='{0}' AND 从='{1}'", strpo, stridentification);
                    CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM  dbo.PRINTLineuUp  WHERE po='{0}' ", strpo));
                    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM  dbo.PalletizingtypeTable  WHERE potype LIKE 'GAP'"));
                    //清除排单表对应数据
                    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.GAPSchedulingTable WHERE guid ='{0}'", dvg.Rows[index_].Cells["guid"].Value.ToString().Trim()));
                    //重新加载喷码队列
                    CsInitialization.UpdateGAPLineUp();
                    //重新加载扫码队列信息
                    CsGetData.UpdataScanLineUp();//更新扫码队列
                    CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                    //更新码垛队列数量
                    CsInitialization.UpdateGAPPalletizingLineUp();
                    CsInitialization.UpdatePrintLine();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return b_v;
        }
        /// <summary>
        /// GAP强制切单后操作
        /// </summary>
        public static bool GAPUnusualCompelChange(string po_, string stridentification_)
        {
            bool b_v = true;
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    if (!CsRWPlc.mCdata.IsConnect())
                    {
                        MessageBox.Show("请先启动设备再切换单");
                        return false;
                    }
                    int num = CsFormShow.GoSqlSelectCount(string.Format("SELECT * FROM dbo.MarkingLineUp   WHERE isnull(DeviceName, '') <> '{0}' AND 订单号码 = '{1}' AND 从 = '{2}'", CSReadPlc.DevName, po_, stridentification_));
                    if (num > 0)
                    {
                        MessageBox.Show("此单不属于本生产线，不允许操作");
                        return false;
                    }
                    //string potype = dvg.Rows[index_].Cells["potype"].Value.ToString().Trim();
                    DataTable dt_line = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE 订单号码='{0}' AND 从='{1}'", po_, stridentification_));
                    if (dt_line.Rows.Count > 0)
                    {
                        string po_guid = dt_line.Rows[0]["packing_guid"].ToString().Trim();
                        string strpo = dt_line.Rows[0]["订单号码"].ToString().Trim();
                        string stridentification = dt_line.Rows[0]["从"].ToString().Trim();
                        string strPoTiaoMa = dt_line.Rows[0]["Sku号码"].ToString().Trim();
                        DataTable dtline = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE 订单号码='{0}' AND 从='{1}'", strpo, stridentification));
                        //修改订单装箱总数
                        string sqlstrdo = "";
                        sqlstrdo = string.Format("UPDATE a SET  packingtotal =a.finishmarknumber,finishPalletizingnumber=a.finishmarknumber FROM dbo.PACKING AS a WHERE 订单号码 = '{0}' AND 从 = '{1}'", strpo, stridentification);
                        CsFormShow.GoSqlUpdateInsert(sqlstrdo);
                        //修改当前扫码队列的信息
                        string sqlstr2lineup = string.Format("UPDATE a SET CurrentPackingNum=ISNULL(b.FinishPackingNum,0) FROM dbo.GAP_Current AS a,dbo.MarkingLineUp AS b  WHERE a.订单号码='{0}' AND a.从='{1}' AND a.订单号码=b.订单号码 AND a.Sku号码=b.Sku号码 AND a.从=b.从", strpo, stridentification);
                        CsFormShow.GoSqlUpdateInsert(sqlstr2lineup);
                        //清除本次队列
                        string sqlstrlineup = string.Format("DELETE FROM  dbo.MarkingLineUp  WHERE 订单号码='{0}' AND 从='{1}'", strpo, stridentification);
                        CsFormShow.GoSqlUpdateInsert(sqlstrlineup);
                        CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM  dbo.PRINTLineuUp  WHERE po='{0}' ", strpo));
                        CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM  dbo.PalletizingtypeTable  WHERE potype LIKE 'GAP'"));
                        //更新库存数量
                        CsFormShow.ComputeStock(po_guid);
                    }
                        //重新加载喷码队列
                        CsInitialization.UpdateGAPLineUp();
                        //重新加载扫码队列信息
                        CsGetData.UpdataScanLineUp();//更新扫码队列
                        CsInitialization.UpdateCurrentScan();//获取当前扫码信息
                       //更新码垛队列数量
                        CsInitialization.UpdateGAPPalletizingLineUp();
                    }
                    CsInitialization.UpdatePrintLine();
                    //清除排单表对应数据
                    CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.SchedulingTable WHERE 订单号码='{0}' AND 从='{1}'", po_, stridentification_));
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return b_v;
        }
        /// <summary>
        /// GAP是否第一次执行订单
        /// </summary>
        public bool IsGAP_FirstGoPo(string packing_guid_)
        {
            try
            {
                bool b_v = false;
                int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.PACKING AS a ,dbo.MarkingLineUp AS b WHERE ISNULL(IsEndMarking,0)<>1 AND a.guid=b.packing_guid AND a.guid='{0}' AND ISNULL(a.finishmarknumber,0)<ISNULL(b.Cartoncount,0)", packing_guid_));
                if (count_v > 0)
                {
                    b_v = true;
                }
                return b_v;
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 更新GAP订单喷码完成计数
        /// </summary>
        /// <param name="packingguid_"></param>
        /// <param name="po_"></param>
        /// <returns></returns>
        public static string UpdateGAPMarkingNumber(string packingguid_)
        {
            try
            {
                //更新码垛类型队列和数据库
                CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO dbo.PalletizingtypeTable (  DeviceName, potype, Entrytime,po_guid )VALUES  ( '{0}','{1}','{2}' ,'{3}')", CSReadPlc.DevName, "GAP", DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss") + " " + DateTime.Now.Millisecond.ToString(), packingguid_));
                 CsInitialization.UpdatePalletizingtype();
                //更新订单喷码完成数量并返回完成的总箱数序号
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PACKING SET  finishmarknumber=isnull(finishmarknumber,0)+1,packingtotal=CASE WHEN ISNULL(packingtotal,0)<ISNULL(finishmarknumber,0) THEN finishmarknumber ELSE packingtotal END WHERE guid='{0}'", packingguid_));
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET finishmarknumber=ISNULL(finishmarknumber,0)+1 WHERE packing_guid='{0}'", packingguid_));
                //更新排单表
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.SchedulingTable SET  CurrentPackingNum=isnull(CurrentPackingNum,0)+1 WHERE  po_guid='{0}'", packingguid_));
                //更新码垛队列
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.MarkingLineUp SET Palletizingstarttime='{1}' WHERE packing_guid='{0}'", packingguid_, DateTime.Now.ToString() + " " + DateTime.Now.Millisecond.ToString()));
                //增加到箱号记录
                DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT dbo.PACKING.*,M.GAPIsPrintRun FROM dbo.PACKING LEFT JOIN dbo.MarkingLineUp AS M ON dbo.PACKING.guid=M.packing_guid WHERE dbo.PACKING.guid='{0}'", packingguid_));
                string finishmarknumber_v = dt.Rows[0]["finishmarknumber"].ToString().Trim();
                string GAPIsPrintRun_v = dt.Rows[0]["GAPIsPrintRun"].ToString().Trim();
                if (dt.Rows.Count > 0 /*&& (stcUserData.IsScanBarcode|| GAPIsPrintRun_v.Contains("不运行"))*/)
                {
                    string orderno_v = dt.Rows[0]["订单号码"].ToString().Trim();
                    int CatornNo = int.Parse(finishmarknumber_v) - 1 + int.Parse(dt.Rows[0]["从"].ToString().Trim());
                    int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM CartonNOHistoricRcords WHERE  单号='{0}' AND 箱号='{1}'", dt.Rows[0]["订单号码"].ToString().Trim(), CatornNo));
                    if (count_v>0)
                    {
                        CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM CartonNOHistoricRcords WHERE  单号='{0}' AND 箱号='{1}'", dt.Rows[0]["订单号码"].ToString().Trim(), CatornNo));
                    }
                    CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO dbo.CartonNOHistoricRcords(单号,卷号,箱号,录入时间,DeviceName)VALUES('{0}','{1}','{2}','{3}','{4}')", orderno_v,M_stcScanData.BatchNo_Now, CatornNo, DateTime.Now.ToString("yyyy-MM-dd"), CSReadPlc.DevName));
                    //获取喷码完成数量
                    Cslogfun.WriteToCartornLog("喷码完成一箱，订单："+ orderno_v+",箱号："+ CatornNo);
                }
                return finishmarknumber_v;
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.ToString());
            }
            return "";
        }
        /// <summary>
        /// 获取GAP订单喷码完成计数
        /// </summary>
        /// <param name="packingguid_"></param>
        /// <param name="po_"></param>
        /// <returns></returns>
        public static string GetGAPMarkingNumber(string packingguid_)
        {
            try
            {
                if (!string.IsNullOrEmpty(packingguid_))
                {
                    DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT finishmarknumber FROM dbo.PACKING WHERE guid='{0}' ", packingguid_));
                    if (dt.Rows.Count > 0)
                    {
                        return dt.Rows[0]["finishmarknumber"].ToString().Trim();
                    }
                }
                else
                {
                    CsFormShow.MessageBoxFormShow("没有喷码订单数据");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return "";
        }
        /// <summary>
        /// 获取GAP订单码垛完成计数
        /// </summary>
        /// <param name="packingguid_"></param>
        /// <param name="po_"></param>
        /// <returns></returns>
        public static string GetGAPPalletizingNumber(string packingguid_)
        {
            try
            {
                if (!string.IsNullOrEmpty(packingguid_))
                {
                    DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT finishPalletizingnumber FROM dbo.PACKING WHERE guid='{0}' ", packingguid_));
                    if (dt.Rows.Count > 0)
                    {
                        return dt.Rows[0]["finishPalletizingnumber"].ToString().Trim();
                    }
                }
                else
                {
                    CsFormShow.MessageBoxFormShow("没有码垛订单GUID数据");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
               MessageBox.Show("提示：" + err.ToString());
            }
            return "-1";
        }
        /// <summary>
        /// GAP排单切单
        /// </summary>
        /// <param name="dvg_"></param>
        public static void GAPCuttingOrder(DataGridView dvg_)
        {
            try
            {
                //检查设备是否启动
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return;
                }
                bool b_v1 = CsFormShow.IsExistenceInSql("dbo.MarkingLineUp", "packing_guid", dvg_.SelectedRows[0].Cells["po_guid"].Value.ToString().Trim());
                if (b_v1)
                {
                    MessageBox.Show("此订单已经在排单列表中");
                    return;
                }
                if (dvg_.Rows.Count > 0 && dvg_.SelectedRows.Count > 0)
                {
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(dvg_, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        bool b_v = CsGetData.GetGAPMarkingPositionRowData(dvg_.Rows[index_v]);
                        if (b_v)
                        {
                            //string strsql = string.Format(" UPDATE dbo.PACKING SET packingtotal = ISNULL(packingtotal,0)+{0} WHERE guid = '{1}'", int.Parse(qtytBox.Text.ToString().Trim()), Poguid_v);
                            //content1.Select_nothing(strsql);
                            //先判断上一个单是否箱子放完
                            string changetype = IsGAPPackingEnd();
                            if (changetype.Contains("取消切单"))
                            {
                                return;
                            }
                            //else if (changetype.Contains("强制切单"))
                            //{
                            //    // CsGetData.GAPCompelChange();
                            //    return;
                            //}
                            //确认可以切单后操作
                            DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.*, b.*,(SUBSTRING(Sku号码,0,7)+'-'+SUBSTRING(Sku号码,7,2)+'-'+SUBSTRING(Sku号码,9,1)+'-'+SUBSTRING(Sku号码,10,5)) AS sku FROM   (SELECT guid AS Scheduling_guid, po_guid, packingqty, IsUnpacking,IsRepeatCheck, MarkingIsshieid, PrintIsshieid, GAPIsPrintRun,IsRepeatCheck, IsPalletizing, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, IsScanRun, HookDirection,TiaoMaVal AS Scheduling_TiaoMaVal FROM SchedulingTable  ) AS a INNER JOIN dbo.PACKING AS b ON a.po_guid = b.guid WHERE Scheduling_guid='{0}'", dvg_.Rows[index_v].Cells["guid"].Value.ToString().Trim()));
                            CsGetData.GAPCuttingOrderSetToMarking(dt.Rows[0]);//更新本次喷码信息
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 获取纸箱参数
        /// </summary>
        /// <param name="DgvRow"></param>
        /// <returns></returns>
        public static bool GetGAPMarkingPositionRowData(DataGridViewRow DgvRow)
        {
            bool bool_ = false;
            string IsPalletizing = "";
            try
            {
                DialogResult btchose = MessageBox.Show("是否与上一个单一起码垛", "提示", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.Yes)
                {
                    IsPalletizing = "允许";
                }
                else if (btchose == DialogResult.No)
                {
                    IsPalletizing = "不允许";
                }
                else
                {
                    return false;
                }
                string Poguid_ = "";
                // DataSet ds = new DataSet();
                #region 计算箱件数
                string Thick = "";
                string guid_v = DgvRow.Cells["guid"].Value.ToString().Trim();
                Poguid_ = DgvRow.Cells["po_guid"].Value.ToString().Trim();
                //string count_bao = DgvRow.Cells["内包装计数"].Value.ToString().Trim();
                DataTable dt_Scheduling = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM GAPSchedulingTable WHERE guid='{0}'", guid_v));
                DataTable dtLayernum = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", Poguid_));
                string waixiang_code = dtLayernum.Rows[0]["外箱代码"].ToString();
                string IsUnpacking = dt_Scheduling.Rows[0]["IsUnpacking"].ToString().Trim();
                string Partitiontype = dt_Scheduling.Rows[0]["Partitiontype"].ToString().Trim();
                string packingqty = dt_Scheduling.Rows[0]["packingqty"].ToString().Trim();
                // string IsPalletizing = /*dt_Scheduling.Rows[0]["IsPalletizing"].ToString().Trim()*/;
                string IsPackingRun = dt_Scheduling.Rows[0]["IsPackingRun"].ToString().Trim();
                string IsYaMaHaRun = dt_Scheduling.Rows[0]["IsYaMaHaRun"].ToString().Trim();
                string IsSealingRun = dt_Scheduling.Rows[0]["IsSealingRun"].ToString().Trim();
                string MarkingIsshieid = dt_Scheduling.Rows[0]["MarkingIsshieid"].ToString().Trim();
                string PrintIsshieid = dt_Scheduling.Rows[0]["PrintIsshieid"].ToString().Trim();
                string GAPIsPrintRun = dt_Scheduling.Rows[0]["GAPIsPrintRun"].ToString().Trim();
                string GAPSideIsshieid = dt_Scheduling.Rows[0]["GAPSideIsshieid"].ToString().Trim();
                string IsPalletizingRun = dt_Scheduling.Rows[0]["IsPalletizingRun"].ToString().Trim();
                string IsScanRun = dt_Scheduling.Rows[0]["IsScanRun"].ToString().Trim();
                string HookDirection = dt_Scheduling.Rows[0]["HookDirection"].ToString().Trim();
                string Layernum = "";
                if (dtLayernum.Rows.Count > 0)
                {
                    Layernum = CsGetData.GetLayernum(dtLayernum, IsUnpacking, "GAP").ToString();
                }
                #endregion
                if (!string.IsNullOrEmpty(Poguid_))
                {
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE GAPSchedulingTable SET IsPalletizing='{0}' WHERE po_guid='{1}'", IsPalletizing, Poguid_));
                    string selectsql = string.Format("SELECT ISNULL(箱数,0)-ISNULL(finishmarknumber,0) as '箱数', Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '3' THEN '国内' ELSE '国外' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向'  FROM dbo.PACKING, dbo.GAPSchedulingTable  where dbo.PACKING.guid = dbo.GAPSchedulingTable.po_guid and  dbo.PACKING.guid = '{0}'", Poguid_);
                    DataTable dt_position = CsFormShow.GoSqlSelect(selectsql);
                    if (dt_position.Rows.Count == 0)
                    {
                        MessageBox.Show("当前订单没有设置纸箱参数");
                        return bool_;
                    }
                    else
                    {
                        //计算厚度
                        int Height = int.Parse(dt_position.Rows[0]["箱子高"].ToString().Trim());
                        int culumn = int.Parse(dt_position.Rows[0]["列数"].ToString().Trim());
                        Thick = (Height / (int.Parse(Layernum) / culumn)).ToString();
                        //计算隔板类型
                        int catornlong = int.Parse(dt_position.Rows[0]["箱子长"].ToString().Trim());
                        int catornwidth = int.Parse(dt_position.Rows[0]["箱子宽"].ToString().Trim());
                        string PartitionType_v = "";
                        string IsBottomPartition = "";
                        string IsTopPartition = "";
                        CsGetData.GetPartitionData(catornlong, catornwidth, Partitiontype, out PartitionType_v, out IsTopPartition, out IsBottomPartition);
                        CsPakingData.M_DicPakingData["IsTopPartition"] = IsTopPartition;
                        CsPakingData.M_DicPakingData["IsBottomPartition"] = IsBottomPartition;
                        CsPakingData.M_DicPakingData["箱子长"] = dt_position.Rows[0]["箱子长"].ToString().Trim();
                        CsPakingData.M_DicPakingData["箱子宽"] = dt_position.Rows[0]["箱子宽"].ToString().Trim();
                        CsPakingData.M_DicPakingData["箱子高"] = dt_position.Rows[0]["箱子高"].ToString().Trim();
                        CsPakingData.M_DicPakingData["距离底部"] = dt_position.Rows[0]["距离底部"].ToString().Trim();
                        CsPakingData.M_DicPakingData["距离左边"] = dt_position.Rows[0]["距离左边"].ToString().Trim();
                        CsPakingData.M_DicPakingData["重量"] = dt_position.Rows[0]["重量"].ToString().Trim();
                        CsPakingData.M_DicPakingData["产品重量"] = dt_position.Rows[0]["产品重量"].ToString().Trim();
                        CsPakingData.M_DicPakingData["列数"] = dt_position.Rows[0]["列数"].ToString().Trim();
                        CsPakingData.M_DicPakingData["行数"] = dt_position.Rows[0]["行数"].ToString().Trim();
                        CsPakingData.M_DicPakingData["箱内件数"] = Layernum;
                        CsPakingData.M_DicPakingData["箱数"] = packingqty /*dt_position.Rows[0]["箱数"].ToString().Trim()*/;
                        CsPakingData.M_DicPakingData["产品厚度"] = Thick;
                        //CsPakingData.M_DicPakingData["加减速度"] = dt_position.Rows[0]["加减速度"].ToString().Trim();
                        //CsPakingData.M_DicPakingData["抓取速度"] = dt_position.Rows[0]["抓取速度"].ToString().Trim();
                        if (waixiang_code.Contains("C2"))
                        {
                            CsPakingData.M_DicPakingData["喷码模式"] = "5";
                        }
                        else
                        {
                            CsPakingData.M_DicPakingData["喷码模式"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["喷码模式"].ToString().Trim()).ToString();
                        }
                        CsPakingData.M_DicPakingData["纸箱大小"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["是否旋转"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["是否旋转"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["贴标类型"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["贴标类型"].ToString().Trim()).ToString();
                        if (string.IsNullOrEmpty(PartitionType_v))
                        {
                            CsPakingData.M_DicPakingData["PartitionType"] = "0";
                        }
                        else
                        {
                            CsPakingData.M_DicPakingData["隔板类型"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["隔板类型"].ToString().Trim()).ToString();
                        }
                        CsPakingData.M_DicPakingData["正反方向"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["正反方向"].ToString().Trim()).ToString();
                        CsPakingData.M_DicPakingData["IsPalletizing"] = CsMarking.GetCatornParameter(IsPalletizing).ToString();
                        CsPakingData.M_DicPakingData["IsPackingRun"] = CsMarking.GetCatornParameter(IsPackingRun).ToString();
                        CsPakingData.M_DicPakingData["IsYaMaHaRun"] = CsMarking.GetCatornParameter(IsYaMaHaRun).ToString();
                        CsPakingData.M_DicPakingData["IsSealingRun"] = CsMarking.GetCatornParameter(IsSealingRun).ToString();
                        CsPakingData.M_DicPakingData["MarkingIsshieid"] = CsMarking.GetCatornParameter(MarkingIsshieid).ToString();
                        CsPakingData.M_DicPakingData["PrintIsshieid"] = CsMarking.GetCatornParameter(PrintIsshieid).ToString();
                        CsPakingData.M_DicPakingData["GAPIsPrintRun"] = CsMarking.GetCatornParameter(GAPIsPrintRun).ToString();
                        CsPakingData.M_DicPakingData["GAPSideIsshieid"] = CsMarking.GetCatornParameter(GAPSideIsshieid).ToString();
                        CsPakingData.M_DicPakingData["IsPalletizingRun"] = CsMarking.GetCatornParameter(IsPalletizingRun).ToString();
                        CsPakingData.M_DicPakingData["IsScanRun"] = CsMarking.GetCatornParameter(IsScanRun).ToString();
                        CsPakingData.M_DicPakingData["HookDirection"] = CsMarking.GetCatornParameter(HookDirection).ToString();
                        CsPakingData.M_DicPakingData["PoTypeSignal"] = "3";
                        bool_ = true;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return bool_;
        }
        /// <summary>
        /// 判断上一个单装箱完成
        /// </summary>
        /// <returns></returns>
        public static string IsGAPPackingEnd()
        {
            try
            {
                if (!string.IsNullOrEmpty(M_stcScanData.strpo))
                {
                    string sqlstr = "";
                    int count_v = 0;
                    DataTable dt_topline = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM(SELECT  potype,Entrytime FROM dbo.YYKMarkingLineUp WHERE  DeviceName='{0}' UNION ALL SELECT 'GAP' potype, Entrytime FROM dbo.MarkingLineUp WHERE  DeviceName='{0}') AS LINE  ORDER BY LINE.Entrytime", CSReadPlc.DevName));
                    if (dt_topline.Rows.Count > 0)
                    {
                        string potype = dt_topline.Rows[0]["potype"].ToString().Trim();
                        if ((potype.Contains("GU") || potype.Contains("UNIQLO")) && !string.IsNullOrEmpty(CsPakingData.GetMarking_Colorcode("Order_No")))
                        {
                            sqlstr = string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}'AND Warehouse='{1}' AND Set_Code='{2}' AND CurrentPackingNum=isnull(FinishPackingNum,0)", M_stcScanData.strpo, M_stcScanData.stridentification, M_stcScanData.Set_Code);
                            count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", M_stcScanData.strpo, M_stcScanData.stridentification, M_stcScanData.Set_Code));
                        }
                        else if (potype.Contains("GAP") && (!string.IsNullOrEmpty(CsPakingData.GetMarking_Net("订单号码")) || !string.IsNullOrEmpty(CsPakingData.GetMarking_NONet("订单号码"))))
                        {
                            sqlstr = string.Format("SELECT * FROM dbo.MarkingLineUp WHERE  订单号码='{0}'  AND 从='{1}' AND CurrentPackingNum=isnull(FinishPackingNum,0)", M_stcScanData.strpo, M_stcScanData.stridentification);
                            count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT * FROM dbo.MarkingLineUp WHERE  订单号码='{0}'  AND 从='{1}'", M_stcScanData.strpo, M_stcScanData.stridentification));
                        }
                    }
                    if (!string.IsNullOrEmpty(sqlstr) && count_v > 0)
                    {
                        DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                        if (dt.Rows.Count == 0)
                        {
                            #region 不允许强制切单的情况
                            //MessageBox.Show("本次装箱数量未放完不能切换单");
                            //return "取消切单";
                            #endregion
                            #region 允许强制切单的情况
                            DialogResult diachose = MessageBox.Show("本次装箱数量未放完是否强制切单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                            if (diachose == DialogResult.OK)
                            {
                                //MessageBox.Show("请先结束当前订单，切单时停止扫码操作并且将设备调成手动状态");
                                CsGetData.GAPUnusualCompelChange(M_stcScanData.strpo, M_stcScanData.stridentification);
                                return "强制切单";
                            }
                            else if (diachose == DialogResult.Cancel)
                            {
                                return "取消切单";
                            }
                            #endregion
                        }
                    }
                }
                return "正常切单";
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 完成此颜色范围内的喷码后切换到下一批颜色范围的喷码箱子的数据行提取喷码数据
        /// </summary>
        /// <param name="dr"></param>
        public static void SetToYYKMarking(DataRow dr)
        {
            try
            {
                Dictionary<string, string> DicMarking_colorcode = new Dictionary<string, string>();
                //优衣库喷码信息
                DicMarking_colorcode.Clear();//清除之前
                DicMarking_colorcode["Color_Code"] = dr["Color_Code"].ToString().Trim();
                DicMarking_colorcode["Order_No"] = dr["Order_No"].ToString().Trim();
                DicMarking_colorcode["Order_Qty"] = dr["Order_Qty"].ToString().Trim();
                DicMarking_colorcode["Picking_Unit"] = dr["Picking_Unit"].ToString().Trim();
                DicMarking_colorcode["YK_guid"] = dr["YK_guid"].ToString().Trim();
                DicMarking_colorcode["Quantity"] = dr["CurrentPackingNum"].ToString().Trim();
                DicMarking_colorcode["Warehouse"] = dr["Warehouse"].ToString().Trim();
                DicMarking_colorcode["Set_Code"] = dr["Set_Code"].ToString().Trim();
                DicMarking_colorcode["Cartoncount"] = dr["Cartoncount"].ToString().Trim();
                DicMarking_colorcode["qty"] = dr["Cartoncount"].ToString().Trim();
                DicMarking_colorcode["FinishPalletizingNum"] = dr["FinishPalletizingNum"].ToString().Trim();
                DicMarking_colorcode["END"] = "";
                DataTable dt_carton = CsFormShow.GoSqlSelect(string.Format("SELECT yyk.Order_No ,SUM(qty) AS finishmarknumberTotal FROM (SELECT Order_No,Set_Code,(SUM(ISNULL(finishmarknumber,0))/COUNT(*)) AS qty,Warehouse,COUNT(*) AS num FROM dbo.YOUYIKUDO WHERE Order_No='{0}'  GROUP BY Order_No,Set_Code,Warehouse) AS yyk GROUP BY yyk.Order_No", dr["Order_No"].ToString().Trim()));
                DicMarking_colorcode["finishmarknumberTotal"] = (int.Parse(dt_carton.Rows[0]["finishmarknumberTotal"].ToString().Trim()) + 1).ToString();
                DicMarking_colorcode["BatchNo"] = M_stcScanData.BatchNo_Now;
                CsPakingData.M_list_Dic_colorcode.Add(DicMarking_colorcode);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 置为当前时将当前选择的颜色范围的行数据放在当前队列最前面并清空之前的队列
        /// </summary>
        /// <param name="DgvRow"></param>
        public static void GAPCuttingOrderSetToMarking(DataRow dr)
        {
            try
            {
                #region 获取dgv列值
                string po_guid = dr["po_guid"].ToString().Trim();
                string fanwei = dr["范围"].ToString().Trim();
                string startcode = dr["从"].ToString().Trim();
                string endcode = dr["到"].ToString().Trim();
                string qty = dr["数量"].ToString().Trim();
                string inqty = dr["内部包装的项目数量"].ToString().Trim();
                string innum = dr["内包装计数"].ToString().Trim();
                string Cartonqty = dr["箱数"].ToString().Trim();
                string netweight = dr["净重"].ToString().Trim();
                string grossweight = dr["毛重"].ToString().Trim();
                string cartonlong = dr["长"].ToString().Trim();
                string cartonwidth = dr["宽"].ToString().Trim();
                string cartonheight = dr["高"].ToString().Trim();
                string startnum = dr["开始序列号"].ToString().Trim();
                string endnum = dr["截止序列号"].ToString().Trim();
                string baocode = dr["包装代码"].ToString().Trim();
                string wai_code = dr["外箱代码"].ToString().Trim();
                string po = dr["订单号码"].ToString().Trim();
                string Skucode = dr["Sku号码"].ToString().Trim();
                string Color = dr["Color"].ToString().Trim();
                string IsUnpacking = dr["IsUnpacking"].ToString().Trim();
                string Partitiontype = dr["Partitiontype"].ToString().Trim();
                string packingqty = dr["packingqty"].ToString().Trim();
                string IsPalletizing = dr["IsPalletizing"].ToString().Trim();
                string IsPackingRun = dr["IsPackingRun"].ToString().Trim();
                string IsYaMaHaRun = dr["IsYaMaHaRun"].ToString().Trim();
                string IsSealingRun = dr["IsSealingRun"].ToString().Trim();
                string MarkingIsshieid = dr["MarkingIsshieid"].ToString().Trim();
                string PrintIsshieid = dr["PrintIsshieid"].ToString().Trim();
                string GAPIsPrintRun = dr["GAPIsPrintRun"].ToString().Trim();
                string GAPSideIsshieid = dr["GAPSideIsshieid"].ToString().Trim();
                string IsPalletizingRun = dr["IsPalletizingRun"].ToString().Trim();
                string IsScanRun = dr["IsScanRun"].ToString().Trim();
                string HookDirection = dr["HookDirection"].ToString().Trim();
                string TiaoMaVal = dr["Scheduling_TiaoMaVal"].ToString().Trim();
                string IsRepeatCheck_v = dr["IsRepeatCheck"].ToString().Trim();
                string finishmarknum = CsGetData.GetGAPMarkingNumber(dr["guid"].ToString().Trim());
                //string Color_Code = dr["Color_Code"].ToString().Trim();
                //string Set_Code = dr["Set_Code"].ToString().Trim();
                string ScanID = dr["扫描ID"].ToString().Trim();
                string markend = "";
                string labelend = "";
                #endregion
                Dictionary<string, string> DicMarking = new Dictionary<string, string>();
                string sqlstr = string.Format("SELECT*FROM MarkingLineUp WHERE ISNULL(IsEndMarking,0)<>1 and 订单号码='{0}' AND  从='{1}'", po, startcode);
                DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("此单已经存在订单队列中");
                    return;
                }
                else if (string.IsNullOrEmpty(M_stcScanData.BatchNo_Now) && !IsRepeatCheck_v.Contains("不运行"))
                {
                    MessageBox.Show("未扫二维码，请先扫描再切换订单！！");
                    return;
                }
                #region//国外普通单喷码信息
                if (wai_code.ToUpper() != "C4" && wai_code.ToUpper() != "C6")
                {
                    DicMarking.Clear();//清除之前
                    DicMarking["订单号码"] = dr["订单号码"].ToString().Trim().Substring(0, 5) + "    " + dr["订单号码"].ToString().Trim().Substring(5, dr["订单号码"].ToString().Length - 5);
                    //DicMarking["订单号码"] = dr["订单号码"].ToString().Trim().Substring(0, 5);
                    //DicMarking["订单号码2"] = dr["订单号码"].ToString().Trim().Substring(5, 2);
                    if (!string.IsNullOrEmpty(dr["内包装计数"].ToString().Trim()) && dr["内包装计数"].ToString().Trim() != "0")//是多条装一包
                    {
                        DicMarking["SKU/Item"] = dr["包装代码"].ToString().Trim();
                        int Unit_v = int.Parse(dr["数量"].ToString().Trim()) / int.Parse(dr["箱数"].ToString().Trim());
                        DicMarking["Unit/Prepack"] = Unit_v.ToString() + "/" + dr["内包装计数"].ToString().Trim();
                    }
                    else//一条装一包
                    {
                        int len = dr["Sku号码"].ToString().Trim().Length;
                        if (len < 14)
                        {
                            for (int i = 0; i < 14 - len; i++)
                            {
                                dr["Sku号码"] = dr["Sku号码"].ToString().Trim() + "0";
                            }
                        }
                        DicMarking["SKU/Item"] = dr["Sku号码"].ToString().Trim().Substring(0, 9) + "-" + dr["Sku号码"].ToString().Trim().Substring(9, 4);
                        DicMarking["Unit/Prepack"] = dr["内部包装的项目数量"].ToString().Trim() + "/0";
                    }
                    int Carton_v = int.Parse(dr["从"].ToString().Trim()) + int.Parse(finishmarknum);
                    DicMarking["Carton"] = Carton_v + "        " + dr["PoQty"].ToString().Trim(); ;
                    DicMarking["从"] = dr["从"].ToString().Trim();
                    DicMarking["到"] = dr["到"].ToString().Trim();
                    DicMarking["箱数"] = dr["箱数"].ToString().Trim();
                    DicMarking["订单总箱数"] = dr["PoQty"].ToString().Trim();
                    DicMarking["END"] = "";
                    DicMarking["po"] = dr["订单号码"].ToString().Trim();
                    DicMarking["Color"] = dr["Color"].ToString().Trim();
                    DicMarking["Size"] = dr["Size"].ToString().Trim();
                    DicMarking["开始序列号"] = dr["开始序列号"].ToString().Trim();
                    DicMarking["Carton1"] = Carton_v.ToString();
                    DicMarking["Carton2"] = dr["到"].ToString().Trim();
                    DicMarking["Cartoncount"] = "1";
                    DicMarking["CurrentPackingNum"] = packingqty;
                    DicMarking["FinishPalletizingNum"] = "0";
                    DicMarking["扫描ID"] = dr["扫描ID"].ToString().Trim();
                    DicMarking["外箱代码"] = dr["外箱代码"].ToString().Trim();
                    DicMarking["packing_guid"] = dr["guid"].ToString().Trim();
                    DicMarking["长"] = dr["长"].ToString().Trim();
                    DicMarking["宽"] = dr["宽"].ToString().Trim();
                    DicMarking["高"] = dr["高"].ToString().Trim();
                    DicMarking["重量"] = dr["毛重"].ToString().Trim();
                    DicMarking["PrintIsshieid"] = PrintIsshieid;
                    DicMarking["GAPIsPrintRun"] = GAPIsPrintRun;
                    DicMarking["GAPSideIsshieid"] = GAPSideIsshieid;
                    DicMarking["MarkingIsshieid"] = MarkingIsshieid;
                    DicMarking["IsUnpacking"] = IsUnpacking;
                    DicMarking["IsPalletizing"] = IsPalletizing;
                    DicMarking["PartitionType"] = Partitiontype;
                    DicMarking["IsPalletizing"] = CsMarking.GetCatornParameter(IsPalletizing).ToString();
                    DicMarking["packing_guid"] = dr["po_guid"].ToString().Trim();
                    DicMarking["BatchNo"] = M_stcScanData.BatchNo_Now;
                    CsPakingData.M_list_DicMarkingNONet.Add(DicMarking);
                    string insertsql = string.Format("INSERT INTO dbo.MarkingLineUp (packing_guid,范围,从,到,数量,内部包装的项目数量,内包装计数,箱数,净重,毛重,长,宽,高,开始序列号,截止序列号,包装代码,外箱代码,订单号码,Sku号码,Color,扫描ID,喷码完成,贴标完成,Entrytime,Cartoncount,CurrentPackingNum,PrintIsshieid,IsPalletizing,MarkingIsshieid,IsUnpacking,PartitionType,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,TiaoMaVal,PoQty,IsScanRun,GAPIsPrintRun,HookDirection,DeviceName,GAPSideIsshieid,BatchNo) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}')", po_guid, fanwei, startcode, endcode, qty, inqty, innum, Cartonqty, netweight, grossweight, cartonlong, cartonwidth, cartonheight, startnum, endnum, baocode, wai_code, po, Skucode, Color, ScanID, markend, labelend, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), 1, packingqty, PrintIsshieid, IsPalletizing, MarkingIsshieid, IsUnpacking, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, TiaoMaVal, dr["PoQty"].ToString().Trim(), IsScanRun, GAPIsPrintRun, HookDirection, CSReadPlc.DevName.Trim(),GAPSideIsshieid,M_stcScanData.BatchNo_Now);
                    CsFormShow.GoSqlUpdateInsert(insertsql);
                    CsMarking.M_MarkParameter = "NONet";
                }
                #endregion
                #region //国外网单喷码信息
                else if (wai_code.ToUpper() == "C4" || wai_code.ToUpper() == "C6")
                {
                    DicMarking.Clear();//清除之前
                    int Carton_v = int.Parse(dr["从"].ToString().Trim()) + int.Parse(finishmarknum);
                    DicMarking["Carton"] = Carton_v + "              " + dr["PoQty"].ToString().Trim();
                    DicMarking["订单号码"] = dr["订单号码"].ToString().Trim().Substring(0, 5) + "    " + dr["订单号码"].ToString().Trim().Substring(5, dr["订单号码"].ToString().Length - 5);
                    DicMarking["style/size"] = dr["Sku号码"].ToString().Trim().Substring(0, 9)
                    + "        " + dr["Sku号码"].ToString().Trim().Substring(9, 4);
                    if (!string.IsNullOrEmpty(dr["内包装计数"].ToString().Trim()) && dr["内包装计数"].ToString().Trim() != "0")//是多条装一包
                    {
                        int Unit_v = int.Parse(dr["数量"].ToString().Trim()) / int.Parse(dr["箱数"].ToString().Trim());
                        DicMarking["qty"] = Unit_v + "/" + dr["内包装计数"].ToString().Trim();
                    }
                    else//一条装一包
                    {
                        DicMarking["qty"] = dr["内部包装的项目数量"].ToString().Trim();
                    }
                    DicMarking["GrossWeight"] = dr["毛重"].ToString().Trim();
                    DicMarking["NetWeight"] = dr["净重"].ToString().Trim();
                    DicMarking["从"] = dr["从"].ToString().Trim();
                    DicMarking["到"] = dr["到"].ToString().Trim();
                    DicMarking["箱数"] = dr["箱数"].ToString().Trim();
                    DicMarking["订单总箱数"] = dr["PoQty"].ToString().Trim();
                    DicMarking["END"] = "";
                    DicMarking["po"] = dr["订单号码"].ToString().Trim();
                    DicMarking["Color"] = dr["Color"].ToString().Trim();
                    DicMarking["Size"] = dr["Size"].ToString().Trim();
                    DicMarking["StartNum"] = Carton_v.ToString();
                    DicMarking["TiaoMaVal"] = dr["TiaoMaVal"].ToString().Trim();
                    DicMarking["sku"] = dr["sku"].ToString().Trim();
                    DicMarking["开始序列号"] = dr["开始序列号"].ToString().Trim();
                    DicMarking["Carton1"] = Carton_v.ToString();
                    DicMarking["Carton2"] = dr["到"].ToString().Trim();
                    DicMarking["Cartoncount"] = "1";
                    DicMarking["CurrentPackingNum"] = packingqty;
                    DicMarking["FinishPalletizingNum"] = "0";
                    DicMarking["外箱代码"] = dr["外箱代码"].ToString().Trim();
                    DicMarking["packing_guid"] = dr["guid"].ToString().Trim();
                    DicMarking["长"] = dr["长"].ToString().Trim();
                    DicMarking["宽"] = dr["宽"].ToString().Trim();
                    DicMarking["高"] = dr["高"].ToString().Trim();
                    DicMarking["重量"] = dr["毛重"].ToString().Trim();
                    DicMarking["GAPSideStartNum"] = dr["GAPSideStartNum"].ToString().Trim();
                    DicMarking["PrintIsshieid"] = PrintIsshieid;
                    DicMarking["GAPIsPrintRun"] = GAPIsPrintRun;
                    DicMarking["GAPSideIsshieid"] = GAPSideIsshieid;
                    DicMarking["MarkingIsshieid"] = MarkingIsshieid;
                    DicMarking["IsUnpacking"] = IsUnpacking;
                    DicMarking["IsPalletizing"] = IsPalletizing;
                    DicMarking["PartitionType"] = Partitiontype;
                    DicMarking["IsScanRun"] = IsScanRun;
                    DicMarking["HookDirection"] = HookDirection;
                    DicMarking["IsPackingRun"] = IsPackingRun;
                    DicMarking["IsYaMaHaRun"] = IsYaMaHaRun;
                    DicMarking["IsSealingRun"] = IsSealingRun;
                    DicMarking["IsPalletizingRun"] = IsPalletizingRun;
                    DicMarking["packing_guid"] = dr["po_guid"].ToString().Trim();
                    DicMarking["BatchNo"] = M_stcScanData.BatchNo_Now;
                    CsPakingData.M_list_DicMarkingNet.Add(DicMarking);
                    //M_MarkingData.M_PackingCode = "2";
                    string insertsql = string.Format("INSERT INTO dbo.MarkingLineUp (packing_guid,范围,从,到,数量,内部包装的项目数量,内包装计数,箱数,净重,毛重,长,宽,高,开始序列号,截止序列号,包装代码,外箱代码,订单号码,Sku号码,Color,扫描ID,喷码完成,贴标完成,Entrytime,Cartoncount,CurrentPackingNum,PrintIsshieid,MarkingIsshieid,IsUnpacking,IsPalletizing,PartitionType,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,TiaoMaVal,PoQty,IsScanRun,GAPIsPrintRun,HookDirection,DeviceName,GAPSideIsshieid,BatchNo) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}')", po_guid, fanwei, startcode, endcode, qty, inqty, innum, Cartonqty, netweight, grossweight, cartonlong, cartonwidth, cartonheight, startnum, endnum, baocode, wai_code, po, Skucode, Color, ScanID, markend, labelend, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), 1, packingqty, PrintIsshieid, MarkingIsshieid, IsUnpacking, IsPalletizing, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, TiaoMaVal, dr["PoQty"].ToString().Trim(), IsScanRun, GAPIsPrintRun, HookDirection, CSReadPlc.DevName.Trim(),GAPSideIsshieid,M_stcScanData.BatchNo_Now);
                    CsFormShow.GoSqlUpdateInsert(insertsql);
                    //设置喷码参数到plc
                    CsMarking.M_MarkParameter = "Net";
                }
                #endregion
                //初始化排单表计数
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.SchedulingTable SET  CurrentPackingNum=0 WHERE  po_guid='{0}'", po_guid));
                //加入历史记录表
                int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT*FROM dbo.HistoricRcords WHERE po_guid='{0}'", po_guid));
                if (count_v == 0)
                {
                    string gaplineup_guid = CsFormShow.SqlGetVal(string.Format("SELECT*FROM dbo.MarkingLineUp WHERE   订单号码='{0}' AND 从='{1}' ", dr["订单号码"].ToString().Trim(), DicMarking["从"]), "guid");

                    CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO HistoricRcords (po_guid,LineUp_guid,StartTime,Total) VALUES('{0}','{1}','{2}','{3}')", po_guid, gaplineup_guid, DateTime.Now.ToString("yyyy-MM-dd"), dr["箱数"].ToString().Trim()));
                }
                //更新扫码队列
                CsGetData.UpdataScanLineUp();
                string strsql = string.Format(" UPDATE dbo.PACKING SET TiaoMaVal='{1}' WHERE guid = '{2}'", int.Parse(packingqty), TiaoMaVal, po_guid);
                CsFormShow.GoSqlUpdateInsert(strsql);
                M_stcScanData.BatchNo_Now = "";
                MessageBox.Show("切换单完成");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 优衣库GU切单设置
        /// </summary>
        public static void YYKGUCuttingOrder(DataGridView dvgScheduling_)
        {
            try
            {
                //检查设备是否启动
                if (!CsRWPlc.mCdata.IsConnect())
                {
                    MessageBox.Show("请先启动设备再切换单");
                    return;
                }
                bool b_v1 = CsFormShow.IsExistenceInSql("dbo.YYKMarkingLineUp", "YK_guid", dvgScheduling_.SelectedRows[0].Cells["po_guid"].Value.ToString().Trim());
                if (b_v1)
                {
                    MessageBox.Show("此订单已经在订单队列中");
                    return;
                }
                if (dvgScheduling_.Rows.Count > 0 && dvgScheduling_.SelectedRows.Count > 0)
                {
                    bool isindex_v; int index_v;
                    CsFormShow.DvgGetcurrentIndex(dvgScheduling_, out isindex_v, out index_v);
                    if (isindex_v)
                    {
                        bool b_v = CsGetData.GetYYKGUPositionRowData(dvgScheduling_.Rows[index_v]);
                        if (b_v)
                        {
                            #region 先判断上一个单是否箱子放完
                            //string changetype = IsPackingEnd();
                            //if (changetype.Contains("取消切单"))
                            //{
                            //    return;
                            //}
                            #endregion
                            //确认可以切单后操作
                            DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.*, b.* FROM   (SELECT guid AS Scheduling_guid, po_guid, packingqty, IsUnpacking,IsRepeatCheck, MarkingIsshieid, PrintIsshieid, GAPIsPrintRun, IsPalletizing, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, IsScanRun, HookDirection,TiaoMaVal AS Scheduling_TiaoMaVal FROM dbo.YYKGUSchedulingTable  ) AS a INNER JOIN dbo.YOUYIKUDO AS b ON a.po_guid = b.guid WHERE Scheduling_guid='{0}'", dvgScheduling_.Rows[index_v].Cells["guid"].Value.ToString().Trim()));
                            CsGetData.YYKGUCuttingOrderSetToMarking(dt.Rows[0]);

                        }
                    }
                    else
                    {
                        MessageBox.Show("提示：没有可执行的数据");
                        return;
                    }
                }
            }

            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 优衣库GU切单设置
        /// </summary>
        public static bool CuttingOrder(DataGridView dvgScheduling_)
        {
            try
            {
                lock (CSReadPlc.lockScan)
                {
                    //检查设备是否启动
                    if (!CsRWPlc.mCdata.IsConnect())
                    {
                        MessageBox.Show("请先启动设备再切换单");
                        return false;
                    }
                    bool b_v1 = false;
                    if (dvgScheduling_.SelectedRows[0].Cells["potype"].Value.ToString().Trim().Contains("UNIQLO") || dvgScheduling_.SelectedRows[0].Cells["potype"].Value.ToString().Trim().Contains("GU"))
                    {
                        b_v1 = CsFormShow.IsExistenceInSql("dbo.YYKMarkingLineUp", "YK_guid", dvgScheduling_.SelectedRows[0].Cells["po_guid"].Value.ToString().Trim());
                        if (b_v1)
                        {
                            MessageBox.Show("此订单已经在订单队列中");
                            return false;
                        }
                        if (dvgScheduling_.Rows.Count > 0 && dvgScheduling_.SelectedRows.Count > 0)
                        {
                            bool isindex_v; int index_v;
                            CsFormShow.DvgGetcurrentIndex(dvgScheduling_, out isindex_v, out index_v);
                            if (isindex_v)
                            {
                                bool b_v = CsGetData.GetPositionRowData(dvgScheduling_.Rows[index_v]);
                                if (b_v)
                                {
                                    #region 先判断上一个单是否箱子放完
                                    #endregion
                                    //确认可以切单后操作
                                    DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.*, b.* FROM   (SELECT guid AS Scheduling_guid, po_guid, packingqty, IsUnpacking,IsRepeatCheck, MarkingIsshieid, PrintIsshieid, GAPIsPrintRun, IsPalletizing, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, IsScanRun, HookDirection,TiaoMaVal AS Scheduling_TiaoMaVal FROM dbo.SchedulingTable  ) AS a INNER JOIN dbo.YOUYIKUDO AS b ON a.po_guid = b.guid WHERE Scheduling_guid='{0}'", dvgScheduling_.Rows[index_v].Cells["guid"].Value.ToString().Trim()));
                                    CsGetData.YYKGUCuttingOrderSetToMarking(dt.Rows[0]);

                                }
                            }
                            else
                            {
                                MessageBox.Show("提示：没有可执行的数据");
                                return false;
                            }
                        }
                    }
                    else if (dvgScheduling_.SelectedRows[0].Cells["potype"].Value.ToString().Trim().Contains("GAP"))
                    {
                        b_v1 = CsFormShow.IsExistenceInSql("dbo.MarkingLineUp", "packing_guid", dvgScheduling_.SelectedRows[0].Cells["po_guid"].Value.ToString().Trim());
                        if (b_v1)
                        {
                            MessageBox.Show("此订单已经在排单列表中");
                            return false;
                        }
                        if (dvgScheduling_.Rows.Count > 0 && dvgScheduling_.SelectedRows.Count > 0)
                        {
                            bool isindex_v; int index_v;
                            CsFormShow.DvgGetcurrentIndex(dvgScheduling_, out isindex_v, out index_v);
                            if (isindex_v)
                            {
                                bool b_v = CsGetData.GetPositionRowData(dvgScheduling_.Rows[index_v]);
                                if (b_v)
                                {
                                    //先判断上一个单是否箱子放完
                                    //string changetype = IsGAPPackingEnd();
                                    //if (changetype.Contains("取消切单"))
                                    //{
                                    //    return;
                                    //}
                                    //确认可以切单后操作
                                    DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT a.*, b.*,(SUBSTRING(Sku号码,0,7)+'-'+SUBSTRING(Sku号码,7,2)+'-'+SUBSTRING(Sku号码,9,1)+'-'+SUBSTRING(Sku号码,10,5)) AS sku,GAPSideIsshieid FROM   (SELECT guid AS Scheduling_guid, po_guid, packingqty, IsUnpacking,IsRepeatCheck, MarkingIsshieid, PrintIsshieid, GAPIsPrintRun, IsPalletizing, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, IsScanRun, HookDirection,TiaoMaVal AS Scheduling_TiaoMaVal,GAPSideIsshieid FROM SchedulingTable  ) AS a INNER JOIN dbo.PACKING AS b ON a.po_guid = b.guid WHERE Scheduling_guid='{0}'", dvgScheduling_.Rows[index_v].Cells["guid"].Value.ToString().Trim()));
                                    CsGetData.GAPCuttingOrderSetToMarking(dt.Rows[0]);//更新本次喷码信息
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return true;
        }
        /// <summary>
        /// 将当前选择写入PLC
        /// </summary>
        /// <param name="DgvRow"></param>
        /// <returns></returns>
        public static bool GetYYKGUPositionRowData(DataGridViewRow DgvRow)
        {
            bool bool_ = false;
            string IsPalletizing = "";
            try
            {
                DialogResult btchose = MessageBox.Show("是否与上一个单一起码垛", "提示", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.Yes)
                {
                    IsPalletizing = "允许";
                }
                else if (btchose == DialogResult.No)
                {
                    IsPalletizing = "不允许";
                }
                else
                {
                    return false;
                }
                string Poguid_ = DgvRow.Cells["po_guid"].Value.ToString().Trim();
                string Scheduling_guid = DgvRow.Cells["guid"].Value.ToString().Trim();
                #region 计算箱件数
                string Layernum = "";
                string Set_Code = DgvRow.Cells["Set_Code"].Value.ToString().Trim();
                DataTable dtLayernum = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YOUYIKUDO WHERE guid='{0}'", Poguid_));
                DataTable dt_Scheduling = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM YYKGUSchedulingTable WHERE guid='{0}'", Scheduling_guid));
                string IsUnpacking = dt_Scheduling.Rows[0]["IsUnpacking"].ToString().Trim();
                string Partitiontype = dt_Scheduling.Rows[0]["Partitiontype"].ToString().Trim();
                string packingqty = dt_Scheduling.Rows[0]["packingqty"].ToString().Trim();
                // string IsPalletizing = dt_Scheduling.Rows[0]["IsPalletizing"].ToString().Trim();
                string IsPackingRun = dt_Scheduling.Rows[0]["IsPackingRun"].ToString().Trim();
                string IsYaMaHaRun = dt_Scheduling.Rows[0]["IsYaMaHaRun"].ToString().Trim();
                string IsSealingRun = dt_Scheduling.Rows[0]["IsSealingRun"].ToString().Trim();
                string MarkingIsshieid = dt_Scheduling.Rows[0]["MarkingIsshieid"].ToString().Trim();
                string PrintIsshieid = dt_Scheduling.Rows[0]["PrintIsshieid"].ToString().Trim();
                string GAPIsPrintRun = dt_Scheduling.Rows[0]["GAPIsPrintRun"].ToString().Trim();
                string IsPalletizingRun = dt_Scheduling.Rows[0]["IsPalletizingRun"].ToString().Trim();
                string IsScanRun = dt_Scheduling.Rows[0]["IsScanRun"].ToString().Trim();
                string HookDirection = dt_Scheduling.Rows[0]["HookDirection"].ToString().Trim();
                if (dtLayernum.Rows.Count > 0)
                {
                    Layernum = CsGetData.GetLayernum(dtLayernum, IsUnpacking, "YYK").ToString();
                }
                #endregion
                if (!string.IsNullOrEmpty(Poguid_))
                {
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.YYKGUSchedulingTable SET IsPalletizing='{0}' WHERE po_guid='{1}'", IsPalletizing, Poguid_));
                    string selectsql = "";
                    selectsql = string.Format("SELECT Quantity, Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '1' THEN '不喷色号' ELSE '喷色号' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向',CASE WHEN  IsRepeatCheck = '1' THEN '是' ELSE '否' END AS '是否再检针',packingqty FROM dbo.YOUYIKUDO, dbo.YYKGUSchedulingTable  where dbo.YOUYIKUDO.guid = dbo.YYKGUSchedulingTable.po_guid and  dbo.YOUYIKUDO.guid='{0}'", Poguid_);
                    DataTable dt = CsFormShow.GoSqlSelect(selectsql);
                    if (dt.Rows.Count == 0)
                    {
                        MessageBox.Show("当前订单没有设置纸箱参数");
                        return bool_;
                    }
                    else
                    {
                        CsPakingData.M_DicMarking_colorcode.Clear();
                        //计算厚度
                        int Height = int.Parse(dt.Rows[0]["箱子高"].ToString().Trim());
                        int culumn = int.Parse(dt.Rows[0]["列数"].ToString().Trim());
                        string Thick = (Height / (int.Parse(Layernum) / culumn)).ToString();
                        //计算隔板类型
                        int catornlong = int.Parse(dt.Rows[0]["箱子长"].ToString().Trim());
                        int catornwidth = int.Parse(dt.Rows[0]["箱子宽"].ToString().Trim());
                        string PartitionType = "";
                        string IsBottomPartition = "";
                        string IsTopPartition = "";
                        CsGetData.GetPartitionData(catornlong, catornwidth, Partitiontype, out PartitionType, out IsTopPartition, out IsBottomPartition);
                        CsPakingData.M_DicMarking_colorcode["IsTopPartition"] = IsTopPartition;
                        CsPakingData.M_DicMarking_colorcode["IsBottomPartition"] = IsBottomPartition;
                        CsPakingData.M_DicMarking_colorcode["Long"] = dt.Rows[0]["箱子长"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Width"] = dt.Rows[0]["箱子宽"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Heghit"] = dt.Rows[0]["箱子高"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Updown_distance"] = dt.Rows[0]["距离底部"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Leftright_distance"] = dt.Rows[0]["距离左边"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["CartonWeight"] = dt.Rows[0]["重量"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["GoodWeight"] = dt.Rows[0]["产品重量"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["ColumnNunber"] = dt.Rows[0]["列数"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["LineNum"] = dt.Rows[0]["行数"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["Layernum"] = Layernum;
                        CsPakingData.M_DicMarking_colorcode["Thick"] = Thick;
                        //CsPakingData.M_DicMarking_colorcode["Jianspeed"] = dt.Rows[0]["加减速度"].ToString().Trim();
                        //CsPakingData.M_DicMarking_colorcode["Grabspeed"] = dt.Rows[0]["抓取速度"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["MarkPattern"] = CsMarking.GetCatornParameter(dt.Rows[0]["喷码模式"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["CatornType"] = CsMarking.GetCatornParameter(dt.Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsRotate"] = CsMarking.GetCatornParameter(dt.Rows[0]["是否旋转"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["LabelingType"] = CsMarking.GetCatornParameter(dt.Rows[0]["贴标类型"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsRepeatCheck"] = CsMarking.GetCatornParameter(dt.Rows[0]["是否再检针"].ToString().Trim()).ToString();
                        if (string.IsNullOrEmpty(PartitionType))
                        {
                            CsPakingData.M_DicMarking_colorcode["PartitionType"] = "0";
                        }
                        else
                        {
                            CsPakingData.M_DicMarking_colorcode["PartitionType"] = CsMarking.GetCatornParameter(PartitionType).ToString();
                        }
                        CsPakingData.M_DicMarking_colorcode["Orientation"] = CsMarking.GetCatornParameter(dt.Rows[0]["正反方向"].ToString().Trim()).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsPalletizing"] = CsMarking.GetCatornParameter(IsPalletizing).ToString();
                        CsPakingData.M_DicMarking_colorcode["Quantity"] = dt.Rows[0]["packingqty"].ToString().Trim();
                        CsPakingData.M_DicMarking_colorcode["IsPackingRun"] = CsMarking.GetCatornParameter(IsPackingRun).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsYaMaHaRun"] = CsMarking.GetCatornParameter(IsYaMaHaRun).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsSealingRun"] = CsMarking.GetCatornParameter(IsSealingRun).ToString();
                        CsPakingData.M_DicMarking_colorcode["MarkingIsshieid"] = CsMarking.GetCatornParameter(MarkingIsshieid).ToString();
                        CsPakingData.M_DicMarking_colorcode["PrintIsshieid"] = CsMarking.GetCatornParameter(PrintIsshieid).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsPalletizingRun"] = CsMarking.GetCatornParameter(IsPalletizingRun).ToString();
                        CsPakingData.M_DicMarking_colorcode["IsScanRun"] = CsMarking.GetCatornParameter(IsScanRun).ToString();
                        CsPakingData.M_DicMarking_colorcode["HookDirection"] = CsMarking.GetCatornParameter(HookDirection).ToString();
                        //  CsPakingData.M_DicMarking_colorcode["装箱单guid"] = dt.Rows[0]["guid"].ToString().Trim();
                        bool_ = true;
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return bool_;
        }
        /// <summary>
        /// 将当前选择写入PLC
        /// </summary>
        /// <param name="DgvRow"></param>
        /// <returns></returns>
        public static bool GetPositionRowData(DataGridViewRow DgvRow)
        {
            bool bool_ = false;
            string IsPalletizing = "";
            string potype = DgvRow.Cells["potype"].Value.ToString().Trim();
            string Poguid_ = DgvRow.Cells["po_guid"].Value.ToString().Trim();
            string Scheduling_guid = DgvRow.Cells["guid"].Value.ToString().Trim();
            string Long_v = DgvRow.Cells["Long"].Value.ToString().Trim() + "0";
            string Width_v = DgvRow.Cells["Width"].Value.ToString().Trim() + "0";
            string Heghit_v = DgvRow.Cells["Heghit"].Value.ToString().Trim() + "0";
            DataTable dt_Scheduling = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM SchedulingTable WHERE guid='{0}'", Scheduling_guid));
            try
            {
                DialogResult btchose = MessageBox.Show("是否与上一个单一起码垛", "提示", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (btchose == DialogResult.Yes)
                {
                    IsPalletizing = "允许";
                    if (Long_v != CsSendYaMaHa.PoData.M_strNowLong || Width_v != CsSendYaMaHa.PoData.M_strNowWide || Heghit_v != CsSendYaMaHa.PoData.M_strNowHeghit)
                    {
                        IsPalletizing = "不允许";
                        MessageBox.Show("尺寸与上一箱不同不允许码垛在一起");
                    }
                }
                else if (btchose == DialogResult.No)
                {
                    IsPalletizing = "不允许";
                }
                else
                {
                    return false;
                }
                if (potype.Contains("UNIQLO") || potype.Contains("GU"))
                {
                    DataTable dtLayernum = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.YOUYIKUDO WHERE guid='{0}'", Poguid_));
                    #region 计算箱件数
                    string Layernum = "";
                    string Sample_Code = "";
                    string Set_Code = DgvRow.Cells["Set_Code"].Value.ToString().Trim();
                    string IsUnpacking = dt_Scheduling.Rows[0]["IsUnpacking"].ToString().Trim();
                    string Partitiontype = dt_Scheduling.Rows[0]["Partitiontype"].ToString().Trim();
                    string packingqty = dt_Scheduling.Rows[0]["packingqty"].ToString().Trim();
                    // string IsPalletizing = dt_Scheduling.Rows[0]["IsPalletizing"].ToString().Trim();
                    string IsPackingRun = dt_Scheduling.Rows[0]["IsPackingRun"].ToString().Trim();
                    string IsYaMaHaRun = dt_Scheduling.Rows[0]["IsYaMaHaRun"].ToString().Trim();
                    string IsSealingRun = dt_Scheduling.Rows[0]["IsSealingRun"].ToString().Trim();
                    string MarkingIsshieid = dt_Scheduling.Rows[0]["MarkingIsshieid"].ToString().Trim();
                    string PrintIsshieid = dt_Scheduling.Rows[0]["PrintIsshieid"].ToString().Trim();
                    string GAPIsPrintRun = dt_Scheduling.Rows[0]["GAPIsPrintRun"].ToString().Trim();
                    string IsPalletizingRun = dt_Scheduling.Rows[0]["IsPalletizingRun"].ToString().Trim();
                    string IsScanRun = dt_Scheduling.Rows[0]["IsScanRun"].ToString().Trim();
                    string HookDirection = dt_Scheduling.Rows[0]["HookDirection"].ToString().Trim();
                    #endregion
                    if (!string.IsNullOrEmpty(Poguid_))
                    {
                        string selectsql = "";
                        selectsql = string.Format("SELECT Quantity, Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '1' THEN '不喷色号' ELSE '喷色号' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向',CASE WHEN  IsRepeatCheck = '1' THEN '是' ELSE '否' END AS '是否再检针',Layernum,Thick,读取条码模式,packingqty FROM dbo.YOUYIKUDO, dbo.SchedulingTable  where dbo.YOUYIKUDO.guid = SchedulingTable.po_guid and  dbo.YOUYIKUDO.guid='{0}'", Poguid_);
                        DataTable dt = CsFormShow.GoSqlSelect(selectsql);
                        if (dt.Rows.Count == 0)
                        {
                            MessageBox.Show("当前订单没有设置纸箱参数");
                            return bool_;
                        }
                        else
                        {
                            // CsPakingData.M_DicMarking_colorcode.Clear();
                            if (potype.Contains("UNIQLO"))
                            {
                                CsPakingData.M_DicMarking_colorcode["PoTypeSignal"] = "1";
                            }
                            else
                            {
                                CsPakingData.M_DicMarking_colorcode["PoTypeSignal"] = "2";
                            }
                            //计算隔板类型
                            int catornlong = int.Parse(dt.Rows[0]["箱子长"].ToString().Trim());
                            int catornwidth = int.Parse(dt.Rows[0]["箱子宽"].ToString().Trim());
                            string PartitionType = "";
                            string IsBottomPartition = "";
                            string IsTopPartition = "";
                            CsGetData.GetPartitionData(catornlong, catornwidth, Partitiontype, out PartitionType, out IsTopPartition, out IsBottomPartition);
                            CsPakingData.M_DicMarking_colorcode["IsTopPartition"] = IsTopPartition;
                            CsPakingData.M_DicMarking_colorcode["IsBottomPartition"] = IsBottomPartition;
                            CsPakingData.M_DicMarking_colorcode["Long"] = dt.Rows[0]["箱子长"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["Width"] = dt.Rows[0]["箱子宽"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["Heghit"] = dt.Rows[0]["箱子高"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["Updown_distance"] = dt.Rows[0]["距离底部"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["Leftright_distance"] = dt.Rows[0]["距离左边"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["CartonWeight"] = dt.Rows[0]["重量"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["GoodWeight"] = dt.Rows[0]["产品重量"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["ColumnNunber"] = dt.Rows[0]["列数"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["LineNum"] = dt.Rows[0]["行数"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["Layernum"] = dt.Rows[0]["Layernum"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["Thick"] = dt.Rows[0]["Thick"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["读取条码模式"] = dt.Rows[0]["读取条码模式"].ToString().Trim();
                            //CsPakingData.M_DicMarking_colorcode["Jianspeed"] = dt.Rows[0]["加减速度"].ToString().Trim();
                            //CsPakingData.M_DicMarking_colorcode["Grabspeed"] = dt.Rows[0]["抓取速度"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["MarkPattern"] = CsMarking.GetCatornParameter(dt.Rows[0]["喷码模式"].ToString().Trim()).ToString();
                            CsPakingData.M_DicMarking_colorcode["CatornType"] = CsMarking.GetCatornParameter(dt.Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                            CsPakingData.M_DicMarking_colorcode["IsRotate"] = CsMarking.GetCatornParameter(dt.Rows[0]["是否旋转"].ToString().Trim()).ToString();
                            CsPakingData.M_DicMarking_colorcode["LabelingType"] = CsMarking.GetCatornParameter(dt.Rows[0]["贴标类型"].ToString().Trim()).ToString();
                            CsPakingData.M_DicMarking_colorcode["IsRepeatCheck"] = CsMarking.GetCatornParameter(dt.Rows[0]["是否再检针"].ToString().Trim()).ToString();
                            if (string.IsNullOrEmpty(PartitionType))
                            {
                                CsPakingData.M_DicMarking_colorcode["PartitionType"] = "0";
                            }
                            else
                            {
                                CsPakingData.M_DicMarking_colorcode["PartitionType"] = CsMarking.GetCatornParameter(PartitionType).ToString();
                            }
                            CsPakingData.M_DicMarking_colorcode["Orientation"] = CsMarking.GetCatornParameter(dt.Rows[0]["正反方向"].ToString().Trim()).ToString();
                            CsPakingData.M_DicMarking_colorcode["IsPalletizing"] = CsMarking.GetCatornParameter(IsPalletizing).ToString();
                            CsPakingData.M_DicMarking_colorcode["Quantity"] = dt.Rows[0]["packingqty"].ToString().Trim();
                            CsPakingData.M_DicMarking_colorcode["IsPackingRun"] = CsMarking.GetCatornParameter(IsPackingRun).ToString();
                            CsPakingData.M_DicMarking_colorcode["IsYaMaHaRun"] = CsMarking.GetCatornParameter(IsYaMaHaRun).ToString();
                            CsPakingData.M_DicMarking_colorcode["IsSealingRun"] = CsMarking.GetCatornParameter(IsSealingRun).ToString();
                            CsPakingData.M_DicMarking_colorcode["MarkingIsshieid"] = CsMarking.GetCatornParameter(MarkingIsshieid).ToString();
                            CsPakingData.M_DicMarking_colorcode["PrintIsshieid"] = CsMarking.GetCatornParameter(PrintIsshieid).ToString();
                            CsPakingData.M_DicMarking_colorcode["IsPalletizingRun"] = CsMarking.GetCatornParameter(IsPalletizingRun).ToString();
                            CsPakingData.M_DicMarking_colorcode["IsScanRun"] = CsMarking.GetCatornParameter(IsScanRun).ToString();
                            CsPakingData.M_DicMarking_colorcode["HookDirection"] = CsMarking.GetCatornParameter(HookDirection).ToString();
                            //  CsPakingData.M_DicMarking_colorcode["装箱单guid"] = dt.Rows[0]["guid"].ToString().Trim();
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.SchedulingTable SET IsPalletizing='{0}',PoTypeSignal='{1}',ABPartitiontype='{2}' WHERE po_guid='{3}'", IsPalletizing, CsPakingData.M_DicMarking_colorcode["PoTypeSignal"], CsPakingData.M_DicMarking_colorcode["PartitionType"], Poguid_));
                            bool_ = true;
                        }
                    }
                }
                else if (potype.Contains("GAP"))
                {
                    // DataSet ds = new DataSet();
                    #region 计算箱件数
                    string Thick = "";
                    string guid_v = DgvRow.Cells["guid"].Value.ToString().Trim();
                    //string count_bao = DgvRow.Cells["内包装计数"].Value.ToString().Trim();
                    DataTable dtLayernum = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", Poguid_));
                    string waixiang_code = dtLayernum.Rows[0]["外箱代码"].ToString().Trim();
                    string IsUnpacking = dt_Scheduling.Rows[0]["IsUnpacking"].ToString().Trim();
                    string Partitiontype = dt_Scheduling.Rows[0]["Partitiontype"].ToString().Trim();
                    string packingqty = dt_Scheduling.Rows[0]["packingqty"].ToString().Trim();
                    // string IsPalletizing = /*dt_Scheduling.Rows[0]["IsPalletizing"].ToString().Trim()*/;
                    string IsPackingRun = dt_Scheduling.Rows[0]["IsPackingRun"].ToString().Trim();
                    string IsYaMaHaRun = dt_Scheduling.Rows[0]["IsYaMaHaRun"].ToString().Trim();
                    string IsSealingRun = dt_Scheduling.Rows[0]["IsSealingRun"].ToString().Trim();
                    string MarkingIsshieid = dt_Scheduling.Rows[0]["MarkingIsshieid"].ToString().Trim();
                    string PrintIsshieid = dt_Scheduling.Rows[0]["PrintIsshieid"].ToString().Trim();
                    string GAPIsPrintRun = dt_Scheduling.Rows[0]["GAPIsPrintRun"].ToString().Trim();
                    string GAPSideIsshieid = dt_Scheduling.Rows[0]["GAPSideIsshieid"].ToString().Trim();
                    string IsPalletizingRun = dt_Scheduling.Rows[0]["IsPalletizingRun"].ToString().Trim();
                    string IsScanRun = dt_Scheduling.Rows[0]["IsScanRun"].ToString().Trim();
                    string HookDirection = dt_Scheduling.Rows[0]["HookDirection"].ToString().Trim();
                    string startcode_v = dtLayernum.Rows[0]["从"].ToString().Trim();
                    int finishmarknum = int.Parse(CsGetData.GetGAPMarkingNumber(dtLayernum.Rows[0]["guid"].ToString().Trim()));
                    int StartNum = int.Parse(startcode_v) + finishmarknum;
                    #endregion
                    if (!string.IsNullOrEmpty(Poguid_))
                    {
                        string selectsql = string.Format("SELECT ISNULL(箱数,0)-ISNULL(finishmarknumber,0) as '箱数', Long AS '箱子长', Width AS '箱子宽', Heghit AS '箱子高', TntervalTop AS '距离底部', TntervalLeft AS '距离左边', CartonWeight AS '重量', GoodWeight AS'产品重量', ColumnNunber AS '列数', LineNum AS '行数', Layernum AS '箱内件数', Thick AS '产品厚度', CASE WHEN  MarkPattern = '3' THEN '国内' ELSE '国外' END  AS '喷码模式', CASE WHEN  CatornType = '1' THEN '大' ELSE '小' END  AS '纸箱大小', CASE WHEN  IsRotate = '1' THEN '旋转' ELSE '不旋转' END AS '是否旋转', CASE WHEN  LabelingType = '1' THEN '小标签' ELSE '大标签' END  AS '贴标类型', CASE WHEN  PartitionType = '1' THEN 'A隔板' ELSE 'B隔板' END AS '隔板类型', CASE WHEN  Orientation = '1' THEN '反' ELSE '正' END AS '正反方向',Thick,Layernum,读取条码模式  FROM dbo.PACKING, dbo.SchedulingTable  where dbo.PACKING.guid = dbo.SchedulingTable.po_guid and  dbo.PACKING.guid = '{0}'", Poguid_);
                        DataTable dt_position = CsFormShow.GoSqlSelect(selectsql);
                        if (dt_position.Rows.Count == 0)
                        {
                            MessageBox.Show("当前订单没有设置纸箱参数");
                            return bool_;
                        }
                        else
                        {
                            // CsPakingData.M_DicPakingData.Clear();
                            CsPakingData.M_DicPakingData["PoTypeSignal"] = "3";
                            //计算隔板类型
                            int catornlong = int.Parse(dt_position.Rows[0]["箱子长"].ToString().Trim());
                            int catornwidth = int.Parse(dt_position.Rows[0]["箱子宽"].ToString().Trim());
                            string PartitionType_v = "";
                            string IsBottomPartition = "";
                            string IsTopPartition = "";
                            CsGetData.GetPartitionData(catornlong, catornwidth, Partitiontype, out PartitionType_v, out IsTopPartition, out IsBottomPartition);
                            if (string.IsNullOrEmpty(PartitionType_v))
                            {
                                CsPakingData.M_DicPakingData["PartitionType"] = "0";
                            }
                            else
                            {
                                CsPakingData.M_DicPakingData["PartitionType"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["隔板类型"].ToString().Trim()).ToString();
                            }
                            CsPakingData.M_DicPakingData["IsTopPartition"] = IsTopPartition;
                            CsPakingData.M_DicPakingData["IsBottomPartition"] = IsBottomPartition;
                            CsPakingData.M_DicPakingData["箱子长"] = dt_position.Rows[0]["箱子长"].ToString().Trim();
                            CsPakingData.M_DicPakingData["箱子宽"] = dt_position.Rows[0]["箱子宽"].ToString().Trim();
                            CsPakingData.M_DicPakingData["箱子高"] = dt_position.Rows[0]["箱子高"].ToString().Trim();
                            CsPakingData.M_DicPakingData["距离底部"] = dt_position.Rows[0]["距离底部"].ToString().Trim();
                            CsPakingData.M_DicPakingData["距离左边"] = dt_position.Rows[0]["距离左边"].ToString().Trim();
                            CsPakingData.M_DicPakingData["重量"] = dt_position.Rows[0]["重量"].ToString().Trim();
                            CsPakingData.M_DicPakingData["产品重量"] = dt_position.Rows[0]["产品重量"].ToString().Trim();
                            CsPakingData.M_DicPakingData["列数"] = dt_position.Rows[0]["列数"].ToString().Trim();
                            CsPakingData.M_DicPakingData["行数"] = dt_position.Rows[0]["行数"].ToString().Trim();
                            CsPakingData.M_DicPakingData["箱内件数"] = dt_position.Rows[0]["Layernum"].ToString().Trim();
                            CsPakingData.M_DicPakingData["箱数"] = packingqty /*dt_position.Rows[0]["箱数"].ToString().Trim()*/;
                            CsPakingData.M_DicPakingData["产品厚度"] = dt_position.Rows[0]["Thick"].ToString().Trim();
                            CsPakingData.M_DicPakingData["读取条码模式"] = "1";
                            //CsPakingData.M_DicPakingData["加减速度"] = dt_position.Rows[0]["加减速度"].ToString().Trim();
                            //CsPakingData.M_DicPakingData["抓取速度"] = dt_position.Rows[0]["抓取速度"].ToString().Trim();
                            if (waixiang_code.Contains("C2"))
                            {
                                CsPakingData.M_DicPakingData["喷码模式"] = "5";
                            }
                            else
                            {
                                CsPakingData.M_DicPakingData["喷码模式"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["喷码模式"].ToString().Trim()).ToString();
                            }
                            CsPakingData.M_DicPakingData["纸箱大小"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["纸箱大小"].ToString().Trim()).ToString();
                            CsPakingData.M_DicPakingData["是否旋转"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["是否旋转"].ToString().Trim()).ToString();
                            CsPakingData.M_DicPakingData["贴标类型"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["贴标类型"].ToString().Trim()).ToString();
                            CsPakingData.M_DicPakingData["正反方向"] = CsMarking.GetCatornParameter(dt_position.Rows[0]["正反方向"].ToString().Trim()).ToString();
                            CsPakingData.M_DicPakingData["IsPalletizing"] = CsMarking.GetCatornParameter(IsPalletizing).ToString();
                            CsPakingData.M_DicPakingData["IsPackingRun"] = CsMarking.GetCatornParameter(IsPackingRun).ToString();
                            CsPakingData.M_DicPakingData["IsYaMaHaRun"] = CsMarking.GetCatornParameter(IsYaMaHaRun).ToString();
                            CsPakingData.M_DicPakingData["IsSealingRun"] = CsMarking.GetCatornParameter(IsSealingRun).ToString();
                            CsPakingData.M_DicPakingData["MarkingIsshieid"] = CsMarking.GetCatornParameter(MarkingIsshieid).ToString();
                            CsPakingData.M_DicPakingData["PrintIsshieid"] = CsMarking.GetCatornParameter(PrintIsshieid).ToString();
                            CsPakingData.M_DicPakingData["GAPIsPrintRun"] = CsMarking.GetCatornParameter(GAPIsPrintRun).ToString();
                            CsPakingData.M_DicPakingData["GAPSideIsshieid"] = CsMarking.GetCatornParameter(GAPSideIsshieid).ToString();
                            CsPakingData.M_DicPakingData["IsPalletizingRun"] = CsMarking.GetCatornParameter(IsPalletizingRun).ToString();
                            CsPakingData.M_DicPakingData["IsScanRun"] = CsMarking.GetCatornParameter(IsScanRun).ToString();
                            CsPakingData.M_DicPakingData["HookDirection"] = CsMarking.GetCatornParameter(HookDirection).ToString();
                            CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.SchedulingTable SET IsPalletizing='{0}',PoTypeSignal='{1}',ABPartitiontype='{2}',起始箱号='{3}' WHERE po_guid='{4}'", IsPalletizing, CsPakingData.M_DicPakingData["PoTypeSignal"], CsPakingData.M_DicPakingData["PartitionType"], StartNum, Poguid_));
                            bool_ = true;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return bool_;
        }
        /// <summary>
        /// 判断是否上一个单装箱完成
        /// </summary>
        /// <returns></returns>
        public static string IsPackingEnd()
        {
            try
            {
                string sqlstr = "";
                DataTable dt_topline = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1 * FROM(SELECT  potype,Entrytime FROM dbo.YYKMarkingLineUp WHERE  DeviceName='{0}' UNION ALL SELECT 'GAP' potype, Entrytime FROM dbo.MarkingLineUp WHERE  DeviceName='{0}') AS LINE  ORDER BY LINE.Entrytime", CSReadPlc.DevName));
                if (dt_topline.Rows.Count > 0)
                {
                    string potype = dt_topline.Rows[0]["potype"].ToString().Trim();
                    if (potype.Contains("GU") || potype.Contains("UNIQLO"))
                    {
                        int datacount = CsFormShow.GoSqlSelectCount(string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", M_stcScanData.strpo, M_stcScanData.stridentification, M_stcScanData.Set_Code));
                        if (datacount > 0)
                        {
                            sqlstr = string.Format("SELECT * FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND Warehouse='{1}' AND CurrentPackingNum=isnull(FinishPackingNum,0) AND Set_Code='{2}'", M_stcScanData.strpo, M_stcScanData.stridentification, M_stcScanData.Set_Code);
                        }
                    }
                    else if (potype.Contains("GAP"))
                    {
                        int datacount = CsFormShow.GoSqlSelectCount(string.Format("SELECT * FROM dbo.MarkingLineUp WHERE  订单号码='{0}'  AND 从='{1}' ", M_stcScanData.strpo, M_stcScanData.stridentification));
                        if (datacount > 0)
                        {
                            sqlstr = string.Format("SELECT * FROM dbo.MarkingLineUp WHERE  订单号码='{0}'  AND 从='{1}' AND CurrentPackingNum=isnull(FinishPackingNum,0)", M_stcScanData.strpo, M_stcScanData.stridentification);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(sqlstr))
                {
                    DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                    if (dt.Rows.Count == 0)
                    {
                        #region 不允许强制切单的情况
                        //MessageBox.Show("本次装箱数量未放完不能切换单");
                        //return "取消切单";
                        #endregion
                        #region 允许强制切单的情况
                        DialogResult diachose = MessageBox.Show("本次装箱数量未放完是否强制切单", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (diachose == DialogResult.OK)
                        {
                            //MessageBox.Show("请先结束当前订单");
                            CsGetData.YYKGUCompelChange(M_stcScanData.strpo, M_stcScanData.stridentification, M_stcScanData.Set_Code);
                            return "强制切单";
                        }
                        else if (diachose == DialogResult.Cancel)
                        {
                            return "取消切单";
                        }
                        #endregion
                    }
                    //string isendscan = IsScanEnd();
                    //if (isendscan.Contains("强制切单"))
                    //{
                    //    return "强制切单";
                    //}
                    //else if (isendscan.Contains("取消切单"))
                    //{
                    //    return "取消切单";
                    //}
                }
                return "正常切单";
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 置为当前时将当前选择的颜色范围的行数据放在当前队列最前面并清空之前的队列
        /// </summary>
        /// <param name="DgvRow"></param>
        public static void YYKGUCuttingOrderSetToMarking(DataRow dr)
        {
            try
            {
                string Order_No = dr["Order_No"].ToString().Trim();
                string Color_Code = dr["Color_Code"].ToString().Trim();
                string SKU_Code = dr["SKU_Code"].ToString().Trim();
                string Warehouse = dr["Warehouse"].ToString().Trim();
                string Set_Code = dr["Set_Code"].ToString().Trim();
                string Po_guid = dr["po_guid"].ToString().Trim();
                string packingqty = dr["packingqty"].ToString().Trim();
                string MarkingIsshieid = dr["MarkingIsshieid"].ToString().Trim();
                string IsUnpacking = dr["IsUnpacking"].ToString().Trim();
                string IsPalletizing = dr["IsPalletizing"].ToString().Trim();
                string Partitiontype = dr["Partitiontype"].ToString().Trim();
                string IsPackingRun = dr["IsPackingRun"].ToString().Trim();
                string IsYaMaHaRun = dr["IsYaMaHaRun"].ToString().Trim();
                string IsSealingRun = dr["IsSealingRun"].ToString().Trim();
                string IsPalletizingRun = dr["IsPalletizingRun"].ToString().Trim();
                string PrintIsshieid = dr["PrintIsshieid"].ToString().Trim();
                string IsScanRun = dr["IsScanRun"].ToString().Trim();
                string HookDirection = dr["HookDirection"].ToString().Trim();
                string potype_v = dr["potype"].ToString().Trim();
                string IsRepeatCheck_v = dr["IsRepeatCheck"].ToString().Trim();
                string sqlselect = string.Format("select*from dbo.YYKMarkingLineUp where Order_No='{0}'and Warehouse='{1}' and Set_Code='{2}' ", Order_No, Warehouse, Set_Code);
                DataTable dt = CsFormShow.GoSqlSelect(sqlselect);
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("当前订单已经在队列中");
                    return;
                }
                else if (string.IsNullOrEmpty(M_stcScanData.BatchNo_Now)&& !IsRepeatCheck_v.Contains("不运行"))
                {
                    MessageBox.Show("未扫二维码，请先扫描再切换订单！！");
                    return;
                }
                Dictionary<string, string> DicMarking_colorcode = new Dictionary<string, string>();
                //优衣库喷码信息
                DicMarking_colorcode.Clear();
                DicMarking_colorcode["Color_Code"] = dr["Color_Code"].ToString().Trim();
                DicMarking_colorcode["Order_No"] = dr["Order_No"].ToString().Trim();
                DicMarking_colorcode["Quantity"] = packingqty;
                DicMarking_colorcode["YK_guid"] = dr["po_guid"].ToString().Trim();
                DicMarking_colorcode["SKU_Code"] = dr["SKU_Code"].ToString().Trim();
                DicMarking_colorcode["Warehouse"] = dr["Warehouse"].ToString().Trim();
                DicMarking_colorcode["Set_Code"] = dr["Set_Code"].ToString().Trim();
                DicMarking_colorcode["Qty_per_Set"] = dr["Qty_per_Set"].ToString().Trim();
                DicMarking_colorcode["Picking_Unit"] = dr["Picking_Unit"].ToString().Trim();
                DicMarking_colorcode["qty"] = "1";
                DicMarking_colorcode["FinishPalletizingNum"] = "0";
                DicMarking_colorcode["END"] = "";
                DicMarking_colorcode["IsPalletizing"] = CsMarking.GetCatornParameter(IsPalletizing).ToString();
                DataTable dt_carton = CsFormShow.GoSqlSelect(string.Format("SELECT yyk.Order_No ,SUM(qty) AS finishmarknumberTotal FROM (SELECT Order_No,Set_Code,(SUM(ISNULL(finishmarknumber,0))/COUNT(*)) AS qty,Warehouse,COUNT(*) AS num FROM dbo.YOUYIKUDO WHERE Order_No='{0}'  GROUP BY Order_No,Set_Code,Warehouse) AS yyk GROUP BY yyk.Order_No", dr["Order_No"].ToString().Trim()));
                DicMarking_colorcode["finishmarknumberTotal"] = (int.Parse(dt_carton.Rows[0]["finishmarknumberTotal"].ToString().Trim()) + 1).ToString();
                DicMarking_colorcode["BatchNo"] = M_stcScanData.BatchNo_Now;
                CsPakingData.M_list_Dic_colorcode.Add(DicMarking_colorcode);
                //订单队列录入数据库
                string sqlstr = string.Format("INSERT INTO YYKMarkingLineUp (YK_guid,Order_No,Color_Code,Order_Qty,Entrytime,SKU_Code,Warehouse,potype,CurrentPackingNum,Cartoncount,Set_Code,MarkingIsshieid,IsUnpacking,Qty_per_Set,Picking_Unit,IsPalletizing,PartitionType,IsPackingRun,IsYaMaHaRun,IsSealingRun,IsPalletizingRun,PrintIsshieid,IsScanRun,HookDirection,DeviceName,BatchNo) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}')", DicMarking_colorcode["YK_guid"], DicMarking_colorcode["Order_No"], DicMarking_colorcode["Color_Code"], packingqty, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + DateTime.Now.Millisecond.ToString(), DicMarking_colorcode["SKU_Code"], DicMarking_colorcode["Warehouse"], potype_v, packingqty, 1, DicMarking_colorcode["Set_Code"], MarkingIsshieid, IsUnpacking, DicMarking_colorcode["Qty_per_Set"], DicMarking_colorcode["Picking_Unit"], IsPalletizing, Partitiontype, IsPackingRun, IsYaMaHaRun, IsSealingRun, IsPalletizingRun, PrintIsshieid, IsScanRun, HookDirection, CSReadPlc.DevName, M_stcScanData.BatchNo_Now);
                CsFormShow.GoSqlUpdateInsert(sqlstr);
                //初始化当前计数
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.SchedulingTable SET  CurrentPackingNum=0 WHERE  订单号码 = '{0}'  AND Set_Code = '{1}'AND Warehouse = '{2}'", Order_No, Set_Code, Warehouse));
                //加入历史记录表
                int count_v = CsFormShow.GoSqlSelectCount(string.Format("select*from dbo.YYKGUHistoricRcords where  Order_No='{0}' and Warehouse='{1}' and Set_Code='{2}'", Order_No, Warehouse, Set_Code));
                if (count_v == 0)
                {
                    string yyklineup_guid = CsFormShow.SqlGetVal(string.Format("SELECT*FROM dbo.YYKMarkingLineUp WHERE Order_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", DicMarking_colorcode["Order_No"], DicMarking_colorcode["Warehouse"], DicMarking_colorcode["Set_Code"]), "guid");

                    CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO YYKGUHistoricRcords(po_guid, LineUp_guid, Order_No, Set_Code, Warehouse, StartTime, Total) VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", Po_guid, yyklineup_guid, DicMarking_colorcode["Order_No"], DicMarking_colorcode["Set_Code"], DicMarking_colorcode["Warehouse"], DateTime.Now.ToString("yyyy-MM-dd"), packingqty));
                }
                //更新扫码队列
                CsGetData.UpdataScanLineUp();
                CsMarking.M_MarkParameter = "ColorCode";
                MessageBox.Show("已切换为当前订单");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 获取当前扫码条码写入PLC的信息
        /// </summary>
        /// <param name="dt_"></param>
        /// <returns></returns>
        public static List<M_stc_WriteToPlcSku> GetSkuWriteToPlc(DataTable dt_, string potype_, string IsUnpacking_)
        {
            try
            {
                List<M_stc_WriteToPlcSku> list_v = new List<M_stc_WriteToPlcSku>();
                for (int i = 0; i < dt_.Rows.Count; i++)
                {
                    M_stc_WriteToPlcSku m_Stc_ = new M_stc_WriteToPlcSku();
                    string tiaoma_v = dt_.Rows[i]["TiaoMaVal"].ToString().Trim();
                    string Qty_per_Set_v = "";
                    if (!potype_.Contains("GAP"))
                    {
                        Qty_per_Set_v = GetColorLayernum(dt_, IsUnpacking_, potype_, dt_.Rows[i]["Color_Code"].ToString().Trim()).ToString();
                    }
                    else
                    {
                        Qty_per_Set_v = GetColorLayernum(dt_, IsUnpacking_, potype_, "").ToString();
                    }
                    switch (i)
                    {
                        case 0:
                            m_Stc_.dic_sku["D5100"] = tiaoma_v;
                            m_Stc_.dic_Write_Layernum["D5060"] = Qty_per_Set_v;
                            list_v.Add(m_Stc_);
                            break;
                        case 1:
                            m_Stc_.dic_sku["D5120"] = tiaoma_v;
                            m_Stc_.dic_Write_Layernum["D5061"] = Qty_per_Set_v;
                            list_v.Add(m_Stc_);
                            break;
                        case 2:
                            m_Stc_.dic_sku["D5140"] = tiaoma_v;
                            m_Stc_.dic_Write_Layernum["D5062"] = Qty_per_Set_v;
                            list_v.Add(m_Stc_);
                            break;
                        case 3:
                            m_Stc_.dic_sku["D5160"] = tiaoma_v;
                            m_Stc_.dic_Write_Layernum["D5063"] = Qty_per_Set_v;
                            list_v.Add(m_Stc_);
                            break;
                        case 4:
                            m_Stc_.dic_sku["D5180"] = tiaoma_v;
                            m_Stc_.dic_Write_Layernum["D5064"] = Qty_per_Set_v;
                            list_v.Add(m_Stc_);
                            break;
                        case 5:
                            m_Stc_.dic_sku["D5200"] = tiaoma_v;
                            m_Stc_.dic_Write_Layernum["D5065"] = Qty_per_Set_v;
                            list_v.Add(m_Stc_);
                            break;
                        case 6:
                            m_Stc_.dic_sku["D5220"] = tiaoma_v;
                            m_Stc_.dic_Write_Layernum["D5066"] = Qty_per_Set_v;
                            list_v.Add(m_Stc_);
                            break;
                        case 7:
                            m_Stc_.dic_sku["D5240"] = tiaoma_v;
                            m_Stc_.dic_Write_Layernum["D5067"] = Qty_per_Set_v;
                            list_v.Add(m_Stc_);
                            break;
                        case 8:
                            m_Stc_.dic_sku["D5260"] = tiaoma_v;
                            m_Stc_.dic_Write_Layernum["D5068"] = Qty_per_Set_v;
                            list_v.Add(m_Stc_);
                            break;
                        case 9:
                            m_Stc_.dic_sku["D5280"] = tiaoma_v;
                            m_Stc_.dic_Write_Layernum["D5069"] = Qty_per_Set_v;
                            list_v.Add(m_Stc_);
                            break;
                        default: break;
                    }
                }
                return list_v;
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// 获取GAP纸箱信息
        /// </summary>
        /// <param name="po_guid_"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetCatornData(string po_guid_)
        {
            Dictionary<string, string> DicPaking = new Dictionary<string, string>();
            try
            {
                DataTable dt_packing = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE guid='{0}'", po_guid_));
                if (dt_packing.Rows.Count > 0)
                {
                    string wai_code = dt_packing.Rows[0]["外箱代码"].ToString().Trim();

                    DicPaking["装箱单guid"] = dt_packing.Rows[0]["guid"].ToString().Trim();
                    DicPaking["外箱代码"] = dt_packing.Rows[0]["外箱代码"].ToString().Trim();
                    DicPaking["重量"] = dt_packing.Rows[0]["毛重"].ToString().Trim();
                    DicPaking["箱数"] = dt_packing.Rows[0]["箱数"].ToString().Trim();
                    // DicPaking["CatornStyle_guid"] = DgvRow.Cells["CatornStyle_guid"].Value.ToString().Trim();
                    if (dt_packing.Rows[0]["内包装计数"].ToString().Trim() == "0")
                    {
                        DicPaking["箱内件数"] = dt_packing.Rows[0]["数量"].ToString().Trim();
                    }
                    else
                    {
                        DicPaking["箱内件数"] = dt_packing.Rows[0]["内包装计数"].ToString().Trim();
                    }
                    if (wai_code.Trim().ToUpper() == "C1")
                    {
                        DicPaking["箱子长"] = "582";
                        DicPaking["箱子宽"] = "384";
                        DicPaking["箱子高"] = "294";
                        DicPaking["距离底部"] = "88";
                        DicPaking["距离左边"] = "225";
                        DicPaking["纸箱大小"] = "大";
                        DicPaking["隔板类型"] = "A隔板";
                        DicPaking["贴标类型"] = "大标签";
                        DicPaking["喷码模式"] = "国内";
                        DicPaking["正反方向"] = "正";
                    }
                    else if (wai_code.Trim().ToUpper() == "C2")
                    {
                        DicPaking["箱子长"] = "578";
                        DicPaking["箱子宽"] = "385";
                        DicPaking["箱子高"] = "219";
                        DicPaking["距离底部"] = "80";
                        DicPaking["距离左边"] = "196";
                        DicPaking["纸箱大小"] = "大";
                        DicPaking["隔板类型"] = "A隔板";
                        DicPaking["贴标类型"] = "大标签";
                        DicPaking["喷码模式"] = "国内";
                        DicPaking["正反方向"] = "正";
                    }
                    else if (wai_code.Trim().ToUpper() == "C3")
                    {
                        DicPaking["箱子长"] = "382";
                        DicPaking["箱子宽"] = "290";
                        DicPaking["箱子高"] = "147";
                        DicPaking["距离底部"] = "67";
                        DicPaking["距离左边"] = "123";
                        DicPaking["纸箱大小"] = "小";
                        DicPaking["隔板类型"] = "";
                        DicPaking["贴标类型"] = "大标签";
                        DicPaking["喷码模式"] = "国内";
                        DicPaking["正反方向"] = "正";
                    }
                    else if (wai_code.Trim().ToUpper() == "C4")
                    {
                        DicPaking["箱子长"] = "555";
                        DicPaking["箱子宽"] = "408";
                        DicPaking["箱子高"] = "303";
                        DicPaking["距离底部"] = "110";
                        DicPaking["距离左边"] = "200";
                        DicPaking["纸箱大小"] = "大";
                        DicPaking["隔板类型"] = "A隔板";
                        DicPaking["贴标类型"] = "大标签";
                        DicPaking["喷码模式"] = "国外";
                        DicPaking["正反方向"] = "正";
                    }
                    else if (wai_code.Trim().ToUpper() == "C6")
                    {
                        DicPaking["箱子长"] = "558";
                        DicPaking["箱子宽"] = "406";
                        DicPaking["箱子高"] = "145";
                        DicPaking["距离底部"] = "82";
                        DicPaking["距离左边"] = "172";
                        DicPaking["纸箱大小"] = "小";
                        DicPaking["隔板类型"] = "A隔板";
                        DicPaking["贴标类型"] = "大标签";
                        DicPaking["喷码模式"] = "国外";
                        DicPaking["正反方向"] = "正";
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
            return DicPaking;
        }
        public static string GetPlcCatornData(string name_)
        {
            string val_v = "";
            try
            {
                string strval = CSReadPlc.GetPlcInt(name_, CSReadPlc.DevName).ToString();
                switch (name_)
                {
                    case "HookDirection":
                        if (strval == "1")
                            val_v = "掉头";
                        else
                            val_v = "不掉头";
                        break;
                    case "CatornType":
                        if (strval == "1")
                            val_v = "大";
                        else
                            val_v = "小";
                        break;
                    case "IsRotate":
                        if (strval == "1")
                            val_v = "旋转";
                        else
                            val_v = "不旋转";
                        break;
                    case "PartitionType":
                        if (strval == "1")
                            val_v = "A隔板";
                        else
                            val_v = "B隔板";
                        break;
                    case "LabelingType":
                        if (strval == "1")
                            val_v = "小标签";
                        else
                            val_v = "大标签";
                        break;
                    case "Orientation":
                        if (strval == "0")
                            val_v = "正";
                        else
                            val_v = "反";
                        break;
                    case "IsPalletizing":
                        if (strval == "1")
                            val_v = "允许";
                        else
                            val_v = "不允许";
                        break;
                    case "IsBottomPartition":
                        if (strval == "1")
                            val_v = "有";
                        else
                            val_v = "无";
                        break;
                    case "IsYaMaHaRun":
                        if (strval == "2")
                            val_v = "不运行";
                        else
                            val_v = "运行";
                        break;
                    case "IsTopPartition":
                        if (strval == "1")
                            val_v = "有";
                        else
                            val_v = "无";
                        break;
                    case "IsSealingRun":
                        if (strval == "2")
                            val_v = "不运行";
                        else
                            val_v = "运行";
                        break;
                    case "IsPackingRun":
                        if (strval == "2")
                            val_v = "不运行";
                        else
                            val_v = "运行";
                        break;
                    case "MarkingIsshieid":
                        if (strval == "2")
                            val_v = "不运行";
                        else
                            val_v = "运行";
                        break;
                    case "PrintIsshieid":
                        if (strval == "2")
                            val_v = "不运行";
                        else
                            val_v = "运行";
                        break;
                    case "IsPalletizingRun":
                        if (strval == "2")
                            val_v = "不运行";
                        else
                            val_v = "运行";
                        break;
                    case "IsScanRun":
                        if (strval == "2")
                            val_v = "不运行";
                        else
                            val_v = "运行";
                        break;
                    default:
                        break;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
            return val_v;
        }
        /// <summary>
        /// 优衣库Do导入
        /// </summary>
        /// <param name="FileName_"></param>
        public static void Batch_import_YYKGU(string FileName_)
        {
            Dictionary<string, string> M_dic_v = new Dictionary<string, string>();
            try
            {
                Dictionary<string, string> dic_v = new Dictionary<string, string>();
                DataTable M_dt = new DataTable();
                DataTable dtexist = new DataTable();
                //System.Windows.Forms.OpenFileDialog fd = new OpenFileDialog();
                // ofd.Filter = "EXCEL文档(*.xls)|*.xls";
                //string fileName = "";
                //fileName = ofd.FileName;
                M_dic_v = InPoData(FileName_); //导入po数据
                GetData.GetExcelData getData = new GetExcelData();
                M_dt = getData.bind(FileName_, "DO", "A2", "AF");
                //删掉旧数据
                if (M_dt.Rows.Count > 0)
                {
                    int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT* FROM dbo.YOUYIKUDO WHERE Order_No='{0}'", M_dt.Rows[0]["Order No#"].ToString().Trim()));
                    if (count_v > 0)
                    {
                        CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.YOUYIKUDO WHERE Order_No='{0}'", M_dt.Rows[0]["Order No#"].ToString().Trim()));
                        CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.TMOM WHERE Order_No='{0}'", M_dt.Rows[0]["Order No#"].ToString().Trim()));
                    }
                }
                #region 录入订单数据到数据库
                if (M_dt.Rows.Count > 0)
                {
                    DataTable dt_insert = new DataTable();
                    dt_insert = M_dt.Clone();
                    for (int i = 0; i < M_dt.Rows.Count; i++)
                    {
                        string sqlstr = string.Format("SELECT*FROM dbo.YOUYIKUDO WHERE  DO_No='{0}' AND Warehouse='{1}' AND Set_Code='{2}'", M_dt.Rows[i]["DO No#"].ToString().Trim(), M_dt.Rows[i]["Warehouse"].ToString().Trim(), M_dt.Rows[i]["Set Code"].ToString().Trim());
                        DataTable dt_old = CsFormShow.GoSqlSelect(sqlstr);
                        dtexist = dt_old;
                        if (dtexist.Rows.Count == 0)
                        {
                            dt_insert.ImportRow(M_dt.Rows[i]);
                        }
                    }

                    dt_insert.Columns["Order No#"].ColumnName = "Order_No";
                    dt_insert.Columns["DO No#"].ColumnName = "DO_No";
                    dt_insert.Columns["Document Status"].ColumnName = "Document_Status";
                    dt_insert.Columns["Item Code"].ColumnName = "Item_Code";
                    //dt_insert.Columns["目的港"].ColumnName = "DESTINATION";
                    dt_insert.Columns["Item"].ColumnName = "Item";
                    dt_insert.Columns["Ship to Port Code"].ColumnName = "Ship_to_Port_Code";
                    dt_insert.Columns["Ship to Port"].ColumnName = "Ship_to_Port";
                    dt_insert.Columns["Warehouse"].ColumnName = "Warehouse";
                    dt_insert.Columns["Set Code"].ColumnName = "Set_Code";
                    dt_insert.Columns["Quantity"].ColumnName = "Quantity";
                    dt_insert.Columns["Color Code"].ColumnName = "Color_Code";
                    dt_insert.Columns["Color"].ColumnName = "Color";
                    dt_insert.Columns["Size"].ColumnName = "Size";
                    dt_insert.Columns["Qty per Set"].ColumnName = "Qty_per_Set";
                    dt_insert.Columns["Picking Unit"].ColumnName = "Picking_Unit";
                    //dt_insert.Columns.Add("SKU_Code");
                    dt_insert.Columns.Add("Entrytime");
                    dt_insert.Columns.Add("potype");
                    dt_insert.Columns.Add("Sample_Code");
                    for (int i = 0; i < dt_insert.Rows.Count; i++)
                    {
                        dt_insert.Rows[i]["Quantity"] = CsGetData.GetNumFromStr(dt_insert.Rows[i]["Quantity"].ToString().Trim());
                        dt_insert.Rows[i]["Entrytime"] = DateTime.Now.ToString("yyyy-MM-dd");
                        dt_insert.Rows[i]["Item"] = dt_insert.Rows[i]["Item"].ToString().Replace("'", "");
                        if (M_dic_v.Count > 0)
                        {
                            dt_insert.Rows[i]["potype"] = M_dic_v["Item_Brand"].ToString().Trim();
                            dt_insert.Rows[i]["Sample_Code"] = M_dic_v["Sample_Code"].ToString().Trim();
                        }
                    }
                    inexcel ss = new inexcel();
                    ss.insertToSql(dt_insert, "YOUYIKUDO");
                }
                else
                {
                    MessageBox.Show("没有数据！");
                }
                #endregion
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// 优衣库po数据
        /// </summary>
        /// <param name="fileName_"></param>
        public static Dictionary<string, string> InPoData(string fileName_)
        {
            Dictionary<string, string> dic_v = new Dictionary<string, string>();
            try
            {
                DataTable M_dt = new DataTable();
                DataTable dtexist = new DataTable();
                GetData.GetExcelData getData = new GetExcelData();
                M_dt = getData.bind(fileName_, "PO", "A2", "BD");
                #region 录入订单数据到数据库
                if (M_dt.Rows.Count > 0)
                {
                    DataTable dt_insert = new DataTable();
                    dt_insert = M_dt.Clone();
                    //删掉旧数据
                    if (M_dt.Rows.Count > 0)
                    {
                        int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT* FROM dbo.YOUYIKUPO WHERE Order_No='{0}'", M_dt.Rows[0]["Order No#"].ToString().Trim()));
                        if (count_v > 0)
                        {
                            CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.YOUYIKUPO WHERE Order_No='{0}'", M_dt.Rows[0]["Order No#"].ToString().Trim()));
                        }
                    }
                    //导入新数据数据
                    for (int i = 0; i < M_dt.Rows.Count; i++)
                    {
                        string sqlstr = string.Format("SELECT*FROM dbo.YOUYIKUPO WHERE  Order_No='{0}' AND SKU_Code='{1}' AND Color_Code='{2}'", M_dt.Rows[i]["Order No#"].ToString().Trim(), M_dt.Rows[i]["SKU Code"].ToString().Trim(), M_dt.Rows[i]["Color Code"].ToString().Trim());
                        DataTable dt_old = CsFormShow.GoSqlSelect(sqlstr);
                        dtexist = dt_old;
                        if (dtexist.Rows.Count == 0)
                        {
                            dt_insert.ImportRow(M_dt.Rows[i]);
                        }
                    }

                    dt_insert.Columns["Order No#"].ColumnName = "Order_No";
                    dt_insert.Columns["Revision No#"].ColumnName = "Revision_No";
                    dt_insert.Columns["Order Plan Number"].ColumnName = "Order_Plan_Number";
                    dt_insert.Columns["Item Code"].ColumnName = "Item_Code";
                    dt_insert.Columns["Color Code"].ColumnName = "Color_Code";
                    dt_insert.Columns["Color"].ColumnName = "Color";
                    dt_insert.Columns["Size Code"].ColumnName = "Size_Code";
                    dt_insert.Columns["Size"].ColumnName = "Size";
                    dt_insert.Columns["SKU Code"].ColumnName = "SKU_Code";
                    dt_insert.Columns["Order Qty(pcs)"].ColumnName = "Order_Qty";
                    dt_insert.Columns["Item Brand"].ColumnName = "Item_Brand";
                    dt_insert.Columns["Sample Code"].ColumnName = "Sample_Code";
                    dt_insert.Columns.Add("Entrytime");
                    for (int i = 0; i < dt_insert.Rows.Count; i++)
                    {
                        dt_insert.Rows[i]["Entrytime"] = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    inexcel ss = new inexcel();
                    ss.insertToSql(dt_insert, "YOUYIKUPO");
                    if (dt_insert.Rows.Count > 0)
                    {
                        dic_v["Sample_Code"] = dt_insert.Rows[0]["Sample_Code"].ToString().Trim();
                        dic_v["Item_Brand"] = dt_insert.Rows[0]["Item_Brand"].ToString().Trim();
                    }
                }
                else
                {
                    MessageBox.Show("没有数据！");
                }
                #endregion
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
            return dic_v;
        }
        /// <summary>
        /// GAP装箱单导入
        /// </summary>
        /// <param name="FileName_"></param>
        public static void Batch_import_excel_GAP(string FileName_)
        {
            try
            {
                inexcel exceltosql = new inexcel();
                //System.Windows.Forms.OpenFileDialog fd = new OpenFileDialog();
                //fd.Filter = "EXCEL文档(*.xls)|*.xls";
                DataTable dt = new DataTable();
                DataTable dttotal = new DataTable();
                //string fileName = fd.FileName;
                GetData.GetExcelData getData = new GetExcelData();
                dt = getData.bind(FileName_, "Detail", "E15", "AO");
                GetData.GetExcelData gettotal = new GetExcelData();
                dttotal = gettotal.bind(FileName_, "Detail", "C7", "O9");
                dt.Columns.Remove("F4");
                dt.Columns.Remove("F6");
                dt.Columns.Remove("F8");
                dt.Columns.Remove("F13");
                dt.Columns.Remove("F17");
                dt.Columns.Remove("F30");
                dt.Columns.Remove("F32");
                dt.Columns.Remove("F35");

                dt.Columns[0].ColumnName = "范围";
                dt.Columns[1].ColumnName = "从";
                dt.Columns[2].ColumnName = "到";
                dt.Columns[3].ColumnName = "开始序列号";
                dt.Columns[4].ColumnName = "截止序列号";
                dt.Columns[5].ColumnName = "包装代码";
                dt.Columns[6].ColumnName = "行号";
                dt.Columns[7].ColumnName = "订单号码";
                dt.Columns[8].ColumnName = "买方项目号";
                dt.Columns[9].ColumnName = "Sku号码";
                dt.Columns[10].ColumnName = "简短描述";
                dt.Columns[11].ColumnName = "Size";
                dt.Columns[12].ColumnName = "Color";
                dt.Columns[13].ColumnName = "Hard Tag Indicator";
                dt.Columns[14].ColumnName = "数量";
                dt.Columns[15].ColumnName = "内部包装的项目数量";
                dt.Columns[16].ColumnName = "内包装计数";
                dt.Columns[17].ColumnName = "箱数";
                //dt.Columns[18].ColumnName = "R";
                dt.Columns[19].ColumnName = "外箱代码";
                dt.Columns[20].ColumnName = "净净重";
                dt.Columns[21].ColumnName = "净重";
                dt.Columns[22].ColumnName = "毛重";
                dt.Columns[23].ColumnName = "重量单位";
                dt.Columns[24].ColumnName = "长";
                dt.Columns[25].ColumnName = "宽";
                dt.Columns[26].ColumnName = "高";
                dt.Columns[27].ColumnName = "尺寸单位";
                dt.Columns[28].ColumnName = "扫描ID";
                //this.PackingdGV.Sort(PackingdGV.Columns["范围"], System.ComponentModel.ListSortDirection.Ascending);//排序
                #region 录入订单数据到数据库
                if (dt.Rows.Count > 0)
                {
                    DataTable dt_insert = new DataTable();
                    dt_insert = dt.Clone();
                    //删掉旧数据
                    if (dttotal.Rows.Count > 0)
                    {
                        int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT* FROM dbo.PACKING WHERE 订单号码='{0}'", dttotal.Rows[0]["PO Number"].ToString().Trim()));
                        if (count_v > 0)
                        {
                            string po = dttotal.Rows[0]["PO Number"].ToString().Trim();
                            string sqlstr = string.Format("DELETE FROM dbo.PACKING WHERE 订单号码='{0}' ", po);
                            CsFormShow.GoSqlUpdateInsert(sqlstr);
                            CsFormShow.GoSqlUpdateInsert(string.Format("DELETE FROM dbo.TMOM WHERE Order_No='{0}'", po));
                        }
                    }
                    //提取格式数据并导入数据库
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["数量"] = CsGetData.GetNumFromStr(dt.Rows[i]["数量"].ToString().Trim());
                        dt.Rows[i]["箱数"] = CsGetData.GetNumFromStr(dt.Rows[i]["箱数"].ToString().Trim());
                        dt.Rows[i]["从"] = CsGetData.GetNumFromStr(dt.Rows[i]["从"].ToString().Trim());
                        dt.Rows[i]["到"] = CsGetData.GetNumFromStr(dt.Rows[i]["到"].ToString().Trim());
                        dt_insert.ImportRow(dt.Rows[i]);
                    }
                    if (dt_insert.Rows.Count > 0)
                    {
                        dt_insert.Columns.Add("Entrytime");
                        for (int i = 0; i < dt_insert.Rows.Count; i++)
                        {
                            dt_insert.Rows[i]["Entrytime"] = DateTime.Now.ToString("yyyy-MM-dd");
                            dt.Rows[i]["箱数"] = CsGetData.GetNumFromStr(dt.Rows[i]["箱数"].ToString().Trim());
                        }
                        exceltosql.insertToSql(dt_insert, "dbo.PACKING");
                    }
                    #region 更新订单总箱数
                    string Totalqty = "";
                    for (int i = 0; i < dttotal.Rows.Count; i++)
                    {
                        string po_number = dttotal.Rows[i]["PO Number"].ToString();
                        if (dttotal.Rows[i]["PO Number"].ToString().Contains("Totals"))
                        {
                            Totalqty = CsGetData.GetNumFromStr(dttotal.Rows[i][3].ToString().Trim());
                        }
                    }
                    CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE A SET PoQty='{1}' FROM  (SELECT 订单号码,SUM(ISNULL(箱数,0)) AS PoQty FROM dbo.PACKING WHERE 订单号码='{0}' GROUP BY  订单号码) AS POQTY,dbo.PACKING AS A WHERE A.订单号码=POQTY.订单号码", dt.Rows[0]["订单号码"].ToString(), Totalqty));
                    #endregion
                }
                else
                {
                    MessageBox.Show("没有数据！");
                }
                #endregion

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }
        /// <summary>
        /// GAP标签数据导入
        /// </summary>
        /// <param name="FileName_"></param>
        public static void Batch_import_pdf_GAP(string FileName_)
        {
            try
            {
                DataTable M_dt = new DataTable();
                M_dt = PDFParser.PDFParser.OnCreated(FileName_, 0);
                //删掉旧数据
                if (M_dt.Rows.Count > 0)
                {
                    int count_v = CsFormShow.GoSqlSelectCount(string.Format("SELECT* FROM dbo.PDFDATA WHERE po='{0}'", M_dt.Rows[0]["po"].ToString().Trim()));
                    if (count_v > 0)
                    {
                        string strsql = string.Format("DELETE FROM dbo.PDFDATA WHERE  po='{0}' ", M_dt.Rows[0]["po"].ToString().Trim());
                        CsFormShow.GoSqlUpdateInsert(strsql);
                    }
                }
                //数据存数据库
                for (int i = 0; i < M_dt.Rows.Count; i++)
                {
                    //string sqlselect = "SELECT*FROM pdfdata WHERE number=" + "'" + M_dt.Rows[i]["number"].ToString() + "'";
                    //DataTable dt_old = CsFormShow.GoSqlSelect(sqlselect);
                    //if (dt_old.Rows.Count > 0)
                    //{
                    //    //MessageBox.Show("已存在此条码的记录");
                    //    continue;
                    //}
                    string sqlpdf = string.Format("insert into PDFDATA (page,StyleProgram,size,number,po,style,qty,NO1,NO2,shipfrom1,shipfrom2,shipfrom3,shipfrom4,shipto1,shipto2,shipto3,number1,Entrytime) values ({0},'{1}','{2}','{3}','{4}','{5}',{6},'{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}')",
                    M_dt.Rows[i]["page"], M_dt.Rows[i]["StyleProgram"], M_dt.Rows[i]["size"], M_dt.Rows[i]["number"], M_dt.Rows[i]["po"], M_dt.Rows[i]["style"], M_dt.Rows[i]["qty"], M_dt.Rows[i]["NO1"], M_dt.Rows[i]["NO2"], M_dt.Rows[i]["shipfrom1"], M_dt.Rows[i]["shipfrom2"], M_dt.Rows[i]["shipfrom3"], M_dt.Rows[i]["shipfrom4"], M_dt.Rows[i]["shipto1"], M_dt.Rows[i]["shipto2"], M_dt.Rows[i]["shipto3"], M_dt.Rows[i]["number1"].ToString().Replace(" ", ""), DateTime.Now.ToString("yyyy-MM-dd"));
                    CsFormShow.GoSqlUpdateInsert(sqlpdf);
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// 导入出货日期
        /// </summary>
        /// <param name="FileName_"></param>
        public static void Batch_import_Date(string FileName_)
        {
            try
            {
                inexcel ss = new inexcel();
                DataTable dt_v = new DataTable();
                GetData.GetExcelData getData = new GetExcelData();
                dt_v = getData.bind(FileName_, "Date", "A", "C");
                #region 录入订单数据到数据库
                if (dt_v.Rows.Count > 0)
                {
                    DataTable dt_insert = dt_v.Copy();
                    dt_insert.Columns.Remove("出货日期");
                    dt_insert.Columns.Add("出货日期");
                    for (int i = 0; i < dt_v.Rows.Count; i++)
                    {
                        string sqlstr = string.Format("SELECT*FROM dbo.tWHDate WHERE 订单号='{0}'", dt_v.Rows[i]["订单号"].ToString().Trim());
                        DataTable dtexist = CsFormShow.GoSqlSelect(sqlstr);
                        if (dtexist.Rows.Count > 0)
                        {
                            //dt_insert.ImportRow(M_dt.Rows[i]);
                            CsFormShow.GoSqlUpdateInsert(string.Format("DELETE  FROM dbo.tWHDate WHERE 订单号='{0}'", dt_v.Rows[i]["订单号"].ToString().Trim()));
                        }
                        if (dt_v.Rows[i]["出货日期"].ToString().Contains("/"))
                        {
                            dt_insert.Rows[i]["出货日期"] = DateTime.Parse(dt_v.Rows[i]["出货日期"].ToString().Replace("/", "-")).ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            dt_insert.Rows[i]["出货日期"] = DateTime.Parse(dt_v.Rows[i]["出货日期"].ToString()).ToString("yyyy-MM-dd");
                        }
                    }
                    ss.insertToSql(dt_insert, "tWHDate");
                }
                #endregion
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// 获取文件夹下指定类型文件的路径集合
        /// </summary>
        /// <param name="Path_">路径名</param>
        /// <param name="SearchPattern_">文件类型</param>
        /// <returns></returns>
        public static List<string> GetfilesList(string Path_, string SearchPattern_)
        {
            try
            {
                List<string> listfile = new List<string>();
                string[] files_arr = Directory.GetFiles(Path_ + @"\", SearchPattern_);
                foreach (string file in files_arr)
                {
                    listfile.Add(file);
                }
                return listfile;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// 赋值当前喷码数据
        /// </summary>
        /// <param name="Po_guid_"></param>
        public static void SetToMakingVaria(string Po_guid_, int CartornNum_)
        {
            try
            {
                if (!string.IsNullOrEmpty(Po_guid_))
                {
                    string ptype = CsFormShow.SqlGetVal(string.Format("SELECT*FROM dbo.SchedulingTable WHERE po_guid='{0}'", Po_guid_), "potype");
                    if (ptype.Contains("GAP"))
                    {
                        DataTable dt_packing = CsFormShow.GoSqlSelect(string.Format("SELECT a.*,b.GAPIsPrintRun,b.Set_Code,b.Warehouse,b.potype,b.起始箱号,(b.起始箱号-a.从) AS '完成箱数',(SUBSTRING(Sku号码,0,7)+'-'+SUBSTRING(Sku号码,7,2)+'-'+SUBSTRING(Sku号码,9,1)+'-'+SUBSTRING(Sku号码,10,5)) AS sku FROM dbo.SchedulingTable AS b INNER JOIN dbo.PACKING AS a ON a.guid=b.po_guid AND  b.po_guid='{0}'", Po_guid_));
                        if (dt_packing.Rows.Count > 0)
                        {
                            CsPublicVariablies.Marking.Po_guid = dt_packing.Rows[0]["guid"].ToString().Trim();
                            CsPublicVariablies.Marking.PoType = dt_packing.Rows[0]["potype"].ToString().Trim();
                            CsPublicVariablies.Marking.OrderNo = dt_packing.Rows[0]["订单号码"].ToString().Trim();
                            CsPublicVariablies.Marking.TiaoMaVal = dt_packing.Rows[0]["TiaoMaVal"].ToString().Trim();
                            CsPublicVariablies.Marking.GAPSideStartNum = dt_packing.Rows[0]["GAPSideStartNum"].ToString().Trim();
                            CsPublicVariablies.Marking.Color = dt_packing.Rows[0]["Color"].ToString().Trim();
                            CsPublicVariablies.Marking.Size = dt_packing.Rows[0]["Size"].ToString().Trim();
                            CsPublicVariablies.Marking.GrossWeight = dt_packing.Rows[0]["毛重"].ToString().Trim();
                            CsPublicVariablies.Marking.NetWeight = dt_packing.Rows[0]["净重"].ToString().Trim();
                            CsPublicVariablies.Marking.GrossWeight = dt_packing.Rows[0]["毛重"].ToString().Trim();
                            CsPublicVariablies.Marking.PoQty = int.Parse(dt_packing.Rows[0]["PoQty"].ToString().Trim());
                            CsPublicVariablies.Marking.Sku号码 = dt_packing.Rows[0]["Sku号码"].ToString().Trim();
                            CsPublicVariablies.Marking.WaiCode = dt_packing.Rows[0]["外箱代码"].ToString().Trim();
                            CsPublicVariablies.Marking.Packing内包装计数 = dt_packing.Rows[0]["内包装计数"].ToString().Trim();
                            CsPublicVariablies.Marking.Packing内部包装的项目数量 = dt_packing.Rows[0]["内部包装的项目数量"].ToString().Trim();
                            CsPublicVariablies.Marking.Packing包装代码 = dt_packing.Rows[0]["包装代码"].ToString().Trim();
                            CsPublicVariablies.Marking.Packing数量 = int.Parse(dt_packing.Rows[0]["数量"].ToString().Trim());
                            CsPublicVariablies.Marking.Packing箱数 = int.Parse(dt_packing.Rows[0]["箱数"].ToString().Trim());
                            CsPublicVariablies.Marking.Packing从 = int.Parse(dt_packing.Rows[0]["从"].ToString().Trim());
                            CsPublicVariablies.Marking.IsPrint = dt_packing.Rows[0]["GAPIsPrintRun"].ToString().Trim();
                            CsPublicVariablies.Marking.Packing开始序列号 = int.Parse(dt_packing.Rows[0]["开始序列号"].ToString().Trim());
                            CsPublicVariablies.Marking.CartornNum = CartornNum_;
                            CsPublicVariablies.Marking.StartNum = int.Parse(dt_packing.Rows[0]["起始箱号"].ToString().Trim());
                            CsPublicVariablies.Marking.Packing当前序列号 = int.Parse(dt_packing.Rows[0]["完成箱数"].ToString().Trim()) + CartornNum_ - 1 + int.Parse(dt_packing.Rows[0]["开始序列号"].ToString().Trim());
                        }
                    }
                    else if (ptype.Contains("UNIQLO") || ptype.Contains("GU"))
                    {
                        DataTable dt_packing = CsFormShow.GoSqlSelect(string.Format("SELECT a.*,b.GAPIsPrintRun,b.Set_Code,b.Warehouse,b.potype FROM dbo.SchedulingTable AS b INNER JOIN dbo.YOUYIKUDO AS a ON a.guid=b.po_guid AND  b.po_guid='{0}'", Po_guid_));
                        if (dt_packing.Rows.Count > 0)
                        {
                            CsPublicVariablies.Marking.Po_guid = dt_packing.Rows[0]["guid"].ToString().Trim();
                            CsPublicVariablies.Marking.PoType = dt_packing.Rows[0]["potype"].ToString().Trim();
                            CsPublicVariablies.Marking.OrderNo = dt_packing.Rows[0]["Order_No"].ToString().Trim();
                            CsPublicVariablies.Marking.Packing数量 = int.Parse(dt_packing.Rows[0]["Quantity"].ToString().Trim());
                            CsPublicVariablies.Marking.Warehouse = dt_packing.Rows[0]["Warehouse"].ToString().Trim();
                            CsPublicVariablies.Marking.Set_Code = dt_packing.Rows[0]["Set_Code"].ToString().Trim();
                            CsPublicVariablies.Marking.CartornNum = CartornNum_;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("订单来源有问题");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.ToString());
            }
        }
        /// <summary>
        /// 获取当前当前打印数据
        /// </summary>
        /// <param name="Po_guid_"></param>
        public static void SetToPrintVaria(string Po_guid_, int CartornNum_)
        {
            try
            {
                if (!string.IsNullOrEmpty(Po_guid_))
                {
                    string ptype = CsFormShow.SqlGetVal(string.Format("SELECT*FROM dbo.SchedulingTable WHERE po_guid='{0}'", Po_guid_), "potype");
                    if (ptype.Contains("GAP"))
                    {
                        DataTable dt_packing = CsFormShow.GoSqlSelect(string.Format("SELECT a.*,b.GAPIsPrintRun,b.Set_Code,b.Warehouse,b.potype,b.起始箱号,(b.起始箱号-a.从) AS '完成箱数'  FROM dbo.SchedulingTable AS b INNER JOIN dbo.PACKING AS a ON a.guid=b.po_guid AND  b.po_guid='{0}'", Po_guid_));
                        if (dt_packing.Rows.Count > 0)
                        {
                            CsPublicVariablies.PrintData.Po_guid = dt_packing.Rows[0]["guid"].ToString().Trim();
                            CsPublicVariablies.PrintData.PoType = dt_packing.Rows[0]["potype"].ToString().Trim();
                            CsPublicVariablies.PrintData.OrderNo = dt_packing.Rows[0]["订单号码"].ToString().Trim();
                            CsPublicVariablies.PrintData.NetWeight = dt_packing.Rows[0]["净重"].ToString().Trim();
                            CsPublicVariablies.PrintData.GrossWeight = dt_packing.Rows[0]["毛重"].ToString().Trim();
                            CsPublicVariablies.PrintData.PoQty = int.Parse(dt_packing.Rows[0]["PoQty"].ToString().Trim());
                            CsPublicVariablies.PrintData.Sku号码 = dt_packing.Rows[0]["Sku号码"].ToString().Trim();
                            CsPublicVariablies.PrintData.WaiCode = dt_packing.Rows[0]["外箱代码"].ToString().Trim();
                            CsPublicVariablies.PrintData.Packing内包装计数 = dt_packing.Rows[0]["内包装计数"].ToString().Trim();
                            CsPublicVariablies.PrintData.Packing内部包装的项目数量 = dt_packing.Rows[0]["内部包装的项目数量"].ToString().Trim();
                            CsPublicVariablies.PrintData.Packing包装代码 = dt_packing.Rows[0]["包装代码"].ToString().Trim();
                            CsPublicVariablies.PrintData.Packing数量 = int.Parse(dt_packing.Rows[0]["数量"].ToString().Trim());
                            CsPublicVariablies.PrintData.Packing箱数 = int.Parse(dt_packing.Rows[0]["箱数"].ToString().Trim());
                            CsPublicVariablies.PrintData.Packing从 = int.Parse(dt_packing.Rows[0]["从"].ToString().Trim());
                            CsPublicVariablies.PrintData.IsPrint = dt_packing.Rows[0]["GAPIsPrintRun"].ToString().Trim();
                            CsPublicVariablies.PrintData.Packing开始序列号 = int.Parse(dt_packing.Rows[0]["开始序列号"].ToString().Trim());
                            CsPublicVariablies.PrintData.CartornNum = CartornNum_;
                            CsPublicVariablies.PrintData.StartNum = int.Parse(dt_packing.Rows[0]["起始箱号"].ToString().Trim());
                            CsPublicVariablies.PrintData.Packing当前序列号 = int.Parse(dt_packing.Rows[0]["完成箱数"].ToString().Trim()) + CartornNum_ - 1 + int.Parse(dt_packing.Rows[0]["开始序列号"].ToString().Trim());
                        }
                    }
                    else if (ptype.Contains("UNIQLO") || ptype.Contains("GU"))
                    {
                        DataTable dt_packing = CsFormShow.GoSqlSelect(string.Format("SELECT a.*,b.GAPIsPrintRun,b.Set_Code,b.Warehouse,b.potype FROM dbo.SchedulingTable AS b INNER JOIN dbo.YOUYIKUDO AS a ON a.guid=b.po_guid AND  b.po_guid='{0}'", Po_guid_));
                        if (dt_packing.Rows.Count > 0)
                        {
                            CsPublicVariablies.PrintData.Po_guid = dt_packing.Rows[0]["guid"].ToString().Trim();
                            CsPublicVariablies.PrintData.PoType = dt_packing.Rows[0]["potype"].ToString().Trim();
                            CsPublicVariablies.PrintData.OrderNo = dt_packing.Rows[0]["Order_No"].ToString().Trim();
                            CsPublicVariablies.PrintData.Packing数量 = int.Parse(dt_packing.Rows[0]["Quantity"].ToString().Trim());
                            CsPublicVariablies.PrintData.Warehouse = dt_packing.Rows[0]["Warehouse"].ToString().Trim();
                            CsPublicVariablies.PrintData.Set_Code = dt_packing.Rows[0]["Set_Code"].ToString().Trim();
                            CsPublicVariablies.PrintData.CartornNum = CartornNum_;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("订单来源有问题");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.ToString());
            }
        }
        /// <summary>
        /// 装箱单批量导入
        /// </summary>
        public static void InPackingData()
        {
            string FileName_ = "";
            try
            {
                DialogResult btchose = MessageBox.Show("是否现在导入数据", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    //GAP
                    List<string> list_GAP_excelfile = new List<string>();
                    List<string> list_GAP_pdffile = new List<string>();
                    list_GAP_excelfile = CsGetData.GetfilesList("D:\\数据导入\\GAP", "*.xls");
                    list_GAP_pdffile = CsGetData.GetfilesList("D:\\数据导入\\GAP", "*.pdf");
                    foreach (string FileName_v in list_GAP_excelfile)
                    {
                        FileName_ = FileName_v;
                        CsGetData.Batch_import_excel_GAP(FileName_v);
                    }
                    foreach (string FileName_v in list_GAP_pdffile)
                    {
                        FileName_ = FileName_v;
                        CsGetData.Batch_import_pdf_GAP(FileName_v);
                    }
                    Delet_click.DeleteDir("D:\\数据导入\\GAP");
                    //UNIQLO
                    List<string> list_UNIQLO_excelfile = new List<string>();
                    list_UNIQLO_excelfile = CsGetData.GetfilesList("D:\\数据导入\\UNIQLO", "*.xls");
                    foreach (string FileName_v in list_UNIQLO_excelfile)
                    {
                        FileName_ = FileName_v;
                        CsGetData.Batch_import_YYKGU(FileName_v);
                    }
                    Delet_click.DeleteDir("D:\\数据导入\\UNIQLO");
                    //GU
                    List<string> list_GU_excelfile = new List<string>();
                    list_GU_excelfile = CsGetData.GetfilesList("D:\\数据导入\\GU", "*.xls");
                    foreach (string FileName_v in list_GU_excelfile)
                    {
                        FileName_ = FileName_v;
                        CsGetData.Batch_import_YYKGU(FileName_v);
                    }
                    Delet_click.DeleteDir("D:\\数据导入\\GU");
                    //出货日期
                    List<string> list_date_excelfile = new List<string>();
                    list_date_excelfile = CsGetData.GetfilesList("D:\\数据导入\\出货日期", "*.xls");
                    foreach (string FileName_v in list_date_excelfile)
                    {
                        FileName_ = FileName_v;
                        CsGetData.Batch_import_Date(FileName_v);
                    }
                    Delet_click.DeleteDir("D:\\数据导入\\出货日期");
                    MessageBox.Show("导入成功！");
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("路径：" + FileName_ + "存在错误或者格式不对，" + err.ToString());
            }
        }
        /// <summary>
        /// 获取关工发来的字符串
        /// </summary>
        /// <param name="RecStr_">接收到的字符串</param>
        public static void GetIpsData(string RecStr_)
        {
            try
            {
                CSReadPlc.recstr = RecStr_.Trim();
                int index1_v = CSReadPlc.recstr.IndexOf(":");
                int index2_v = CSReadPlc.recstr.IndexOf(",");
                string type_v = CSReadPlc.recstr.Substring(0, index1_v).ToLower();
                string po_guid = CSReadPlc.recstr.Substring(index1_v + 1, index2_v - index1_v - 1);
                int CartornNum = int.Parse(CSReadPlc.recstr.Substring(index2_v + 1, CSReadPlc.recstr.Length - index2_v - 1));
                if (type_v.Contains("marking"))
                {
                    SetToMakingVaria(po_guid, CartornNum);
                }
                else if (type_v.Contains("print"))
                {
                    if (CSReadPlc.DevName.Contains("plc1") && !string.IsNullOrEmpty(po_guid))
                    {
                        CsFormShow.GoSqlUpdateInsert("DELETE FROM tPrintData");
                        CsFormShow.GoSqlUpdateInsert(string.Format("INSERT INTO tPrintData (po_guid,CartornNum,DeviceName)VALUES('{0}','{1}','{2}')", po_guid, CartornNum, CSReadPlc.DevName));
                        SetToPrintVaria(po_guid, CartornNum);
                    }
                    else if (CSReadPlc.DevName.Contains("plc2"))
                    {
                        SetToPrintVaria(po_guid, CartornNum);
                    }
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }
        /// <summary>
        /// 获取排单表排列序号
        /// </summary>
        /// <param name="DeviceName_">生产线名称</param>
        /// <returns></returns>
        public static int GetMaxSort(string DeviceName_)
        {
            int sort_v = -1;
            try
            {
                DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT TOP 1* FROM dbo.SchedulingTable WHERE DeviceName='{0}' ORDER BY sort DESC", DeviceName_));
                if (dt.Rows.Count > 0)
                {
                    sort_v = int.Parse(dt.Rows[0]["sort"].ToString().Trim());
                    sort_v = sort_v + 1;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
            return sort_v;
        }
        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="RecStr_"></param>
        /// <returns></returns>
        public static string GetCenlitData(string RecStr_)
        {
            string po_guid = "";
            try
            {
                int index1_v = RecStr_.IndexOf("<");
                int index2_v = RecStr_.IndexOf(">");
                po_guid = RecStr_.Substring(index1_v + 1, index2_v - index1_v - 1);

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
            return po_guid;
        }

    }
}
