﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using GetData;
using System.Threading;
using System.Data;
using System.Configuration;

namespace WindowsForms01
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Formlogin login = new Formlogin();//新建登录窗口实例
            //if (login.ShowDialog() == DialogResult.OK)//登陆成功后
            //{
            //    Application.Run(new MainrpForm());//进入主窗体。
            //}
            //Application.Run(new Formlogin());
            string name = System.Windows.Forms.Application.ProductName;
            bool bCreatedNew;
            Mutex m = new Mutex(false, name, out bCreatedNew);
            if (!bCreatedNew)
            {
                MessageBox.Show("程序已经打开，请勿重复！");
                return;
            }
            Formlogin fl = new Formlogin();
            fl.ShowDialog();
            if (fl.DialogResult == DialogResult.OK)
            {
                Application.Run(new MainrpForm());
            }
            else
            {
                return;
            }
        }
    }
    public class CSReadPlc
    {
        static CSReadPlc()
        {
            try
            {
                DevName = ConfigurationManager.AppSettings["DeviceName"];
                if(DevName==""||DevName==null)
                {
                    DevName = "plc1";
                }

                code_v = int.Parse(ConfigurationManager.AppSettings["code_v"]);
                if (code_v <=0||code_v>2)
                {
                    code_v = 1;
                }
            }
            catch (Exception)
            {
                code_v = 1;
                DevName = "plc1";
            }

        }

        public static object M_lockerzb = new object();
        public static int code_v = 1;
        public static string DevName = "plc1";
        public static string PoType = "";
        public static object lockrwplc = new object();
        public static object lockScan = new object();
        public static object lockprint = new object();
        public static object locklogother = new object();
        public static int readplcflag = 0;
        public static string recstr = "";
        public static string EditionFlag = "";
        public static string ToPcFlag = "";
        public static string serialTestFlag = "";
        public static string serialManualFlag = "";
        public static string ToPcStr = "";
        public static string MarkIP = "";//喷码IP
        public static string MarkIPoint = "";//喷码端口
        public static string PrintIP = "";//打印IP
        public static string PrintIPoint = "";//打印端口
        public static string YaMaHaIP = "";
        public static string YaMaHaIPoint = "";
        public static string ToPcIP = "";
        public static string ToPcIPoint = "";
        public static Dictionary<string, Dictionary<string, struct_bao.stc_bao_tag>> Dic_val_vlue = new Dictionary<string, Dictionary<string, struct_bao.stc_bao_tag>>();//字典用于存储设备名和设备对应的打包结构体集合
        //public static  Dictionary<string, Cs_struct_bao.stc_bao_tag> Dic_val_vlues = new Dictionary<string, Cs_struct_bao.stc_bao_tag>();
        public static object GetPlc(string valname, string Device)
        {
            // Dictionary<string, object> dic = new Dictionary<string, object>();
            object Number = 0;
            try
            {
                if (Dic_val_vlue.Count > 0)
                {
                    if (Dic_val_vlue[Device].Count > 0)
                    {
                        if (Dic_val_vlue[Device].ContainsKey(valname))
                        {
                            Number = Dic_val_vlue[Device][valname].strVAL_VALUES.Trim();//指定设备的变量对应的值
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                MessageBox.Show("提示：" + err.Message);
            }
            return Number;
        }
        /// <summary>
        /// 读取PLC的整数值
        /// </summary>
        /// <param name="valname">变量名</param>
        /// <param name="Device">设备名</param>
        /// <returns></returns>
        public static int GetPlcInt(string valname, string Device)
        {
            // Dictionary<string, object> dic = new Dictionary<string, object>();
            int Number = 0;
            try
            {
                if (Dic_val_vlue.Count > 0)
                {
                    if (Dic_val_vlue[Device].Count > 0)
                    {
                        if (Dic_val_vlue[Device].ContainsKey(valname))
                        {
                            Number = int.Parse(Dic_val_vlue[Device][valname].strVAL_VALUES.Trim());//指定设备的变量对应的值
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err + Environment.NewLine + "本次读取的变量名：" + valname + "返回的值Number为：" + Number);
                MessageBox.Show("提示：" + err.Message);
            }
            return Number;
        }
        /// <summary>
        /// 读取PLC的字符串值
        /// </summary>
        /// <param name="valname">变量名</param>
        /// <param name="Device">设备名</param>
        /// <returns></returns>
        public static string GetPlcString(string valname, string Device)
        {
            // Dictionary<string, object> dic = new Dictionary<string, object>();
            string Number = "";
            try
            {
                if (Dic_val_vlue.Count > 0)
                {
                    if (Dic_val_vlue[Device].Count > 0)
                    {
                        if (Dic_val_vlue[Device].ContainsKey(valname))
                        {
                            Number = Dic_val_vlue[Device][valname].strVAL_VALUES.Trim();//指定设备的变量对应的值
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err + "变量名：" + valname + "变量值：" + Number);
                MessageBox.Show("提示：" + err.Message);
            }
            return Number;
        }
    }
    public struct stcUserData
    {
        public static string M_username = "";
        public static bool IsYamahaSetup=true;
        public static bool IsMarkingSetup = true;
        public static bool IsPrintSetup = true;
        public static bool IsSmallLabelPrintSetup = false;
        public static string IsSmallLabelPrintSetupFlag = "";
        public static bool IsScanBarcode = true;
        public static string ShipDate = "";
    }
    public struct M_stcPrintData
    {
        public static Dictionary<string, string> DicManualPrint = new Dictionary<string, string>();
        public static Dictionary<string, string> DicSerialManualPrint = new Dictionary<string, string>();
        public static List<Dictionary<string, string>> lis_printdata = new List<Dictionary<string, string>>();
        public static List<Dictionary<string, string>> lis_OtherLine_printdata = new List<Dictionary<string, string>>();
        public static List<Dictionary<string, string>> lis_OtherLine_SmallLabeldata = new List<Dictionary<string, string>>();
        public static string strPrint_po = "";//记录当前打印的单号
        public static int iPage = 0;//当前打印的页码
        public static int ErrorPage = 0;//当前打印的页码
        public static int iTotal = 0;//当前装箱数据行的总箱数
        public static string M_PrintPath = @"D:\cph\sendtxt.txt";
        public static string M_bmpringpath = @"D:\cph\GAP小标签.txt";
        public static string Isshieid = "";
        public static string ManualFlag = "";
        public static string TestPrintFlag = "";
        public static string CheckMarkFlag = "";
        public static string SerialPortName = "";
        /// 获取打印队列第一个字典中的值
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static string GetprintLineUpVal(string a)
        {
            string val = "";
            if (M_stcPrintData.lis_printdata.Count > 0)
            {
                if (M_stcPrintData.lis_printdata.ToList()[0].Count > 0)
                {
                    val = M_stcPrintData.lis_printdata.ToList()[0][a].ToString().Trim();
                }
            }
            if (val == "")
            {
                val = "0";
                // MessageBox.Show("元素键值‘" + a + "’对应元素没有喷码数据");
                //Cslogfun.WriteTomarkingLog("元素键值‘" + a + "’对应元素没有喷码数据");
            }
            return val;
        }
        //存储变量数据结构体
        public struct stcprintval
        {
            public static string[] name = new string[100];
            public static string[] val = new string[100];
            public static int valcount = 0;
        };
        /// <summary>
        /// 替换txt文本中变量
        /// </summary>
        /// <param name="Dic_SetStr_"></param>
        /// <param name="txtpath_"></param>
        /// <returns></returns>
        public static string GetPrintNewStr(Dictionary<string,string> Dic_SetStr_,string txtpath_)
        {
            string str_new = ""; string valname = ""; string strval = ""; 
            try
            {
                if (Dic_SetStr_.Count>0)
                {
                    for (int j = 0; j < Dic_SetStr_.ToList().Count; j++)
                    {
                        int floag = 0;
                        valname = Dic_SetStr_.ToList()[j].Key;
                        strval = Dic_SetStr_.ToList()[j].Value;
                        for (int i = 0; i < stcprintval.valcount; i++)
                        {
                            if (stcprintval.name[i] == valname)
                            {
                                stcprintval.val[i] = strval;
                                floag = 1;
                            }
                        }
                        if (floag == 0)
                        {
                            stcprintval.name[stcprintval.valcount] = valname;
                            stcprintval.val[stcprintval.valcount] = strval;
                            stcprintval.valcount += 1;
                        }
                    }
                    //用指定值替换文本中的变量
                    str_new = System.IO.File.ReadAllText(txtpath_);
                    for (int i = 0; i < stcprintval.valcount; i++)
                    {
                        str_new = str_new.Replace(stcprintval.name[i], stcprintval.val[i]);
                    }
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
            return str_new;
        }
        public static void PrintOnce(string Po_,string StartNum_)
        {
            try
            {
                DataTable dt_ = CsFormShow.GoSqlSelect(string.Format(" SELECT*FROM dbo.PDFDATA WHERE po='{0}' AND page='{1}'", Po_, StartNum_));
                //M_stcPrintData.DicManualPrint["Poguid"] = dt_.Rows[0]["Poguid"].ToString().Trim();
                M_stcPrintData.DicManualPrint["Pdf_guid"] = dt_.Rows[0]["guid"].ToString().Trim();
                M_stcPrintData.DicManualPrint["page"] = dt_.Rows[0]["page"].ToString().Trim();
                M_stcPrintData.DicManualPrint["StyleProgram"] = dt_.Rows[0]["StyleProgram"].ToString().Trim();
                M_stcPrintData.DicManualPrint["size"] = dt_.Rows[0]["size"].ToString().Trim();
                M_stcPrintData.DicManualPrint["number"] = dt_.Rows[0]["number"].ToString().Trim();
                M_stcPrintData.DicManualPrint["po"] = dt_.Rows[0]["po"].ToString().Trim();
                M_stcPrintData.DicManualPrint["style"] = dt_.Rows[0]["style"].ToString().Trim();
                M_stcPrintData.DicManualPrint["qty"] = dt_.Rows[0]["qty"].ToString().Trim();
                M_stcPrintData.DicManualPrint["NO1"] = dt_.Rows[0]["NO1"].ToString().Trim();
                M_stcPrintData.DicManualPrint["NO2"] = dt_.Rows[0]["NO2"].ToString().Trim();
                M_stcPrintData.DicManualPrint["shipfrom1"] = dt_.Rows[0]["shipfrom1"].ToString().Trim();
                M_stcPrintData.DicManualPrint["shipfrom2"] = dt_.Rows[0]["shipfrom2"].ToString().Trim();
                M_stcPrintData.DicManualPrint["shipfrom3"] = dt_.Rows[0]["shipfrom3"].ToString().Trim();
                M_stcPrintData.DicManualPrint["shipfrom4"] = dt_.Rows[0]["shipfrom4"].ToString().Trim();
                M_stcPrintData.DicManualPrint["shipto1"] = dt_.Rows[0]["shipto1"].ToString().Trim();
                M_stcPrintData.DicManualPrint["shipto2"] = dt_.Rows[0]["shipto2"].ToString().Trim();
                M_stcPrintData.DicManualPrint["shipto3"] = dt_.Rows[0]["shipto3"].ToString().Trim();
                M_stcPrintData.DicManualPrint["shipto4"] = dt_.Rows[0]["shipto4"].ToString().Trim();
                M_stcPrintData.DicManualPrint["number1"] = dt_.Rows[0]["number1"].ToString().Trim();
                M_stcPrintData.DicManualPrint["number2"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(0, 15);
                M_stcPrintData.DicManualPrint["number3"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(15, 4);
                M_stcPrintData.DicManualPrint["number4"] = dt_.Rows[0]["number1"].ToString().Trim().Substring(19, 1);
                M_stcPrintData.ManualFlag = "start";
                CsFormShow.GoSqlUpdateInsert(string.Format("UPDATE dbo.PDFDATA SET PrintCount=ISNULL(PrintCount,0)+1 WHERE po='{0}' AND page='{1}'", dt_.Rows[0]["po"].ToString().Trim(), dt_.Rows[0]["page"].ToString().Trim()));
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }

    }
    public struct M_stcScanData
    {
        public static string strTiaoMa = "";
        public static string strPoTiaoMa = "";
        public static string strpo = "";
        public static string stridentification = "0";
        public static int FinishmarknumberTotal = 0;
        public static string Color_Code = "";
        public static string Set_Code = "";
        public static string potype = "";
        public static DataTable dtCurrentData = new DataTable();
        public static string CurrentPackingNum = "0";
        public static string FinishPackingNum = "0";
        public static int FinishLayernum = 1;
        public static int Layernum = 0;
        public static string IsUnpacking = "";
        public static string MarkingIsshieid = "";
        public static string PrintIsshieid = "";
        public static string IsPalletizing = "";
        public static string IsPackingRun = "";
        public static string IsYaMaHaRun = "";
        public static string IsSealingRun = "";
        public static string IsPalletizingRun = "";
        public static string IsScanRun = "";
        public static string HookDirection = "";
        public static string Sample_Code = "";
        public static string GAPIsPrintRun = "";
        public static string GAPSideIsshieid = "";
        public static string PartitionType = "";
        public static string IP = "";
        public static string IPoint = "";
        public static bool IsEndScan = false;
        public static bool IsEndPacking = false;
        public static string M_WriteToPlcSku = "";
        public static string TiaoMaVal_Now = "";
        public static string BatchNo_Now = "";
        public static int TiaoMa_Qty_per_Set_1 = 0;
        public static int TiaoMa_Qty_per_Set_2 = 0;
        public static int TiaoMa_Qty_per_Set_3 = 0;
        public static int TiaoMa_Qty_per_Set_4 = 0;
        public static int TiaoMa_Qty_per_Set_5 = 0;
        public static int TiaoMa_Qty_per_Set_6 = 0;
        public static int TiaoMa_Qty_per_Set_7 = 0;
        public static int TiaoMa_Qty_per_Set_8 = 0;
        public static int TiaoMa_Qty_per_Set_9 = 0;
        public static int TiaoMa_Qty_per_Set_10 = 0;
        public static List<M_stc_WriteToPlcSku> M_List_WriteToPlcSku = new List<M_stc_WriteToPlcSku>();
        public static List<string> M_List_BatchNo = new List<string>();
        public static Dictionary<string, int> dic_Read_Layernum = new Dictionary<string, int>();
    }
    public class M_stc_WriteToPlcSku
    {
        public Dictionary<string, string> dic_sku = new Dictionary<string, string>();
        public Dictionary<string, string> dic_Write_Layernum = new Dictionary<string, string>();
    }
    public struct M_stcPalletizingData
    {
        public static string strCatornLong = "";
        public static string strCatornWidth = "";
        public static string strCatornHeight = "";
        public static string strWsizeFlag = "";
        public static string strResetFlag = "";
        public static int Palletizing_Unit = 0;
        public static string ReadPalletizingflag = "";
        public static string PalletizingLineType = "";
        public static string PalletizingTest = "";
    }
    public class CsPakingData
    {
        public static Dictionary<string, string> M_DicPakingData = new Dictionary<string, string>();
        public static Dictionary<string, string> M_Dic_CurrentNONet = new Dictionary<string, string>();//国外普通单
        public static List<Dictionary<string, string>> M_list_DicMarkingNONet = new List<Dictionary<string, string>>();
        public static Dictionary<string, string> M_Dic_CurrentNet = new Dictionary<string, string>();//国外网单
        public static List<Dictionary<string, string>> M_list_DicMarkingNet = new List<Dictionary<string, string>>();
        public static Dictionary<string, string> M_DicMarking_colorcode = new Dictionary<string, string>();//喷色号
        public static List<Dictionary<string, string>> M_list_Dic_colorcode = new List<Dictionary<string, string>>();
        public static List<Dictionary<string, string>> M_list_Dic_YYKGUPalletizing = new List<Dictionary<string, string>>();
        public static List<Dictionary<string, string>> M_list_Dic_GAPPalletizing = new List<Dictionary<string, string>>();
        public static List<Dictionary<string, string>> M_list_Dic_palletizingtype = new List<Dictionary<string, string>>();
        /// <summary>
        /// 获取普通单第一行指定序号元素的值
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static string GetMarking_NONet(int a)
        {
            string val = "";
            try
            {
                if (M_list_DicMarkingNONet.Count > 0)
                {
                    if (M_list_DicMarkingNONet.ToList()[0].Count > 0)
                    {
                        val = M_list_DicMarkingNONet.ToList()[0].ToList()[a].Value;
                    }
                }
                if (val == "")
                {
                    val = "0";
                    // MessageBox.Show("元素" + a + "没有喷码数据");
                    // Cslogfun.WriteTomarkingLog("元素" + a + "没有喷码数据");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
            return val;
        }
        public static string GetMarking_NONet(string a)
        {
            string val = "";
            try
            {
                if (M_list_DicMarkingNONet.Count > 0)
                {
                    if (M_list_DicMarkingNONet.ToList()[0].Count > 0)
                    {
                        val = M_list_DicMarkingNONet.ToList()[0][a].ToString().Trim();
                    }
                }
                if (val == "")
                {
                    val = "stop";
                    //MessageBox.Show("元素键值‘" + a + "’对应元素没有喷码数据");
                    // Cslogfun.WriteTomarkingLog("元素键值‘" + a + "’对应元素没有喷码数据");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
               MessageBox.Show("提示：" + ex.ToString());
            }
            return val;
        }
        /// <summary>
        /// 获取网单第一行指定序号元素的值
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static string GetMarking_Net(int a)
        {
            string val = "";
            try
            {
                if (M_list_DicMarkingNet.Count > 0)
                {
                    if (M_list_DicMarkingNet.ToList()[0].Count > 0)
                    {
                        val = M_list_DicMarkingNet.ToList()[0].ToList()[a].Value;
                    }
                }
                if (val == "")
                {
                    val = "0";
                    //MessageBox.Show("元素" + a + "没有喷码数据");
                    //Cslogfun.WriteTomarkingLog("元素" + a + "没有喷码数据");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
            return val;
        }
        public static string GetMarking_Net(string a)
        {
            string val = "";
            try
            {
                if (M_list_DicMarkingNet.Count > 0)
                {
                    if (M_list_DicMarkingNet.ToList()[0].Count > 0)
                    {
                        val = M_list_DicMarkingNet.ToList()[0][a].ToString().Trim();
                    }
                }
                if (val == "")
                {
                    val = "stop";
                    //MessageBox.Show("元素键值‘" + a + "’对应元素没有喷码数据");
                    // Cslogfun.WriteTomarkingLog("元素键值‘" + a + "’对应元素没有喷码数据");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
            return val;
        }
        /// <summary>
        /// 获取网单最后一行指定序号元素的值
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        /// <summary>
        /// 获取喷色号单第一行指定序号元素的值
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static string GetMarking_Colorcode(int a)
        {
            string val = "";
            try
            {
                if (M_list_Dic_colorcode.Count > 0)
                {
                    if (M_list_Dic_colorcode.ToList()[0].Count > 0)
                    {
                        val = M_list_Dic_colorcode.ToList()[0].ToList()[a].Value;
                    }
                }
                if (val == "")
                {
                    val = "stop";
                    // MessageBox.Show("元素" + a + "没有喷码数据");
                    // Cslogfun.WriteTomarkingLog("元素" + a + "没有喷码数据");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
            return val;
        }
        public static string GetMarking_Colorcode(string a)
        {
            string val = "";
            try
            {
                if (M_list_Dic_colorcode.Count > 0)
                {
                    if (M_list_Dic_colorcode.ToList()[0].Count > 0)
                    {
                        val = M_list_Dic_colorcode.ToList()[0][a].ToString().Trim();
                    }
                }
                if (val == "")
                {
                    val = "0";
                    // MessageBox.Show("元素键值‘" + a + "’对应元素没有喷码数据");
                    //Cslogfun.WriteTomarkingLog("元素键值‘" + a + "’对应元素没有喷码数据");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
            return val;
        }
        /// <summary>
        /// 获取码垛队列第一行指定名称元素的值
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static string GetYYKGUPalletizing(string a)
        {
            string val = "";
            try
            {
                if (M_list_Dic_YYKGUPalletizing.Count > 0)
                {
                    if (M_list_Dic_YYKGUPalletizing.ToList()[0].Count > 0)
                    {
                        val = M_list_Dic_YYKGUPalletizing.ToList()[0][a].ToString().Trim();
                    }
                }
                if (val == "")
                {
                    val = "0";
                    // MessageBox.Show("元素键值‘" + a + "’对应元素没有喷码数据");
                    //Cslogfun.WriteTomarkingLog("元素键值‘" + a + "’对应元素没有喷码数据");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
            return val;
        }
        /// <summary>
        /// 获取码垛队列第一行指定名称元素的值
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static string GetPalletizingtype(string a)
        {
            string val = "";
            try
            {
                if (M_list_Dic_palletizingtype.Count > 0)
                {
                    if (M_list_Dic_palletizingtype.ToList()[0].Count > 0)
                    {
                        val = M_list_Dic_palletizingtype.ToList()[0][a].ToString().Trim();
                    }
                }
                if (val == "")
                {
                    val = "0";
                    // MessageBox.Show("元素键值‘" + a + "’对应元素没有喷码数据");
                    //Cslogfun.WriteTomarkingLog("元素键值‘" + a + "’对应元素没有喷码数据");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
            return val;
        }
        /// <summary>
        /// 获取码垛队列第一行指定名称元素的值
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static string GetGAPPalletizing(string a)
        {
            string val = "";
            try
            {
                if (M_list_Dic_GAPPalletizing.Count > 0)
                {
                    if (M_list_Dic_GAPPalletizing.ToList()[0].Count > 0)
                    {
                        val = M_list_Dic_GAPPalletizing.ToList()[0][a].ToString().Trim();
                    }
                }
                if (val == "")
                {
                    val = "0";
                    // MessageBox.Show("元素键值‘" + a + "’对应元素没有喷码数据");
                    Cslogfun.WriteTomarkingLog("元素键值‘" + a + "’对应元素没有喷码数据");
                }
            }
            catch (Exception)
            {
                Cslogfun.WriteTomarkingLog("元素键值‘" + a + "’对应元素没有喷码数据");
            }
            return val;
        }
        /// <summary>
        /// /获取喷码内容
        /// </summary>
        /// <param name="Po_"></param>
        /// <param name="StartCode_"></param>
        /// <param name="StartNum_"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetMarkingData(string Po_,string StartCode_,string StartNum_)
        {
            Dictionary<string, string> dic_marking = new Dictionary<string, string>();
            try
            {
                DataTable dt = CsFormShow.GoSqlSelect(string.Format("SELECT*FROM dbo.PACKING WHERE 订单号码='{0}' AND 从='{1}'", Po_, StartCode_));
                string wai_code = dt.Rows[0]["外箱代码"].ToString().Trim();
                //string finishmarknum = dt.Rows[0]["finishmarknumber"].ToString().Trim();
                int Carton_v =int.Parse(StartNum_);
                if (wai_code.ToUpper() != "C4" && wai_code.ToUpper() != "C6")
                {
                    //dic_marking.Clear();//清除之前
                    dic_marking["订单号码"] = dt.Rows[0]["订单号码"].ToString().Trim().Substring(0, 5) + "    " + dt.Rows[0]["订单号码"].ToString().Trim().Substring(5, dt.Rows[0]["订单号码"].ToString().Length - 5);
                    if (!string.IsNullOrEmpty(dt.Rows[0]["内包装计数"].ToString().Trim()) && dt.Rows[0]["内包装计数"].ToString().Trim() != "0")//是多条装一包
                    {
                        dic_marking["SKU/Item"] = dt.Rows[0]["包装代码"].ToString().Trim();
                        int Unit_v = int.Parse(dt.Rows[0]["数量"].ToString().Trim()) / int.Parse(dt.Rows[0]["箱数"].ToString().Trim());
                        dic_marking["Unit/Prepack"] = Unit_v.ToString() + "/" + dt.Rows[0]["内包装计数"].ToString().Trim();
                    }
                    else//一条装一包
                    {
                        int len = dt.Rows[0]["Sku号码"].ToString().Trim().Length;
                        if (len < 14)
                        {
                            for (int i = 0; i < 14 - len; i++)
                            {
                                dt.Rows[0]["Sku号码"] = dt.Rows[0]["Sku号码"].ToString().Trim() + "0";
                            }
                        }
                        dic_marking["SKU/Item"] = dt.Rows[0]["Sku号码"].ToString().Trim().Substring(0, 9) + "-" + dt.Rows[0]["Sku号码"].ToString().Trim().Substring(9, 4);
                        dic_marking["Unit/Prepack"] = dt.Rows[0]["内部包装的项目数量"].ToString().Trim() + "/0";
                    }
                    dic_marking["Carton"] = Carton_v + "        " + dt.Rows[0]["PoQty"].ToString().Trim(); ;
                }
                else if (wai_code.ToUpper() == "C4" || wai_code.ToUpper() == "C6")
                {
                    //dic_marking.Clear();//清除之前
                    dic_marking["Carton"] = Carton_v + "               " + dt.Rows[0]["PoQty"].ToString().Trim();
                    dic_marking["订单号码"] = dt.Rows[0]["订单号码"].ToString().Trim().Substring(0, 5) + "    " + dt.Rows[0]["订单号码"].ToString().Trim().Substring(5, dt.Rows[0]["订单号码"].ToString().Length - 5);
                    dic_marking["style/size"] = dt.Rows[0]["Sku号码"].ToString().Trim().Substring(0, 9)
                    + "              " + dt.Rows[0]["Sku号码"].ToString().Trim().Substring(9, 4);
                    dic_marking["qty"] = dt.Rows[0]["数量"].ToString().Trim();
                    dic_marking["GrossWeight"] = dt.Rows[0]["毛重"].ToString().Trim();
                    dic_marking["NetWeight"] = dt.Rows[0]["净重"].ToString().Trim();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
            return dic_marking;
        }
    }
    public struct M_MarkingData
    {
        public static string M_markingData = "";
        public static string M_MarkPattern = "";
        public static string M_CatornType = "";
        public static string M_PackingCode = "";
        public static int M_Trg_x = 0;
        public static int M_Trg_y = 0;
        public static string IsRepeatCheck = "";
        public static string IsMarkingNum = "";
        public static string StrPo = "";
        public static string StarNum = "";
        public static string GetMarkingSendMessage = "";
        public static string SendMarkingSendMessage = "我来了";
        public static string SendMessageFlag = "";
    }
    public class Message
    {
        public const int USER = 0x0400;
        public const int WM_TEST = USER + 101;
        public const int WM_MSG = USER + 102;
        public const int WM_COPYDATA = 0x004A;
    }
}
