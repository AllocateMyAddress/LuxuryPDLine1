﻿
namespace WindowsForms01
{
    partial class GAPSideStartNumForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SideSatrtNumtBx = new System.Windows.Forms.TextBox();
            this.okbtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "网单标签序列号:";
            // 
            // SideSatrtNumtBx
            // 
            this.SideSatrtNumtBx.Location = new System.Drawing.Point(168, 37);
            this.SideSatrtNumtBx.Name = "SideSatrtNumtBx";
            this.SideSatrtNumtBx.Size = new System.Drawing.Size(194, 25);
            this.SideSatrtNumtBx.TabIndex = 1;
            this.SideSatrtNumtBx.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // okbtn
            // 
            this.okbtn.Location = new System.Drawing.Point(168, 105);
            this.okbtn.Name = "okbtn";
            this.okbtn.Size = new System.Drawing.Size(95, 34);
            this.okbtn.TabIndex = 2;
            this.okbtn.Text = "确定";
            this.okbtn.UseVisualStyleBackColor = true;
            this.okbtn.Click += new System.EventHandler(this.okbtn_Click);
            // 
            // GAPSideStartNumForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 151);
            this.Controls.Add(this.okbtn);
            this.Controls.Add(this.SideSatrtNumtBx);
            this.Controls.Add(this.label1);
            this.Name = "GAPSideStartNumForm";
            this.Text = "网单标签序列号";
            this.Load += new System.EventHandler(this.GAPSideStartNumForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SideSatrtNumtBx;
        private System.Windows.Forms.Button okbtn;
    }
}