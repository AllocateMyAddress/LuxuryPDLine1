﻿namespace WindowsForms01
{
    partial class MainrpForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainrpForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.用户管理tStDDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.用户管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton4 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.gU数据导入ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GAPtStpDropDownBtn = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.导入装箱EXCELZToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.导入PDFFToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.打印标签队列ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton5 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.导入优衣库EXCELEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.喷码队列ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.订单历史记录查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.PotSpTBox = new System.Windows.Forms.ToolStripTextBox();
            this.othertStpDBtn = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.PakinputtSpBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.喷码日期输入ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.喷码纸箱位置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.扫描条码重新计数ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.赋值拍照信号ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.模拟进箱ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.模拟码垛完成ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.查询寄存器值ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.测试ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.读字符串ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.打印GAP侧面贴标信号ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.取消喷码编号ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.获取雅马哈计数ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.清除雅马哈计数ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.切换牌照号模板ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.换款信号ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.模拟喷码赋值ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.读取公共变量ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.启动关工程序ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendMessageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.测试串口打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SelecttSpButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.Device_tSCboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.数据BToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.看板ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.User_tSLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.User_tSLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.textRecMsgtbox = new System.Windows.Forms.TextBox();
            this.galleryContainer4 = new DevComponents.DotNetBar.GalleryContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.DataInBtn = new ControlExs.ImageButton();
            this.YYKproducingBtn = new ControlExs.ImageButton();
            this.GAPHistoricalDataBtn = new ControlExs.ImageButton();
            this.GAPproducingBtn = new ControlExs.ImageButton();
            this.GAPPackingDataBtn = new ControlExs.ImageButton();
            this.MarkDateBtn = new ControlExs.ImageButton();
            this.offLineBtn = new ControlExs.ImageButton();
            this.PDFBtn = new ControlExs.ImageButton();
            this.YYKHistoricalDataBtn = new ControlExs.ImageButton();
            this.YYKPackingDataBtn = new ControlExs.ImageButton();
            this.producingBtn = new ControlExs.ImageButton();
            this.PaiDanBtn = new ControlExs.ImageButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.OthersimgBtn = new ControlExs.ImageButton();
            this.GAPimgBtn = new ControlExs.ImageButton();
            this.YYKimgBtn = new ControlExs.ImageButton();
            this.PaidanimgBtn = new ControlExs.ImageButton();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataInBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YYKproducingBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAPHistoricalDataBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAPproducingBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAPPackingDataBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MarkDateBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offLineBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDFBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YYKHistoricalDataBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YYKPackingDataBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.producingBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaiDanBtn)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OthersimgBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAPimgBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YYKimgBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidanimgBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.用户管理tStDDownButton,
            this.toolStripDropDownButton4,
            this.GAPtStpDropDownBtn,
            this.toolStripDropDownButton5,
            this.toolStripLabel2,
            this.PotSpTBox,
            this.othertStpDBtn,
            this.toolStripDropDownButton1,
            this.SelecttSpButton,
            this.toolStripLabel1,
            this.Device_tSCboBox,
            this.toolStripDropDownButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1365, 48);
            this.toolStrip1.TabIndex = 22;
            this.toolStrip1.Text = "主菜单";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // 用户管理tStDDownButton
            // 
            this.用户管理tStDDownButton.AutoSize = false;
            this.用户管理tStDDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator2,
            this.用户管理ToolStripMenuItem});
            this.用户管理tStDDownButton.Image = ((System.Drawing.Image)(resources.GetObject("用户管理tStDDownButton.Image")));
            this.用户管理tStDDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.用户管理tStDDownButton.Name = "用户管理tStDDownButton";
            this.用户管理tStDDownButton.Size = new System.Drawing.Size(120, 56);
            this.用户管理tStDDownButton.Text = "系统管理";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(154, 6);
            // 
            // 用户管理ToolStripMenuItem
            // 
            this.用户管理ToolStripMenuItem.Name = "用户管理ToolStripMenuItem";
            this.用户管理ToolStripMenuItem.Size = new System.Drawing.Size(157, 24);
            this.用户管理ToolStripMenuItem.Text = "用户管理(&Y)";
            this.用户管理ToolStripMenuItem.Click += new System.EventHandler(this.用户管理ToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton4
            // 
            this.toolStripDropDownButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.gU数据导入ToolStripMenuItem});
            this.toolStripDropDownButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton4.Image")));
            this.toolStripDropDownButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton4.Name = "toolStripDropDownButton4";
            this.toolStripDropDownButton4.Size = new System.Drawing.Size(77, 45);
            this.toolStripDropDownButton4.Text = "排单计划";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(195, 6);
            // 
            // gU数据导入ToolStripMenuItem
            // 
            this.gU数据导入ToolStripMenuItem.Name = "gU数据导入ToolStripMenuItem";
            this.gU数据导入ToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.gU数据导入ToolStripMenuItem.Text = "排单计划(&G)";
            this.gU数据导入ToolStripMenuItem.Click += new System.EventHandler(this.gU数据导入ToolStripMenuItem_Click);
            // 
            // GAPtStpDropDownBtn
            // 
            this.GAPtStpDropDownBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator6,
            this.导入装箱EXCELZToolStripMenuItem1,
            this.toolStripSeparator9,
            this.toolStripMenuItem8,
            this.toolStripSeparator3,
            this.toolStripMenuItem1,
            this.toolStripMenuItem3,
            this.导入PDFFToolStripMenuItem1,
            this.toolStripSeparator11,
            this.打印标签队列ToolStripMenuItem});
            this.GAPtStpDropDownBtn.Image = ((System.Drawing.Image)(resources.GetObject("GAPtStpDropDownBtn.Image")));
            this.GAPtStpDropDownBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.GAPtStpDropDownBtn.Name = "GAPtStpDropDownBtn";
            this.GAPtStpDropDownBtn.Size = new System.Drawing.Size(62, 45);
            this.GAPtStpDropDownBtn.Text = "GAP";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(203, 6);
            // 
            // 导入装箱EXCELZToolStripMenuItem1
            // 
            this.导入装箱EXCELZToolStripMenuItem1.Name = "导入装箱EXCELZToolStripMenuItem1";
            this.导入装箱EXCELZToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.导入装箱EXCELZToolStripMenuItem1.Size = new System.Drawing.Size(206, 24);
            this.导入装箱EXCELZToolStripMenuItem1.Text = "装箱数据(&Z)";
            this.导入装箱EXCELZToolStripMenuItem1.Click += new System.EventHandler(this.导入装箱EXCELZToolStripMenuItem1_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(203, 6);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(206, 24);
            this.toolStripMenuItem8.Text = "正在生产";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(203, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(206, 24);
            this.toolStripMenuItem1.Text = "订单生产记录查询";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click_1);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(203, 6);
            // 
            // 导入PDFFToolStripMenuItem1
            // 
            this.导入PDFFToolStripMenuItem1.Name = "导入PDFFToolStripMenuItem1";
            this.导入PDFFToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.导入PDFFToolStripMenuItem1.Size = new System.Drawing.Size(206, 24);
            this.导入PDFFToolStripMenuItem1.Text = "标签数据(&F)";
            this.导入PDFFToolStripMenuItem1.Click += new System.EventHandler(this.导入PDFFToolStripMenuItem1_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(203, 6);
            // 
            // 打印标签队列ToolStripMenuItem
            // 
            this.打印标签队列ToolStripMenuItem.Name = "打印标签队列ToolStripMenuItem";
            this.打印标签队列ToolStripMenuItem.Size = new System.Drawing.Size(206, 24);
            this.打印标签队列ToolStripMenuItem.Text = "正在等候打印的标签";
            this.打印标签队列ToolStripMenuItem.Click += new System.EventHandler(this.打印标签队列ToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton5
            // 
            this.toolStripDropDownButton5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.导入优衣库EXCELEToolStripMenuItem,
            this.toolStripMenuItem10,
            this.喷码队列ToolStripMenuItem,
            this.toolStripSeparator8,
            this.订单历史记录查询ToolStripMenuItem});
            this.toolStripDropDownButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton5.Image")));
            this.toolStripDropDownButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton5.Name = "toolStripDropDownButton5";
            this.toolStripDropDownButton5.Size = new System.Drawing.Size(132, 45);
            this.toolStripDropDownButton5.Text = "优衣库/GU数据";
            this.toolStripDropDownButton5.ToolTipText = "优衣库数据";
            this.toolStripDropDownButton5.Click += new System.EventHandler(this.toolStripDropDownButton5_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(195, 6);
            // 
            // 导入优衣库EXCELEToolStripMenuItem
            // 
            this.导入优衣库EXCELEToolStripMenuItem.Name = "导入优衣库EXCELEToolStripMenuItem";
            this.导入优衣库EXCELEToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.导入优衣库EXCELEToolStripMenuItem.Text = "装箱数据(&E)";
            this.导入优衣库EXCELEToolStripMenuItem.Click += new System.EventHandler(this.导入优衣库EXCELEToolStripMenuItem_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(195, 6);
            // 
            // 喷码队列ToolStripMenuItem
            // 
            this.喷码队列ToolStripMenuItem.Name = "喷码队列ToolStripMenuItem";
            this.喷码队列ToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.喷码队列ToolStripMenuItem.Text = "正在生产";
            this.喷码队列ToolStripMenuItem.Click += new System.EventHandler(this.喷码队列ToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(195, 6);
            // 
            // 订单历史记录查询ToolStripMenuItem
            // 
            this.订单历史记录查询ToolStripMenuItem.Name = "订单历史记录查询ToolStripMenuItem";
            this.订单历史记录查询ToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.订单历史记录查询ToolStripMenuItem.Text = "订单生产记录查询";
            this.订单历史记录查询ToolStripMenuItem.Click += new System.EventHandler(this.订单历史记录查询ToolStripMenuItem_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(49, 45);
            this.toolStripLabel2.Text = "订单号";
            this.toolStripLabel2.Visible = false;
            // 
            // PotSpTBox
            // 
            this.PotSpTBox.AutoSize = false;
            this.PotSpTBox.Name = "PotSpTBox";
            this.PotSpTBox.Size = new System.Drawing.Size(132, 22);
            this.PotSpTBox.Visible = false;
            // 
            // othertStpDBtn
            // 
            this.othertStpDBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.othertStpDBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.PakinputtSpBtn,
            this.toolStripMenuItem6,
            this.喷码日期输入ToolStripMenuItem,
            this.toolStripMenuItem7,
            this.toolStripMenuItem16,
            this.toolStripSeparator15,
            this.toolStripMenuItem11,
            this.toolStripSeparator10,
            this.toolStripMenuItem12,
            this.toolStripSeparator7,
            this.toolStripMenuItem13,
            this.toolStripSeparator12,
            this.toolStripMenuItem14,
            this.toolStripSeparator13,
            this.toolStripMenuItem17,
            this.toolStripMenuItem15,
            this.喷码纸箱位置ToolStripMenuItem,
            this.扫描条码重新计数ToolStripMenuItem});
            this.othertStpDBtn.Image = ((System.Drawing.Image)(resources.GetObject("othertStpDBtn.Image")));
            this.othertStpDBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.othertStpDBtn.Name = "othertStpDBtn";
            this.othertStpDBtn.Size = new System.Drawing.Size(77, 45);
            this.othertStpDBtn.Text = "其他功能";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(195, 6);
            // 
            // PakinputtSpBtn
            // 
            this.PakinputtSpBtn.Name = "PakinputtSpBtn";
            this.PakinputtSpBtn.Size = new System.Drawing.Size(198, 24);
            this.PakinputtSpBtn.Text = "离线装箱信息输入";
            this.PakinputtSpBtn.Click += new System.EventHandler(this.PakinputtSpBtn_Click_1);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(195, 6);
            // 
            // 喷码日期输入ToolStripMenuItem
            // 
            this.喷码日期输入ToolStripMenuItem.Name = "喷码日期输入ToolStripMenuItem";
            this.喷码日期输入ToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.喷码日期输入ToolStripMenuItem.Text = "喷码日期输入";
            this.喷码日期输入ToolStripMenuItem.Click += new System.EventHandler(this.喷码日期输入ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(195, 6);
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(198, 24);
            this.toolStripMenuItem16.Text = "数据导入";
            this.toolStripMenuItem16.Click += new System.EventHandler(this.toolStripMenuItem16_Click_1);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(195, 6);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(198, 24);
            this.toolStripMenuItem11.Text = "启动GAP测试打印";
            this.toolStripMenuItem11.Click += new System.EventHandler(this.toolStripMenuItem11_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(195, 6);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(198, 24);
            this.toolStripMenuItem12.Text = "取消GAP测试打印";
            this.toolStripMenuItem12.Click += new System.EventHandler(this.toolStripMenuItem12_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(195, 6);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(198, 24);
            this.toolStripMenuItem13.Text = "打印标签第一张";
            this.toolStripMenuItem13.Click += new System.EventHandler(this.toolStripMenuItem13_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(195, 6);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(198, 24);
            this.toolStripMenuItem14.Text = "追溯记录";
            this.toolStripMenuItem14.Click += new System.EventHandler(this.toolStripMenuItem14_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(195, 6);
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(198, 24);
            this.toolStripMenuItem17.Text = "库存";
            this.toolStripMenuItem17.Click += new System.EventHandler(this.toolStripMenuItem17_Click);
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Enabled = false;
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(198, 24);
            this.toolStripMenuItem15.Text = "设备启用选择";
            this.toolStripMenuItem15.Click += new System.EventHandler(this.toolStripMenuItem15_Click);
            // 
            // 喷码纸箱位置ToolStripMenuItem
            // 
            this.喷码纸箱位置ToolStripMenuItem.Enabled = false;
            this.喷码纸箱位置ToolStripMenuItem.Name = "喷码纸箱位置ToolStripMenuItem";
            this.喷码纸箱位置ToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.喷码纸箱位置ToolStripMenuItem.Text = "喷码纸箱位置";
            this.喷码纸箱位置ToolStripMenuItem.Visible = false;
            this.喷码纸箱位置ToolStripMenuItem.Click += new System.EventHandler(this.喷码纸箱位置ToolStripMenuItem_Click);
            // 
            // 扫描条码重新计数ToolStripMenuItem
            // 
            this.扫描条码重新计数ToolStripMenuItem.Enabled = false;
            this.扫描条码重新计数ToolStripMenuItem.Name = "扫描条码重新计数ToolStripMenuItem";
            this.扫描条码重新计数ToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.扫描条码重新计数ToolStripMenuItem.Text = "扫描条码重新计数";
            this.扫描条码重新计数ToolStripMenuItem.Click += new System.EventHandler(this.扫描条码重新计数ToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.赋值拍照信号ToolStripMenuItem,
            this.模拟进箱ToolStripMenuItem,
            this.模拟码垛完成ToolStripMenuItem,
            this.查询寄存器值ToolStripMenuItem,
            this.测试ToolStripMenuItem,
            this.打印ToolStripMenuItem,
            this.读字符串ToolStripMenuItem,
            this.打印GAP侧面贴标信号ToolStripMenuItem,
            this.取消喷码编号ToolStripMenuItem,
            this.获取雅马哈计数ToolStripMenuItem,
            this.清除雅马哈计数ToolStripMenuItem,
            this.切换牌照号模板ToolStripMenuItem,
            this.换款信号ToolStripMenuItem,
            this.模拟喷码赋值ToolStripMenuItem,
            this.读取公共变量ToolStripMenuItem,
            this.启动关工程序ToolStripMenuItem,
            this.sendMessageToolStripMenuItem,
            this.测试串口打印ToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(77, 45);
            this.toolStripDropDownButton1.Text = "临时按钮";
            // 
            // 赋值拍照信号ToolStripMenuItem
            // 
            this.赋值拍照信号ToolStripMenuItem.Enabled = false;
            this.赋值拍照信号ToolStripMenuItem.Name = "赋值拍照信号ToolStripMenuItem";
            this.赋值拍照信号ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.赋值拍照信号ToolStripMenuItem.Text = "赋值拍照信号";
            this.赋值拍照信号ToolStripMenuItem.Visible = false;
            this.赋值拍照信号ToolStripMenuItem.Click += new System.EventHandler(this.赋值拍照信号ToolStripMenuItem_Click);
            // 
            // 模拟进箱ToolStripMenuItem
            // 
            this.模拟进箱ToolStripMenuItem.Name = "模拟进箱ToolStripMenuItem";
            this.模拟进箱ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.模拟进箱ToolStripMenuItem.Text = "模拟进箱";
            this.模拟进箱ToolStripMenuItem.Click += new System.EventHandler(this.模拟进箱ToolStripMenuItem_Click);
            // 
            // 模拟码垛完成ToolStripMenuItem
            // 
            this.模拟码垛完成ToolStripMenuItem.Name = "模拟码垛完成ToolStripMenuItem";
            this.模拟码垛完成ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.模拟码垛完成ToolStripMenuItem.Text = "模拟码垛完成";
            this.模拟码垛完成ToolStripMenuItem.Click += new System.EventHandler(this.模拟码垛完成ToolStripMenuItem_Click);
            // 
            // 查询寄存器值ToolStripMenuItem
            // 
            this.查询寄存器值ToolStripMenuItem.Name = "查询寄存器值ToolStripMenuItem";
            this.查询寄存器值ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.查询寄存器值ToolStripMenuItem.Text = "查询寄存器值";
            this.查询寄存器值ToolStripMenuItem.Click += new System.EventHandler(this.查询寄存器值ToolStripMenuItem_Click);
            // 
            // 测试ToolStripMenuItem
            // 
            this.测试ToolStripMenuItem.Name = "测试ToolStripMenuItem";
            this.测试ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.测试ToolStripMenuItem.Text = "测试喷码";
            this.测试ToolStripMenuItem.Click += new System.EventHandler(this.测试ToolStripMenuItem_Click);
            // 
            // 打印ToolStripMenuItem
            // 
            this.打印ToolStripMenuItem.Name = "打印ToolStripMenuItem";
            this.打印ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.打印ToolStripMenuItem.Text = "打印";
            this.打印ToolStripMenuItem.Click += new System.EventHandler(this.打印ToolStripMenuItem_Click_1);
            // 
            // 读字符串ToolStripMenuItem
            // 
            this.读字符串ToolStripMenuItem.Name = "读字符串ToolStripMenuItem";
            this.读字符串ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.读字符串ToolStripMenuItem.Text = "读字符串";
            this.读字符串ToolStripMenuItem.Click += new System.EventHandler(this.读字符串ToolStripMenuItem_Click);
            // 
            // 打印GAP侧面贴标信号ToolStripMenuItem
            // 
            this.打印GAP侧面贴标信号ToolStripMenuItem.Name = "打印GAP侧面贴标信号ToolStripMenuItem";
            this.打印GAP侧面贴标信号ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.打印GAP侧面贴标信号ToolStripMenuItem.Text = "打印GAP侧面贴标信号";
            this.打印GAP侧面贴标信号ToolStripMenuItem.Click += new System.EventHandler(this.打印GAP侧面贴标信号ToolStripMenuItem_Click);
            // 
            // 取消喷码编号ToolStripMenuItem
            // 
            this.取消喷码编号ToolStripMenuItem.Name = "取消喷码编号ToolStripMenuItem";
            this.取消喷码编号ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.取消喷码编号ToolStripMenuItem.Text = "取消喷码编号";
            this.取消喷码编号ToolStripMenuItem.Click += new System.EventHandler(this.取消喷码编号ToolStripMenuItem_Click);
            // 
            // 获取雅马哈计数ToolStripMenuItem
            // 
            this.获取雅马哈计数ToolStripMenuItem.Name = "获取雅马哈计数ToolStripMenuItem";
            this.获取雅马哈计数ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.获取雅马哈计数ToolStripMenuItem.Text = "获取雅马哈计数";
            this.获取雅马哈计数ToolStripMenuItem.Click += new System.EventHandler(this.获取雅马哈计数ToolStripMenuItem_Click);
            // 
            // 清除雅马哈计数ToolStripMenuItem
            // 
            this.清除雅马哈计数ToolStripMenuItem.Name = "清除雅马哈计数ToolStripMenuItem";
            this.清除雅马哈计数ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.清除雅马哈计数ToolStripMenuItem.Text = "清除雅马哈计数";
            this.清除雅马哈计数ToolStripMenuItem.Click += new System.EventHandler(this.清除雅马哈计数ToolStripMenuItem_Click);
            // 
            // 切换牌照号模板ToolStripMenuItem
            // 
            this.切换牌照号模板ToolStripMenuItem.Name = "切换牌照号模板ToolStripMenuItem";
            this.切换牌照号模板ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.切换牌照号模板ToolStripMenuItem.Text = "切换牌照号模板";
            this.切换牌照号模板ToolStripMenuItem.Click += new System.EventHandler(this.切换牌照号模板ToolStripMenuItem_Click);
            // 
            // 换款信号ToolStripMenuItem
            // 
            this.换款信号ToolStripMenuItem.Name = "换款信号ToolStripMenuItem";
            this.换款信号ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.换款信号ToolStripMenuItem.Text = "换款信号";
            this.换款信号ToolStripMenuItem.Click += new System.EventHandler(this.换款信号ToolStripMenuItem_Click);
            // 
            // 模拟喷码赋值ToolStripMenuItem
            // 
            this.模拟喷码赋值ToolStripMenuItem.Name = "模拟喷码赋值ToolStripMenuItem";
            this.模拟喷码赋值ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.模拟喷码赋值ToolStripMenuItem.Text = "模拟喷码赋值";
            this.模拟喷码赋值ToolStripMenuItem.Click += new System.EventHandler(this.模拟喷码赋值ToolStripMenuItem_Click);
            // 
            // 读取公共变量ToolStripMenuItem
            // 
            this.读取公共变量ToolStripMenuItem.Name = "读取公共变量ToolStripMenuItem";
            this.读取公共变量ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.读取公共变量ToolStripMenuItem.Text = "读取公共变量";
            this.读取公共变量ToolStripMenuItem.Click += new System.EventHandler(this.读取公共变量ToolStripMenuItem_Click);
            // 
            // 启动关工程序ToolStripMenuItem
            // 
            this.启动关工程序ToolStripMenuItem.Name = "启动关工程序ToolStripMenuItem";
            this.启动关工程序ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.启动关工程序ToolStripMenuItem.Text = "启动关工程序";
            this.启动关工程序ToolStripMenuItem.Click += new System.EventHandler(this.启动关工程序ToolStripMenuItem_Click);
            // 
            // sendMessageToolStripMenuItem
            // 
            this.sendMessageToolStripMenuItem.Name = "sendMessageToolStripMenuItem";
            this.sendMessageToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.sendMessageToolStripMenuItem.Text = "SendMessage";
            this.sendMessageToolStripMenuItem.Click += new System.EventHandler(this.sendMessageToolStripMenuItem_Click);
            // 
            // 测试串口打印ToolStripMenuItem
            // 
            this.测试串口打印ToolStripMenuItem.Name = "测试串口打印ToolStripMenuItem";
            this.测试串口打印ToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.测试串口打印ToolStripMenuItem.Text = "测试串口打印";
            this.测试串口打印ToolStripMenuItem.Click += new System.EventHandler(this.测试串口打印ToolStripMenuItem_Click);
            // 
            // SelecttSpButton
            // 
            this.SelecttSpButton.AutoSize = false;
            this.SelecttSpButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.SelecttSpButton.Image = ((System.Drawing.Image)(resources.GetObject("SelecttSpButton.Image")));
            this.SelecttSpButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SelecttSpButton.Name = "SelecttSpButton";
            this.SelecttSpButton.Size = new System.Drawing.Size(80, 52);
            this.SelecttSpButton.Text = "查询";
            this.SelecttSpButton.Visible = false;
            this.SelecttSpButton.Click += new System.EventHandler(this.SelecttSpButton_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(63, 45);
            this.toolStripLabel1.Text = "设备名称";
            // 
            // Device_tSCboBox
            // 
            this.Device_tSCboBox.AutoSize = false;
            this.Device_tSCboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Device_tSCboBox.Name = "Device_tSCboBox";
            this.Device_tSCboBox.Size = new System.Drawing.Size(70, 21);
            this.Device_tSCboBox.Click += new System.EventHandler(this.Device_tSCboBox_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator4,
            this.数据BToolStripMenuItem,
            this.看板ToolStripMenuItem});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(77, 45);
            this.toolStripDropDownButton2.Text = "变量导入";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(126, 6);
            // 
            // 数据BToolStripMenuItem
            // 
            this.数据BToolStripMenuItem.Name = "数据BToolStripMenuItem";
            this.数据BToolStripMenuItem.Size = new System.Drawing.Size(129, 24);
            this.数据BToolStripMenuItem.Text = "数据(&B)";
            this.数据BToolStripMenuItem.Click += new System.EventHandler(this.数据BToolStripMenuItem_Click);
            // 
            // 看板ToolStripMenuItem
            // 
            this.看板ToolStripMenuItem.Name = "看板ToolStripMenuItem";
            this.看板ToolStripMenuItem.Size = new System.Drawing.Size(129, 24);
            this.看板ToolStripMenuItem.Text = "看板";
            this.看板ToolStripMenuItem.Click += new System.EventHandler(this.看板ToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.User_tSLabel,
            this.User_tSLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(3, 482);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 12, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1357, 38);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // User_tSLabel
            // 
            this.User_tSLabel.Name = "User_tSLabel";
            this.User_tSLabel.Size = new System.Drawing.Size(77, 33);
            this.User_tSLabel.Text = "登录用户：";
            // 
            // User_tSLabel2
            // 
            this.User_tSLabel2.Name = "User_tSLabel2";
            this.User_tSLabel2.Size = new System.Drawing.Size(49, 33);
            this.User_tSLabel2.Text = "用户栏";
            // 
            // textRecMsgtbox
            // 
            this.textRecMsgtbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textRecMsgtbox.Location = new System.Drawing.Point(259, 482);
            this.textRecMsgtbox.Multiline = true;
            this.textRecMsgtbox.Name = "textRecMsgtbox";
            this.textRecMsgtbox.Size = new System.Drawing.Size(924, 33);
            this.textRecMsgtbox.TabIndex = 3;
            this.textRecMsgtbox.Text = "textRec";
            this.textRecMsgtbox.TextChanged += new System.EventHandler(this.textRecMsgtbox_TextChanged);
            // 
            // galleryContainer4
            // 
            this.galleryContainer4.EnableGalleryPopup = false;
            this.galleryContainer4.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer4.MinimumSize = new System.Drawing.Size(150, 200);
            this.galleryContainer4.MultiLine = false;
            this.galleryContainer4.Name = "galleryContainer4";
            this.galleryContainer4.PopupUsesStandardScrollbars = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Location = new System.Drawing.Point(1032, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(118, 427);
            this.panel2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(25, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = "设备状态";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(5, 35);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(116, 386);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(7, 35);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(106, 386);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Controls.Add(this.DataInBtn);
            this.panel1.Controls.Add(this.YYKproducingBtn);
            this.panel1.Controls.Add(this.GAPHistoricalDataBtn);
            this.panel1.Controls.Add(this.GAPproducingBtn);
            this.panel1.Controls.Add(this.GAPPackingDataBtn);
            this.panel1.Controls.Add(this.MarkDateBtn);
            this.panel1.Controls.Add(this.offLineBtn);
            this.panel1.Controls.Add(this.PDFBtn);
            this.panel1.Controls.Add(this.YYKHistoricalDataBtn);
            this.panel1.Controls.Add(this.YYKPackingDataBtn);
            this.panel1.Controls.Add(this.producingBtn);
            this.panel1.Controls.Add(this.PaiDanBtn);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(215, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1150, 429);
            this.panel1.TabIndex = 23;
            // 
            // DataInBtn
            // 
            this.DataInBtn.BackColor = System.Drawing.Color.Transparent;
            this.DataInBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.DataInBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("DataInBtn.DownImage")));
            this.DataInBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("DataInBtn.HoverImage")));
            this.DataInBtn.Location = new System.Drawing.Point(296, 293);
            this.DataInBtn.Name = "DataInBtn";
            this.DataInBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("DataInBtn.NormalImage")));
            this.DataInBtn.Size = new System.Drawing.Size(96, 91);
            this.DataInBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.DataInBtn.TabIndex = 19;
            this.DataInBtn.TabStop = false;
            this.DataInBtn.ToolTipText = null;
            this.DataInBtn.Click += new System.EventHandler(this.DataInBtn_Click);
            // 
            // YYKproducingBtn
            // 
            this.YYKproducingBtn.BackColor = System.Drawing.Color.Transparent;
            this.YYKproducingBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.YYKproducingBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("YYKproducingBtn.DownImage")));
            this.YYKproducingBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("YYKproducingBtn.HoverImage")));
            this.YYKproducingBtn.Location = new System.Drawing.Point(177, 118);
            this.YYKproducingBtn.Name = "YYKproducingBtn";
            this.YYKproducingBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("YYKproducingBtn.NormalImage")));
            this.YYKproducingBtn.Size = new System.Drawing.Size(88, 81);
            this.YYKproducingBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.YYKproducingBtn.TabIndex = 18;
            this.YYKproducingBtn.TabStop = false;
            this.YYKproducingBtn.ToolTipText = null;
            this.YYKproducingBtn.Click += new System.EventHandler(this.YYKproducingBtn_Click);
            // 
            // GAPHistoricalDataBtn
            // 
            this.GAPHistoricalDataBtn.BackColor = System.Drawing.Color.Transparent;
            this.GAPHistoricalDataBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.GAPHistoricalDataBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("GAPHistoricalDataBtn.DownImage")));
            this.GAPHistoricalDataBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("GAPHistoricalDataBtn.HoverImage")));
            this.GAPHistoricalDataBtn.Location = new System.Drawing.Point(305, 205);
            this.GAPHistoricalDataBtn.Name = "GAPHistoricalDataBtn";
            this.GAPHistoricalDataBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("GAPHistoricalDataBtn.NormalImage")));
            this.GAPHistoricalDataBtn.Size = new System.Drawing.Size(94, 81);
            this.GAPHistoricalDataBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.GAPHistoricalDataBtn.TabIndex = 17;
            this.GAPHistoricalDataBtn.TabStop = false;
            this.GAPHistoricalDataBtn.ToolTipText = null;
            this.GAPHistoricalDataBtn.Click += new System.EventHandler(this.GAPHistoricalDataBtn_Click);
            // 
            // GAPproducingBtn
            // 
            this.GAPproducingBtn.BackColor = System.Drawing.Color.Transparent;
            this.GAPproducingBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.GAPproducingBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("GAPproducingBtn.DownImage")));
            this.GAPproducingBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("GAPproducingBtn.HoverImage")));
            this.GAPproducingBtn.Location = new System.Drawing.Point(177, 205);
            this.GAPproducingBtn.Name = "GAPproducingBtn";
            this.GAPproducingBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("GAPproducingBtn.NormalImage")));
            this.GAPproducingBtn.Size = new System.Drawing.Size(88, 81);
            this.GAPproducingBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.GAPproducingBtn.TabIndex = 16;
            this.GAPproducingBtn.TabStop = false;
            this.GAPproducingBtn.ToolTipText = null;
            this.GAPproducingBtn.Click += new System.EventHandler(this.GAPproducingBtn_Click);
            // 
            // GAPPackingDataBtn
            // 
            this.GAPPackingDataBtn.BackColor = System.Drawing.Color.Transparent;
            this.GAPPackingDataBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.GAPPackingDataBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("GAPPackingDataBtn.DownImage")));
            this.GAPPackingDataBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("GAPPackingDataBtn.HoverImage")));
            this.GAPPackingDataBtn.Location = new System.Drawing.Point(44, 205);
            this.GAPPackingDataBtn.Name = "GAPPackingDataBtn";
            this.GAPPackingDataBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("GAPPackingDataBtn.NormalImage")));
            this.GAPPackingDataBtn.Size = new System.Drawing.Size(88, 81);
            this.GAPPackingDataBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.GAPPackingDataBtn.TabIndex = 15;
            this.GAPPackingDataBtn.TabStop = false;
            this.GAPPackingDataBtn.ToolTipText = null;
            this.GAPPackingDataBtn.Click += new System.EventHandler(this.GAPPackingDataBtn_Click);
            // 
            // MarkDateBtn
            // 
            this.MarkDateBtn.BackColor = System.Drawing.Color.Transparent;
            this.MarkDateBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.MarkDateBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("MarkDateBtn.DownImage")));
            this.MarkDateBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("MarkDateBtn.HoverImage")));
            this.MarkDateBtn.Location = new System.Drawing.Point(175, 291);
            this.MarkDateBtn.Name = "MarkDateBtn";
            this.MarkDateBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("MarkDateBtn.NormalImage")));
            this.MarkDateBtn.Size = new System.Drawing.Size(88, 93);
            this.MarkDateBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.MarkDateBtn.TabIndex = 14;
            this.MarkDateBtn.TabStop = false;
            this.MarkDateBtn.ToolTipText = null;
            this.MarkDateBtn.Click += new System.EventHandler(this.MarkDateBtn_Click);
            // 
            // offLineBtn
            // 
            this.offLineBtn.BackColor = System.Drawing.Color.Transparent;
            this.offLineBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.offLineBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("offLineBtn.DownImage")));
            this.offLineBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("offLineBtn.HoverImage")));
            this.offLineBtn.Location = new System.Drawing.Point(44, 291);
            this.offLineBtn.Name = "offLineBtn";
            this.offLineBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("offLineBtn.NormalImage")));
            this.offLineBtn.Size = new System.Drawing.Size(88, 86);
            this.offLineBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.offLineBtn.TabIndex = 13;
            this.offLineBtn.TabStop = false;
            this.offLineBtn.ToolTipText = null;
            this.offLineBtn.Click += new System.EventHandler(this.offLineBtn_Click);
            // 
            // PDFBtn
            // 
            this.PDFBtn.BackColor = System.Drawing.Color.Transparent;
            this.PDFBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.PDFBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("PDFBtn.DownImage")));
            this.PDFBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("PDFBtn.HoverImage")));
            this.PDFBtn.Location = new System.Drawing.Point(428, 205);
            this.PDFBtn.Name = "PDFBtn";
            this.PDFBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("PDFBtn.NormalImage")));
            this.PDFBtn.Size = new System.Drawing.Size(95, 81);
            this.PDFBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PDFBtn.TabIndex = 12;
            this.PDFBtn.TabStop = false;
            this.PDFBtn.ToolTipText = null;
            this.PDFBtn.Click += new System.EventHandler(this.PDFBtn_Click);
            // 
            // YYKHistoricalDataBtn
            // 
            this.YYKHistoricalDataBtn.BackColor = System.Drawing.Color.Transparent;
            this.YYKHistoricalDataBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.YYKHistoricalDataBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("YYKHistoricalDataBtn.DownImage")));
            this.YYKHistoricalDataBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("YYKHistoricalDataBtn.HoverImage")));
            this.YYKHistoricalDataBtn.Location = new System.Drawing.Point(305, 118);
            this.YYKHistoricalDataBtn.Name = "YYKHistoricalDataBtn";
            this.YYKHistoricalDataBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("YYKHistoricalDataBtn.NormalImage")));
            this.YYKHistoricalDataBtn.Size = new System.Drawing.Size(88, 81);
            this.YYKHistoricalDataBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.YYKHistoricalDataBtn.TabIndex = 11;
            this.YYKHistoricalDataBtn.TabStop = false;
            this.YYKHistoricalDataBtn.ToolTipText = null;
            this.YYKHistoricalDataBtn.Click += new System.EventHandler(this.YYKHistoricalDataBtn_Click);
            // 
            // YYKPackingDataBtn
            // 
            this.YYKPackingDataBtn.BackColor = System.Drawing.Color.Transparent;
            this.YYKPackingDataBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.YYKPackingDataBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("YYKPackingDataBtn.DownImage")));
            this.YYKPackingDataBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("YYKPackingDataBtn.HoverImage")));
            this.YYKPackingDataBtn.Location = new System.Drawing.Point(44, 118);
            this.YYKPackingDataBtn.Name = "YYKPackingDataBtn";
            this.YYKPackingDataBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("YYKPackingDataBtn.NormalImage")));
            this.YYKPackingDataBtn.Size = new System.Drawing.Size(88, 81);
            this.YYKPackingDataBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.YYKPackingDataBtn.TabIndex = 10;
            this.YYKPackingDataBtn.TabStop = false;
            this.YYKPackingDataBtn.ToolTipText = null;
            this.YYKPackingDataBtn.Click += new System.EventHandler(this.YYKPackingDataBtn_Click);
            // 
            // producingBtn
            // 
            this.producingBtn.BackColor = System.Drawing.Color.Transparent;
            this.producingBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.producingBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("producingBtn.DownImage")));
            this.producingBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("producingBtn.HoverImage")));
            this.producingBtn.Location = new System.Drawing.Point(175, 31);
            this.producingBtn.Name = "producingBtn";
            this.producingBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("producingBtn.NormalImage")));
            this.producingBtn.Size = new System.Drawing.Size(88, 81);
            this.producingBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.producingBtn.TabIndex = 9;
            this.producingBtn.TabStop = false;
            this.producingBtn.ToolTipText = null;
            this.producingBtn.Click += new System.EventHandler(this.producingBtn_Click);
            // 
            // PaiDanBtn
            // 
            this.PaiDanBtn.BackColor = System.Drawing.Color.Transparent;
            this.PaiDanBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.PaiDanBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("PaiDanBtn.DownImage")));
            this.PaiDanBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("PaiDanBtn.HoverImage")));
            this.PaiDanBtn.Location = new System.Drawing.Point(44, 31);
            this.PaiDanBtn.Name = "PaiDanBtn";
            this.PaiDanBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("PaiDanBtn.NormalImage")));
            this.PaiDanBtn.Size = new System.Drawing.Size(88, 81);
            this.PaiDanBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PaiDanBtn.TabIndex = 4;
            this.PaiDanBtn.TabStop = false;
            this.PaiDanBtn.ToolTipText = null;
            this.PaiDanBtn.Click += new System.EventHandler(this.PaiDanBtn_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.BackColor = System.Drawing.Color.SlateGray;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.OthersimgBtn);
            this.panel3.Controls.Add(this.GAPimgBtn);
            this.panel3.Controls.Add(this.YYKimgBtn);
            this.panel3.Controls.Add(this.PaidanimgBtn);
            this.panel3.Location = new System.Drawing.Point(3, 50);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(208, 430);
            this.panel3.TabIndex = 10;
            // 
            // OthersimgBtn
            // 
            this.OthersimgBtn.BackColor = System.Drawing.Color.Transparent;
            this.OthersimgBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.OthersimgBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("OthersimgBtn.DownImage")));
            this.OthersimgBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("OthersimgBtn.HoverImage")));
            this.OthersimgBtn.Location = new System.Drawing.Point(24, 317);
            this.OthersimgBtn.Name = "OthersimgBtn";
            this.OthersimgBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("OthersimgBtn.NormalImage")));
            this.OthersimgBtn.Size = new System.Drawing.Size(154, 54);
            this.OthersimgBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.OthersimgBtn.TabIndex = 3;
            this.OthersimgBtn.TabStop = false;
            this.OthersimgBtn.ToolTipText = null;
            this.OthersimgBtn.Click += new System.EventHandler(this.OthersimgBtn_Click);
            // 
            // GAPimgBtn
            // 
            this.GAPimgBtn.BackColor = System.Drawing.Color.Transparent;
            this.GAPimgBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.GAPimgBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("GAPimgBtn.DownImage")));
            this.GAPimgBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("GAPimgBtn.HoverImage")));
            this.GAPimgBtn.Location = new System.Drawing.Point(24, 231);
            this.GAPimgBtn.Name = "GAPimgBtn";
            this.GAPimgBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("GAPimgBtn.NormalImage")));
            this.GAPimgBtn.Size = new System.Drawing.Size(154, 54);
            this.GAPimgBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.GAPimgBtn.TabIndex = 2;
            this.GAPimgBtn.TabStop = false;
            this.GAPimgBtn.ToolTipText = null;
            this.GAPimgBtn.Click += new System.EventHandler(this.GAPimgBtn_Click);
            // 
            // YYKimgBtn
            // 
            this.YYKimgBtn.BackColor = System.Drawing.Color.Transparent;
            this.YYKimgBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.YYKimgBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("YYKimgBtn.DownImage")));
            this.YYKimgBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("YYKimgBtn.HoverImage")));
            this.YYKimgBtn.Location = new System.Drawing.Point(24, 144);
            this.YYKimgBtn.Name = "YYKimgBtn";
            this.YYKimgBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("YYKimgBtn.NormalImage")));
            this.YYKimgBtn.Size = new System.Drawing.Size(154, 54);
            this.YYKimgBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.YYKimgBtn.TabIndex = 1;
            this.YYKimgBtn.TabStop = false;
            this.YYKimgBtn.ToolTipText = null;
            this.YYKimgBtn.Click += new System.EventHandler(this.YYKimgBtn_Click);
            // 
            // PaidanimgBtn
            // 
            this.PaidanimgBtn.BackColor = System.Drawing.Color.Transparent;
            this.PaidanimgBtn.DialogResult = System.Windows.Forms.DialogResult.None;
            this.PaidanimgBtn.DownImage = ((System.Drawing.Image)(resources.GetObject("PaidanimgBtn.DownImage")));
            this.PaidanimgBtn.HoverImage = ((System.Drawing.Image)(resources.GetObject("PaidanimgBtn.HoverImage")));
            this.PaidanimgBtn.Location = new System.Drawing.Point(24, 57);
            this.PaidanimgBtn.Name = "PaidanimgBtn";
            this.PaidanimgBtn.NormalImage = ((System.Drawing.Image)(resources.GetObject("PaidanimgBtn.NormalImage")));
            this.PaidanimgBtn.Size = new System.Drawing.Size(154, 54);
            this.PaidanimgBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PaidanimgBtn.TabIndex = 0;
            this.PaidanimgBtn.TabStop = false;
            this.PaidanimgBtn.ToolTipText = null;
            this.PaidanimgBtn.Click += new System.EventHandler(this.PaidanimgBtn_Click);
            // 
            // MainrpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1365, 522);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.textRecMsgtbox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "MainrpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "分拣系统";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainrpForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataInBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YYKproducingBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAPHistoricalDataBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAPproducingBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAPPackingDataBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MarkDateBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offLineBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDFBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YYKHistoricalDataBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YYKPackingDataBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.producingBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaiDanBtn)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OthersimgBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAPimgBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YYKimgBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidanimgBtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton 用户管理tStDDownButton;
        private System.Windows.Forms.ToolStripMenuItem 用户管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox Device_tSCboBox;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem 数据BToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel User_tSLabel;
        private System.Windows.Forms.ToolStripStatusLabel User_tSLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripDropDownButton GAPtStpDropDownBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem 导入PDFFToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem 导入装箱EXCELZToolStripMenuItem1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox PotSpTBox;
        private System.Windows.Forms.ToolStripButton SelecttSpButton;
        private System.Windows.Forms.ToolStripMenuItem 打印标签队列ToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem gU数据导入ToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem 导入优衣库EXCELEToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem 赋值拍照信号ToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton othertStpDBtn;
        private System.Windows.Forms.ToolStripMenuItem PakinputtSpBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem 喷码日期输入ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem 喷码纸箱位置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 喷码队列ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem 模拟进箱ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 扫描条码重新计数ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 模拟码垛完成ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 查询寄存器值ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 测试ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 订单历史记录查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 打印ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem 读字符串ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem 看板ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 打印GAP侧面贴标信号ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 取消喷码编号ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 获取雅马哈计数ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 清除雅马哈计数ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 切换牌照号模板ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 换款信号ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 模拟喷码赋值ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 读取公共变量ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 启动关工程序ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendMessageToolStripMenuItem;
        private System.Windows.Forms.TextBox textRecMsgtbox;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private ControlExs.ImageButton PaidanimgBtn;
        private ControlExs.ImageButton YYKimgBtn;
        private ControlExs.ImageButton GAPimgBtn;
        private ControlExs.ImageButton OthersimgBtn;
        private ControlExs.ImageButton PaiDanBtn;
        private ControlExs.ImageButton DataInBtn;
        private ControlExs.ImageButton YYKproducingBtn;
        private ControlExs.ImageButton GAPHistoricalDataBtn;
        private ControlExs.ImageButton GAPproducingBtn;
        private ControlExs.ImageButton GAPPackingDataBtn;
        private ControlExs.ImageButton MarkDateBtn;
        private ControlExs.ImageButton offLineBtn;
        private ControlExs.ImageButton PDFBtn;
        private ControlExs.ImageButton YYKHistoricalDataBtn;
        private ControlExs.ImageButton YYKPackingDataBtn;
        private ControlExs.ImageButton producingBtn;
        private System.Windows.Forms.ToolStripMenuItem 测试串口打印ToolStripMenuItem;
    }
}

