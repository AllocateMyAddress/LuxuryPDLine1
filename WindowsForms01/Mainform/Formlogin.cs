﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using A.BLL;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Web.Security;  //安全加密
using A.DBUility;
using System.IO;


namespace WindowsForms01
{
    public partial class Formlogin : Form
    {
        public int count = 0;
        public DataRow dr;
        public static readonly string CONN_STRsql = Connection.GetSqlServerConnectionString();
        A.BLL.newsContent content1 = new A.BLL.newsContent();
        ShieldForm shieldform = null;
        public Formlogin()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            try
            {
                string sqlstr = "SELECT uName FROM dbo.UserInfo ORDER BY dDate ";
                DataSet ds = new DataSet();
                ds = content1.Select_nothing(sqlstr);
                UNamecbBox.DataSource = ds.Tables[0];
                UNamecbBox.DisplayMember = "uName";
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                if (ex.Message.ToString().Contains(" SQL Server 建立连接时出现与网络相关的或特定于实例的错误"))
                {
                    CsFormShow.MessageBoxFormShow("请检查1号线上位机是否正常开启和网络是否正常");
                }
                MessageBox.Show("提示："+ex.Message);
                System.Environment.Exit(0);
                //throw new Exception("提示：" + ex);
            }

        }

        private void Loginbttn_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckInput() == true && Checkpwd() == true)
                {
                    this.DialogResult = DialogResult.OK;    //返回一个登录成功的对话框状态
                    this.Close();    //关闭登录窗口
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        public bool CheckInput()
        {
            try
            {
                if (string.IsNullOrEmpty(this.UNamecbBox.Text.Trim()))
                {
                    MessageBox.Show("用户名不能为空！");
                    this.UNamecbBox.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(this.PwdTBOX.Text.Trim()))
                {
                    MessageBox.Show("密码不能为空！");
                    this.PwdTBOX.Focus();
                    return false;
                }

            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示："+err.Message);
            }

            return true;
        }

        private bool Checkpwd()
        {
            string pwd = this.PwdTBOX.Text.Trim();
            string pwd_jiami = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "md5");
            string username = this.UNamecbBox.Text.Trim();
            stcUserData.M_username = username;
            string selCmd = string.Format("select password from userINfo where uName='{0}'", username);
            string password = "";
            //using (SqlConnection CONN = new SqlConnection(CONN_STRsql))
            //{
            //    CONN.Open();
            //    SqlCommand cmd = new SqlCommand(selCmd, CONN);

            //    int a = cmd.ExecuteNonQuery();
            //    password = cmd.ExecuteScalar().ToString();

            //}
            try
            {
                DataSet ds2 = content1.Select_nothing(selCmd);
                if (ds2.Tables[0].Rows.Count != 0)
                {
                    password = ds2.Tables[0].Rows[0]["password"].ToString();
                    if (password == pwd_jiami)
                    {

                        MessageBox.Show("登录成功");

                        return true;
                    }
                    else
                    {
                        MessageBox.Show("密码错误！");
                        this.PwdTBOX.Focus();
                        return false;
                    }

                }
                else
                {
                    MessageBox.Show("用户名有误");
                    this.UNamecbBox.Focus();
                    return false;
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }


        }


        private void PwdTBOX_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)//如果输入的是回车键  
            {
                this.Loginbttn_Click(sender, e);//触发button事件  
            }
        }

        private void Loginbttn_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (CheckInput() == true && Checkpwd() ==true)//admin,admin默认用户
                {
                    this.DialogResult = DialogResult.OK;    //返回一个登录成功的对话框状态
                    this.Close();    //关闭登录窗口
                    this.Dispose();
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }

        private void Formlogin_Load(object sender, EventArgs e)
        {

        }

        private void userTBOX_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
