﻿namespace WindowsForms01
{
    partial class Formlogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formlogin));
            this.Loginbttn = new System.Windows.Forms.Button();
            this.PwdTBOX = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.UNamecbBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // Loginbttn
            // 
            this.Loginbttn.Location = new System.Drawing.Point(134, 163);
            this.Loginbttn.Name = "Loginbttn";
            this.Loginbttn.Size = new System.Drawing.Size(91, 36);
            this.Loginbttn.TabIndex = 9;
            this.Loginbttn.Text = "登陆";
            this.Loginbttn.UseVisualStyleBackColor = true;
            this.Loginbttn.Click += new System.EventHandler(this.Loginbttn_Click_1);
            // 
            // PwdTBOX
            // 
            this.PwdTBOX.Location = new System.Drawing.Point(112, 100);
            this.PwdTBOX.Name = "PwdTBOX";
            this.PwdTBOX.PasswordChar = '*';
            this.PwdTBOX.Size = new System.Drawing.Size(143, 25);
            this.PwdTBOX.TabIndex = 8;
            this.PwdTBOX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PwdTBOX_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "密码";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "用户名";
            // 
            // UNamecbBox
            // 
            this.UNamecbBox.FormattingEnabled = true;
            this.UNamecbBox.Location = new System.Drawing.Point(112, 44);
            this.UNamecbBox.Name = "UNamecbBox";
            this.UNamecbBox.Size = new System.Drawing.Size(140, 23);
            this.UNamecbBox.TabIndex = 10;
            // 
            // Formlogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 255);
            this.Controls.Add(this.UNamecbBox);
            this.Controls.Add(this.Loginbttn);
            this.Controls.Add(this.PwdTBOX);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formlogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "用户登陆";
            this.Load += new System.EventHandler(this.Formlogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Loginbttn;
        private System.Windows.Forms.TextBox PwdTBOX;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox UNamecbBox;
    }
}