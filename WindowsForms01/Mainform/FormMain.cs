﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using A.BLL;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Web.Security;  //安全加密
//using A.DBUility;
using iTextSharp.text.pdf;
using Spire.Pdf.Graphics;
using Spire.Pdf;
using PDFParser;
using GetData;
using MCData;
using System.Threading;
using System.IO;
using Keyence.AutoID.SDK;
using System.Diagnostics;
using Soketprint;
using System.Collections;
//using CopyDataStruct;
using System.Runtime.InteropServices;
using System.Configuration;

namespace WindowsForms01
{

    public partial class MainrpForm : Form
    {
        #region  变量定义
        PdfForm pdfform = null;
        //ScanForm scanform = null;
        userForm userform = null;
        PrintForm printform = null;
        PackingForm packingform = null;
        YouyikuForm youyikuform = null;
        ScanPackingForm scanpackingform = null;
        MarkPositionForm markpositionform = null;
        MarkingDateForm markingdateform = null;
        MarkingLineUpForm MarkingLineUpform = null;
        PlcValForm  plcValform  = null;
        ShieldForm shieldform = null;
        StockForm Stockform = null;
        ProducingForm Producingform = null;
        public int count = 0;
        int readplc = 0;
        public DataRow dr;
        string GoDevice = "";
        DataSet dsget = new DataSet();
        DataTable dtget = new DataTable();
        DataTable M_dt = new DataTable();
        DataTable dtexist = new DataTable();
        OpenFileDialog ofd = new OpenFileDialog();
        //Cs_struct_bao Cs_struct_bao = new Cs_struct_bao();
        //A.BLL.newsContent content1 = new A.BLL.newsContent();
        static int setupcount = 2;
        static string MarkIP = "";
        static string MarkIPoint = "";
        static string YaMaHa1IP = "";
        static string YaMaHa1IPoint = "";
        static string YaMaHa2IP = "";
        static string YaMaHa2IPoint = "";
        static string PrintIP = "";
        static string PrintIPoint = "";
        static string Camera1IP = "";
        static string CameraRec1IPoint = "";
        static string CameraSend1IPoint = "";
        static string Camera2IP = "";
        static string CameraRec2IPoint = "";
        static string CameraSend2IPoint = "";
        static string Scanning1IP = "";
        static string Scanning1IPoint = "";
        static string Scanning2IP = "";
        static string Scanning2IPoint = "";
        public static string PcserverIP = "";
        public static string PcserverIPoint = "";
        public static string Devicename = "";
        public static string Plc1IP = "";
        public static string Plc1IPoint = "";
        public static string Plc2IP = "";
        public static string Plc2IPoint = "";
        public static string M_Postype = "";
        public static int ceshi = 0;
        const int WM_COPYDATA = 0x004A;
        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("User32.dll", EntryPoint = "FindWindowEx")]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpClassName, string lpWindowName);
        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage(IntPtr hwnd, int wMsg, int wParam, string lParam);
        //public static string PlcIP = "";
        //public static string PlcIPoint = "";
        public static CsToPcSoket csToPcSoket;
       // List<Cs_struct_bao.stc_bao> stc_Baos = new List<Cs_struct_bao.stc_bao>();
        //object lokerreadplc = new object();
        struct stc_adrtype
        {
            public static List<string> LL_TEXT_STR = new List<string>();
            public static List<string> L_TEXT = new List<string>();
        }
        #endregion
        MCData.MCdata mCdata = new MCdata();
        GetPlcBao csGetBao1;
        YYKMarkLineUpForm YYKMarklineupform = null;
        HistoricRcordsForm historicRcordsform = null;
        YYKHistoricRcordsForm YYKHistoricRcordsform = null;
        //YYKGUSchedulingForm YYKGUSchedulingform = null;
        SchedulingTableForm SchedulingTableform = null;
        CartonNOHistoricRcordsForm CartonNOHistoricRcordsform = null;
       // SchedulingSortForm schedulingSortform = null;
        public MainrpForm()
        {
            InitializeComponent();
            CsFormShow.FormShowDialog(shieldform, this, "WindowsForms01.ShieldForm");
            GetDeviceIP();
            GetDeviceType();
            Devicename = Device_tSCboBox.ComboBox.Text.ToString().Trim();
            CSReadPlc.DevName = Devicename;
            pictureBox1.Visible = false;
            textRecMsgtbox.ReadOnly = true;
            textRecMsgtbox.Visible = false;
            Cslogfun.DeleteLog(30);
            if (!stcUserData.M_username.Contains("starcity"))
            {
                //toolStripDropDownButton3.Visible = false;
                toolStripDropDownButton2.Visible = false;
                toolStripDropDownButton1.Visible = false;
                打印标签队列ToolStripMenuItem.Visible = false;
                toolStripMenuItem13.Visible = false;
                toolStripMenuItem17.Visible = false;
            }
            SetupDevice();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = true;
            this.MinimizeBox = false;
            // CsInitialization.SetupIps();
            NewShow();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;//设置该属性 为false
        }

        private void MainrpForm_FormClosing(object sender, FormClosingEventArgs e)//主窗体关闭事件
        {
            Formclose.Closeform(this, e);
        }

        #region 用户管理按钮触发事件
        private void 用户管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(userform, this, "WindowsForms01.userForm");
        }
        #endregion

        #region 导入按钮触发事件
        private void 导入信息1ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        #endregion
        /// <summary>
        /// 获取设备IP和端口号
        /// </summary>
        public void GetDeviceIP()
        {
            try
            {
                string strsql = "SELECT*FROM DeviceIP";
                DataTable dt_ip = CsFormShow.GoSqlSelect(strsql);
                for (int i = 0; i < dt_ip.Rows.Count; i++)
                {
                    if (dt_ip.Rows[i]["DeviceName"].ToString() == "M_PLC1_IP")
                    {
                        Plc1IP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        Plc1IPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                    }
                    else if (dt_ip.Rows[i]["DeviceName"].ToString() == "M_PLC2_IP")
                    {
                        Plc2IP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        Plc2IPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                    }
                    else if (dt_ip.Rows[i]["DeviceName"].ToString() == "M_marking_IP")
                    {
                        MarkIP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        MarkIPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                        CSReadPlc.MarkIP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        CSReadPlc.MarkIPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                    }
                    else if (dt_ip.Rows[i]["DeviceName"].ToString() == "M_yamaha1_IP")
                    {
                        YaMaHa1IP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        YaMaHa1IPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                        CSReadPlc.YaMaHaIP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        CSReadPlc.YaMaHaIPoint= dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                    }
                    else if (dt_ip.Rows[i]["DeviceName"].ToString() == "M_yamaha2_IP")
                    {
                        YaMaHa2IP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        YaMaHa2IPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                    }
                    else if (dt_ip.Rows[i]["DeviceName"].ToString() == "M_print_IP")
                    {
                        PrintIP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        PrintIPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                        CSReadPlc.PrintIP= dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        CSReadPlc.PrintIPoint= dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                    }
                    else if (dt_ip.Rows[i]["DeviceName"].ToString() == "M_camera1_IP")
                    {
                        Camera1IP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        CameraRec1IPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                        CameraSend1IPoint = dt_ip.Rows[i]["DeviceIPoint_out"].ToString().Trim();
                    }
                    else if (dt_ip.Rows[i]["DeviceName"].ToString() == "M_camera2_IP")
                    {
                        Camera2IP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        CameraRec2IPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                        CameraSend2IPoint = dt_ip.Rows[i]["DeviceIPoint_out"].ToString().Trim();
                    }
                    else if (dt_ip.Rows[i]["DeviceName"].ToString() == "M_Scanning1_IP")
                    {
                        Scanning1IP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        Scanning1IPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                        M_stcScanData.IP = Scanning1IP;
                        M_stcScanData.IPoint = Scanning1IPoint;
                    }
                    else if (dt_ip.Rows[i]["DeviceName"].ToString() == "M_Scanning2_IP")
                    {
                        Scanning2IP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        Scanning2IPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                        M_stcScanData.IP = Scanning2IP;
                        M_stcScanData.IPoint = Scanning2IPoint;
                    }
                    else if (dt_ip.Rows[i]["DeviceName"].ToString() == "PCServer")
                    {
                        PcserverIP = dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        PcserverIPoint = dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                        CSReadPlc.ToPcIP= dt_ip.Rows[i]["DeviceIP"].ToString().Trim();
                        CSReadPlc.ToPcIPoint= dt_ip.Rows[i]["DeviceIPoint_in"].ToString().Trim();
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        /// <summary>
        /// 获取设备plc类型
        /// </summary>
        public void GetDeviceType()
        {
            //string strsql = "SELECT Device FROM dbo.MAINTABLE GROUP BY Device";
            //DataSet ds = content1.Select_nothing(strsql);
            //Device_tSCboBox.ComboBox.DataSource = ds.Tables[0];
            //Device_tSCboBox.ComboBox.DisplayMember = "Device";
           // this.Text = "分拣系统";
            if (CSReadPlc.code_v==1)
            {
                Device_tSCboBox.ComboBox.Items.Add("plc1");
                Device_tSCboBox.ComboBox.SelectedIndex = 0;
            }
            else if(CSReadPlc.code_v == 2)
            {
                Device_tSCboBox.ComboBox.Items.Add("plc2");
                Device_tSCboBox.ComboBox.SelectedIndex = 0;
            }
            User_tSLabel2.Text = stcUserData.M_username + ConfigurationManager.AppSettings["Version"]; 
        }
        /// <summary>
        /// 启动设备及运行线程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoDevicebtn_Click(object sender, EventArgs e)
        {
            SetupDeviceThread();
        }
        #region 导入excel
        private void 导入PDFPToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        #endregion
        #region 导入变量表
        private void 数据BToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                inexcel ss = new inexcel();
                // System.Windows.Forms.OpenFileDialog fd = new OpenFileDialog();
                ofd.Filter = "EXCEL文档(*.xls)|*.xls";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    string fileName = ofd.FileName;
                    GetData.GetExcelData getData = new GetExcelData();
                    M_dt = getData.bind(fileName, "Sheet1", "A1", "F");
                }
                //dataGridView2.DataSource = null;
                #region 录入变量数据到数据库
                if (M_dt.Rows.Count > 0)
                {
                    //this.dataGridView2.DataSource = M_dt;
                    DataTable dt_insert = new DataTable();
                    dt_insert = M_dt.Copy();
                    string sqlstr = "SELECT*FROM dbo.MAINTABLE ";
                    DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                    for (int i = 0; i < dt_insert.Rows.Count; i++)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (dt_insert.Rows[i]["TAG_ID"].ToString().Trim() ==dt.Rows[j]["TAG_ID"].ToString().Trim())
                                {
                                    dt_insert.Rows.Remove(dt_insert.Rows[i]);
                                }
                            }
                        }
                    }
                    if (dt_insert.Rows.Count > 0)
                    {
                        ss.insertToSql(dt_insert, "dbo.MAINTABLE");
                    }
                    //dt_insert.Columns["实际出货数量"].ColumnName = "OUTQTY";
                    MessageBox.Show("导入成功！");
                }
                else
                {
                    MessageBox.Show("没有数据！");
                }
                #endregion
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        #endregion

        /// <summary>
        /// 选择置为当前执行的订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 导入PDFFToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                CsFormShow.FormShowDialog(pdfform, this, "WindowsForms01.PdfForm");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }

        private void 导入装箱EXCELZToolStripMenuItem1_Click(object sender, EventArgs e)
        {
           // M_Postype = potypetSpCBox.Text.Trim();
            CsFormShow.FormShowDialog(packingform, this, "WindowsForms01.PackingForm");
        }

        private void 删除数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //if (stcUserData.M_username != "admin")
                //{
                //    MessageBox.Show("提示：非admin管理员不能进行此操作");
                //    return;
                //}
                //Delet_click.Delete_TSMenuItem_Click(this, dataGridView2, "dbo.HHXSDH");
                //NewdataGridView_show();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err.Message);
            }
        }


        private void 打印ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Soketprint.SoketPrint soketprint = new Soketprint.SoketPrint();
            csGetBao1.WriteToPlc("M_printsignal", 1, CSReadPlc.DevName);
            //CsPrint.deleteprint(soketprint, PrintIP, PrintIPoint);
            // Dictionary<string, Dictionary<string, Cs_struct_bao.stc_bao_tag>> Dic_val_vlue = CSReadPlc.Dic_val_vlue;
        }
        private void SelecttSpButton_Click(object sender, EventArgs e)
        {
            //NewMainShow(PotSpTBox.TextBox.Text.ToString().Trim());
        }
        /// <summary>
        /// 启动设备
        /// </summary>
        public void SetupDeviceThread()
        {
            try
            {
                //if (mCdata.IsConnect())
                //{
                //    MessageBox.Show("设备已启动，如需更换设备请退出再重新选择！");
                //    return;
                //}
                if (Devicename == "plc1" && (User_tSLabel2.Text.Trim().Contains("admin") || User_tSLabel2.Text.Trim().Contains("starcity")))
                {
                    if (!mCdata.IsConnect())
                    {
                        mCdata.NewTcp(Plc1IP,int.Parse(Plc1IPoint));
                    }
                    if (mCdata.IsConnect())
                    {
                        csGetBao1 = new GetPlcBao(mCdata, CSReadPlc.DevName, Plc1IP,int.Parse(Plc1IPoint));//传递本次读写plc连接实例
                        CsMarking ngMarking = new CsMarking(mCdata, csGetBao1);
                        CsTrg csTrg = new CsTrg(mCdata, csGetBao1);
                        CsSendYaMaHa csSendYaMaHa1 = new CsSendYaMaHa(mCdata, csGetBao1);
                        CsScanning csScanning = new CsScanning(mCdata, csGetBao1);
                        CsRWPlc csRWPlc = new CsRWPlc(mCdata, csGetBao1);
                        CsPrint csPrint = new CsPrint(mCdata, csGetBao1);
                        csToPcSoket = new CsToPcSoket();
                        csRWPlc.ReadPlcTh(pictureBox1, pictureBox2);
                        csRWPlc.WritePlcTh();
                        csRWPlc.ReadPackingNumber();
                        /*csScanning.Scanning(Scanning1IP, int.Parse(Scanning1IPoint));*/
                        if (stcUserData.IsMarkingSetup)
                        {
                            ngMarking.MarkingSetup(MarkIP, MarkIPoint);
                            csTrg.TrgSetup(Camera1IP, CameraRec1IPoint, CameraSend1IPoint);
                        }
                        if (stcUserData.IsYamahaSetup)
                        {
                            csSendYaMaHa1.Sendchange(YaMaHa1IP, YaMaHa1IPoint);
                        }
                        CsInitialization.SetupLineUp();
                       // Cslogfun.WriteToLog("连接ip:" + Plc1IP + "端口：" + Plc1IPoint + "plc1成功开始启动线程");
                    }
                }
                else if (Devicename == "plc2" && (User_tSLabel2.Text.Trim().Contains("admin") || User_tSLabel2.Text.Trim().Contains("starcity")))
                {
                    if (!mCdata.IsConnect())
                    {
                        mCdata.NewTcp(Plc2IP,int.Parse(Plc2IPoint));
                    }
                    if (mCdata.IsConnect())
                    {
                        csGetBao1 = new GetPlcBao(mCdata, CSReadPlc.DevName, Plc2IP,int.Parse(Plc2IPoint));
                        CsMarking ngMarking = new CsMarking(mCdata, csGetBao1);
                        CsTrg csTrg = new CsTrg(mCdata, csGetBao1);
                        CsSendYaMaHa csSendYaMaHa2 = new CsSendYaMaHa(mCdata, csGetBao1);
                        CsPrint csPrint = new CsPrint(mCdata, csGetBao1);
                        CsScanning csScanning = new CsScanning(mCdata, csGetBao1);
                        CsRWPlc csRWPlc = new CsRWPlc(mCdata, csGetBao1);
                        csToPcSoket = new CsToPcSoket();
                        csRWPlc.ReadPlcAdr();
                        csRWPlc.ReadPlcTh(pictureBox1, pictureBox2);
                        csRWPlc.WritePlcTh();
                        csRWPlc.ReadPackingNumber();
                        /*csScanning.Scanning(Scanning2IP, int.Parse(Scanning2IPoint));*/
                        if (stcUserData.IsMarkingSetup)
                        {
                            ngMarking.MarkingSetup(MarkIP, MarkIPoint);
                        }
                        if (stcUserData.IsYamahaSetup)
                        {
                            csSendYaMaHa2.Sendchange(YaMaHa2IP, YaMaHa2IPoint);
                        }
                        if (stcUserData.IsPrintSetup)
                        {
                            csPrint.tcpprint(PrintIP, PrintIPoint);
                        }
                        if (stcUserData.IsSmallLabelPrintSetup)
                        {
                            csPrint.serial_print();
                        }
                        CsInitialization.SetupLineUp();
                       // Cslogfun.WriteToLog("连接ip:" + Plc2IP + "端口：" + Plc2IPoint + "plc2成功开始启动线程");
                    }
                }
                //MessageBox.Show("设备启动完毕");
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
        }
        public void SetupDevice(string type_)
        {
            try
            {
                if (Devicename == "plc1" && (User_tSLabel2.Text.Trim().Contains("admin") || User_tSLabel2.Text.Trim().Contains("starcity")))
                {
                    if (!mCdata.IsConnect())
                    {
                        mCdata.NewTcp(Plc1IP, int.Parse(Plc1IPoint));
                    }
                    if (mCdata.IsConnect())
                    {
                        csGetBao1 = new GetPlcBao(mCdata, CSReadPlc.DevName, Plc1IP,int.Parse(Plc1IPoint));//传递本次读写plc连接实例
                        CsMarking ngMarking = new CsMarking(mCdata, csGetBao1);
                        CsTrg csTrg = new CsTrg(mCdata, csGetBao1);
                        CsSendYaMaHa csSendYaMaHa1 = new CsSendYaMaHa(mCdata, csGetBao1);
                        CsPrint csPrint = new CsPrint(mCdata, csGetBao1);
                        CsScanning csScanning = new CsScanning(mCdata, csGetBao1);
                        CsRWPlc csRWPlc = new CsRWPlc(mCdata, csGetBao1);
                        csRWPlc.ReadPlcAdr();
                        csRWPlc.ReadPlcTh(pictureBox1, pictureBox2);
                        csRWPlc.WritePlcTh();
                        csRWPlc.ReadPackingNumber();
                        if (type_.Contains("Scanning"))
                        {
                            csScanning.Scanning(Scanning1IP, int.Parse(Scanning1IPoint));
                        }
                        else if (type_.Contains("YaMaHa"))
                        {
                           csSendYaMaHa1.Sendchange(YaMaHa1IP, YaMaHa1IPoint);
                        }
                        else if (type_.Contains("Marking"))
                        {
                            ngMarking.MarkingSetup(MarkIP, MarkIPoint);
                            csTrg.TrgSetup(Camera1IP, CameraRec1IPoint, CameraSend1IPoint);
                            csPrint.tcpprint(PrintIP, PrintIPoint);
                        }
                        CsInitialization.SetupLineUp();
                    }
                }
                else if (Devicename == "plc2" &&( User_tSLabel2.Text.Trim().Contains("admin") || User_tSLabel2.Text.Trim().Contains("starcity")))
                {
                    if (!mCdata.IsConnect())
                    {
                        mCdata.NewTcp(Plc2IP, int.Parse(Plc2IPoint));
                    }
                    if (mCdata.IsConnect())
                    {
                        csGetBao1 = new GetPlcBao(mCdata, CSReadPlc.DevName, Plc2IP, int.Parse(Plc2IPoint));
                        CsMarking ngMarking = new CsMarking(mCdata, csGetBao1);
                        CsTrg csTrg = new CsTrg(mCdata, csGetBao1);
                        CsSendYaMaHa csSendYaMaHa2 = new CsSendYaMaHa(mCdata, csGetBao1);
                        CsPrint csPrint = new CsPrint(mCdata, csGetBao1);
                        CsScanning csScanning = new CsScanning(mCdata, csGetBao1);
                        CsRWPlc csRWPlc = new CsRWPlc(mCdata, csGetBao1);
                        csRWPlc.ReadPlcAdr();
                        csRWPlc.ReadPlcTh(pictureBox1, pictureBox2);
                        csRWPlc.WritePlcTh();
                        csRWPlc.ReadPackingNumber();
                        if (type_.Contains("Scanning"))
                        {
                            csScanning.Scanning(Scanning2IP, int.Parse(Scanning2IPoint));
                        }
                        else if (type_.Contains("YaMaHa"))
                        {
                            csSendYaMaHa2.Sendchange(YaMaHa2IP, YaMaHa2IPoint);
                        }
                        else if (type_.Contains("Marking"))
                        {
                            ngMarking.MarkingSetup(MarkIP, MarkIPoint);
                            csPrint.tcpprint(PrintIP, PrintIPoint);
                        }
                        CsInitialization.SetupLineUp();
                    }
                }

                //MessageBox.Show("设备启动完毕");
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
        }
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                if (scanpackingform == null || scanpackingform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                {
                    scanpackingform = new ScanPackingForm(mCdata);
                    scanpackingform.Owner = this;
                    scanpackingform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    scanpackingform.ShowDialog();
                }
                else
                {
                    scanpackingform.Show();
                    scanpackingform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    scanpackingform.BringToFront();
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
        }

        private void PakinputtSpBtn_Click(object sender, EventArgs e)
        {

        }
        private void GoDevicetSpBtn_Click(object sender, EventArgs e)
        {
            SetupDevice();
        }

        private void 打印标签ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (mCdata.IsConnect())
                {
                    csGetBao1.WriteToPlc("M_printsignal", 1, CSReadPlc.DevName);
                }
                else
                {
                    MessageBox.Show("提示：请先启动设备");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }

        }
        private void 打印标签队列ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(printform, this, "WindowsForms01.PrintForm");
        }

        private void gU数据导入ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                CsFormShow.FormShowDialog(SchedulingTableform, this, "WindowsForms01.SchedulingTableForm");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        private void 导入优衣库EXCELEToolStripMenuItem_Click(object sender, EventArgs e)
        {
           CsFormShow.FormShowDialog(youyikuform, this, "WindowsForms01.YouyikuForm");
        }

        private void 切换拍照信号ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csGetBao1.WriteToPlc("M_PackingCode", 5, CSReadPlc.DevName);
        }

        private void 赋值拍照信号ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csGetBao1.WriteToPlc("M_Trg", 1, CSReadPlc.DevName);
            // Number = int.Parse(CSReadPlc.Dic_val_vlue["plc1"]["M_Trg"].strVAL_VALUES.Trim());
            //int Trg = CSReadPlc.GetPlcInt("M_Trg", CSReadPlc.DevName);//获取拍照信号
        }

        private void 控制扫描电机ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //int M_engine = CSReadPlc.GetPlcInt("Scan1", CSReadPlc.DevName);
            if (mCdata.IsConnect())
            {
                if (CSReadPlc.GetPlcInt("Scan1", CSReadPlc.DevName).ToString().Contains("1"))
                {
                    csGetBao1.WriteToPlc("M_engine", 0, CSReadPlc.DevName);//启动
                }
                else
                {
                    csGetBao1.WriteToPlc("M_engine", 1, CSReadPlc.DevName);//停止
                }
            }
        }


        private void PakinputtSpBtn_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (scanpackingform == null || scanpackingform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                {
                    scanpackingform = new ScanPackingForm(mCdata);
                    scanpackingform.Owner = this;
                    scanpackingform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    scanpackingform.ShowDialog();
                }
                else
                {
                    scanpackingform.Show();
                    scanpackingform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    scanpackingform.BringToFront();
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
        }

        private void 喷码日期输入ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(markingdateform, this, "WindowsForms01.MarkingDateForm");
        }

        private void 喷码纸箱位置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(markpositionform, this, "WindowsForms01.MarkPositionForm");
        }


        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(MarkingLineUpform, this, "WindowsForms01.MarkingLineUpForm");
        }

        private void 喷码队列ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShow(YYKMarklineupform, this, "WindowsForms01.YYKMarkLineUpForm");
        }

        private void 发送至PC服务器ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //CsToPcSoket csToPcSoket = new CsToPcSoket();
            //csToPcSoket.SoketConnetion(PcserverIP,PcserverIPoint);
            //string rec= csToPcSoket.Gocmd("我来了");
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //CsFormShow.FormShowDialog(gapMarkPositionForm,this,"WindowsForms01.GAPMarkPositionForm");
        }

        private void 手动打印标签ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (mCdata.IsConnect())
                {
                    csGetBao1.WriteToPlc("M_printsignal", 1, CSReadPlc.DevName);
                }
                else
                {
                    MessageBox.Show("提示：请先启动设备");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
        }

        private void 模拟进箱ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mCdata.IsConnect())
            {

                if (ceshi==0)
                {
                    csGetBao1.WriteToPlc("PakingCount", 0, CSReadPlc.DevName);
                    ceshi = 1;
                }
                // string lastcount = "";
                CsRWPlc.PakingCount = CSReadPlc.GetPlcInt("PakingCount", CSReadPlc.DevName).ToString().Trim();//当前单装箱数量
                if (CsRWPlc.PakingCount=="0")
                {
                   // lastcount = CsRWPlc.PakingCount;
                    //short num = (short)(int.Parse(CsRWPlc.PakingCount.Trim()) + 1);
                   csGetBao1.WriteToPlc("PakingCount", 1, CSReadPlc.DevName);
                   // csGetBao1.WriteToPlc("PlcChangePo", 1, CSReadPlc.DevName);
                }
            }
            else
            {
                MessageBox.Show("提示：请先启动设备");
            }
        }

        private void 扫描条码重新计数ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsInitialization.ScanningCountInit();
            MessageBox.Show("开始重新计数");
        }

        private void 扫描ToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void 模拟码垛完成ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csGetBao1.WriteToPlc("PalletizingCount", 1, CSReadPlc.DevName);//复位计数
            //if (CSReadPlc.DevName.Contains("plc1"))
            //{
            //    csGetBao1.WriteToPlc("PalletizingLineType", 1, CSReadPlc.DevName);//复位计数
            //}
            //else
            //{
            //    csGetBao1.WriteToPlc("PalletizingLineType", 2, CSReadPlc.DevName);//复位计数
            //}
            M_stcPalletizingData.PalletizingTest = "start";
            MessageBox.Show("码垛完成一次");
        }

        private void 只启动扫描设备ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (GoDevice == "")
                {
                    if (User_tSLabel2.Text.Trim().Contains("admin") || User_tSLabel2.Text.Trim().Contains("starcity"))
                    {
                        SetupDevice("Scanning");
                        if (mCdata.IsConnect())
                        {
                            GoDevice = "go";
                            pictureBox2.Visible = false;
                            pictureBox1.Visible = true;
                            MessageBox.Show("提示：设备启动完毕");
                        }
                        else
                        {
                            MessageBox.Show("提示：PLC设备未正常启动");
                        }
                    }
                    else
                    {
                        MessageBox.Show("提示：非管理员不能进行此操作");
                    }
                }
                else
                {
                    MessageBox.Show("提示：设备已经启动，请勿重复");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                MessageBox.Show("提示：" + ex.Message);
                throw new Exception("提示：" + ex.Message);
            }
        }

        private void 只启动机器人设备ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (GoDevice == "")
                {
                    if (User_tSLabel2.Text.Trim().Contains("admin") || User_tSLabel2.Text.Trim().Contains("starcity"))
                    {
                        SetupDevice("YaMaHa");
                        if (mCdata.IsConnect())
                        {
                            GoDevice = "go";
                            pictureBox2.Visible = false;
                            pictureBox1.Visible = true;
                            MessageBox.Show("提示：设备启动完毕");
                        }
                        else
                        {
                            MessageBox.Show("提示：PLC设备未正常启动");
                        }
                    }
                    else
                    {
                        MessageBox.Show("提示：非管理员不能进行此操作");
                    }
                }
                else
                {
                    MessageBox.Show("提示：设备已经启动，请勿重复");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                MessageBox.Show("提示：" + ex.Message);
                throw new Exception("提示：" + ex.Message);
            }
        }

        private void 只启动喷码设备ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (GoDevice == "")
                {
                    if (User_tSLabel2.Text.Trim().Contains("admin") || User_tSLabel2.Text.Trim().Contains("starcity"))
                    {
                        SetupDevice("Marking");
                        if (mCdata.IsConnect())
                        {
                            GoDevice = "go";
                            pictureBox2.Visible = false;
                            pictureBox1.Visible = true;
                            MessageBox.Show("提示：设备启动完毕");
                        }
                        else
                        {
                            MessageBox.Show("提示：PLC设备未正常启动");
                        }
                    }
                    else
                    {
                        MessageBox.Show("提示：非管理员不能进行此操作");
                    }
                }
                else
                {
                    MessageBox.Show("提示：设备已经启动，请勿重复");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                MessageBox.Show("提示：" + ex.Message);
                throw new Exception("提示：" + ex.Message);
            }
        }
        private void 查询寄存器值ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                if (mCdata.IsConnect())
                {
                    if (plcValform == null || plcValform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                    {
                        plcValform = new PlcValForm(csGetBao1, mCdata);
                        plcValform.Owner = this;
                        plcValform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        plcValform.ShowDialog();
                    }
                    else
                    {
                        plcValform.Show();
                        plcValform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                        plcValform.BringToFront();
                    }
                }
                else
                {
                    MessageBox.Show("设备未启动！！");   
                }

            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void 只启动PLCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (GoDevice == "")
                {
                    if (User_tSLabel2.Text.Trim().Contains("admin") || User_tSLabel2.Text.Trim().Contains("starcity"))
                    {
                        SetupDevice("PLC");
                        if (mCdata.IsConnect())
                        {
                            GoDevice = "go";
                            pictureBox2.Visible = false;
                            pictureBox1.Visible = true;
                            MessageBox.Show("提示：设备启动完毕");
                        }
                        else
                        {
                            MessageBox.Show("提示：PLC设备未正常启动");
                        }
                    }
                    else
                    {
                        MessageBox.Show("提示：非管理员不能进行此操作");
                    }
                }
                else
                {
                    MessageBox.Show("提示：设备已经启动，请勿重复");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                MessageBox.Show("提示：" + ex.Message);
                throw new Exception("提示：" + ex.Message);
            }
        }

        private void 切换扫描模式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsScaning.SoketScaning Soketscaning = new CsScaning.SoketScaning(CSReadPlc.DevName);
            bool  connet = Soketscaning.SoketConnetion(Scanning2IP, Scanning2IPoint);
            if (connet)
            {
               string str=Soketscaning.Gocmd("WP,250,1");//改变读取条码数量为2个
            }

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            historicRcordsform= (WindowsForms01.HistoricRcordsForm)CsFormShow.FormShow(historicRcordsform, null, "WindowsForms01.HistoricRcordsForm");
        }

        private void 测试ToolStripMenuItem_Click(object sender, EventArgs e)
        {
             Soketprint.SoketPrint M_soketprint = new SoketPrint(CSReadPlc.DevName);
             CsMarking csMarking = new CsMarking(mCdata,csGetBao1);
            //bool  M_Ismarkingconnet = M_soketprint.MarkingConnetion(MarkIP, MarkIPoint);//连接喷码机
            //if (M_Ismarkingconnet)
            //{
            //    string M_recstr = M_soketprint.Maketing("13", "13");//设置模板喷印内容
            //}
            //csMarking.ChangeMark(13,"2", M_soketprint);
            //csMarking.ChangeMark(14, "3", M_soketprint);
        }

        private void 订单历史记录查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            YYKHistoricRcordsform = (WindowsForms01.YYKHistoricRcordsForm)CsFormShow.FormShow(YYKHistoricRcordsform, null, "WindowsForms01.YYKHistoricRcordsForm");
        }

        private void 打印ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (mCdata.IsConnect())
                {
                    csGetBao1.WriteToPlc("M_printsignal", 1, CSReadPlc.DevName);
                }
                else
                {
                    MessageBox.Show("提示：请先启动设备");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
        }

        private void 排单列表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void 读字符串ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //读取
            Dictionary<string, object> dic = new Dictionary<string, object>();
            for (int i = 0; i < M_stcScanData.M_List_WriteToPlcSku.ToList().Count; i++)
            {
                string sku_dress = M_stcScanData.M_List_WriteToPlcSku.ToList()[i].dic_sku.ToList()[0].Key;
                string sku_val = M_stcScanData.M_List_WriteToPlcSku.ToList()[i].dic_sku.ToList()[0].Value;
                string Layernum_dress = M_stcScanData.M_List_WriteToPlcSku.ToList()[i].dic_Write_Layernum.ToList()[0].Key;
                string Layernum_val = M_stcScanData.M_List_WriteToPlcSku.ToList()[i].dic_Write_Layernum.ToList()[0].Value;
                string a=  csGetBao1.ReadPlcString(sku_dress, 20);
                csGetBao1.ReadPlc(Layernum_dress, 1);
            }
            dic = csGetBao1.ReadPlc("D5100",50);
            M_stcScanData.TiaoMaVal_Now = csGetBao1.ReadPlcString("TiaoMaVal_Now", CSReadPlc.DevName, 20);
        }

        private void toolStripMenuItem11_Click(object sender, EventArgs e)
        {
            M_stcPrintData.TestPrintFlag = "start";
            MessageBox.Show("已启动测试打印");
        }

        private void toolStripMenuItem12_Click(object sender, EventArgs e)
        {
            M_stcPrintData.TestPrintFlag = "";
            MessageBox.Show("已取消测试打印");
        }

        private void 手动打印第一张ToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult btchose = MessageBox.Show("是否打印当前标签队列中的第一张标签", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (btchose == DialogResult.OK)
                {
                    if (mCdata.IsConnect())
                    {
                        csGetBao1.WriteToPlc("M_printsignal", 1, CSReadPlc.DevName);
                    }
                    else
                    {
                        MessageBox.Show("提示：请先启动设备");
                    }
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
        }

        private void toolStripMenuItem14_Click(object sender, EventArgs e)
        {
            try
            {
                CsFormShow.FormShowDialog(CartonNOHistoricRcordsform,this,"WindowsForms01.CartonNOHistoricRcordsForm");
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
        }

        private void toolStripMenuItem16_Click(object sender, EventArgs e)
        {
            try
            {
                if (GoDevice == "")
                {
                    if (User_tSLabel2.Text.Trim().Contains("admin") || User_tSLabel2.Text.Trim().Contains("starcity"))
                    {
                        SetupDevice("Scanning");
                        if (mCdata.IsConnect())
                        {
                            GoDevice = "go";
                            pictureBox2.Visible = false;
                            pictureBox1.Visible = true;
                            MessageBox.Show("提示：设备启动完毕");
                        }
                        else
                        {
                            MessageBox.Show("提示：PLC设备未正常启动");
                        }
                    }
                    else
                    {
                        MessageBox.Show("提示：非管理员不能进行此操作");
                    }
                }
                else
                {
                    MessageBox.Show("提示：设备已经启动，请勿重复");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                MessageBox.Show("提示：" + ex.Message);
                throw new Exception("提示：" + ex.Message);
            }
        }

        private void toolStripMenuItem15_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(shieldform, this, "WindowsForms01.ShieldForm");
        }

        private void toolStripMenuItem16_Click_1(object sender, EventArgs e)
        {
            CsGetData.InPackingData();
        }

        private void 看板ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                inexcel ss = new inexcel();
                // System.Windows.Forms.OpenFileDialog fd = new OpenFileDialog();
                ofd.Filter = "EXCEL文档(*.xls)|*.xls";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    string fileName = ofd.FileName;
                    GetData.GetExcelData getData = new GetExcelData();
                    M_dt = getData.bind(fileName, "Sheet1", "A1", "F");
                }
                //dataGridView2.DataSource = null;
                #region 录入变量数据到数据库
                if (M_dt.Rows.Count > 0)
                {
                    //this.dataGridView2.DataSource = M_dt;
                    DataTable dt_insert = new DataTable();
                    dt_insert = M_dt.Copy();
                    string sqlstr = "SELECT*FROM dbo.KanBanMAINTABLE ";
                    DataTable dt = CsFormShow.GoSqlSelect(sqlstr);
                    for (int i = 0; i < dt_insert.Rows.Count; i++)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (dt_insert.Rows[i]["TAG_ID"].ToString().Trim() == dt.Rows[j]["TAG_ID"].ToString().Trim())
                                {
                                    dt_insert.Rows.Remove(dt_insert.Rows[i]);
                                }
                            }
                        }
                    }
                    if (dt_insert.Rows.Count > 0)
                    {
                        ss.insertToSql(dt_insert, "dbo.KanBanMAINTABLE");
                    }
                    //dt_insert.Columns["实际出货数量"].ColumnName = "OUTQTY";
                    MessageBox.Show("导入成功！");
                }
                else
                {
                    MessageBox.Show("没有数据！");
                }
                #endregion
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }
        public void SetupDevice()
        {
            try
            {
                if (User_tSLabel2.Text.Trim().Contains("admin") || User_tSLabel2.Text.Trim().Contains("starcity"))
                {
                    if (!mCdata.IsConnect())
                    {
                        SetupDeviceThread();
                    }
                    if (mCdata.IsConnect())
                    {
                        //GoDevice = "go";
                        pictureBox2.Visible = false;
                        pictureBox1.Visible = true;
                       // MessageBox.Show("提示：设备启动完毕");
                    }
                    if (!mCdata.IsConnect())
                    {
                        MessageBox.Show("提示：PLC设备未正常启动,请检查网络及设备是否正常");
                        System.Environment.Exit(0);
                    }
                }
                else
                {
                    MessageBox.Show("提示：非管理员不能进行此操作");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                MessageBox.Show("提示：" + ex.Message);
                throw new Exception("提示：" + ex.Message);
            }
        }

        private void 打印GAP侧面贴标信号ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (mCdata.IsConnect())
                {
                    csGetBao1.WriteToPlc("M_serialsignal", 1, CSReadPlc.DevName);
                }
                else
                {
                    MessageBox.Show("提示：请先启动设备");
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }
        }

        private void 取消喷码编号ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            M_MarkingData.IsMarkingNum = "NO";
        }

        private void gAP排单ToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void toolStripDropDownButton5_Click(object sender, EventArgs e)
        {

        }

        private void 优衣库排单ToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void 获取雅马哈计数ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsSendYaMaHa.M_IsGetYaMaHaData = true;
        }

        private void 清除雅马哈计数ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsSendYaMaHa.M_sendflag = "";
        }

        private void 切换牌照号模板ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csGetBao1.WriteToPlc("M_PackingCode", 8, CSReadPlc.DevName);
        }

        private void 换款信号ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
                 csGetBao1.WriteToPlc("M_strSwitchsignal", 1, CSReadPlc.DevName);
        }

        private void 模拟喷码赋值ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int cartornnum = 1;
            string po_guid = CsFormShow.SqlGetVal(string.Format("SELECT * FROM dbo.SchedulingTable WHERE po_guid='4B30C0F8-3F8A-4FBB-AA65-BB3346638739'"),"po_guid");
            CsGetData.SetToMakingVaria(po_guid, cartornnum);
            CsGetData.SetToPrintVaria(po_guid, cartornnum);
        }

        private void 读取公共变量ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //网单
            string catorn = CsPublicVariablies.Marking.StartNum+CsPublicVariablies.Marking.CartornNum-1 + "                " + CsPublicVariablies.Marking.PoQty;
            string po = CsPublicVariablies.Marking.Po;
            string style = CsPublicVariablies.Marking.StyleSize;
            string qty = CsPublicVariablies.Marking.Qty;
            string grossweight = CsPublicVariablies.Marking.GrossWeight;
            string NetWeight = CsPublicVariablies.Marking.NetWeight;
            //普通单
             po = CsPublicVariablies.Marking.Po;
             string skuitem = CsPublicVariablies.Marking.SkuItem;
            string unit = CsPublicVariablies.Marking.UnitPrepack;
            catorn= CsPublicVariablies.Marking.StartNum + CsPublicVariablies.Marking.CartornNum -1+ "           " + CsPublicVariablies.Marking.PoQty;
            //打印
            CsPrint.SetPrintConnect(CsPublicVariablies.PrintData.Po_guid, CsPublicVariablies.PrintData.OrderNo, CsPublicVariablies.PrintData.Cartorn, CsPublicVariablies.PrintData.Packing当前序列号);

        }

        private void 排单计划表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
           // CsFormShow.FormShowDialog(schedulingSortform, this, "WindowsForms01.SchedulingSortForm");
        }

        private void 启动关工程序ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CsInitialization.SetupIps();
        }

        private void sendMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        public void Fun_SendMessage(string str_)
        {
            IntPtr maindHwnd = FindWindow(null, "Form1"); //获得句柄   
            if (maindHwnd != IntPtr.Zero)
            {
                IntPtr maindHwndp = FindWindowEx(maindHwnd, IntPtr.Zero, null, "textRecMsg");
                if (maindHwndp != IntPtr.Zero)
                {
                    SendMessage(maindHwndp, 12, 0, str_);
                }
            }
            else
            {
                Console.WriteLine("没有找到窗口");
            }
        }

        private void textRecMsgtbox_TextChanged(object sender, EventArgs e)
        {
           CsGetData. GetIpsData( this.textRecMsgtbox.Text.ToString().Trim());
            System.Diagnostics.Process[] processList = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process process in processList)
            {
                if (process.ProcessName == "IPSMainProgram")
                {
                    Fun_SendMessage("yes");
                }
                else
                {
                   // System.Diagnostics.Process.Start(@"C:\Users\Administrator.USER-20190414RK\Desktop\ips\IPSMainProgram\IPSMainProgram\bin\Debug\IPSMainProgram.exe");
                }
            }
        }

        private void Schedulingbtn_Click_1(object sender, EventArgs e)
        {
            try
            {
                CsFormShow.FormShowDialog(SchedulingTableform, this, "WindowsForms01.SchedulingTableForm");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }

        private void PaidanbtnX1_Click(object sender, EventArgs e)
        {
            try
            {
                CsFormShow.FormShowDialog(SchedulingTableform, this, "WindowsForms01.SchedulingTableForm");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }

        private void GAPOrderbtnX1_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(packingform, this, "WindowsForms01.PackingForm");
        }

        private void YYKGUbtnX1_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(youyikuform, this, "WindowsForms01.YouyikuForm");
        }

        private void 测试发送Pc服务器ToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
        private void toolStripMenuItem17_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(Stockform,this,"WindowsForms01.StockForm");
        }
        private void NewShow()
        {
            try
            {
                foreach (Control ctr in panel1.Controls)
                {
                    //Type type=  ctr.GetType();//获取类型名
                    if (ctr is DevComponents.DotNetBar.ButtonX||ctr is ControlExs.ImageButton)
                    {
                        if (ctr.Name!= "PaiDanBtn"&& ctr.Name != "producingBtn")
                        {
                              ctr.Visible = false;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }

        private void PaidanimgBtn_Click(object sender, EventArgs e)
        {
            try
            {
                Control[] controls = new Control[] { PaiDanBtn, producingBtn };
                SetShowControls(controls);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }

        private void PaiDanBtn_Click(object sender, EventArgs e)
        {
            try
            {
                CsFormShow.FormShowDialog(SchedulingTableform, this, "WindowsForms01.SchedulingTableForm");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }

        private void YYKPackingDataBtn_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(youyikuform, this, "WindowsForms01.YouyikuForm");
        }

        private void YYKproducingBtn_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShow(YYKMarklineupform, this, "WindowsForms01.YYKMarkLineUpForm");
        }

        private void YYKHistoricalDataBtn_Click(object sender, EventArgs e)
        {
            YYKHistoricRcordsform = (WindowsForms01.YYKHistoricRcordsForm)CsFormShow.FormShow(YYKHistoricRcordsform, null, "WindowsForms01.YYKHistoricRcordsForm");
        }

        private void GAPPackingDataBtn_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(packingform, this, "WindowsForms01.PackingForm");
        }

        private void GAPproducingBtn_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(MarkingLineUpform, this, "WindowsForms01.MarkingLineUpForm");
        }

        private void GAPHistoricalDataBtn_Click(object sender, EventArgs e)
        {
            historicRcordsform = (WindowsForms01.HistoricRcordsForm)CsFormShow.FormShow(historicRcordsform, null, "WindowsForms01.HistoricRcordsForm");
        }

        private void PDFBtn_Click(object sender, EventArgs e)
        {
            try
            {
                CsFormShow.FormShowDialog(pdfform, this, "WindowsForms01.PdfForm");
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }

        private void offLineBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (scanpackingform == null || scanpackingform.IsDisposed)   //注意先判断null，再判断IsDisposed，不能先判断IsDisposed
                {
                    scanpackingform = new ScanPackingForm(mCdata);
                    scanpackingform.Owner = this;
                    scanpackingform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    scanpackingform.ShowDialog();
                }
                else
                {
                    scanpackingform.Show();
                    scanpackingform.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                    scanpackingform.BringToFront();
                }
            }
            catch (Exception ex)
            {
                Cslogfun.WriteToLog(ex);
                throw new Exception("提示：" + ex);
            }
        }

        private void MarkDateBtn_Click(object sender, EventArgs e)
        {
            CsFormShow.FormShowDialog(markingdateform, this, "WindowsForms01.MarkingDateForm");
        }

        private void DataInBtn_Click(object sender, EventArgs e)
        {
            CsGetData.InPackingData();
        }

        private void producingBtn_Click(object sender, EventArgs e)
        {
            try
            {
                CsFormShow.FormShowDialog(Producingform, this, "WindowsForms01.ProducingForm");
                new ProducingForm();
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err);
                throw new Exception("提示：" + err);
            }
        }

        public void SetShowControls( Control [] controls_)
        {
            try
            {
                foreach (Control ctr in panel1.Controls)
                {
                    //Type type=  ctr.GetType();//获取类型名
                    if (ctr is DevComponents.DotNetBar.ButtonX||ctr is ControlExs.ImageButton)
                    {
                        int  iexist= Array.IndexOf(controls_,ctr);
                        if (iexist!=-1)
                        {
                            ctr.Visible = true;
                        }
                        else
                        {
                            ctr.Visible = false;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }

        private void YYKimgBtn_Click(object sender, EventArgs e)
        {
            try
            {
                Control[] controls = new Control[] { YYKPackingDataBtn, YYKproducingBtn, YYKHistoricalDataBtn };
                SetShowControls(controls);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }

        private void GAPimgBtn_Click(object sender, EventArgs e)
        {
            try
            {
                Control[] controls = new Control[] { GAPHistoricalDataBtn, GAPPackingDataBtn, GAPproducingBtn ,PDFBtn};
                SetShowControls(controls);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }

        private void OthersimgBtn_Click(object sender, EventArgs e)
        {
            try
            {
                Control[] controls = new Control[] { offLineBtn, MarkDateBtn, DataInBtn };
                SetShowControls(controls);
            }
            catch (Exception err)
            {
                Cslogfun.WriteToLog(err.ToString());
                MessageBox.Show(err.ToString());
            }
        }

        private void Stoplb_Click(object sender, EventArgs e)
        {

        }

        private void 扫描卷号ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 测试串口打印ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CSReadPlc.serialTestFlag = "start";
        }

        private void Device_tSCboBox_Click(object sender, EventArgs e)
        {

        }
    }
}

